# !/usr/bin/python
# -*-coding:utf-8-*-
# Author：Mongoole
# Date：2020/9/9
import urllib
import re
import os
import time
import requests

#可下载视频url
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36',
    }
with open('download_url_lst','r',encoding='utf-8') as f:
    all_url = f.readlines()

path = "E:\\study\\spider\\Project\\peopledaily\\videos\\"
length = len(all_url)

for num in range(length):
    url = all_url[num].strip()
    response = requests.get(url,headers=headers)
    with open(path + '{}.mp4'.format(num),'wb') as f:
        f.write(response.content)
        f.flush()
    time.sleep(3)
    print(
        '--------------------Page num {} Finished---------------------'.format(num)
    )
