# -*-coding:utf-8 -*-
# Project:
# Author:mongoole
# Date:2022/1/14

import io
import re
import sys
import logging
import json
import hashlib
import urllib3
import time
import requests
import cchardet

try:
    from pm import mongo
except:
    from peopledaily.pm import mongo

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')

def get_html(urls, collection):

    headers = {
        'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36'
    }
    # urls = ['https://wap.peopleapp.com/article/rmh24205166/rmh24205166']
    for url in urls:
        data_dict = {}
        weburl = url.strip()
        uid = weburl.split('/')[-2]
        articleid = weburl.split('/')[-1]
        security = security_key(uid)
        url = 'https://app.peopleapp.com/WapApi/610/ArtInfoApi/getInfoUp?id={}' \
              '&securitykey={}&interface_code=610'.format(uid,security)

        response = requests.get(url,headers=headers)

        if response.status_code == 200:
            html_chardet = cchardet.detect(response.content)
            # print(response.headers["content-type"])
            # print(response.encoding)
            # print(html_chardet)
            response.encoding = html_chardet["encoding"]
            if articleid.isdigit():
                html_content = response.text
            else:
                html_content = response.text.encode("utf-8").decode("unicode_escape")

            try:
                content = re.findall(r'"contents"(.*?)",',html_content,re.M|re.S)[0].replace(':"','')
                dr = re.compile(r'<[^>]+>', re.S)
                content_text = dr.sub('', content).strip()
                text = re.sub(u"([^\u4e00-\u9fa5\u0030-\u0039\u0041-\u005a\u0061-\u007a])", "", content_text)
            except Exception as e:
                print('Re Error:{} and URL: {}'.format(e,weburl))


            data_dict['uid'] = uid
            data_dict['article_id'] = articleid
            data_dict['article_url'] = weburl
            data_dict['article_text'] = text
            # print(data_dict)
            # breakpoint()
            collection[1].insert_one(data_dict)
            logging.info("ArticleID: {}  Insert the Data Sucessfully!".format(articleid,url))
            time.sleep(1)


def read_file():
    with open(
            r'article_url', encoding='utf-8'
    ) as f:
        urls = f.readlines()

    return urls


def security_key(uid):
    #n = "id=rmh25920687|interface_code=610"
    #id=rmh25920687|interface_code=610rmj$cd&2e09elp468b7b87dlsye#jipl
    data = "id=" + uid + "|" + "interface_code=610"
    key = "rmj$cd&2e09elp468b7b87dlsye#jipl"
    strs = data + key
    # print(strs)
    security = hashlib.md5(strs.encode(encoding='utf-8')).hexdigest()

    return security


def main():
    collection = mongo()
    article_urls = read_file()
    get_html(article_urls, collection)


if __name__ == '__main__':
    main()