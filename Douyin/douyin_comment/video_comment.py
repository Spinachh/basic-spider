# -*- coding:utf-8 -*-
# Author: mongoole
# Date: 2023-08-17

import io
import re
import os
import sys
import json
import time
import yaml
import pymongo
import logging
import requests

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')

project_name = 'None'
project_timezone = '2023-'
since_time = time.mktime(time.strptime('2020-07-01 00:00:00', '%Y-%m-%d %H:%M:%S'))


def mongodb():
    # connect mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # choose the mongodb
    db = client.douyin_content
    # choose the collection
    mongo = db.video_comment_content
    return mongo


def get_user_url():
    with open(r'E:\kexieproject\douyin\douyin_pc_spider\data_user\douyin_video_comment_url.txt', 'r') as f:
        user_account_urls = f.readlines()
    return user_account_urls


def get_cookie_yml():
    with open(r'E:\kexieproject\douyin\douyin_pc_spider\application.yml', 'rb') as f:
        cookie_config = yaml.safe_load(f)
    return cookie_config


def get_comment_detail(comment_lst):
    # get video detail information eg:like_count, digg_count,comment_count
    for comment in comment_lst:
        comment_info_dict = {'root_aweme_id': comment.get('aweme_id'), 'cid': comment.get('cid')}

        create_time = comment.get('create_time')
        if int(create_time) > int(since_time):

            comment_info_dict['create_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(create_time))
            comment_info_dict['digg_count'] = comment.get('digg_count')
            comment_info_dict['image_list'] = comment.get('image_list')
            comment_info_dict['ip_label'] = comment.get('ip_label')
            comment_info_dict['item_comment_total'] = comment.get('item_comment_total')
            comment_info_dict['reply_comment'] = comment.get('reply_comment')
            comment_info_dict['reply_comment_total'] = comment.get('reply_comment_total')
            comment_info_dict['reply_id'] = comment.get('reply_id')
            comment_info_dict['reply_to_reply_id'] = comment.get('reply_to_reply_id')
            comment_info_dict['reply_to_reply_id'] = comment.get('reply_to_reply_id')
            comment_info_dict['text'] = comment.get('text')
            comment_info_dict['text_music_info'] = comment.get('text_music_info')

            # VIDEO'S AUTHOR INFORMATION
            author_information = comment.get('user')
            comment_info_dict['aweme_count'] = author_information.get('aweme_count')
            comment_info_dict['bind_phone'] = author_information.get('bind_phone')
            comment_info_dict['authority_status'] = author_information.get('authority_status')
            comment_info_dict['favoriting_count'] = author_information.get('favoriting_count')
            comment_info_dict['follower_count'] = author_information.get('follower_count')
            comment_info_dict['following_count'] = author_information.get('following_count')
            comment_info_dict['is_phone_binded'] = author_information.get('is_phone_binded')
            comment_info_dict['nickname'] = author_information.get('nickname')
            comment_info_dict['region'] = author_information.get('region')
            comment_info_dict['enterprise_verify_reason'] = ''
            comment_info_dict['sec_uid'] = author_information.get('sec_uid')
            comment_info_dict['short_id'] = author_information.get('short_id')
            comment_info_dict['signature'] = author_information.get('signature')
            comment_info_dict['unique_id'] = author_information.get('unique_id')
            comment_info_dict['unique_id_modify_time'] = author_information.get('unique_id_modify_time')
            comment_info_dict['uid'] = author_information.get('uid')

            # insert to mongodb
            # mongodb().insert(comment_info_dict)
            print(comment_info_dict)
            logging.warning('TEXT:{} SUCCESSFULLY INSERT TO MONGODB.'
                            .format(comment_info_dict.get('text'), ))
            breakpoint()
        else:
            logging.warning('THIS COMMENT INFORMATION\'S TIME WAS DEAD.')


def get_comment_lst(user_page_url):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; ''Nexus 5 Build/MRA58N) AppleWebKit/537.36 ('
                      'KHTML, like Gecko) Chrome/80.0.3987.116 Mobile Safari/537.36',
        'referer': 'https://www.douyin.com/',
        'cookie': str(get_cookie_yml()['cookie']),
    }

    resp = requests.get(user_page_url, headers=headers)
    # logging.warning(user_page_url)
    try:
        if resp.status_code == 200:
            page_lst = resp.json()
            # print(page_lst)
            # breakpoint()
            comment_lst = page_lst.get('comments')
            max_cursor = page_lst.get('cursor')
            # min_cursor = page_lst.get('min_cursor')
            has_more = page_lst.get('has_more')

            return max_cursor, comment_lst, has_more

    except Exception as e:
        logging.warning('THE GET_COMMENT_LST FUNCTION HAS ERROR: {}'.format(e))


def get_max_cursor(user_url):
    # user_url = 'https://www.douyin.com/user/MS4wLjABAAAAn8tvTQ0rdhV8gqjpEJ6MIi82k1p_IGu3dQZKlJE2wH8?modal_id
    # =7224032097978174776&showTab=post'

    comment_gid = user_url.split('#####')[1]
    max_cursor = 0

    while 1:
        user_page_url = 'https://www.douyin.com/aweme/v1/web/comment/list/?' \
                        'device_platform=webapp&aid=6383&channel=channel_pc_web' \
                        '&aweme_id={comment_gid}&cursor={max_cursor}&count=20' \
                        '&item_type=0'.format(comment_gid=comment_gid, max_cursor=max_cursor)
        next_max_cursor, comment_lst, has_more = get_comment_lst(user_page_url)
        # print(comment_lst)
        # breakpoint()
        if next_max_cursor:
            get_comment_detail(comment_lst)
            max_cursor = next_max_cursor
            if next_max_cursor == 0:
                break
        else:
            break
        time.sleep(5)


def main():
    # example
    for user_url in get_user_url():
        get_max_cursor(user_url)
        time.sleep(10)


if __name__ == '__main__':
    main()
