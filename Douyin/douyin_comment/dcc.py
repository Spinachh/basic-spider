import json
import time
import pymongo
from pymongo.errors import DuplicateKeyError


def mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1',port=27017)
    #选择数据库
    db = client.DouYin
    #选择操作的集合
    collection_text = db.comment_2021_06_25
    return collection_text


collection = mongo()
# print(collection)
#必须这样写！因为是要获取响应的内容，mitmdump规定必须这样写才能获取到数据,所以必须写response

def response(flow):


    if "aweme/v2/comment/list" in flow.request.url:
        print("{}We got The Data!{}".format('-'*10,'-'*10))
        text = flow.response.text
        content = json.loads(text)
        results = content["comments"]
        for item in results:
            data = {}
            data["nick_name"] = item["user"]["nickname"]
            data["aweme_count"] = item["user"]["aweme_count"]
            data["birthday"] = item["user"]["birthday"]
            data["follower_count"] = item["user"]["follower_count"]
            data["gender"] = item["user"]["gender"]
            data["location"] = item["user"]["location"]
            # data["phone"] = item["user"]["bind_phone"]
            data["signature"] = item["user"]["signature"]
            data["uid"] = item["user"]["uid"]
            data["aweme_id"] = item["aweme_id"]
            create_time = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(item["create_time"]))
            data["create_time"] = create_time
            data["digg_count"] = item["digg_count"]
            data["text"] = item["text"]
            data["account_name"] = '共青团'
            collection.insert(data)
            print("{}Insert into MongoDB Sucessfully!{}".format('-'*10,'-'*10))
