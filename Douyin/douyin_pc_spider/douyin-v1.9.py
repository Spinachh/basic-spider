# -*- coding:utf-8 -*-
# Author: mongoole
# Date: 2023-08-18
# FILTER "TOP VIDEO" THAT IF THE VIDEO HAS NOT IN FILTER TIME

import io
import re
import os
import sys
import json
import time
import yaml
import pymongo
import logging
import requests

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')

project_name = 'KeXie'
project_timezone = '2023-Q2-Q3-half'
since_time = time.mktime(time.strptime('2023-04-01 00:00:00', '%Y-%m-%d %H:%M:%S'))


def mongodb():
    # connect mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # choose the mongodb
    db = client.douyin_content
    # choose the collection
    mongo = db.user_content_Q2
    return mongo


def get_user_url():
    with open(r'E:\kexieproject\douyin\douyin_pc_spider\data_user\douyin_user_account_url.txt', 'r') as f:
        user_account_urls = f.readlines()
    return user_account_urls


def get_cookie_yml():
    with open(r'E:\kexieproject\douyin\douyin_pc_spider\application.yml', 'rb') as f:
        cookie_config = yaml.safe_load(f)
    return cookie_config


def get_video_detail(video_lst, mongo):
    # 1. GET VIDEO DEATIL INFORMATION EG:LIKE_COUNT, DIGG_COUNT,COMMENT_COUNT
    # 2. JUDEG THE VIDEO\'S FLAG (IS_TOP == 0)

    for video in video_lst:
        video_info_dict = {}
        video_info_dict['aweme_id'] = video.get('aweme_id')
        video_info_dict['aweme_type'] = 'show_video'
        video_info_dict['comment_gid'] = video.get('comment_gid')
        create_time = video.get('create_time')
        TOP_FLAG = video.get('is_top')

        if TOP_FLAG == 0:
            if int(create_time) > int(since_time):
                video_info_dict['create_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(create_time))
                video_info_dict['desc'] = video.get('desc')
                video_info_dict['duration'] = video.get('duration')
                video_info_dict['title'] = video.get('preview_title')
                video_info_dict['share_url'] = video.get('share_url')
                video_info_dict['admire_count'] = video.get('statistics').get('admire_count')
                video_info_dict['collect_count'] = video.get('statistics').get('collect_count')
                video_info_dict['comment_count'] = video.get('statistics').get('comment_count')
                video_info_dict['digg_count'] = video.get('statistics').get('digg_count')
                video_info_dict['play_count'] = video.get('statistics').get('play_count')
                video_info_dict['share_count'] = video.get('statistics').get('share_count')

                # video information tag
                tag_lst = video.get('video_tag')
                tags = []
                for item in tag_lst:
                    tag = item.get('tag_name')
                    tags.append(tag)

                video_info_dict['tag'] = tags

                # video information author information
                author_information = video.get('author')
                video_info_dict['video_count'] = author_information.get('aweme_count')
                video_info_dict['verify_reason'] = author_information.get('enterprise_verify_reason')
                video_info_dict['favoriting_count'] = author_information.get('favoriting_count')
                video_info_dict['following_count'] = author_information.get('following_count')
                video_info_dict['fans'] = author_information.get('follower_count')
                video_info_dict['total_favorited'] = author_information.get('total_favorited')
                video_info_dict['author_user_id'] = author_information.get('uid')
                video_info_dict['nickname'] = author_information.get('nickname')
                video_info_dict['project_name'] = project_name
                video_info_dict['project_timezone'] = project_timezone

                # insert to mongodb
                mongo.insert_one(video_info_dict)
                logging.warning('ACCOUNT:{} TITLE:{} PUBLISH:{} INSERT TO MONGODB.'
                                .format(video.get('author').get('nickname'), video_info_dict.get('title').strip()[:15],
                                        video_info_dict.get('create_time')))
                time.sleep(0.5)
            else:
                logging.warning('{} ACCOUNT:{} THIS VIDEO INFORMATION\'S TIME WAS DEAD.{}'
                                .format('*' * 15, video_info_dict.get('nickname'), '*' * 15))
                deadline = True
                return deadline


def get_user_video_lst(user_page_url):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; ''Nexus 5 Build/MRA58N) AppleWebKit/537.36 ('
                      'KHTML, '
                      'like Gecko) Chrome/80.0.3987.116 Mobile Safari/537.36',
        'referer': 'https://www.douyin.com/',
        'cookie': str(get_cookie_yml()['cookie']),
    }

    resp = requests.get(user_page_url, headers=headers)
    # print(user_page_url)
    try:
        if resp.status_code == 200:
            page_lst = resp.json()
            # print(page_lst)
            # breakpoint()
            video_lst = page_lst.get('aweme_list')
            max_cursor = page_lst.get('max_cursor')
            # min_cursor = page_lst.get('min_cursor')
            has_more = page_lst.get('has_more')

            return max_cursor, video_lst, has_more

    except Exception as e:
        logging.warning('THE GET_USER_VIDEO_LST FUNCTION HAS ERROR: {}'.format(e))


def get_max_cursor(user_url, mongo):
    # user_url = 'https://www.douyin.com/user/MS4wLjABAAAA5UB13GANlM_l19f05Ypp9UR4Hw5XcBAu1fL_fc6OQMAEng
    # -9417jTjZqAyeUe4u3 '
    sec_uid = user_url.split('/')[-1]
    max_cursor = 0

    while 1:
        user_page_url = 'https://www.douyin.com/aweme/v1/web/aweme/post/?aid=6383' \
                        '&sec_user_id={sec_uid}&count=10&max_cursor={max_cursor}' \
                        '&publish_video_strategy_type=2'.format(sec_uid=sec_uid, max_cursor=max_cursor)
        next_max_cursor, video_lst, has_more = get_user_video_lst(user_page_url)
        # print(video_lst)
        # breakpoint()
        if next_max_cursor:
            deadline = get_video_detail(video_lst, mongo)
            if has_more == 0:
                break

            if deadline:
                break
            else:
                max_cursor = next_max_cursor
                time.sleep(5)
        else:
            break


def main():
    # example
    # user_url = 'https://www.douyin.com/user/MS4wLjABAAAACmUNK8yr2VV3JyWj5_zVxjC0llSWAcYSjtOV0EdyQxs'
    # user_url = 'https://www.douyin.com/user/MS4wLjABAAAAn8tvTQ0rdhV8gqjpEJ6MIi82k1p_IGu3dQZKlJE2wH8'
    mongo = mongodb()
    for user_url in get_user_url():
        get_max_cursor(user_url, mongo)
        time.sleep(10)


if __name__ == '__main__':
    main()
    # get_cookie_yml()
