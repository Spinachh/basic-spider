# -*- coding:utf-8 -*-
# Author: mongoole
# Date: 2023-08-21

import io
import re
import os
import sys
import json
import time
import yaml
import pymongo
import logging
import requests

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')

project_name = 'None'
project_timezone = '2023-'
since_time = time.mktime(time.strptime('2020-07-01 00:00:00', '%Y-%m-%d %H:%M:%S'))
until_time = time.mktime(time.strptime('2020-07-01 00:00:00', '%Y-%m-%d %H:%M:%S'))


def mongodb():
    # connect mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # choose the mongodb
    db = client.douyin_content
    # choose the collection
    mongo = db.search_keyword_content
    return mongo


def get_keywords():
    with open(r'E:\kexieproject\douyin\douyin_pc_spider\data_user\douyin_keywords_video.txt', 'r') as f:
        user_account_urls = f.readlines()
    return user_account_urls


def get_cookie_yml():
    with open(r'E:\kexieproject\douyin\douyin_pc_spider\application.yml', 'rb') as f:
        cookie_config = yaml.safe_load(f)
    return cookie_config


def get_video_detail(video_lst):
    # get video detail information eg:like_count, digg_count,comment_count
    for vinfo in video_lst:
        video_info_dict = {}
        video = vinfo.get('aweme_info')

        create_stamp_time = video.get('create_time')
        
        if int(create_stamp_time) > int(since_time):

            video_info_dict['create_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(create_stamp_time))
            video_info_dict['author_user_id'] = video.get('author_user_id')
            video_info_dict['aweme_id'] = video.get('aweme_id')
            video_info_dict['desc'] = video.get('desc')

            # SHARE INFORMATION
            share_info = video.get('share_info')

            video_info_dict['share_title'] = share_info.get('share_title')
            video_info_dict['share_link_desc'] = share_info.get('share_link_desc')
            video_info_dict['share_url'] = share_info.get('share_url')

            # STATIC INFORMATION
            static_info = video.get('statistics')

            video_info_dict['collect_count'] = static_info.get('collect_count')
            video_info_dict['comment_count'] = static_info.get('comment_count')
            video_info_dict['digg_countv'] = static_info.get('digg_count')
            video_info_dict['download_count'] = static_info.get('download_count')
            video_info_dict['forward_count'] = static_info.get('forward_count')
            # video_info_dict['live_watch_count'] = static_info.get('live_watch_count')
            # video_info_dict['lose_comment_count'] = static_info.get('lose_comment_count')
            # video_info_dict['lose_count'] = static_info.get('lose_count')
            video_info_dict['play_count'] = static_info.get('play_count')
            video_info_dict['share_count'] = static_info.get('share_count')
            # video_info_dict['whatsapp_share_count'] = static_info.get('whatsapp_share_count')

            # VIDEO'S AUTHOR INFORMATION
            author_information = video.get('author')
            video_info_dict['aweme_count'] = author_information.get('aweme_count')
            video_info_dict['bind_phone'] = ''
            video_info_dict['authority_status'] = ''
            video_info_dict['favoriting_count'] = author_information.get('favoriting_count')
            video_info_dict['follower_count'] = author_information.get('follower_count')
            video_info_dict['following_count'] = author_information.get('following_count')
            video_info_dict['is_phone_binded'] = ''
            video_info_dict['nickname'] = author_information.get('nickname')
            video_info_dict['region'] = ''
            video_info_dict['enterprise_verify_reason'] = author_information.get('enterprise_verify_reason')
            video_info_dict['sec_uid'] = author_information.get('sec_uid')
            video_info_dict['short_id'] = author_information.get('short_id')
            video_info_dict['signature'] = author_information.get('signature')
            video_info_dict['unique_id'] = author_information.get('unique_id')
            video_info_dict['unique_id_modify_time'] = ''
            video_info_dict['uid'] = author_information.get('uid')

            # insert to mongodb
            # mongodb().insert(video_info_dict)
            print(video_info_dict)
            logging.warning('DESC:{} SUCCESSFULLY INSERT TO MONGODB.'
                            .format(video.get('desc'), ))
            breakpoint()
        else:
            logging.warning('THIS VIDEO INFORMATION\'S TIME WAS DEAD.')


def get_video_lst(user_page_url):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; ''Nexus 5 Build/MRA58N) AppleWebKit/537.36 ('
                      'KHTML, like Gecko) Chrome/80.0.3987.116 Mobile Safari/537.36',
        'referer': 'https://www.douyin.com/',
        'cookie': str(get_cookie_yml()['cookie']),
    }

    resp = requests.get(user_page_url, headers=headers)
    # logging.warning(user_page_url)
    try:
        if resp.status_code == 200:
            page_lst = resp.json()
            # print(page_lst)
            # breakpoint()
            video_lst = page_lst.get('data')
            next_offset = page_lst.get('cursor')
            # min_cursor = page_lst.get('min_cursor')
            has_more = page_lst.get('has_more')

            return next_offset, video_lst, has_more

    except Exception as e:
        logging.warning('THE GET_COMMENT_LST FUNCTION HAS ERROR: {}'.format(e))


def get_max_cursor(keyword):
    # user_url = 'https://www.douyin.com/user/MS4wLjABAAAAn8tvTQ0rdhV8gqjpEJ6MIi82k1p_IGu3dQZKlJE2wH8?modal_id
    # =7224032097978174776&showTab=post'

    offset = 0
    while 1:
        user_page_url = 'https://www.douyin.com/aweme/v1/web/search/item/?device_platform=webapp&aid=6383' \
                        '&channel=channel_pc_web&search_channel=aweme_video_web&sort_type=0&publish_time=0' \
                        f'&keyword={keyword}&search_source=normal_search&query_correct_type=1&is_filter_search=0' \
                        f'&from_group_id=&offset={offset}&count=10'.format(keyword=keyword, offset=offset)
        next_offset, video_lst, has_more = get_video_lst(user_page_url)
        # print(comment_lst)
        # breakpoint()
        if next_offset:
            get_video_detail(video_lst)
            if has_more == 0:
                break
            else:
                offset = next_offset
        else:
            break
        time.sleep(5)


def main():
    # example
    for keyword in get_keywords():
        get_max_cursor(keyword)
        time.sleep(10)


if __name__ == '__main__':
    main()
