import json
import time
import pymongo
from pymongo.errors import DuplicateKeyError


def mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1',port=27017)
    #选择数据库
    db = client.DouyinNewKePu
    #选择操作的集合
    collection_info = db.user_info
    collection_content = db.content_add
    return collection_info,collection_content


collection = mongo()
# print(collection)
#必须这样写！因为是要获取响应的内容，mitmdump规定必须这样写才能获取到数据,所以必须写response

def response(flow):

    #获取用户信息
    if "aweme/v1/user/profile/other/" in flow.request.url:
        print("---------- We got The User Information ----------")
        text = flow.response.text
        content = json.loads(text)
        user_nick_name = content["user"]["nickname"]
        user_aweme_count = content["user"]["aweme_count"]
        user_birthday = content["user"]["birthday"]
        user_city = content["user"]["city"]
        user_country = content["user"]["country"]
        user_cover_url = content["user"]["cover_url"][0]["url_list"][0]
        user_enterprise_verify_reason = content["user"]["enterprise_verify_reason"]
        user_favoriting_count = content["user"]["favoriting_count"]
        user_follower_count = content["user"]["follower_count"]
        user_news_article_follow_count = content["user"]["followers_detail"][1]["fans_count"]
        user_live_stream_follow_count = content["user"]["followers_detail"][2]["fans_count"]
        user_mplatform_count = content["user"]["mplatform_followers_count"]


        try:
            collection[0].insert_one(
                {"_id":user_nick_name,"dongtai":user_aweme_count,"birthday":user_birthday,"city":user_city,
                 "country":user_country,"cover_url":user_cover_url,"verify":user_enterprise_verify_reason,
                 "favorit_count":user_favoriting_count,"follower":user_follower_count,
                 "mplatform_count":user_mplatform_count,"news_article_follow":user_news_article_follow_count,
                 "live_stream_follow":user_live_stream_follow_count
                 }
            )
        except DuplicateKeyError as e:
            collection[0].find_one_and_update(
                {"_id": user_nick_name,"dongtai":user_aweme_count,"birthday": user_birthday, "city": user_city,
                 "country": user_country, "cover_url": user_cover_url, "verify": user_enterprise_verify_reason,
                 "favorit_count": user_favoriting_count, "follower": user_follower_count,
                 "mplatform_count": user_mplatform_count, "news_article_follow": user_news_article_follow_count,
                 "live_stream_follow": user_live_stream_follow_count
                 }
            )

    #获取视频信息

    if "aweme/v1/aweme/post/" in flow.request.url:
        print("----------We got The Data----------")
        text = flow.response.text
        content = json.loads(text)
        results = content["aweme_list"]
        for item in results:
            data = {}
            data["nick_name"] = item["author"]["nickname"]
            data["user_id"] = item["author_user_id"]
            data["aweme_id"] = item["aweme_id"]
            data["desc"] = item["desc"]
            create_time = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(item["create_time"]))
            data["create_time"] = create_time
            data["duration"] = item["duration"]
            data["share_url"] = item["share_url"]
            data["comment_count"] = item["statistics"]["comment_count"]
            data["digg_count"] = item["statistics"]["digg_count"]
            data["download_count"] = item["statistics"]["download_count"]
            data["play_count"] = item["statistics"]["play_count"]
            data["share_count"] = item["statistics"]["share_count"]
            data["author_user_id"] = item["author_user_id"]
            data["long_video"] = item["long_video"]
            data["account_block"] = '作品'
            collection[1].insert(data)
            print("----------Insert into MongoDB Sucessfully----------")


    #获取收藏视频信息
    if "aweme/v1/mix/aweme" in flow.request.url:
        print("----------We got The Data----------")
        text = flow.response.text
        content = json.loads(text)
        results = content["aweme_list"]
        for item in results:
            data = {}
            data["nick_name"] = item["author"]["nickname"]
            data["user_id"] = item["author_user_id"]
            data["aweme_id"] = item["aweme_id"]
            data["desc"] = item["desc"]
            create_time = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(item["create_time"]))
            data["create_time"] = create_time
            data["duration"] = item["duration"]
            data["share_url"] = item["share_url"]
            data["comment_count"] = item["statistics"]["comment_count"]
            data["digg_count"] = item["statistics"]["digg_count"]
            data["download_count"] = item["statistics"]["download_count"]
            data["play_count"] = item["statistics"]["play_count"]
            data["share_count"] = item["statistics"]["share_count"]
            data["author_user_id"] = item["author_user_id"]
            data["long_video"] = item["long_video"]
            data["account_block"] = '收藏合集'
            collection[1].insert(data)
            print("{}Insert Favorites into MongoDB Sucessfully{}".format("-" * 10,"-" * 10))
