#!/usr/bin/python3
# -*- coding:utf-8 -*-
# Author : Mongoole
# Date : 2022/1/19

import os
import io
import re
import sys
import time
import json
import pymongo
import logging
import asyncio
import cchardet
import requests
from fake_useragent import UserAgent


sys.stdout = io.TextIOWrapper(sys.stdout.buffer,encoding='gb18030') #改变标准输出的默认编码
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(filename)s - %(levelname)s: %(message)s')


def mongodb():
    # 连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # 选择数据库
    db = client.facebook_content
    # 选择操作的集合
    mongo = db.user_content

    return mongo


def get_response(url, cursor):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36',
        'Cookie': 'sb=Dn6XX-UfZhWadAAyz5TyKfLh; datr=Dn6XXwPhYm5ckIK29w-ZXy77; m_pixel_ratio=1; locale=en_US; c_user=100076272891585; xs=29%3AWCExtjW1HN3gTw%3A2%3A1642576591%3A-1%3A-1; fr=0YqrEUDeo8iYgsQhC.AWX2MZXuRklG-jpRqS9ygbKViRk.Bh5lgm.lZ.AAA.0.0.Bh57rQ.AWXgWMSoZxk; x-referer=eyJyIjoiL2JiY25ld3MvP3RzaWQ9MC4xOTA4MzAyNzEyNjQxMzg1NCZzb3VyY2U9cmVzdWx0IiwiaCI6Ii9iYmNuZXdzLz90c2lkPTAuMTkwODMwMjcxMjY0MTM4NTQmc291cmNlPXJlc3VsdCIsInMiOiJtIn0%3D; wd=1920x337; spin=r.1004979175_b.trunk_t.1643166703_s.1_v.2_; presence=C%7B%22t3%22%3A%5B%5D%2C%22utc3%22%3A1643166705014%2C%22v%22%3A1%7D',
        'referer': 'https://www.facebook.com/bbcnews',
    }

    data = {
        'av': '100076272891585',
        '__user': '100076272891585',
        '__a': '1',
        '__dyn': '7xe6HwkEng5KbwKBAo2vwAxu13w8CewSwMwyzE2qwJw4BwUx609vCwjE1xoszUswuo662y11xmfz83WwgEcHzoaEd82ly87e2l0Fw4HwnEfoowYwpHxG1Pxi4UaEW0D888brwKxm5o7G4UnDw-wUw9q0ium2S3qazo11E2cwMw',
        '__csr': 'gF5Oqffb98ABWq9lTn4C8GvJkDFszQ8b9TZJq8KTRKJBKuO6VYCF9bGl6ipVeJp9oyqamXBAzbACGrxmuXV-m2icBCyoS3au8ggybLzaxaEG49Uy58lAG498kwCAGbCBxm6bgc89VEC4kq2TgKfx6E8E4m685C1lxqcz40yU98c8G482wwbu1hzU5W5U9U420UWCzUao5-U4e0FU25wpU26xO0196w0vQE3gyC0jm1RwcS04Piw0_ww4xw',
        '__req': '6',
        '__hs': '19018.HYP:comet_pkg.2.1.0.2.',
        'dpr': '1',
        '__ccg': 'MODERATE',
        '__rev': '1004979175',
        '__s': 'yd3q9a:hqogdj:d90tci',
        '__hsi': '7057347278207906281-0',
        '__comet_req': '1',
        'fb_dtsg': 'AQGOU9Ie-Xh1Dfk:29:1642576591',
        'jazoest': '21924',
        'lsd': 'u6DxXdufYnFBbGCBzMER3p',
        '__spin_r': '1004979175',
        '__spin_b': 'trunk',
        '__spin_t': '1643166709',
        'fb_api_caller_class': 'RelayModern',
        'fb_api_req_friendly_name': 'CometModernPageFeedPaginationQuery',
        'variables': '{"UFI2CommentsProvider_commentsKey":"CometSinglePageContentContainerFeedQuery",'
                     '"count":3,"cursor":"%s","displayCommentsContextEnableComment":null,'
                     '"displayCommentsContextIsAdPreview":null,"displayCommentsContextIsAggregatedShare":null,'
                     '"displayCommentsContextIsStorySet":null,"displayCommentsFeedbackContext":null,'
                     '"feedLocation":"PAGE_TIMELINE","feedbackSource":22,"focusCommentID":null,'
                     '"privacySelectorRenderLocation":"COMET_STREAM","renderLocation":"timeline","scale":1,'
                     '"useDefaultActor":false,"id":"228735667216"}' % cursor,
        'server_timestamps': 'true',
        'doc_id': '4742214935862560',
    }

    # aa = json.loads(data['variables'])
    # aa['cursor'] = cursor
    # data['variables'] = str(aa)
    # print(type(data))
    # print(data)
    try:
        response = requests.post(url, headers=headers, data=data)
        # print(response.text)
        # breakpoint()
        if response.status_code == 200:
            html_content = response.text
            results = html_content.strip().split('\n')
            html_content = results[0]
            html_content = json.loads(html_content)
            return html_content

    except Exception as e:
        print(e)


def get_data(response, mongo):
    # print('results: {}'.format(response))
    results = response['data']['node']['timeline_feed_units']['edges']

    for data in results:
        data_dict = {}
        item = data['node']['comet_sections']
        text = item['content']['story']['comet_sections']['message']['story']['message']['text']
        creation_time_stamp = item['context_layout']['story']['comet_sections']['metadata'][0]['story']['creation_time']
        creation_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(creation_time_stamp)))
        url = item['context_layout']['story']['comet_sections']['metadata'][0]['story']['url']
        like = \
            item['feedback']['story']['feedback_context']['feedback_target_with_context']['ufi_renderer']['feedback'][
                'comet_ufi_summary_and_actions_renderer']['feedback']['reaction_count']['count']
        # comment = item['feedback']['story']['feedback_context']['feedback_target_with_context']['ufi_renderer']['feedback']['comet_ufi_summary_and_actions_renderer']['feedback']['comment_count']['total_count']
        comment = \
            item['feedback']['story']['feedback_context']['feedback_target_with_context']['ufi_renderer']['feedback'][
                'comment_count']['total_count']
        share = \
            item['feedback']['story']['feedback_context']['feedback_target_with_context']['ufi_renderer']['feedback'][
                'comet_ufi_summary_and_actions_renderer']['feedback']['share_count']['count']

        # print(creation_time, like, comment, share, text, url)
        logging.info('Time: {} Url: {}'.format(creation_time, url))
        data_dict['author'] = 'BBC News'
        data_dict['text'] = text
        data_dict['creation_time'] = creation_time
        data_dict['like'] = like
        data_dict['comment'] = comment
        data_dict['share'] = share
        data_dict['url'] = url
        mongo.insert(data_dict)

    end_cursor = response['data']['node']['timeline_feed_units']['page_info']['end_cursor']
    has_next_page = response['data']['node']['timeline_feed_units']['page_info']['has_next_page']

    return end_cursor, has_next_page


def main():
    mongo = mongodb()
    cursor = 'AQHRrWyVcDblkuNe5-TBPPU0Sc2FQgqRL3vrm0tFzI8gi9zoEGMQjupYMKw93YkxWMN_SUbswy-v4drDWO-OBsURxr2LFlo1cpZoaE4G33KyHtMX8B-4wm8g1kg0WgmrQ1rN'
    url = 'https://www.facebook.com/api/graphql/'
    num = 0

    while True:
        # print('cursor:{}'.format(cursor))
        response = get_response(url, cursor)
        end_cursor, has_next_page = get_data(response, mongo)
        logging.info('Cursor: {}'.format(end_cursor))
        cursor = end_cursor
        if not end_cursor:
            break
        num += 1
        logging.info('Page {} finished .'.format(num))
        time.sleep(5)


if __name__ == '__main__':
    main()
