# !/usr/bin/python
# -*-coding:utf-8-*-
# Author：Mongoole
# Date：2021/12/21

import io
import os
import re
import csv
import sys
import time
import json
import cchardet
import requests
from lxml import etree
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')  # change the default encoding of standard output

# crawl_time = time.strftime('%Y-%m-%d', time.localtime(time.time()))
crawl_time = '2021-12-24'

def driver_():
    urls = [
       'https://www.youtube.com/c/bbcnews/videos',
       'https://www.youtube.com/user/CNN/videos',
       'https://www.youtube.com/c/cgtn/videos',
       'https://www.youtube.com/c/ChinaPlusOfficial/videos',]

    for url in urls:
        driver = webdriver.Chrome()
        driver.get(url)
        driver.maximize_window()
        time.sleep(5)

        while True:

            video_all_information = driver.find_elements_by_xpath('//*[@id="video-title"]')
            all_information = "".join([item.get_attribute('aria-label') for item in video_all_information])

            for item in video_all_information:
                information = item.get_attribute('aria-label')
                url = item.get_attribute('href')
                # write_file1(url + "##########" + information)
                write_file2(url + "##########" + information)
                # print(url + "##########" + information)

            if '8天前' in all_information:
                driver.quit()
                break
            elif '9天前' in all_information:
                driver.quit()
                break
            else:

                driver.execute_script("window.scrollBy(0,2000)","")
                time.sleep(5)


def write_file1(content):
    with open('video_information.txt','a') as f:
        f.write(str(content) + "\n")


def write_file2(content):

    with open('video_url-{}.txt'.format(crawl_time),'a') as f:
        f.write(content + "\n")


def read_account():
    with open('youtube_account_video.txt') as f:
        content = f.readlines()
    return content


def read_file():
    # crawl_time = time.strftime('%Y-%m-%d',time.localtime(time.time()))
    with open('video_url-{}.txt'.format(crawl_time),'r') as f:
        content = f.readlines()

    results = list(set(content))
    return results


def get_video_information():
    results = read_file()
    print('需采集的url的个数：{}'.format(len(results)))
    print('开始时间：{}'.format(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))))
    with open('youtube-video-{}.csv'.format(crawl_time),'a',newline='',encoding='gbk') as f:
        f_csv = csv.writer(f)
        f_csv.writerow(('account','title','long_time','publish_time',
                        'view_count','like_count','comment_count','url'))
        driver = webdriver.Chrome()

        for result in results:
            result = result.split('##########')
            url = result[0]
            long_time = result[1].strip().split(' ')[-2]
            information = result[1].strip().split(' ')[-3]
            # days = ['3天前','4天前','5天前','6天前','7天前','8天前','9天前']
            days = [str(x) + '天前' for x in range(3,10)]
            if information in days:
                driver.get(url)
                driver.maximize_window()
                time.sleep(1)
                driver.execute_script("window.scrollBy(0,1000)")
                time.sleep(7)

                account = driver.find_element_by_xpath(
                    '//*[@id="text"]/a').text

                title = driver.find_element_by_xpath(
                    '//*[@id="container"]/h1/yt-formatted-string').text

                # long_time = driver.find_element_by_xpath(
                #     '//*[@class="ytp-time-duration"]').text

                publish_time = driver.find_element_by_xpath(
                    '//*[@id="info-strings"]/yt-formatted-string').text

                view_count = driver.find_element_by_xpath(
                    '//*[@id="count"]/ytd-video-view-count-renderer/span').text
                # view_short_count = driver.find_element_by_xpath(
                #     '//*[@id="count"]/ytd-video-view-count-renderer/span[2]').text

                emo_count = driver.find_element_by_xpath(
                    '//*[@class="style-scope ytd-toggle-button-renderer style-text"][@id="text"]').text

                like_count = emo_count

                try:
                    if 'class="count-text style-scope ytd-comments-header-renderer"' in driver.page_source:
                        comment_count = driver.find_element_by_xpath(
                            '//*[@id="count"]/yt-formatted-string/span').text
                    else:
                        comment_count = 0
                except:
                    comment_count = 0

                print(title,publish_time,view_count,like_count,comment_count,url)
                data_lst = [account,title,long_time,publish_time,view_count,like_count,comment_count,url]
                f_csv.writerow(data_lst)
                time.sleep(1)


def main():
    driver_()
    get_video_information()
    print('结束时间：{}'.format(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))))




if __name__ == '__main__':
    main()
