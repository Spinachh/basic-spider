# !/usr/bin/python
# -*-coding:utf-8-*-
# Author：Mongoole
# Date：2021/12/21

import io
import os
import re
import csv
import sys
import time
import random
import datetime
import json
import cchardet
import requests
import logging
from lxml import etree
from selenium import webdriver
from multiprocessing import Pool
from selenium.webdriver.common.keys import Keys

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')  # change the default encoding of standard output

crawl_time = time.strftime('%Y-%m-%d', time.localtime(time.time()))

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - : %(message)s')


# crawl_time = '2021-12-302'


def driver_(url):
    driver = webdriver.Chrome()
    driver.get(url)
    driver.maximize_window()
    time.sleep(5)

    while True:

        video_all_information = driver.find_elements_by_xpath('//*[@id="video-title"]')
        all_information = "".join([item.get_attribute('aria-label') for item in video_all_information])

        if '2个月前' in all_information:
            for item in video_all_information:
                information = item.get_attribute('aria-label')
                url = item.get_attribute('href')
                write_file2(url + "##########" + information)

            driver.quit()
            break
        else:
            driver.execute_script("window.scrollBy(0,5000)", "")
            time.sleep(5)


def write_file1(content):
    with open('video_information.txt', 'a') as f:
        f.write(str(content) + "\n")


def write_file2(content):
    with open(r'video-txt/video_url-{}.txt'.format(crawl_time), 'a', encoding="utf-8") as f:
        f.write(content + "\n")


def read_account():
    with open('youtube_account_video.txt') as f:
        content = f.readlines()
    return content


def read_file():
    # crawl_time = time.strftime('%Y-%m-%d',time.localtime(time.time()))
    with open('video-txt/video_url-{}.txt'.format(crawl_time), 'r', encoding="utf-8") as f:
        content = f.readlines()

    # results = list(set(content))
    results = content
    return results


def get_video_information(item, writer_csv):

    since_time = '2022年1月1日'
    until_time = '2022年2月1日'

    result = item.split('##########')
    url = result[0]
    long_time = result[1].strip().split(' ')[-2]
    # Petrol tanker crash causes massive fire in Long Island, New York - BBC News 来自BBC News 1小时前 1分钟17秒钟 7,791次观看
    # information = 1小时前
    # information = result[1].strip().split(' ')[-3]

    # if '直播时间' in information:
    #     information = information.split(' ')[-3].split('：')[-1]
    # else:
    #     information = information
    # print(long_time)
    # print(information)
    # breakpoint()
    # days = ['3天前','4天前','5天前','6天前','7天前','8天前','9天前']
    # days1 = [str(x) + '天前' for x in range(13, 14)]
    # days2 = [str(x) + '周前' for x in range(2, 5)]
    # days3 = [str(x) + '个月前' for x in range(1, 3)]
    # days = days2 + days3
    # if information in days:
    driver = webdriver.Chrome()
    driver.get(url)

    try:
        publish_time = driver.find_element_by_xpath(
            '//*[@id="info-strings"]/yt-formatted-string').text
        if '直播开始日期' in publish_time:
            publish_time = publish_time.split('：')[-1]
    except:
        publish_time = '0'

    if since_time <= publish_time <= until_time:

        driver.maximize_window()
        time.sleep(0.2)
        driver.execute_script("window.scrollBy(0,1000)")
        time.sleep(random.uniform(0.5, 2.5))


        try:
            account = driver.find_element_by_xpath(
                '//*[@id="text"]/a').text
        except:
            account = ''

        try:
            title = driver.find_element_by_xpath(
                '//*[@id="container"]/h1/yt-formatted-string').text
        except:
            title = ''

        # long_time = driver.find_element_by_xpath(
        #     '//*[@class="ytp-time-duration"]').text

        try:
            view_count = driver.find_element_by_xpath(
                '//*[@id="count"]/ytd-video-view-count-renderer/span').text
        # view_short_count = driver.find_element_by_xpath(
        #     '//*[@id="count"]/ytd-video-view-count-renderer/span[2]').text
        except:
            view_count = 0

        try:
            emo_count = driver.find_element_by_xpath(
                '//*[@class="style-scope ytd-toggle-button-renderer style-text"][@id="text"]').text
            like_count = emo_count
        except:
            like_count = ''

        try:
            if 'class="count-text style-scope ytd-comments-header-renderer"' in driver.page_source:
                comment_count = driver.find_element_by_xpath(
                    '//*[@id="count"]/yt-formatted-string/span').text
            else:
                comment_count = 0
        except:
            comment_count = 0

        logging.info('Publish: {} View: {} Comment: {} Like: {} Account: {} URL: {}'
                     .format(publish_time, view_count, comment_count, like_count, account, url))
        if title:
            item = [account, title, publish_time, long_time, view_count, like_count, comment_count, url]
            writer_csv.writerow(item)
            driver.quit()
        else:
            print('URL: {} was not finished!'.format(url))
            with open(r'video-error/video_error-{}.txt'.format(crawl_time), 'a', encoding='utf-8') as f:
                f.write(item)
    else:
        driver.quit()


def video_process():
    file_name = time.strftime('%Y-%m-%d', time.localtime(time.time()))
    data = read_file()
    item_lst = []
    days2 = [str(x) + '周前' for x in range(2, 5)]
    days3 = [str(x) + '个月前' for x in range(1, 3)]

    days = days2 + days3
    for item in data:
        item_time = item.split(' ')[-3]
        if item_time in days:
            item_lst.append(item)
    # with open(r'video-unique-txt/unique-{}.txt'.format(crawl_time), 'w', encoding="utf-8") as f:
    #     f.writelines(datas)
    # with open(r'video-unique-txt/unique-{}.txt'.format(crawl_time), encoding='gb18030') as f:
    #     results = f.readlines()
    logging.info('需采集的url的个数：{}'.format(len(item_lst)))
    results = item_lst

    with open(r'video-csv/video-information-{}-.csv'.format(file_name), 'a+', newline='', encoding='gb18030') as f:
        fieldnames = ['account', 'title', 'publish_time', 'long_time',
                      'view_count', 'like_count', 'comment_count', 'url']

        writer_csv = csv.writer(f)
        writer_csv.writerow(fieldnames)
        '''
        pool = Pool(processes=3)
        for item in results:
            pool.apply_async(get_video_information, args=(item,))
        pool.close()
        pool.join()

        '''
        for item in results:
            get_video_information(item, writer_csv)


def get_all_video():
    urls = [
        'https://www.youtube.com/c/bbcnews/videos',
        'https://www.youtube.com/user/CNN/videos',
        'https://www.youtube.com/c/cgtn/videos',
        'https://www.youtube.com/c/ChinaPlusOfficial/videos',
        'https://www.youtube.com/channel/UCq2Cg1w0sfmo_HcxbUMsXqA/videos',
    ]

    # urls = ['https://www.youtube.com/c/bbcnews/videos', ]
    pool = Pool(processes=2)
    for i in range(0, len(urls)):
        pool.apply_async(driver_, args={urls[i]})

    pool.close()
    pool.join()


def main():
    start_time = datetime.datetime.now()
    logging.info('开始时间')

    # get_all_video()
    video_process()

    end_time = datetime.datetime.now()
    logging.info('结束时间')
    logging.info('总共用时：{} 秒'.format((end_time - start_time).seconds))


if __name__ == '__main__':
    main()
