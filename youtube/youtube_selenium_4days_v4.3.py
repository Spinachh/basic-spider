# !/usr/bin/python
# -*-coding:utf-8-*-
# Author：mongoole
# Date：2022/11/04

import re
import csv
import time
import datetime
import logging

import selenium
from selenium import webdriver
from multiprocessing import Pool
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from time2time import sec_to_data

# sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')  # change the default encoding of standard output

crawl_time = time.strftime('%Y-%m-%d', time.localtime(time.time()))
logging.basicConfig(level=logging.INFO, format='%(asctime)s-:%(message)s')


class YouTuBe(object):

    @staticmethod
    def write_file1(content):
        with open('video_information.txt', 'a') as f:
            f.write(str(content) + "\n")

    @staticmethod
    def write_file2(content):
        with open(r'video-txt/video_url-{}.txt'.format(crawl_time), 'a', encoding="utf-8") as f:
            f.write(content + "\n")

    @staticmethod
    def read_account():
        with open('youtube_account_video.txt') as f:
            content = f.readlines()
        return content

    @staticmethod
    def read_file():
        with open('video-txt/video_url-{}.txt'.format(crawl_time), 'r', encoding='utf-8') as f:
            content = f.readlines()
        days = [str(x) + '天前' for x in range(3, 8)]  # 2月28日采集range（3,9）（24：开始:——19：结束）

        with open(r'video-unique-txt/video-{}.txt'.format(crawl_time), 'a', encoding='utf-8') as f:
            for item in content:
                if '/shorts/' in item:
                    time_time = item.split('来自')[-1].split(' ')[-7]
                else:
                    time_time = item.strip().split(' ')[-3]

                if '直播时间' in time_time:
                    day_time = time_time.split('：')[-1]
                    if day_time in days:
                        f.write(item)
                else:
                    if time_time in days:
                        f.write(item)

        with open(r'video-unique-txt/video-{}.txt'.format(crawl_time), 'r', encoding='utf-8') as f:
            content = f.readlines()
        results = content
        return results

    @staticmethod
    def read_file_uniq():
        with open(r'video-unique-txt/video-{}.txt'.format(crawl_time), 'r', encoding='utf-8') as f:
            content = f.readlines()
        results = content
        return results

    @staticmethod
    def read_file_error():
        with open(r'video-error/video_error-{}.txt'.format(crawl_time), 'r', encoding='utf-8') as f:
            content = f.readlines()
        results = content
        return results

    @staticmethod
    def driver():
        driver = webdriver.Chrome()
        return driver

    def driver_get_all_video(self, url):
        driver = self.driver()
        driver.get(url)
        driver.maximize_window()
        time.sleep(5)
        driver.refresh()
        time.sleep(5)
        while True:
            video_all_information = driver.find_elements_by_xpath('//*[@id="video-title-link"]')[:-1]

            all_information = "".join([item.get_attribute('aria-label') for item in video_all_information])
            # [print(item.get_attribute('aria-label')) for item in video_all_information]
            # breakpoint()
            if '8天前' in all_information:
                for item in video_all_information:
                    information = item.get_attribute('aria-label')
                    video_url = item.get_attribute('href')
                    self.write_file2(video_url + "##########" + information)
                driver.quit()
                break
            elif '9天前' in all_information:
                for item in video_all_information:
                    information = item.get_attribute('aria-label')
                    video_url = item.get_attribute('href')
                    self.write_file2(video_url + "##########" + information)
                driver.quit()
                break
            elif '10天前' in all_information:
                for item in video_all_information:
                    information = item.get_attribute('aria-label')
                    video_url = item.get_attribute('href')
                    self.write_file2(video_url + "##########" + information)
                driver.quit()
                break
            else:
                driver.execute_script("window.scrollBy(0,5000)", "")
                time.sleep(5)

    def get_video_information(self, item, writer_csv):
        result = item.split('##########')
        url = result[0]
        if 'shorts' not in url:
            driver = self.driver()
            driver.get(url)

            driver.maximize_window()
            time.sleep(2)
            page_source = driver.page_source
            driver.execute_script("window.scrollBy(0,1000)")
            time.sleep(4)

            try:
                # account = result[-1].split('来自')[0].split('-')[-1].strip()
                # account = item.split('来自')[-1].split(' ')[0]
                account = re.findall(r'"author":"(.*?)",', page_source, re.S | re.M)[0]
            except:
                account = driver.find_element_by_xpath(
                    '//*[@id="text"]/a').text

            try:
                title = result[-1].split('来自')[0]
            except:
                title = driver.find_element_by_xpath(
                    '//*[@id="container"]/h1/yt-formatted-string').text

            '''
            try:
                publish_time = driver.find_element_by_xpath(
                    '//*[@id="info-strings"]/yt-formatted-string').text
                if '直播开始日期' in publish_time:
                    publish_time = publish_time.split('：')[-1]
            except:
                publish_time = '0'
            '''

            try:
                publish_time = re.findall(r'dateText\":{\"simpleText\":"(.*?)"}', page_source, re.S | re.M)[0]
                if '直播开始日期' in publish_time:
                    publish_time = publish_time.split('：')[-1]
            except:
                publish_time = '0'

            try:
                # long_time = driver.find_element_by_xpath('//*[@class="ytp-time-duration"]').text
                long_time = re.findall(r'"lengthSeconds":"(.*?)",', page_source, re.M | re.S)[0]
                long_time = sec_to_data(int(long_time))
            except:
                # long_time = driver.find_element_by_xpath('//*[@class="ytp-time-duration"]').text
                # long_time = driver.find_element(by=By.XPATH, value='//*[@class="ytp-time-duration"]').text
                # long_time = sec_to_data(int(long_time))
                long_time = result[1].strip().split(' ')[-2]

            try:
                keyword = re.findall(r'"keywords":(.*?)\,"channelId"', page_source, re.M | re.S)[0]
            except:
                keyword = ""

            try:
                view_count = result[1].split(' ')[-1]
            except:
                view_count = re.findall(r'"viewCount":{"videoViewCountRenderer":{"viewCount":{"simpleText":(.*?)},',
                                        page_source, re.M | re.S)
                # view_count = driver.find_element_by_xpath(
                #     '//*[@id="count"]/ytd-video-view-count-renderer/span').text

            try:
                like_count = \
                    re.findall(r'"defaultText":{"accessibility":{"accessibilityData":{"label":(.*?)}},"simpleText"',
                               page_source, re.M | re.S)[0]

            except:
                emo_count = driver.find_element_by_xpath(
                    '//*[@class="style-scope ytd-toggle-button-renderer style-text"][@id="text"]').text
                like_count = emo_count

            try:
                comment_count = re.findall(r'"commentCount":{"simpleText":(.*?)},"', page_source, re.M | re.S)[0]

            except:
                if 'class="count-text style-scope ytd-comments-header-renderer"' in driver.page_source:
                    comment_count = driver.find_element_by_xpath(
                        '//*[@id="count"]/yt-formatted-string/span').text
                else:
                    comment_count = '0'

            logging.info('Publish:{} View:{} Comment:{} Like:{} URL:{}'
                         .format(publish_time, view_count, comment_count, like_count, url))
            if comment_count:
                item = [account, title, keyword, publish_time, long_time, view_count,
                        like_count, comment_count, url, crawl_time]
                writer_csv.writerow(item)
                driver.quit()
            else:
                logging.info('Url: {} was not finished!'.format(url))
                with open(r'video-error/video_error-{}.txt'.format(crawl_time), 'a', encoding='utf-8') as f:
                    f.write(item)
        else:
            driver = self.driver()
            driver.get(url)
            driver.maximize_window()
            time.sleep(2)
            page_source = driver.page_source

            try:
                # account = item.split('来自')[-1].split(' ')[0]
                account = re.findall(r'"author":"(.*?)",', page_source, re.S | re.M)[0]
            except:
                account = ''

            try:
                title = result[-1].split('来自')[0]
            except:
                title = driver.find_element_by_xpath(
                    '//*[@id="container"]/h1/yt-formatted-string').text

            try:
                publish_time = re.findall(r'{"publishTimeText":{"runs":\[{"text":(.*?)},', page_source, re.S | re.M)[0]
                if '直播开始日期' in publish_time:
                    publish_time = publish_time.split('：')[-1]
            except:
                publish_time = '0'

            try:
                # long_time = driver.find_element_by_xpath('//*[@class="ytp-time-duration"]').text
                long_time = re.findall(r'"lengthSeconds":"(.*?)","keywords"', page_source, re.M | re.S)[0]
                long_time = sec_to_data(int(long_time))
            except:
                # long_time = driver.find_element_by_xpath('//*[@class="ytp-time-duration"]').text
                # long_time = driver.find_element(by=By.XPATH, value='//*[@class="ytp-time-duration"]').text
                # long_time = sec_to_data(int(long_time))
                long_time = item.split('来自')[-1].split(' ')[-6]

            try:
                keyword = re.findall(r'"keywords":(.*?)\,"channelId"',
                                     page_source, re.M | re.S)[0]
            except:
                keyword = ""

            try:
                view_count = item.split('来自')[-1].split(' ')[-5]
            # view_short_count = driver.find_element_by_xpath(
            #     '//*[@id="count"]/ytd-video-view-count-renderer/span[2]').text
            except:
                # view_count = driver.find_element_by_xpath(
                #     '//*[@id="count"]/ytd-video-view-count-renderer/span').text
                view_count = re.findall(r'viewCountText":{"runs":\[{"text":(.*?)\},',
                                        page_source, re.M | re.S)[0].replace('"', '')

            try:
                like_count = re.findall(r'"likeCount":(.*?),', page_source, re.M | re.S)[0]
            except:
                emo_count = driver.find_element_by_xpath(
                    '//*[@class="style-scope ytd-toggle-button-renderer style-text"][@id="text"]').text
                like_count = emo_count

            try:
                comment_count = re.findall(r'"accessibility":{"label":(.*?)},"',
                                           page_source, re.M | re.S)[0]
                if '查看评论' in comment_count:
                    comment_count = '0'
            except:
                if 'class="count-text style-scope ytd-comments-header-renderer"' in driver.page_source:
                    comment_count = driver.find_element_by_xpath(
                        '//*[@id="count"]/yt-formatted-string/span').text
                else:
                    comment_count = '0'

            logging.info('Publish:{} View:{} Comment:{} Like:{} URL:{}'
                         .format(publish_time, view_count, comment_count, like_count, url))
            if comment_count:
                item = [account, title, keyword, publish_time, long_time, view_count,
                        like_count, comment_count, url, crawl_time]
                writer_csv.writerow(item)
                driver.quit()
            else:
                logging.info('URL: {} was not finished!'.format(url))
                with open(r'video-error/video_error-{}.txt'.format(crawl_time), 'a', encoding='utf-8') as f:
                    f.write(item)

    def video_process(self):
        self.read_file()
        file_name = time.strftime('%Y-%m-%d', time.localtime(time.time()))
        results = self.read_file_uniq()
        logging.info('Crawl Video Url\'s Count：{}'.format(len(results)))
        with open(r'video-csv/youtube-video-{}.csv'.format(file_name), 'a+', newline='',
                  encoding='gb18030') as f:
            fieldnames = ['account', 'title', 'keyword', 'publish_time', 'long_time',
                          'view_count', 'like_count', 'comment_count', 'url', 'crawl_time']

            writer_csv = csv.writer(f)
            writer_csv.writerow(fieldnames)

            for item in results:
                self.get_video_information(item, writer_csv)

    def video_error_crawl(self):
        file_name = time.strftime('%Y-%m-%d', time.localtime(time.time()))
        data = self.read_file_error()

        logging.info('需采集的error url的个数：{}'.format(len(data)))
        results = data

        with open(r'video-csv/youtube-video-{}.csv'.format(file_name), 'a+', newline='',
                  encoding='gb18030') as f:
            fieldnames = ['account', 'title', 'keyword', 'publish_time', 'long_time',
                          'view_count', 'like_count', 'comment_count', 'url']

            writer_csv = csv.writer(f)
            writer_csv.writerow(fieldnames)

            for item in results:
                self.get_video_information(item, writer_csv)

    def get_all_video(self):
        account_urls = [
            # 'https://www.youtube.com/c/bbcnews/videos',
            'https://www.youtube.com/user/CNN/videos',
            # 'https://www.youtube.com/c/cgtn/videos',
            # 'https://www.youtube.com/c/ChinaPlusOfficial/videos',
            # 'https://www.youtube.com/channel/UCq2Cg1w0sfmo_HcxbUMsXqA/videos',
        ]

        pool = Pool(processes=1)
        for i in range(0, len(account_urls)):
            pool.apply_async(self.driver_get_all_video, args={account_urls[i]})

        pool.close()
        pool.join()

    def start_run(self):
        start_time = datetime.datetime.now()
        logging.info('Start Time: {}'.format(start_time))
        self.get_all_video()  # get video page list
        '''
        self.video_process()  # get video detail information
        try:
            self.video_error_crawl()  # crawl error video url
        except Exception as e:
            logging.info('Has not Error url : {}'.format(e))
        end_time = datetime.datetime.now()
        logging.info('End Time : {}'.format(end_time))
        logging.info('Total Times：{} Seconds'.format((end_time - start_time).seconds))
        '''

if __name__ == '__main__':
    YouTuBeSpider = YouTuBe()
    YouTuBeSpider.start_run()
