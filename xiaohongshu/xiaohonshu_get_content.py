import requests
import random
import re,time
from bs4 import BeautifulSoup
from lxml import etree
from setting import User_Agent

def get_content(nickname,url):
    user_agent = random.choice(User_Agent)
    headers= {'User-Agent':user_agent}

    # 隧道服务器
    proxyServer = "tps136.kdlapi.com"
    proxyPort = "15818"
    # 隧道id和密码
    proxyUser = "t17535136923494"
    proxyPass = "lwUvJ8dK6All"

    proxies = {
        "http": "http://%s:%s@%s:%s/" % (proxyUser, proxyPass, proxyServer, proxyPort),
        "https": "https://%s:%s@%s:%s/" % (proxyUser, proxyPass, proxyServer, proxyPort)
    }

    # html = requests.get(url=url,proxies=proxies,headers=headers).text
    html = requests.get(url=url,headers=headers).text
    # print(html)
    bs = BeautifulSoup(html,"html.parser")
    title = bs.title.string
    content = bs.find_all('p')
    # for item in content:
    #     item = item.string
    #     # print(item)

    with open('gzliuyun\\%s.txt'%nickname,'a',encoding='utf-8') as file:
        file.write(title + '\n')
        for item in content:
            file.write(str(item.string) + '\n')

    '''
    #正则表达式获取文本内容如下
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html).strip()
    # print(content_text)
    # content = re.findall(r',"title":(.*?)\)","id"',html)
    # content = re.findall(r',"comments":(.*?)\)","id"',html)
    # print(content)
    '''
    '''
    #xpath
    # selector = etree.HTML(html)
    # content = selector.xpath('/html/body/div/div[2]/div[1]/div[3]/div/p/text()')
    # print(content)
    '''

def main():
    # url = 'https://www.xiaohongshu.com/discovery/item/5c98d37d000000000f0189c7'
    # chars = ['/', '\\', ':', '*', '?', '"', '|', '<', '>']
    with open('liuyun','r',encoding='utf-8') as f:
        # all_user_url = f.readlines()
        i = 0
        for full_url in f:
            nickname = full_url.split('####')[0].strip()
            nickname = re.sub(r'[/\\:*?"|<>]','',nickname)
            url = full_url.split('####')[1]
            get_content(nickname,url)
            print('==========第 %s 作者：%s 已经写完了=========='%(i,nickname))
            time.sleep(1)
            i += 1
            # break

if __name__ == '__main__':
    main()