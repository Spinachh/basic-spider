# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-10-26

import re
import io
import sys
import time
import json
import cchardet
import urllib3
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_html(url):
    response = requests.get(url, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="news-list"]/ul/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    # print(part_nodes)
    # breakpoint()
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('.//a/@title')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = news_title
        except:
            news_abstract = ''

        try:
            if '团体阅读' not in news_classify:
                news_publish_time = part_nodes[i].xpath('./span[4]/span/text()')[0]
            else:
                news_publish_time = part_nodes[i].xpath('./span[2]/span/text()')[0]

        except:
            news_publish_time = part_nodes[i].xpath('./span[3]/span/text()')[0]

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('.//a/@href')[0]
            if 'mp.weixin.qq.com' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.ces.org.cn' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    # html_response = requests.get(news_page_url, verify=False)
    html_response = requests.get(news_page_url)
    # if 'www.cstp.org.cn' not in news_page_url:
    #     html_response.encoding = 'gb2312'
    # else:
    html_response.encoding = 'utf-8'

    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    try:
        news_content = selector_page.xpath('//*[@class="newscont"]//p/text()')
        # news_content = selector_page.xpath('//*[@class="cont_txt"]/span/text()')
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="newscont"]//p//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：<span>(.*?)</span>', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学会动态': 'http://www.ces.org.cn/html/category/17060165-1.htm',
        '首页-通知公告': 'http://www.ces.org.cn/html/category/17060182-1.htm',
        '首页-编辑出版': 'http://www.ces.org.cn/html/folder/18031570-1.htm',
        '首页-党建强会': 'http://www.ces.org.cn/html/category/17060137-1.htm',
        '首页-会员天地-会员服务': 'http://www.ces.org.cn/html/category/17060048-1.htm',
        '首页-会员天地-通知公告': 'http://www.ces.org.cn/html/category/17060052-1.htm',
        '首页-组织建设-通知公告': 'http://www.ces.org.cn/html/category/18031480-1.htm',
        '首页-组织建设-工作动态': 'http://www.ces.org.cn/html/category/18121206-1.htm',
        '首页-学术交流-学术动态': 'http://www.ces.org.cn/html/category/17060075-1.htm',
        '首页-国际合作-通知公告': 'http://www.ces.org.cn/html/category/17060083-1.htm',
        '首页-国际合作-开展项目': 'http://www.ces.org.cn/html/category/17070562-1.htm',
        '首页-科技展览-通知公告': 'http://www.ces.org.cn/html/category/17060084-1.htm',
        '首页-青年人才托举-青年人才风采展示': 'http://www.ces.org.cn/html/category/22033043-1.htm',
        '首页-工程能力评价-通知公告': 'http://www.ces.org.cn/html/category/17060105-1.htm',
        '首页-科技咨询-通知公告': 'http://www.ces.org.cn/html/category/17060094-1.htm',
        '首页-科技咨询-创新驱动助力': 'http://www.ces.org.cn/html/category/17060096-1.htm',
        '首页-科技咨询-成果鉴定': 'http://www.ces.org.cn/html/category/17070565-1.htm',
        '首页-继续教育-通知公告': 'http://www.ces.org.cn/html/category/17060101-1.htm',
        '首页-继续教育-继续教育活动': 'http://www.ces.org.cn/html/category/19061714-1.htm',
        '首页-科学普及-通知公告': 'http://www.ces.org.cn/html/category/21030447-1.htm',
        '首页-科学普及-科普活动': 'http://www.ces.org.cn/html/category/17060111-1.htm',
        '首页-科学普及-科普知识': 'http://www.ces.org.cn/html/category/17060112-1.htm',
        '首页-学会奖励-通知公告': 'http://www.ces.org.cn/html/category/17060122-1.htm',
        '首页-学会奖励-奖励工作动态': 'http://www.ces.org.cn/html/category/21042563-1.htm',
        '首页-团体标准-通知公告': 'http://www.ces.org.cn/html/category/17060132-1.htm',
        '首页-团体标准-工作动态': 'http://www.ces.org.cn/html/category/17060133-1.htm',
        '首页-团体标准-文件下载': 'http://www.ces.org.cn/html/category/17060134-1.htm',
        '首页-团体标准-团体阅读': 'http://www.ces.org.cn/html/category/21040052-1.htm',
        '首页-资产财务-通知公告': 'http://www.ces.org.cn/html/category/21042690-1.htm',
        '首页-工程教育认证-通知公告': 'http://www.ces.org.cn/html/category/17070566-1.htm',
        '首页-工程教育认证-工作动态': 'http://www.ces.org.cn/html/category/17070567-1.htm',
        '首页-学术交流-活动预报': 'https://www.ces.org.cn/html/category/17060074-1.htm',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            url = value.replace('-1.htm', '-' + str(page) + '.htm')
            html_text, status_code = get_html(url)
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                logging.warning('CLASSIFY：{} PAGE: {} CONTENT DEADLINE'.format(news_classify, page))
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'B-06 中国电工技术学会'
    start_run(args.projectname, args.sinceyear, account_name)
