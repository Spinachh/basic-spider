# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-11-02


import re
import io
import sys
import time
import json
import random
import argparse
import urllib3
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content5
    collection = db['{}'.format(c_name)]
    return collection


def get_html(url, page, id):

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Origin': 'https://www.caai.cn',
        'Referer': url,
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-User': '?1',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/117.0.0.0 Safari/537.36',
        'sec-ch-ua': '"Google Chrome";v="117", "Not;A=Brand";v="8", "Chromium";v="117"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    params = (
        ('s', '/home/article/index.html'),
    )

    data = {
        'p': page,
        'tagid': '0',
        'id': str(id)
    }

    response = requests.post('https://www.caai.cn/index.php', headers=headers,
                             params=params,data=data,
                             )
    # print(response.status_code)
    # print(response.text)
    # breakpoint()
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data( html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    # re_data = re.findall(r'<h1>{}</h1><ul>(.*?)</ul>'.format(key_word), html_text, re.M | re.S)[0]
    selector = etree.HTML(html_text)
    try:
        # part1_nodes = selector.xpath('//*[@class="article-list contentDisplay"]/ul/li')
        # part1_nodes = selector.xpath('//*[@class="mainList"]/ul/li')
        part1_nodes = selector.xpath('//*[@class="clear"]')[1:]
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    # print(part_nodes)
    # breakpoint()
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('.//a/@title')[0].strip()
        except:
            news_title = ''

        news_abstract = news_title

        try:
            news_publish_time = part_nodes[i].xpath('./h4/text()')[0].strip()
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('.//a/@href')[0]
            if 'https' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://caai.cn' + url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="articleContent"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="articleContent"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'<span class="news_top_lyname">(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'<span class="news_top_zzname">(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击量：<span id="clicks">(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻动态-科协头条': 'https://www.caai.cn/index.php?s=/home/article/index/id/148.html',
        '首页-新闻动态-学会新闻': 'https://caai.cn/index.php?s=/home/article/index/id/46.html',
        '首页-新闻动态-通知公告': 'https://caai.cn/index.php?s=/home/article/index/id/47.html',
        '首页-新闻动态-活动预告': 'https://caai.cn/index.php?s=/home/article/index/id/49.html',
        '首页-新闻动态-对外合作': 'https://caai.cn/index.php?s=/home/article/index/id/71.html',
        '首页-关于CAAI-条例与法规': 'https://caai.cn/index.php?s=/home/article/index/id/40.html',
        '首页-关于CAAI-分支机构': 'https://caai.cn/index.php?s=/home/article/index/id/43.html',
        '首页-关于CAAI-学会党建': 'https://caai.cn/index.php?s=/home/article/index/id/90.html',
        '首页-关于CAAI-文档下载': 'https://caai.cn/index.php?s=/home/article/index/id/77.html',
        '首页-党建强会-党建强会': 'https://caai.cn/index.php?s=/home/article/index/id/79.html',
        '首页-党建强会-党史百科': 'https://caai.cn/index.php?s=/home/article/index/id/115.html',

        '首页-党建强会-专题教育': 'https://caai.cn/index.php?s=/home/article/index/id/154.html',  # page4


        '首页-奖励与合作-吴文俊奖': 'https://caai.cn/index.php?s=/home/article/index/id/64.html',
        '首页-奖励与合作-青托计划': 'https://caai.cn/index.php?s=/home/article/index/id/86.html',
        '首页-奖励与合作-院士推荐': 'https://caai.cn/index.php?s=/home/article/index/id/67.html',
        '首页-奖励与合作-成果鉴定': 'https://caai.cn/index.php?s=/home/article/index/id/91.html',
        '首页-奖励与合作-基金项目': 'https://caai.cn/index.php?s=/home/article/index/id/143.html',
        '首页-奖励与合作-激励计划': 'https://caai.cn/index.php?s=/home/article/index/id/160.html',

        '首页-CAAI资源-学会期刊': 'https://caai.cn/index.php?s=/home/article/index/id/50.html',
        '首页-CAAI资源-学科皮书系列': 'https://caai.cn/index.php?s=/home/article/index/id/53.html',
        '首页-CAAI资源-年鉴及发展报告': 'https://caai.cn/index.php?s=/home/article/index/id/54.html',
        '首页-会员专区-学会会士': 'https://caai.cn/index.php?s=/home/article/index/id/69.html',
        '首页-会员专区-高级会员': 'https://caai.cn/index.php?s=/home/article/index/id/135.html',
        '首页-会员专区-会员荣誉': 'https://caai.cn/index.php?s=/home/article/index/id/83.html',
        '首页-会员专区-常务理事单位会员': 'https://caai.cn/index.php?s=/home/article/index/id/144.html',
        '首页-会员专区-普通单位会员': 'https://caai.cn/index.php?s=/home/article/index/id/74.html',
    }

    for key, value in urls.items():
        news_classify = key
        key_word = key.split('-')[-1]
        id = re.findall(r'id/(.*?).html', value)[0]
        for page in range(4, 50):
            html_text, status_code = get_html(value, page, id)
            # print(html_text)
            # breakpoint()
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break

            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'B-66 中国人工智能学会'
    start_run(args.projectname, args.sinceyear, account_name)
