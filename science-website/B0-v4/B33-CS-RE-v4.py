# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-10-31

import random
import re
import io
import sys
import time
import argparse
import urllib3
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_html(url):

    response = requests.get(url,  verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="ls_right fr mt_20"]/ul/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0].replace('/', '-')
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'https:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.cs-re.org.cn/' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url,verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="news-con"]//p/span/text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="news-con"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-学会动态-学会新闻': 'http://www.cs-re.org.cn/news/?page=1',
        '首页-学会动态-通知公告': 'http://www.cs-re.org.cn/news/notice/?page=1',
        '首页-学会动态-分支机构动态': 'http://www.cs-re.org.cn/news/branch/?page=1',
        '首页-学会动态-学会七大': 'http://www.cs-re.org.cn/c98/?page=1',
        '首页-学术交流-会议预告': 'http://www.cs-re.org.cn/c50/?page=1',
        '首页-学术交流-会议报道': 'http://www.cs-re.org.cn/c51/?page=1',
        '首页-会员之窗-会员工作站': 'http://www.cs-re.org.cn/c25/?page=1',
        '首页-会员之窗-会员活动': 'http://www.cs-re.org.cn/c26/?page=1',
        '首页-学术奖励-奖励公告': 'http://www.cs-re.org.cn/award/notice/?page=1',
        '首页-科普教育-科普园地': 'http://www.cs-re.org.cn/popular/?page=1',
        '首页-科普教育-继续教育': 'http://www.cs-re.org.cn/training/?page=1',
        '首页-科普教育-图书出版': 'http://www.cs-re.org.cn/book/?page=1',
        '首页-科普教育-学会期刊': 'http://www.cs-re.org.cn/journal/?page=1',
        '首页-科普教育-专题报告': 'http://www.cs-re.org.cn/report/?page=1',
        '首页-科普教育-科学家精神': 'http://www.cs-re.org.cn/report/?page=1',
        '首页-党的建设-党建动态': 'http://www.cs-re.org.cn/news/cpc/?page=1',
        '首页-党的建设-要闻播报': 'http://www.cs-re.org.cn/cpc/important.html',
        '首页-党的建设-两学一做': 'http://www.cs-re.org.cn/cpc/lxyz/?page=1',
        '首页-党的建设-反腐倡廉': 'http://www.cs-re.org.cn/cpc/anticorruption/?page=1',
        '首页-人才举荐-人才名录': 'http://www.cs-re.org.cn/talent/list/?page=1',
        '首页-人才举荐-院士推荐': 'http://www.cs-re.org.cn/talent/fellowship/?page=1',
        '首页-网上书店-出版图书': 'http://www.cs-re.org.cn/c83?page=1',
        '首页-网上书店-稀土年鉴': 'http://www.cs-re.org.cn/c85?page=1',
        '首页-网上书店-稀土期刊': 'http://www.cs-re.org.cn/c86?page=1',
        '首页-网上书店-会议资料': 'http://www.cs-re.org.cn/c87?page=1',
        '首页-网上书店-学会书籍': 'http://www.cs-re.org.cn/c94?page=1',
        '首页-网上展厅-业务介绍': 'http://www.cs-re.org.cn/c92?page=1',
        '首页-网上展厅-网上展厅': 'http://www.cs-re.org.cn/olexhibition/?page=1',
        '首页-稀土资讯-科技资讯': 'http://www.cs-re.org.cn/info/?page=1',
        '首页-稀土资讯-成果报道': 'http://www.cs-re.org.cn/c40?page=1',
        '首页-稀土资讯-科技人物': 'http://www.cs-re.org.cn/c42?page=1',
        '首页-稀土资讯-获奖成果': 'http://www.cs-re.org.cn/c43?page=1',
        '首页-稀土资讯-稀土专利': 'http://www.cs-re.org.cn/c44?page=1',
        '首页-稀土资讯-稀土标准': 'http://www.cs-re.org.cn/c45?page=1',
        '首页-政策法规-最新政策': 'http://www.cs-re.org.cn/c47?page=1',
        '首页-政策法规-产业政策': 'http://www.cs-re.org.cn/c48?page=1',
        '首页-政策法规-产业分析': 'http://www.cs-re.org.cn/c49?page=1',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            url = value.replace('page=1', 'page=' + str(page))
            html_text,status_code = get_html(url)
            if status_code != 200:
                break

            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break

            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'B-33 中国稀土学会'
    start_run(args.projectname, args.sinceyear, account_name)
