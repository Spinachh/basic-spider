# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-11-02

import re
import io
import sys
import time
import urllib3
import requests
import requests.utils
import argparse
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_cookie(url):
    r = requests.get(url)
    c = requests.utils.dict_from_cookiejar(r.cookies)
    for a in r.cookies:
        return a.value


def get_singature():
    sin_url = 'https://www.csve.org.cn/api/sign.php?siteid=10000&type=1'
    resp = requests.get(sin_url).text
    timestamp = re.findall(r'\$\("#timestamp"\).val\("(.*?)"\)', resp)[0]
    singature = re.findall(r'\$\("#signature"\).val\("(.*?)"\)', resp)[0]
    # print(timestamp)
    # print(singature)
    return timestamp, singature


def get_supflash(url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, )
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url):

    headers = {
        'authority': 'www.csve.org.cn',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'accept-language': 'zh-CN,zh;q=0.9',
        'cookie': 'PHPSESSID=bfraq1a2ur43jf5psttjvmpdoe',
        'referer': url,
        'sec-ch-ua': '"Not/A)Brand";v="99", "Google Chrome";v="115", "Chromium";v="115"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/115.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers)

    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        if 'sp' not in news_classify:
            part1_nodes = selector.xpath('//*[@class="two_list"]/li')
            deadline = xpath_data(part1_nodes, news_classify, science_system,
                                  mongo, account_name, project_time, start_time)
        else:
            part1_nodes = selector.xpath('//*[@class="first_list"]/li')
            deadline = xpath_data(part1_nodes, news_classify, science_system,
                                  mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if 'sp' not in news_classify:
                news_title = part_nodes[i].xpath('./a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('.//h6/text()')[0].strip()

        except:
            news_title = ''
        # print(news_title)
        # breakpoint()
        try:
            if 'sp' not in news_classify:
                news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('.//p[1]/text()')[0].strip()

        except:
            news_abstract = ''

        try:
            if 'sp' not in news_classify:
                news_publish_time = part_nodes[i].xpath('./span/text()')[0].strip()
            else:
                news_publish_time = part_nodes[i].xpath('.//p[2]/text()')[0].strip()

        except:
            news_publish_time = '2023-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.csve.org.cn' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url, url_part2)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url, url_part2):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    # print(result)
    # breakpoint()
    try:
        news_content = selector_page.xpath('//*[@class="NewsText"]//p//text()|'
                                           '//*[@class="NewsText"]//p//span/text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'发布人：(.*?)&nbsp;', content_text, re.M | re.S)[0]

    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="NewsText"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)<', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        timestamp, singature = get_singature()
        read_count_url = 'https://www.csve.org.cn/api/articleclick.php?page=1&url={url}&attr=clicks' \
                         '&timestamp={timestamp}&signature={singature}&siteid=10000&type=1' \
                         '&isEnableUuid=0 '.format(url=url_part2, timestamp=timestamp, singature=singature)
        read_resp = requests.get(read_count_url).json()
        # print(read_resp)
        # breakpoint()
        read_count = read_resp.get('data').get('clicks')

    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学会动态': 'https://www.csve.org.cn/34/index_2.html',
        '首页-通知公告': 'https://www.csve.org.cn/35/index_2.html',
        '首页-学术预报': 'https://www.csve.org.cn/36/index_2.html',
        '首页-学术活动-品牌会议': 'https://www.csve.org.cn/8/index_2.html',
        '首页-学术活动-国内会议': 'https://www.csve.org.cn/9/index_2.html',
        '首页-学术活动-国际交流': 'https://www.csve.org.cn/10/index_2.html',
        '首页-学术活动-活动计划': 'https://www.csve.org.cn/11/index_2.html',
        '首页-科学普及-技术大讲堂-sp': 'https://www.csve.org.cn/13/index_2.html',
        '首页-科学普及-技术课堂-sp': 'https://www.csve.org.cn/14/index_2.html',
        '首页-科学普及-科普前沿': 'https://www.csve.org.cn/15/index_2.html',
        '首页-奖励表彰-青年人才托举工程': 'https://www.csve.org.cn/19/index_2.html',
        '首页-党的建设-党建工作动态': 'https://www.csve.org.cn/25/index_2.html',
        '首页-会员中心-政策法规': 'https://www.csve.org.cn/31/index_2.html',

    }

    for key, value in urls.items():
        news_classify = key
        get_cookie(value)
        for page in range(1, 50):
            if page == 1:
                url = value.replace('_2', '')
            else:
                url = value.replace('_2', '_' + str(page))

            html_text, status_code = get_html(url)
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'B-56 中国振动工程学会'
    start_run(args.projectname, args.sinceyear, account_name)
