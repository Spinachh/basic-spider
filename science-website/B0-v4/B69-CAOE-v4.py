# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-11-03

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
import urllib3
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_supflash(url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, verify=False)
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, pageid, itemid):

    cookies = {
        'td_cookie': get_supflash(url),
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/117.0.0.0 Safari/537.36',
    }

    params = (
        ('pageid', pageid),
        ('itemid', itemid),
    )

    response = requests.get('http://www.caoe.org.cn/nr/list.aspx', headers=headers, params=params, cookies=cookies,
                            verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@id="ctl00_main_panel3"]/table[2]/tr')
        part1_nodes.pop(5)
        part1_nodes.pop(-1)
        # print(part1_nodes)
        # breakpoint()
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    # print(part_nodes)
    # breakpoint()
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('.//a/@title')[0].strip()
        except:
            news_title = ''
        try:
            news_abstract = part_nodes[i].xpath('.//a/@title')[0].strip()
        except:
            news_abstract = ''
        try:
            news_publish_time = part_nodes[i].xpath('./td[2]/text()')[0]
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('.//a/@href')[0]
            if 'https:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.caoe.org.cn/nr/' + url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="nrTxt02"]//span//text()')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="nrTxt02"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'<span class="news_top_lyname">(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'<span class="news_top_zzname">(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击率: (.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻通告-通知公告': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=209',
        '首页-新闻通告-协会新闻': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=210',
        '首页-新闻通告-分会动态': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=397',
        '首页-新闻通告-行业动态': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=211',
        '首页-表彰奖励-科协技术奖-通知公告': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=225',
        '首页-表彰奖励-科协技术奖-奖励动态': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=226',
        '首页-表彰奖励-科协技术奖-政策法规': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=227',
        '首页-工程动态': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=6',
        '首页-专题研究': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=7',
        '首页-政策法规-社团相关规章制度': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=232',
        '首页-政策法规-协会规章制度': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=232',
        '首页-政策法规-海洋相关政策法规': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=231',
        '首页-团体标准-组织机构': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=359',
        '首页-团体标准-工作动态': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=362',
        '首页-团体标准-政策法规': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=363',
        '首页-团体标准-交流培训': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=369',
        '首页-期刊读物-交流培训': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=369',

    }

    for key, value in urls.items():
        news_classify = key
        itemid = value.split('itemid=')[1]
        for page in range(1, 50):
            url = value.replace('pageid=1', 'pageid=' + str(page))

            html_text, status_code = get_html(url, page, itemid)
            # print(html_text)
            # breakpoint()
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'B-69T 中国海洋工程咨询协会'
    start_run(args.projectname, args.sinceyear, account_name)