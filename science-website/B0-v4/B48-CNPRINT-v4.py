# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-11-01

import random
import re
import io
import sys
import time
import urllib3
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_html(url):
    response = requests.get(url, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="listbox"]/ul/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a[2]/text()')[0].strip()
        except:
            news_title = ''
        # print(news_title)
        # breakpoint()
        try:
            news_abstract = part_nodes[i].xpath('.//p//text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span//text()')[2].split(' ')[0]
        except:
            news_publish_time = '2022-01-01'

        try:
            click_count = part_nodes[i].xpath('./span//text()')[4]
        except:
            click_count = ''
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # print(click_count)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a[1]/@href')[0]
            if 'cateid/' in url_part2:
                break
            elif 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.cnprint.org.cn' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, read_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    # print(result)
    # breakpoint()
    try:
        news_content = result.get('content')

    except:
        news_content = selector_page.xpath('//*[@class="content"]//p//text() |'
                                           '//*[@class="content"]//div/text() |'
                                           '//*[@class="content"]//span/text() |'
                                           '//*[@class="content"]//p/span/text() |'
                                           '//*[@class="content"]//p/strong/text()')

        news_content = ''.join(news_content)

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="content"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源:</small>(.*?)<small>', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''


    try:
        read_count = re.findall(r'浏览：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''
    '''
    try:
        click_count = re.findall(r'点击：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    '''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-协会放送-协会公告': 'https://www.cnprint.org.cn/a/xhfs/xhgg/list_2_1.html',
        '首页-协会放送-协会动态': 'https://www.cnprint.org.cn/a/xhfs/xhdt/list_3_1.html',
        '首页-协会放送-奖项直击': 'https://www.cnprint.org.cn/a/xhfs/jxzj/list_4_1.html',
        '首页-协会放送-专业分会': 'https://www.cnprint.org.cn/a/xhfs/zyfh/list_5_1.html',
        '首页-国内动态-热点扫描': 'https://www.cnprint.org.cn/a/gndt/rdsm/list_13_1.html',
        '首页-国内动态-市场动态': 'https://www.cnprint.org.cn/a/gndt/scdt/list_14_1.html',
        '首页-国内动态-企业院校': 'https://www.cnprint.org.cn/a/gndt/qyyx/list_15_1.html',
        '首页-国内动态-产品技术': 'https://www.cnprint.org.cn/a/gndt/cpjs/list_16_1.html',
        '首页-国内动态-观察评论': 'https://www.cnprint.org.cn/a/gndt/gcpl/list_17_1.html',
        '首页-印行国际-供应商动态': 'https://www.cnprint.org.cn/a/yhgj/gysdt/list_19_1.html',
        '首页-印行国际-环球视线': 'https://www.cnprint.org.cn/a/yhgj/hqsx/list_18_1.html',
        '首页-印行国际-产品上市': 'https://www.cnprint.org.cn/a/yhgj/cpss/list_20_1.html',
        '首页-印行国际-技术追踪': 'https://www.cnprint.org.cn/a/yhgj/jszz/list_21_1.html',
        '首页-展会活动-国内': 'https://www.cnprint.org.cn/a/zhhd/guona/list_22_1.html',
        '首页-展会活动-国际': 'https://www.cnprint.org.cn/a/zhhd/guoji/list_23_1.html',
        '首页-发展环境-政策法规': 'https://www.cnprint.org.cn/a/fzhj/zcfg/list_28_1.html',
        '首页-发展环境-标准建设': 'https://www.cnprint.org.cn/a/fzhj/bzjs/list_29_1.html',
        '首页-发展环境-文件下载': 'https://www.cnprint.org.cn/a/fzhj/wjxz/list_30_1.html',
        '首页-技术市场-绿色印刷': 'https://www.cnprint.org.cn/a/jssc/lsys/list_24_1.html',
        '首页-技术市场-市场热点': 'https://www.cnprint.org.cn/a/jssc/scrd/list_25_1.html',
        '首页-技术市场-厂商动态': 'https://www.cnprint.org.cn/a/jssc/csdt/list_26_1.html',
        '首页-专题': 'https://www.cnprint.org.cn/a/ztpd/list_43_1.html',
        '首页-供应商信息': 'https://www.cnprint.org.cn/a/jssc/gyxx/list_27_1.html',
        '首页-人物志': 'https://www.cnprint.org.cn/a/rwz/list_11_1.html',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            url = value.replace('_1.html', '_' + str(page) + '.html')
            html_text, status_code = get_html(url)
            # print(html_text)
            # breakpoint()
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'B-48T 中国印刷技术协会'
    start_run(args.projectname, args.sinceyear, account_name)
