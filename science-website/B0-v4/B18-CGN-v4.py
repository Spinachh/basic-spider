# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-10-27

import random
import re
import io
import sys
import time
import json
import cchardet
import argparse
import requests
import logging
import pymongo
import urllib3
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_html(url):

    response = requests.get(url, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="nav_hd_content"]/ul/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./div/div[1]/a/@title')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./div/div[1]/a/@title')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./div/div[3]/text()')[0]
            news_publish_time = '-'.join(re.findall(r'\d+', news_publish_time))

            # if '-' not in news_publish_time:
            #     news_publish_time = news_publish_time.replace('/', '-')
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./div/div[1]/a/@href')[0]
            if 'http://www.cgn.net.cn/' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.cgn.net.cn' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = result.get('content')
    except:
        news_content = selector_page.xpath('//*[@class="dynamicsCon"]//p/text()')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="fulong_news_content"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'发稿部门：(.*?)</div', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>阅读：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-学会动态-学会动态': 'http://www.cgn.net.cn/cms/news/100000/0000000022/0000000022_1.shtml',
        '首页-学会动态-通知公告': 'http://www.cgn.net.cn/cms/news/100000/0000000021/0000000021_1.shtml',
        '首页-学会动态-信息转载': 'http://www.cgn.net.cn/cms/news/100000/0000000023/0000000023_1.shtml',
        '首页-党建动态-党建动态': 'http://www.cgn.net.cn/cms/news/100000/0000000217/0000000217_1.shtml',
        '首页-党建动态-党史学习教育': 'http://www.cgn.net.cn/cms/news/100000/0000000218/0000000218_1.shtml',
        '首页-党建动态-学习园地': 'http://www.cgn.net.cn/cms/news/100000/0000000219/0000000219_1.shtml',
        '首页-党建动态-党员风采': 'http://www.cgn.net.cn/cms/news/100000/0000000220/0000000220_1.shtml',
        '首页-科学普及-科普资讯': 'http://www.cgn.net.cn/cms/news/100000/0000000212/0000000212_1.shtml',
        '首页-科学普及-科普活动': 'http://www.cgn.net.cn/cms/news/100000/0000000213/0000000213_1.shtml',
        '首页-科学普及-BIM项目应用解析': 'http://www.cgn.net.cn/cms/news/100000/0000000026/0000000026_1.shtml',
        '首页-科学普及-BIM大讲堂': 'http://www.cgn.net.cn/cms/news/100000/0000000172/0000000172_1.shtml',
        '首页-团体标准-制度文件': 'http://www.cgn.net.cn/cms/news/100000/0000000168/0000000168_1.shtml',
        '首页-团体标准-工作动态': 'http://www.cgn.net.cn/cms/news/100000/0000000169/0000000169_1.shtml',
        '首页-团体标准-标准进行时': 'http://www.cgn.net.cn/cms/news/100000/0000000170/0000000170_1.shtml',
        '首页-青年托举-青年托举': 'http://www.cgn.net.cn/cms/news/100000/0000000165/0000000165_1.shtml',
        '首页-通知公告-期刊动态': 'http://www.cgn.net.cn/cms/news/100000/0000000163/0000000163_1.shtml',
        '首页-通知公告-论文欣赏': 'http://www.cgn.net.cn/cms/news/100000/0000000184/0000000184_1.shtml',
        '首页-人才培训-信息发布': 'http://www.cgn.net.cn/cms/news/100000/0000000107/0000000107_1.shtml',
        '首页-人才培训-考点': 'http://www.cgn.net.cn/cms/news/100000/0000000105/0000000105_1.shtml',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            if page == 1:
                url = value.replace('_1.shtml', '.shtml')
            else:
                url = value.replace('_1.shtml', '_' + str(page) + '.shtml')
            html_text, status_code = get_html(url)
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'B-18 中国图学学会'
    start_run(args.projectname, args.sinceyear, account_name)