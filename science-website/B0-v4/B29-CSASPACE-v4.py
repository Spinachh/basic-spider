# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-10-30

import random
import re
import io
import sys
import time
import argparse
import urllib3
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_html(url):

    cookies = {
        'maxPageNum2505728': '29',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        # 'If-Modified-Since': 'Sat, 10 Dec 2022 11:27:27 GMT',
        # 'If-None-Match': '"1500000008c9c2-90b1-5ef778e1b62af"',
        # 'Referer': 'http://www.csaspace.org.cn/n2489262/n2489292/index.html',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/108.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(num, html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        if num == 0:
            part1_nodes = selector.xpath('//*[@id="comp_2505728"]//table/tr')
        else:
            part1_nodes = selector.xpath('//*//tr')
        publish_time = xpath_data(part1_nodes, news_classify, science_system,
                                  mongo, account_name, project_time, start_time)
    except Exception as e:
        publish_time = ''
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))

    # print(publish_time)
    return publish_time


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./td[2]/a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./td[2]/a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_part = part_nodes[i].xpath('./td[3]/text()')[0].strip()
            news_publish_time = start_time.split('-')[0] + '-' + news_publish_part
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./td[2]/a/@href')[0]
            if 'https://www' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.csaspace.org.cn/' + url_part2.replace('../', '').replace('../','')

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            return ''

    end_publish_time = start_time.split('-')[0] + '-' + part_nodes[-1].xpath('./td[3]/text()')[0].strip()
    return time.mktime(time.strptime(end_publish_time, '%Y-%m-%d'))


def get_page_content(news_page_url):

    headers = {
        'Referer': news_page_url,
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/108.0.0.0 Safari/537.36',
    }

    html_response = requests.get(news_page_url, headers=headers)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="article_text"]//p/span/text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="article_text"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源:<a href="" target="_blank">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    science_system = read_science_account(account_name)
    urls = {
        '首页-学会新闻-学会动态': 'http://www.csaspace.org.cn/n2489262/n2489292/index.html#',
        '首页-学会新闻-通知公告': 'http://www.csaspace.org.cn/n2489262/n2489297/index.html#',
        '首页-学会新闻-党建强会': 'http://www.csaspace.org.cn/n2489262/n2489302/index.html#',
        '首页-学术交流-活动计划': 'http://www.csaspace.org.cn/n2489267/n2505372/index.html#',
        '首页-国际合作-国际会展': 'http://www.csaspace.org.cn/n2489272/n2505401/index.html#',
        '首页-国际合作-国际奖项': 'http://www.csaspace.org.cn/n2489272/n2505406/index.html#',
        '首页-国际合作-出访团组': 'http://www.csaspace.org.cn/n2489272/n2505411/index.html#',
        '首页-国际合作-来访团组': 'http://www.csaspace.org.cn/n2489272/n2505416/index.html#',
        '首页-智库建设-航天国际化研究': 'http://www.csaspace.org.cn/n2489282/n2505456/index.html#',
        '首页-智库建设-空间法律政策研究': 'http://www.csaspace.org.cn/n2489282/n2505465/index.html#',
        '首页-智库建设-学术咨询研究': 'http://www.csaspace.org.cn/n2489282/n2505470/index.html#',

    }

    for key, value in urls.items():
        news_classify = key
        url = value
        html_text, status_code = get_html(url)
        if status_code != 200:
            break
        next_page = re.findall(r'var purl="../../(.*?)"', html_text, re.M | re.S)[0]
        next_page_num = re.findall(r'maxPageNum = (.*?);', html_text, re.M | re.S)[0]
        # print(next_page)
        # print(next_page_num)
        # breakpoint()
        num = 0
        while True:
            html_text, status_code = get_html(url)
            # print(html_text)
            # breakpoint()
            if status_code != 200:
                break
            publish_time = get_data(num, html_text, news_classify, account_name,
                                    science_system, mongo, project_time, start_time)
            try:
                if int(publish_time) > int(start_time_stamp):
                    num += 1
                    next_page_num = int(next_page_num) - num
                    next_page_url = 'http://www.csaspace.org.cn/' + next_page + \
                                    '_{}'.format(next_page_num) + '.html#'
                    url = next_page_url
                    continue
                else:
                    break
            except:
                break
            finally:
                time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'B-29 中国宇航学会'
    start_run(args.projectname, args.sinceyear, account_name)
