# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-11-01

import re
import io
import sys
import time
import json
import argparse
import requests
import logging
import pymongo
import urllib3
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account


sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_pageId(url):
    html_text = requests.get(url).text
    pageId = re.findall(r"'pageId':'(.*?)'", html_text)[0]
    return pageId


def get_html(url, page, pageId):

    cookies = {
        'slb-route': '9d609105f9643d53d3650b76d4117012',
    }

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/117.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    params = (
        ('webId', 'e34f778bcbac490e94c0ca843eeafeef'),
        ('pageId', pageId),
        ('parseType', 'bulidstatic'),
        ('pageType', 'column'),
        ('tagId', '\u5185\u5BB9'),
        ('tplSetId', 'e53c495208fc4b458740c9d17e437217'),
        ('paramJson', '{"pageNo": %s,"pageSize":"15"}' % page),
    )

    response = requests.get('http://www.ctapi.org.cn/api-gateway/jpaas-publish-server/front/page/build/unit',
                            headers=headers, params=params, cookies=cookies, verify=False)

    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    content = json.loads(html_text)
    try:
        deadline = ree_data(content, news_classify, science_system,
                            mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    content_data = content['data']['html']
    # print(content_data)
    # breakpoint()
    selector = etree.HTML(content_data)
    nodes = selector.xpath('//*[@class="page-content"]//li')

    for i in range(len(nodes)):
        try:
            news_title = nodes[i].xpath('./a/@title')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = nodes[i].xpath('./a/@title')[0].strip()
        except:
            news_abstract = news_title

        try:
            news_publish_time = nodes[i].xpath('./span/text()')[0].strip()
        except:
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = nodes[i].xpath('./a/@href')[0].strip()
            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.ctapi.org.cn' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        # 'If-Modified-Since': 'Sat, 31 Dec 2022 11:28:56 GMT',
        # 'If-None-Match': '"2cc3-5f11e0612767f"',
        'Referer': news_page_url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Mobile Safari/537.36',
    }
    html_response = requests.get(news_page_url, headers=headers, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="contact"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="contact"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源: (.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：(.*?)<', content_text, re.M | re.S)[0].strip()
        # print(read_count)
        # breakpoint()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻动态-通知公告': 'http://www.ctapi.org.cn/xwdt/TZGG/index.html',
        '首页-新闻动态-学会动态': 'http://www.ctapi.org.cn/xwdt/XHDT/index.html',
        '首页-新闻动态-地方学会': 'http://www.ctapi.org.cn/xwdt/DFXH/index.html',
        '首页-新闻动态-政策法规': 'http://www.ctapi.org.cn/xwdt/ZCFG/index.html',
        '首页-新闻动态-行业资讯': 'http://www.ctapi.org.cn/xwdt/XYZX/index.html',
        '首页-新闻动态-精彩图集': 'http://www.ctapi.org.cn/xwdt/JCTJ/index.html',
        '首页-党建专题-党建动态': 'http://www.ctapi.org.cn/djzt/DJDT/index.html',
        '首页-党建专题-理论学习': 'http://www.ctapi.org.cn/djzt/LLXX/index.html',
        '首页-党建专题-党史学习': 'http://www.ctapi.org.cn/djzt/DSXX/index.html',
        '首页-党建专题-科学家精神': 'http://www.ctapi.org.cn/djzt/KXJJS/index.html',
        '首页-智库建设-智库动态': 'http://www.ctapi.org.cn/zkjs/ZKDT/index.html',
        '首页-智库建设-智库观点': 'http://www.ctapi.org.cn/zkjs/ZKGD/index.html',
        '首页-智库建设-智库专家': 'http://www.ctapi.org.cn/zkjs/ZKZJ/index.html',
        '首页-学术交流-学术动态': 'http://www.ctapi.org.cn/xsjl/XSDT/index.html',
        '首页-学术交流-国内交流': 'http://www.ctapi.org.cn/xsjl/GNJL/index.html',
        '首页-学术交流-国际交流': 'http://www.ctapi.org.cn/xsjl/GJJL/index.html',
        '首页-学术交流-学术年会': 'http://www.ctapi.org.cn/xsjl/XSNH/index.html',
        '首页-学术交流-活动安排': 'http://www.ctapi.org.cn/xsjl/HDAP/index.html',
        '首页-学术交流-中国造纸学会年鉴': 'http://www.ctapi.org.cn/xsjl/ZGZZXHNJ/index.html',
        '首页-科普之家-科普知识': 'http://www.ctapi.org.cn/kpzj/KPZS/index.html',
        '首页-科普之家-科普活动': 'http://www.ctapi.org.cn/kpzj/KPHD/index.html',
        '首页-科普之家-出版物': 'http://www.ctapi.org.cn/kpzj/CBW/index.html',
        '首页-会员之家-会员动态': 'http://www.ctapi.org.cn/hyzj/HYDT/index.html',
        # '首页-会员之家-会员风采': 'http://www.ctapi.org.cn/hyzj/HYFC/index.html',
    }
    for key, value in urls.items():
        news_classify = key
        pageId = get_pageId(value)
        for page in range(1, 50):
            html_text, status_code = get_html(value, page, pageId)
            # print(html_text)
            # breakpoint()
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'B-46 中国造纸学会'
    start_run(args.projectname, args.sinceyear, account_name)
