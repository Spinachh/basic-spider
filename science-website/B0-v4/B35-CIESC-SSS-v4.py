# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-10-31

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
import urllib3
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content1
    collection = db['{}'.format(c_name)]
    return collection


def get_html(url):
    response = requests.get(url, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="newslist_list"]/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0].replace('/', '-')
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'https:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.ciesc.cn' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="news_content"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="news_content"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)<', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-学会动态-重要通知': 'http://www.ciesc.cn/notice/?page=1',
        '首页-学会动态-学会新闻': 'http://www.ciesc.cn/news/?page=1',
        '首页-学术交流-展览展示': 'http://www.ciesc.cn/exhibition/?page=1',
        '首页-学术交流-国际会议': 'http://www.ciesc.cn/scholar/international/?page=1',
        '首页-科学普及-科普动态': 'http://www.ciesc.cn/popular/?page=1',
        '首页-科学普及-科普基地': 'http://www.ciesc.cn/c240/?page=1',
        '首页-科学普及-大学生化工设计竞赛': 'http://www.ciesc.cn/popular/CUCEDC/?page=1',
        '首页-科学普及-中国大学生Chem-E-Car竞赛': 'http://www.ciesc.cn/popular/CHEMECAR/?page=1',
        '首页-科学普及-全国“互联网+化学反应工程”课模设计大赛': 'http://www.ciesc.cn/popular/I-CHEMREAENG/?page=1',
        '首页-科学普及-全国大学生化工安全设计大赛': 'http://www.ciesc.cn/popular/CHEMSAFE/?page=1',
        '首页-科学普及-全国大学生化工短视频大赛': 'http://www.ciesc.cn/c247/?page=1',
        '首页-科学普及-科普资料下载': 'http://www.ciesc.cn/c155/?page=1',
        '首页-科学普及-化工第一课': 'http://www.ciesc.cn/c251/?page=1',
        '首页-奖励荣誉-侯德榜化工科学技术奖': 'http://www.ciesc.cn/award/houdebang/?page=1',
        '首页-奖励荣誉-中国化工学会科学技术奖': 'http://www.ciesc.cn/c149/?page=1',
        '首页-奖励荣誉-中国化工学会会士': 'http://www.ciesc.cn/fellow/?page=1',
        '首页-奖励荣誉-国际杰出青年化学工程师奖': 'http://www.ciesc.cn/c231/?page=1',
        '首页-奖励荣誉-青年人才托举工程': 'http://www.ciesc.cn/c147/?page=1',
        '首页-奖励荣誉-全国优秀科技工作者': 'http://www.ciesc.cn/c148/?page=1',
        '首页-奖励荣誉-亚洲杰出科研工作者和工程师奖': 'http://www.ciesc.cn/c232/?page=1',
        '首页-分支机构-分支机构动态': 'http://www.ciesc.cn/news/branch/?page=1',
        '首页-分支机构-地方学会动态': 'http://www.ciesc.cn/news/local/?page=1',
        '首页-党建工作-工作动态': 'http://www.ciesc.cn/news/party/?page=1',
        '首页-党建工作-上级精神': 'http://www.ciesc.cn/party/spirit/?page=1',
        '首页-党建工作-党建通知通告': 'http://www.ciesc.cn/party/notice/?page=1',
        '首页-党建工作-党务知识': 'http://www.ciesc.cn/party/learn/?page=1',
        '首页-下载中心-技术资料下载': 'http://www.ciesc.cn/download/vip/?page=1',
        '首页-下载中心-中国化工学会年度报告': 'http://www.ciesc.cn/download/annualreport/?page=1',
        '首页-下载中心-视频下载': 'http://www.ciesc.cn/download/chemecar/?page=1',
        '首页-下载中心-科普资料下载': 'http://www.ciesc.cn/c155/?page=1',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            url = value.replace('page=1', 'page=' + str(page))
            html_text, status_code = get_html(url)
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'B-35 中国化工学会'
    start_run(args.projectname, args.sinceyear, account_name)
