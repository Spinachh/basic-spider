# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-10-30

import random
import re
import io
import sys
import json
import time
import requests
import argparse
import logging
import pymongo
import urllib3
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content1
    collection = db['{}'.format(c_name)]
    return collection


def get_pageId(url):
    html_text = requests.get(url).text
    pageId = re.findall(r"'pageId':'(.*?)'", html_text)[0]
    return pageId


def get_html(url, page, pageId):

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/114.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    params = (
        ('webId', 'bfb6753ba3b942ed9cb9adbcda3ac8e6'),
        ('pageId', pageId),
        ('parseType', 'bulidstatic'),
        ('pageType', 'column'),
        ('tagId', '\u5B66\u4F1A\u52A8\u6001\u5217\u8868'),
        ('tplSetId', 'bb4653e645974c0b847ed5b914705120'),
        ('paramJson', '{"pageNo": %s ,"pageSize":10}' % page),
    )

    response = requests.get('http://www.chts.cn/api-gateway/jpaas-publish-server/front/page/build/unit',
                            headers=headers, params=params, verify=False)

    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    content = json.loads(html_text)
    try:
        deadline = ree_data(content, news_classify, science_system,
                            mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    content_data = content['data']['html']
    # print(content_data)
    # breakpoint()
    selector = etree.HTML(content_data)
    nodes = selector.xpath('//*[@class="page-content"]//li/div/div')

    for i in range(len(nodes)):
        try:
            news_title = nodes[i].xpath('.//p/a/@title')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = nodes[i].xpath('.//a/text()')[1].strip()
        except:
            news_abstract = news_title

        try:
            news_publish_year = nodes[i].xpath('.//h5/text()')[0].strip().replace('/', '-')
            news_publish_day = nodes[i].xpath('.//h2/text()')[0].strip()
            news_publish_time = news_publish_year + '-' + news_publish_day
        except:
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = nodes[i].xpath('.//a/@href')[0].strip()
            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.chts.cn' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="contact"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="contact"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源:<a href="" target="_blank">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻-通知公告': 'http://www.chts.cn/xw/TZGG/index.html',
        '首页-新闻-学会动态': 'http://www.chts.cn/xw/XHDT/index.html',
        '首页-新闻-业内资讯': 'http://www.chts.cn/xw/YNZX/index.html',
        '首页-新闻-地方学会资讯': 'http://www.chts.cn/xw/DFXHZX/index.html',
        '首页-新闻-青托工程': 'http://www.chts.cn/xw/QTGC/index.html',
        '首页-新闻-精彩图片': 'http://www.chts.cn/xw/JCTP/index.html',
        '首页-智库-智库动态': 'http://www.chts.cn/zk/ZKDT/index.html',
        '首页-学术与科普-学术新闻': 'http://www.chts.cn/xs/XSXW/index.html',
        '首页-学术与科普-通知公告': 'http://www.chts.cn/xs/TZGG/index.html',
        '首页-学术与科普-活动预告': 'http://www.chts.cn/xs/HDYG/index.html',
        '首页-学术与科普-品牌会议': 'http://www.chts.cn/xs/PPHY/index.html',
        '首页-学术与科普-学术期刊': 'http://www.chts.cn/xs/XSQK/index.html',
        '首页-学术与科普-学术咨询': 'http://www.chts.cn/xs/XSZX/index.html',
        '首页-标准-标准动态': 'http://www.chts.cn/bz/BZDT/index.html',
        '首页-标准-标准通知': 'http://www.chts.cn/bz/BZTZ/index.html',
        '首页-标准-快速链接': 'http://www.chts.cn/bz/KSLJ/index.html',
        '首页-认证-公司动态': 'http://www.chts.cn/rz/GSDT/index.html',
        '首页-认证-主要业务': 'http://www.chts.cn/rz/ZYYW/index.html',
        '首页-认证-认证业务': 'http://www.chts.cn/rz/RZYW/index.html',
        '首页-认证-双碳业务': 'http://www.chts.cn/rz/STYW/index.html',
        '首页-科技评价-科技评价动态': 'http://www.chts.cn/kjpj/KJPJDT/index.html',
        '首页-科技评价-科技评价': 'http://www.chts.cn/kjpj/KJPJ/index.html',
        '首页-科技评价-项目咨询': 'http://www.chts.cn/kjpj/XMZX/index.html',
        '首页-科技评价-科技奖励': 'http://www.chts.cn/kjpj/KJJL/index.html',
        '首页-科技评价-科技评价通知': 'http://www.chts.cn/kjpj/KJPJTZ/index.html',
        '首页-成果推广-成果推广动态': 'http://www.chts.cn/cgtg/CGTGDT/index.html',
        '首页-成果推广-科技成果': 'http://www.chts.cn/cgtg/KJCG/index.html',
        '首页-成果推广-成果转化': 'http://www.chts.cn/cgtg/CGZH/index.html',
        '首页-成果推广-成果推广通知': 'http://www.chts.cn/cgtg/CGTGTZ/index.html',
        '首页-教育培训-教育培训动态': 'http://www.chts.cn/jypx/JYPXDT/index.html',
        '首页-教育培训-特色培训': 'http://www.chts.cn/jypx/TSPX/index.html',
        '首页-教育培训-高速公路管理学院': 'http://www.chts.cn/jypx/GSGLGLXY/index.html',
        '首页-教育培训-教育培训通知': 'http://www.chts.cn/jypx/JYPXTZ/index.html',
        '首页-国际合作-国际合作动态': 'http://www.chts.cn/gjhz/GJHZDT/index.html',
        '首页-国际合作-国际会议': 'http://www.chts.cn/gjhz/GJHY/index.html',
        '首页-国际合作-一带一路': 'http://www.chts.cn/gjhz/YDYLGJJTLM/index.html',
        '首页-党建工作-党建动态': 'http://www.chts.cn/dj/DJDT/index.html',
        '首页-党建工作-党建工作': 'http://www.chts.cn/dj/DJGZ/index.html',
        '首页-党建工作-主题教育': 'http://www.chts.cn/dj/ZTJY/index.html',
        '首页-党建工作-党建专题': 'http://www.chts.cn/dj/DJZT/index.html',
        '首页-党建工作-党建活动': 'http://www.chts.cn/dj/DJHD/index.html',
    }
    for key, value in urls.items():
        news_classify = key
        pageId = get_pageId(value)
        for page in range(1, 50):
            html_text, status_code = get_html(value, page, pageId)
            # print(html_text)
            # breakpoint()
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'B-27 中国公路学会'
    start_run(args.projectname, args.sinceyear, account_name)
