# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-10-27

import random
import re
import io
import sys
import time
import json
import cchardet
import argparse
import requests
import logging
import pymongo
import urllib3
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content5
    collection = db['{}'.format(c_name)]
    return collection


def get_supflash(url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.cncos.org.cn/Content/index/catid/20.html?&p=4',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, verify=False)
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash



def get_html(url, detailId):

    cookies = {
        'td_cookie': get_supflash(url),
        'sajssdk_2015_cross_ZQSensorsObjnew_user': '1',
        'sensorsdata2015jssdkcrossZQSensorsObj': '%7B%22distinct_id%22%3A%2218b407feb97581-090329161f9add-26031e51'
                                                 '-2073600-18b407feb98110d%22%2C%22first_id%22%3A%22%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E7%9B%B4%E6%8E%A5%E6%B5%81%E9%87%8F%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC_%E7%9B%B4%E6%8E%A5%E6%89%93%E5%BC%80%22%2C%22%24latest_referrer%22%3A%22%22%2C%22%24latest_landing_page%22%3A%22http%3A%2F%2Fwww.chinesevacuum.com%2FNews%2F1630726886441488384-0-8.html%22%7D%2C%22%24device_id%22%3A%2218b407feb97581-090329161f9add-26031e51-2073600-18b407feb98110d%22%7D',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/117.0.0.0 Safari/537.36',
    }

    params = (
        ('_detailId', detailId),
    )
    req_url = url.split('?')[0]
    response = requests.get(req_url, headers=headers, params=params,
                            cookies=cookies, verify=False)

    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        if 'sp' not in news_classify:
            part1_nodes = selector.xpath('//*[@class="cbox-1 p_loopitem"]/div/div[2]/div/div')
        else:
            part1_nodes = selector.xpath('//*[@class="cbox-1 p_loopitem"]/div')

        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            if 'sp' not in news_classify:
                news_title = part_nodes[i].xpath('./div[1]//a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./div[2]//a/text()')[0].strip()

        except:
            news_title = ''

        try:
            news_abstract = news_title
        except:
            news_abstract = ''

        try:
            if 'sp' not in news_classify:
                news_publish_time = part_nodes[i].xpath('./div/div[2]/p/text()')[0].strip()
            else:
                news_publish_d = part_nodes[i].xpath('./div[1]//p[1]/text()')[0].strip()
                news_publish_y = part_nodes[i].xpath('./div[1]//p[2]/text()')[0].strip()
                news_publish_time = news_publish_y + '-' + news_publish_d

        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            if 'sp' not in news_classify:
                url_part2 = part_nodes[i].xpath('./div/div[1]//a/@href')[0]
            else:
                url_part2 = part_nodes[i].xpath('./div[2]//a/@href')[0]

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.chinesevacuum.com' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = result.get('content')
    except:
        news_content = selector_page.xpath('//*[@class="dynamicsCon"]//p/text()')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="p_articles summary"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'>来源：</span>(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>阅读：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-新闻资讯-直播专栏': 'http://www.chinesevacuum.com/News/c-0-8.html?_detailId=1630726898760159232',

        '首页-新闻资讯-科协资讯': 'http://www.chinesevacuum.com/News/c-0-8.html?_detailId=1630726885942366208',
        '首页-新闻资讯-学会动态': 'http://www.chinesevacuum.com/News/c-0-8.html?_detailId=1630726886441488384',
        '首页-新闻资讯-专业委员会': 'http://www.chinesevacuum.com/News/c-0-8.html?_detailId=1630726886928027648',

        '首页-新闻资讯-科普教育': 'http://www.chinesevacuum.com/News/c-0-8.html?_detailId=1630726887414566912',

        '首页-新闻资讯-活动资讯': 'http://www.chinesevacuum.com/News/c-0-8.html?_detailId=1630726893068488704',
        '首页-新闻资讯-市场动态': 'http://www.chinesevacuum.com/News/c-0-8.html?_detailId=1630726893571805184',
        '首页-新闻资讯-地方学会': 'http://www.chinesevacuum.com/News/c-0-8.html?_detailId=1630726894058344448',

        '首页-会议展览-会展信息-sp': 'http://www.chinesevacuum.com/Conference_exhibition/c-0-8.html?_detailId=1630726927960903680',
        '首页-会议展览-会议信息-sp': 'http://www.chinesevacuum.com/Conference_exhibition/c-0-8.html?_detailId=1630726928451637248',

        '首页-学术天地-真空与低温杂志社': '-15929806608356522&cid=21&pageSize=8&currentPage=1',
        '首页-学术天地-真空科学与技术学报': '-15929806608356522&cid=20&pageSize=8&currentPage=1',
        '首页-学术天地-真空杂志': '-15929806608356522&cid=22&pageSize=8&currentPage=1',
        '首页-学术天地-IUSVSTA国际联盟': '-15929806608356522&cid=25&pageSize=8&currentPage=1',
        '首页-学术天地-人物专栏': '-15929806608356522&cid=26&pageSize=8&currentPage=1',
    }
    for key, value in urls.items():
        news_classify = key
        # detailId = re.findall(r'detailId=(.*?)', value)[0]
        detailId = value.split('=')[1]

        for page in range(50):
            url = value.replace('0-8.html', str(page * 8) + '-8.html')
            # print(url, detailId)
            html_text, status_code = get_html(url, detailId)
            # print(html_text)
            # breakpoint()
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'B-13 中国真空学会'
    start_run(args.projectname, args.sinceyear, account_name)
