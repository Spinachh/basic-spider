# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-11-02

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
import urllib3
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_html(url, page, colId, category):

    headers = {
        'authority': 'www.cpss.org.cn',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'accept-language': 'zh-CN,zh;q=0.9',
        'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
        # 'cookie': 'ASP.NET_SessionId=l5dgyz5c2svgnf4s3qjzbukm; __RequestVerificationToken=RoGhpuFHGNkGF7OPHFw38uDerUOauGCNhDM0bVzjfvELe5GtE44idFFueJT6hnaNOoQqMLhhF_0Sy1OBY2AfMkH8Zu01; Hm_lvt_0d46326835bf94e72c5e23ec7a27763f=1673848149; Hm_lpvt_0d46326835bf94e72c5e23ec7a27763f=1673848604',
        'origin': 'https://www.cpss.org.cn',
        'referer': url,
        'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
        'sec-ch-ua-mobile': '?1',
        'sec-ch-ua-platform': '"Android"',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin',
        'user-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Mobile Safari/537.36',
        'x-requested-with': 'XMLHttpRequest',
    }

    data = {
        '$page': page,
        'rows': '15',
        'sidx': '  (case when isNew=\'1\' then 1 else 0 end) desc,'
                ' ( case when isnew=\'1\' then  ReleaseTime else null  end )\t desc ,'
                ' ( case when isUp=\'1\' then 1 else 0 end) desc, ReleaseTime  desc ',
        'sord': 'desc',
        'queryJson': '{EnabledMark:1,colId: "%s",'
                     'category: "%s",FullHead:"",'
                     'AuthorName:"",SourceName:"",sorts:"  (case when isNew=\'1\' then 1 else 0 end) desc,'
                     ' ( case when isnew=\'1\' then  ReleaseTime else null  end )\t desc ,'
                     ' ( case when isUp=\'1\' then 1 else 0 end) desc, ReleaseTime  desc "}'%(colId, category)
    }

    response = requests.post('https://www.cpss.org.cn/Home/getColNewsList',
                             headers=headers, data=data)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    # content = re.findall(r'getlistdata\((.*?)\);', html_text)[0]
    content = json.loads(html_text)
    try:
        deadline = ree_data(content, news_classify, science_system, mongo,
                            account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
             account_name, project_time, start_time):

    results = content['data']
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for item in results:
        try:
            news_title = item.get('FullHead')
        except:
            news_title = ''
        try:
            news_abstract = item.get('NewsNote')
        except:
            news_abstract = ''

        try:
            news_publish_time = item.get('ReleaseTime').split(' ')[0]
            news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
            # news_publish_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(news_publish_stamp)))
        except:
            news_publish_time = '2022-01-01'
            news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        try:
            read_count = item.get('PV')
        except:
            read_count = ''

        # print(news_title)
        # print(news_publish_time)
        # print(news_publish_stamp)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):
            # https://www.cpss.org.cn/Home/NewsDetail?newsid=b9954af5-616f-472e-9dcd-261bdb6a71f3&colId=57bf9951-639b-4ff2-85e9-09e69085998a&categoryName=
            newsid = item.get('NewsId')
            news_page_url = 'https://www.cpss.org.cn/Home/NewsDetail?newsid=' + newsid
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    try:
        news_content = selector_page.xpath('//*[@class="body_content view"]//p/text()')
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="body_content view"]//p//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''
    '''
    try:
        read_count = re.findall(r'阅读量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''
    '''
    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻动态-通知公告': 'https://www.cpss.org.cn/Home/News?colId=57bf9951-639b-4ff2-85e9-09e69085998a&category=873892bb-ec4a-45ca-b414-132b96ff0218&',
        '首页-新闻动态-学会要闻': 'https://www.cpss.org.cn/Home/News?colId=45c7ddc9-f7f6-4f65-9de4-b3ce1ba360c0&category=6703479c-b027-46f7-83dc-082484ad983a&',
        '首页-新闻动态-会员动态': 'https://www.cpss.org.cn/Home/News?colId=7c63b005-099f-4158-9fe3-5356c51b4fad&category=4ac080a5-dc4e-4721-8886-a005d73ea814&',
        '首页-新闻动态-电源战"疫"专题': 'https://www.cpss.org.cn/Home/News?colId=55ae6b73-28b2-4b82-9a29-f525eebb6090&coltype=2&coldatasource=87f6c4fe-c5f1-4864-bd38-e3ba138f5a31',
        '首页-新闻动态-会员产品推介': 'https://www.cpss.org.cn/Home/News?colId=035802ed-35c1-4002-9deb-4262a338bb0e&coltype=2&coldatasource=14527ace-ed55-4149-89cf-30d2a23b299d',
        '首页-新闻动态-会员供求信息': 'https://www.cpss.org.cn/Home/News?colId=da236c57-fa69-4dfd-9839-a22b27ffea08&coltype=4&coldatasource=8c2b3740-c8bd-4731-9911-9cfd0178f1bd',
        '首页-分支机构-专业委员会': 'https://www.cpss.org.cn/Home/News?colId=X_760729d1-9b7b-41e1-92f7-83387ed02937&coltype=4&coldatasource=123865fe-bdb7-4a27-9aab-b3636357c9cc',
        '首页-科技奖励-第八届电源科技奖': 'https://www.cpss.org.cn/Home/News?colId=beb2e0d0-da3d-4b3a-a7c9-c207294d0b8a&coltype=4&coldatasource=35b115e2-ce2a-4467-b03f-14784979da9a',
        '首页-科技奖励-往届回顾': 'https://www.cpss.org.cn/Home/News?colId=7836feef-e7e8-48fc-90d6-66b343412800&coltype=4&coldatasource=aace1880-3366-4de5-90c5-08560043f102',
        '首页-竞赛科普-科普知识': 'https://www.cpss.org.cn/Home/News?colId=c4683421-1faf-4639-9edf-bbc07eb179b0&coltype=4&coldatasource=48a868d1-5aea-4ce0-9161-4a22cd7c31f5',
        '首页-竞赛科普-科普活动': 'https://www.cpss.org.cn/Home/News?colId=832ede3d-731e-4e91-94a0-6743aadd6ced&coltype=4&coldatasource=32d5943d-71a8-45c5-a99a-4c4eedaa403c',
        '首页-国际交流-工作动态': 'https://www.cpss.org.cn/Home/News?colId=0a867d10-4330-43a3-804a-2c38700e907a&coltype=4&coldatasource=18246b55-e71a-4e39-9e99-5c4233f342f2',
        '首页-国际交流-国际资讯': 'https://www.cpss.org.cn/Home/News?colId=16532cb1-b32c-4fbf-8f36-709efe945733&coltype=4&coldatasource=38b40091-d7a2-4295-8fa2-f8f9240d06fe',
        '首页-团体标准-工作动态': 'https://www.cpss.org.cn/Home/News?colId=26c537e2-4160-44ae-a48e-0b371d0bceda&coltype=4&coldatasource=b31be3c2-5abb-4b05-8934-456060bbfc80',
        '首页-团体标准-标准全文': 'https://www.cpss.org.cn/Home/News?colId=74aab5fe-e115-4ebd-a981-0f9195f52451&coltype=4&coldatasource=b32b8a22-847a-46ce-9479-3ca6bb9e4d07',

    }
    for key, value in urls.items():
        news_classify = key
        colId = re.findall(r'colId=(.*?)&', value)[0]
        category = re.findall(r'category=(.*?)&', value)[0]
        for page in range(1, 50):
            html_text, status_code = get_html(value, page, colId, category)
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'B-62 中国电源学会'
    start_run(args.projectname, args.sinceyear, account_name)
