# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-10-30

import re
import io
import sys
import time
import json
import urllib3
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account


urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content_B31
    collection = db['{}'.format(c_name)]
    return collection


def get_pageId(url):

    response = requests.get(url)

    html_text = response.text
    # print(html_text)
    # breakpoint()
    pageId = re.findall(r"'pageId':'(.*?)'", html_text, re.M | re.S)[0].strip()
    # print(pageId)
    # breakpoint()
    return pageId


def get_html(url, page, pageId):

    cookies = {
        'slb-route': 'afa27c7106223b4adad2e68036691ac2',
    }

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/117.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
        'sec-ch-ua': '"Google Chrome";v="117", "Not;A=Brand";v="8", "Chromium";v="117"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    params = (
        ('webId', '028e4cf291774fbc8500d05be158b746'),
        ('pageId', pageId),
        ('parseType', 'bulidstatic'),
        ('pageType', 'column'),
        ('tagId', '\u5F53\u524D\u5217\u8868'),
        ('tplSetId', 'b555f115f5e744b09bc63f7028774b1a'),
        ('paramJson', '{"pageNo":%s,"pageSize":"15"}' % page),
    )

    response = requests.get('https://www.csm.org.cn/api-gateway/jpaas-publish-server/front/page/build/unit',
                            headers=headers, params=params, cookies=cookies)

    response.encoding = 'utf-8'
    status_code = response.status_code
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    content = json.loads(html_text)
    try:
        deadline = ree_data(content, news_classify, science_system,
                            mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    content_data = content['data']['html']
    # print(content_data)
    # breakpoint()
    selector = etree.HTML(content_data)
    nodes = selector.xpath('//*[@class="page-content"]//li')

    for i in range(len(nodes)):
        try:
            news_title = nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = news_title

        try:
            news_publish_time = nodes[i].xpath('./span/text()')[0].strip()
        except:
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = nodes[i].xpath('./a/@href')[0].strip()
            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.csm.org.cn' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'If-Modified-Since': 'Sat, 03 Dec 2022 09:00:00 GMT',
        'If-None-Match': '"8e60-5eee8ade6fd80"',
        # 'Referer': 'http://www.csm.org.cn/col/col8038/index.html',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    }
    html_response = requests.get(news_page_url, headers=headers, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="pages_content"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="pages_content"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源:<a href="" target="_blank">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_url = re.findall(r"访问量：<script language='javascript' src=\"(.*?)\"",
                              content_text, re.M | re.S)[0]
        read_url = 'http://www.csm.org.cn' + read_url
        read_content = requests.get(read_url).text
        read_count = re.findall(r'document.write\("(.*?)"\);', read_content, re.M | re.S)[0]
        # print(read_count)
        # breakpoint()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {

        '首页-新闻中心-学会动态': 'https://www.csm.org.cn/col/col8038/index.html',
        '首页-新闻中心-头条资讯': 'http://www.csm.org.cn/col/col6442/index.html',
        '首页-新闻中心-科协要闻': 'https://www.csm.org.cn/col/col6290/index.html',
        '首页-新闻中心-图片新闻': 'https://www.csm.org.cn/col/col8034/index.html',
        '首页-新闻中心-科技新闻-媒体聚焦': 'https://www.csm.org.cn/col/col6293/index.html',
        '首页-新闻中心-科技新闻-公开数据': 'https://www.csm.org.cn/col/col6294/index.html',
        '首页-新闻中心-科技新闻-政策研究': 'https://www.csm.org.cn/col/col6316/index.html',
        '首页-新闻中心-科技新闻-科技新进展': 'https://www.csm.org.cn/col/col6317/index.html',
        '首页-新闻中心-科技新闻-国际信息': 'https://www.csm.org.cn/col/col6296/index.html',
        '首页-学术交流-活动报道': 'https://www.csm.org.cn/col/col6298/index.html',
        '首页-学术交流-活动通知': 'https://www.csm.org.cn/col/col6291/index.html',
        '首页-学术交流-活动专栏': 'https://www.csm.org.cn/col/col7762/index.html',
        '首页-学术交流-图书': 'https://www.csm.org.cn/col/col6301/index.html',
        '首页-学术交流-期刊': 'http://www.csm.org.cn/col/col6302/index.html',
        '首页-学术交流-文集': 'https://www.csm.org.cn/col/col6303/index.html',
        '首页-学术交流-活动计划-重点活动预告': 'http://www.csm.org.cn/col/col6305/index.html',
        '首页-学术交流-活动计划-历年活动计划': 'http://www.csm.org.cn/col/col6307/index.html',
        '首页-学术交流-活动专题-活动图片': 'https://www.csm.org.cn/col/col6299/index.html',
        '首页-学术交流-活动专题-活动视频': 'http://www.csm.org.cn/col/col6299/index.html',
        '首页-国际交流-会议预告': 'https://www.csm.org.cn/col/col6308/index.html',
        '首页-国际交流-会议报道': 'https://www.csm.org.cn/col/col6309/index.html',
        '首页-国际交流-会议日历': 'http://www.csm.org.cn/col/col7244/index.html',
        '首页-国际交流-参与国际组织': 'http://www.csm.org.cn/col/col6310/index.html',
        '首页-国际交流-国外学会': 'http://www.csm.org.cn/col/col7652/index.html',
        '首页-党建强会-党建动态': 'https://www.csm.org.cn/col/col6312/index.html',
        '首页-党建强会-理论学习': 'https://www.csm.org.cn/col/col6313/index.html',
        '首页-党建强会-党建专题': 'http://www.csm.org.cn/col/col6314/index.html',
        '首页-党建强会-党建资源': 'http://www.csm.org.cn/col/col6315/index.html',
        '首页-智库建设-智库动态': 'https://www.csm.org.cn/col/col6372/index.html',
        '首页-智库建设-工程前沿': 'https://www.csm.org.cn/col/col7653/index.html',
        '首页-智库建设-独家观点': 'https://www.csm.org.cn/col/col6362/index.html',
        '首页-智库建设-科技人物': 'https://www.csm.org.cn/col/col6363/index.html',
        '首页-智库建设-精彩报告': 'https://www.csm.org.cn/col/col6364/index.html',
        '首页-智库建设-图书期刊': 'https://www.csm.org.cn/col/col6365/index.html',
        '首页-智库建设-行业热点': 'https://www.csm.org.cn/col/col6371/index.html',
        '首页-智库建设-科普园地': 'https://www.csm.org.cn/col/col6373/index.html',
        '首页-会议信息-总部会议': 'https://www.csm.org.cn/col/col6318/index.html',
        '首页-会议信息-专业分会': 'https://www.csm.org.cn/col/col6319/index.html',
        '首页-会议信息-地方学会': 'https://www.csm.org.cn/col/col6320/index.html',
        '首页-会议信息-工作委员会': 'http://www.csm.org.cn/col/col6321/index.html',
        '首页-科普园地-科普动态': 'https://www.csm.org.cn/col/col6322/index.html',
        '首页-科普园地-科学传播专家团队': 'http://www.csm.org.cn/col/col6323/index.html',
        '首页-科普园地-科普读物': 'http://www.csm.org.cn/col/col6324/index.html',
        '首页-科普园地-科普示范基地': 'http://www.csm.org.cn/col/col6325/index.html',
        '首页-科技奖励-冶金科学技术奖': 'https://www.csm.org.cn/col/col6326/index.html',
        '首页-科技奖励-冶金青年科技奖': 'https://www.csm.org.cn/col/col6327/index.html',
        '首页-科技奖励-其他奖项': 'https://www.csm.org.cn/col/col6328/index.html ',

        '首页-认证工作-通知': 'http://www.csm.org.cn/col/col6329/index.html',
        '首页-认证工作-新闻': 'http://www.csm.org.cn/col/col6330/index.html',
        '首页-认证工作-认证介绍': 'http://www.csm.org.cn/col/col6331/index.html',
        '首页-认证工作-制度方法': 'http://www.csm.org.cn/col/col6332/index.html',

    }

    for key, value in urls.items():
        news_classify = key
        pageId = get_pageId(value)
        for page in range(1, 50):
            html_text, status_code = get_html(value, page, pageId)
            # print(html_text)
            # breakpoint()
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))

    args = parser.parse_args()
    account_name = 'B-31 中国金属学会'
    start_run(args.projectname, args.sinceyear, account_name)
