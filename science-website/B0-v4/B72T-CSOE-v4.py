# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-11-03

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import urllib3
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_html(url, page):
    cookies = {
        'PHPSESSID': 'em74vvqn38313f8dahsmk5lmiq',
        'saw_terminal': 'default',
        'Hm_lvt_e7801fee9cdcef66f2a4b8fee3845d60': '1674095857',
        'Hm_lvt_20234982858f500c9cc3ef65ecd9bd6c': '1674095857',
        'Hm_lvt_be8342019ca81706ec3c3f8ea218df70': '1674095857',
        'Hm_lpvt_e7801fee9cdcef66f2a4b8fee3845d60': '1674095913',
        'Hm_lpvt_20234982858f500c9cc3ef65ecd9bd6c': '1674095913',
        'Hm_lpvt_be8342019ca81706ec3c3f8ea218df70': '1674095913',
    }

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/109.0.0.0 Mobile Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
        'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
        'sec-ch-ua-mobile': '?1',
        'sec-ch-ua-platform': '"Android"',
    }

    params = (
        ('p', page),
        ('tpl_file', 'article/activities_list_wap'),
    )

    response = requests.get(url, headers=headers, params=params, )
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="li"]')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a//div/div[2]/text()')[0].strip()
        except:
            news_title = ''
        # print(news_title)
        # breakpoint()
        try:
            news_abstract = part_nodes[i].xpath('./a//div/div[3]/text()')[0].strip()
        except:
            news_abstract = ''
        # print(news_abstract)
        # breakpoint()
        try:
            news_publish_time = part_nodes[i].xpath('./a/div//div/div[1]/text()')[0].replace('.', '-')

        except:
            news_publish_time = '2022-01-01'
        # print(news_publish_time)
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'https:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.csoe.org.cn' + url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # break
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    try:
        news_content = selector_page.xpath('//*[@class="article-cont"]//span//text()')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="article-cont"]//span//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'阅读量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻资讯': 'https://www.csoe.org.cn/hangye_list.html',
        '首页-学会动态': 'https://www.csoe.org.cn/xuehui_list.html',
        '首页-会员动态': 'https://www.csoe.org.cn/xinnews.html',
        '首页-学术交流-综合会议': 'https://www.csoe.org.cn/zh_list.html',
        '首页-学术交流-品牌会议': 'https://www.csoe.org.cn/pp_list.html',
        '首页-学术交流-前沿光学论坛': 'https://www.csoe.org.cn/qy_list.html',
        '首页-学术交流-青年科学家论坛': 'https://www.csoe.org.cn/gf_list.html',
        '首页-学术交流-产业化论坛': 'https://www.csoe.org.cn/lm_list.html',
        '首页-国际合作-焦点新闻': 'https://www.csoe.org.cn/jd_list.html',
        '首页-国际合作-通知公告': 'https://www.csoe.org.cn/tz_list.html',
        '首页-人才奖励-奖励动态': 'https://www.csoe.org.cn/kj_tz.html',
        '首页-人才奖励-通知公告': 'https://www.csoe.org.cn/zl_down.html',
        '首页-人才奖励-人才推荐': 'https://www.csoe.org.cn/kj_rc.html',
        '首页-人才奖励-优秀项目推荐': 'https://www.csoe.org.cn/kj_hjxmtj.html',
        '首页-科普与培训-科普活动': 'https://www.csoe.org.cn/kp_list.html',
        '首页-科普与培训-青少年创客行': 'https://www.csoe.org.cn/qs_list.html',
        '首页-科普与培训-创新大赛': 'https://www.csoe.org.cn/kp_cxds.html',
        '首页-科普与培训-技能培训': 'https://www.csoe.org.cn/kp_jnpx.html',
        '首页-党建专区-政策通知': 'https://www.csoe.org.cn/zhengce_list.html',
        '首页-党建专区-工作动态': 'https://www.csoe.org.cn/tongzhi_list.html',
        '首页-党建专区-党员风采': 'https://www.csoe.org.cn/dangyuan_list.html',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            html_text, status_code = get_html(value, page)
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'B-72T 中国光学工程学会'
    start_run(args.projectname, args.sinceyear, account_name)
