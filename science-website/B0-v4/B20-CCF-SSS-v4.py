# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-10-30

import random
import re
import io
import sys
import time
import json
import cchardet
import urllib3
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection



def get_html(url):

    response = requests.get(url)

    response.encoding = 'utf-8'
    status_code = response.status_code
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="m-activ"]/ul/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./div[2]//a/text()')[0].strip()
        except:
            news_title = part_nodes[i].xpath('./div//a/text()')[0].strip()

        try:
            news_abstract = part_nodes[i].xpath('./div[2]//p/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./div[2]//span/text()')[0]
            # if '-' not in news_publish_time:
            #     news_publish_time = news_publish_time.replace('/', '-')
        except:
            news_publish_time = part_nodes[i].xpath('./div/span/text()')[0]

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                url_part2 = part_nodes[i].xpath('./div[2]//a/@href')[0]
            except:
                url_part2 = part_nodes[i].xpath('./div//a/@href')[0]

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.ccf.org.cn' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = result.get('content')
    except:
        news_content = selector_page.xpath('//*[@class="dynamicsCon"]//p/text()')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="txt"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'>来源：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        article_id = news_page_url.split('/')[-1].split('.')[0]
        read_url = 'https://www.ccf.org.cn/ccf/counter?Type=Article' \
                   '&ID={}&DomID=hitcount{}'.format(article_id, article_id)
        read_content = requests.get(read_url).text
        read_count = re.findall(r'innerHTML=(.*?);', read_content, re.M | re.S)[0]
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-周年庆聚焦': 'https://www.ccf.org.cn/60zn/znqjj/index_1.shtml',
        '首页-会员活动': 'https://www.ccf.org.cn/Member_Activities/index_1.shtml',
        '首页-会员-个人会员-会员专属': 'https://www.ccf.org.cn/Membership/Individual_member/Only/index_1.shtml',
        '首页-会员-个人会员-新闻动态': 'https://www.ccf.org.cn/Membership/Individual_member/Updates/index_1.shtml',
        '首页-新闻-CCF聚焦': 'https://www.ccf.org.cn/Focus/index_1.shtml',
        '首页-新闻-CCF新闻': 'https://www.ccf.org.cn/Media_list/index_1.shtml',
        '首页-新闻-ACM信息': 'https://www.ccf.org.cn/ACM_News/index_1.shtml',
        '首页-奖励-奖励动态': 'https://www.ccf.org.cn/Awards/Awards/index_1.shtml',
        '首页-奖励-奖励推荐': 'https://www.ccf.org.cn/Awards/recommendations/index_1.shtml',
        '首页-分支机构-TC新闻': 'https://www.ccf.org.cn/Chapters/TC/News_/index_1.shtml',
        '首页-分支机构-会议通知': 'https://www.ccf.org.cn/Chapters/TC/Notice/Call_for_Papers/index_1.shtml',
        '首页-合作-产学研基金-热点新闻': 'https://www.ccf.org.cn/Chapters/TC/Notice/Call_for_Papers/index_1.shtml',
        '首页-最新动态': 'https://www.ccf.org.cn/Chapters/TC/Notice/Call_for_Papers/index_1.shtml',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            if page == 1:
                url = value.replace('index_1', 'index')
            elif page >= 6:
                if 'CCF聚焦' in news_classify:
                    url = 'https://www.ccf.org.cn/ccf/catalog/15115/pc/index_{}.shtml'.format(page)
                elif 'CCF新闻' in news_classify:
                    url = 'https://www.ccf.org.cn/ccf/catalog/15116/pc/index_{}.shtml'.format(page)
                elif '会员活动' in news_classify:
                    url = 'https://www.ccf.org.cn/ccf/catalog/21428/pc/index_{}.shtml'.format(page)
            else:
                url = value.replace('_1', '_' + str(page))

            html_text, status_code = get_html(url)
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'B-20 中国计算机学会'
    start_run(args.projectname, args.sinceyear, account_name)
