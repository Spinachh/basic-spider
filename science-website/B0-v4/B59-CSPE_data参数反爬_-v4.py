# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-11-02

import random
import re
import io
import sys
import time
import urllib3
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_html(url, pid, page):

    cookies = {
        'Hm_lvt_447319a65e071e805c8b7c176c056553': '1697184676',
        'td_cookie': '3552001351',
        'sdwaf-test-item': 'a74f660005080953090802530000595c070000500e540505070257070a0c51560104564c07080b4d08080a4d020958160202',
        'Hm_lpvt_447319a65e071e805c8b7c176c056553': '1697184940',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Origin': 'http://cspe.cpeweb.com.cn',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/117.0.0.0 Safari/537.36',
    }

    params = (
        ('pid', pid),
    )

    # data = {
    #     '__VIEWSTATE': '/wEPDwULLTE2MzgwNDM2NTIPZBYCZg9kFgICAw9kFgQCBQ8WAh4LXyFJdGVtQ291bnQCDBYYZg9kFgJmDxUCAjExD+i+heacuuS4k+WnlOS8mmQCAQ9kFgJmDxUCAjIyD+iHquaOp+S4k+WnlOS8mmQCAg9kFgJmDxUCAjIxD+mUheeCieS4k+WnlOS8mmQCAw9kFgJmDxUCAjIwD+adkOaWmeS4k+WnlOS8mmQCBA9kFgJmDxUCAjE5D+aguOeUteS4k+WnlOS8mmQCBQ9kFgJmDxUCAjE4D+mAj+W5s+S4k+WnlOS8mmQCBg9kFgJmDxUCAjE3GOeDreWKm+S4jueHg+awlOS4k+WnlOS8mmQCBw9kFgJmDxUCAjE2EuawtOi9ruacuuS4k+WnlOS8mmQCCA9kFgJmDxUCAjE1FeW3peS4muawlOS9k+S4k+WnlOS8mmQCCQ9kFgJmDxUCAjE0GOaWsOiDvea6kOiuvuWkh+S4k+WnlOS8mmQCCg9kFgJmDxUCAjEyFeWPtueJh+WItumAoOS4k+WnlOS8mmQCCw9kFgJmDxUCAjEzHueOr+S/neaKgOacr+S4juijheWkh+S4k+WnlOS8mmQCBw9kFgZmDxYCHwACARYCZg9kFgRmDxUCDOWtpuS8muaWsOmXuwExZAIBDxUBDOWtpuS8muaWsOmXu2QCAw8WAh8AAgoWFGYPZBYCZg8VBAExAzU4OC3lrabkvJrlj6zlvIDnrKzljYHkuIDlsYrkuozmrKHnkIbkuovkvJrkvJrorq4KMjAyMC0xMi0wNGQCAQ9kFgJmDxUEATEDNTg2MOWtpuS8muesrOWFq+WxiumdkuW5tOWtpuacr+W5tOS8muWcqOmHjeW6huWPrOW8gAoyMDIwLTEwLTA5ZAICD2QWAmYPFQQBMQM1NzEz5a2m5Lya5Zyo5LiK5rW35Y+s5byA56ys5Y2B5LiA5qyh5Lya5ZGY5Luj6KGo5aSn5LyaCjIwMTktMTItMjRkAgMPZBYCZg8VBAExAzU2OXAyMDE55bm05Lit5Zu95Yqo5Yqb5bel56iL5a2m5Lya6ZSF54KJ5LiT5aeU5Lya5a2m5pyv5Lqk5rWB5Lya5pqo6ZSF54KJ6KGM5Lia5oC75bel56iL5biI56CU6K6o5Lya5Zyo5YyX5Lqs5Y+s5byACjIwMTktMTItMDJkAgQPZBYCZg8VBAExAzU2N0QyMDE55bm05Zu96ZmF5Yqo5Yqb5bel56iL5Lya6K6u77yISUNPUEUtMjAxOe+8ieWcqOaYhuaYjumhuuWIqeWPrOW8gAoyMDE5LTExLTE4ZAIFD2QWAmYPFQQBMQM1NjGHAeS4reWbveWKqOWKm+W3peeoi+WtpuS8mueOr+S/neaKgOacr+S4juijheWkh+S4k+S4muWnlOWRmOS8muaNouWxiuWkp+S8muaaqOesrOS4g+WxiuW6n+W8g+eJqei9rOiDvea6kOWbvemZheS8muiuruWcqOa3seWcs+mhuuWIqeS4vuihjAoyMDE5LTA3LTE3ZAIGD2QWAmYPFQQBMQM1NTiHAeS4reWbveWKqOWKm+W3peeoi+WtpuS8muWNgeWxiuWNgeS4gOasoeW4uOWKoeeQhuS6i++8iOaJqeWkp++8ieS8muiuruaaqOOAiuWKqOWKm+acuuaisOW3peeoi+WtpuenkeWPkeWxleaKpeWRiuOAi+eglOiuqOS8mumhuuWIqeWPrOW8gAoyMDE5LTA2LTI3ZAIHD2QWAmYPFQQBMQM1NTMr5a2m5Lya5Y+s5byAMjAxOeW5tOW6puenmOS5pumVv+W3peS9nOS8muiurgoyMDE5LTA0LTE2ZAIID2QWAmYPFQQBMQM1NDQu5a2m5Lya5Li+5YqeMjAxOeW5tOS6rOOAgeayquaWsOaYpeW3peS9nOS8muiurgoyMDE5LTAxLTI0ZAIJD2QWAmYPFQQBMQM1NDAt5a2m5Lya5Y2B5bGK5LqU5qyh55CG5LqL5Lya6K6u5Zyo5p2t5bee5Y+s5byACjIwMTgtMTEtMzBkAgQPDxYEHgtSZWNvcmRjb3VudAKgAR4QQ3VycmVudFBhZ2VJbmRleAIDZGQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgEFEmN0bDAwJEltYWdlQnV0dG9uMYTUtZKmWeBYznvqCBPd+nojcGrd',
    #     '__VIEWSTATEGENERATOR': 'CA8C29DA',
    #     '__EVENTTARGET': 'ctl00$ContentPlaceHolder1$AspNetPager1',
    #     '__EVENTARGUMENT': '1',
    #     '__EVENTVALIDATION': '/wEWAwKbkKHoDQK6zbWsAwLssvLQA2qJ7iFc4Kr6z3I41CLbY8K13rGx',
    #     'ctl00$textfield3': '',
    #     'ctl00$ContentPlaceHolder1$AspNetPager1_input': '3'
    # }

    data_content = requests.post(url, headers=headers).text

    viewstate = re.findall(r'value="(.*?)" />', data_content, re.M | re.S)[0]
    viewstategenerator = re.findall(r'id="__VIEWSTATEGENERATOR" value="(.*?)" />', data_content, re.M | re.S)[0]
    eventvalidation = re.findall(r'id="__EVENTVALIDATION" value="(.*?)" />', data_content, re.M | re.S)[0]

    data = {
        '__VIEWSTATE': viewstate,
        '__VIEWSTATEGENERATOR': viewstategenerator,
        '__EVENTTARGET': 'ctl00$ContentPlaceHolder1$AspNetPager1',
        '__EVENTARGUMENT': page,
        '__EVENTVALIDATION': eventvalidation,
        'ctl00$textfield3': '',
        'ctl00$ContentPlaceHolder1$AspNetPager1_input': (page -1)
    }
    response = requests.post('http://cspe.cpeweb.com.cn/news.aspx', headers=headers, params=params, cookies=cookies,
                             data=data, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@width="96%"]/tr')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./td[2]/a/text()')[0].strip()
        except:
            news_title = ''
        try:
            news_abstract = part_nodes[i].xpath('./td[2]/a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./td[3]/text()')[0]
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./td[2]/a/@href')[0]
            if 'cateid/' in url_part2:
                break
            elif 'http://' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://cspe.cpeweb.com.cn/' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    # print(result)
    # breakpoint()
    try:
        news_content = selector_page.xpath('//*[@class="zw14pxblack"]//text()')
        news_content = ''.join(x.strip() for x in news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="zw14pxblack"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)<', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'阅读次数： (.*?)</', content_text, re.M | re.S)[0]
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        # '首页-关于学会-学会理事会': 'http://cspe.cpeweb.com.cn/about051.aspx',
        # '首页-关于学会-学会工作委员会': 'http://cspe.cpeweb.com.cn/about061.aspx',
        '首页-信息中心-学会新闻': 'http://cspe.cpeweb.com.cn/news.aspx?pid=1',
        '首页-信息中心-学会公告': 'http://cspe.cpeweb.com.cn/news.aspx?pid=2',
        '首页-信息中心-媒体报道': 'http://cspe.cpeweb.com.cn/news.aspx?pid=3',
        '首页-信息中心-行业动态': 'http://cspe.cpeweb.com.cn/news.aspx?pid=4',
        '首页-信息中心-展会展览': 'http://cspe.cpeweb.com.cn/news.aspx?pid=5',
        '首页-信息中心-国际交流': 'http://cspe.cpeweb.com.cn/news.aspx?pid=6',
        '首页-学术交流-国内会议': 'http://cspe.cpeweb.com.cn/news.aspx?pid=7',
        '首页-学术交流-国际会议': 'http://cspe.cpeweb.com.cn/news.aspx?pid=8',
        '首页-学术交流-科学论文': 'http://cspe.cpeweb.com.cn/news.aspx?pid=9',
        '首页-学术交流-会议征文': 'http://cspe.cpeweb.com.cn/news.aspx?pid=10',
        '首页-科普知识-能源知识': 'http://cspe.cpeweb.com.cn/news.aspx?pid=11',
        '首页-科普知识-科普活动': 'http://cspe.cpeweb.com.cn/news.aspx?pid=12',
    }
    for key, value in urls.items():
        news_classify = key
        pid = value.split('=')[1]
        for page in range(1, 50):
            html_text, status_code = get_html(value, pid, page)
            # print(html_text)
            # breakpoint()
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break

            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'B-59 中国动力工程学会'
    start_run(args.projectname, args.sinceyear, account_name)
