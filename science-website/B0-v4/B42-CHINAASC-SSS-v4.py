# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-10-31

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
import urllib3
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content1
    collection = db['{}'.format(c_name)]
    return collection


def get_html(url):

    headers = {
        'authority': 'chinaasc.org.cn',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'accept-language': 'zh-CN,zh;q=0.9',
        'cache-control': 'max-age=0',
        # 'if-modified-since': 'Thu, 22 Dec 2022 08:04:37 GMT',
        # 'if-none-match': 'W/"63a40f95-6e8e"',
        'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
        'sec-ch-ua-mobile': '?1',
        'sec-ch-ua-platform': '"Android"',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'none',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/108.0.0.0 Mobile Safari/537.36',
    }

    response = requests.get(url, headers=headers)
    status_code = response.status_code
    response.encoding = 'gb2312'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="newsPage"]/div')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/div[2]/h4/text()')[0].strip()
        except:
            news_title = ''
        # print(news_title)
        # breakpoint()
        try:
            news_abstract = part_nodes[i].xpath('./a/div[2]//p/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./a/div[2]/span/text()')[0].split('发布时间：')[-1]
            # print(news_publish_time)
            news_publish_time = news_publish_time.split(' ')[0]
        except:
            news_publish_time = '2022-01-01'
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'cateid/' in url_part2:
                break
            elif '//www' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.chinaasc.org.cn/' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)

    html_response.encoding = 'gb2312'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    # print(result)
    # breakpoint()
    try:
        news_content = selector_page.xpath('//*[@class="main_body"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="main_body"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)|', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''


    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：</span><span id="hits">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-新闻动态-学会新闻': 'https://chinaasc.org.cn/news/xuehuiyaowen/list_101_1.html',
        '首页-新闻动态-通知公告': 'https://www.chinaasc.org.cn/news/tongzhigonggao/list_127_1.html',
        '首页-新闻动态-综合消息': 'https://www.chinaasc.org.cn/news/zonghexiaoxi/list_100_1.html',
        '首页-新闻动态-媒体报道': 'https://www.chinaasc.org.cn/news/meitibaodao/list_191_1.html',
        '首页-新闻动态-上级部门': 'https://www.chinaasc.org.cn/news/shangjibumen/list_352_1.html',
        '首页-新闻动态-会员动态': 'https://www.chinaasc.org.cn/news/huiyuandongtai/list_368_1.html',
        '首页-新闻动态-抗击疫情-学会在行动': 'https://www.chinaasc.org.cn/news/kangjiyiqing/xuehuizaixingdong',
        '首页-新闻动态-抗击疫情-各类指南': 'https://www.chinaasc.org.cn/news/kangjiyiqing/geleizhinan/list_311_1.html',
        '首页-新闻动态-抗击疫情-建筑人在行动': 'https://www.chinaasc.org.cn/news/kangjiyiqing/jianzhurenzaixingdong/list_312_1.html',
        '首页-新闻动态-抗击疫情-在线课程': 'https://www.chinaasc.org.cn/news/kangjiyiqing/zaixiankecheng/list_313_1.html',
        '首页-新闻动态-抗击疫情-国际友人在行动': 'https://www.chinaasc.org.cn/news/kangjiyiqing/guojiyourenzaixingdong/list_314_1.html',
        '首页-新闻动态-党史党课-中央精神': 'https://www.chinaasc.org.cn/news/dang/zhongyangbushu/list_354_1.html',
        '首页-新闻动态-党史党课-部委部署': 'https://www.chinaasc.org.cn/news/dang/buweibushu/list_355_1.html',
        '首页-新闻动态-党史党课-党建强会': 'https://www.chinaasc.org.cn/news/dang/dangjianqianghui/list_356_1.html',
        '首页-新闻动态-党史党课-党史纵横': 'https://www.chinaasc.org.cn/news/dang/dangshizongheng/list_357_1.html',
        '首页-学术交流-学术会议-年度会议计划': 'https://www.chinaasc.org.cn/news/xueshuhuiyi/nianduhuiyijihua/list_331_2.html',
        '首页-学术交流-学术会议-学术年会': 'https://www.chinaasc.org.cn/news/xueshuhuiyi/xueshunianhui/list_333_1.html',
        '首页-学术交流-学术会议-郑州城市设计大会': 'https://www.chinaasc.org.cn/news/xueshuhuiyi/zhengzhouchengshishejidahui/list_334_1.html',
        '首页-学术交流-学术会议-威海国际人居节': 'https://www.chinaasc.org.cn/news/xueshuhuiyi/weihaiguojirenjijie/list_335_1.html',
        '首页-学术交流-学术会议-年度重点会议': 'https://www.chinaasc.org.cn/news/xueshuhuiyi/nianduzhongdianhuiyi/list_336_1.html',
        '首页-学术交流-学术动态': 'https://www.chinaasc.org.cn/news/xueshudongtai/list_284_1.html',
        '首页-学术交流-课题研究': 'https://www.chinaasc.org.cn/news/ketiyanjiu/list_283_1.html',
        '首页-对外交往-国际事务动态-国际事务动态': 'https://www.chinaasc.org.cn/html/gjswdt/list_54_1.html',
        '首页-对外交往-国际事务动态-国际科技组织': 'https://www.chinaasc.org.cn/news/guoji/guojizuzhi',
        '首页-对外交往-国际事务动态-国际双边合作': 'https://www.chinaasc.org.cn/news/guoji/guojishuangbianhezuo/',
        '首页-对外交往-国际事务动态-国际学术期刊': 'https://www.chinaasc.org.cn/news/guoji/guojiqikan/',
        '首页-学会奖项-梁思成建筑奖-通知公告': 'https://www.chinaasc.org.cn/news/liangsicheng/tongzhigonggao/',
        '首页-学会奖项-梁思成建筑奖-评奖动态': 'https://www.chinaasc.org.cn/news/liangsicheng/pingjiangdongtai/list_69_1.html',
        '首页-学会奖项-梁思成建筑奖-宣传报道': 'https://www.chinaasc.org.cn/news/liangsicheng/xuanchuan/',
        '首页-学会奖项-梁思成建筑奖-奖项简介': 'https://www.chinaasc.org.cn/news/liangsicheng/jiangxiangjianjie/',
        '首页-学会奖项-梁思成建筑奖-评选办法': 'https://www.chinaasc.org.cn/news/liangsicheng/huojiangmingdan/list_70_1.html',
        '首页-学会奖项-梁思成建筑奖-历届获奖者': 'https://www.chinaasc.org.cn/news/liangsicheng/zuopinzhanshi/list_71_1.html',
        '首页-学会奖项-建筑设计奖-通知公告': 'https://www.chinaasc.org.cn/news/jianzhushejijiang/tongzhigonggao/list_299_1.html',
        '首页-学会奖项-建筑设计奖-评选动态': 'https://www.chinaasc.org.cn/news/jianzhushejijiang/pingxuandongtai/',
        '首页-学会奖项-建筑设计奖-获奖名单': 'https://www.chinaasc.org.cn/news/jianzhushejijiang/huojiangmingdan/list_272_1.html',
        '首页-学会奖项-建筑设计奖-评选办法': 'https://www.chinaasc.org.cn/news/jianzhushejijiang/pingxuanbanfa/',
        '首页-学会奖项-科技进步奖-通知公告': 'https://www.chinaasc.org.cn/news/kjj/tongzhigonggao/',
        '首页-学会奖项-科技进步奖-评选动态': 'https://www.chinaasc.org.cn/news/kjj/dongtai/',
        '首页-学会奖项-科技进步奖-获奖名单': 'https://www.chinaasc.org.cn/news/kjj/huojiangmingdan/',
        '首页-学会奖项-科技进步奖-评选办法': 'https://www.chinaasc.org.cn/news/kjj/banfa/',
        '首页-科技服务': 'https://www.chinaasc.org.cn/news/kejifuwu/list_252_1.html',
        '首页-科普展览': 'https://www.chinaasc.org.cn/news/kpzl/list_287_1.html',
        '首页-建筑教育': 'https://www.chinaasc.org.cn/news/jiaoyukepu/list_239_1.html',
        '首页-科技培训': 'https://www.chinaasc.org.cn/news/kejipeixun/list_249_1.html',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            if '_1.html' in value:
                url = value.replace('_1.html', '_' + str(page) + '.html')
            else:
                url = value
            html_text, status_code = get_html(url)
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break

            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'B-42 中国建筑学会'
    start_run(args.projectname, args.sinceyear, account_name)
