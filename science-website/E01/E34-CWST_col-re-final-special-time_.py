# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-03-24

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, columnid, uid, startrecord, endrecord):

    headers = {
        'Accept': 'application/xml, text/xml, */*; q=0.01',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'http://www.cwst.net',
        'Referer': url,
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    params = (
        ('startrecord', startrecord),
        ('endrecord', endrecord),
        ('perpage', '40'),
    )

    data = {
        'col': '1',
        'appid': '1',
        'webid': '73',
        'path': '/',
        'columnid': columnid,
        'sourceContentType': '1',
        'unitid': uid,
        'webname': '\u4E2D\u56FD\u5973\u79D1\u6280\u5DE5\u4F5C\u8005\u534F\u4F1A',
        'permissiontype': '0'
    }

    response = requests.post('http://www.cwst.net/module/web/jpage/dataproxy.jsp',
                             headers=headers, params=params,
                             data=data, verify=False)

    response.encoding = 'utf-8'
    return response.text


def get_data(columnid, html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    content = re.findall(r'<record><!\[CDATA\[(.*?)\]\>', html_text, re.M | re.S)
    try:
        if news_classify == '首页-会员之家':
            content_lst = content[1:]
        else:
            content_lst = content

        flag = ree_data(columnid, content_lst, news_classify, science_system, mongo, account_name, project_time, start_time)
        return flag
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(columnid, content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(content)):
        try:
            news_title = re.findall(r'html" target="_blank">(.*?)</a>', content[i])[0]
        except Exception as e:
            logging.warning('{} {} Article Title Was Error :{}'.format(account_name, news_classify, e))
            news_title = ''

        try:
            news_abstract = re.findall(r'<p class="description">(.*?)</p>', content[i])[0]
        except Exception as e:
            logging.warning('{} {} Article Abstract Was Error :{}'.format(account_name, news_classify, e))
            news_abstract = ''

        try:
            news_publish_time = re.findall(r'/art/(.*?)/art_', content[i])[0].replace('/', '-')
            # news_publish_d = re.findall(r'<h2 style="font-size:24px; font-weight:bold">(.*?)</h2>', content[i])[0].strip()
            # news_publish_y = re.findall(r'<h5 style="font-size:13px;">(.*?)</h5>', content[i])[0].strip().replace('/', '-')
            # news_publish_time = news_publish_y + '-' + news_publish_d

        except Exception as e:
            logging.warning('{} {} Article Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                url_part2 = re.findall(r'href="(.*?)"', content[i])[0]
            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.cwst.net' + url_part2

            if 'mp.weixin.qq.c' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_page_content(news_page_url, columnid)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            flag = '00'
            return flag


def get_page_content(news_page_url, columnid):
    article_id = news_page_url.split('_')[-1].replace('.html', '')
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="u_content_text"]//span/text() | '
                                           '//*[@class="u_content_text"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'<span id="author">(.*?)</', content_text, re.M | re.S)[0]
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="u_content_text"]//span//img/@src |'
                                        ' //*[@class="u_content_text"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_url = 'http://www.cwst.net/module/visitcount/' \
                   'articlehits.jsp?colid={}&artid={}'.format(columnid, article_id)
        read_content = requests.get(read_url).text

        read_count = re.findall(r'(\d+)', read_content, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # print(click_count)
    # breakpoint()
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    # print(news_page_url)
    # breakpoint()
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻动态-协会要闻-2023': 'http://www.cwst.net/col/col8225/index.html?uid=27376&pageNum=1',
        '首页-新闻动态-协会要闻-2022': 'http://www.cwst.net/col/col7858/index.html?uid=27376&pageNum=1',
        '首页-新闻动态-通知公告': 'http://www.cwst.net/col/col7388/index.html?uid=27356&pageNum=1',
        '首页-党建动态-党建动态': 'http://www.cwst.net/col/col7410/index.html?uid=27356&pageNum=1',
        '首页-党建动态-党建资源': 'http://www.cwst.net/col/col7406/index.html?uid=27356&pageNum=1',
        '首页-党建动态-中央精神': 'http://www.cwst.net/col/col7406/index.html?uid=27356&pageNum=1',
        '首页-党建动态-思想建设': 'http://www.cwst.net/col/col7407/index.html?uid=27356&pageNum=1',
        '首页-党建动态-学习二十大': 'http://www.cwst.net/col/col7408/index.html?uid=27356&pageNum=1',
        '首页-智库服务-会员风采': 'http://www.cwst.net/col/col7429/index.html?uid=27356&pageNum=1',
        '首页-智库服务-调查研究': 'http://www.cwst.net/col/col7427/index.html?uid=27356&pageNum=1',
        '首页-智库服务-媒体报道': 'http://www.cwst.net/col/col7390/index.html?uid=27356&pageNum=1',
        '首页-交流活动-活动聚焦': 'http://www.cwst.net/col/col7428/index.html?uid=27356&pageNum=1',
        '首页-交流活动-论坛集锦': 'http://www.cwst.net/col/col7425/index.html?uid=27356&pageNum=1',
        '首页-交流活动-国际交流': 'http://www.cwst.net/col/col7337/index.html?uid=27356&pageNum=1',
        '首页-科普活动-科技达人': 'http://www.cwst.net/col/col7430/index.html?uid=27356&pageNum=1',
        '首页-科普活动-科研进行时': 'http://www.cwst.net/col/col7431/index.html?uid=27356&pageNum=1',
        '首页-科普活动-校园之旅': 'http://www.cwst.net/col/col7426/index.html?uid=27356&pageNum=1',
        '首页-影像资源-视频点播': 'http://www.cwst.net/col/col7394/index.html?uid=27356&pageNum=1',
        '首页-会员服务-读书会': 'http://www.cwst.net/col/col7415/index.html?uid=27356&pageNum=1',
    }

    startrecord = 1
    endrecord = 43
    flag ='11'
    for key, value in urls.items():
        news_classify = key
        columnid = re.findall(r'col\/col(.*?)\/index', value)[0]
        try:
            uid = re.findall(r'uid=(.*?)&', value)[0]
        except:
            uid = ''

        while flag:
            html_text = get_html(value, columnid, uid, startrecord, endrecord)
            # print(html_text)
            # breakpoint()
            flag = get_data(columnid, html_text, news_classify, account_name, science_system, mongo, project_time, start_time)
            if flag != '00':
                startrecord = endrecord + 1
                endrecord = endrecord + 43
                logging.warning('startrecord:{}'.format(startrecord))
                logging.warning('endrecord:{}'.format(endrecord))
            else:
                break


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'E-34T 中国女科技工作者协会'
    start_run(args.projectname, args.sinceyear, account_name)
