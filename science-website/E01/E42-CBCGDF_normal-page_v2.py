# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-04-10

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import urllib3
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
urllib3.disable_warnings()
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash(url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, )
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url):

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/111.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, verify=False)
    response.encoding = 'utf-8'
    status_codes = response.status_code
    return response.text, status_codes


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    try:
        if 'special' not in news_classify:
            selector = etree.HTML(html_text)
            part_nodes1 = selector.xpath('//*[@class="cont_03"]//li')
            if 'sp' in news_classify:
                part_nodes = part_nodes1[1:]
            else:
                part_nodes = part_nodes1
            deadline = xpath_data(part_nodes, news_classify, science_system, mongo,
                                  account_name, project_time, start_time)
            if deadline:
                return deadline
        else:
            selector = etree.HTML(html_text)
            part1_nodes = selector.xpath('//*[@class="cont_02"]//li')
            deadline = xpath_data(part1_nodes, news_classify, science_system, mongo,
                                  account_name, project_time, start_time)
            if deadline:
                return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('./div[2]//a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./span[2]/a/text()')[0].strip()
        except Exception as e:
            logging.warning('News Title Was Error: {}'.format(e))
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = part_nodes[i].xpath('./div[2]/p[1]/text()')[0].strip()
            else:
                news_abstract = news_title
        except Exception as e:
            logging.warning('News Abstract Was Error: {}'.format(e))
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_time = part_nodes[i].xpath('./div[2]/p[2]/text()')[0].strip().replace('.', '-')
            else:
                news_publish_time = part_nodes[i].xpath('./span[1]//text()')[0].replace('.', '-')
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2023-12-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('./div[1]/a/@href')[0].strip()
                else:
                    url_part2 = part_nodes[i].xpath('./span[2]/a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.cbcgdf.org' + url_part2

            if 'mp.weixin.qq' in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                    read_count, click_count = get_weixin_page_content(news_page_url)

            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                    read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False,
                                 )
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''
    try:
        news_content = selector_page.xpath('//*[@class="cont"]//span//text() | '
                                           '//*[@class="cont"]//p//text() | '
                                           '//*[@class="cont"]//p//span//text()'
                                           )
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'<span class="AUTHOR">(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="cont"]//span//img/@src |'
                                        '//*[@class="cont"]//p//img/@src | '
                                        '//*[@class="cont"]//div//img/@src |'
                                        '//*[@class="cont"]//img/@src')[0]
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="cont"]//span//video/@src |'
                                         ' //*[@class="cont"]//p//video/@src |'
                                         ' //*[@class="cont"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源：(.*?)&nbsp', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r';阅读数：(.*?)&nbsp', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''
    # print(read_count)
    # breakpoint()

    try:
        click_count = re.findall(r'点击量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻资讯-公益活动': 'http://www.cbcgdf.org/NewsList/4936/1.html',
        '首页-新闻资讯-两会建议-special': 'http://www.cbcgdf.org/NewsList/5022/1.html',
        '首页-新闻资讯-通知公告-sp': 'http://www.cbcgdf.org/NewsList/5033/1.html',
        '首页-新闻资讯-宣传材料': 'http://www.cbcgdf.org/NewsList/4935/1.html',
        '首页-新闻资讯-媒体报道': 'http://www.cbcgdf.org/NewsList/4856/1.html',
        '首页-新闻资讯-公益新闻-sp': 'http://www.cbcgdf.org/NewsList/4856/1.html',
        '首页-绿会党建-党建动态': 'http://www.cbcgdf.org/NewsList/5052/1.html',
        '首页-二级机构-绿会绿少-special': 'http://www.cbcgdf.org/NewsList/5036/1.html',
        '首页-信息公开-信息披露-special': 'http://www.cbcgdf.org/NewsList/4944/1.html',
        '首页-绿会文化历史-绿会历史-sp': 'http://www.cbcgdf.org/NewsList/4951/1.html',
        '首页-绿会文化历史-政策法规-special': 'http://www.cbcgdf.org/NewsList/4934/1.html',
        '首页-国际交流合作-国际交流-sp': 'http://www.cbcgdf.org/NewsList/4937/1.html',
        '首页-国际交流合作-国际公约': 'http://www.cbcgdf.org/NewsList/5053/1.html',
        '首页-国际交流合作-国际合作-special': 'http://www.cbcgdf.org/NewsList/5027/1.html',
    }

    for key, value in urls.items():
        news_classify = key

        for page in range(1, 50):
            url = value.replace('1.html', str(page) + '.html')
            html_text, status_code = get_html(url)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                logging.warning('{} Account: {} Classfiy: {} DeadLine.{}'
                                .format('*'*15, account_name, news_classify, '*'*15))
                break
            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'E-42W 中国生物多样性保护与绿色发展基金会'
    start_run(args.projectname, args.sinceyear, account_name)
