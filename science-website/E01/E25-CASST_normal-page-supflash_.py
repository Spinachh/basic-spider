# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-03-17

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import urllib3
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
urllib3.disable_warnings()
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash(url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, )
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, page, id, supFlash):

    cookies = {
        'td_cookie': supFlash,
        'JSESSIONID': 'E00C547053ED0B2DF52EA491F1CFC143',
        'Hm_lvt_80e1c0ee9cd4f82e2e7ddec902c44c51': '1678690967',
        'Hm_lpvt_80e1c0ee9cd4f82e2e7ddec902c44c51': '1678690997',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Referer': 'http://www.casst.org.cn/',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
    }

    params = (
        ('method', 'more'),
        ('page', page),
        ('id', id),
    )

    response = requests.get('http://www.casst.org.cn/cms/columnmanager.do',
                            headers=headers, params=params, cookies=cookies,
                            verify=False)

    response.encoding = 'gb2312'
    return response.text


def get_data(page, html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    try:
        if 'special' not in news_classify:
            selector = etree.HTML(html_text)
            part_nodes = selector.xpath('//*[@class="news_list"]/li')
            xpath_data(part_nodes, news_classify, science_system, mongo,
                       account_name, project_time, start_time)
        else:
            selector = etree.HTML(html_text)
            part1_nodes = selector.xpath('//*[@class="clear"]/li')
            xpath_data(part1_nodes, news_classify, science_system, mongo,
                       account_name, project_time, start_time)

    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('.//a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('.//h6/text()')[0].strip()
        except Exception as e:
            logging.warning('News Title Was Error: {}'.format(e))
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./div//p/text()')[0].strip()
        except Exception as e:
            logging.warning('News Abstract Was Error: {}'.format(e))
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_time = part_nodes[i].xpath('.//span/text()')[0].strip()
            else:
                news_publish_time = part_nodes[i].xpath('.//p/text()')[0]
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0]
                else:
                    url_part2 = part_nodes[i].xpath('./a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.casst.org.cn' + url_part2

            if 'mp.weixin.qq.co' in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_page_url)

            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    page_id = re.findall(r'&id=(\w+)', news_page_url)[0]
    cookies = {
        'td_cookie': get_supflash(news_page_url),
        'JSESSIONID': 'E00C547053ED0B2DF52EA491F1CFC143',
        'Hm_lvt_80e1c0ee9cd4f82e2e7ddec902c44c51': '1678690967',
        'Hm_lpvt_80e1c0ee9cd4f82e2e7ddec902c44c51': '1678691656',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': news_page_url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
    }

    params = (
        ('method', 'view'),
        ('pageid', 'view'),
        ('id', page_id),
    )

    html_response = requests.get('http://www.casst.org.cn/cms/contentmanager.do',
                                 headers=headers, params=params,
                                 cookies=cookies, verify=False)
    html_response.encoding = 'gb2312'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''
    try:
        news_content = selector_page.xpath('//*[@class="news_text clearfix"]//span//text() | '
                                           '//*[@class="news_text clearfix"]//p//text() | '
                                           '//*[@class="news_text clearfix"]//p//span//text()'
                                           )
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者：</b>(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="news_text clearfix"]//span//img/@src |'
                                        '//*[@class="news_text clearfix"]//p//img/@src | '
                                        '//*[@class="news_text clearfix"]//div//img/@src |'
                                        '//*[@class="news_text clearfix"]//img/@src')[0]
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="news_text clearfix"]//span//video/@src |'
                                         ' //*[@class="news_text clearfix"]//p//video/@src |'
                                         ' //*[@class="news_text clearfix"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'文章来源:(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'阅读量： (.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''
    # print(read_count)
    # breakpoint()

    try:
        click_count = re.findall(r'点击量：<i>(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-通知公告': 'http://www.casst.org.cn/cms/columnmanager.do?method=more&page=1&id=tree98387ce083ea',
        '首页-工作动态': 'http://www.casst.org.cn/cms/columnmanager.do?method=more&page=1&id=tree985e947083ea',
        '首页-全部新闻': 'http://www.casst.org.cn/cms/columnmanager.do?method=more&page=1&id=treed4c262d086c1',
        '首页-老科协智库': 'http://www.casst.org.cn/cms/columnmanager.do?method=more&page=1&id=tree0cd20d48843e',
        '首页-老科协奖': 'http://www.casst.org.cn/cms/columnmanager.do?method=more&page=1&id=tree0ce2ef8c843e',
        '首页-老科技工作者日': 'http://www.casst.org.cn/cms/columnmanager.do?method=more&page=1&id=tree0cf31268843e',
        '首页-老年科技大学': 'http://www.casst.org.cn/cms/columnmanager.do?method=more&page=1&id=tree456dc1e48f23',
        '首页-老科沙龙': 'http://www.casst.org.cn/cms/columnmanager.do?method=more&page=1&id=tree3e59f5948f23',
        '首页-老科党校': 'http://www.casst.org.cn/cms/columnmanager.do?method=more&page=1&id=tree725f219c889d',
        '首页-今日科苑': 'http://www.casst.org.cn/cms/columnmanager.do?method=more&page=1&id=tree60d5c830814d',
        '首页-建言献策': 'http://www.casst.org.cn/cms/columnmanager.do?method=more&page=1&id=tree43205a4c865f',
        '首页-科学普及': 'http://www.casst.org.cn/cms/columnmanager.do?method=more&page=1&id=tree42ec24ac865f',
        '首页-振兴乡村': 'http://www.casst.org.cn/cms/columnmanager.do?method=more&page=1&id=tree056c1760843e',
        '首页-助力企业': 'http://www.casst.org.cn/cms/columnmanager.do?method=more&page=1&id=tree05cd8fb8843e',
        '首页-表彰奖励': 'http://www.casst.org.cn/cms/columnmanager.do?method=more&page=1&id=tree433f1838865f',
        '首页-科学健康': 'http://www.casst.org.cn/cms/columnmanager.do?method=more&page=1&id=tree06bf4ed4843e',
    }

    for key, value in urls.items():
        news_classify = key
        supFlash = get_supflash(value)
        id = re.findall(r'id=(\w+)', value)[0]
        for page in range(1, 10):
            url = value.replace('page=1', 'page=' + str(page))
            html_text = get_html(url, page, id, supFlash)
            # print(html_text)
            # breakpoint()
            try:
                get_data(page, html_text, news_classify, account_name,
                         science_system, mongo, project_time, start_time,)
            except Exception as e:
                logging.warning('Page {} Get Data Was Error:{}'.format(page, e))
                break

            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2022-07-01')
    args = parser.parse_args()
    account_name = 'E-25 中国老科学技术工作者协会'
    start_run(args.projectname, args.sinceyear, account_name)
