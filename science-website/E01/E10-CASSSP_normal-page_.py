# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-03-13

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash(url):

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, verify=False)
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, page):

    cid = re.findall(r'news/(.*?)/#', url)[0]
    compid = re.findall(r'#c_(\D+\d+)', url)[0]

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Content-Length': '0',
        'Origin': 'http://www.casssp.org.cn',
        'Referer': 'http://www.casssp.org.cn/news/3/',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    params = (
        ('compId', compid),
        ('cid', cid),
        ('pageSize', '15'),
        ('currentPage', page),
    )

    response = requests.post('http://www.casssp.org.cn/comp/portalResNews/list.do',
                             headers=headers, params=params, verify=False)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):

    try:
        if 'special' in news_classify:
            content = json.loads(html_text)
            ree_data(content, news_classify, science_system, mongo,
                     account_name, project_time, start_time)
        else:
            selector = etree.HTML(html_text)
            part1_nodes = selector.xpath('//*[@class="newList row"]')
            xpath_data(part1_nodes, news_classify, science_system, mongo,
                   account_name, project_time, start_time)

    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
             account_name, project_time, start_time):
    results = content['data']['result']
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for item in results:
        try:
            news_title = item.get('title')
        except:
            news_title = ''
        try:
            news_abstract = item.get('introduce').strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = item.get('publishTime').split(' ')[0]
        except Exception as e:
            logging.warning('Publish Time Was Error: {}'.format(e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # print(news_publish_stamp)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            idCategory = item.get('idCategory')
            article_id = item.get('id')
            news_page_url = 'https://www.lsc.org.cn/cns/contents/{}/{}.html'.format(idCategory, article_id)

            if 'https://mp.weixin.qq.co' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_page_url)



            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):

        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('./a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('.//a/div[2]/h2/text()')[0].strip()
        except Exception as e:
            logging.warning('News Title Was Error: {}'.format(e))
            news_title = ''

        try:
            news_abstract = news_title
        except Exception as e:
            logging.warning('News Abstract Was Error: {}'.format(e))
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_time = part_nodes[i].xpath('./span/text()')[0].strip()
            else:
                news_publish_d = part_nodes[i].xpath('.//h5/text()')[0]
                news_publish_y = part_nodes[i].xpath('.//h6/text()')[0]
                news_publish_time = news_publish_y + '-' + news_publish_d
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'


        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('./a/@href')[0]
                else:
                    url_part2 = part_nodes[i].xpath('./a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'https' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.casssp.org.cn' + url_part2

            if 'mp.weixin.qq' in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_page_url)

            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''
    try:
        news_content = selector_page.xpath('//*[@class="e_box p_articles"]//p//span//text() | '
                                           '//*[@class="e_box p_articles"]//p//text() | '
                                           '//*[@class="e_box p_articles"]//span//text() | '
                                           )
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者：</span>(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="e_box p_articles"]//span//img/@src |'
                                        '//*[@class="e_box p_articles"]//p//img/@src | '
                                        '//*[@class="e_box p_articles"]//div//img/@src |'
                                        '//*[@class="e_box p_articles"]//img/@src')[0]
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="e_box p_articles"]//span//video/@src |'
                                         ' //*[@class="e_box p_articles"]//p//video/@src |'
                                         ' //*[@class="e_box p_articles"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源：</span>(.*?)</', content_text, re.M | re.S)[0].strip().split('来源')[-1]
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        if '.html' in news_page_url:
            ID = re.findall(r'(\d+)', news_page_url)[0]
            click_url = 'http://www.casssp.org.cn/commentViewCount/ajax/updateAndGetViewCount?id={}&appTag=3'\
                .format(ID)
            # http://www.casssp.org.cn/commentViewCount/ajax/updateAndGetViewCount?id=877&appTag=3
            click_content = requests.get(click_url).json()
            click_count = click_content.get('data')
        else:
            click_count = ''
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-研究会动态-通知公告': 'http://www.casssp.org.cn/news/3/#c_portalResNews_list-16255421806704236-',
        '首页-研究会动态-新闻动态': 'http://www.casssp.org.cn/news/2/#c_portalResNews_list-16255421806704236-',
        '首页-研究会动态-信息公开': 'http://www.casssp.org.cn/news/92/#c_portalResNews_list-16255421806704236-',
        '首页-研究会动态-时政要闻': 'http://www.casssp.org.cn/news/93/#c_portalResNews_list-16255421806704236-',
        '首页-学术活动-会议预告': 'http://www.casssp.org.cn/news/8/#c_portalResNews_list-1625563120278-',
        '首页-学术活动-品牌活动': 'http://www.casssp.org.cn/news/51/#c_portalResNews_list-1625563120278-',
        '首页-学术活动-中国科技政策与管理学术年会': 'http://www.casssp.org.cn/news/52/#c_portalResNews_list-1625563120278-',
        '首页-学术活动-中国科技智库论坛': 'http://www.casssp.org.cn/news/53/#c_portalResNews_list-1625563120278-',
        '首页-学术活动-中国科技政策论坛': 'http://www.casssp.org.cn/news/54/#c_portalResNews_list-1625563120278-',
        '首页-智库观点-热点话题': 'http://www.casssp.org.cn/news/47/#c_portalResNews_list-1625563789333-',
    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 4):
            url = value + str(page)
            # supFlash = get_supflash(url)
            html_text = get_html(url, page,)
            # print(html_text)
            # breakpoint()
            try:
                get_data(html_text, news_classify, account_name,
                         science_system, mongo, project_time, start_time)
            except Exception as e:
                logging.warning('Page {} Get Data Was Error:{}'.format(page, e))
                break

            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'E-10 中国科学学与科技政策研究会'
    start_run(args.projectname, args.sinceyear, account_name)
