# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-03-20
import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, parentIds, categoryid, page):
    headers = {
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
    }

    params = (
        ('access_token', 'ec700a61d1468ce80bb7d9a17d7b2076'),
        ('expand_fields', ''),
        ('categoryId', categoryid),
        ('parentIds', '1,{},{}'.format(parentIds, categoryid)),
        ('status', '30'),
        ('page_size', '10'),
        ('page_num', page),
        ('sort', 'a.publish_time'),
        ('order_by', 'DESC'),
    )

    response = requests.get('http://www.case.org.cn/upms/api/v1/cms_doc',
                            headers=headers, params=params, verify=False)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    try:
        if 'special' not in news_classify:
            content = json.loads(html_text)
            ree_data(content, news_classify, science_system, mongo,
                     account_name, project_time, start_time)
        else:
            content = json.loads(html_text)
            content_html = content['data']['html']
            selector = etree.HTML(content_html)
            part1_nodes = selector.xpath('//*[@class="anUl"]/li')
            xpath_data(part1_nodes, news_classify, science_system, mongo,
                       account_name, project_time, start_time)

    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
             account_name, project_time, start_time):
    results = content['data']
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for item in results:
        try:
            news_title = item.get('title')
        except:
            news_title = ''
        try:
            news_abstract = item.get('description').strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = item.get('publish_time').split(' ')[0]
        except Exception as e:
            logging.warning('Publish Time Was Error: {}'.format(e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # print(news_publish_stamp)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            article_id = item.get('id')
            news_page_url = 'http://www.case.org.cn/upms/api/v1/cms_doc/{}?' \
                            'access_token=ec700a61d1468ce80bb7d9a17d7b2076' \
                            '&expand_fields=detail,prenext'.format(article_id)

            if 'https://mp.weixin.qq.c' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_page_url, article_id)
            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            logging.warning(news_dict_content)
            breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a//text()')[0].strip()
        except Exception as e:
            logging.warning('Title Was Error: {}'.format(e))
            news_title = ''
        try:
            news_abstract = news_title
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0].strip()
            news_publish_time = re.sub(r'[\],\[]', '', news_publish_time)
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                url_part2 = part_nodes[i].xpath('./a/@href')[0]
            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://cansm.org.cn' + url_part2

            if 'mp.weixin.qq.com' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url, article_id):
    headers = {
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.case.org.cn/xhdtnr/{}'.format(article_id),
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
    }

    params = (
        ('access_token', 'ec700a61d1468ce80bb7d9a17d7b2076'),
        ('expand_fields', 'detail,prenext'),
    )

    html_text = requests.get('http://www.case.org.cn/upms/api/v1/cms_doc/8d2b2cd9da014d28bcd40750e552f3ae',
                                 headers=headers, params=params, verify=False).json()


    html_response = html_text.get('data')
    # html_response.encoding = 'utf-8'
    # content_text = html_response.text
    # selector_page = etree.HTML(content_text)
    # extractor = GeneralNewsExtractor()


    try:
        news_text = html_response.get('content')
        selector_page = etree.HTML(news_text)
        # news_content = selector_page.xpath('//*//p/text()| '
        #                                    '//p//span/text()|')
        news_content = re.findall(r'[\u3002\uff1b\uff0c\uff1a\u201c\u201d\uff08\uff09'
                                  r'\u3001\uff1f\u300a\u300b\u4e00-\u9fa5\d+]', news_text)

        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = ''

    try:
        news_author = html_response.get('author')
    except:
        news_author = ''

    try:
        news_text = html_response.get('content')
        news_imgs = re.findall(r'img src="(.*?)"', news_text)[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_text = html_response.get('content')
        news_video = re.findall(r'video src="(.*?)"', news_text)[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = html_response.get('resource')
    except:
        source = ''

    try:
        read_count = html_response.get('view')
    except:
        read_count = ''

    try:
        click_count = html_response.get('read')
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-协会动态-通知公告': 'http://www.case.org.cn/xhdt/2e98991d32034fdd8b0a136ce8391647#####d2b1398a111c4b05864001b0042dd79a',
        '首页-协会动态-工作动态': 'http://www.case.org.cn/xhdt/484cf6eade0d4f729a1b8dffd0009612#####d2b1398a111c4b05864001b0042dd79a',
        '首页-科学考察-活动招募': 'http://www.case.org.cn/xhdt/3945f0f029584ef0b34bca68a6f0f906#####da35d99588374aa6b0919d518750b24c',
        '首页-科学考察-活动专栏': 'http://www.case.org.cn/xhdt/5a203692e1c540a48631ea146361aec8#####da35d99588374aa6b0919d518750b24c',
        '首页-科学普及-政策指引': 'http://www.case.org.cn/xhdt/36c524c98bf14640affd406053e5f835#####08b8c89e02e5458a9e3a8ad518582c82',
        '首页-科学普及-成果展示': 'http://www.case.org.cn/xhdt/9bf96478b7fa457cb8f6d2ffdfb4cbe2#####08b8c89e02e5458a9e3a8ad518582c82',
        '首页-科学普及-科普之窗': 'http://www.case.org.cn/xhdt/680af03986084a548d06e49cfac164a1#####08b8c89e02e5458a9e3a8ad518582c82',
        '首页-社会资讯-社会资讯': 'http://www.case.org.cn/xhdt/6b416db1cb784362bd3322f94e5c761e#####1,6b416db1cb784362bd3322f94e5c761e',
        '首页-会员服务-会员风采': 'http://www.case.org.cn/xhdt/96f8e25c12924fa8af7124447f36bcaa#####1,8255753bd74b43c0b496a8bfaa6c14e7',
    }

    for key, value in urls.items():
        news_classify = key
        parentIds = value.split('#####')[-1]
        url = value.split('#####')[0]
        pageID = url.split('/')[-1]

        for page in range(1, 5):
            html_text = get_html(url, parentIds, pageID, page)
            # print(html_text)
            # breakpoint()
            try:
                get_data(html_text, news_classify, account_name,
                         science_system, mongo, project_time, start_time)
            except Exception as e:
                logging.warning('Page {} Get Data Was Error:{}'.format(page, e))
                break

            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2022-07-01')
    args = parser.parse_args()
    account_name = 'E-26T 中国科学探险协会'
    start_run(args.projectname, args.sinceyear, account_name)
