# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-06-20

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):
    response = requests.get(url, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="zjjg-panel-bd "]/ul/li')
        deadline = xpath_data(part1_nodes, news_classify,
                              science_system, mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[1].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[1].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./a/span/text()')[0]
            # print(news_publish_time)
            # breakpoint()
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'http://' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.csfish.org.cn' + url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="solution-inform-content infoDetail"]//span//text()')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="solution-inform-content infoDetail"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?);', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击次数：<span>(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-信息发布-工作动态': 'http://www.csfish.org.cn/catalog/198-',
        '首页-信息发布-通知公告': 'http://www.csfish.org.cn/catalog/199-',
        '首页-组织机构-规章制度': 'http://www.csfish.org.cn/catalog/rules',
        '首页-党建-党建强会': 'http://www.csfish.org.cn/catalog/127-',
        '首页-党建-秘书处党建': 'http://www.csfish.org.cn/catalog/128-',
        '首页-学术交流-学术大会': 'http://www.csfish.org.cn/catalog/119-',
        '首页-学术交流-学术活动': 'http://www.csfish.org.cn/catalog/118-',
        '首页-学术交流-学术期刊': 'http://www.csfish.org.cn/catalog/periodical',
        '首页-学术交流-青年年会': 'http://www.csfish.org.cn/catalog/209-',
        '首页-科学普及-科普活动': 'http://www.csfish.org.cn/catalog/124-',
        '首页-科学普及-科普基地': 'http://www.csfish.org.cn/catalog/183-',
        '首页-科学普及-科普队伍': 'http://www.csfish.org.cn/catalog/173-',
        '首页-科学普及-科普作品': 'http://www.csfish.org.cn/catalog/125-',
        '首页-人才奖励-人才举荐': 'http://www.csfish.org.cn/catalog/talentRecommendation',
        '首页-人才奖励-范蠡奖': 'http://www.csfish.org.cn/catalog/203-',
        '首页-人才奖励-青年科技奖': 'http://www.csfish.org.cn/catalog/yonth',
        '首页-人才奖励-青年人才托举': 'http://www.csfish.org.cn/catalog/qingtuo',
        '首页-专家智库-智库建设': 'http://www.csfish.org.cn/catalog/TTConstruction',
        '首页-专家智库-抗击疫情': 'http://www.csfish.org.cn/catalog/yiqing',
        '首页-体系队伍-分支机构': 'http://www.csfish.org.cn/catalog/130-',
        '首页-体系队伍-地方学会': 'http://www.csfish.org.cn/catalog/131-',
        '首页-体系队伍-服务站点': 'http://www.csfish.org.cn/catalog/serviceSites',
        '首页-体系队伍-单位会员': 'http://www.csfish.org.cn/catalog/181-',
        '首页-服务-团体标准': 'http://www.csfish.org.cn/catalog/tuantibiaozhun',
        '首页-服务-种业创新': 'http://www.csfish.org.cn/catalog/213-',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            if '-' in value:
                url = value + str(page)
            else:
                url = value
            html_text, status_code = get_html(url)
            if status_code != 200:
                logging.warning('Has Not Got the Html\'s Correct Response')
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'C-04 中国水产学会'
    start_run(args.projectname, args.sinceyear, account_name)
