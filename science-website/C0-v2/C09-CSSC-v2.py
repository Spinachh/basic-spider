# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-06-21

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    headers = {
        'authority': 'www.chinacrops.org',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'accept-language': 'zh-CN,zh;q=0.9',
        'cookie': 'PHPSESSID=58foovquhc305h1utg89jo8ef2',
        'referer': 'https://www.chinacrops.org/',
        'sec-ch-ua': '"Chromium";v="112", "Google Chrome";v="112", "Not:A-Brand";v="99"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/112.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers)

    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="content"]//ul/li')
        deadline = xpath_data(part1_nodes, news_classify,
                              science_system, mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = news_title
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0]
        except:
            news_publish_time = '2023-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.chinacrops.org' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url, url_part2)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url, url_part2):

    headers = {
        'authority': 'www.chinacrops.org',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'accept-language': 'zh-CN,zh;q=0.9',
        'cache-control': 'max-age=0',
        'cookie': 'PHPSESSID=58foovquhc305h1utg89jo8ef2',
        # 'if-modified-since': 'Tue, 25 Apr 2023 01:01:12 GMT',
        # 'if-none-match': 'W/"64472658-6fc2"',
        'sec-ch-ua': '"Chromium";v="112", "Google Chrome";v="112", "Not:A-Brand";v="99"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'none',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/112.0.0.0 Safari/537.36',
    }

    html_response = requests.get(news_page_url, headers=headers)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="NewsText"]//span/text()|'
                                           '//*[@class="NewsText"]//p/text()|')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="NewsText"]//span//img/@src |'
                                        ' //*[@class="NewsText"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)&nbsp', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    # 这里的点击数是api接口获取，网站coder比较懒没有继续做
    try:
        signature_content = requests.get('https://www.chinacrops.org/api/sign.php?siteid=10000&type=1').text
        signature_content = signature_content.split('.')[-1]
        signature = re.findall(r'"(.*?)"', signature_content)[0]
        click_url = 'https://www.chinacrops.org/api/articleclick.php?' \
                    'url={}&attr=clicks' \
                    '&timestamp={}' \
                    '&signature={}' \
                    '&siteid=10000&type=1&isEnableUuid=0'.format(url_part2, int(time.time()), signature)
        click_content = requests.get(click_url).json()
        click_count = click_content.get('data')['clicks']
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '主页-新闻动态': 'https://www.chinacrops.org/44/index_1.html',
        '主页-学术交流-通知公告': 'https://www.chinacrops.org/17/index_1.html',
        '主页-学术交流-学术交流': 'https://www.chinacrops.org/18/index_1.html',
        '主页-学术交流-人才培养': 'https://www.chinacrops.org/20/index_1.html',
        '主页-学术交流-科技奖励': 'https://www.chinacrops.org/19/index_1.html',
        '主页-科学普及-科普活动': 'https://www.chinacrops.org/26/index_1.html',
        '主页-科学普及-科普作品': 'https://www.chinacrops.org/26/index_1.html',
        '主页-会员之家-会员风采': 'https://www.chinacrops.org/33/index_1.html',
        '主页-科技服务-科创活动': 'https://www.chinacrops.org/237/index_1.html',
        '主页-科技服务-科技成果': 'https://www.chinacrops.org/238/index_1.html',
        '主页-学会党建-党建活动': 'https://www.chinacrops.org/37/index_1.html',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            if page == 1:
                url = value.replace('index_1.html', 'index.html')
            else:
                url = value.replace('index_1.html', 'index_' + str(page) + '.html')
            html_text, status_code = get_html(url)
            if status_code != 200:
                logging.warning('Has Not Got the Html\'s Correct Response')
                break
            # print(html_text)
            # breakpoint()
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break

            time.strptime(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'C-09 中国作物学会'
    start_run(args.projectname, args.sinceyear, account_name)
