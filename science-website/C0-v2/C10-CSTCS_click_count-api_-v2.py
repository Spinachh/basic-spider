# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-06-25

import random
import re
import io
import sys
import time
import json
import urllib3
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        # 'If-Modified-Since': 'Sun, 29 Jan 2023 00:42:08 GMT',
        # 'If-None-Match': '"17e5a2847a33d91:0"',
        'Referer': url,
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-User': '?1',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Mobile Safari/537.36',
        'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
        'sec-ch-ua-mobile': '?1',
        'sec-ch-ua-platform': '"Android"',
    }

    response = requests.get(url, headers=headers, verify=False, timeout=3)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name,
             science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="rightBox"]//li')
        deadline = xpath_data(part1_nodes, news_classify,
                              science_system, mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0]
            news_publish_time = news_publish_time
            # print(news_publish_time)
            # breakpoint()
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'http://' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.cstcs.org.cn' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        # 'If-Modified-Since': 'Sun, 29 Jan 2023 00:42:03 GMT',
        # 'If-None-Match': '"96662f817a33d91:0"',
        'Referer': 'https://www.cstcs.org.cn/channels/11.html',
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-User': '?1',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/109.0.0.0 Mobile Safari/537.36',
        'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
        'sec-ch-ua-mobile': '?1',
        'sec-ch-ua-platform': '"Android"',
    }
    html_response = requests.get(news_page_url, headers=headers, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        re_content_text = re.findall(r'font-size: 18px;">(.*?)var container_2', content_text, re.M | re.S)[0]
        dr = re.compile(r'<[^>]+>', re.S)
        news_content = dr.sub('', re_content_text).strip()
    except:
        news_content = selector_page.xpath('//*[@id="content"]//span/text() | //*[@id="content"]//p/text()')
        news_content = ''.join([x.strip() for x in news_content])
    # print(news_content)
    # breakpoint()
    try:
        news_author = result.get('author')
    except:
        news_author = re.findall(r'作者：</td><td>(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')

    try:
        content = re.findall(r'font-size: 18px;">(.*?)var container_2', content_text, re.M | re.S)[0]
        news_imgs = re.findall(r'src="(.*?)"', content, re.M | re.S)
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    # 这里的点击数是api接口获取
    try:
        value = re.findall(r'value: \'(.*?)\',', content_text, re.M | re.S)[0]
        headers = {
            'Accept': 'application/vnd.siteserver+json; version=1',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Connection': 'keep-alive',
            'Content-Type': 'application/json;charset=UTF-8',
            'Origin': 'https://www.cstcs.org.cn',
            'Referer': news_page_url,
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-origin',
            'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Mobile Safari/537.36',
            'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
            'sec-ch-ua-mobile': '?1',
            'sec-ch-ua-platform': '"Android"',
        }

        params = (
            ('', ['', '']),
            (str(int(time.time())), ''),
        )

        data_json = {"value": value, "page": 1}
        data = str(data_json)

        response = requests.post('https://www.cstcs.org.cn/api/sys/stl/actions/dynamic', headers=headers, params=params,
                                 data=data).text
        # print(response)
        # breakpoint()
        content = json.loads(response)
        click_count = content.get('html')
    except:
        click_count = ''
    # print(click_count)
    # breakpoint()
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-最要新闻': 'https://www.cstcs.org.cn/channels/4_1.html',
        '首页-通知公告': 'https://www.cstcs.org.cn/channels/8_1.html',
        '首页-党建强会': 'https://www.cstcs.org.cn/channels/11_1.html',
        '首页-会员之家': 'https://www.cstcs.org.cn/channels/166_1.html',
        '首页-分支机构': 'https://www.cstcs.org.cn/channels/42_1.html',
        '首页-省级学会': 'https://www.cstcs.org.cn/channels/42_1.html',
        '首页-创新驱动-学术交流合作': 'https://www.cstcs.org.cn/channels/173_1.html',
        '首页-创新驱动-产研动态': 'https://www.cstcs.org.cn/channels/174_1.html',
        '首页-科学普及-科普新闻': 'https://www.cstcs.org.cn/channels/82_1.html',
        '首页-科学普及-科普知识': 'https://www.cstcs.org.cn/channels/83_1.html',
        '首页-科学普及-应急科普': 'https://www.cstcs.org.cn/channels/108_1.html',
        '首页-科学普及-科普短视频': 'https://www.cstcs.org.cn/channels/109_1.html',
        '首页-奖励举荐-奖励章程': 'https://www.cstcs.org.cn/channels/74_1.html',
        '首页-奖励举荐-科技进步奖': 'https://www.cstcs.org.cn/channels/74_1.html',
        '首页-奖励举荐-表彰奖励': 'https://www.cstcs.org.cn/channels/172_1.html',
        '首页-特别推荐-二十大精神': 'https://www.cstcs.org.cn/channels/181_1.html',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            if page == 1:
                url = value.replace('_1.html', '.html')
            else:
                url = value.replace('_1.html', '_' + str(page) + '.html')
            html_text, status_code = get_html(url)
            if status_code != 200:
                logging.warning('Has Not Got the Html\'s Correct Response')
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'C-10 中国热带作物学会'
    start_run(args.projectname, args.sinceyear, account_name)
