# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-07-19

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import urllib3
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
urllib3.disable_warnings()
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content_new_20230719
    return collection


def get_html(url):
    response = requests.get(url, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    try:
        if 'special' not in news_classify:
            selector = etree.HTML(html_text)
            part_nodes1 = selector.xpath('//*[@class="ldConRMain"]//li')
            deadline = xpath_data(part_nodes1, news_classify, science_system, mongo,
                                  account_name, project_time, start_time)
            if deadline:
                return deadline
        else:
            selector = etree.HTML(html_text)
            part1_nodes = selector.xpath('//*[@class="first_list three"]/li')
            deadline = xpath_data(part1_nodes, news_classify, science_system, mongo,
                                  account_name, project_time, start_time)
            if deadline:
                return deadline

    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('./a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except Exception as e:
            logging.warning('News Title Was Error: {}'.format(e))
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = news_title
            else:
                news_abstract = news_title
        except Exception as e:
            logging.warning('News Abstract Was Error: {}'.format(e))
            news_abstract = ''

        try:
            if 'special' in news_classify:
                news_publish_text = part_nodes[i].xpath('./span/text()')[0].strip()
                news_publish_time = re.sub(r'[\(,\)]', '', news_publish_text)
            else:
                news_publish_time = part_nodes[i].xpath('./span/text()')[0].strip()
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2023-12-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('./a/@href')[0].strip()
                else:
                    url_part2 = part_nodes[i].xpath('./a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.tobacco.gov.cn' + url_part2

            if 'mp.weixin.qq' in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_page_url)

            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''
    try:
        news_content = selector_page.xpath('//*[@class="tyContent"]//span//text() | '
                                           '//*[@class="tyContent"]//p//text() | '
                                           '//*[@class="tyContent"]//p//span//text()'
                                           )
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'<span class="AUTHOR">(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="tyContent"]//span//img/@src |'
                                        '//*[@class="tyContent"]//p//img/@src | '
                                        '//*[@class="tyContent"]//div//img/@src |'
                                        '//*[@class="tyContent"]//img/@src')[0]
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="tyContent"]//span//video/@src |'
                                         ' //*[@class="tyContent"]//p//video/@src |'
                                         ' //*[@class="tyContent"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'<span class="source">(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r';阅读数：(.*?)&nbsp', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''
    # print(read_count)
    # breakpoint()

    try:
        click_count = re.findall(r'点击量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-烟草学会-学会资讯-学会要闻': 'http://www.tobacco.gov.cn/gjyc/xhxhyw/ycxh_ejxlist_2.shtml',
        '首页-烟草学会-学会资讯-省级学会资讯': 'http://www.tobacco.gov.cn/gjyc/sjxhzx/ycxh_ejxlist_2.shtml',
        '首页-学术交流-国内交流': 'http://www.tobacco.gov.cn/gjyc/xhgnjl/ycxh_ejxlist_2.shtml',
        '首页-学术交流-国际交流': 'http://www.tobacco.gov.cn/gjyc/xhgjjl/ycxh_ejxlist_2.shtml',
        '首页-学术交流-学术论文': 'http://www.tobacco.gov.cn/gjyc/xhxslw/ycxh_ejxlist_2.shtml',
        '首页-科普园地-大众科普': 'http://www.tobacco.gov.cn/gjyc/xhdzkp/ycxh_ejxlist_2.shtml',
        '首页-科普园地-烟草科普': 'http://www.tobacco.gov.cn/gjyc/xhyckp/ycxh_ejxlist_2.shtml',
        '首页-科普园地-科普活动': 'http://www.tobacco.gov.cn/gjyc/xhkphd/ycxh_ejxlist_2.shtml',
        '首页-科普园地-作品展区': 'http://www.tobacco.gov.cn/gjyc/xhzpzq/ycxh_ejxlist_2.shtml',
        '首页-烟草学会-政策文件': 'http://www.tobacco.gov.cn/gjyc/xhzcwj/ycxh_ejxlist_2.shtml',
        '首页-烟草学会-通知公告': 'http://www.tobacco.gov.cn/gjyc/xhtzgg/ycxh_ejxlist_2.shtml',
    }

    for key, value in urls.items():
        news_classify = key
        if 'sp' in news_classify:
            target_page = 100
        else:
            target_page = 50

        for page in range(1, target_page):
            if page == 1:
                url = value.replace('_2', '')
            else:
                url = value.replace('_2', '_' + str(page))

            html_text, status_code = get_html(url)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q2')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-04-01')
    args = parser.parse_args()
    account_name = 'B-53 中国烟草学会'
    start_run(args.projectname, args.sinceyear, account_name)
