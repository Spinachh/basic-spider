# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-07-10

import random
import re
import io
import sys
import time
import json
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content_new
    return collection


def get_pageId(url):
    html_text = requests.get(url).text
    pageId = re.findall(r"'pageId':'(.*?)'", html_text)[0]
    return pageId


def get_html(url, page, pageId):

    cookies = {
        'slb-route': '40b2e3b251dfd62e52f067ffb4208cc9',
    }

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/114.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    params = (
        ('webId', 'd4bf4412d68e47e9933cb6d862f1c456'),
        ('pageId', pageId),
        ('parseType', 'bulidstatic'),
        ('pageType', 'column'),
        ('tagId', '\u4FE1\u606F\u5217\u8868'),
        ('tplSetId', 'c66939a7d8fc4a8c9eb3d22cbab3690d'),
        ('paramJson', '{"pageNo":%s,"pageSize":"15"}'% page),
    )

    response = requests.get('http://www.cies.ac.cn/api-gateway/jpaas-publish-server/front/page/build/unit',
                            headers=headers, params=params, cookies=cookies, verify=False)

    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    content = json.loads(html_text)
    try:
        deadline = ree_data(content, news_classify, science_system,
                            mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    content_data = content['data']['html']
    selector = etree.HTML(content_data)
    nodes = selector.xpath('//*[@class="page-content"]//li')

    for i in range(len(nodes)):
        try:
            news_title = nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = nodes[i].xpath('./p/text()')[0].strip()
        except:
            news_abstract = news_title

        try:
            news_publish_time = nodes[i].xpath('./span/text()')[0].strip()
        except:
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = nodes[i].xpath('./a/@href')[0].strip()
            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.cies.ac.cn' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="txt_box"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="txt_box"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源: (.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：(.*?)<', content_text, re.M | re.S)[0].strip()
        # print(read_count)
        # breakpoint()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学会动态-通知公告': 'http://www.cies.ac.cn/xhdt/tzgg/index.html',
        '首页-学会动态-学会动态': 'http://www.cies.ac.cn/xhdt/xhdt/index.html',
        '首页-学会动态-省学会动态': 'http://www.cies.ac.cn/xhdt/ssxhdt/index.html',
        '首页-学会动态-分支机构动态': 'http://www.cies.ac.cn/xhdt/fzjgdt/index.html',
        '首页-学会动态-专题报道': 'http://www.cies.ac.cn/xhdt/ztbd/index.html',
        '首页-学会动态-工作要点': 'http://www.cies.ac.cn/xhdt/gzyd/index.html',
        # '首页-高照班-培训报名': 'http://www.cies.ac.cn/gzb/pxbm/index.html',
        '首页-学术交流-学术动态': 'http://www.cies.ac.cn/xsjl/xsdt/index.html',
        '首页-学术交流-中国照明展': 'http://www.cies.ac.cn/xsjl/zgzmz/index.html',
        '首页-学术交流-国际交流': 'http://www.cies.ac.cn/xsjl/gjjl/index.html',
        '首页-学术交流-学会出版物': 'http://www.cies.ac.cn/xsjl/xhcbw/index.html',
        '首页-学术交流-照明工程学报': 'http://www.cies.ac.cn/xsjl/zmgcxb/index.html',
        '首页-学术交流-照明工程年鉴': 'http://www.cies.ac.cn/xsjl/zmgcnj/index.html',
        '首页-科学普及-科普活动': 'http://www.cies.ac.cn/kxpj/kphd/index.html',
        '首页-科学普及-科普知识': 'http://www.cies.ac.cn/kxpj/kpzs/index.html',
        '首页-科学普及-科普资源': 'http://www.cies.ac.cn/kxpj/kpzy/index.html',
        '首页-中照照明奖-获奖推介': 'http://www.cies.ac.cn/zzzmj/hjtj/index.html',
        '首页-党史党建-党建动态': 'http://www.cies.ac.cn/dsdj/djdt/index.html',
        '首页-党史党建-理论学习': 'http://www.cies.ac.cn/dsdj/llxx/index.html',
        '首页-党史党建-党史学习': 'http://www.cies.ac.cn/dsdj/dsxx/index.html',
        '首页-党史党建-科学家精神': 'http://www.cies.ac.cn/dsdj/kxjjs/index.html',
        '首页-党史党建-学习资料': 'http://www.cies.ac.cn/dsdj/xxzl/index.html',
        '首页-党史党建-党建专题': 'http://www.cies.ac.cn/dsdj/djzt/index.html',

    }
    for key, value in urls.items():
        news_classify = key
        pageId = get_pageId(value)
        for page in range(1, 50):
            html_text, status_code = get_html(value, page, pageId)
            # print(html_text)
            # breakpoint()
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(0.11)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q2')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-03-01')
    args = parser.parse_args()
    account_name = 'B-58 中国照明学会'
    start_run(args.projectname, args.sinceyear, account_name)
