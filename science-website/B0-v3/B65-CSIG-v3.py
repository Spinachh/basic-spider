# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-07-10

import random
import re
import io
import sys
import time
import urllib3
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content_new
    return collection


def get_html(url):

    response = requests.get(url,  verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="three_list"]//li')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/@title')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/@title')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0].strip()
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'https:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://csig.org.cn' + url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="text-body"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="text-body"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'<span class="news_top_lyname">(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'<span class="news_top_zzname">(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击量：<span id="clicks">(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        # '首页-关于CSIG': 'http://csig.org.cn/list/aboutcsig/1',
        # '首页-学会动态-学会新闻': 'https://www.csig.org.cn/23/index_2.html',
        # '首页-学会动态-活动预告': 'https://www.csig.org.cn/22/index_2.html',
        # '首页-学会动态-资讯分享': 'https://www.csig.org.cn/24/index_2.html',
        # '首页-学会动态-通知公告': 'https://www.csig.org.cn/21/index_2.html',
        '首页-学会动态-行业招聘': 'https://www.csig.org.cn/25/index_2.html',
        '首页-学会动态-CSIG专访': 'https://www.csig.org.cn/159/index_2.html',

        '首页-会员-会员风采': 'http://csig.org.cn/list/huiyuanchengjiu/1',

        # '首页-学会活动-图像图形学术会议': 'http://csig.org.cn/list/txtxxxshy/1',
        # '首页-学会活动-青年科学家论坛': 'http://csig.org.cn/list/qnkxjlt/1',
        # '首页-学会活动-专委学术活动': 'https://www.csig.org.cn/48/index_2.html',
        '首页-学会活动-CSIG中国行': 'https://www.csig.org.cn/42/index_2.html',
        '首页-学会活动-CSIG讲习班': 'https://www.csig.org.cn/43/index_2.html',
        # '首页-学会活动-中国图像图形大会': 'http://csig.org.cn/list/zhongguotuxiangtuxingdahui/1',
        '首页-学会活动-CSIG挑战赛': 'https://www.csig.org.cn/47/index_2.html',
        '首页-学会活动-CSIG企业行': 'https://www.csig.org.cn/45/index_2.html',
        '首页-学会活动-CSIG图像图形高峰论坛': 'https://www.csig.org.cn/44/index_2.html',
        '首页-学会活动-CSIG云讲堂': 'https://www.csig.org.cn/46/index_2.html',

        '首页-科普工作-科普新闻': 'https://www.csig.org.cn/51/index_2.html',
        # '首页-科普工作-科普团队': 'https://www.csig.org.cn/52/index_2.html',
        '首页-科普工作-全国科技周': 'https://www.csig.org.cn/49/index_2.html',
        '首页-科普工作-全国科普日': 'https://www.csig.org.cn/50/index_2.html',

        '首页-奖励与评价-奖励条例': 'https://www.csig.org.cn/58/index_2.html',
        '首页-奖励与评价-奖励动态': 'https://www.csig.org.cn/59/index_2.html',
        '首页-奖励与评价-获奖名单': 'https://www.csig.org.cn/60/index_2.html',
        '首页-奖励与评价-人才托举': 'https://www.csig.org.cn/65/index_2.html',
        '首页-奖励与评价-成果评价': 'https://www.csig.org.cn/66/index_2.html',
        '首页-奖励与评价-CSIG奖励访谈': 'https://www.csig.org.cn/67/index_2.html',
        '首页-党建强会-党建动态': 'https://www.csig.org.cn/26/index_2.html',
        '首页-党建强会-专题学习': 'https://www.csig.org.cn/27/index_2.html',

    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            if page == 1:
                # https://www.csig.org.cn/23/index_2.html
                url = value.replace('index_2', 'index')
            else:
                url = value.replace('index_2', 'index_' + str(page) )
            html_text, status_code = get_html(url)
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(0.4)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q2')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-04-01')
    args = parser.parse_args()
    account_name = 'B-65 中国图象图形学学会'
    start_run(args.projectname, args.sinceyear, account_name)
