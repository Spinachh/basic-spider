# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-05-29

import random
import re
import io
import sys
import time
import json
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="ListArtic"]')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('.//a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('.//p/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('.//dd/text()')[0].strip()
            # if '-' not in news_publish_time:
            #     news_publish_time = news_publish_time.replace('/', '-')
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('.//a/@href')[0]
            if 'https://csgpc.org/' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://csgpc.org' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = result.get('content')
    except:
        news_content = selector_page.xpath('//*[@class="dynamicsCon"]//p/text()')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="ArticleContent"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源:<a href="" target="_blank">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)
    # https://www.china-cic.cn/list/146/19/2/
    urls = {
        '首页-咨询中心-时政要闻': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=142&page=1',
        '首页-咨询中心-自然资源部动态': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=167&page=1',
        '首页-咨询中心-学会动态': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=107&page=1',
        '首页-咨询中心-通知公告': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=108&page=1',
        '首页-咨询中心-地方动态': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=172&page=1',
        '首页-咨询中心-分支机构动态': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=171&page=1',
        '首页-咨询中心-团体会员动态': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=173&page=1',
        '首页-咨询中心-会展资讯': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=141&page=1',
        '首页-咨询中心-每日重点': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=1309&page=1',
        '首页-咨询中心-媒体声音': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=113&page=1',
        '首页-学术交流-国内动态': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=117&page=1',
        '首页-学术交流-国际交流': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=116&page=1',
        '首页-学术交流-港澳台交流': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=168&page=1',
        '首页-测绘智库-智库观点': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=122&page=1',
        '首页-测绘智库-科技年鉴': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=126&page=1',
        '首页-科普天地-科普动态': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=127&page=1',
        '首页-科普天地-定向越野': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=129&page=1',
        '首页-科普天地-红色地图': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=1378&page=1',
        '首页-科技奖励-奖励公报': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=2556&page=1',
        '首页-科技奖励-奖励制度': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=132&page=1',
        '首页-科技奖励-奖励公示': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=2551&page=1',
        '首页-教育认证-工作动态': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=194&page=1',
        '首页-教育认证-认证名单': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=184&page=1',
        '首页-教育认证-办法': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=183&page=1',
        '首页-教育认证-标准': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=182&page=1',
        '首页-科技成果-科技成果发布': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=2482&page=1',
        '首页-科技成果-科技成果展示': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=2481&page=1',
        '首页-科技成果-科技成果推荐': 'https://csgpc.org/addons/cms/archives/ajaxarclistnews.html?id=2480&page=1',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            url = value.replace('page=1', 'page=' + str(page))
            html_text, status_code = get_html(url)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'B-23 中国测绘学会'
    start_run(args.projectname, args.sinceyear, account_name)
