# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-05-24

import random
import re
import io
import sys
import time
import json
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    cookies = {
        'UM_distinctid': '184e1c7ada1351-053930e22c9037-26021151-1fa400-184e1c7ada257f',
        'CNZZDATA1281183464': '1915066091-1670233487-%7C1670233487',
        '_pk_id.1.e342': '7792eed59e48a513.1670235271.',
        '_pk_ses.1.e342': '1',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/108.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="m-newslistl"]/ul/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/h3/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/h3/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./a/div/span[1]/text()')[0]
            if '-' not in news_publish_time:
                news_publish_time = news_publish_time.replace('/', '-')
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'http://www.cis.org.cn/' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.cis.org.cn' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = result.get('content')
    except:
        news_content = selector_page.xpath('//*[@class="dynamicsCon"]//p/text()')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="txt zdetail"]/p/img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'新闻来源：<span>(.*?)</span>', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>阅读：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-最新消息': 'http://www.cis.org.cn/post/index/110?page=1',
        '首页-资讯与活动-工作动态': 'http://www.cis.org.cn/post/index/141?page=1',
        '首页-资讯与活动-行业服务': 'http://www.cis.org.cn/post/index/140?page=1',
        '首页-资讯与活动-政策资讯': 'http://www.cis.org.cn/post/index/120?page=1',
        '首页-资讯与活动-业界新闻': 'http://www.cis.org.cn/post/index/142?page=1',
        '首页-资讯与活动-活动通知': 'http://www.cis.org.cn/post/index/143?page=1',
        '首页-资讯与活动-党建活动': 'http://www.cis.org.cn/post/index/146?page=1',
        '首页-资讯与活动-会员资讯': 'http://www.cis.org.cn/post/index/162?page=1',
        '首页-会员服务-团体标准': 'http://www.cis.org.cn/post/index/116?page=1',
        '首页-会员服务-国家职业标准': 'http://www.cis.org.cn/post/index/165?page=1',
        '首页-会员服务-战略咨询': 'http://www.cis.org.cn/post/index/156?page=1',
        '首页-会员服务-优秀博士论文': 'http://www.cis.org.cn/post/index/118?page=1',
        '首页-会员服务-继续教育': 'http://www.cis.org.cn/post/index/122?page=1',
        '首页-会员服务-奖学金': 'http://www.cis.org.cn/post/index/129?page=1',
        '首页-会员服务-青年人才托举': 'http://www.cis.org.cn/post/index/130?page=1',
        '首页-会员服务-成果转化与鉴定': 'http://www.cis.org.cn/post/index/131?page=1',
        '首页-会员服务-国际注册工程师': 'http://www.cis.org.cn/post/index/163?page=1',
        '首页-会员服务-工程师资格认证': 'http://www.cis.org.cn/post/index/135?page=1',
        '首页-会员服务-工程教育认证': 'http://www.cis.org.cn/post/index/138?page=1',
        '首页-会员服务-会员风采': 'http://www.cis.org.cn/post/index/126?page=1',
        '首页-会员服务-科学家精神': 'http://www.cis.org.cn/post/index/155?page=1',
        '首页-会员服务-技能人才评价': 'http://www.cis.org.cn/post/index/166?page=1',
        '首页-学术交流-学术会议': 'http://www.cis.org.cn/post/index/119?page=1',
        '首页-科学普及-仪器与生活': 'http://www.cis.org.cn/post/index/132?page=1',
        '首页-科学普及-科普知识': 'http://www.cis.org.cn/post/index/133?page=1',
        '首页-科学普及-科普动态': 'http://www.cis.org.cn/post/index/134?page=1',
        '首页-科技咨询-科创中国': 'http://www.cis.org.cn/post/index/145?page=1',
        '首页-国际交流': 'http://www.cis.org.cn/post/index/123?page=1',
        '首页-学会赛事-国家级赛事': 'http://www.cis.org.cn/post/index/168?page=1',
        '首页-学会赛事-行业赛事': 'http://www.cis.org.cn/post/index/117?page=1',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            url = value.replace('page=1', 'page=' + str(page))
            html_text, status_code = get_html(url)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'B-15 中国仪器仪表学会'
    start_run(args.projectname, args.sinceyear, account_name)
