# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-06-09

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import urllib3
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
urllib3.disable_warnings()
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash(url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, )
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url):
    response = requests.get(url, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    try:
        if 'special' not in news_classify:
            selector = etree.HTML(html_text)
            part_nodes1 = selector.xpath('//*[@class="tyList"]//li')
            deadline = xpath_data(part_nodes1, news_classify, science_system, mongo,
                                  account_name, project_time, start_time)
            if deadline:
                return deadline
        else:
            selector = etree.HTML(html_text)
            part1_nodes = selector.xpath('//*[@class="first_list three"]/li')
            deadline = xpath_data(part1_nodes, news_classify, science_system, mongo,
                                  account_name, project_time, start_time)
            if deadline:
                return deadline

    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('./a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except Exception as e:
            logging.warning('News Title Was Error: {}'.format(e))
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = news_title
            else:
                news_abstract = news_title
        except Exception as e:
            logging.warning('News Abstract Was Error: {}'.format(e))
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_text = part_nodes[i].xpath('./span/text()')[0].strip()
                news_publish_time = re.sub(r'[\(,\)]', '', news_publish_text)
            else:
                news_publish_time = part_nodes[i].xpath('.//p/text()')[0]
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2023-12-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('./a/@href')[0].strip()
                else:
                    url_part2 = part_nodes[i].xpath('./a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.tobacco.gov.cn' + url_part2

            if 'mp.weixin.qq' in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_page_url)

            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False,
                                 )
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''
    try:
        news_content = selector_page.xpath('//*[@class="tyContent"]//span//text() | '
                                           '//*[@class="tyContent"]//p//text() | '
                                           '//*[@class="tyContent"]//p//span//text()'
                                           )
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'<span class="AUTHOR">(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="tyContent"]//span//img/@src |'
                                        '//*[@class="tyContent"]//p//img/@src | '
                                        '//*[@class="tyContent"]//div//img/@src |'
                                        '//*[@class="tyContent"]//img/@src')[0]
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="tyContent"]//span//video/@src |'
                                         ' //*[@class="tyContent"]//p//video/@src |'
                                         ' //*[@class="tyContent"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'<span class="source">(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r';阅读数：(.*?)&nbsp', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''
    # print(read_count)
    # breakpoint()

    try:
        click_count = re.findall(r'点击量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-行业资讯-烟草新闻-行业要闻': 'http://www.tobacco.gov.cn/gjyc/hyyw/list_2.shtml',
        '首页-行业资讯-烟草新闻-央媒报道': 'http://www.tobacco.gov.cn/gjyc/ymbd/list_2.shtml',
        '首页-行业资讯-烟草新闻-各地新闻': 'http://www.tobacco.gov.cn/gjyc/gdxw/list_2.shtml',
        '首页-行业资讯-烟草新闻-基层工作': 'http://www.tobacco.gov.cn/gjyc/jcgz/list_2.shtml',
        '首页-行业资讯-乡村振兴-乡村振兴工作信息': 'http://www.tobacco.gov.cn/gjyc/fpgzdt/list_2.shtml',
        '首页-行业资讯-乡村振兴-相关政策': 'http://www.tobacco.gov.cn/gjyc/fpzc/list_2.shtml',
        '首页-行业资讯-专卖管理-专卖管理': 'http://www.tobacco.gov.cn/gjyc/zmgl/list_2.shtml',
        '首页-行业资讯-统计信息-统计信息': 'http://www.tobacco.gov.cn/gjyc/tjxx/list_2.shtml',
        '首页-行业资讯-计划规划-计划规划': 'http://www.tobacco.gov.cn/gjyc/jhgh/list_2.shtml',
        '首页-行业资讯-采购信息-采购信息-sp': 'http://www.tobacco.gov.cn/gjyc/zfcg/list_2.shtml',
        '首页-行业资讯-数字化转型-国家数字化转型': 'http://www.tobacco.gov.cn/gjyc/gjlhrh/lhrhzt_list_2.shtml',
        '首页-行业资讯-数字化转型-烟草行业数字化转型': 'http://www.tobacco.gov.cn/gjyc/hylhrh/lhrhzt_list_2.shtml',
        '首页-行业资讯-人事与培训-人事信息': 'http://www.tobacco.gov.cn/gjyc/rsxx/list_2.shtml',
        '首页-行业资讯-人事与培训-人才招聘平台': 'http://www.tobacco.gov.cn/gjyc/zpxx/list_2.shtml',
        '首页-行业资讯-人事与培训-培训信息': 'http://www.tobacco.gov.cn/gjyc/pxxx/list_2.shtml',
        '首页-行业资讯-人事与培训-职工风采': 'http://www.tobacco.gov.cn/gjyc/zgfc/list_2.shtml',
        '首页-行业资讯-互联网+政务服务-工作信息': 'http://www.tobacco.gov.cn/gjyc/gzdt/list_2.shtml',
        '首页-行业资讯-互联网+政务服务-政府网站工作年报': 'http://www.tobacco.gov.cn/gjyc/wzndbb/list_2.shtml',
        '首页-专题专栏': 'http://www.tobacco.gov.cn/gjyc/hyzl/list_2.shtml',
        '首页-电子烟管理': 'http://www.tobacco.gov.cn/gjyc/dzygl/dzygl_list_2.shtml',
    }

    for key, value in urls.items():
        news_classify = key
        if 'sp' in news_classify:
            target_page = 100
        else:
            target_page = 50

        for page in range(1, target_page):
            if page == 1:
                url = value.replace('_2', '')
            else:
                url = value.replace('_2', '_' + str(page))

            html_text, status_code = get_html(url)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'B-53 中国烟草学会'
    start_run(args.projectname, args.sinceyear, account_name)
