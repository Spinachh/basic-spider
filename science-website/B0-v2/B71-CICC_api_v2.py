# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-06-16

import random
import re
import io
import sys
import time
import json
import argparse
import urllib3
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash(url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, )
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, page, colId):
    data_json = {
        '125': '503', '126': '504', '138': '505', '143': '508',
    }
    cookies = {
        '_cliid': 'TF9Hk1fRZNU0mfXN',
        '_lastEnterDay': '2023-01-18',
        '_siteStatId': 'ee614d99-8d06-4220-a96a-9de56d880472',
        '_siteStatDay': '20230118',
        '_siteStatRedirectUv': 'redirectUv_20551066',
        '_siteStatVisitorType': 'visitorType_20551066',
        '_siteStatVisit': 'visit_20551066',
        'www.c2.org.cn__VSIGN_662': 'AJucnZ4GCgRaY3UyEJDkk-cF',
        '_checkSiteLvBrowser': 'true',
        '_siteStatReVisit': 'reVisit_20551066',
        '_siteStatVisitTime': '1674006049175',
    }
    moduleId = data_json.get(colId)

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'http://www.c2.org.cn',
        'Referer': url,
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Mobile Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    data = {
        'cmd': 'getWafNotCk_getAjaxPageModuleInfo',
        '_colId': colId,
        '_extId': '0',
        'moduleId': moduleId,
        'href': '/col.jsp?m{}pageno={}&id={}'.format(moduleId, page, colId),
        'newNextPage': 'false',
        'needIncToVue': 'false'
    }

    response = requests.post('http://www.c2.org.cn/ajax/ajaxLoadModuleDom_h.jsp', headers=headers,
                             data=data, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get1_html(url):
    cookies = {
        'www.c2.org.cn__VSIGN_662': 'AJm1v6EGCgQ2WnZhEPD6x7wC',
        '_checkSiteLvBrowser': 'true',
        '_cliid': 'Oy60TostkyKLZAhi',
        '_siteStatId': '8854e991-7d83-44f8-9fbd-dae80606760f',
        '_siteStatDay': '20230407',
        '_siteStatVisitorType': 'visitorType_20551066',
        '_siteStatRedirectUv': 'redirectUv_20551066',
        '_siteStatVisit': 'visit_20551066',
        '_reqArgs': '',
        '_lastEnterDay': '2023-04-07',
        '_siteStatReVisit': 'reVisit_20551066',
        '_siteStatVisitTime': '1680858210353',
        'td_cookie': get_supflash(url),
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(page, html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    if page == 1:
        try:
            selector = etree.HTML(html_text)
            part_nodes1 = selector.xpath('//*[@id="newsList503"]//tr')
            # print(part_nodes1)
            # breakpoint()
            deadline = xpath1_data(part_nodes1, news_classify, science_system, mongo,
                                   account_name, project_time, start_time)
            if deadline:
                return deadline
        except Exception as e:
            logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))
    else:
        content = json.loads(html_text)
        try:
            deadline = xpath_data(content, news_classify, science_system, mongo,
                                  account_name, project_time, start_time)
            if deadline:
                return deadline
        except Exception as e:
            logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def xpath_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    results = content['domStr']
    # results = re.findall(r'target=_blank title="(.*?)</1', results, re.M | re.S)
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    selector = etree.HTML(results)
    part_nodes = selector.xpath('//*[@id="newsList503"]/div//tr//a/text()')
    article_url = selector.xpath('//*[@id="newsList503"]/div//tr/td[2]/a/@href')
    # print(part_nodes)
    # breakpoint()
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    # print(results)
    # breakpoint()
    for i in range(1, len(part_nodes)):
        try:
            news_title = part_nodes[i]
        except:
            news_title = ''
        # print(news_title)
        # breakpoint()
        try:
            news_abstract = part_nodes[i]
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i + 1]
        except:
            news_publish_time = '2022-07-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_publish_time)
        # print(news_publish_stamp)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = article_url[i]
            if 'https:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.c2.org.cn' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def xpath1_data(part_nodes, news_classify, science_system, mongo,
                account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('./td[2]/a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except Exception as e:
            logging.warning('News Title Was Error: {}'.format(e))
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = news_title
            else:
                news_abstract = news_title
        except Exception as e:
            logging.warning('News Abstract Was Error: {}'.format(e))
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_time = part_nodes[i].xpath('./td[3]/a/text()')[0].strip()
            else:
                news_publish_time = part_nodes[i].xpath('.//p/text()')[0]
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2023-12-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('./td[2]/a/@href')[0].strip()
                else:
                    url_part2 = part_nodes[i].xpath('./a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.cahe.edu.cn' + url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url, )

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    cookies = {
        '_cliid': 'TF9Hk1fRZNU0mfXN',
        '_siteStatRedirectUv': 'redirectUv_20551066',
        '_siteStatVisitorType': 'visitorType_20551066',
        'www.c2.org.cn__VSIGN_662': 'AJucnZ4GCgRaY3UyEJDkk-cF',
        '_checkSiteLvBrowser': 'true',
        '_lastEnterDay': '2023-01-19',
        '_siteStatId': '2bf10adb-35c9-4db2-93fb-952ef3fb8ac3',
        '_siteStatDay': '20230119',
        '_siteStatVisit': 'visit_20551066',
        '_siteStatReVisit': 'reVisit_20551066',
        'td_cookie': '4021550602',
        '_siteStatVisitTime': '1674092337723',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Referer': news_page_url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Mobile Safari/537.36',
    }

    html_response = requests.get(news_page_url, headers=headers, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    try:
        news_content = selector_page.xpath('//*[@class="richContent  richContent0"]//p//text()')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="richContent  richContent0"]//p//img/@src |'
                                        '//*[@class="richContent  richContent0"]//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'阅读量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学会动态-学会新闻': 'http://www.c2.org.cn/h-col-125.html',
        '首页-学会动态-通知公告': 'http://www.c2.org.cn/h-col-126.html',
        '首页-学会动态-活动预告': 'http://www.c2.org.cn/h-col-128.html',
        '首页-党建强会-党建动态': 'http://www.c2.org.cn/h-col-138.html',
        '首页-科普工作-前沿科技': 'http://www.c2.org.cn/h-col-143.html',
    }
    for key, value in urls.items():
        news_classify = key
        id = re.findall(r'h-col-(.*?).html', value)[0]
        for page in range(1, 50):
            if page == 1:
                html_text, status_code = get1_html(value)

            else:
                html_text, status_code = get_html(value, page, id)

            if status_code == 404:
                break
            deadline = get_data(page, html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'B-71 中国指挥与控制学会'
    start_run(args.projectname, args.sinceyear, account_name)
