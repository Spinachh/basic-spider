# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-06-12

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(page, id):
    headers = {
        'authority': 'api.cntv.cn',
        'accept': '*/*',
        'accept-language': 'zh-CN,zh;q=0.9',
        'referer': 'https://www.csmpte.com/',
        'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
        'sec-ch-ua-mobile': '?1',
        'sec-ch-ua-platform': '"Android"',
        'sec-fetch-dest': 'script',
        'sec-fetch-mode': 'no-cors',
        'sec-fetch-site': 'cross-site',
        'user-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/108.0.0.0 Mobile Safari/537.36',
    }

    params = (
        ('serviceId', 'csmpte'),
        ('id', id),
        ('callback', 'getlistdata'),
        ('n', '10'),
        ('p', page),
        ('t', 'jsonp'),
        ('cb', 'getlistdata'),
        ('sortfield', '1'),
        ('kind', 'a'),
    )

    response = requests.get('https://api.cntv.cn/List/getListByPageid', headers=headers, params=params)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    content = re.findall(r'getlistdata\((.*?)\);', html_text)[0]
    content = json.loads(content)
    try:
        deadline = ree_data(content, news_classify, science_system, mongo,
                            account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
             account_name, project_time, start_time):
    results = content['data']['list']
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for item in results:
        try:
            news_title = item.get('title')
        except:
            news_title = ''
        try:
            news_abstract = item.get('desc')
        except:
            news_abstract = ''

        try:
            news_publish_stamp = item.get('focus_date')[:10]
            news_publish_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(news_publish_stamp)))
        except:
            news_publish_stamp = item.get('focus_date')[:10]
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_publish_time)
        # print(news_publish_stamp)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            news_page_url = item.get('url')
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    try:
        news_content = selector_page.xpath('//*[@class="content_area"]//p/text()')
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="content_area"]//p//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'阅读量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学会活动-学会动态': 'https://api.cntv.cn/List/getListByPageid?serviceId=csmpte&id=PAGEnDIder23Hw7AWfK0hbje211206&callback=getlistdata&n=10&p=1&t=jsonp&cb=getlistdata&sortfield=1&kind=a',
        '首页-学会活动-通知公告': 'https://api.cntv.cn/List/getListByPageid?serviceId=csmpte&id=PAGEpRODWtCzWUPvFrlHAA1r211208&callback=getlistdata&n=10&p=1&t=jsonp&cb=getlistdata&sortfield=1&kind=a',
        '首页-学会活动-分支机构': 'https://api.cntv.cn/List/getListByPageid?serviceId=csmpte&id=PAGEM9mIkJBERzskNDImFe7X211208&callback=getlistdata&n=10&p=1&t=jsonp&cb=getlistdata&sortfield=1&kind=a',
        '首页-学会活动-专题活动': 'https://api.cntv.cn/List/getListByPageid?serviceId=csmpte&id=PAGE3iUZRduGlYE7kSsnCD0W211208&callback=getlistdata&n=10&p=1&t=jsonp&cb=getlistdata&sortfield=1&kind=a',
        '首页-奖项评价-奖项公告': 'https://api.cntv.cn/List/getListByPageid?serviceId=csmpte&id=PAGEMlWxA0qzqahAljI7xuzB211206&callback=getlistdata&n=10&p=1&t=jsonp&cb=getlistdata&sortfield=1&kind=a',
        '首页-奖项评价-政策条例': 'https://api.cntv.cn/List/getListByPageid?serviceId=csmpte&id=PAGEk4c6j8pRNergxb3eBYsD211208&callback=getlistdata&n=10&p=1&t=jsonp&cb=getlistdata&sortfield=1&kind=a',
        '首页-奖项评价-申报与查询': 'https://api.cntv.cn/List/getListByPageid?serviceId=csmpte&id=PAGE68woJMettiFMugoQFlr1211208&callback=getlistdata&n=10&p=1&t=jsonp&cb=getlistdata&sortfield=1&kind=a',
        '首页-学术交流-培训交流': 'https://api.cntv.cn/List/getListByPageid?serviceId=csmpte&id=PAGE8NDKDpeELxOdUAu5rhfd211208&callback=getlistdata&n=10&p=1&t=jsonp&cb=getlistdata&sortfield=1&kind=a',
        '首页-学术交流-专题研讨': 'https://api.cntv.cn/List/getListByPageid?serviceId=csmpte&id=PAGE88FiqL3tPsIkpkDPmJT5211208&callback=getlistdata&n=10&p=1&t=jsonp&cb=getlistdata&sortfield=1&kind=a',
        '首页-学术交流-科协普及': 'https://api.cntv.cn/List/getListByPageid?serviceId=csmpte&id=PAGExuuf2v0RDDV88nwDX3Jk211208&callback=getlistdata&n=10&p=1&t=jsonp&cb=getlistdata&sortfield=1&kind=a',
        '首页-学术交流-标准规则': 'https://api.cntv.cn/List/getListByPageid?serviceId=csmpte&id=PAGEBIKMv9BBNkTa7mgO02Ld211208&callback=getlistdata&n=10&p=1&t=jsonp&cb=getlistdata&sortfield=1&kind=a',
        '首页-党建专区-党建要闻': 'https://api.cntv.cn/List/getListByPageid?serviceId=csmpte&id=PAGEdGxEpFQLebs7yPnIfGLt211208&callback=getlistdata&n=10&p=1&t=jsonp&cb=getlistdata&sortfield=1&kind=a',
        '首页-党建专区-学会党建': 'https://api.cntv.cn/List/getListByPageid?serviceId=csmpte&id=PAGEKK93wzqiciIlgN1vwDKq211208&callback=getlistdata&n=10&p=1&t=jsonp&cb=getlistdata&sortfield=1&kind=a',
    }
    for key, value in urls.items():
        news_classify = key
        id = re.findall(r'id=(.*?)&callback', value)[0]
        for page in range(1, 50):
            html_text, status_code = get_html(page, id)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'B-55 中国电影电视技术学会'
    start_run(args.projectname, args.sinceyear, account_name)
