# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-06-08

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="cols-1"]/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''
        # print(news_title)
        # breakpoint()
        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0].replace('/', '-')
        except:
            news_publish_time = '2022-01-01'

        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'cateid/' in url_part2:
                break
            elif 'http://' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.c-mrs.org.cn/' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadlien = True
            return deadlien


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    # print(result)
    # breakpoint()
    try:
        news_content = selector_page.xpath('//*[@class="article"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="article"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)<', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-学会动态-头版新闻': 'http://www.c-mrs.org.cn/c113?page=1',
        '首页-学会动态-通知公告': 'http://www.c-mrs.org.cn/c114?page=1',
        '首页-党建强会-党建动态': 'http://www.c-mrs.org.cn/c107?page=1',
        '首页-党建强会-科学家精神': 'http://www.c-mrs.org.cn/c108?page=1',
        '首页-党建强会-党建活动': 'http://www.c-mrs.org.cn/c109?page=1',
        '首页-党建强会-党史学习': 'http://www.c-mrs.org.cn/c110?page=1',
        '首页-党建强会-党建专题': 'http://www.c-mrs.org.cn/c111?page=1',
        '首页-品牌会议-中国材料大全': 'http://www.c-mrs.org.cn/c128?page=1',
        '首页-品牌会议-新材料产业发展大会': 'http://www.c-mrs.org.cn/c124?page=1',
        '首页-品牌会议-中国新材料技术应用大会': 'http://www.c-mrs.org.cn/c125?page=1',
        '首页-品牌会议-中国新材料投资大会': 'http://www.c-mrs.org.cn/c126?page=1',
        '首页-品牌会议-世界材料大会': 'http://www.c-mrs.org.cn/c127?page=1',
        '首页-品牌会议-会议动态': 'http://www.c-mrs.org.cn/c152?page=1',
        '首页-战略研究院-高端智库': 'http://www.c-mrs.org.cn/c120?page=1',
        '首页-战略研究院-产业咨询': 'http://www.c-mrs.org.cn/c121?page=1',
        '首页-战略研究院-材料强会论坛': 'http://www.c-mrs.org.cn/c148?page=1',
        '首页-产业服务-技术对接': 'http://www.c-mrs.org.cn/c144?page=1',
        '首页-产业服务-科创中国': 'http://www.c-mrs.org.cn/c145?page=1',
        '首页-科学普及-科普动态': 'http://www.c-mrs.org.cn/c130?page=1',
        '首页-科学普及-科普出版': 'http://www.c-mrs.org.cn/c131?page=1',
        '首页-科学普及-科普视频': 'http://www.c-mrs.org.cn/c132?page=1',
        '首页-期刊出版': 'http://www.c-mrs.org.cn/c151?page=1',
        '首页-荣誉奖励': 'http://www.c-mrs.org.cn/c154?page=1',
        '首页-会员服务': 'http://www.c-mrs.org.cn/c133?page=1',


    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            url = value.replace('page=1', 'page=' + str(page))
            html_text, status_code = get_html(url)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'B-49 中国材料研究学会'
    start_run(args.projectname, args.sinceyear, account_name)
