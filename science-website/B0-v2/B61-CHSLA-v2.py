# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-06-13

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url,  verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="listul"]/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                   mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0].strip().replace('/', '-')
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'https:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.chsla.org.cn' + url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url,verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="article_con"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="article_con"]//p//img/@src | '
                                        '//*[@class="article_con"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'发布： (.*?) ', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击量：<span id="clicks">(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-学会活动-通知': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=793&_MID=1100022',
        '首页-学会活动-学会工作': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=598&_MID=1100028',
        '首页-学会活动-表彰评优': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=184&_MID=1100025',
        '首页-学会活动-分支机构': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=79&_MID=1100027',
        '首页-国际交往-IFLA': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=28&_MID=1100042',
        '首页-编辑出版-专业书籍': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=47&_MID=1100104',
        '首页-编辑出版-中国园林': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=319&_MID=1100101',
        '首页-编辑出版-学会简讯': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=59&_MID=1500013',
        '首页-法律规范-法律': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=64&_MID=1200009',
        '首页-法律规范-法规': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=401&_MID=1200010',
        '首页-法律规范-规章': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=523&_MID=1200011',
        '首页-法律规范-规范性文件': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=524&_MID=1200012',
        '首页-法律规范-国家标准': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=105&_MID=1200015',
        '首页-法律规范-地方标准': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=84&_MID=1200016',
        '首页-法律规范-其他标准规范': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=38&_MID=1400011',
        '首页-法律规范-其他文件': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=85&_MID=1200013',
        '首页-园林教育-学生园地': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=85&_MID=1200079',
        '首页-园林教育-高校教育': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=90&_MID=1400015',
        '首页-园林百科-园林工程': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=668&_MID=1200024',
        '首页-园林百科-建筑小品': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=362&_MID=1500004',
        '首页-园林百科-园林理论': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=168&_MID=1200083',
        '首页-园林百科-附属绿化': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=970&_MID=1200060',
        '首页-园林百科-盆景赏石': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=274&_MID=1500007',
        '首页-政策文件': 'http://www.chsla.org.cn/Column/List?page=1&pageCount=122&_MID=1500014',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):

            url = value.replace('page=1', 'page=' + str(page))
            html_text, status_code = get_html(url)
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'B-61 中国风景园林学会'
    start_run(args.projectname, args.sinceyear, account_name)
