# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-05-24

import random
import re
import io
import sys
import time
import json
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    cookies = {
        'PHPSESSID': 'b6aqcho7tjt1ji1icm9qihndpe',
        'Hm_lvt_c8dc8e09842fb88a290e20567c648c78': '1670151916',
        'Hm_lpvt_c8dc8e09842fb88a290e20567c648c78': '1670152006',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'none',
        'Sec-Fetch-User': '?1',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
        'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)

    response.encoding = 'utf-8'
    status_code = response.status_code
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="nei_newslist"]/ul/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a//h2/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a//h2/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time1 = part_nodes[i].xpath('./a/div[1]/p/text()')[0]\
                .replace('年', '-').replace('月', '-')
            news_publish_time2 = part_nodes[i].xpath('./a/div[1]/b/text()')[0]
            news_publish_time = news_publish_time1 + news_publish_time2
            if '-' not in news_publish_time:
                news_publish_time = news_publish_time.replace('/', '-')
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'https://www.caa.org.cn/' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.caa.org.cn' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} ='
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = result.get('content')
    except:
        news_content = selector_page.xpath('//*[@class="dynamicsCon"]//p/text()')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="nei_newsxq"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'新闻来源：<span>(.*?)</span>', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>阅读：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-学会动态-重要通知': 'http://www.caa.org.cn/Lists/192_1.html',
        '首页-学会动态-活动预告': 'http://www.caa.org.cn/Lists/15_1.html',
        '首页-学会动态-学会新闻': 'http://www.caa.org.cn/Lists/191_1.html',
        '首页-学会动态-工作会议': 'http://www.caa.org.cn/Lists/200_1.html',
        '首页-学会动态-口述历史': 'http://www.caa.org.cn/Lists/199_1.html',
        '首页-学会动态-党建强会': 'http://www.caa.org.cn/Lists/208_1.html',
        '首页-学会动态-科创中国': 'http://www.caa.org.cn/Lists/17_1.html',
        '首页-学会动态-形式通报': 'http://www.caa.org.cn/Lists/207_1.html',
        '首页-学会动态-分支机构': 'http://www.caa.org.cn/Lists/201_1.html',
        '首页-学会动态-省级学会': 'http://www.caa.org.cn/Lists/203_1.html',
        '首页-学会动态-青年人才托举': 'http://www.caa.org.cn/Lists/198_1.html',
        '首页-学会动态-行业招聘': 'http://www.caa.org.cn/Lists/19_1.html',
        '首页-学会动态-党的二十大精神': 'http://www.caa.org.cn/Lists/308_1.html',
        '首页-CAA科普-科普新闻': 'http://www.caa.org.cn/Lists/51_1.html',
        '首页-CAA科普-科普知识': 'http://www.caa.org.cn/Lists/52_1.html',
        '首页-国际合作-新闻动态': 'http://www.caa.org.cn/Lists/251_1.html',
        '首页-会员专区-会员活动': 'http://www.caa.org.cn/Lists/189_1.html',
        '首页-会员专区-会员成就': 'http://www.caa.org.cn/Lists/194_1.html',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            url = value.replace('1.html', str(page) + '.html')
            html_text, status_code = get_html(url)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'B-14 中国自动化学会'
    start_run(args.projectname, args.sinceyear, account_name)
