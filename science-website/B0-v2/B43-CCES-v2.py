# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-06-07

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):
    headers = {
        # 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        # 'Cache-Control': 'max-age=0',
        # 'Connection': 'keep-alive',
        'Referer': url,
        # 'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Mobile Safari/537.36',
    }

    response = requests.get(url, headers=headers, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="list0"]/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''
        # print(news_title)
        # breakpoint()
        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./a/span/text()')[0]
            news_publish_time = re.sub(r'[\[,\]]', '', news_publish_time)
            # print(news_publish_time)
        except:
            news_publish_time = '2022-01-01'
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'cateid/' in url_part2:
                break
            elif 'http://' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://cces.net.cn' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)

    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    # print(result)
    # breakpoint()
    try:
        news_content = selector_page.xpath('//*[@class="article-content"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="article-content"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)<', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-新闻中心-行业要闻': 'http://cces.net.cn/html/tm/12/18/18_1.html',
        '首页-新闻中心-学会要闻': 'http://cces.net.cn/html/tm/12/17/17_1.html',
        '首页-新闻中心-通知公告': 'http://cces.net.cn/html/tm/12/13/13_1.html',
        '首页-新闻中心-图片新闻': 'http://cces.net.cn/html/tm/12/21/21_1.html',
        '首页-学术活动-学术会议': 'http://cces.net.cn/html/tm/19/22/22_1.html',
        '首页-学术活动-课题研究': 'http://cces.net.cn/html/tm/19/23/23_1.html',
        '首页-学术活动-计划与总结': 'http://cces.net.cn/html/tm/19/24/24_1.html',
        '首页-学术活动-专家讲座': 'http://cces.net.cn/html/tm/19/25/25_1.html',
        '首页-学术活动-学术成果': 'http://cces.net.cn/html/tm/19/26/26_1.html',
        '首页-学术活动-学术动态': 'http://cces.net.cn/html/tm/19/27/27_1.html',
        '首页-国际交流-国际学术组织': 'http://cces.net.cn/html/tm/28/34/34_1.html',
        '首页-国际交流-国际学术组织-组织介绍': 'http://cces.net.cn/html/tm/28/34/63/63_1.html',
        '首页-国际交流-国际学术组织-多边事务': 'http://cces.net.cn/html/tm/28/34/64/64_1.html',
        '首页-国际交流-国际学术组织-学术活动': 'http://cces.net.cn/html/tm/28/34/88/88_1.html',
        '首页-表彰奖励-詹天佑大奖': 'http://cces.net.cn/html/tm/29/38/38_1.html',
        '首页-标准规范-标准动态': 'http://cces.net.cn/html/tm/98/598/598_1.html',
        '首页-标准规范-申报与立项': 'http://cces.net.cn/html/tm/98/99/99_1.html',
        '首页-标准规范-编制进度': 'http://cces.net.cn/html/tm/98/101/101_1.html',
        '首页-标准规范-标准发布': 'http://cces.net.cn/html/tm/98/102/102_1.html',
        '首页-科普与培训-科协普及': 'http://cces.net.cn/html/tm/31/49/49_1.html',
        '首页-科普与培训-教育培训': 'http://cces.net.cn/html/tm/31/50/50_1.html',
        '首页-科普与培训-专题讲座': 'http://cces.net.cn/html/tm/31/51/51_1.html',
        '首页-科普与培训-教育园地': 'http://cces.net.cn/html/tm/31/52/52_1.html',
        '首页-编辑出版-学会期刊': 'http://cces.net.cn/html/tm/32/53/53_1.html',
        '首页-编辑出版-图书出版': 'http://cces.net.cn/html/tm/32/54/54_1.html',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            if page == 1:
                url = value.replace('_1.html', '.html')
            else:
                url = value.replace('_1.html', '_' + str(page) + '.html')
            html_text, status_code = get_html(url)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'B-43 中国土木工程学会'
    start_run(args.projectname, args.sinceyear, account_name)
