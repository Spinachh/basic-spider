# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-05-26

import random
import re
import io
import sys
import time
import json
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    headers = {
        'authority': 'www.china-cic.cn',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'accept-language': 'zh-CN,zh;q=0.9',
        'referer': 'https://www.china-cic.cn/list/146/19/2/',
        'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/108.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="newlist"]/ul/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./div/a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./div/a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./div/div/text()')[0]
            # if '-' not in news_publish_time:
            #     news_publish_time = news_publish_time.replace('/', '-')
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./div/a/@href')[0]
            if 'https://www.china-cic.cn/' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.china-cic.cn' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} '
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = result.get('content')
    except:
        news_content = selector_page.xpath('//*[@class="dynamicsCon"]//p/text()')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="newcon"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'>来源：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        article_id = news_page_url.split('/')[-1].split('.')[0]
        read_url = 'https://www.ccf.org.cn/ccf/counter?Type=Article' \
                   '&ID={}&DomID=hitcount{}'.format(article_id, article_id)
        read_content = requests.get(read_url).text
        read_count = re.findall(r'innerHTML=(.*?);', read_content, re.M | re.S)[0]
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)
    # https://www.china-cic.cn/list/146/19/2/
    urls = {
        '首页-党建工作-重要讲话': 'https://www.china-cic.cn/list/146/19',
        '首页-党建工作-党建动态': 'https://www.china-cic.cn/list/28/19',
        '首页-党建工作-主题教育活动': 'https://www.china-cic.cn/list/27/19',
        '首页-党建工作-制度法规': 'https://www.china-cic.cn/list/25/19',
        '首页-党建工作-党史学习教育': 'https://www.china-cic.cn/list/205/19',
        '首页-党建工作-党的二十大精神': 'https://www.china-cic.cn/list/215/19',
        '首页-组织建设-会员代表大会': 'https://www.china-cic.cn/list/34/12',
        '首页-组织建设-理事会': 'https://www.china-cic.cn/list/33/12',
        '首页-学术工作-学术动态': 'https://www.china-cic.cn/list/64/13',
        '首页-学术工作-专委会会议新闻': 'https://www.china-cic.cn/list/65/13',
        '首页-学术工作-专委会会议通知/征文': 'https://www.china-cic.cn/list/65/13',
        '首页-学术工作-征文投稿平台': 'https://www.china-cic.cn/list/179/13',
        '首页-科普教育-科普原创': 'https://www.china-cic.cn/list/208/15',
        '首页-科普教育-会展专栏': 'https://www.china-cic.cn/list/217/15',
        '首页-科普教育-5G科普行': 'https://www.china-cic.cn/list/44/15',
        '首页-科普教育-文件下载': 'https://www.china-cic.cn/list/43/15',
        '首页-咨询服务-研究动态': 'https://www.china-cic.cn/list/48/22',
        '首页-咨询服务-咨询建议': 'https://www.china-cic.cn/list/47/22',
        '首页-咨询服务-研究报告': 'https://www.china-cic.cn/list/15/22',
        '首页-咨询服务-项目管理': 'https://www.china-cic.cn/list/68/22',
        '首页-科技奖励-奖励动态': 'https://www.china-cic.cn/list/13/20',
        '首页-科技奖励-获奖白名单': 'https://www.china-cic.cn/list/51/20',
        '首页-科技奖励-国家科技奖励': 'https://www.china-cic.cn/list/52/20',
        '首页-科技奖励-奖励申报': 'https://www.china-cic.cn/list/53/20',
        '首页-科技奖励-下载中心': 'https://www.china-cic.cn/list/54/20',
        '首页-会员之窗-通知公告': 'https://www.china-cic.cn/list/69/25',
        '首页-会员之窗-人才举荐': 'https://www.china-cic.cn/list/70/25',
        '首页-会员之窗-管理文件': 'https://www.china-cic.cn/list/71/25',
        '首页-会员之窗-单位会员': 'https://www.china-cic.cn/list/72/25',
        '首页-会员之窗-个人会员': 'https://www.china-cic.cn/list/73/25',
        '首页-能力评价-工程能力评价': 'https://www.china-cic.cn/list/209/50',
        '首页-能力评价-工程师国际认证': 'https://www.china-cic.cn/list/210/50',
        '首页-能力评价-IET国际注册工程师': 'https://www.china-cic.cn/list/211/50',
        '首页-能力评价-产教协同育人': 'https://www.china-cic.cn/list/212/50',
        '首页-能力评价-持续职业发展': 'https://www.china-cic.cn/list/213/50',
        '首页-通知公告': 'https://www.china-cic.cn/list/67/24',
        '首页-学会新闻': 'https://www.china-cic.cn/list/60/24',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            url = value + '/' + str(page) + '/'
            html_text, status_code = get_html(url)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'B-21 中国通信学会'
    start_run(args.projectname, args.sinceyear, account_name)
