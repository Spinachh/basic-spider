# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-06-14

import random
import re
import io
import sys
import time
import argparse
import urllib3
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, page, id):

    cookies = {
        'PHPSESSID': '07j050m3jepj6gs7ogqv7nubo2',
        'UM_distinctid': '185bd73b24546e-01a8d23f9c444-9196f2c-1fa400-185bd73b246973',
        'CNZZDATA1273359029': '2087355042-1673917348-%7C1673917348',
        'Hm_lvt_f1b9aef44b8656063724e8564c0924d0': '1673920755',
        'td_cookie': '3849993426',
        'Hm_lpvt_f1b9aef44b8656063724e8564c0924d0': '1673920784',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Origin': 'http://caai.cn',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/109.0.0.0 Mobile Safari/537.36',
    }

    params = (
        ('s', '/home/article/index.html'),
    )

    data = {
        'p': page,
        'tagid': '0',
        'id': id,
    }

    response = requests.post('http://caai.cn/index.php',
                             headers=headers, params=params,
                             cookies=cookies, data=data,
                             verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="article-list contentDisplay"]/ul/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./h3/a/@title')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./h3/a/@title')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./h4/text()')[0].strip()
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./h3/a/@href')[0]
            if 'https:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://caai.cn' + url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="articleContent"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="articleContent"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'<span class="news_top_lyname">(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'<span class="news_top_zzname">(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击量：<span id="clicks">(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学会动态-学会新闻': 'http://caai.cn/index.php?s=/home/article/index/id/46.html',
        '首页-学会动态-通知公告': 'https://caai.cn/index.php?s=/home/article/index/id/47.html',
        '首页-学会动态-活动预告': 'https://caai.cn/index.php?s=/home/article/index/id/49.html',
        '首页-学会动态-对外合作': 'https://caai.cn/index.php?s=/home/article/index/id/71.html',
        '首页-关于CAAI-条例与法规': 'https://caai.cn/index.php?s=/home/article/index/id/40.html',
        '首页-关于CAAI-分支机构': 'https://caai.cn/index.php?s=/home/article/index/id/43.html',
        '首页-关于CAAI-学会党建': 'https://caai.cn/index.php?s=/home/article/index/id/90.html',
        '首页-关于CAAI-文档下载': 'https://caai.cn/index.php?s=/home/article/index/id/77.html',
        '首页-党建强会-党建强会': 'https://caai.cn/index.php?s=/home/article/index/id/79.html',
        '首页-党建强会-党史学习': 'https://caai.cn/index.php?s=/home/article/index/id/115.html',
        '首页-奖励与合作-吴文俊奖': 'https://caai.cn/index.php?s=/home/article/index/id/64.html',
        '首页-奖励与合作-青托计划': 'https://caai.cn/index.php?s=/home/article/index/id/86.html',
        '首页-奖励与合作-院士推荐': 'https://caai.cn/index.php?s=/home/article/index/id/67.html',
        '首页-奖励与合作-成功鉴定': 'https://caai.cn/index.php?s=/home/article/index/id/91.html',
        '首页-奖励与合作-基金项目': 'https://caai.cn/index.php?s=/home/article/index/id/143.html',
        '首页-CAAI资源-学会期刊': 'https://caai.cn/index.php?s=/home/article/index/id/50.html',
        '首页-CAAI资源-学科皮书系列': 'https://caai.cn/index.php?s=/home/article/index/id/53.html',
        '首页-CAAI资源-年鉴及发展报告': 'https://caai.cn/index.php?s=/home/article/index/id/54.html',
        '首页-会员专区-学会会士': 'https://caai.cn/index.php?s=/home/article/index/id/69.html',
        '首页-会员专区-高级会员': 'https://caai.cn/index.php?s=/home/article/index/id/135.html',
        '首页-会员专区-会员荣誉': 'https://caai.cn/index.php?s=/home/article/index/id/83.html',
        '首页-会员专区-常务理事单位会员': 'https://caai.cn/index.php?s=/home/article/index/id/144.html',
        '首页-会员专区-普通单位会员': 'https://caai.cn/index.php?s=/home/article/index/id/74.html',

    }

    for key, value in urls.items():
        news_classify = key
        id = re.findall(r'id/(.*?).html', value)[0]
        for page in range(1, 50):
            html_text, status_code = get_html(value, page, id)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break

            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'B-66 中国人工智能学会'
    start_run(args.projectname, args.sinceyear, account_name)
