# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-06-19

import random
import re
import io
import sys
import time
import urllib3
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash(url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }
    response = requests.get(url, headers=headers, )
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, page):

    rid = re.findall(r'id=(\d+)', url)[0]

    cookies = {
        'td_cookie': get_supflash(url),
        'ASPSESSIONIDQQDDQDCC': 'KBLEBLKBAIHMGOLKGPPBCPAG',
        'Hm_lvt_c1135323ad254a5450859d837cfb14c9': '1679018683',
        'Hm_lpvt_c1135323ad254a5450859d837cfb14c9': '1679018801',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/111.0.0.0 Safari/537.36',
    }

    params = (
        ('CurPage', page),
        ('rid', rid),
    )

    response = requests.get('http://www.csbm.org.cn/article/index.asp',
                            headers=headers, params=params, cookies=cookies,
                            verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@id="contenttext"]//li')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('.//a/text()')[0].strip()
        except:
            news_title = ''
        # print(news_title)
        # breakpoint()
        try:
            news_abstract = news_title
        except:
            news_abstract = ''

        try:
            news_publish_text = part_nodes[i].xpath('.//small/text()')[0]
            news_publish_time = re.sub(r'[\], \[]', '', news_publish_text)
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('.//a/@href')[0]
            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.csbm.org.cn/article/' + url_part2

            if 'mp.weixin.qq.com' in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_page_url)

            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    id = re.findall(r'id=(\d+)', news_page_url)[0]
    cookies = {
        'td_cookie': get_supflash(news_page_url),
        'ASPSESSIONIDQQDDQDCC': 'KBLEBLKBAIHMGOLKGPPBCPAG',
        'Hm_lvt_c1135323ad254a5450859d837cfb14c9': '1679018683',
        'Hm_lpvt_c1135323ad254a5450859d837cfb14c9': '1679020064',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/111.0.0.0 Safari/537.36',
    }

    params = (
        ('id', ),
    )

    html_response = requests.get('http://www.csbm.org.cn/article/articleinfo.asp',
                            headers=headers, params=params,
                            cookies=cookies, verify=False)

    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="row"]//span//text() | '
                                           '//*[@class="row"]//p//text() | '
                                           '//*[@class="row"]//p//span//text()'
                                           )

        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="row"]//span//img/@src |'
                                        '//*[@class="row"]//p//img/@src | '
                                        '//*[@class="row"]//div//img/@src |'
                                        '//*[@class="row"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)<', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_url = 'http://www.csbm.org.cn/article/count.asp?id={}'.format(id)
        read_content = requests.get(read_url).text

        read_count = re.findall(r'(\d+)', read_content)[0]
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*//p/text() | '
                                           '//*//span/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '主页-学会动态': 'http://www.csbm.org.cn/article/index.asp?CurPage=1&rid=18',
        '主页-学术交流-会议通知': 'http://www.csbm.org.cn/article/index.asp?CurPage=1&caid=44',
        '主页-学术交流-会议活动': 'http://www.csbm.org.cn/article/index.asp?CurPage=1&caid=19',
        '主页-学术交流-学术活动计划': 'http://www.csbm.org.cn/article/index.asp?CurPage=1&caid=20',
        '主页-学术交流-继续教育': 'http://www.csbm.org.cn/article/index.asp?CurPage=1&caid=21',
        '主页-科学普及-科普活动': 'http://www.csbm.org.cn/article/index.asp?CurPage=1&caid=25',
        '主页-科学普及-科普园地': 'http://www.csbm.org.cn/article/index.asp?CurPage=1&caid=26',
        '主页-科学普及-科普防疫': 'http://www.csbm.org.cn/article/index.asp?CurPage=1&caid=56',
        '主页-学会奖励-科学技术奖': 'http://www.csbm.org.cn/article/index.asp?CurPage=1&caid=27',
        '主页-学会党建-思想聚焦': 'http://www.csbm.org.cn/article/index.asp?CurPage=1&caid=38',
        '主页-学会党建-科学道德和学风建设': 'http://www.csbm.org.cn/article/index.asp?CurPage=1&caid=49',
        '主页-招贤纳士-高校招聘': 'http://www.csbm.org.cn/article/index.asp?CurPage=1&caid=53',
        '主页-招贤纳士-科研机构招聘': 'http://www.csbm.org.cn/article/index.asp?CurPage=1&caid=54',
        '主页-招贤纳士-企事业招聘': 'http://www.csbm.org.cn/article/index.asp?CurPage=1&caid=55',
        '主页-下载中心-团体标准': 'http://www.csbm.org.cn/article/index.asp?CurPage=1&caid=6',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            url = value.replace('CurPage=1', 'CurPage=' + str(page))
            html_text, status_code = get_html(url, page)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'B-77W 中国生物材料学会'
    start_run(args.projectname, args.sinceyear, account_name)
