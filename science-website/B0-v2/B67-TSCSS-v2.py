# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-06-15

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, id, pageIndex):

    cookies = {
        'JSESSIONID': '89114ACD4CE7173BFAD3AECFF9485598',
        'td_cookie': '3851573566',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/109.0.0.0 Mobile Safari/537.36',
    }

    params = (
        ('id', id),
        ('pageIndex', pageIndex),
    )

    response = requests.get(url, headers=headers, params=params, cookies=cookies,
                            verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="news_c"]/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/div[2]/h4/text()')[0].strip()
        except:
            news_title = ''
        # print(news_title)
        try:
            news_abstract = part_nodes[i].xpath('./a/div[2]/p[1]/text()')[0].strip()
        except:
            news_abstract = ''
        # print(news_abstract)
        # breakpoint()
        try:
            news_publish_time = part_nodes[i].xpath('./a/div[2]/p[2]/b/text()')[0].split('上传时间：')[-1].strip()
        except:
            news_publish_time = '2022-01-01'

        # print(news_publish_time)
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'https:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.tscss.org/' + url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # break
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="cont_t"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="cont_t"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'<span class="news_top_lyname">(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'<span class="news_top_zzname">(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击量：<span id="clicks">(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学会动态-学会动态': 'http://www.tscss.org/List2.html?id=44&pageIndex=1',
        '首页-学会动态-新闻资讯': 'http://www.tscss.org/List1.html?id=45&pageIndex=1',
        '首页-学会动态-通知公告': 'http://www.tscss.org/List1.html?id=46&pageIndex=1',
        '首页-学会动态-会议信息': 'http://www.tscss.org/List1.html?id=47&pageIndex=1',
        '首页-学会动态-国际交流': 'http://www.tscss.org/List1.html?id=79&pageIndex=1',
        '首页-学会党建-党建学习': 'http://www.tscss.org/List1.html?id=49&pageIndex=1',
        '首页-学会党建-党建动态': 'http://www.tscss.org/List1.html?id=50&pageIndex=1',
        '首页-学会奖项-奖励条件': 'http://www.tscss.org/List1.html?id=66&pageIndex=1',
        '首页-学会奖项-奖励年鉴': 'http://www.tscss.org/List1.html?id=68&pageIndex=1',
        '首页-体视学科普-科普资源': 'http://www.tscss.org/List2.html?id=70&pageIndex=1',
        '首页-体视学科普-科普活动': 'http://www.tscss.org/List2.html?id=71&pageIndex=1',
        '首页-关于学会-学会规章': 'http://www.tscss.org/List1.html?id=40&pageIndex=1',

    }

    for key, value in urls.items():
        news_classify = key
        id = re.findall(r'id=(.*?)&', value)[0]
        for page in range(1, 50):
            url = value.split('?')[0]
            html_text, status_code = get_html(url, id, page)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break

            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'B-67 中国体视学学会'
    start_run(args.projectname, args.sinceyear, account_name)