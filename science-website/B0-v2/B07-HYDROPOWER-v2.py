# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-05-18
import random
import re
import io
import sys
import time
import json
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    cookies = {
        'ASPSESSIONIDSSDQQDTA': 'OHDIDHKDODEKMFBIPFBOCHLA',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/107.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="subNewsListTitle"]')[0:50]
        part2_nodes = selector.xpath('//*[@class="subNewsListDate"]')
        # print(part1_nodes)
        # print(part2_nodes)
        deadline = xpath_data(part1_nodes, part2_nodes, news_classify,
                              science_system, mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, part2_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a[2]/font/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a[2]/font/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part2_nodes[i].xpath('./text()')[0]
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a[2]/@href')[0]
            if 'mp.weixin.qq.com' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.hydropower.org.cn' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    cookies = {
        'ASPSESSIONIDSSDQQDTA': 'OHDIDHKDODEKMFBIPFBOCHLA',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36',
    }

    html_response = requests.get(news_page_url, headers=headers, cookies=cookies, verify=False)

    # if 'www.cstp.org.cn' not in news_page_url:
    #     html_response.encoding = 'gb2312'
    # else:
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    try:
        news_content = result.get('content')
        # news_content = selector_page.xpath('//*[@class="contain"]//p/span/text()')
    except:
        news_content = selector_page.xpath('//*[@class="cont_txt"]/span/text()')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="contain"]//p//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'新闻来源：<span>(.*?)</span>', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-行业新闻-行业要闻': 'http://www.hydropower.org.cn/showNewsList.asp?smlId=1&bigId=1&page=1',
        '首页-行业新闻-学术活动': 'http://www.hydropower.org.cn/showNewsList.asp?bigId=1&smlId=7&page=1',
        '首页-学会介绍-学会介绍': 'http://www.hydropower.org.cn/showNewsList.asp?bigId=6&smlId=19&page=1',
        '首页-学会动态-通知公告': 'http://www.hydropower.org.cn/showNewsList.asp?bigId=2&smlId=12&page=1',
        '首页-学会动态-学会动态': 'http://www.hydropower.org.cn/showNewsList.asp?bigId=2&smlId=8&page=1',
        '首页-学会动态-学会期刊': 'http://www.hydropower.org.cn/showNewsList.asp?bigId=2&smlId=10&page=1',
        '首页-科技奖励-最新动态': 'http://www.hydropower.org.cn/showNewsTechList.asp?bigId=13&smlId=65&page=1',
        '首页-科技基金-相关新闻': 'http://www.hydropower.org.cn/showNewsPjzList.asp?showTitle=&page=1',
        '首页-行业新闻-农村水电': 'http://www.hydropower.org.cn/showNewsList.asp?smlId=45&bigId=1&page=1',
        '首页-行业新闻-水电建设': 'http://www.hydropower.org.cn/showNewsList.asp?bigId=1&smlId=5&page=1',
        '首页-行业新闻-水电移民': 'http://www.hydropower.org.cn/showNewsList.asp?bigId=1&smlId=40&page=1',
        '首页-行业新闻-水电环保': 'http://www.hydropower.org.cn/showNewsList.asp?bigId=1&smlId=41&page=1',
        '首页-行业新闻-水电科普': 'http://www.hydropower.org.cn/showNewsList.asp?bigId=1&smlId=48&page=1',
        '首页-行业新闻-抽水蓄能': 'http://www.hydropower.org.cn/showNewsList.asp?bigId=1&smlId=44&page=1',
        '首页-行业新闻-对话水电': 'http://www.hydropower.org.cn/showNewsList.asp?bigId=1&smlId=6&page=1',
        '首页-行业新闻-国外水电': 'http://www.hydropower.org.cn/showNewsList.asp?smlId=57&bigId=1&page=1',
        '首页-行业新闻-企业之窗': 'http://www.hydropower.org.cn/showNewsList.asp?smlId=46&bigId=1&page=1',
        '首页-行业新闻-政策法规': 'http://www.hydropower.org.cn/showNewsList.asp?smlId=3&bigId=1&page=1',
        '首页-资源中心': 'http://www.hydropower.org.cn/showNewsList.asp?smlId=18&bigId=5&page=1',
        '首页-党建园地-党建园地': 'http://www.hydropower.org.cn/showNewsList.asp?smlId=63&bigId=11&page=1',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            url = value.replace('page=1', 'page=' + str(page))
            html_text, status_code = get_html(url)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default',
                        default='2022-07-01')
    args = parser.parse_args()
    account_name = 'B-07 中国水力发电工程学会'
    start_run(args.projectname, args.sinceyear, account_name)
