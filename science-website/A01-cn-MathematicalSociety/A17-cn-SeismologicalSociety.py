# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-11-08
import random
import re
import io
import sys
import time
import json
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash():

    cookies = {
        'td_cookie': '2115838947',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Referer': 'http://www.ssoc.org.cn/',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    params = (
        ('id', '18'),
    )

    response = requests.get('http://www.ssoc.org.cn/List1.html', headers=headers, params=params, cookies=cookies,
                            verify=False)
    html_text = response.text
    # print(html_text)
    # breakpoint()
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, page_id, page, supFlash):

    cookies = {
        'td_cookie': supFlash,
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.ssoc.org.cn/List.html?id={}&pageIndex={}'.format(page_id, page),
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)
    response.encoding = 'utf-8'
    # print(response.text)
    # breakpoint()
    return response.text


def xpath_data( part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time, supFlash):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('.//div/h4/text()')[0]
        except:
            news_title = ''
        try:
            news_abstract = part_nodes[i].xpath('.//div/font/text()')[0]
        except:
            news_abstract = ''
        try:
            news_publish_time = part_nodes[i].xpath('.//span/text()')[0].split('更新时间：')[-1].strip()
        except:
            news_publish_time = '2022-01-01'
        # try:
        #     news_imgs = 'http://www.gsc.org.cn/' + part_nodes[i].xpath('./a/img/@src')
        # except:
        #     news_imgs = ''

        # print('http://csmpg.gyig.cas.cn/xhyw' + part_nodes[i].xpath('./a/@href')[0].strip().replace('./', '/'))
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            news_page_url = 'http://www.ssoc.org.cn/' + url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url, supFlash)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            print(news_dict_content)
            breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time, supFlash):

    selector = etree.HTML(html_text)
    # part1
    try:
        part1_nodes = selector.xpath('//*[@class="bc_b"]/ul/li')
        xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time, supFlash)
    except Exception as e:
        print('Classify： {} Part1 has not content: {}'.format(news_classify, e))

    # part2
    # try:
    #     part2_nodes = selector.xpath('//*[@width="50%"]')
    #     xpath_data(part2_nodes, page, supFlash, news_classify, science_system, mongo, account_name, project_time)
    # except:
    #     print('Classify： {} Part2 has not content: {}'.format(news_classify, e))


def get_page_content(news_page_url, supFlash):

    cookies = {
        'td_cookie': supFlash,
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Referer': news_page_url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Mobile Safari/537.36',
    }
    html_response = requests.get(news_page_url, headers=headers,cookies=cookies, verify=False)

    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="bc_right"]//p/img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'>来源：(.*?)　时间', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '学会动态-学会动态': 'http://www.ssoc.org.cn/List1.html?id=18&pageIndex=1',
        '学会动态-分支机构动态': 'http://www.ssoc.org.cn/List1.html?id=19&pageIndex=1',
        '学会动态-地方学会动态': 'http://www.ssoc.org.cn/List1.html?id=20&pageIndex=1',
        '表彰奖励-表彰奖励动态': 'http://www.ssoc.org.cn/List1.html?id=21&pageIndex=1',
        '科技服务-科技服务动态': 'http://www.ssoc.org.cn/List1.html?id=25&pageIndex=1',
        '科技服务-地震安全性评价报告审查': 'http://www.ssoc.org.cn/List1.html?id=26&pageIndex=1',
        '科技服务-科技成果鉴定': 'http://www.ssoc.org.cn/List1.html?id=27&pageIndex=1',
        '科技服务-科技咨询': 'http://www.ssoc.org.cn/List1.html?id=28&pageIndex=1',
        '科技服务-其它科技服务': 'http://www.ssoc.org.cn/List1.html?id=29&pageIndex=1',
        '学会期刊-信息公告': 'http://www.ssoc.org.cn/List1.html?id=30&pageIndex=1',
        '科普园地-科普动态': 'http://www.ssoc.org.cn/List1.html?id=38&pageIndex=1',
        '科普园地-科普传播专家团队': 'http://www.ssoc.org.cn/List1.html?id=39&pageIndex=1',
        '通知公告': 'http://www.ssoc.org.cn/List.html?id=10&pageIndex=1',
    }
    supFlash = get_supflash()
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            url = value
            page_id = re.findall(r'id=(.*?)&', value)[0].strip()
            page = re.findall(r'pageIndex=(.*?)', value)[0].strip()
            html_text = get_html(url, page_id, page, supFlash)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name, science_system, mongo, project_time,
                     start_time, supFlash)
            time.sleep(1)
        time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'A-17 中国地震学会'
    start_run(project_time, start_time, account_name)