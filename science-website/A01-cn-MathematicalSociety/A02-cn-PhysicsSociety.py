# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-10-21

import re
import io
import sys
import time
import json
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):
    cookies = {
        'PHPSESSID': '4ma67da4e7a5s2adid0t9g1opp',
        'think_language': 'zh-CN',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Referer': 'http://www.cps-net.org.cn/Partybuild/Partybuild/index.do',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)

    return response.text


def get_data(html_text, news_classify, account_name, science_system, start_time, mongo, project_time):
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    selector = etree.HTML(html_text)
    nodes = selector.xpath('//*[@class="trends-li"]')
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify',
                      'account_name', 'science_system', 'project_time']

    for i in range(len(nodes)):
        news_title = nodes[i].xpath('.//div/a/@title')[0].strip()
        news_abstract = nodes[i].xpath('./div[2]/div[2]/text()')[0].strip()
        news_publish_time = ' '.join(nodes[i].xpath('./div[2]/div[3]/span/text()'))
        news_read = nodes[i].xpath('./div[1]/div/span/text()')[0]
        news_page_url = 'http://www.cps-net.org.cn/' + nodes[i].xpath('.//div/a/@href')[0]
        news_author, news_imgs, news_content_type, news_content, \
        source, read_count, click_count = get_page_content(news_page_url)

        news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                           news_content_type, news_content, news_page_url, source,
                           news_author, news_read, click_count, news_classify,
                           account_name, science_system, project_time,
                           ]
        news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
        # print(news_dict_content)
        # breakpoint()
        mongo.insert_one(news_dict_content)
        logging.warning('Title :{} Publish: {} Was Finished!'.format(news_title, news_publish_time))


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url)
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        source = re.findall(r'文章来源：(.*?)', content_text, re.M | re.S)[0]
    except:
        source = ''

    try:
        news_imgs = selector_page.xpath('//*/article//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        click_count = re.findall(r'阅读次数：(.*?)', content_text, re.M | re.S)[0]
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time):
    mongo = mongodb()
    start_time = '2022-07-31'
    account_name = 'A-01 中国数学会'
    science_system = read_science_account(account_name)

    urls = {
        '新闻资讯-科研资讯': 'http://www.cps-net.org.cn/News/Content/index/cateid/59.do',
        '学会动态': 'http://www.cps-net.org.cn/Home/Content/index/cateid/75.do',
        '通知': 'http://www.cps-net.org.cn/Home/Content/index/cateid/76.do',
        '科研进展': 'http://www.cps-net.org.cn/Home/Content/index/cateid/80.do',
        '科普天地': 'http://www.cps-net.org.cn/Home/Content/index/cateid/81.do',
        '科学普及-科普动态': 'http://www.cps-net.org.cn/Science/Content/index/cateid/256.do',
        '奖励举荐-奖励动态': 'http://www.cps-net.org.cn/Science/Content/index/cateid/255.do',
        '学术会议-活动纪要': 'http://www.cps-net.org.cn/Meeting/Content/index/cateid/262.do',
        '学会党建-园地动态': 'http://www.cps-net.org.cn/News/Content/index/cateid/296.do',
    }

    for key, value in urls.items():
        news_classify = key
        url = value
        html_text = get_html(url)
        get_data(html_text, news_classify, account_name, science_system, start_time, mongo, project_time)
        time.sleep(1)


if __name__ == '__main__':
    project_time = '2022-Q3'
    start_run(project_time)
