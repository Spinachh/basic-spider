# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-10-25

import re
import io
import sys
import time
import json
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash():
    url = 'http://astronomy.pmo.cas.cn/xwdt/zhxw/index_1.html'

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Referer': 'http://astronomy.pmo.cas.cn/xwdt/zhxw/index_1.html',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }
    response = requests.get(url, headers=headers, verify=False)
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_A07_html(url, page, supFlash):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;'
                  'q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://astronomy.pmo.cas.cn/xwdt/zhxw/index.html',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }
    cookies = {
        'td_cookie': supFlash,
        'PHPSESSID': 'nlss62tn73el4eierftqf99ba6',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)
    response.encoding = 'utf-8'
    # html_chardet = cchardet.detect(response.content)
    # # print(response.headers["content-type"])
    # # print(response.encoding)
    # # print(html_chardet)
    # response.encoding = html_chardet["encoding"]
    # # response = response.text.replace('\\"', '#').replace("\\n","")  # 对json格式中的多余的引号去除
    # html_content = response.text.encode("utf-8").decode("unicode_escape")
    # # breakpoint()
    return response.text


def get_A07_data(html_text, news_classify, account_name, science_system, start_time, mongo, project_time, supFlash):

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    selector = etree.HTML(html_text)
    nodes = selector.xpath('//*[@class="list_list"]/ul/li')
    # print(nodes)
    # breakpoint()
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify',
                      'account_name', 'science_system', 'project_time']

    for i in range(len(nodes)):
        news_title = nodes[i].xpath('./a/text()')[0]
        news_abstract = nodes[i].xpath('./a/@title')[0]
        news_publish_time = nodes[i].xpath('./em/text()')[0].strip()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_publish_stamp)
        # breakpoint()
        news_page_url = 'http://astronomy.pmo.cas.cn/xwdt/' + nodes[i].xpath('./a/@href')[0].replace('..', '')

        if int(news_publish_stamp) >= int(start_time_stamp):
            news_author, news_imgs, news_content_type, news_content, source,\
            read_count, click_count = get_page_content(news_page_url, supFlash)

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            mongo.insert_one(news_dict_content)
            logging.warning('Title :{} Publish: {} Was Finished!'.format(news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url, supFlash):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': news_page_url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }
    cookies = {
        'td_cookie': supFlash,
        'PHPSESSID': 'sviinfuvdipq3jvqp33lboi667',
    }

    html_response = requests.get(news_page_url, headers=headers, cookies=cookies, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="TRS_Editor"]/p//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'>文章来源：(.*?)<', content_text, re.M | re.S)[0]
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览次数：(.*?)', content_text, re.M | re.S)[0]
    except:
        read_count = ''
    try:
        click_count = re.findall(r'阅读次数：(.*?)', content_text, re.M | re.S)[0]
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻动态-综合新闻': 'http://astronomy.pmo.cas.cn/xwdt/zhxw/index.html',
        '首页-新闻动态-图片新闻': 'http://astronomy.pmo.cas.cn/xwdt/tpxw/index.html',
        '首页-新闻动态-学会动态': 'http://astronomy.pmo.cas.cn/xwdt/hxdt/index.html',
        '首页-学术交流-学术活动计划': 'http://astronomy.pmo.cas.cn/xsjl/hdjh/',
        '首页-学术交流-国内学术交流': 'http://astronomy.pmo.cas.cn/xsjl/gnxsjl/',
        '首页-学术交流-国际学术交流': 'http://astronomy.pmo.cas.cn/xsjl/gjxsjl/',
        '首页-表彰奖励-中国天文学会张钰哲奖': 'http://astronomy.pmo.cas.cn/bzjl/zyzj/',
        '首页-表彰奖励-中国天文学会黄授书奖': 'http://astronomy.pmo.cas.cn/bzjl/hssj/',
        '首页-科普工作-科普活动': 'http://astronomy.pmo.cas.cn/kpgz/kphd/',
        '首页-科普工作-综合报道': 'http://astronomy.pmo.cas.cn/kpgz/zhbd/',
        '首页-天文人才-人才招聘': 'http://astronomy.pmo.cas.cn/twrc/rczp/',
        '首页-青托工程-学会青托工程': 'http://astronomy.pmo.cas.cn/qtgc/xhqtgc/',
        '首页-青托工程-青托风采': 'http://astronomy.pmo.cas.cn/qtgc/qtfc/',
        '首页-其它-通知公告': 'http://astronomy.pmo.cas.cn/qt/tzgg/',
        '首页-其它-学会100周年': 'http://astronomy.pmo.cas.cn/qt/cas100/',
        '首页-其它-学会出版': 'http://astronomy.pmo.cas.cn/qt/xhcb/',
        '首页-其它-科普活动': 'http://astronomy.pmo.cas.cn/qt/kphd/',
        '首页-其它-下载中心': 'http://astronomy.pmo.cas.cn/qt/xzzx/',
        '首页-其它-征集赞助': 'http://astronomy.pmo.cas.cn/qt/zjzz/',
    }
    supFlash = get_supflash()
    for key, value in urls.items():
        news_classify = key
        # print(key, value)
        # break
        for page in range(3):
            if page == 0:
                url = value
            else:
                url = value.replace('index', 'index' + '_' + str(page))
            html_text = get_A07_html(url, page, supFlash)
            # print(html_text)
            # breakpoint()
            result = get_A07_data(html_text, news_classify, account_name, science_system,
                              start_time, mongo, project_time, supFlash)
            time.sleep(1)
            if not result:
                break


if __name__ == '__main__':
    project_time = '2022-Q3'
    start_time = '2022-07-31'
    account_name = 'A-07 中国天文学会'
    start_run(project_time, start_time, account_name)