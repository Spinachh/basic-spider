# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-11-02
import random
import re
import io
import sys
import time
import json
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash():

    url = 'http://www.gsc.org.cn/channel.aspx?id=68'
    cookies = {
        'ASP.NET_SessionId': 'qisopuwtgn1gxwem3xelysjk',
        'td_cookie': '1666073779',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.gsc.org.cn/channel.aspx?id=68',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)
    html_text = response.text
    # print(html_text)
    # breakpoint()
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, id, page, supFlash):

    cookies = {
        'ASP.NET_SessionId': 'qisopuwtgn1gxwem3xelysjk',
        'td_cookie': supFlash,
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        # 'Referer': 'http://www.gsc.org.cn/channel.aspx?id=68&page={}'.format(page),
        'Referer': 'http://www.gsc.org.cn/channel.aspx?id=68',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers,
                            cookies=cookies, verify=False)
    # print(response.text)
    # breakpoint()
    return response.text


def xpath_data(part_nodes, page, supFlash, news_classify, science_system, mongo,
               account_name, project_time, start_time ):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        news_title = part_nodes[i].xpath('./div//a/text()')[0].strip()
        news_abstract = part_nodes[i].xpath('./div/p/text()')[0]
        news_publish_time = part_nodes[i].xpath('./div//b/text()')[0].replace('/', '-')

        # try:
        #     news_imgs = 'http://www.gsc.org.cn/' + part_nodes[i].xpath('./a/img/@src')
        # except:
        #     news_imgs = ''

        # print(news_publish_time)
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d %H:%M:%S'))
        if int(news_publish_stamp) >= int(start_time_stamp):
            news_page_url = 'http://www.gsc.org.cn/' + part_nodes[i].xpath('./a/@href')[0]
            # if news_imgs:
            #     news_content_type = 'text-img'
            # else:
            #     news_content_type = 'text'
            news_content_type = 'text-img'
            news_author, news_imgs, news_content, source, \
            read_count, click_count = get_page_content(news_page_url, page, supFlash)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))

            mongo.insert_one(news_dict_content)
            logging.warning('Classfiy: {} Title :{} Publish: {} Was Finished!'.format(news_classify, news_title, news_publish_time))
        else:
            return ''


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, page, supFlash, start_time):

    selector = etree.HTML(html_text)

    # part1
    try:
        part1_nodes = selector.xpath('//*[@class="news"]//li')
        xpath_data(part1_nodes, page, supFlash, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        print('Classify： {} Part1 has not content: {}'.format(news_classify, e))

    # part2
    # try:
    #     part2_nodes = selector.xpath('//*[@width="50%"]')
    #     xpath_data(part2_nodes, page, supFlash, news_classify, science_system, mongo, account_name, project_time)
    # except:
    #     print('Classify： {} Part2 has not content: {}'.format(news_classify, e))


def get_page_content(news_page_url, page, supFlash):

    cookies = {
        'ASP.NET_SessionId': 'qisopuwtgn1gxwem3xelysjk',
        'td_cookie': supFlash,
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': news_page_url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    html_response = requests.get(news_page_url, headers=headers, cookies=cookies,
                            verify=False)

    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="news"]//p/img/@src')
    except:
        news_imgs = ''
    try:
        source = re.findall(r'>来源：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''
    try:
        click_count = re.findall(r'>阅读次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()

    science_system = read_science_account(account_name)

    urls = {
        '首页内容-新闻动态': 'http://www.gsc.org.cn/channel.aspx?id=68&page=1',
        '学术交流-学术交流': 'http://www.gsc.org.cn/channel.aspx?id=13&page=1',
        '学术交流-国内交流': 'http://www.gsc.org.cn/channel.aspx?id=14&page=1',
        '学术交流-国际交流': 'http://www.gsc.org.cn/channel.aspx?id=15&page=1',
        '学术交流-两岸交流': 'http://www.gsc.org.cn/channel.aspx?id=16&page=1',
        '学术交流-国际交往': 'http://www.gsc.org.cn/channel.aspx?id=61&page=1',
        '学术交流-品牌活动': 'http://www.gsc.org.cn/channel.aspx?id=17&page=1',
        '智库建设-活动报道': 'http://www.gsc.org.cn/channel.aspx?id=62&page=1',
        '智库建设-机构建设': 'http://www.gsc.org.cn/channel.aspx?id=19&page=1',
        '智库建设-平台搭建': 'http://www.gsc.org.cn/channel.aspx?id=20&page=1',
        '智库建设-品牌活动': 'http://www.gsc.org.cn/channel.aspx?id=21&page=1',
        '科普天地-品牌活动': 'http://www.gsc.org.cn/channel.aspx?id=24&page=1',
        '科普天地-科普动态': 'http://www.gsc.org.cn/channel.aspx?id=25&page=1',
        '科普天地-科普资源': 'http://www.gsc.org.cn/channel.aspx?id=26&page=1',
        '科普天地-地理知识': 'http://www.gsc.org.cn/channel.aspx?id=27&page=1',
        '科普天地-科普专家': 'http://www.gsc.org.cn/channel.aspx?id=28&page=1',
        '科普天地-科普志愿者': 'http://www.gsc.org.cn/channel.aspx?id=29&page=1',
        '科普天地-科普教育基地': 'http://www.gsc.org.cn/channel.aspx?id=30&page=1',
        '党建强会-社会服务': 'http://www.gsc.org.cn/channel.aspx?id=90&page=1',
        '党建强会-工作动态': 'http://www.gsc.org.cn/channel.aspx?id=32&page=1',
        '党建强会-学界动态': 'http://www.gsc.org.cn/channel.aspx?id=60&page=1',
        '党建强会-承能服务': 'http://www.gsc.org.cn/channel.aspx?id=41&page=1',
        '期刊书籍-品牌活动': 'http://www.gsc.org.cn/channel.aspx?id=45&page=1',
        '学会奖励-中国地理科学成就奖': 'http://www.gsc.org.cn/channel.aspx?id=37&page=1',
        '学会奖励-全国优秀地理科技工作者': 'http://www.gsc.org.cn/channel.aspx?id=46&page=1',
        '学会奖励-全国青年地理科技奖': 'http://www.gsc.org.cn/channel.aspx?id=47&page=1',
        '学会奖励-全国优秀中学地理教育工作者': 'http://www.gsc.org.cn/channel.aspx?id=48&page=1',
        '学会奖励-全国优秀地理图书奖': 'http://www.gsc.org.cn/channel.aspx?id=52&page=1',
        '学会奖励-各类优秀论文奖': 'http://www.gsc.org.cn/channel.aspx?id=53&page=1',
        '学会奖励-其它奖励': 'http://www.gsc.org.cn/channel.aspx?id=54&page=1',
        '活动通知-会议通知': 'http://www.gsc.org.cn/channel.aspx?id=18&page=1',
        '活动通知-学术活动': 'http://www.gsc.org.cn/channel.aspx?id=64&page=1',
        '活动通知-科普活动': 'http://www.gsc.org.cn/channel.aspx?id=65&page=1',
        '活动通知-表彰奖励': 'http://www.gsc.org.cn/channel.aspx?id=66&page=1',
        '活动通知-公告公示': 'http://www.gsc.org.cn/channel.aspx?id=67&page=1',
        '会员服务-公告公示': 'http://www.gsc.org.cn/channel.aspx?id=67&page=1',
    }
    supFlash = get_supflash()
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 10):
            url = value.replace('page=1', 'page=' + str(page))
            id = re.findall(r'aspx\?id=(.*?)&', url)[0].strip()
            html_text = get_html(url, id, page, supFlash)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name, science_system, mongo, project_time, page, supFlash,
                     start_time)
            time.sleep(1)
        time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'A-10 中国地质学会'
    start_run(project_time, start_time, account_name)