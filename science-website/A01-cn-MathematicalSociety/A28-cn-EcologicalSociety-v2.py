# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-11-11
import random
import re
import io
import sys
import time
import json
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash():
    cookies = {
        'td_cookie': '2095021031',
        'Hm_lvt_ec22f37ea1cce8f86744c8d3c25d6c4e': '1667870775',
        'Hm_lpvt_ec22f37ea1cce8f86744c8d3c25d6c4e': '1667870812',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.botany.org.cn/xwzx/xwdt_/index_1.html',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get('http://www.botany.org.cn/xwzx/xwdt_/index.html', headers=headers, cookies=cookies,
                            verify=False)
    html_text = response.text
    # print(html_text)
    # breakpoint()
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, id):

    cookies = {
        'td_cookie': '2370167964',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Mobile Safari/537.36',
    }

    response = requests.get(url, headers=headers, cookies=cookies)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    selector = etree.HTML(html_text)
    # part1
    try:
        if '通知公告' in news_classify:
            part1_nodes = selector.xpath('//*[@class="rightNotice"]/li')

        else:
            part1_nodes = selector.xpath('//*[@class="rightNews"]/li')
        # print(part1_nodes)
        # breakpoint()
        xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        print('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            if '通知公告' not in news_classify:
                news_title = part_nodes[i].xpath('./a/div[2]/h4/text()')[0]
            else:
                news_title = part_nodes[i].xpath('./a/p/text()')[0]

        except:
            news_title = ''

        try:
            if '通知公告' not in news_classify:
                news_abstract = part_nodes[i].xpath('./a/div[2]/p/text()')[0]
            else:
                news_abstract = news_title
        except:
            news_abstract = ''

        try:
            if '通知公告' not in news_classify:
                news_publish_time = part_nodes[i].xpath('./a/div[2]/div/span[1]/text()')[0].split('发布时间：')[-1]
            else:
                news_publish_time = part_nodes[i].xpath('./a/span/text()')[0]
        except:
            news_publish_time = '2022-01-01'

        try:
            news_imgs =  part_nodes[i].xpath('./a/div[1]/img/@src')
            if news_imgs:
                news_content_type = 'text-img'
            else:
                news_content_type = 'text'
        except:
            news_imgs = ''
            news_content_type = 'text'

        try:
            read_count = part_nodes[i].xpath('./a/div[2]/div/span[2]/text()')[0].split('点击量：')[-1]
        except:
            read_count = ''

        # print(news_title)
        # print(news_publish_time)
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))


        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'www' in url_part2:
                news_page_url = 'http:' + url_part2
            else:
                news_page_url = 'http://www.esc.org.cn/' + url_part2

            news_author, news_content, source, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    '''
    try:
        news_imgs = selector_page.xpath('//*[@valign="top"]//p//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
        
    try:
        read_count = re.findall(r'浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''
    '''
    try:
        source = re.findall(r'>来源:(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        click_count = re.findall(r'>点击量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_content, source, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻动态-重要新闻': 'http://www.esc.org.cn/newsList.html?id=39&pageIndex=1',
        '首页-新闻动态-学会动态': 'http://www.esc.org.cn/newsList.html?id=40&pageIndex=1',
        '首页-新闻动态-专业委员会动态': 'http://www.esc.org.cn/newsList.html?id=41&pageIndex=1',
        '首页-新闻动态-地方学会动态': 'http://www.esc.org.cn/newsList.html?id=42&pageIndex=1',
        '首页-党建强会-工作动态': 'http://www.esc.org.cn/newsList.html?id=16&pageIndex=1',
        '首页-党建强会-学习园地': 'http://www.esc.org.cn/newsList.html?id=17&pageIndex=1',
        '首页-学术交流-学术会议': 'http://www.esc.org.cn/newsList.html?id=56&pageIndex=1',
        '首页-学术交流-会议动态': 'http://www.esc.org.cn/newsList.html?id=57&pageIndex=1',
        '首页-科普教育-科普动态': 'http://www.esc.org.cn/newsList.html?id=21&pageIndex=1',
        '首页-科普教育-科学传播': 'http://www.esc.org.cn/newsList.html?id=22&pageIndex=1',
        '首页-科普教育-科普教育基地': 'http://www.esc.org.cn/newsList.html?id=23&pageIndex=1',
        '首页-科普教育-继续教育': 'http://www.esc.org.cn/newsList.html?id=24&pageIndex=1',
        '首页-国际交流-国际事务动态': 'http://www.esc.org.cn/newsList.html?id=18&pageIndex=1',
        '首页-国际交流-国际学术会议': 'http://www.esc.org.cn/newsList.html?id=19&pageIndex=1',
        '首页-国际交流-国际合作交往': 'http://www.esc.org.cn/newsList.html?id=20&pageIndex=1',
        '首页-决策咨询-中国生态库': 'http://www.esc.org.cn/newsList.html?id=25&pageIndex=1',
        '首页-决策咨询-高层论坛': 'http://www.esc.org.cn/newsList.html?id=26&pageIndex=1',
        '首页-决策咨询-专题调研': 'http://www.esc.org.cn/newsList.html?id=27&pageIndex=1',
        '首页-决策咨询-决策咨询': 'http://www.esc.org.cn/newsList.html?id=29&pageIndex=1',
        '首页-期刊数据-期刊速递': 'http://www.esc.org.cn/newsList.html?id=15&pageIndex=1',
        '首页-通知公告-通知公告': 'http://www.esc.org.cn/notice.html?id=43&pageIndex=1',

    }
    # supFlash = get_supflash()
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 10):
            id = re.findall(r'id=(.*?)\&', value)[0]
            url = value.replace('pageIndex=1', 'pageIndex=' + str(page))
            html_text = get_html(url, id)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time)
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default', default='2023-01-01')
    args = parser.parse_args()
    account_name = 'A-28 中国生态学学会'
    start_run(args.projectname, args.sinceyear, account_name)
