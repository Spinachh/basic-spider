# -*- coding:utf-8 -*-
# Project : KeXie
# Author : mongoole
# Date : 

import re
import io
import sys
import time
import json
import cchardet
import requests
from lxml import etree


science_account = {
    '全国学会': [
        'A-01 中国数学会', 'A-02 中国物理学会', 'A-05 中国声学学会', 'A-08 中国气象学会', 'A-21 中国微生物学会',
        'A-23 中国细胞生物学学会', 'A-36 中国实验动物学会', 'B-03 中国农业机械学会', 'B-06 中国电工技术学会',
        'B-07 中国水力发电工程学会', 'B-09 中国内燃机学会', 'B-13 中国真空学会', 'B-19 中国电子学会', 'B-20 中国计算机学会',
        'B-44 中国生物工程学会', 'B-49 中国材料研究学会', 'B-62 中国电源学会', 'C-04 中国水产学会', 'D-20 中国免疫学会',
        'E-28T 中国产学研合作促进会', 'A-03 中国力学学会', 'A-04 中国光学学会', 'A-06 中国化学会', 'A-07 中国天文学会',
        'A-09 中国空间科学学会', 'A-11 中国地理学会', 'A-12 中国地球物理学会', 'A-13 中国矿物岩石地球化学学会',
        'A-16 中国海洋学会', 'A-18 中国动物学会', 'A-19 中国植物学会', 'A-20 中国昆虫学会', 'A-22 中国生物化学与分子生物学会',
        'A-24 中国植物生理与植物分子生物学学会', 'A-25 中国生物物理学会', 'A-26 中国遗传学会', 'A-27 中国心理学会',
        'A-28 中国生态学学会', 'A-29 中国环境科学学会', 'A-30 中国自然资源学会', 'A-31 中国感光学会',
        'A-32 中国优选法统筹法与经济数学研究会', 'A-33 中国岩石力学与工程学会', 'A-34 中国野生动物保护协会', 'A-35 中国系统工程学会',
        'A-38 中国环境诱变剂学会', 'A-39 中国运筹学会', 'A-40 中国菌物学会', 'A-41 中国晶体学会', 'A-43W 中国认知科学学会',
        'B-01 中国机械工程学会', 'B-02 中国汽车工程学会', 'B-04 中国农业工程学会', 'B-05 中国电机工程学会', 'B-08 中国水利学会',
        'B-10 中国工程热物理学会', 'B-11 中国空气动力学会', 'B-12 中国制冷学会', 'B-14 中国自动化学会', 'B-15 中国仪器仪表学会',
        'B-16 中国计量测试学会', 'B-17T 中国标准化协会', 'B-18 中国图学学会', 'B-21 中国通信学会', 'B-22 中国中文信息学会',
        'B-23 中国测绘学会', 'B-25 中国航海学会', 'B-27 中国公路学会', 'B-28 中国航空学会', 'B-29 中国宇航学会',
        'B-30 中国兵工学会', 'B-31 中国金属学会', 'B-32 中国有色金属学会', 'B-33 中国稀土学会', 'B-34 中国腐蚀与防护学会',
        'B-35 中国化工学会', 'B-36 中国核学会', 'B-37 中国石油学会', 'B-38 中国煤炭学会', 'B-39 中国可再生能源学会',
        'B-40 中国能源研究会', 'B-41 中国硅酸盐学会', 'B-42 中国建筑学会', 'B-43 中国土木工程学会', 'B-45 中国纺织工程学会',
        'B-46 中国造纸学会', 'B-47 中国文物保护技术协会', 'B-48T 中国印刷技术协会', 'B-50 中国食品科学技术学会',
        'B-51 中国粮油学会', 'B-52T 中国职业安全健康协会', 'B-53 中国烟草学会', 'B-54 中国仿真学会', 'B-55 中国电影电视技术学会',
        'B-56 中国振动工程学会', 'B-57 中国颗粒学会', 'B-58 中国照明学会', 'B-60 中国惯性技术学会', 'B-61 中国风景园林学会',
        'E-12T 中国工业设计协会', 'E-13 中国工艺美术学会', 'E-14 中国科普作家协会', 'E-15 中国自然科学博物馆学会',
        'E-16T 中国可持续发展研究会', 'E-17 中国青少年科技教育工作者协会', 'E-18 中国科教电影电视协会',
        'E-19 中国科学技术期刊编辑学会', 'E-22 中国国土经济学会', 'E-23 中国土地学会', 'E-24 中国科技新闻学会',
        'E-25 中国老科学技术工作者协会', 'E-26T 中国科学探险协会', 'E-27T 中国城市规划学会', 'E-29T 中国知识产权研究会',
        'E-30T 中国发明协会', 'E-33T 中国检验检测学会', 'E-34T 中国女科技工作者协会', 'E-35W 中国创造学会',
        'E-37W 中国高科技产业化研究会', 'E-41W 中国科技馆发展基金会', 'E-42W 中国生物多样性保护与绿色发展基金会',
        'E-20T 中国流行色协会', 'E-40W 中国基本建设优化研究会', 'A-37 中国青藏高原研究会', 'D-17 中国体育科学学会',
        'D-28W 中国卒中学会', 'B-77W 中国生物材料学会', 'E-02 中国管理现代化研究会', 'A-45G 国际数字地球协会', 'C-07 中国植物病理学会',
        'D-22 中国法医学会', 'A-42 中国神经科学学会', 'B-63 中国复合材料学会', 'B-64T 中国消防协会', 'B-65 中国图象图形学学会',
        'B-66 中国人工智能学会', 'B-67 中国体视学学会', 'B-70T 中国遥感应用协会', 'B-71 中国指挥与控制学会', 'B-72T 中国光学工程学会',
        'B-73T 中国微米纳米技术学会', 'B-74 中国密码学会', 'B-75T 中国大坝工程学会', 'B-76T 中国卫星导航定位协会', 'C-03 中国土壤学会',
        'C-05 中国园艺学会', 'C-06 中国畜牧兽医学会', 'C-09 中国作物学会', 'C-10 中国热带作物学会', 'C-12 中国水土保持学会',
        'C-13 中国茶叶学会', 'C-14 中国草学会', 'C-15T 中国植物营养与肥料学会', 'D-01 中华医学会', 'D-05 中华护理学会',
        'D-06 中国生理学会', 'D-07 中国解剖学会', 'D-08 中国生物医学工程学会', 'D-09 中国病理生理学会', 'D-13 中国防痨协会',
        'D-14 中国麻风防治协会', 'D-15T 中国心理卫生协会', 'D-16 中国抗癌协会', 'D-18 中国毒理学会', 'D-19 中国康复医学会',
        'D-21 中华预防医学会', 'D-23T 中华口腔医学会', 'D-24T 中国医学救援协会', 'D-26T 中国研究型医院学会', 'D-27W 中国睡眠研究会',
        'E-01 中国自然辩证法研究会', 'E-03 中国技术经济学会', 'E-05 中国未来研究会', 'E-08 中国图书馆学会', 'E-09 中国城市科学研究会',
        'E-10 中国科学学与科技政策研究会', 'E-11 中国农村专业技术协会', 'A-10 中国地质学会', 'A-15 中国海洋湖沼学会',
        'B-24 中国造船工程学会', 'C-01 中国农学会', 'C-02 中国林学会', 'E-44T 中国高等教育学会', 'B-69T 中国海洋工程咨询协会',
        'D-04 中国药学会', 'D-02 中华中医药学会', 'A-46G 国际动物学会', 'E-43W 中国反邪教协会', 'E-32T 中国工程教育专业认证协会']
}

# account_name = 'A-01 中国数学会'
# for key, value in science_account.items():
#     print(key)
#     if account_name in value:
#         print('ok')
#         science_system = key
#         print(science_system)