# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-11-17
import random
import re
import io
import sys
import time
import json
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash():
    cookies = {
        'td_cookie': '2095021031',
        'Hm_lvt_ec22f37ea1cce8f86744c8d3c25d6c4e': '1667870775',
        'Hm_lpvt_ec22f37ea1cce8f86744c8d3c25d6c4e': '1667870812',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.botany.org.cn/xwzx/xwdt_/index_1.html',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get('http://www.botany.org.cn/xwzx/xwdt_/index.html', headers=headers, cookies=cookies,
                            verify=False)
    html_text = response.text
    # print(html_text)
    # breakpoint()
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, catid):

    cookies = {
        'UM_distinctid': '184838474f7245-039d3e5e61c30c-9166f2c-1fa400-184838474f8a93',
        'CNZZDATA1264453274': '1434254583-1668653481-%7C1668653481',
        'td_cookie': '2878047306',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid={}'.format(catid),
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Mobile Safari/537.36',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    selector = etree.HTML(html_text)
    # part1
    try:
        part1_nodes = selector.xpath('//*[@class="com_list"]/li')
        # print(part1_nodes)
        # breakpoint()
        xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        print('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/h3/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/h3/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./a/span/text()')[0].strip()
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'mp.weixin.qq.com' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="text_content"]//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
        
    try:
        read_count = re.findall(r'浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        source = re.findall(r'>来源：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        click_count = re.findall(r'> 点击率：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '学会资讯-学会动态': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=19&page=1',
        '学会资讯-通知公告': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=20&page=1',
        '学会资讯-行业资讯': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=22&page=1',
        '学会资讯-政策法规': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=95&page=1',
        '学会资讯-电子通讯': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=100&page=1',
        '学术交流-国内学术交流': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=9&page=1',
        '学术交流-国际学术交流': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=48&page=1',
        '教育培训-政策法规': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=10&page=1',
        '教育培训-中国实验动物从业人员资格等级培训': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=94&page=1',
        '科技奖励-中国实验动物学会科学技术奖': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=11&page=1',
        '科技奖励-中国实验动物学会突出贡献奖': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=53&page=1',
        '科技奖励-实验动物青年科学家奖': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=54&page=1',
        '科技奖励-举荐奖励': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=55&page=1',
        '科学普及-科普活动': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=12&page=1',
        '科学普及-科普知识': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=23&page=1',
        '科技服务-行业标准': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=13&page=1',
        '科技服务-科技咨询': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=14&page=1',
        '科技服务-为单位服务': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=60&page=1',
        '科技服务-为企业服务': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=61&page=1',
        '科技服务-为会员服务': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=62&page=1',
        '科技服务-机构福利伦理评价': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=106&page=1',
        '期刊出版-中国实验动物学报': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=63&page=1',
        '期刊出版-中国比较医学杂志': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=64&page=1',
        '期刊出版-Animal Models and Experimental Medicine': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=65&page=1',
        '期刊出版-实验动物图书': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=66&page=1',
        '学会党建-党的二十大精神': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=16&page=1',
        '学会党建-中央精神': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=67&page=1',
        '学会党建-科协党建': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=97&page=1',
        '学会党建-党建动态': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=68&page=1',
        '学会党建-党风廉政': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=69&page=1',
        '学会党建-党务课堂': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=70&page=1',
        '会员中心-入会指南': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=17&page=1',
        '会员中心-常见问题': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=83&page=1',

    }

    # supFlash = get_supflash()
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            catid = re.findall(r'catid=(.*?)\&', value)[0]
            url = value.replace('page=1', 'page=' + str(page))
            html_text = get_html(url, catid)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time)
            time.sleep(1)
            logging.warning('NewsClassify: {} Page：{} was finished.'.format(news_classify, page))
        time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'A-36 中国实验动物学会'
    start_run(project_time, start_time, account_name)