# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-10-28

import re
import io
import sys
import time
import json
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_A08_html(url, page):

    cookies = {
        'td_cookie': '1161787341',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    params = (
        ('currentpage', page),
    )

    response = requests.get(url, headers=headers, params=params,
                            cookies=cookies, verify=False)

    return response.text


def get_A08_img_html(url, page):

    cookies = {
        'td_cookie': '1161787341',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    params = (
        ('currentpage', page),
    )

    response = requests.get(url, headers=headers, params=params,
                            cookies=cookies, verify=False)

    return response.text



def get_A08_data(html_text, news_classify, account_name, science_system, start_time, mongo, project_time):

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    selector = etree.HTML(html_text)
    nodes = selector.xpath('//*[@width="827"]//tr')
    # print(nodes)
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    for i in range(len(nodes)):
        news_title = nodes[i].xpath('.//span/a/text()')[0]
        news_abstract = nodes[i].xpath('.//span/a/text()')[0]
        try:
            news_publish_time = nodes[i].xpath('./td[3]/span/text()')[0].replace('[', '').replace(']', '')
        except:
            news_publish_time = time.strftime('%Y-%m-%d', time.localtime())
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = nodes[i].xpath('.//span/a/@href')[0].strip()
                else:
                    url_part2 = nodes[i].xpath('./a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.cms1924.org/WebPage/' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_A08_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_A08_img_data(html_text, news_classify, account_name, science_system, start_time, mongo, project_time):
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    selector = etree.HTML(html_text)
    nodes = selector.xpath('//*[@width="100%"]//table')
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    for i in range(len(nodes)):
        news_title = nodes[i].xpath('.//a/text()')[0]
        try:
            news_abstract = nodes[i].xpath('.//tr[2]//span/text()')[0]
        except:
            news_abstract = ''
        news_publish_time = nodes[i].xpath('.//td[2]/span/text()')[0].replace('[', '').replace(']', '')
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            news_page_url = 'http://www.cms1924.org/WebPage/' + nodes[i].xpath('.//a/@href')[0]
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_A08_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_A08_page_content(news_page_url):
    cookies = {
        'td_cookie': '1161787341',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Referer': 'http://www.cms1924.org/WebPage/WebPage_370_392.aspx?currentpage=1',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }

    html_response = requests.get(news_page_url, headers=headers,
                            cookies=cookies, verify=False)
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@width="827"]//tr//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'>文章来源：(.*?)<', content_text, re.M | re.S)[0]
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览量：(.*?)<', content_text, re.M | re.S)[0]
    except:
        read_count = ''
    try:
        click_count = re.findall(r'>阅读次数：(.*?)<', content_text, re.M | re.S)[0]
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学会要闻-学会新闻': 'http://www.cms1924.org/WebPage/WebPage_370_392.aspx?currentpage=1',
        '首页-学会要闻-图片新闻': 'http://www.cms1924.org/WebPage/WebPage_370_391.aspx?currentpage=1',
        '首页-学会之窗-学会历史-历年大事记': 'http://www.cms1924.org/webpage/WebPage2_74_83_156.aspx?currentpage=1',
        '首页-学术交流-会议通知': 'http://www.cms1924.org/webpage/WebPage_75_110.aspx?currentpage=1',
        '首页-学术交流-国内交流': 'http://www.cms1924.org/webpage/WebPage_75_108.aspx?currentpage=1',
        '首页-学术交流-国际交流': 'http://www.cms1924.org/webpage/WebPage_75_109.aspx?currentpage=1',
        '首页-学术交流-活动预告': 'http://www.cms1924.org/webpage/WebPage_75_111.aspx?currentpage=1',
        '首页-气象科普-科普动态': 'http://www.cms1924.org/WebPage/WebPage_76_116.aspx?currentpage=1',
    }
    for key, value in urls.items():
        news_classify = key
        url = value
        for page in range(1, 12):
            if '图片新闻' not in news_classify:
                html_text = get_A08_html(url, page)
                result = get_A08_data(html_text, news_classify, account_name, science_system, start_time, mongo,
                                      project_time)

            else:
                html_text = get_A08_img_html(url, page)
                # print(html_text)
                # breakpoint()
                result = get_A08_img_data(html_text, news_classify, account_name, science_system, start_time, mongo,
                                      project_time)

            # print(html_text)
            # breakpoint()
            time.sleep(1)
            if not result:
                break


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',default='kexie-2023-01-01')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default', default='2023-01-01')
    args = parser.parse_args()
    account_name = 'A-08 中国气象学会'

    start_run(args.projectname, args.sinceyear, account_name)
