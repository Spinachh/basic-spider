# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-04-20

import re
import io
import sys
import time
import json
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, page):
    cookies = {
        'td_cookie': '469853856',
        'PHPSESSID': 'jmgb8q21d57m0oq2at6c19bgsa',
        'Hm_lvt_8b6b5e8657ed0c0213276d5954e736eb': '1666244889',
        'Hm_lpvt_8b6b5e8657ed0c0213276d5954e736eb': '1666245639',
    }

    headers = {
        'Accept': 'image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.cms.org.cn/Home/news/news/cid/1.html?page={}'.format(page),
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
        'Cache-Control': 'max-age=0',
        'Sec-Fetch-Dest': 'image',
        'Sec-Fetch-Mode': 'no-cors',
        'Sec-Fetch-Site': 'cross-site',
        'sec-ch-ua': '"Chromium";v="106", "Google Chrome";v="106", "Not;A=Brand";v="99"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'If-None-Match': '43661a1b40adcc1225f2609bee4decdb',
        'Intervention': '<https://www.chromestatus.com/feature/5718547946799104>; level="warning"',
        'authority': 'cm.g.doubleclick.net',
        'accept': 'image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8',
        'accept-language': 'zh-CN,zh;q=0.9',
        'referer': 'http://static.bshare.cn/',
        'sec-fetch-dest': 'image',
        'sec-fetch-mode': 'no-cors',
        'sec-fetch-site': 'cross-site',
        'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }

    params = (
        ('page', page),
    )

    response = requests.get(url, headers=headers, params=params,
                            cookies=cookies, verify=False)
    status_code = response.status_code
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, start_time, mongo, project_time):

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    selector = etree.HTML(html_text)
    nodes = selector.xpath('//*[@class="newsPage"]/div')
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    for i in range(len(nodes)):
        try:
            news_title = nodes[i].xpath('./a//h4/text()')[0]
        except:
            news_title = ''

        try:
            news_abstract = nodes[i].xpath('./a//p/text()')[0]
        except:
            news_abstract = ''
        try:
            if nodes[i].xpath('.//img/@src')[0]:
                news_imgs = 'www.cms.org.cn' + nodes[i].xpath('.//img/@src')[0]
            else:
                news_imgs = ''
        except:
            news_imgs = ''

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        news_publish_time = nodes[i].xpath('./a//span/text()')[0]
        news_publish_struct = news_publish_time.split('发布时间：')[-1]
        news_publish_stamp = time.mktime(time.strptime(news_publish_struct, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            news_page_url = 'http://www.cms.org.cn' + nodes[i].xpath('./a/@href')[0]
            # print(news_title, news_img, news_publish_time, news_page_url)

            news_author, news_content, source, read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url)
    content_text = html_response.text
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        source = re.findall(r'>文章来源：(.*?)<', content_text, re.M | re.S)[0]
    except:
        source = ''

    try:
        read_count = re.findall(r'>浏览次数：(.*?)<', content_text, re.M | re.S)[0]
    except:
        read_count = ''
    try:
        click_count = re.findall(r'>阅读次数：(.*?)<', content_text, re.M | re.S)[0]
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '学会新闻': 'http://www.cms.org.cn/Home/news/news/cid/1.html',
        '科学新闻': 'http://www.cms.org.cn/Home/news/news/cid/2.html',
        '通知': 'http://www.cms.org.cn/Home/notices/notices_1.html',
    }
    for key, value in urls.items():
        news_classify = key
        url = value
        for page in range(1, 50):
            html_text, status_code = get_html(url, page)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, start_time, mongo, project_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2022-07-01')
    args = parser.parse_args()
    account_name = 'A-01 中国数学会'
    start_run(args.projectname, args.sinceyear, account_name)

