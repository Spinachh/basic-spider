# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-11-07
import random
import re
import io
import sys
import time
import json
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash():

    cookies = {
        'ASP.NET_SessionId': 'ec1bpnn1rmufiaelrsqms2rh',
        'td_cookie': '2008652607',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://csol.qdio.ac.cn/XueHuidt.aspx?funId=185',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    params = (
        ('page', '2'),
        ('funId', '185'),
    )

    response = requests.get('http://csol.qdio.ac.cn/XueHuidt.aspx', headers=headers, params=params, cookies=cookies,
                            verify=False)
    html_text = response.text
    # print(html_text)
    # breakpoint()
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, page):

    cookies = {
        'clientlanguage': 'zh_CN',
        'JSESSIONID': '34E8B6D742CB0A89E6EAAC98C6D1B0E4',
        'td_cookie': '2014716140',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.cso.org.cn/zfsjxw/index.jhtml',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers,
                            cookies=cookies, verify=False)
    response.encoding = 'utf-8'
    return response.text


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time ):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0]
        except:
            news_title = ''

        news_abstract = news_title
        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0]
        except:
            news_publish_time = '2022-01-01'
        # try:
        #     news_imgs = 'http://www.gsc.org.cn/' + part_nodes[i].xpath('./a/img/@src')
        # except:
        #     news_imgs = ''

        # print('http://csmpg.gyig.cas.cn/xhyw' + part_nodes[i].xpath('./a/@href')[0].strip().replace('./', '/'))
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            # if 'html' in url_part2:
            news_page_url = url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    selector = etree.HTML(html_text)
    # part1
    try:
        part1_nodes = selector.xpath('//*[@class="lists-con"]//li')
        xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        print('Classify： {} Part1 has not content: {}'.format(news_classify, e))

    # part2
    # try:
    #     part2_nodes = selector.xpath('//*[@width="50%"]')
    #     xpath_data(part2_nodes, page, supFlash, news_classify, science_system, mongo, account_name, project_time)
    # except:
    #     print('Classify： {} Part2 has not content: {}'.format(news_classify, e))


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="content"]//p//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'>来源：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''
    try:
        click_count = re.findall(r'>访问数量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学会新闻-总会新闻': 'http://www.cso.org.cn/zfsjxw/index_1.jhtml',
        '首页-学会新闻-学会动态': 'http://www.cso.org.cn/hyxhdt/index_1.jhtml',
        '首页-学会新闻-分支机构新闻': 'http://www.cso.org.cn/fzjgxw/index_1.jhtml',
        '首页-学会新闻-其它新闻': 'http://www.cso.org.cn/qtxw/index_1.jhtml',
        '首页-科学普及-海洋科普委员会': 'http://www.cso.org.cn/hykpgzwyh/index_1.jhtml',
        '首页-科学普及-全国海洋科普教育基地': 'http://www.cso.org.cn/qghykpjd/index_1.jhtml',
        '首页-科学普及-全国海洋科普教育基地-基地相关文件': 'http://www.cso.org.cn/jdxgwj/index_1.jhtml',
        '首页-科学普及-全国海洋科普教育基地-基地名录': 'http://www.cso.org.cn/jdmlv/index_1.jhtml',
        '首页-科学普及-全国海洋科普教育基地-特色活动': 'http://www.cso.org.cn/jdtshd/index_1.jhtml',
        '首页-科学普及-海洋科普特色活动': 'http://www.cso.org.cn/hykptshd/index_1.jhtml',
        '首页-科学普及-海洋科普特色活动-科普进校园': 'http://www.cso.org.cn/hykpjxy/index_1.jhtml',
        '首页-科学普及-海洋科普特色活动-科普进内陆': 'http://www.cso.org.cn/hykpjnl/index_1.jhtml',
        '首页-科学普及-海洋科普特色活动-公益慢跑': 'http://www.cso.org.cn/dxsgymp/index_1.jhtml',
        '首页-科学普及-海洋科普特色活动-科技进展评选': 'http://www.cso.org.cn/kjjzpx/index_1.jhtml',
        '首页-科学普及-海洋科普资料库-科普图文': 'http://www.cso.org.cn/kptw/index_1.jhtml',
        '首页-科技奖励-海洋科普资料库-历年获奖': 'http://www.cso.org.cn/lngjqk/index_1.jhtml',
        '首页-公告信息': 'http://www.cso.org.cn/ggxx/index_1.jhtml',
        '首页-政策法规-国家政策': 'http://www.cso.org.cn/gjzc/index_1.jhtml',
        '首页-政策法规-学会相关文件': 'http://www.cso.org.cn/xhzcjglgd/index_1.jhtml',
    }
    # supFlash = get_supflash()
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 10):
            url = value.replace('index_1', 'index_' + str(page))
            html_text = get_html(url, page)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name, science_system, mongo, project_time,
                     start_time)
            time.sleep(1)
        time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'A-16 中国海洋学会'
    start_run(project_time, start_time, account_name)