# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-11-01

import re
import io
import sys
import time
import json
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash():
    url = 'http://www.cssr.org.cn/'

    cookies = {
        'PHPSESSID': 'b3bv2uu7lvhanqc1vff8v0jgv1',
        'Hm_lvt_affc2d33241916b852ac72b49e19a521': '1667282016',
        'td_cookie': '1506716259',
        'Hm_lpvt_affc2d33241916b852ac72b49e19a521': '1667282538',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.cssr.org.cn/c141',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }

    params = (
        ('page', '2'),
    )
    response = requests.get(url, headers=headers, params=params, cookies=cookies, verify=False)
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, page, supFlash):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.cncos.org/Content/index/catid/20.html?&p={}'.format(page),
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }
    cookies = {
        'td_cookie': supFlash,
        'PHPSESSID': 'b3bv2uu7lvhanqc1vff8v0jgv1',
        'Hm_lvt_affc2d33241916b852ac72b49e19a521': '1667282016',
        'Hm_lpvt_affc2d33241916b852ac72b49e19a521': '1667282538',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)
    return response.text


def get_data(html_text, news_classify, account_name, science_system, start_time, mongo, project_time, supFlash):

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    selector = etree.HTML(html_text)
    nodes = selector.xpath('//*[@class="cols-1"]/li')
    # print(nodes)
    # breakpoint()
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    for i in range(len(nodes)):
        news_title = nodes[i].xpath('./a/text()')[0]
        news_abstract = nodes[i].xpath('./a/text()')[0]
        news_publish_time = nodes[i].xpath('./span/text()')[0].strip().replace('/', '-')
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_publish_stamp)
        # breakpoint()
        news_page_url = 'http://www.cssr.org.cn/' + nodes[i].xpath('./a/@href')[0]

        if int(news_publish_stamp) >= int(start_time_stamp):
            news_author, news_imgs, news_content_type, news_content, source,\
            read_count, click_count = get_page_content(news_page_url, supFlash)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Title :{} Publish: {} Was Finished!'.format(news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url, supFlash):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': news_page_url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }
    cookies = {
        'PHPSESSID': 'b3bv2uu7lvhanqc1vff8v0jgv1',
        'Hm_lvt_affc2d33241916b852ac72b49e19a521': '1667282016',
        'Hm_lpvt_affc2d33241916b852ac72b49e19a521': '1667283550',
        'td_cookie': supFlash,
    }

    html_response = requests.get(news_page_url, headers=headers, cookies=cookies, verify=False)
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="article"]/p//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'>文章来源：(.*?)<', content_text, re.M | re.S)[0]
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览次数：(.*?)', content_text, re.M | re.S)[0]
    except:
        read_count = ''
    try:
        click_count = re.findall(r'阅读次数：(.*?)', content_text, re.M | re.S)[0]
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学会动态-学会新闻': 'http://www.cssr.org.cn/c108?page=1',
        '首页-学会动态-通知公告': 'http://www.cssr.org.cn/c107?page=1',
        '首页-学会概况-专业委员会': 'http://www.cssr.org.cn/c103?page=1',
        '首页-学会概况-工作委员会': 'http://www.cssr.org.cn/c104?page=1',
        '首页-学会概况-政策制度': 'http://www.cssr.org.cn/c105?page=1',
        '首页-学术交流-近期会议': 'http://www.cssr.org.cn/c112?page=1',
        '首页-学术交流-中国空间科学大会': 'http://www.cssr.org.cn/c113?page=1',
        '首页-学术交流-专委会学术会议': 'http://www.cssr.org.cn/c114?page=1',
        '首页-学术交流-国际会议': 'http://www.cssr.org.cn/c115?page=1',
        '首页-科学普及-科普活动': 'http://www.cssr.org.cn/c126?page=1',
        '首页-科学普及-科普图书': 'http://www.cssr.org.cn/c127?page=1',
        '首页-科学普及-科普文章': 'http://www.cssr.org.cn/c143?page=1',
        '首页-科学普及-科普视频': 'http://www.cssr.org.cn/c144?page=1',
        '首页-人才举荐-人才举荐': 'http://www.cssr.org.cn/c117?page=1',
        '首页-人才举荐-青托风采': 'http://www.cssr.org.cn/c151?page=1',
        '首页-人才举荐-青托人才成长故事': 'http://www.cssr.org.cn/c147?page=1',
        '首页-人才举荐-最美科技工作者': 'http://www.cssr.org.cn/c118?page=1',
        '首页-科技奖励-中国空间科学学会科技奖': 'http://www.cssr.org.cn/c122?page=1',
        '首页-学会会员-会员风采': 'http://www.cssr.org.cn/c131?page=1',
        '首页-党建强会-党建动态': 'http://www.cssr.org.cn/c137?page=1',
        '首页-党建强会-学习园地': 'http://www.cssr.org.cn/c138?page=1',
        '首页-前言动态-前言动态': 'http://www.cssr.org.cn/c141?page=1',
        '首页-科技咨询': 'http://www.cssr.org.cn/c145?page=1',

    }
    supFlash = get_supflash()
    for key, value in urls.items():
        news_classify = key
        url = value
        # print(key, value)
        # break
        for page in range(1, 3):
            url = url.replace('?p=1', '?p=' + str(page))
            html_text = get_html(url, page, supFlash)
            # print(html_text)
            # breakpoint()
            result = get_data(html_text, news_classify, account_name, science_system,
                              start_time, mongo, project_time, supFlash)
            time.sleep(1)
            if not result:
                break


if __name__ == '__main__':
    project_time = '2022-Q3'
    start_time = '2022-07-31'
    account_name = 'A-09 中国空间科学学会'
    start_run(project_time, start_time, account_name)