# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-11-24
import random
import re
import io
import sys
import time
import json
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(page, catId):
    cookies = {
        'JSESSIONID': '4F38B3A56C1C3A942A7338BD50F8C3B8',
        'Hm_lvt_4df70a4d04bcb11f2839a29d0d99d692': '1669277912',
        'Hm_lpvt_4df70a4d04bcb11f2839a29d0d99d692': '1669277917',
    }

    headers = {
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'https://www.cmes.org',
        'Referer': 'https://www.cmes.org/news/dynamic/index.html',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
        'sec-ch-ua': '"Google Chrome";v="107", "Chromium";v="107", "Not=A?Brand";v="24"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    data = {
        'pageSize': '10',
        'current': page,
        'categoryId': catId,
        'isAttribute': '1'
    }

    response = requests.post('https://www.cmes.org/cmes/content/list.json', headers=headers, cookies=cookies, data=data)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    content = json.loads(html_text)
    try:
        ree_data(content, news_classify, science_system, mongo,
                 account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
             account_name, project_time, start_time):
    results = content['result']['records']
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for item in results:
        try:
            news_title = item.get('TITLE')
        except:
            news_title = ''
        try:
            news_abstract = news_title
        except:
            news_abstract = ''

        try:
            news_publish_time = item.get('PUBLISH_DATE')
        except:
            news_publish_time = '2022-01-01'

        try:
            category_code = item.get('CATEGORY_CODE')
        except:
            category_code = ''

        try:
            contentId = item.get('CONTENT_ID')
        except:
            contentId = ''

        news_publish_stamp = time.mktime(time.strptime(news_publish_time,
                                                       '%Y-%m-%d %H:%M:%S'))

        # print(news_title)
        # print(news_publish_time)
        # print(category_code)
        # print(contentId)
        # print(news_publish_stamp)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            if category_code == 'dynamic':
                news_page_url = 'https://www.cmes.org/dynamic/{}.html'.format(contentId)

            elif category_code == 'notice':
                news_page_url = 'https://www.cmes.org/notice/{}.html'.format(contentId)
            else:
                pass
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):
    # html_response = requests.get(news_page_url, verify=False)
    html_response = requests.get(news_page_url)
    # if 'www.cstp.org.cn' not in news_page_url:
    #     html_response.encoding = 'gb2312'
    # else:
    html_response.encoding = 'utf-8'

    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    try:
        news_content = selector_page.xpath('//*[@class="container"]///p/text()')
        # news_content = selector_page.xpath('//*[@class="cont_txt"]/span/text()')
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="content"]//p//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'文章来源：</span><span class="time-blue">(.*?)</span>', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'阅读量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻-学会动态': 'b15f7ac60b044356af55cda934c5f9a6',
        '首页-新闻-通知公告': 'b8627312414b45bf80f806217e978fb8',
        '首页-标准': 'a901dc783ff840ce94e929ce3bf13ea7',
        '首页-科普-科普活动': '6eb30e74f9ef488b94cd420a377ee741',
        '首页-教育评价-培训-国家级继续教育基地': '181902a0aecd4d248fbffaf37a20f75d',
        '首页-教育评价-培训-北京机械进修学院': '003c112872bf4c2b9d9448e7371fedaa',
        '首页-教育评价-评价-工程能力评价': '1d9616b803d341cda316a4ff5c74b649',
        '首页-教育评价-评价-认证通过名单': '3cfa8ef83e79443cba1ee0c78d1234e2',
        '首页-教育评价-评价-工作动态': '249d817968994e9cbe4ea2288a5a24da',
        '首页-奖励-中国机械工程学会科技奖': 'b63ae1dd63ff4ca58542552b7c375388',
        '首页-奖励-通知公告': '19162b8a089b4302a79b1c24074a6c03',
        '首页-展览-展会动态-展会预报': '642081ff5a054da5a1b55cc936381eda',
        '首页-展览-展会动态-SCHWEISSEN&SCHNEID': '1311d285b0d044218d689fa2827a95d2',
        '首页-展览-展会动态-智能制造国际会议': 'a613fedd1de745728f2effbd553db7c5',
        '首页-公共服务-科技评估': 'c1c0e1e8627e4d59ad14cb705a716986',
        '首页-公共服务-科创中国动态': 'a48d0d1e62e24891b0a1301d6446032d',
        '首页-公共服务-政策法规': 'fa608c81686f486aa0434344534b71d5',
        '首页-公共服务-咨询活动': '9725e90c8dc146feb0003dd591bf4a8c',
        '首页-公共服务-对外交流': '2e516dcd40c5455da985b7feb078a238',
        '首页-党建-通知公告': '5b433b2e9d4641d6ab082fb1e7f84f9f',
        '首页-党建-工作动态': '3f6ec7602ae64bdc85fcf936b48a4dd8',
        '首页-党建-相关资料查询': '50fa8edf750e4aceb9167086314a6e41',
        '首页-党建-二十大专题-权威解读': 'dde54a84663845859fc8c4b01d86c900',
        '首页-党建-二十大专题-学习动态': '449961e069d64c26897ea06cac6b9ade',
    }
    for key, value in urls.items():
        news_classify = key
        catId = value
        for page in range(1, 5):
            html_text = get_html(page, catId)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)
            time.sleep(1)
            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'A-46G 国际动物学会'
    start_run(project_time, start_time, account_name)