# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-11-09
import random
import re
import io
import sys
import time
import json
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    cookies = {
        'JSESSIONID': '9BAD7CAC794243C23C4AD3EFAB46FF91',
    }

    headers = {
        'Accept': 'application/xml, text/xml, */*; q=0.01',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'http://www.csm1952.org.cn',
        'Referer': 'http://www.csm1952.org.cn/col/col5531/index.html?uid=20852&pageNum=5',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Mobile Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    params = (
        ('startrecord', '1'),
        ('endrecord', '120'),
        ('perpage', '40'),
    )

    data = {
        'col': '1',
        'appid': '1',
        'webid': '62',
        'path': '/',
        'columnid': '5531',
        'sourceContentType': '1',
        'unitid': '20852',
        'webname': '\u4E2D\u56FD\u5FAE\u751F\u7269\u5B66\u4F1A',
        'permissiontype': '0'
    }

    response = requests.post(url, headers=headers, params=params,
                             cookies=cookies, data=data, verify=False)

    response.encoding = 'utf-8'
    return response.text

def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    content = re.findall(r'<record><!\[CDATA\[(.*?)\]\>', html_text, re.M | re.S)
    # print(content)
    # breakpoint()
    # part1
    try:
        # print(part1_nodes)
        # breakpoint()
        ree_data(content, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        print('Classify： {} Part1 has not content: {}'.format(news_classify, e))



def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(content)):
        try:
            news_title = re.findall(r'class="rfl">(.*?)</div>', content[i])[0]
        except:
            news_title = ''

        news_abstract = news_title

        try:
            news_publish_time = re.findall(r'class="rfr">(.*?)</div>', content[i])[0]
        except:
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))


        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = re.findall(r'class="li_link" href=(.*?)>', content[i])[0].replace('"', '')

            news_page_url = 'http://www.csm1952.org.cn' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)

    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="view_tt"]//p//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'>来源：(.*?)　时间', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻动态-学会动态': 'http://www.csm1952.org.cn/col/col5531/index.html',
        '首页-新闻动态-通知公告': 'http://www.csm1952.org.cn/col/col5530/index.html',
        '首页-新闻动态-学会通讯': 'http://www.csm1952.org.cn/col/col5534/index.html',
        '首页-新闻动态-地方学位': 'http://www.csm1952.org.cn/col/col5774/index.html',
        '首页-新闻动态-图片新闻': 'http://www.csm1952.org.cn/col/col6602/index.html',
        '首页-党建强会-党建动态': 'http://www.csm1952.org.cn/col/col5532/index.html',
        '首页-党建强会-科学家精神': 'http://www.csm1952.org.cn/col/col5546/index.html',
        '首页-智库建设-智库动态': 'http://www.csm1952.org.cn/col/col5556/index.html',
        '首页-智库建设-专家队伍': 'http://www.csm1952.org.cn/col/col5561/index.html',
        '首页-学术活动-学术动态': 'http://www.csm1952.org.cn/col/col5553/index.html',
        '首页-学术活动-对外交流': 'http://www.csm1952.org.cn/col/col5746/index.html',
        '首页-学术活动-学术年会': 'http://www.csm1952.org.cn/col/col5535/index.html',
        '首页-学术活动-会议剪影': 'http://www.csm1952.org.cn/col/col5555/index.html',
        '首页-科普之窗': 'http://www.csm1952.org.cn/col/col5540/index.html',
        '首页-学术资讯': 'http://www.csm1952.org.cn/col/col5539/index.html',
        '首页-科学普及-科普之窗': 'http://www.csm1952.org.cn/col/col5550/index.html',
        '首页-科学普及-科普知识': 'http://www.csm1952.org.cn/col/col5551/index.html',
    }
    for key, value in urls.items():
        news_classify = key
        url = value
        html_text = get_html(url)
        # print(html_text)
        # breakpoint()
        get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time)
        time.sleep(1)
        time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'A-21 中国微生物学会'
    start_run(project_time, start_time, account_name)