# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-11-21
import random
import re
import io
import sys
import time
import json
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, uid):

    headers = {
        'authority': 'www.orsc.org.cn',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'accept-language': 'zh-CN,zh;q=0.9',
        'cookie': 'XSRF-TOKEN=eyJpdiI6IlloQUpJcGxUT3hLbEtDODZDZDZvN3c9PSIsInZhbHVlIjoiNlNjbllydTlnNmViQjNVUzdFZkFKalBDa1U4RWNDakI5WGxNeDNMeUl6RVNQVTJPZzZHTmtvaE9NN3EyTGNxSVZ5enZKSjR6eWtDUndYRCtpbWoxTlFsRjZyWnkvMGZ1ejlSbGFzc0NCK1VUQnZOSVVQQUVaVTdoaDlCNVF0Z0siLCJtYWMiOiJlMTk2NmFhOGYyMzcwN2QyZGU5NjMzMTU3MzcxZmY5MmZhYTE1OGFkZDgyNDgwMTg2YmY4NjllOGQwMWYyNGU3In0%3D; laravel_session=eyJpdiI6Ii9HZUV5UDlnMHZhNGJtSk9VQ0ltK0E9PSIsInZhbHVlIjoidkhrN3pVeXZVTkNieUdKQjlXeW9xOWdnSVpJMUFjc1V6MUN5Z2ttME1EMkhjbG91NUNtU3MwbVFjK3JGekZrZ2dqbWhNWXZhZVZvTUI2RWpjNkhubUpjUTVPR1RpTWg0VHFoaDZVY0VIdy8rODQxMVhRUkc3eGxrUGN0R0paWEoiLCJtYWMiOiI1ZGQ3OTllYmNiNjAzZjQ5MzFhNTRhZTRjNzY4YzY3ODZkYWI3YTQxOGEyMDVjM2RkNzViN2NmMThhYjQ5YTFhIn0%3D',
        'referer': 'https://www.orsc.org.cn/article/list?id={}'.format(uid),
        'sec-ch-ua': '"Google Chrome";v="107", "Chromium";v="107", "Not=A?Brand";v="24"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36',
    }

    # response = requests.get(url, headers=headers, verify=False)
    response = requests.get(url, headers=headers)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    selector = etree.HTML(html_text)
    # print(content)
    # breakpoint()
    try:
        part1_nodes = selector.xpath('//*[@class="item"]')
        xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('.//a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('.//p/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('.//a/span/text()')[0].strip()
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('.//a/@href')[0]
            if 'mp.weixin.qq.com' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.orsc.org.cn' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    # html_response = requests.get(news_page_url, verify=False)
    html_response = requests.get(news_page_url)
    # if 'www.cstp.org.cn' not in news_page_url:
    #     html_response.encoding = 'gb2312'
    # else:
    html_response.encoding = 'utf-8'

    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    try:
        news_content = selector_page.xpath('//*[@class="container"]///p/text()')
        # news_content = selector_page.xpath('//*[@class="cont_txt"]/span/text()')
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="container"]//p//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'>来源：(.*?)　时间', content_text, re.M | re.S)[0].strip()
    except:
        source = ''


    try:
        read_count = re.findall(r'阅读量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    
    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '新闻资讯-学会动态': 'https://www.orsc.org.cn/article/list?id=5&page=1',
        '新闻资讯-通知公告': 'https://www.orsc.org.cn/article/list?id=6&page=1',
        '新闻资讯-前言进展': 'https://www.orsc.org.cn/article/list?id=7&page=1',
        '新闻资讯-青托工程': 'https://www.orsc.org.cn/article/list?id=47&page=1',
        '党建强会': 'https://www.orsc.org.cn/article/list?id=11&page=1',
        '运筹档案-学术年会': 'https://www.orsc.org.cn/article/list?id=13&page=1',
        '运筹档案-会议纪要': 'https://www.orsc.org.cn/article/list?id=14&page=1',
        '运筹档案-运筹历史': 'https://www.orsc.org.cn/article/list?id=15&page=1',
        '科普教育-科普动态': 'https://www.orsc.org.cn/article/list?id=18&page=1',
        '科普教育-科普资源': 'https://www.orsc.org.cn/article/list?id=19&page=1',
        '科普教育-教育动态': 'https://www.orsc.org.cn/article/list?id=20&page=1',
        '科普教育-教育资源': 'https://www.orsc.org.cn/article/list?id=21&page=1',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            uid = re.findall(r'id=(.*?)', value)[0]
            url = value.replace('page=1', 'page=' + str(page))
            html_text = get_html(url, uid)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time)
            time.sleep(1)
            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'A-39 中国运筹学会'
    start_run(project_time, start_time, account_name)