# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-10-25

import re
import io
import sys
import time
import json
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash():
    url = 'http://www.cncos.org/Content/index/catid/20.html?&p=4'
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.cncos.org/Content/index/catid/20.html?&p=4',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, verify=False)
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, page, supFlash):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.cncos.org/Content/index/catid/20.html?&p={}'.format(page),
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }
    cookies = {
        'td_cookie': supFlash,
        'PHPSESSID': 'nlss62tn73el4eierftqf99ba6',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)
    return response.text


def get_data(html_text, news_classify, account_name, science_system, start_time, mongo, project_time, supFlash):

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    selector = etree.HTML(html_text)
    nodes = selector.xpath('//*[@class="actlist"]/li')
    # print(nodes)
    # breakpoint()
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify',
                      'account_name', 'science_system', 'project_time']

    for i in range(len(nodes)):
        news_title = nodes[i].xpath('.//p/text()')[0]
        news_abstract = nodes[i].xpath('.//p/text()')[0]
        news_publish_time = nodes[i].xpath('.//span/text()')[0].strip()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_publish_stamp)
        # breakpoint()
        news_page_url = 'http://www.cncos.org/' + nodes[i].xpath('./a/@href')[0]

        if int(news_publish_stamp) >= int(start_time_stamp):
            news_author, news_imgs, news_content_type, news_content, source,\
            read_count, click_count = get_page_content(news_page_url, supFlash)

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            mongo.insert_one(news_dict_content)
            logging.warning('Title :{} Publish: {} Was Finished!'.format(news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url, supFlash):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': news_page_url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }
    cookies = {
        'td_cookie': supFlash,
        'PHPSESSID': 'sviinfuvdipq3jvqp33lboi667',
    }

    html_response = requests.get(news_page_url, headers=headers, cookies=cookies, verify=False)
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="detail"]//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'文章来源：(.*?)', content_text, re.M | re.S)[0]
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览次数：(.*?)', content_text, re.M | re.S)[0]
    except:
        read_count = ''
    try:
        click_count = re.findall(r'阅读次数：(.*?)', content_text, re.M | re.S)[0]
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time):
    mongo = mongodb()
    start_time = '2022-07-31'
    account_name = 'A-04 中国光学学会'
    science_system = read_science_account(account_name)

    urls = {
        '学会动态-学会工作信息': 'http://www.cncos.org/Content/index/catid/761.html?&p=1',
        '学会动态-专委会地方学会动态': 'http://www.cncos.org/Content/index/catid/20.html?&p=1',
        '学会动态-会员风采': 'http://www.cncos.org/Content/index/catid/23.html?&p=1',
        '学会动态-行业动态': 'http://www.cncos.org/Content/index/catid/17.html?&p=1',
        '会议展览-国内会议': 'http://www.cncos.org/Content/index/catid/12.html?&p=1',
        '会议展览-国际会议': 'http://www.cncos.org/Content/index/catid/13.html?&p=1',
        '会议展览-专业委员会会议': 'http://www.cncos.org/Content/index/catid/768.html?&p=1',
        '会议展览-地方学会会议': 'http://www.cncos.org/Content/index/catid/769.html?&p=1',
        '会议展览-国内展览': 'http://www.cncos.org/Content/index/catid/15.html?&p=1',
        '会议展览-国际展览': 'http://www.cncos.org/Content/index/catid/14.html?&p=1',
        '科普与讲座-国际光日动态': 'http://www.cncos.org/Content/index/catid/765.html?&p=1',
        '科普与讲座-光学科普': 'http://www.cncos.org/Content/index/catid/27.html?&p=1',
        '科普与讲座-中国科协年会科普': 'http://www.cncos.org/Content/index/catid/45.html?&p=1',
        '科普与讲座-科普图书': 'http://www.cncos.org/Content/index/catid/29.html?&p=1',
        '科普与讲座-地方专委会科普活动': 'http://www.cncos.org/Content/index/catid/755.html?&p=1',
        '科普与讲座-中学生科普': 'http://www.cncos.org/Content/index/catid/69.html?&p=1',
        '科技奖励-王大珩光学奖': 'http://www.cncos.org/Content/index/catid/31.html?&p=1',
        '科技奖励-中国光学学会光学科技奖': 'http://www.cncos.org/Content/index/catid/33.html?&p=1',
        '科技奖励-其他科技奖项': 'http://www.cncos.org/Content/index/catid/35.html?&p=1',
        '会员服务-个人会员': 'http://www.cncos.org/Content/index/catid/787.html?&p=1',
        '会员服务-团体会员': 'http://www.cncos.org/Content/index/catid/789.html?&p=1',
        '公告-通知公告': 'http://www.cncos.org/Content/index/catid/42.html?&p=1',
        '国际光日-国际光日动态': 'http://www.cncos.org/Content/index/catid/765.html?&p=1',
        '学会党建-党的二十大': 'http://www.cncos.org/Content/index/catid/791.html?&p=1',
        '学会党建-学会党建': 'http://www.cncos.org/Content/index/catid/786.html?&p=1',

    }
    supFlash = get_supflash()
    for key, value in urls.items():
        news_classify = key
        url = value
        # print(key, value)
        # break
        for page in range(1, 3):
            url = url.replace('&p=1', '&p=' + str(page))
            html_text = get_html(url, page, supFlash)
            # print(html_text)
            # breakpoint()
            result = get_data(html_text, news_classify, account_name, science_system,
                              start_time, mongo, project_time, supFlash)
            time.sleep(1)
            if not result:
                break


if __name__ == '__main__':
    project_time = '2022-Q3'
    start_run(project_time)