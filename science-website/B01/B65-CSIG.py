# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-01-20
import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url,  verify=False)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="item-ul"]/li')
        xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/@title')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/@title')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0].strip()
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'https:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://csig.org.cn' + url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="text-body"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="text-body"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'<span class="news_top_lyname">(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'<span class="news_top_zzname">(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击量：<span id="clicks">(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-关于CSIG': 'http://csig.org.cn/list/aboutcsig/1',
        '首页-学会动态-学会新闻': 'http://csig.org.cn/list/xuehuixinwen/1',
        '首页-学会动态-活动预告': 'http://csig.org.cn/list/huodongyugao/1',
        '首页-学会动态-求职招聘': 'http://csig.org.cn/list/qiuzhizhaopin/1',
        '首页-学会动态-通知公告': 'http://csig.org.cn/list/tongzhigonggao/1',
        '首页-学会动态-CSIG专访': 'http://csig.org.cn/list/tongzhigonggao/1',
        '首页-会员-会员风采': 'http://csig.org.cn/list/huiyuanchengjiu/1',
        '首页-学会活动-图像图形学术会议': 'http://csig.org.cn/list/txtxxxshy/1',
        '首页-学会活动-青年科学家论坛': 'http://csig.org.cn/list/qnkxjlt/1',
        '首页-学会活动-专委学术活动': 'http://csig.org.cn/list/zwhxshd/1',
        '首页-学会活动-CSIG中国行': 'http://csig.org.cn/list/zwhxshd/1',
        '首页-学会活动-CSIG讲习班': 'http://csig.org.cn/list/CSIGtxtxxkqyjxb/1',
        '首页-学会活动-中国图像图形大会': 'http://csig.org.cn/list/zhongguotuxiangtuxingdahui/1',
        '首页-学会活动-CSIG挑战赛': 'http://csig.org.cn/list/tiaozhansai/1',
        '首页-学会活动-CSIG企业行': 'http://csig.org.cn/list/CSIGtxtxqyx/1',
        '首页-学会活动-CSIG图像图形高峰论坛': 'http://csig.org.cn/list/txtxgflt/1',
        '首页-学会活动-CSIG云讲堂': 'http://csig.org.cn/list/yjt/1',
        '首页-科普工作-科普新闻': 'http://csig.org.cn/list/kepuxinwen/1',
        '首页-科普工作-科普团队': 'http://csig.org.cn/list/keputuandui/1',
        '首页-科普工作-2022全国科技周': 'http://csig.org.cn/list/quanguokejizhou/1',
        '首页-科普工作-2022全国科普日': 'http://csig.org.cn/list/quanguokeopuri/1',
        '首页-奖励与评价-奖励条例': 'http://csig.org.cn/list/csigkxjsj/1',
        '首页-奖励与评价-奖励动态': 'http://csig.org.cn/list/csigyxbsxwlwj/1',
        '首页-奖励与评价-获奖名单': 'http://csig.org.cn/list/CSIGsqynkxjj/1',
        '首页-奖励与评价-中国科协青年人才托举': 'http://csig.org.cn/list/qingtuo/1',
        '首页-奖励与评价-成果评价': 'http://csig.org.cn/list/CSIGjianglicgjd/1',
        '首页-奖励与评价-CSIG奖励访谈': 'http://csig.org.cn/list/jianglifangtan/1',
        '首页-党建强会-二十大精神': 'http://csig.org.cn/list/dangjianqianghui/1',

    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            url = value.replace('/1', '/' + str(page))
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)

            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'B-65 中国图象图形学学会'
    start_run(args.projectname, args.sinceyear, account_name)
