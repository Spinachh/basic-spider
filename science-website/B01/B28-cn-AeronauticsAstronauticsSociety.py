# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-12-11
import random
import re
import io
import sys
import time
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    content = re.findall(r'<record><!\[CDATA\[(.*?)\]\>', html_text, re.M | re.S)
    try:
        ree_data(content, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(content)):
        try:
            news_title = re.findall(r'target="_blank">(.*?)</a></b>', content[i])[0]
        except:
            news_title = ''

        try:
            news_abstract = re.findall(r'</b>								<span>(.*?)\.\.\.<a ', content[i])[0]
        except:
            news_abstract = ''

        try:
            news_publish_time = re.findall(r'<p><b><a href="/art/(.*?)/art_', content[i])[0].replace('/', '-')
        except:
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = re.findall(r'<p><b><a href="(.*?)"', content[i])[0]
            if 'http://www.csaa.org.cn/' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.csaa.org.cn' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="contect"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="contect"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源:<a href="" target="_blank">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻资讯': 'http://www.csaa.org.cn/col/col470/index.html?uid=2203&pageNum=1',
        '首页-信息公开-学会动态': 'http://www.csaa.org.cn/col/col410/index.html?uid=2203&pageNum=1',
        '首页-信息公开-通知公告': 'http://www.csaa.org.cn/col/col411/index.html?uid=2203&pageNum=1',
        '首页-信息公开-工作文件': 'http://www.csaa.org.cn/col/col427/index.html?uid=2203&pageNum=1',
        '首页-信息公开-规划计划': 'http://www.csaa.org.cn/col/col428/index.html?uid=2203&pageNum=1',
        '首页-关于学会-荣誉资质': 'http://www.csaa.org.cn/col/col424/index.html?uid=2203&pageNum=1',
        '首页-关于学会-出版物': 'http://www.csaa.org.cn/col/col457/index.html?uid=2203&pageNum=1',
        '首页-关于学会-管理条例': 'http://www.csaa.org.cn/col/col426/index.html?uid=2203&pageNum=1',
        '首页-党建工作-管理条例': 'http://www.csaa.org.cn/col/col416/index.html?uid=2203&pageNum=1',
        '首页-数字图书管': 'http://www.csaa.org.cn/col/col414/index.html?uid=2203&pageNum=1',

    }
    for key, value in urls.items():
        news_classify = key
        url = value
        html_text = get_html(url)
        # print(html_text)
        # breakpoint()
        get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time)
        time.sleep(1)
        time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'B-28 中国航空学会'
    start_run(project_time, start_time, account_name)