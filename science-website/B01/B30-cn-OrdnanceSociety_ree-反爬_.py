# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-12-12
import random
import re
import io
import sys
import time
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        # 'If-Modified-Since': 'Wed, 30 Nov 2022 02:24:36 GMT',
        # 'If-None-Match': 'W/"7eb2-5eea6ce428100"',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, verify=False)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    content = re.findall(r'<record><!\[CDATA\[(.*?)\]\>', html_text, re.M | re.S)
    try:
        ree_data(content, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(content)):
        try:
            news_title = re.findall(r'html">(.*?)</a></h1>', content[i])[0]
        except:
            news_title = ''

        news_abstract = news_title

        try:
            news_publish_time = re.findall(r'发布时间：</b>(.*?)</p>', content[i])[0]
        except:
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = re.findall(r'<a target="_blank" href="(.*?)"', content[i])[0]
            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://bgxh.norincogroup.com.cn' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        # 'If-Modified-Since': 'Wed, 30 Nov 2022 02:24:36 GMT',
        # 'If-None-Match': 'W/"51c0-5eea6ce428100"',
        # 'Referer': 'http://bgxh.norincogroup.com.cn/col/col1766/index.html?uid=11896&pageNum=1',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    }

    html_response = requests.get(news_page_url, headers=headers,
                            verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="detail"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="detail"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源:<a href="" target="_blank">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻中心-学会新闻': 'http://bgxh.norincogroup.com.cn/col/col1766/index.html?uid=11896&pageNum=1',
        '首页-新闻中心-集团新闻': 'http://bgxh.norincogroup.com.cn/col/col1767/index.html?uid=11896&pageNum=1',
        '首页-新闻中心-国资新闻': 'http://bgxh.norincogroup.com.cn/col/col1768/index.html?uid=11896&pageNum=1',
        '首页-新闻中心-通知公告': 'http://bgxh.norincogroup.com.cn/col/col1769/index.html?uid=11896&pageNum=1',
        '首页-新闻中心-专题专栏': 'http://bgxh.norincogroup.com.cn/col/col1770/index.html?uid=11896&pageNum=1',
        '首页-走进学会-学会荣誉': 'http://bgxh.norincogroup.com.cn/col/col1764/index.html?uid=11896&pageNum=1',
        '首页-学术交流-国内学术交流': 'http://bgxh.norincogroup.com.cn/col/col1771/index.html?uid=11896&pageNum=1',
        '首页-学术交流-国际学术交流': 'http://bgxh.norincogroup.com.cn/col/col1772/index.html?uid=11896&pageNum=1',
        '首页-学术交流-专委会学术交流': 'http://bgxh.norincogroup.com.cn/col/col1773/index.html?uid=11896&pageNum=1',
        '首页-学术交流-地方学会学术交流': 'http://bgxh.norincogroup.com.cn/col/col1774/index.html?uid=11896&pageNum=1',
        '首页-组织工作-学会活动': 'http://bgxh.norincogroup.com.cn/col/col1775/index.html?uid=11896&pageNum=1',
        '首页-组织工作-章程条例': 'http://bgxh.norincogroup.com.cn/col/col1776/index.html?uid=11896&pageNum=1',
        '首页-组织工作-组织建设': 'http://bgxh.norincogroup.com.cn/col/col1777/index.html?uid=11896&pageNum=1',
        '首页-组织工作-奖励评选': 'http://bgxh.norincogroup.com.cn/col/col1778/index.html?uid=11896&pageNum=1',
        '首页-科学普及-科普教育基地': 'http://bgxh.norincogroup.com.cn/col/col1779/index.html?uid=11896&pageNum=1',
        '首页-科学普及-科普讲座': 'http://bgxh.norincogroup.com.cn/col/col1780/index.html?uid=11896&pageNum=1',
        '首页-科学普及-国防教育': 'http://bgxh.norincogroup.com.cn/col/col1781/index.html?uid=11896&pageNum=1',
        '首页-咨询评估-咨询活动': 'http://bgxh.norincogroup.com.cn/col/col1782/index.html?uid=11896&pageNum=1',
        '首页-党群工作-咨询活动': 'http://bgxh.norincogroup.com.cn/col/col1757/index.html?uid=11896&pageNum=1',
        '首页-继续教育-工程教育认证': 'http://bgxh.norincogroup.com.cn/col/col2492/index.html?uid=11896&pageNum=1',
        '首页-继续教育-短期培训': 'http://bgxh.norincogroup.com.cn/col/col2493/index.html?uid=11896&pageNum=1',
        '首页-继续教育-全国继续教育工作群': 'http://bgxh.norincogroup.com.cn/col/col2491/index.html?uid=11896&pageNum=1',

    }
    for key, value in urls.items():
        news_classify = key
        url = value
        html_text = get_html(url)
        # print(html_text)
        # breakpoint()
        get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time)
        time.sleep(1)
        time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'B-30 中国兵工学会'
    start_run(project_time, start_time, account_name)