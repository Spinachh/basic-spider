# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-01-13
import random
import re
import io
import sys
import time
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="cols-1"]/li')
        xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''
        # print(news_title)
        # breakpoint()
        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0].replace('/', '-')
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'cateid/' in url_part2:
                break
            elif 'http://' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://csp.org.cn/' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    # print(result)
    # breakpoint()
    try:
        news_content = selector_page.xpath('//*[@class="content"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="content"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)<', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：(.*?)</', content_text, re.M | re.S)[0]
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-会议展览-学术会议': 'https://www.csp.org.cn/c103?page=1',
        '首页-会议展览-展览信息': 'https://www.csp.org.cn/c104?page=1',
        '首页-奖励人才-奖项资讯': 'https://www.csp.org.cn/c137?page=1',
        '首页-学会刊物-最新期刊': 'https://www.csp.org.cn/c118?page=1',
        '首页-学会动态-学会新闻': 'https://www.csp.org.cn/c120?page=1',
        '首页-学会动态-国内学术活动': 'https://www.csp.org.cn/c121?page=1',
        '首页-学会动态-国际学术活动': 'https://www.csp.org.cn/c122?page=1',
        '首页-学会动态-产学研项目': 'https://www.csp.org.cn/c123?page=1',
        '首页-学会动态-科研进展': 'https://www.csp.org.cn/c124?page=1',
        '首页-党建强会-重要精神': 'https://www.csp.org.cn/c174?page=1',
        '首页-党建强会-百年党史学习教育': 'https://www.csp.org.cn/c175?page=1',
        '首页-党建强会-学会党建动态': 'https://www.csp.org.cn/c177?page=1',
        '首页-党建强会-弘扬科学家精神': 'https://www.csp.org.cn/c181?page=1',
        '首页-行业动态': 'https://www.csp.org.cn/c100?page=1',
        '首页-通知公告': 'https://www.csp.org.cn/c136?page=1',
        '首页-科普园地-科普活动': 'https://www.csp.org.cn/c128?page=1',
        '首页-科普园地-颗粒学基本知识': 'https://www.csp.org.cn/c130?page=1',
        '首页-科普园地-小颗粒大健康': 'https://www.csp.org.cn/c194?page=1',
        '首页-科普园地-小颗粒大工业': 'https://www.csp.org.cn/c134?page=1',
        '首页-科普园地-小颗粒大环境': 'https://www.csp.org.cn/c135?page=1',
        '首页-科普园地-继续教育': 'https://www.csp.org.cn/c131?page=1',
        '首页-Home-About': 'https://www.csp.org.cn/c184?page=1',
        '首页-Home-News & Events': 'https://www.csp.org.cn/c186?page=1',
        '首页-Home-Publications': 'https://www.csp.org.cn/c187?page=1',
        '首页-Home-Conferences': 'https://www.csp.org.cn/c189?page=1',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            url = value.replace('page=1', 'page=' + str(page))
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)

            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'B-57 中国颗粒学会'
    start_run(project_time, start_time, account_name)
