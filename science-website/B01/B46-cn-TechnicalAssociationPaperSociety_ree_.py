# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-01-09
import random
import re
import io
import sys
import time
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, columnid):

    headers = {
        'Accept': 'application/xml, text/xml, */*; q=0.01',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'http://www.ctapi.org.cn',
        'Referer': url,
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Mobile Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    params = (
        ('startrecord', '1'),
        ('endrecord', '120'),
        ('perpage', '40'),
    )

    data = {
        'col': '1',
        'appid': '1',
        'webid': '26',
        'path': '/',
        'columnid': columnid,
        'sourceContentType': '1',
        'unitid': '12541',
        'webname': '\u4E2D\u56FD\u9020\u7EB8\u5B66\u4F1A-\u9020\u7EB8\u79D1\u6280\u7F51',
        'permissiontype': '0'
    }
    response = requests.post('http://www.ctapi.org.cn/module/web/jpage/dataproxy.jsp', headers=headers, params=params,
                             data=data, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    content = re.findall(r'<record><!\[CDATA\[(.*?)\]\>', html_text, re.M | re.S)
    # print(content[0])
    # breakpoint()
    try:
        ree_data(content, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(content)):
        try:
            news_title = re.findall(r'target="_blank">(.*?)</a>', content[i])[0]
        except:
            news_title = ''

        news_abstract = news_title

        try:
            news_publish_time = re.findall(r'/art/(.*?)/art_', content[i])[0].replace('/', '-')
            # print(news_publish_time)
            # breakpoint()
        except:
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = re.findall(r'<a href="(.*?)"', content[i])[0]
            if 'http://' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.ctapi.org.cn' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        # 'If-Modified-Since': 'Sat, 31 Dec 2022 11:28:56 GMT',
        # 'If-None-Match': '"2cc3-5f11e0612767f"',
        'Referer': news_page_url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Mobile Safari/537.36',
    }
    html_response = requests.get(news_page_url, headers=headers, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="contact"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="contact"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源: (.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：(.*?)<', content_text, re.M | re.S)[0].strip()
        # print(read_count)
        # breakpoint()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻动态-学会要闻': 'http://www.ctapi.org.cn/col/col1792/index.html',
        '首页-新闻动态-学会通知': 'http://www.ctapi.org.cn/col/col1794/index.html',
        '首页-新闻动态-学会动态': 'http://www.ctapi.org.cn/col/col1817/index.html',
        '首页-新闻动态-地方学会': 'http://www.ctapi.org.cn/col/col1795/index.html',
        '首页-新闻动态-政策法规': 'http://www.ctapi.org.cn/col/col1796/index.html',
        '首页-新闻动态-行业资讯': 'http://www.ctapi.org.cn/col/col1804/index.html',
        '首页-新闻动态-精彩图集': 'http://www.ctapi.org.cn/col/col1807/index.html',
        '首页-党建专题-党建动态': 'http://www.ctapi.org.cn/col/col1818/index.html',
        '首页-党建专题-理论学习': 'http://www.ctapi.org.cn/col/col1819/index.html',
        '首页-党建专题-党史学习': 'http://www.ctapi.org.cn/col/col1820/index.html',
        '首页-党建专题-科学家精神': 'http://www.ctapi.org.cn/col/col1821/index.html',
        '首页-智库建设-智库动态': 'http://www.ctapi.org.cn/col/col1824/index.html',
        '首页-智库建设-智库观点': 'http://www.ctapi.org.cn/col/col1825/index.html',
        '首页-智库建设-智库专家': 'http://www.ctapi.org.cn/col/col1827/index.html',
        '首页-学术交流-学术动态': 'http://www.ctapi.org.cn/col/col1832/index.html',
        '首页-学术交流-国内交流': 'http://www.ctapi.org.cn/col/col1833/index.html',
        '首页-学术交流-国际交流': 'http://www.ctapi.org.cn/col/col1835/index.html',
        '首页-学术交流-学术年会': 'http://www.ctapi.org.cn/col/col1834/index.html',
        '首页-学术交流-活动安排': 'http://www.ctapi.org.cn/col/col1836/index.html',
        '首页-学术交流-中国造纸学会年鉴': 'http://www.ctapi.org.cn/col/col1837/index.html',
        '首页-科普之家-科普知识': 'http://www.ctapi.org.cn/col/col1841/index.html',
        '首页-科普之家-科普活动': 'http://www.ctapi.org.cn/col/col1842/index.html',
        '首页-科普之家-出版物': 'http://www.ctapi.org.cn/col/col1844/index.html',
        '首页-会员之家-会员动态': 'http://www.ctapi.org.cn/col/col4218/index.html',
        '首页-会员之家-会员风采': 'http://www.ctapi.org.cn/col/col4220/index.html',
    }
    for key, value in urls.items():
        news_classify = key
        url = value
        columnid = re.findall(r'col\/col(.*?)\/index', value)[0]
        html_text = get_html(url, columnid)
        # print(html_text)
        # breakpoint()
        get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time)
        time.sleep(1)
        time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'B-46 中国造纸学会'
    start_run(project_time, start_time, account_name)