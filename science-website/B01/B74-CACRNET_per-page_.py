# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-01-28

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url,  verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="clearFloat"]/ul/li')
        xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time_md = part_nodes[i].xpath('./a/span/text()')[0]
            news_publish_time = re.sub(r'[\[, \]]', '', news_publish_time_md)
            # print(news_publish_time)
            # breakpoint()
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'https:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.cacrnet.org.cn' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url,verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="recpagep"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="recpagep"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)<', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览数： <span id="clicks">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-活动会议-要闻动态': 'https://www.cacrnet.org.cn/site/term/43.html?page=1&per-page=15',
        '首页-活动会议-活动预报': 'https://www.cacrnet.org.cn/site/term/44.html?page=1&per-page=15',
        '首页-活动会议-通知公告': 'https://www.cacrnet.org.cn/site/term/45.html?page=1&per-page=15',
        '首页-活动会议-年会报道': 'https://www.cacrnet.org.cn/site/term/120.html?page=1&per-page=15',
        '首页-党建工作-重要精神': 'https://www.cacrnet.org.cn/site/term/67.html?page=1&per-page=15',
        '首页-党建工作-党史': 'https://www.cacrnet.org.cn/site/term/67.html?page=1&per-page=15',
        '首页-党建工作-党建动态': 'https://www.cacrnet.org.cn/site/term/40.html?page=1&per-page=15',
        '首页-学会奖励-密码创新奖': 'https://www.cacrnet.org.cn/site/term/50.html?page=1&per-page=15',
        '首页-学会奖励-优秀博士学位论文': 'https://www.cacrnet.org.cn/site/term/51.html?page=1&per-page=15',
        '首页-学会奖励-中国密码学会奖': 'https://www.cacrnet.org.cn/site/term/52.html?page=1&per-page=15',
        '首页-会员中心-会员风采': 'https://www.cacrnet.org.cn/site/term/54.html?page=1&per-page=15',
        '首页-密码科普-科普活动': 'https://www.cacrnet.org.cn/site/term/61.html?page=1&per-page=15',
        '首页-密码科普-密码大讲堂': 'https://www.cacrnet.org.cn/site/term/118.html?page=1&per-page=15',
        '首页-密码科普-资料园地': 'https://www.cacrnet.org.cn/site/term/62.html?page=1&per-page=15',
        '首页-人才托举-项目动态': 'https://www.cacrnet.org.cn/site/term/102.html?page=1&per-page=15',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            url = value.replace('page=1', 'page=' + str(page))
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)

            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'B-74 中国密码学会'
    start_run(args.projectname, args.sinceyear, account_name)
