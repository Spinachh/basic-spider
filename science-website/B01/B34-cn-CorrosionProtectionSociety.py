# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-12-24
import random
import re
import io
import sys
import time
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url,  verify=False)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="newsOne_list"]/li')
        xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0].replace('/', '-')
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'https:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://cscp.org.cn' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url,verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="NewsText"]//p/span/text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="NewsText"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)<', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-学会动态-学会新闻': 'http://cscp.org.cn/site/term/14.html?page=1&per-page=10',
        '首页-学会动态-通知公告': 'http://cscp.org.cn/site/term/15.html?page=1&per-page=10',
        '首页-学会动态-结果公示': 'http://cscp.org.cn/site/term/16.html?page=1&per-page=10',
        '首页-学术交流-活动计划': 'http://cscp.org.cn/site/term/18.html?page=1&per-page=10',
        '首页-学术交流-国际会议': 'http://cscp.org.cn/site/term/19.html?page=1&per-page=10',
        '首页-学术交流-国内会议': 'http://cscp.org.cn/site/term/20.html?page=1&per-page=10',
        '首页-科普活动-科普传播': 'http://cscp.org.cn/site/term/23.html?page=1&per-page=10',
        '首页-行业服务-科技奖励': 'http://cscp.org.cn/site/term/26.html?page=1&per-page=10',
        '首页-行业服务-科技成果评价': 'http://cscp.org.cn/site/term/27.html?page=1&per-page=10',
        '首页-行业服务-专业技术资格认证': 'http://cscp.org.cn/site/term/28.html?page=1&per-page=10',
        '首页-行业服务-施工资质': 'http://cscp.org.cn/site/term/29.html?page=1&per-page=10',
        '首页-教育培训-防腐工程师培训': 'http://cscp.org.cn/site/term/33.html?page=1&per-page=10',
        '首页-教育培训-国际培训': 'http://cscp.org.cn/site/term/34.html?page=1&per-page=10',
        '首页-教育培训-专业技术培训': 'http://cscp.org.cn/site/term/35.html?page=1&per-page=10',
        '首页-教育培训-防腐公开课': 'http://cscp.org.cn/site/term/81.html?page=1&per-page=10',
        '首页-成果推介-科技成果评价': 'http://cscp.org.cn/site/term/37.html?page=1&per-page=10',
        '首页-成果推介-科学技术奖': 'http://cscp.org.cn/site/term/38.html?page=1&per-page=10',
        '首页-证书查询-专业资格证书': 'http://cscp.org.cn/site/term/41.html?page=1&per-page=10',
        '首页-证书查询-施工单位资质证书': 'http://cscp.org.cn/site/term/42.html?page=1&per-page=10',
        '首页-证书查询-科技奖励证书': 'http://cscp.org.cn/site/term/43.html?page=1&per-page=10',
        '首页-团体标准-团体标准': 'http://cscp.org.cn/site/term/45.html?page=1&per-page=10',
        '首页-团体标准-相关文件': 'http://cscp.org.cn/site/term/46.html?page=1&per-page=10',
        '首页-团体标准-公示公告': 'http://cscp.org.cn/site/term/47.html?page=1&per-page=10',
        '首页-团体标准-标准动态': 'http://cscp.org.cn/site/term/48.html?page=1&per-page=10',
        '首页-党建工作-通知公告': 'http://cscp.org.cn/site/term/50.html?page=1&per-page=10',
        '首页-党建工作-党建动态': 'http://cscp.org.cn/site/term/51.html?page=1&per-page=10',
        '首页-党建工作-学习专栏': 'http://cscp.org.cn/site/term/52.html?page=1&per-page=10',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            url = value.replace('page=1', 'page=' + str(page))
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)

            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'B-34 中国腐蚀与防护学会'
    start_run(project_time, start_time, account_name)