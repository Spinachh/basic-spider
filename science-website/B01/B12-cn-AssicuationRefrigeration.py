# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-12-04
import random
import re
import io
import sys
import time
import json
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, page, typeid):

    cookies = {
        'PHPSESSID': 'tebn502fejcgc9ng68s3t7gkp2',
        'BkGOp9578O_think_template': 'default',
        'Hm_lvt_4b063acd22c181eda3e54979cb62c171': '1670142000',
        'Hm_lvt_379bd18ad9a3b4a71a6cbd12fa124a6a': '1670142000',
        'Hm_lpvt_4b063acd22c181eda3e54979cb62c171': '1670142017',
        'Hm_lpvt_379bd18ad9a3b4a71a6cbd12fa124a6a': '1670142017',
    }

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'https://www.car.org.cn',
        'Referer': 'https://www.car.org.cn/index.php?s=/lists_20.html',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
        'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    params = (
        ('s', '/List_getnews.html'),
    )

    data = {
        'page': page,
        'typeid': typeid,
    }

    response = requests.post(url, headers=headers, params=params, cookies=cookies,
                             data=data)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    content = re.findall(r'<li>(.*?)</li>', html_text, re.M | re.S)
    try:
        ree_data(content, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(content)):
        try:
            news_title = re.findall(r'"new">(.*?)</a>', content[i])[0]
        except:
            news_title = '123'

        news_abstract = news_title

        try:
            news_publish_time = re.findall(r'class="new_c1 left">(.*?)<', content[i])[0]
        except:
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = re.findall(r'<a href="(.*?)" target="new">', content[i])[0]
            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.car.org.cn' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    cookies = {
        'PHPSESSID': 'tebn502fejcgc9ng68s3t7gkp2',
        'BkGOp9578O_think_template': 'default',
        'Hm_lvt_4b063acd22c181eda3e54979cb62c171': '1670142000',
        'Hm_lvt_379bd18ad9a3b4a71a6cbd12fa124a6a': '1670142000',
        'Hm_lpvt_4b063acd22c181eda3e54979cb62c171': '1670144284',
        'Hm_lpvt_379bd18ad9a3b4a71a6cbd12fa124a6a': '1670144284',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Referer': 'https://www.car.org.cn/index.php?s=/lists_20.html',
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-User': '?1',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
        'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    params = (
        ('s', '/articles_1565.html'),
    )

    html_response = requests.get(news_page_url, headers=headers, params=params, cookies=cookies)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = result.get('content')
    except:
        news_content = selector_page.xpath('//*[@class="dynamicsCon"]//p/text()')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="learn_d"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'新闻来源：<span>(.*?)</span>', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>阅读：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)
    url = 'https://www.car.org.cn/index.php?s=/List_getnews.html'

    urls = {
        '首页-新闻动态-学会动态': '20',
        '首页-新闻动态-地方学会动态': '21',
        '首页-新闻动态-行业动态': '22',
        '首页-新闻动态-会议计划': '23',

    }
    for key, value in urls.items():
        typeid = value
        news_classify = key
        for page in range(1, 10):
            html_text = get_html(url, page, typeid)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time)
            time.sleep(1)
            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'B-12 中国制冷学会'
    start_run(project_time, start_time, account_name)