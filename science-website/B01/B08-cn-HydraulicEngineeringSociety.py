# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-12-01
import random
import re
import io
import sys
import time
import json
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    cookies = {
        'JSESSIONID': '6C6DB1C6235B994A7D074EC903D167E6',
        '__FT10000040': '2022-12-1-16-9-11',
        '__NRU10000040': '1669882151511',
        '__RT10000040': '2022-12-1-16-9-11',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    selector = etree.HTML(html_text)
    if '首页' in news_classify:
        try:
            part1_nodes = selector.xpath('//*[@class="col-lg-12 col-md-12 '
                                         'col-sm-12 col-xs-12"]')[1:]
            xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
        except Exception as e:
            logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))
    else:
        try:
            part2_nodes = selector.xpath('//*[@class="news_box"]/ul/li')
            xpath2_data(part2_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
        except Exception as e:
            logging.warning('Classify：{} Get_Data Part2 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('.//h5/a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('.//h5/a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('.//h6/text()')[0].strip()
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('.//h5/a/@href')[0]
            if 'mp.weixin.qq.com' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.ches.org.cn/ches/xhxw' + \
                                url_part2.replace('./', '/')

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def xpath2_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/@title')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/@title')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./div/text()')[0]\
                .replace('[', '').replace(']', '')
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'mp.weixin.qq.com' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.ches.org.cn/ches/' + \
                                url_part2.replace('../', '')

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)

    # if 'www.cstp.org.cn' not in news_page_url:
    #     html_response.encoding = 'gb2312'
    # else:
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    try:
        news_content = result.get('content')
        # news_content = selector_page.xpath('//*[@class="contain"]//p/span/text()')
    except:
        news_content = selector_page.xpath('//*[@class="cont_txt"]/span/text()')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="TRS_Editor"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'新闻来源：<span>(.*?)</span>', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学会新闻': 'http://www.ches.org.cn/ches/xhxw/index_1.html',
        '首页-公告栏': 'http://www.ches.org.cn/ches/ggl/index_1.html',
        '首页-行业新闻': 'http://www.ches.org.cn/ches/hyxw_22318/index_1.html',
        '首页-专委会动态': 'http://www.ches.org.cn/ches/zwhdt/index_1.html',
        '首页-地方动态': 'http://www.ches.org.cn/ches/dfdt_22320/index_1.html',
        '首页-会员动态': 'http://www.ches.org.cn/ches/dwhydt/index_1.html',
        '首页-成果评价-通知公告': 'http://www.ches.org.cn/ches/2021cgpj/cgpj_tzgg/index_1.html',
        '首页-人才举荐-院士举荐': 'http://www.ches.org.cn/ches/rcpyyjj/ysjj/ysjjtzgg/index_1.html',
        '首页-人才举荐-人才奖励': 'http://www.ches.org.cn/ches/rcpyyjj/rcjl/rcjlrcjl/index_1.html',
        '首页-专业认证-认证公告': 'http://www.ches.org.cn/ches/rzgg/index_1.html',
        '首页-专业认证-认证标准': 'http://www.ches.org.cn/ches/rzbz/index_1.html',
        '首页-专业认证-认证新闻': 'http://www.ches.org.cn/ches/rzxx/index_1.html',
        '首页-专业认证-认证回答': 'http://www.ches.org.cn/ches/rzhd/index_1.html',
        '首页-标准化-通知公告': 'http://www.ches.org.cn/ches/bzh/hybzgl/hytzgg/index_1.html',
        '首页-标准化-标准动态': 'http://www.ches.org.cn/ches/bzh/hybzgl/hybzdt/index_1.html',
        '首页-标准化-征求意见': 'http://www.ches.org.cn/ches/bzh/hybzgl/hyzqyj/index_1.html',
        '首页-标准化-标准公告': 'http://www.ches.org.cn/ches/bzh/hybzgl/index_1.html',
        '首页-党建-党建要闻': 'http://www.ches.org.cn/ches/dj/gzdt/index_1.html',
        '首页-党建-工作动态': 'http://www.ches.org.cn/ches/dj/tzgg/index_1.html',
        '首页-党建-理论动态': 'http://www.ches.org.cn/ches/dj/llxx/index_1.html',
        '首页-党建-青年小组': 'http://www.ches.org.cn/ches/dj/jyjl/index_1.html',
        '首页-党建-不忘初心': 'http://www.ches.org.cn/ches/dj/bwcx/index_1.html',
        '首页-党建-党史学习': 'http://www.ches.org.cn/ches/dj/dsxx/index_1.html',
        '科普动态': 'http://www.ches.org.cn/ches/slkp/kpdt/index_1.html',
        '水利科普-通知公告': 'http://www.ches.org.cn/ches/slkp/slkptzgg/index_1.html',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 10):
            if page == 1:
                url = value.replace('index_1', 'index')
            else:
                url = value.replace('index_1', 'index_' + str(page - 1))

            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time)
            time.sleep(1)
            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'B-08 中国水利学会'
    start_run(project_time, start_time, account_name)