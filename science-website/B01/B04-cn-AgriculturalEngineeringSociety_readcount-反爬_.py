# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-11-29
import random
import re
import io
import sys
import time
import json
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.csae.org.cn/rdxw/index_1.html',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="cbw backn"]/ul/li')
        xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0]
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'mp.weixin.qq.com' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.csae.org.cn/rdxw' + \
                                url_part2.replace('./', '/')

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    # html_response = requests.get(news_page_url, verify=False)
    html_response = requests.get(news_page_url)
    # if 'www.cstp.org.cn' not in news_page_url:
    #     html_response.encoding = 'gb2312'
    # else:
    html_response.encoding = 'utf-8'

    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    try:
        news_content = selector_page.xpath('//*[@class="container"]///p/text()')
        # news_content = selector_page.xpath('//*[@class="cont_txt"]/span/text()')
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="TRS_Editor"]/p//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'>文章来源：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''


    try:
        # read_count = re.findall(r'>浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()

        cookies = {
            '202.127.42.112': '202.127.42.112',
        }

        headers = {
            'Accept': '*/*',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Connection': 'keep-alive',
            'Referer': 'http://www.csae.org.cn/',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36',
        }

        params = (
            ('id', '_browse_count'),
        )

        response = requests.get('http://user.csae.org.cn/counter/counter.js', headers=headers, params=params,
                                cookies=cookies, verify=False)
        # read_count2 = response.text.split('=')[-1]
        # print(read_count2)
        read_count = re.findall(r"innerHTML='(.*?)'", response.text)[0]
    except:
        read_count = ''

    
    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-重点新闻': 'http://www.csae.org.cn/rdxw/index_1.html',
        '首页-通知公告': 'http://www.csae.org.cn/ggl/index_1.html',
        '首页-党的建设': 'http://www.csae.org.cn/ddjs/index_1.html',
        '首页-工程教育认证': 'http://www.csae.org.cn/gcjyrz/index_1.html',
        '首页-学会动态-中国农业工程学会': 'http://www.csae.org.cn/xhdt/zgnygcxh/index_1.html',
        '首页-学会动态-分支机构': 'http://www.csae.org.cn/xhdt/zywyh/index_1.html',
        '首页-学会动态-地方学会': 'http://www.csae.org.cn/xhdt/dfxh/index_1.html',
        '首页-组织建设-会员代表大会': 'http://www.csae.org.cn/zzjs/qghydbdh/index_1.html',
        '首页-组织建设-常务理事会议': 'http://www.csae.org.cn/zzjs/dwlshlshhy/index_1.html',
        '首页-学术交流-学术动态': 'http://www.csae.org.cn/xsjl/xsdt/index_1.html',
        '首页-学术交流-国际交流': 'http://www.csae.org.cn/xsjl/hytz/index_1.html',
        '首页-学术交流-学科发展': 'http://www.csae.org.cn/xsjl/xkfz/index_1.html',
        '首页-科学普及-科普动态': 'http://www.csae.org.cn/kxbj/kpdt/index_1.html',
        '首页-科学普及-科技信息': 'http://www.csae.org.cn/kxbj/hyxx/index_1.html',
        '首页-评价咨询-成果评价': 'http://www.csae.org.cn/pjzx/kjcgpj/index_1.html',
        '首页-评价咨询-典型案例': 'http://www.csae.org.cn/pjzx/kjcgpj/dxal/index_1.html',
        '首页-评价咨询-教育培训': 'http://www.csae.org.cn/pjzx/jypx/index_1.html',
        '首页-评价咨询-智库询问': 'http://www.csae.org.cn/pjzx/zzzx/index_1.html',
        '首页-评价咨询-农业工程标准': 'http://www.csae.org.cn/pjzx/nygcbz/index_1.html',
        '首页-表彰奖励-学会科技奖': 'http://www.csae.org.cn/bzjl/zgnygckjj/index_1.html',
        '首页-表彰奖励-人才举荐': 'http://www.csae.org.cn/bzjl/rcpy/index_1.html',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            if page == 1:
                url = value.replace('index_1', 'index')
            else:
                url = value.replace('index_1', 'index_' + str(page - 1))
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time)
            time.sleep(1)
            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'B-04 中国农业工程学会'
    start_run(project_time, start_time, account_name)