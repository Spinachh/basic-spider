# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-01-23

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@id="ctl00_main_panel3"]//table[2]//tr')
        xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./td[1]/a/@title')[0].strip()
        except:
            news_title = ''
        try:
            news_abstract = part_nodes[i].xpath('./td[1]/a/@title')[0].strip()
        except:
            news_abstract = ''
        try:
            news_publish_time = part_nodes[i].xpath('./td[2]/text()')[0]
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./td[1]/a/@href')[0]
            if 'https:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.caoe.org.cn/nr/' + url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="nrTxt02"]//span//text()')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="nrTxt02"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'<span class="news_top_lyname">(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'<span class="news_top_zzname">(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击率: (.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻通告-通知公告': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=209',
        '首页-新闻通告-协会新闻': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=210',
        '首页-新闻通告-分会动态': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=397',
        '首页-新闻通告-行业动态': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=211',
        '首页-表彰奖励-科协技术奖-通知公告': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=225',
        '首页-表彰奖励-科协技术奖-奖励动态': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=226',
        '首页-表彰奖励-科协技术奖-政策法规': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=227',
        '首页-工程动态': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=6',
        '首页-专题研究': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=7',
        '首页-政策法规-社团相关规章制度': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=232',
        '首页-政策法规-协会规章制度': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=232',
        '首页-政策法规-海洋相关政策法规': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=231',
        '首页-团体标准-组织机构': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=359',
        '首页-团体标准-工作动态': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=362',
        '首页-团体标准-政策法规': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=363',
        '首页-团体标准-交流培训': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=369',
        '首页-期刊读物-交流培训': 'http://www.caoe.org.cn/nr/list.aspx?pageid=1&itemid=369',

    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            url = value.replace('pageid=1', 'pageid=' + str(page))
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)

            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2022-10-01')
    args = parser.parse_args()
    account_name = 'B-69T 中国海洋工程咨询协会'
    start_run(args.projectname, args.sinceyear, account_name)