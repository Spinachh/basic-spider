# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-11-30
import random
import re
import io
import sys
import time
import json
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):
    response = requests.get(url, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    selector = etree.HTML(html_text)
    if news_classify == '首页-新闻-学会要闻':
        try:
            part1_nodes = selector.xpath('//*[@class="fl c-s-right"]/dl')
            xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
        except Exception as e:
            logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))
    else:
        try:
            part2_nodes = selector.xpath('//*[@class="fl c-s-right dynamic"]/ul/li')
            xpath2_data(part2_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
        except Exception as e:
            logging.warning('Classify：{} Get_Data Part2 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            if '首页-新闻-学会要闻' in news_classify:
                news_title = part_nodes[i].xpath('./dd/h4/a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./a/text()')[0].strip()

        except:
            news_title = ''

        try:
            if '首页-新闻-学会要闻' in news_classify:
                news_abstract = part_nodes[i].xpath('./dd/p[2]/text()')[0].strip()
            else:
                news_abstract = news_title
        except:
            news_abstract = ''

        try:
            if '首页-新闻-学会要闻' in news_classify:
                news_publish_time = part_nodes[i].xpath('./dd/p[1]/text()')[0]\
                    .split('发布时间：')[-1]
            else:
                news_publish_time = part_nodes[i].xpath('./p/text()')
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):
            if '首页-新闻-学会要闻' in news_classify:
                url_part2 = part_nodes[i].xpath('./dd/h4/a/@href')[0]
            else:
                url_part2 = part_nodes[i].xpath('./a/@href')[0]

            if 'mp.weixin.qq.com' in url_part2:
                news_page_url = url_part2
            elif 'www' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.csee.org.cn/' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def xpath2_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[-1]
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[-1]
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./p/text()')[0]
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'mp.weixin.qq.com' in url_part2:
                news_page_url = url_part2
            elif 'www' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.csee.org.cn/' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    # html_response = requests.get(news_page_url, verify=False)
    html_response = requests.get(news_page_url)
    # if 'www.cstp.org.cn' not in news_page_url:
    #     html_response.encoding = 'gb2312'
    # else:
    html_response.encoding = 'utf-8'

    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    try:
        news_content = selector_page.xpath('//*[@class="text-detail"]//p/text()')
        # news_content = selector_page.xpath('//*[@class="cont_txt"]/span/text()')
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="text-detail"]/p//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：<span>(.*?)</span>', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻-学会要闻': 'http://www.csee.org.cn/portal/xfpxhyw/index_1.html',
        '首页-新闻-通知公告': 'http://www.csee.org.cn/portal/xfptzgg/index_1.html',
        '首页-新闻-行业新闻': 'http://www.csee.org.cn/portal/xfphexw/index_1.html',
        '首页-新闻-专题专栏': 'http://www.csee.org.cn/portal/xfpztzl/index_1.html',
        '首页-新闻-学会动态-学会综合': 'http://www.csee.org.cn/portal/xfdxhzh/index_1.html',
        '首页-新闻-学会动态-省学会': 'http://www.csee.org.cn/portal/xfdsxh/index_1.html',
        '首页-新闻-学会动态-专委会': 'http://www.csee.org.cn/portal/xfdzwh/index_1.html',
        '首页-学术-学术会议-会议新闻': 'http://www.csee.org.cn/portal/xpxshyxw/index_1.html',
        '首页-学术-CSEE学术报告-电力科技研究报告': 'http://www.csee.org.cn/portal/xpxszdkjbg/index_1.html',
        '首页-学术-CSEE学术报告-动力与电气工程学科发展报告': 'http://www.csee.org.cn/portal/xpxsdldqbg/index_1.html',
        '首页-学术-CSEE学术报告-CSEE专题技术报告': 'http://www.csee.org.cn/portal/xpxsztjsbg/index_1.html',
        '首页-科普-科普机构-学会科普介绍': 'http://www.csee.org.cn/portal/xpkpkpb/index_1.html',
        '首页-科普-科普机构-电力行业资源链接': 'http://www.csee.org.cn/portal/xpkpzylj/index_1.html',
        '首页-科普-工作动态-总会科普活动': 'http://www.csee.org.cn/portal/xpkpzhhd/index_1.html',
        '首页-科普-工作动态-地方科普活动': 'http://www.csee.org.cn/portal/xpkpdfhd/index_1.html',
        '首页-科普-科普基地-2022': 'http://www.csee.org.cn/portal/xpkpjdjs2022/index_1.html',
        '首页-科普-传播专家-中国科学首席': 'http://www.csee.org.cn/portal/xpkxsxzj/index_1.html',
        '首页-科普-传播专家-电力之光传播专家': 'http://www.csee.org.cn/portal/xpdgcbzj/index_1.html',
        '首页-科普-品牌专题-电力科普下乡': 'http://www.csee.org.cn/portal/xpkpdlxx/index_1.html',
        '首页-科普-品牌专题-全国科普日': 'http://www.csee.org.cn/portal/xpkpr/index_1.html',
        '首页-科普-品牌专题-学院心得': 'http://www.csee.org.cn/portal/xpkpxyxd/index_1.html',
        '首页-科普-文件下载-中国科协文件': 'http://www.csee.org.cn/portal/xpkpkxwj/index_1.html',
        '首页-科普-文件下载-学会文件': 'http://www.csee.org.cn/portal/xpkpxhwj/index_1.html',
        '首页-国际-国际交流动态': 'http://www.csee.org.cn/portal/xpgjjldt/index_1.html',
        '首页-国际-国际会议': 'http://www.csee.org.cn/portal/xpgjhy/index_1.html',
        '首页-国际-海外分支-英国分会': 'http://www.csee.org.cn/portal/xpgjygfh/index_1.html',
        '首页-国际-海外分支-欧洲会员': 'http://www.csee.org.cn/portal/xpgjozct/index_1.html',
        '首页-国际-海外分支-北美会员': 'http://www.csee.org.cn/portal/xpgjbmct/index_1.html',
        '首页-国际-国际工程师认证-动态': 'http://www.csee.org.cn/portal/xpgjrzdt/index_1.html',
        '首页-国际-国际工程师认证-白名单': 'http://www.csee.org.cn/portal/xpgjtgrymd/index_1.html',
        '首页-国际-国际标准': 'http://www.csee.org.cn/portal/xpggjbz/index_1.html',
        '首页-国际-合作国际组织-CIGRE': 'http://www.csee.org.cn/portal/xpgjcig/index_1.html',
        '首页-国际-合作国际组织-CIRED': 'http://www.csee.org.cn/portal/xpgjcir/index_1.html',
        '首页-咨询-科技评价-工作动态': 'http://www.csee.org.cn/portal/xppjgzdt/index_1.html',
        '首页-咨询-科技查新-工作动态': 'http://www.csee.org.cn/portal/xpcxgzdt/index_1.html',
        '首页-咨询-团体标准-通知公告': 'http://www.csee.org.cn/portal/xpzxtg/index_1.html',
        '首页-咨询-团体标准-工作动态': 'http://www.csee.org.cn/portal/xpzxdt/index_1.html',
        '首页-咨询-团体标准-CSEE': 'http://www.csee.org.cn/portal/xpzxyjzq/index_1.html',
        '首页-咨询-团体标准-标准发布': 'http://www.csee.org.cn/portal/xpzxbf/index_1.html',
        '首页-期刊出版-期刊精选-中国电机工程学报': 'http://www.csee.org.cn/portal/xpqkxb/index_1.html',
        '首页-期刊出版-期刊精选-农村电气化': 'http://www.csee.org.cn/portal/xpqkndq/index_1.html',
        '首页-期刊出版-期刊精选-PowerEnergy-System': 'http://www.csee.org.cn/portal/xpqkpes/index_1.html',
        '首页-期刊出版-期刊精选-高压电技术': 'http://www.csee.org.cn/portal/xpqkgyj/index_1.html',
        '首页-期刊出版-期刊精选-农电管理': 'http://www.csee.org.cn/portal/xpqkndgl/index_1.html',
        '首页-期刊出版-期刊精选-热力发电': 'http://www.csee.org.cn/portal/xpqkrlfd/index_1.html',
        '首页-期刊出版-期刊精选-中国电力': 'http://www.csee.org.cn/portal/xpqkzgdl/index_1.html',
        '首页-期刊出版-期刊精选-电网技术': 'http://www.csee.org.cn/portal/xpqkdwjs/index_1.html',
        '首页-期刊出版-期刊精选-期刊投稿': 'http://www.csee.org.cn/portal/xpqksy/index_1.html',
        '首页-认证评价-工程教育认证-工作动态': 'http://www.csee.org.cn/portal/xprzjydt/index_1.html',
        '首页-认证评价-工程能力评价-工作动态': 'http://www.csee.org.cn/portal/xprzhgzdt/index_1.html',
        '首页-电力奖励-电力科学技术奖-最新动态': 'http://www.csee.org.cn/portal/xpkjdt/index_1.html',
        '首页-电力奖励-电力科学技术奖-推荐通知': 'http://www.csee.org.cn/portal/xpkjtz/index_1.html',
        '首页-电力奖励-电力科学技术奖-在线公示': 'http://www.csee.org.cn/portal/xpkjzxgs/index_1.html',
        '首页-电力奖励-电力科学技术奖-奖励通报': 'http://www.csee.org.cn/portal/xpkjtb/index_1.html',
        '首页-会员-公告': 'http://www.csee.org.cn/portal/xphygg/index_1.html',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 15):
            if page == 1:
                url = value.replace('index_1', 'index')
            else:
                url = value.replace('index_1', 'index_' + str(page))
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time)
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default', default='2023-01-01')
    args = parser.parse_args()
    account_name = 'B-05 中国电机工程学会'
    start_run(args.projectname, args.sinceyear, account_name)
