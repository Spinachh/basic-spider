# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-01-03
import random
import re
import io
import sys
import time
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url,  verify=False)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    if '图片新闻' in news_classify:
        part1_nodes = selector.xpath('//*[@class="two_list"]/li')
        xpath2_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
    else:
        try:
            part1_nodes = selector.xpath('//*[@class="newslist"]/li')
            xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
        except Exception as e:
            logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0]
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'https:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.ns.org.cn' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def xpath2_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/div[2]/p[1]/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/div[2]/p[3]/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./a/div[2]/p[2]/text()')[0]
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'https:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.ns.org.cn' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''



def get_page_content(news_page_url):

    html_response = requests.get(news_page_url,verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="NewsText"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="NewsText"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'发布：(.*?)<', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-学会新闻-图片新闻': 'http://www.ns.org.cn/site/term/240.html?page=1&per-page=5',
        '首页-学会新闻-学会要闻': 'http://m.ns.org.cn/site/term/243.html?page=1',
        '首页-学会新闻-行业要闻': 'http://m.ns.org.cn/site/term/244.html?page=1',
        '首页-学会新闻-分会动态': 'http://m.ns.org.cn/site/term/245.html?page=1',
        '首页-学会新闻-通知公告': 'http://m.ns.org.cn/site/term/278.html?page=1',
        '首页-学术交流-学术年会': 'http://m.ns.org.cn/site/term/255.html?page=1',
        '首页-学术交流-国内学术会议-三核论坛': 'http://m.ns.org.cn/site/term/287.html?page=1',
        '首页-学术交流-国内学术会议-海峡两岸核能学术交流研讨会': 'http://m.ns.org.cn/site/term/288.html?page=1',
        '首页-学术交流-国际会议-国际核工程大会': 'http://m.ns.org.cn/site/term/289.html?page=1',
        '首页-学术交流-国际会议-太平洋地区核能大会': 'http://m.ns.org.cn/site/term/290.html?page=1',
        '首页-学术交流-国际会议-世界核妇女大会': 'http://m.ns.org.cn/site/term/291.html?page=1',
        '首页-学术交流-国际会议-国际核青年大会': 'http://m.ns.org.cn/site/term/292.html?page=1',
        '首页-学术交流-国际合作-国际组织': 'http://m.ns.org.cn/site/term/294.html?page=1',
        '首页-学术交流-国际合作-互访新闻': 'http://m.ns.org.cn/site/term/295.html?page=1',
        '首页-学术交流-工程能力评价-核工程专业教育认证': 'http://m.ns.org.cn/site/term/296.html?page=1',
        '首页-学术交流-工程能力评价-工程师资格国际互认': 'http://m.ns.org.cn/site/term/297.html?page=1',
        '首页-学术交流-战略研究': 'http://m.ns.org.cn/site/term/259.html?page=1',
        '首页-学术交流-团体标准': 'http://m.ns.org.cn/site/term/260.html?page=1',
        '首页-学术交流-青年人才托举工程': 'http://m.ns.org.cn/site/term/261.html?page=1',
        '首页-科学普及-科普中国-绿色核能主题科普活动': 'http://m.ns.org.cn/site/term/263.html?page=1',
        '首页-科学普及-科普中国-核科普奖': 'http://m.ns.org.cn/site/term/264.html?page=1',
        '首页-科学普及-科普中国-核科普教育基地': 'http://m.ns.org.cn/site/term/265.html?page=1',
        '首页-科学普及-科普中国-绿色核能特色学校': 'http://m.ns.org.cn/site/term/266.html?page=1',
        '首页-科学普及-科普中国-核科普讲师培训班': 'http://m.ns.org.cn/site/term/267.html?page=1',
        '首页-科学普及-科普中国-院士行': 'http://m.ns.org.cn/site/term/268.html?page=1',
        '首页-科学普及-科普中国-魅力之光': 'http://m.ns.org.cn/site/term/269.html?page=1',
        '首页-科学普及-科普资源-核科普百科': 'http://m.ns.org.cn/site/term/299.html?page=1',
        '首页-科学普及-科普资源-科普展览': 'http://m.ns.org.cn/site/term/300.html?page=1',
        '首页-科学普及-科普资源-科普专栏': 'http://m.ns.org.cn/site/term/314.html?page=1',
        '首页-党建强会-办事机构党支部': 'http://m.ns.org.cn/site/term/272.html?page=1',
        '首页-党建强会-党建要闻': 'http://m.ns.org.cn/site/term/274.html?page=1',
        '首页-学会刊物-主办期刊': 'http://m.ns.org.cn/site/term/275.html?page=1',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            url = value.replace('page=1', 'page=' + str(page))
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)

            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'B-36 中国核学会'
    start_run(project_time, start_time, account_name)