# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-01-14
import random
import re
import io
import sys
import time
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, pid, page):

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Origin': 'http://cspe.cpeweb.com.cn',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Mobile Safari/537.36',
    }

    params = (
        ('pid', pid),
    )

    data_content = requests.post(url, headers=headers).text

    viewstate = re.findall(r'value="(.*?)" />', data_content, re.M | re.S)[0]
    viewstategenerator = re.findall(r'id="__VIEWSTATEGENERATOR" value="(.*?)" />', data_content, re.M | re.S)[0]
    eventvalidation = re.findall(r'id="__EVENTVALIDATION" value="(.*?)" />', data_content, re.M | re.S)[0]

    data = {
        '__VIEWSTATE': viewstate,
        '__VIEWSTATEGENERATOR': viewstategenerator,
        '__EVENTTARGET': 'ctl00$ContentPlaceHolder1$AspNetPager1',
        '__EVENTARGUMENT': page,
        '__EVENTVALIDATION': eventvalidation,
        'ctl00$textfield3': '',
        'ctl00$ContentPlaceHolder1$AspNetPager1_input': (page -1)
    }
    response = requests.post(url, headers=headers, params=params,
                             data=data, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@width="96%"]/tr')
        xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./td[2]/a/text()')[0].strip()
        except:
            news_title = ''
        try:
            news_abstract = part_nodes[i].xpath('./td[2]/a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./td[3]/text()')[0]
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./td[2]/a/@href')[0]
            if 'cateid/' in url_part2:
                break
            elif 'http://' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://cspe.cpeweb.com.cn/' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    # print(result)
    # breakpoint()
    try:
        news_content = selector_page.xpath('//*[@class="zw14pxblack"]//text()')
        news_content = ''.join(x.strip() for x in news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="zw14pxblack"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)<', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'阅读次数： (.*?)</', content_text, re.M | re.S)[0]
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-关于学会-学会理事会': 'http://cspe.cpeweb.com.cn/about051.aspx',
        '首页-关于学会-学会工作委员会': 'http://cspe.cpeweb.com.cn/about061.aspx',
        '首页-信息中心-学会新闻': 'http://cspe.cpeweb.com.cn/news.aspx?pid=1',
        '首页-信息中心-学会公告': 'http://cspe.cpeweb.com.cn/news.aspx?pid=2',
        '首页-信息中心-媒体报道': 'http://cspe.cpeweb.com.cn/news.aspx?pid=3',
        '首页-信息中心-行业动态': 'http://cspe.cpeweb.com.cn/news.aspx?pid=4',
        '首页-信息中心-展会展览': 'http://cspe.cpeweb.com.cn/news.aspx?pid=5',
        '首页-信息中心-国际交流': 'http://cspe.cpeweb.com.cn/news.aspx?pid=6',
        '首页-学术交流-国内会议': 'http://cspe.cpeweb.com.cn/news.aspx?pid=7',
        '首页-学术交流-国际会议': 'http://cspe.cpeweb.com.cn/news.aspx?pid=8',
        '首页-学术交流-科学论文': 'http://cspe.cpeweb.com.cn/news.aspx?pid=9',
        '首页-学术交流-会议征文': 'http://cspe.cpeweb.com.cn/news.aspx?pid=10',
        '首页-科普知识-能源知识': 'http://cspe.cpeweb.com.cn/news.aspx?pid=11',
        '首页-科普知识-科普活动': 'http://cspe.cpeweb.com.cn/news.aspx?pid=12',
    }
    for key, value in urls.items():
        news_classify = key
        pid = re.findall(r'\?pid=(.*?)', value)[0]
        for page in range(1, 5):
            html_text = get_html(value, pid, page)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)

            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'B-59 中国动力工程学会'
    start_run(project_time, start_time, account_name)
