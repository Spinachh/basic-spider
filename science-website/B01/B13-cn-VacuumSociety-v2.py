# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-12-04
import random
import re
import io
import sys
import time
import json
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    cookies = {
        'JSESSIONID': '3771B808B1B380AD37B99D54746DFA4F',
        'sajssdk_2015_cross_ZQSensorsObjnew_user': '1',
        'sensorsdata2015jssdkcrossZQSensorsObj': '%7B%22distinct_id%22%3A%22184dcafd2ee97-0d7ec55af3b58d8-26021151-2073600-184dcafd2efa9f%22%2C%22first_id%22%3A%22%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E7%9B%B4%E6%8E%A5%E6%B5%81%E9%87%8F%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC_%E7%9B%B4%E6%8E%A5%E6%89%93%E5%BC%80%22%2C%22%24latest_referrer%22%3A%22%22%2C%22%24latest_landing_page%22%3A%22http%3A%2F%2Fwww.chinesevacuum.com%2F%22%7D%2C%22%24device_id%22%3A%22184dcafd2ee97-0d7ec55af3b58d8-26021151-2073600-184dcafd2efa9f%22%7D',
    }

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Content-Length': '0',
        'Origin': 'http://www.chinesevacuum.com',
        'Referer': 'http://www.chinesevacuum.com/news/30/',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    response = requests.post(url, headers=headers,
                             cookies=cookies, verify=False)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="cbox-1 p_loopitem"]/div')
        xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./div[2]/div//a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = news_title
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./div[2]//div[2]/p/text()')[0].strip()
            if '-' not in news_publish_time:
                news_publish_time = news_publish_time.replace('/', '-')
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./div[2]/div//a/@href')[0]
            if 'http://www.chinesevacuum.com' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.chinesevacuum.com' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = result.get('content')
    except:
        news_content = selector_page.xpath('//*[@class="dynamicsCon"]//p/text()')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="p_articles summary"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'>来源：</span>(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>阅读：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-新闻资讯-科协资讯': 'http://www.chinesevacuum.com/News/1630726885942366208-0-8.html',
        '首页-新闻资讯-学会动态': 'http://www.chinesevacuum.com/News/1630726886441488384-0-8.html',
        '首页-新闻资讯-专业委员会': 'http://www.chinesevacuum.com/News/1630726886928027648-0-8.html',
        '首页-新闻资讯-科普教育': 'http://www.chinesevacuum.com/News/1630726887414566912-0-8.html',
        '首页-新闻资讯-活动资讯': 'http://www.chinesevacuum.com/News/1630726893068488704-0-8.html',
        '首页-新闻资讯-市场动态': 'http://www.chinesevacuum.com/News/1630726893571805184-0-8.html',
        '首页-新闻资讯-地方学会': 'http://www.chinesevacuum.com/News/1630726894058344448-0-8.html',
        '首页-新闻资讯-直播专栏': 'http://www.chinesevacuum.com/News/1630726898760159232-0-8.html',
        # '首页-会议展览-会议信息': '',
        # '首页-学术天地-真空与低温杂志社': '-15929806608356522&cid=21&pageSize=8&currentPage=1',
        # '首页-学术天地-真空科学与技术学报': '-15929806608356522&cid=20&pageSize=8&currentPage=1',
        # '首页-学术天地-真空杂志': '-15929806608356522&cid=22&pageSize=8&currentPage=1',
        # '首页-学术天地-IUSVSTA国际联盟': '-15929806608356522&cid=25&pageSize=8&currentPage=1',
        # '首页-学术天地-人物专栏': '-15929806608356522&cid=26&pageSize=8&currentPage=1',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(10):
            url = value.replace('0-8.html', str(page * 8) + '-8.html')
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time)
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default', default='2023-01-01')
    args = parser.parse_args()
    account_name = 'B-13 中国真空学会'
    start_run(args.projectname, args.sinceyear, account_name)
