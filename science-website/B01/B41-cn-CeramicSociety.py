# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-01-06
import random
import re
import io
import sys
import time
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, page, cateid):

    headers = {
        'authority': 'www.ceramsoc.com',
        'accept': '*/*',
        'accept-language': 'zh-CN,zh;q=0.9',
        'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'cookie': 'think_language=zh-CN; PHPSESSID=60ff0ff0b16687a5e69bdbef79d3d74d',
        'origin': 'https://www.ceramsoc.com',
        'referer': url,
        'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
        'sec-ch-ua-mobile': '?1',
        'sec-ch-ua-platform': '"Android"',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin',
        'user-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Mobile Safari/537.36',
        'x-requested-with': 'XMLHttpRequest',
    }

    data = {
        'cateid': cateid,
        'p': page,
        'index': '0',
        'perpage': '10'
    }

    response = requests.post('https://www.ceramsoc.com/Home/Index/zx_list_inc', headers=headers, data=data)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="entry__excerpt"]')
        xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    # print(part_nodes)
    # breakpoint()
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./h2/a/text()')[0].strip()
        except:
            news_title = ''
        print(news_title)
        # breakpoint()
        try:
            news_abstract = part_nodes[i].xpath('./h2/a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./ul/li[1]/text()')[-1].strip()
            news_publish_time = news_publish_time.split(',')[-1] + '-' + '-'.join(news_publish_time.split(',')[0].split(' '))
            # print(news_publish_time)
            # breakpoint()
        except:
            news_publish_time = '2022-01-01'
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        try:
            read_count = part_nodes[i].xpath('./ul/li[2]/a/text()')[0].strip()
        except:
            read_count = ''
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./h2/a/@href')[0]
            if 'cateid/' in url_part2:
                break
            elif 'https://www' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.ceramsoc.com' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):
    headers = {
        # 'authority': 'www.ceramsoc.com',
        # 'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'accept-language': 'zh-CN,zh;q=0.9',
        # 'cache-control': 'max-age=0',
        # 'cookie': 'PHPSESSID=60ff0ff0b16687a5e69bdbef79d3d74d; think_language=zh-CN',
        # 'referer': news_page_url,
        # 'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
        # 'sec-ch-ua-mobile': '?1',
        # 'sec-ch-ua-platform': '"Android"',
        # 'sec-fetch-dest': 'document',
        # 'sec-fetch-mode': 'navigate',
        # 'sec-fetch-site': 'same-origin',
        # 'sec-fetch-user': '?1',
        # 'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Mobile Safari/537.36',
    }

    html_response = requests.get(news_page_url, headers=headers, verify=False)

    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    # print(result)
    # breakpoint()
    try:
        news_content = selector_page.xpath('//*[@class="entry__img-holder"]//p/text() |'
                                           ' //*[@class="entry__img-holder"]//span//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="entry__img-holder"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：</span>(.*?)</span>', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    '''
    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''
    '''
    try:
        click_count = re.findall(r'点击：</span><span id="hits">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-顶部幻灯': 'https://www.ceramsoc.com/News/Content/index/cateid/74.do',
        '首页-新闻动态': 'https://www.ceramsoc.com/News/Content/index/cateid/75.do',
        '首页-学会工作': 'https://www.ceramsoc.com/News/Content/index/cateid/121.do',
        '首页-活动会议': 'https://www.ceramsoc.com/News/Content/index/cateid/126.do',
        '首页-广告位': 'https://www.ceramsoc.com/News/Content/index/cateid/127.do',
        '首页-编辑出版': 'https://www.ceramsoc.com/News/Content/index/cateid/117.do',
        '首页-二十大要闻': 'https://www.ceramsoc.com/News/Content/index/cateid/151.do',
        '首页-党建': 'https://www.ceramsoc.com/News/Content/index/cateid/120.do',
        '首页-会员天地': 'https://www.ceramsoc.com/News/Content/index/cateid/119.do',
        '首页-科普教育': 'https://www.ceramsoc.com/News/Content/index/cateid/118.do',
        '首页-国际交流': 'https://www.ceramsoc.com/News/Content/index/cateid/116.do',
        '首页-学会活动': 'https://www.ceramsoc.com/News/Content/index/cateid/115.do',
        '首页-组织机构': 'https://www.ceramsoc.com/News/Content/index/cateid/114.do',
        '首页-学会介绍': 'https://www.ceramsoc.com/News/Content/index/cateid/113.do',
        '首页-学会活动-年度学术会议安排': 'https://www.ceramsoc.com/News/Content/index/cateid/140.do',
        '首页-学会活动-国内学术会议': 'https://www.ceramsoc.com/News/Content/index/cateid/141.do',
        '首页-学会活动-国际学术会议': 'https://www.ceramsoc.com/News/Content/index/cateid/142.do',
        '首页-国际交流-外事活动': 'https://www.ceramsoc.com/News/Content/index/cateid/143.do',
        '首页-国际交流-出访活动': 'https://www.ceramsoc.com/News/Content/index/cateid/144.do',

    }
    for key, value in urls.items():
        news_classify = key
        cateid = re.findall(r'cateid/(.*?).do', value)
        for page in range(0, 5):
            url = value
            html_text = get_html(url, page, cateid)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)

            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'B-41 中国硅酸盐学会'
    start_run(project_time, start_time, account_name)