# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-12-13
import random
import re
import io
import sys
import time
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url,  verify=False)

    response.encoding = 'utf-8'
    return response.text


def get_data(page_id, html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@id="list_tab"]/ul/li')
        xpath_data(page_id, part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(page_id, part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0].strip()
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'https:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.nfsoc.org.cn/' + page_id + url_part2.replace('./', '/')

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''



def get_page_content(news_page_url):

    html_response = requests.get(news_page_url,verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="TRS_Editor"]//p/span/text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="TRS_Editor"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源:<a href="" target="_blank">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    science_system = read_science_account(account_name)
    urls = {
        '首页-科普园地': 'http://www.nfsoc.org.cn/kpyd/index_1.html',
        '首页-重点新闻': 'http://www.nfsoc.org.cn/zdxw/index_1.html',
        '首页-政策法规': 'http://www.nfsoc.org.cn/zcfg_160/index_1.html',
        '首页-期刊出版': 'http://www.nfsoc.org.cn/qkcb/index_1.html',
        '首页-专家库': 'http://www.nfsoc.org.cn/zjk/index_1.html',
        '首页-行业新闻-国内新闻': 'http://www.nfsoc.org.cn/xyxw_157/gnxw_158/index_1.html',
        '首页-行业新闻-国际新闻': 'http://www.nfsoc.org.cn/xyxw_157/gnxw_159/index_1.html',
        '首页-学术活动-国内会议': 'http://www.nfsoc.org.cn/xshd_161/gnhy_162/index_1.html',
        '首页-学术活动-国际会议': 'http://www.nfsoc.org.cn/xshd_161/gnhy_163/index_1.html',
        '首页-培训中心-新闻资讯': 'http://www.nfsoc.org.cn/pxzx_164/xwzx_165/index_1.html',
        '首页-培训中心-培训信息': 'http://www.nfsoc.org.cn/pxzx_164/xwzx_166/index_1.html',
        '首页-培训中心-培训教材': 'http://www.nfsoc.org.cn/pxzx_164/xwzx_167/index_1.html',
        '首页-培训中心-培训项目': 'http://www.nfsoc.org.cn/pxzx_164/xwzx_168/index_1.html',
        '首页-培训中心-在线资源': 'http://www.nfsoc.org.cn/pxzx_164/xwzx_169/index_1.html',
        '首页-成果奖励-成果奖励': 'http://www.nfsoc.org.cn/cgjl/cgjl/index_1.html',
        '首页-成果奖励-科技成果': 'http://www.nfsoc.org.cn/cgjl/kjcg/index_1.html',
        '首页-党建强会-科协党建': 'http://www.nfsoc.org.cn/djqh/kxdj/index_1.html',
        '首页-党建强会-党建动态': 'http://www.nfsoc.org.cn/djqh/djdt/index_1.html',
        '首页-党建强会-有色党建': 'http://www.nfsoc.org.cn/djqh/ysdj/index_1.html',
        '首页-会员中心-会员动态': 'http://www.nfsoc.org.cn/hydt/index_1.html',
        '首页-会员中心-会员注册': 'http://www.nfsoc.org.cn/hydt/hyzc/index_1.html',
        '首页-会员中心-下载中心': 'http://www.nfsoc.org.cn/hydt/xzzx_192/index_1.html',
        '首页-成果奖励-科技奖励-科学技术奖': 'http://www.nfsoc.org.cn/cgjl/kjjl/kxjsj/index_1.html',
        '首页-成果奖励-科技奖励-青年科技奖': 'http://www.nfsoc.org.cn/cgjl/kjjl/qnkjj/index_1.html',
        '首页-成果奖励-科技奖励-项目评价': 'http://www.nfsoc.org.cn/cgjl/kjjl/xmpj/index_1.html',
        '首页-成果奖励-科技奖励-其他奖励': 'http://www.nfsoc.org.cn/cgjl/kjjl/qtjl/index_1.html',

    }
    for key, value in urls.items():
        news_classify = key
        page_id = re.findall(r'.cn/(.*?)/index', value)[0]
        # print(page_id)
        # breakpoint()
        for page in range(1, 5):
            if page == 1:
                url = value.replace('index_1', 'index')
            else:
                url = value.replace('index_1', 'index_' + str(page - 1))
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(page_id, html_text, news_classify, account_name,
                                    science_system, mongo, project_time, start_time)

            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'B-32 中国有色金属学会'
    start_run(project_time, start_time, account_name)
