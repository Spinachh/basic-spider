# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-12-02
import random
import re
import io
import sys
import time
import json
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="taskList newList"]/ul/li')
        xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./a/span/text()')[0].strip()
            if '-' not in news_publish_time:
                news_publish_time = news_publish_time.replace('/', '-')

        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'http://www.csice.org.cn/' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.cars.org.cn' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):
    # aid = re.findall(r'\d+', news_page_url)[0]
    aid = news_page_url.split('=')[-1]
    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': news_page_url,
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    params = (
        ('type', 'dynamic'),
        ('aid', aid),
    )

    html_response = requests.get('http://www.cars.org.cn/home/view',
                                 headers=headers,
                                 params=params,
                                 verify=False
                                 )
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = result.get('content')
    except:
        news_content = selector_page.xpath('//*[@class="dynamicsCon"]//p/text()')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="dynamicsCon"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'新闻来源：<span>(.*?)</span>', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>阅读：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学会工作-学会工作': 'http://www.cars.org.cn/home/list?type=dynamic&pageNumber=1',
        '首页-学会工作-表彰奖励': 'http://www.cars.org.cn/home/list?type=branch&pageNumber=1',
        '首页-学会工作-党建强会': 'http://www.cars.org.cn/home/list?type=strong&pageNumber=1',
        '首页-学会工作-学会期刊': 'http://www.cars.org.cn/home/list?type=education&pageNumber=1',
        '首页-学术会议-学术会议': 'http://www.cars.org.cn/home/list?type=learning&pageNumber=1',
        '首页-学术会议-会议通知': 'http://www.cars.org.cn/home/list?type=inform&pageNumber=1',
        '首页-学术会议-评奖申报': 'http://www.cars.org.cn/home/list?type=fund&pageNumber=1',
        '首页-承能服务-承能服务': 'http://www.cars.org.cn/home/list?type=sciencinfo&pageNumber=1',
        '首页-承能服务-科普动态': 'http://www.cars.org.cn/home/list?type=scienceact&pageNumber=1',
        '首页-承能服务-科普文章': 'http://www.cars.org.cn/home/list?type=scienceatcl&pageNumber=1',
        '首页-下载专区-下载专区': 'http://www.cars.org.cn/home/download?pageNumber=1',
        '首页-所有会议-所有会议': 'http://www.cars.org.cn/home/allMeetings?pageNumber=1',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 10):
            url = value.replace('pageNumber=1', 'pageNumber=' + str(page))
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time)
            time.sleep(1)
            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'B-11 中国空气动力学会'
    start_run(project_time, start_time, account_name)