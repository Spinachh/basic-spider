# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-01-04
import random
import re
import io
import sys
import time
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, columnid):

    cookies = {
        'JSESSIONID': '55FCC30688EBACFD3A757ECA41321074',
        'UM_distinctid': '1857aa7ab5c424-003ff0c34f5d5d-9186f2c-1fa400-1857aa7ab5d402',
        'CNZZDATA1278132517': '1603930798-1635742451-%7C1635742451',
    }

    headers = {
        'Accept': 'application/xml, text/xml, */*; q=0.01',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'http://www.cres.org.cn',
        'Referer': 'http://www.cres.org.cn/col/col6897/index.html?uid=25721&pageNum=2',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Mobile Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    params = (
        ('startrecord', '1'),
        ('endrecord', '53'),
        ('perpage', '40'),
    )

    data = {
        'col': '1',
        'appid': '1',
        'webid': '68',
        'path': '/',
        'columnid': columnid,
        'sourceContentType': '1',
        'unitid': '25721',
        'webname': '\u4E2D\u56FD\u53EF\u518D\u751F\u80FD\u6E90\u5B66\u4F1A',
        'permissiontype': '0'
    }

    response = requests.post(url, headers=headers, params=params,
                             cookies=cookies, data=data, verify=False)

    # response = requests.get(url, headers=headers, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    content = re.findall(r'<record><!\[CDATA\[(.*?)\]\>', html_text, re.M | re.S)
    # print(content[0])
    # breakpoint()
    try:
        ree_data(content, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(content)):
        try:
            news_title = re.findall(r'target="_blank">                <h2>(.*?)</h2>', content[i])[0]
        except:
            news_title = ''

        news_abstract = news_title

        try:
            news_publish_time = re.findall(r'/art/(.*?)/art_', content[i])[0].replace('/', '-')
            # print(news_publish_time)
            # breakpoint()
        except:
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = re.findall(r'<a href="(.*?)"', content[i])[0]
            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.cres.org.cn' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        # 'If-Modified-Since': 'Fri, 18 Nov 2022 08:01:41 GMT',
        # 'If-None-Match': '"a360-5edba1db323e0"',
        'Referer': news_page_url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Mobile Safari/537.36',
    }
    html_response = requests.get(news_page_url, headers=headers, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="u_content_text"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="u_content_text"]//p//img/@src |'
                                        ' //*[@class="u_content_text"]//section//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源:<a href="" target="_blank">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_url = re.findall(r"访问量：<script language='javascript' src=\"(.*?)\"",
                              content_text, re.M | re.S)[0]
        read_url = 'http://www.csm.org.cn' + read_url
        read_content = requests.get(read_url).text
        read_count = re.findall(r'document.write\("(.*?)"\);', read_content, re.M | re.S)[0]
        # print(read_count)
        # breakpoint()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻中心-学会动态': 'http://www.cres.org.cn/col/col6897/index.html',
        '首页-新闻中心-分类信息': 'http://www.cres.org.cn/col/col6899/index.html',
        '首页-新闻中心-通知公告': 'http://www.cres.org.cn/col/col6854/index.html',
        '首页-新闻中心-要闻推荐': 'http://www.cres.org.cn/col/col6896/index.html',
        '首页-新闻中心-分类信息-人才招聘': 'http://www.cres.org.cn/col/col7827/index.html',
        '首页-新闻中心-行业资讯-风能': 'http://www.cres.org.cn/col/col6900/index.html',
        '首页-新闻中心-行业资讯-生物质能': 'http://www.cres.org.cn/col/col6901/index.html',
        '首页-新闻中心-行业资讯-太阳能': 'http://www.cres.org.cn/col/col6902/index.html',
        '首页-新闻中心-行业资讯-地热能': 'http://www.cres.org.cn/col/col6903/index.html',
        '首页-新闻中心-行业资讯-海洋能': 'http://www.cres.org.cn/col/col6904/index.html',
        '首页-新闻中心-行业资讯-其它清洁能源': 'http://www.cres.org.cn/col/col6905/index.html',
        '首页-党建-党建动态': 'http://www.cres.org.cn/col/col6906/index.html',
        '首页-党建-学习科学': 'http://www.cres.org.cn/col/col6907/index.html',
        '首页-党建-理论文选': 'http://www.cres.org.cn/col/col6908/index.html',
        '首页-党建-弘扬科学精神': 'http://www.cres.org.cn/col/col6909/index.html',
        '首页-党建-学习理论': 'http://www.cres.org.cn/col/col6910/index.html',
        '首页-党建-学习时评': 'http://www.cres.org.cn/col/col6911/index.html',
        '首页-党建-最美科技工作者': 'http://www.cres.org.cn/col/col8120/index.html',
        '首页-智库-智库动态': 'http://www.cres.org.cn/col/col6913/index.html',
        '首页-智库-发展规划': 'http://www.cres.org.cn/col/col6914/index.html',
        '首页-智库-政策法规': 'http://www.cres.org.cn/col/col6916/index.html',
        '首页-智库-政策专题': 'http://www.cres.org.cn/col/col6917/index.html',
        '首页-智库-研究报告': 'http://www.cres.org.cn/col/col6915/index.html',
        '首页-学术-会议通知': 'http://www.cres.org.cn/col/col6919/index.html',
        '首页-科普-科普活动': 'http://www.cres.org.cn/col/col6923/index.html',
        '首页-科普-能源文章': 'http://www.cres.org.cn/col/col6925/index.html',
        '首页-科普-能源文章-分析论述': 'http://www.cres.org.cn/col/col6926/index.html',
        '首页-科普-能源文章-环境保护': 'http://www.cres.org.cn/col/col6927/index.html',
        '首页-科普-能源文章-气候变化': 'http://www.cres.org.cn/col/col6928/index.html',
    }
    for key, value in urls.items():
        news_classify = key
        url = value
        columnid = re.findall(r'col\/col(.*?)\/index', value)[0]
        html_text = get_html(url, columnid)
        # print(html_text)
        # breakpoint()
        get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time)
        time.sleep(1)
        time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'B-39 中国可再生能源学会'
    start_run(project_time, start_time, account_name)