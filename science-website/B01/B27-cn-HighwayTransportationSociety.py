# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2022-12-10
import random
import re
import io
import sys
import time
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    content = re.findall(r'<record><!\[CDATA\[(.*?)\]\>', html_text, re.M | re.S)
    try:
        ree_data(content, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))

def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(content)):
        try:
            news_title = re.findall(r'target="_blank">(.*?)</a></p>', content[i])[0]
        except:
            news_title = ''

        try:
            news_abstract = re.findall(r'target="_blank">(.*?)</a> ', content[i])[0]
            dr = re.compile(r'<[^>]+>', re.S)
            news_abstract = dr.sub('', news_abstract)

        except:
            news_abstract = ''

        try:
            news_publish_part1 = re.findall(r'<h2 style="font-size:24px; font-weight:bold">(.*?)</h2>'
                                            , content[i])[0]
            # print(news_publish_part1)
            # print(news_publish_part2)
            # breakpoint()
            news_publish_part2 = re.findall(r'<h5 style="font-size:13px;">(.*?)</h5>'
                                            , content[i])[0].replace('/', '-')

            news_publish_time = news_publish_part2 + '-' + news_publish_part1
        except:
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = re.findall(r'<p><a href="(.*?)"', content[i])[0].replace('"', '')
            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.chts.cn' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="contact"]//p//text()')
        news_content = ''.join(news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="contact"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源:<a href="" target="_blank">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻-通知公告': 'http://www.chts.cn/col/col1530/index.html?uid=12209&pageNum=1',
        '首页-新闻-学会动态': 'http://www.chts.cn/col/col1526/index.html?uid=11220&pageNum=1',
        '首页-新闻-业内资讯': 'http://www.chts.cn/col/col1531/index.html?uid=11220&pageNum=1',
        '首页-新闻-地方学会资讯': 'http://www.chts.cn/col/col1532/index.html?uid=11220&pageNum=1',
        '首页-新闻-青托工程': 'http://www.chts.cn/col/col8101/index.html?uid=11220&pageNum=1',
        '首页-新闻-精彩图片': 'http://www.chts.cn/col/col8101/index.html?uid=11220&pageNum=1',
        '首页-智库-智库动态': 'http://www.chts.cn/col/col1538/index.html?uid=11220&pageNum=1',
        '首页-学术与科普-学术新闻': 'http://www.chts.cn/col/col1558/index.html?uid=11220&pageNum=1',
        '首页-学术与科普-通知公告': 'http://www.chts.cn/col/col1559/index.html?uid=11220&pageNum=1',
        '首页-学术与科普-活动预告': 'http://www.chts.cn/col/col1560/index.html?uid=11220&pageNum=1',
        '首页-学术与科普-品牌会议': 'http://www.chts.cn/col/col1561/index.html?uid=11220&pageNum=1',
        '首页-学术与科普-学术期刊': 'http://www.chts.cn/col/col1562/index.html?uid=11220&pageNum=1',
        '首页-学术与科普-学术咨询': 'http://www.chts.cn/col/col1778/index.html?uid=11220&pageNum=1',
        '首页-标准-标准动态': 'http://www.chts.cn/col/col1606/index.html?uid=11220&pageNum=1',
        '首页-标准-标准通知': 'http://www.chts.cn/col/col1608/index.html?uid=11220&pageNum=1',
        '首页-标准-快速链接': 'http://www.chts.cn/col/col1607/index.html?uid=11220&pageNum=1',
        '首页-认证-公司动态': 'http://www.chts.cn/col/col7138/index.html?uid=11220&pageNum=1',
        '首页-认证-主要业务': 'http://www.chts.cn/col/col7139/index.html?uid=11220&pageNum=1',
        '首页-认证-认证业务': 'http://www.chts.cn/col/col7703/index.html?uid=11220&pageNum=1',
        '首页-认证-双碳业务': 'http://www.chts.cn/col/col7704/index.html?uid=11220&pageNum=1',
        '首页-科技评价-科技评价动态': 'http://www.chts.cn/col/col1609/index.html?uid=11220&pageNum=1',
        '首页-科技评价-科技评价': 'http://www.chts.cn/col/col1610/index.html?uid=11220&pageNum=1',
        '首页-科技评价-项目咨询': 'http://www.chts.cn/col/col1611/index.html?uid=11220&pageNum=1',
        '首页-科技评价-科技奖励': 'http://www.chts.cn/col/col1612/index.html?uid=11220&pageNum=1',
        '首页-成果推广-成果推广动态': 'http://www.chts.cn/col/col1613/index.html?uid=11220&pageNum=1',
        '首页-成果推广-科技成果': 'http://www.chts.cn/col/col1614/index.html?uid=11220&pageNum=1',
        '首页-成果推广-成果转化': 'http://www.chts.cn/col/col1615/index.html?uid=11220&pageNum=1',
        '首页-教育培训-教育培训动态': 'http://www.chts.cn/col/col1616/index.html?uid=11220&pageNum=1',
        '首页-教育培训-特色培训': 'http://www.chts.cn/col/col1617/index.html?uid=11220&pageNum=1',
        '首页-教育培训-高速公路管理学院': 'http://www.chts.cn/col/col1618/index.html?uid=11220&pageNum=1',
        '首页-国际合作-国际合作动态': 'http://www.chts.cn/col/col1534/index.html?uid=11220&pageNum=1',
        '首页-国际合作-国际会议': 'http://www.chts.cn/col/col1535/index.html?uid=11220&pageNum=1',
        '首页-国际合作-一带一路': 'http://www.chts.cn/col/col1536/index.html?uid=11220&pageNum=1',
        '首页-党建工作-党建动态': 'http://www.chts.cn/col/col1564/index.html?uid=12209&pageNum=1',
        '首页-党建工作-党建工作': 'http://www.chts.cn/col/col1565/index.html?uid=12209&pageNum=1',
        '首页-党建工作-二十大学习专栏': 'http://www.chts.cn/col/col1566/index.html?uid=12209&pageNum=1',
        '首页-党建工作-党建专题': 'http://www.chts.cn/col/col1567/index.html?uid=12209&pageNum=1',
        '首页-党建工作-党建活动': 'http://www.chts.cn/col/col1568/index.html?uid=12209&pageNum=1',
    }
    for key, value in urls.items():
        news_classify = key
        url = value
        html_text = get_html(url)
        # print(html_text)
        # breakpoint()
        get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time)
        time.sleep(1)
        time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    project_time = '2022-Q4'
    start_time = '2022-07-01'
    account_name = 'B-27 中国公路学会'
    start_run(project_time, start_time, account_name)
