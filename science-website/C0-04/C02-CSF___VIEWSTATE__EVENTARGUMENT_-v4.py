# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-11-03

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
import urllib3
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_html(url, newstype, page):
    cookies = {
        'td_cookie': '506445088',
        'ASP.NET_SessionId': 'jkr3otkdiyqwmegjobzp1bly',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Origin': 'http://www.csf.org.cn',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/109.0.0.0 Mobile Safari/537.36',
    }

    params = (
        ('newstype', newstype),
    )

    data_content = requests.post(url, headers=headers).text

    viewstate = re.findall(r'value="(.*?)" />', data_content, re.M | re.S)[0]
    # viewstategenerator = re.findall(r'id="__VIEWSTATEGENERATOR" value="(.*?)" />', data_content, re.M | re.S)[0]
    eventvalidation = re.findall(r'id="__EVENTVALIDATION" value="(.*?)" />', data_content, re.M | re.S)[0]

    data = {
        '__VIEWSTATE': viewstate,
        '__VIEWSTATEGENERATOR': '03E81997',
        '__EVENTTARGET': 'AspNetPager1',
        '__EVENTARGUMENT': page,
        '__EVENTVALIDATION': eventvalidation,
        'top1$ddl_Type': '1',
        'top1$txtBox_KW': ''
    }

    response = requests.post(url, headers=headers, params=params, cookies=cookies,
                             data=data, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="news_list"]/ul/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''
        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./a/i/text()')[0]
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'cateid/' in url_part2:
                break
            elif 'http://' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.csf.org.cn/news/' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    # print(result)
    # breakpoint()
    try:
        news_content = selector_page.xpath('//*[@class="news_content"]//p//text()')
        news_content = ''.join(x.strip() for x in news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="news_content"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：</dt>(.*?)</dd', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'阅读<span>(.*?)</', content_text, re.M | re.S)[0]
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-新闻报道-最新要闻': 'http://www.csf.org.cn/news/newslist.aspx?newstype=7',
        '首页-新闻报道-综合工作': 'http://www.csf.org.cn/news/newslist.aspx?newstype=1',
        '首页-新闻报道-学术引领': 'http://www.csf.org.cn/news/newslist.aspx?newstype=8',
        '首页-新闻报道-科普惠民': 'http://www.csf.org.cn/news/newslist.aspx?newstype=9',
        '首页-新闻报道-咨询服务': 'http://www.csf.org.cn/news/newslist.aspx?newstype=10',
        '首页-新闻报道-期刊编辑': 'http://www.csf.org.cn/news/newslist.aspx?newstype=11',
        '首页-新闻报道-国际交流': 'http://www.csf.org.cn/news/newslist.aspx?newstype=12',
        '首页-新闻报道-科创中国': 'http://www.csf.org.cn/news/newslist.aspx?newstype=13',
        '首页-新闻报道-党建强会': 'http://www.csf.org.cn/news/newslist.aspx?newstype=14',
        '首页-新闻报道-科技奖励与人才': 'http://www.csf.org.cn/news/newslist.aspx?newstype=15',
        '首页-新闻报道-分支机构和省级学会动态': 'http://www.csf.org.cn/news/newslist.aspx?newstype=2',
        '首页-新闻报道-其它新闻-基地快讯': 'http://www.csf.org.cn/news/newslist.aspx?newstype=4',
        '首页-新闻报道-其它新闻-媒体关注': 'http://www.csf.org.cn/news/newslist.aspx?newstype=5',
        '首页-新闻报道-其它新闻-行业新闻': 'http://www.csf.org.cn/news/newslist.aspx?newstype=6',
        '首页-文件通知-工作通知': 'http://www.csf.org.cn/news/noticelist.aspx?typeid=1',
    }
    for key, value in urls.items():
        news_classify = key
        try:
            newstype = re.findall(r'\?newstype=(.*?)', value)[0]
        except:
            newstype = re.findall(r'\?typeid=(.*?)', value)[0]
        for page in range(1, 50):
            html_text, status_code = get_html(value, newstype, page)
            if status_code != 200:
                logging.warning('Has Not Got the Html\'s Correct Response')
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'C-02 中国林学会'
    start_run(args.projectname, args.sinceyear, account_name)
