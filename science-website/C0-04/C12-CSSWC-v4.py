# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-11-10

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
import urllib3
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_supflash(url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }
    response = requests.get(url, headers=headers, verify=False)
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url):

    cookies = {
        'td_cookie': get_supflash(url),
        'Hm_lvt_48d95ee395b5f1a777d0372240134a32': '1688541520',
        'Hm_lpvt_48d95ee395b5f1a777d0372240134a32': '1688541811',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/114.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers,
                            cookies=cookies, verify=False)

    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="col-xs-12 rightcontent"]/ul/li')
        deadline = xpath_data(part1_nodes, news_classify,
                              science_system, mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./a/span/text()')[0]
            news_publish_time = re.sub(r'[\],\[]', '', news_publish_time)
            # print(news_publish_time)
            # breakpoint()
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'http://' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.sbxh.org' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    cookies = {
        'td_cookie': get_supflash(news_page_url),
        'Hm_lvt_48d95ee395b5f1a777d0372240134a32': '1688541520',
        'Hm_lpvt_48d95ee395b5f1a777d0372240134a32': '1688541811',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': news_page_url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/114.0.0.0 Safari/537.36',
    }

    html_response = requests.get(news_page_url, headers=headers,
                            cookies=cookies, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="content"]//span/text()')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = re.findall(r'作者:(.*?)</', content_text, re.M | re.S)[0].strip()

    try:
        news_imgs = selector_page.xpath('//*[@class="content"]//span//img/@src |'
                                        ' //*[@class="content"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源:(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    # 这里的点击数是api接口获取，网站coder比较懒没有继续做
    try:
        aid = re.findall(r'\d+', news_page_url.split('/')[-1])
        click_url = 'http://www.sbxh.org/sbxhindex/plus/count.php?view=yes&aid={}&mid=2'.format(aid)
        click_response = requests.get(click_url).text
        click_count = re.findall(r'\d+', click_response)[0]
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        # '主页-关于学会': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xuehuixinwen/list_1_1.html',
        # '主页-专业委员会': 'http://www.sbxh.org/sbxhindex/a/zhuanyeweiyuanhui/list_37_1.html',
        # '主页-关于学会-省级学会': 'http://www.sbxh.org/sbxhindex/a/shengjixuehui/list_38_1.html',
        # '主页-关于学会-专业委员会': 'http://www.sbxh.org/sbxhindex/a/guanyuxuehui/zhuanyeweiyuanhui/',
        # '主页-通知公告-水平评价': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xuehuixinwen/list_33_1.html',
        # '主页-通知公告-培训': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xuehuixinwen/list_34_1.html',
        # '主页-通知公告-奖励': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xuehuixinwen/list_35_1.html',
        # '主页-通知公告-其它': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xuehuixinwen/list_72_1.html',
        # '主页-新闻资讯': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xuehuixinwen/list_9_1.html',
        # '主页-新闻资讯-学会新闻': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xuehuixinwen/list_10_1.html',
        # '主页-新闻资讯-行业新闻': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xingyexinwen/list_11_1.html',
        # '主页-新闻资讯-社会新闻': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xingyexinwen/list_12_1.html',
        # '主页-学术活动-国内活动': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xingyexinwen/list_15_1.html',
        # '主页-学术活动-国际活动': 'http://www.sbxh.org/sbxhindex/a/xueshuhuodong/guowaihuodong/',
        # '主页-学术活动-成功评价': 'http://www.sbxh.org/sbxhindex/a/xueshuhuodong/chengguopingjia/',
        # '主页-科普园地': 'http://www.sbxh.org/sbxhindex/a/kepuyuandi/kepuhuodong/list_17_1.html',
        # '主页-科普园地-科普活动': 'http://www.sbxh.org/sbxhindex/a/kepuyuandi/kepuhuodong/list_18_1.html',
        # '主页-科普园地-科普知识': 'http://www.sbxh.org/sbxhindex/a/kepuyuandi/kepuzhishi/',
        # '主页-科普园地-科普基地': 'http://www.sbxh.org/sbxhindex/a/kepuyuandi/kepujidi/',
        # '主页-水平评价-管理办法': 'http://www.sbxh.org/sbxhindex/a/zizhiguanli/guanlibanfa/',
        # '主页-水平评价-通知公告': 'http://www.sbxh.org/sbxhindex/a/xiangguanwenjian/xiangguanwenjian/list_68_1.html',
        # '主页-水平评价-相关材料': 'http://www.sbxh.org/sbxhindex/a/xiangguanwenjian/',
        # '主页-表彰奖励-相关文件': 'http://www.sbxh.org/sbxhindex/a/biaozhangjiangli/xiangguanwenjian/',
        # '主页-会员管理-通知公告': 'http://www.sbxh.org/sbxhindex/a/huiyuanguanli/xiangguancailiao/',
        # '主页-党建工作-党的活动': 'http://www.sbxh.org/sbxhindex/a/dangjiangongzuo/dangdehuodong/',
        # '主页-下载专区': 'http://www.sbxh.org/sbxhindex/a/xiangguanxiazai/',
        '主页-通知公告': 'http://www.sbxh.org/sbxhindex/a/tongzhigonggao/list_32_1.html',
        '主页-新闻资讯': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/list_9_1.html',
        '主页-学术活动': 'http://www.sbxh.org/sbxhindex/a/xueshuhuodong/list_14_1.html',
        '主页-科普园地': 'http://www.sbxh.org/sbxhindex/a/kepuyuandi/list_17_1.html',
        '主页-省级学会': 'http://www.sbxh.org/sbxhindex/a/shengjixuehui/list_38_1.html',
        '主页-专业委员会': 'http://www.sbxh.org/sbxhindex/a/zhuanyeweiyuanhui/list_37_1.html',



    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            if '.html' in value:
                url = value.replace('_1.html', '_' + str(page) + '.html')
            else:
                url = value
            html_text, status_code = get_html(url)
            if status_code != 200:
                logging.warning('Has Not Got the Html\'s Correct Response')
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'C-12 中国水土保持学会'
    start_run(args.projectname, args.sinceyear, account_name)
