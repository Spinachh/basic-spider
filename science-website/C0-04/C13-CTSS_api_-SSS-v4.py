# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-11-10

import random
import re
import io
import sys
import time
import json
import urllib3
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content1
    collection = db['{}'.format(c_name)]
    return collection


def get_html(url):
    response = requests.get(url, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    content = json.loads(html_text)
    try:
        deadline = ree_data(content, news_classify, science_system, mongo,
                            account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
             account_name, project_time, start_time):
    results = content['varList']
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for item in results:
        try:
            news_title = item.get('PART_TITLE')
        except:
            news_title = ''
        try:
            news_abstract = item.get('PART_TITLE')
        except:
            news_abstract = ''

        try:
            news_publish_time = item.get('PUBLISH_TIME')
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_publish_time)
        # print(news_publish_stamp)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            news_page_url = 'https://chinatss.cn/teaApi/party/detail?PARTY_ID={}'.format(item.get('PARTY_ID'))
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    html_text = json.loads(html_response.text)
    content_text = html_text.get('data')
    # selector_page = etree.HTML(content_text)
    # extractor = GeneralNewsExtractor()
    # result = extractor.extract(content_text)
    try:
        news_content = content_text.get('PARTY_CONTENT')
        dr = re.compile(r'<[^>]+>', re.S)
        news_content = dr.sub('', news_content).strip()
    except:
        news_content = ''
    try:
        news_author = content_text.get('CREATE_BY')
    except:
        news_author = ''
    try:
        news_imgs = content_text.get('ATTACHMENT')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = content_text.get('PARTY_AUTHOR')
    except:
        source = ''

    try:
        read_count = re.findall(r'阅读量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学会要闻': 'https://chinatss.cn/teaApi/focus/listPhone?showCount=10&currentPage=1',
        '首页-通知公告': 'https://chinatss.cn/teaApi/notify/listPhone?showCount=10&currentPage=1',
        '首页-党建强会-党史学习': 'https://chinatss.cn/teaApi/copHistory/list?showCount=10&currentPage=1',
        '首页-党建强会-党建活动': 'https://chinatss.cn/teaApi/party/listPhone?showCount=10&currentPage=1',
        '首页-学术交流-通知公告': 'https://chinatss.cn/teaApi/notice/listPhone?showCount=10&currentPage=1',
        '首页-学术交流-交流成果': 'https://chinatss.cn/teaApi/exchangeResults/listPhone?showCount=5&currentPage=1',
        # '首页-科技普及-科普常识': 'https://chinatss.cn/teaApi/knowledge/listPhone?showCount=6&currentPage=1',
        '首页-品质评价-评价要闻': 'https://chinatss.cn/teaApi/evaluationFocus/list?showCount=10&currentPage=1',
        '首页-成果评价-科技成果评价': 'https://chinatss.cn/teaApi/AEATNotice/list?showCount=10&currentPage=1',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            url = value.replace('currentPage=1', 'currentPage=' + str(page))
            html_text, status_code = get_html(url)
            if status_code != 200:
                logging.warning('Has Not Got the Html\'s Correct Response')
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'C-13 中国茶叶学会'
    start_run(args.projectname, args.sinceyear, account_name)
