# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-10-17

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
import urllib3
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content4
    collection = db['{}'.format(c_name)]
    return collection


def get_html(year, month):

    headers = {
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Origin': 'https://www.cyscc.org',
        'Referer': 'https://www.cyscc.org/',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-site',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
        'sec-ch-ua': '"Chromium";v="110", "Not A(Brand";v="24", "Google Chrome";v="110"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    params = (
        ('MOLAWARE', '1'),
        ('year', year),
        ('month', month),
    )

    response = requests.get('https://api-portal.cyscc.org/portal/api/workTrend/findListByType', headers=headers,
                            params=params).json()
    # status_code = response.status_code
    # response.encoding = 'utf-8'
    return response


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    # print('get_data', html_text)
    # breakpoint()
    # html_text = str(html_text).replace("('", '').replace("', 200)", '')
    # print(html_text)
    # breakpoint()

    try:
        content = html_text.get('data')
        # print(content)
        # breakpoint()
        news_branch = {
            'gsggList': 'notice', 'dfdtList': 'news', 'gzbdList': 'news',
            'hdtzList': 'notice', 'mtxwList': 'news',
        }
        for branch, branch_type in news_branch.items():
            news_classify = news_classify + '-' + branch
            results = content.get(branch)
            # print('get_data_result:', results)
            # breakpoint()
            deadline = ree_data(results, news_classify, science_system, mongo,
                                account_name, project_time, start_time, branch_type)
            if deadline:
                return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(results, news_classify, science_system, mongo,
             account_name, project_time, start_time, branch_type):
    news_dict_name = [
        'news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
        'news_content_type', 'news_content', 'news_page_url', 'source',
        'news_author', 'read_count', 'click_count', 'news_classify',
        'crawl_time', 'account_name', 'science_system', 'project_time'
    ]

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    # print('result:', results)
    # breakpoint()
    for item in results:
        try:
            news_title = item.get('title').strip()
        except:
            news_title = ''

        try:
            news_abstract = item.get('title').strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = item.get('publishTime').split(' ')[0]
        except Exception as e:
            logging.warning('Publish Time Was Error: {}'.format(e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            id = item.get('id')
            news_page_url = 'https://www.cyscc.org/#/newsDetail?type=notice&id={}'.format(id)
            news_api_url = 'https://api-portal.cyscc.org/portal/api/{}/{}?MOLAWARE=1'.format(branch_type, id)

            if 'mp.weixin.qq.c' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_api_url, branch_type)
            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_api_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            logging.warning(news_dict_content)
            breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_api_url, branch_type):
    headers = {
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Origin': 'https://www.cyscc.org',
        'Referer': 'https://www.cyscc.org/',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-site',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
        'sec-ch-ua': '"Chromium";v="110", "Not A(Brand";v="24", "Google Chrome";v="110"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    params = (
        ('MOLAWARE', '1'),
    )

    html_response = requests.get(
        news_api_url,
        headers=headers, params=params
    )

    html_response.encoding = 'utf-8'
    content_text = html_response.json()

    try:
        if branch_type == 'notice':
            content = content_text['content']
        else:
            content = content_text['articleContent']

        text = re.findall(
            r'[\u3002\uff1b\uff0c\uff1a\u201c\u201d\uff08\uff09\u3001\uff1f\u300a\u300b\u4e00-\u9fa5\d+]',
            content
        )
        news_content = ''.join([x.strip() for x in text])

    except:
        news_content = ''

    try:
        if branch_type == 'notice':
            news_author = content_text['author']
        else:
            news_author = content_text['articleAuthor']
    except:
        news_author = ''

    try:
        if branch_type == 'notice':
            content = content_text['content']
        else:
            content = content_text['articleContent']

        text = re.findall(
            r'[\u3002\uff1b\uff0c\uff1a\u201c\u201d\uff08\uff09\u3001\uff1f\u300a\u300b\u4e00-\u9fa5]',
            content
        )
        news_imgs = re.findall(r'src="(.*?)"', str(text))[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        if branch_type == 'notice':
            source = content_text['articleFrom']
        else:
            source = content_text['From']
    except:
        source = ''

    try:
        read_count = content_text['articleRead']
    except:
        read_count = ''

    try:
        click_count = content_text['articleClick']
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-工作动态': 'https://api-portal.cyscc.org/portal/api/workTrend/findListByType?MOLAWARE=1',
    }
    for key, value in urls.items():
        news_classify = key
        for year in range(2022, 2024):
            for month in range(1, 13):
                # url = value + '&year={}&month={}'.format(year, month)

                crontrol_time = str(year) + '-' + str(month)
                start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
                crontrol_time_stamp = time.mktime(time.strptime(crontrol_time, '%Y-%m'))

                if int(start_time_stamp) < int(crontrol_time_stamp) < int(time.time()):
                    html_text = get_html(year, month)
                    # print('filter_time', html_text)
                    # breakpoint()
                    try:
                        get_data(html_text, news_classify, account_name,
                                 science_system, mongo, project_time, start_time)
                    except Exception as e:
                        logging.warning('Get Data Was Error:{}'.format(e))
                        break
                    time.sleep(1)
                else:
                    break


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'DU-05 中国科协青少年科技中心'
    start_run(args.projectname, args.sinceyear, account_name)
