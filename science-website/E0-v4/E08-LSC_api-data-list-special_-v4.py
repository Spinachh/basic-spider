# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-11-30

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
import urllib3
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content4
    collection = db['{}'.format(c_name)]
    return collection


def get_html(idCategory, siteGroup, page):
    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'https://www.lsc.org.cn/',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
        'sec-ch-ua': '"Chromium";v="110", "Not A(Brand";v="24", "Google Chrome";v="110"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    params = (
        ('pageSize', '10'),
        ('pageNum', page),
        ('idCategory', idCategory),
        ('siteGroup', siteGroup),
    )

    response = requests.get('https://www.lsc.org.cn/cns/pc_pass/content/pcDataPage',
                            headers=headers, params=params)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    try:
        if 'special' not in news_classify:
            content = json.loads(html_text)
            deadline = ree_data(content, news_classify, science_system, mongo,
                                account_name, project_time, start_time)
            if deadline:
                return deadline
        else:
            selector = etree.HTML(html_text)
            part1_nodes = selector.xpath('//*[@class="kx-content"]//li')
            deadline = xpath_data(part1_nodes, news_classify, science_system, mongo,
                                  account_name, project_time, start_time)
            if deadline:
                return deadline

    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
             account_name, project_time, start_time):
    results = content['data']['result']
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for item in results:
        try:
            news_title = item.get('title')
        except:
            news_title = ''
        try:
            news_abstract = item.get('introduce').strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = item.get('publishTime').split(' ')[0]
        except Exception as e:
            logging.warning('Publish Time Was Error: {}'.format(e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # print(news_publish_stamp)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            idCategory = item.get('idCategory')
            article_id = item.get('id')
            news_page_url = 'https://www.lsc.org.cn/cns/contents/{}/{}.html'.format(idCategory, article_id)

            if 'https://mp.weixin.qq.co' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if '要闻动态' in news_classify:
                news_title = part_nodes[i].xpath('.//a/p[1]/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./a/@title')[0].strip()
        except Exception as e:
            logging.warning('Title Was Error: {}'.format(e))
            news_title = ''
        try:
            if '要闻动态' in news_classify:
                news_abstract = part_nodes[i].xpath('.//a/p[2]/text()')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()

        except:
            news_abstract = ''

        try:
            if '要闻动态' in news_classify:
                news_publish_d = part_nodes[i].xpath('.//h3/text()')[0]
                news_publish_y = part_nodes[i].xpath('.//h2/text()')[0]
                news_publish_time = news_publish_y + '-' + news_publish_d
            else:
                news_publish_time = part_nodes[i].xpath('./time/text()')[0].strip()
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if '要闻动态' not in news_classify:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0]
                else:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0]

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.imast.org.cn' + url_part2

            if 'https://mp.weixin.qq.com' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''
    try:
        news_content = selector_page.xpath('//*[@class="contentMsg"]//p//span//text() | '
                                           '//*[@class="contentMsg"]//p//text() | '
                                           '//*[@class="contentMsg"]//span//text() | '
                                           )
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者：(.*?) ', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="contentMsg"]//span//img/@src |'
                                        '//*[@class="contentMsg"]//p//img/@src | '
                                        '//*[@class="contentMsg"]//div//img/@src |'
                                        '//*[@class="contentMsg"]//img/@src')[0]
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="details-page"]//span//video/@src |'
                                         ' //*[@class="details-page"]//p//video/@src |'
                                         ' //*[@class="details-page"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip().split('来源')[-1]
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        if '.shtml' in news_page_url:
            ID = news_page_url.split('/')[-1].split('.')[0]
            click_url = 'https://www.imast.org.cn//zcms/front/counter?Type=Article&ID={}&DomID=hitcount{}' \
                .format(ID, ID)
            # https://www.imast.org.cn//zcms/front/counter?Type=Article&ID=22621&DomID=hitcount22621
            click_content = requests.get(click_url).text
            click_count = re.findall(r'innerHTML=(.*?);', click_content)[0]
        else:
            click_count = ''
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-栏目首页-通知公告': 'https://www.lsc.org.cn/cns/categoryList?id=1129&siteGroup=1',
        '首页-栏目首页-最新资讯': 'https://www.lsc.org.cn/cns/categoryList?id=1127&siteGroup=1',
        '首页-栏目首页-热点新闻': 'https://www.lsc.org.cn/cns/categoryList?id=1121&siteGroup=1',
        '首页-党的建设-热点新闻': 'https://www.lsc.org.cn/cns/categoryList?id=1652774688689&siteGroup=24',
        '首页-党的建设-党建要闻': 'https://www.lsc.org.cn/cns/categoryList?id=1652774704963&siteGroup=24',
        '首页-党的建设-党建动态': 'https://www.lsc.org.cn/cns/categoryList?id=1304&siteGroup=24',
        '首页-学术交流-年会': 'https://www.lsc.org.cn/cns/categoryList?id=1135&siteGroup=1',
        '首页-学术交流-专项资助': 'https://www.lsc.org.cn/cns/categoryList?id=1384&siteGroup=1',
        '首页-行业协调-图书馆评估': 'https://www.lsc.org.cn/cns/categoryList?id=1184&siteGroup=1',
        '首页-行业协调-全国智慧图书馆建设联席会议': 'https://www.lsc.org.cn/cns/categoryList?id=1186&siteGroup=1',
        '首页-行业协调-革命文献与民国时期文献保护': 'https://www.lsc.org.cn/cns/categoryList?id=1670908857115&siteGroup=1',
        '首页-行业协调-继续教育': 'https://www.lsc.org.cn/cns/categoryList?id=1124&siteGroup=1',
        '首页-科普阅读-阅读推广人”培育行动': 'https://www.lsc.org.cn/cns/categoryList?id=1198&siteGroup=1',
        '首页-科普阅读-活动资源': 'https://www.lsc.org.cn/cns/categoryList?id=1200&siteGroup=1',
        '首页-科普阅读-全民阅读': 'https://www.lsc.org.cn/cns/categoryList?id=1198&siteGroup=1',
        '首页-科普阅读-品牌阅读': 'https://www.lsc.org.cn/cns/categoryList?id=1197&siteGroup=1',
        '首页-科普阅读-科普动态': 'https://www.lsc.org.cn/cns/categoryList?id=1199&siteGroup=1',
        '首页-对外交流-综合信息': 'https://www.lsc.org.cn/cns/categoryList?id=1275&siteGroup=1',
        '首页-业界动态-图书馆动态': 'https://www.lsc.org.cn/cns/categoryList?id=1132&siteGroup=1',
        '首页-业界动态-地方学会动态': 'https://www.lsc.org.cn/cns/categoryList?id=1131&siteGroup=1',
        '首页-交流展示-凝聚精华 助力推广': 'https://www.lsc.org.cn/cns/categoryList?id=1672215860724&siteGroup=1',
        '首页-交流展示-图书馆阅读空间展示': 'https://www.lsc.org.cn/cns/categoryList?id=1385&siteGroup=1',
        '首页-关于学会-分支机构动态': 'https://www.lsc.org.cn/cns/categoryList?id=1149&siteGroup=1',
        '首页-关于学会-组织建设': 'https://www.lsc.org.cn/cns/categoryList?id=1652780564792&siteGroup=1',
        '首页-关于学会-规章制度': 'https://www.lsc.org.cn/cns/categoryList?id=1143&siteGroup=1',
    }
    for key, value in urls.items():
        news_classify = key

        for page in range(1, 50):
            idCategory = re.findall(r'id=(.*?)&', value)[0]
            siteGroup = re.findall(r'siteGroup=(\d+)', value)[0]
            html_text, status_code = get_html(idCategory, siteGroup, page)
            if status_code != 200:
                logging.warning('Has Not Got the Html\'s Correct Response')
                break

            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'E-08 中国图书馆学会'
    start_run(args.projectname, args.sinceyear, account_name)
