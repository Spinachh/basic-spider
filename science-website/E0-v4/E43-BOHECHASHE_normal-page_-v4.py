# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2024-01-11

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import urllib3
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
urllib3.disable_warnings()
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content2
    collection = db['{}'.format(c_name)]
    return collection


def get_supflash(url):

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36',
    }
    response = requests.get(url, headers=headers, )
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, page):

    cookies = {
        'td_cookie': get_supflash(url),
        'SSCSum': '7',
        'zh_choose': 'n',
        'Hm_lvt_56f79f10e32a45d6625df4d57469db56': '1704952804',
        'Hm_lpvt_56f79f10e32a45d6625df4d57469db56': '1704953383',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/120.0.0.0 Safari/537.36',
    }

    params = (
        ('page', page),
    )
    # 'http://www.bohechashe.org.cn/category/2082?page=',
    # 'http://www.bohechashe.org.cn/category/2082'
    req_url = url.split('?')[0]
    response = requests.get(req_url, headers=headers, params=params,
                            cookies=cookies, verify=False)
    response.encoding = 'utf-8'
    status_code = response.status_code
    # print(response.text)
    # breakpoint()
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    try:
        if 'special' not in news_classify:
            selector = etree.HTML(html_text)
            part_nodes1 = selector.xpath('//*[@class="wzlb"]/div/ul')
            part_nodes = part_nodes1
            deadline = xpath_data(part_nodes, news_classify, science_system, mongo,
                                  account_name, project_time, start_time)
            if deadline:
                return deadline
        else:
            selector = etree.HTML(html_text)
            part1_nodes = selector.xpath('//*[@class="first_list three"]/li')
            deadline = xpath_data(part1_nodes, news_classify, science_system, mongo,
                                  account_name, project_time, start_time)
            if deadline:
                return deadline

    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(1, len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('./li[1]/a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = part_nodes[i].xpath('./li[2]/a/text()')[0].strip()
            else:
                news_abstract = news_title
        except:
            news_abstract = ''

        # print(news_title)
        # print(news_abstract)
        # breakpoint()

        try:
            if 'special' not in news_classify:
                url_part2 = part_nodes[i].xpath('./li[1]/a/@href')[0].strip()
            else:
                url_part2 = part_nodes[i].xpath('./a/@href')[0].strip()

        except Exception as e:
            logging.warning('{} {} ARTICLE URL WAS ERROR :{}'.format(account_name, news_classify, e))
            break

        if 'http' in url_part2:
            news_page_url = url_part2
        else:
            news_page_url = 'http://www.bohechashe.org.cn' + url_part2

        if 'mp.weixin.qq' in news_page_url:
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count, news_publish_time = get_weixin_page_content(news_page_url)

        else:
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count, news_publish_time = get_page_content(news_page_url)

        crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

        news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                           news_content_type, news_content, news_page_url, source,
                           news_author, read_count, click_count, news_classify, crawl_time,
                           account_name, science_system, project_time,
                           ]

        news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))

        # logging.warning(news_dict_content)
        # print(news_publish_time)
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        if int(news_publish_stamp) >= int(start_time_stamp):

            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    cookies = {
        'td_cookie': get_supflash(news_page_url),
        'SSCSum': '3',
        'zh_choose': 'n',
        'Hm_lvt_56f79f10e32a45d6625df4d57469db56': '1704952804',
        'Hm_lpvt_56f79f10e32a45d6625df4d57469db56': '1704953872',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/120.0.0.0 Safari/537.36',
    }

    html_response = requests.get(news_page_url, headers=headers, cookies=cookies, verify=False)

    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''
    try:
        news_content = selector_page.xpath('//*[@class="nr_left"]//span//text() | '
                                           '//*[@class="nr_left"]//p//text() | '
                                           '//*[@class="nr_left"]//p//span//text()'
                                           )
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者：(.*?)&nbsp;', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="nr_left"]//span//img/@src |'
                                        '//*[@class="nr_left"]//p//img/@src | '
                                        '//*[@class="nr_left"]//div//img/@src |'
                                        '//*[@class="nr_left"]//img/@src')[0]
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="nr_left"]//span//video/@src |'
                                         ' //*[@class="nr_left"]//p//video/@src |'
                                         ' //*[@class="nr_left"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源：(.*?)&nbsp', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r';阅读数：(.*?)&nbsp', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''
    # print(read_count)
    # breakpoint()

    try:
        click_count = re.findall(r'点击量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    try:
        news_publish_time = re.findall(r'日期：(.*?) <div', content_text, re.M | re.S)[0].strip()
    except:
        news_publish_time = '2023-01-01'

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count, news_publish_time


def get_weixin_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    news_publish_time = re.findall(r"var createTime = '(.*?)'", content_text, re.M | re.S)[0].strip()

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count, news_publish_time


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-警示教育': 'http://www.bohechashe.org.cn/category/2082?page=',
        '首页-反邪动态': 'http://www.bohechashe.org.cn/category/2083?page=',
        '首页-反邪在线': 'http://www.bohechashe.org.cn/category/2084?page=',
        '首页-理论研究': 'http://www.bohechashe.org.cn/category/2086?page=',
        '首页-依法治理': 'http://www.bohechashe.org.cn/category/2089?page=',
        # '首页-薄荷专题': 'http://www.bohechashe.org.cn/category/2085?page=',    # 不采集
        '首页-薄荷评论': 'http://www.bohechashe.org.cn/category/2090?page=',
        '首页-关爱之家': 'http://bohechashe.org.cn/category/adada7d937fa41abb7a6a1c1348a53eb?page=',
        '首页-邪教曝光': 'http://www.bohechashe.org.cn/category/2093?page=',
        '首页-答疑解惑': 'http://www.bohechashe.org.cn/category/2094?page=',
        '首页-心理疏导': 'http://www.bohechashe.org.cn/category/2095?page=',
        '首页-正教克邪': 'http://www.bohechashe.org.cn/category/2621?page=',
        '首页-破除迷信': 'http://www.bohechashe.org.cn/category/2439?page=',
        '首页-视频': 'http://www.bohechashe.org.cn/category/2442?page=',
        '首页-反对伪科学': 'http://www.bohechashe.org.cn/category/2440?page=',
    }

    for key, value in urls.items():
        news_classify = key

        for page in range(1, 50):
            url = value + str(page)
            html_text, status_code = get_html(url, page)
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)
            logging.warning('CLASSIFY：{} PAGE: {} CONTENT DEADLINE'.format(news_classify, page))


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'E-43W 中国反邪教协会'
    start_run(args.projectname, args.sinceyear, account_name)
