# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-07-12

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content_new3
    return collection


def get_pageId(url):
    html_text = requests.get(url).text
    pageId = re.findall(r"'pageId':'(.*?)'", html_text)[0]
    return pageId


def get_html(url, page, pageId):

    cookies = {
        'slb-route': '40b2e3b251dfd62e52f067ffb4208cc9',
    }

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/114.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
        'sec-ch-ua': '"Not.A/Brand";v="8", "Chromium";v="114", "Google Chrome";v="114"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    params = (
        ('webId', '5cdd0202df354a68a8dec754afa8d7fd'),
        ('pageId', pageId),
        ('parseType', 'bulidstatic'),
        ('pageType', 'column'),
        ('tagId', '\u4FE1\u606F'),
        ('tplSetId', 'e2f98e34aba4409bbd9476401144bf33'),
        ('paramJson', '{"pageNo":%s,"pageSize":"15"}'% page),
    )

    response = requests.get('https://cccst.org.cn/api-gateway/jpaas-publish-server/front/page/build/unit',
                            headers=headers, params=params, cookies=cookies)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code
    # html = response.encode('ascii').decode('unicode_escape')


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    content = json.loads(html_text)
    try:
        deadline = ree_data(content, news_classify, science_system,
                            mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
             account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    content_data = content['data']['html']
    selector = etree.HTML(content_data)
    nodes = selector.xpath('//*[@class="page-content"]//li')

    for i in range(len(nodes)):
        try:
            news_title = nodes[i].xpath('.//a/@title')[0].strip()
        except:
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = nodes[i].xpath('.//p[2]/text()')[0].strip()
            else:
                news_abstract = nodes[i].xpath('.//p/text()')[0].strip()
        except:
            news_abstract = news_title

        try:
            if 'special' not in news_classify:
                news_publish_time = nodes[i].xpath('.//span/text()')[0].split(' ')[0]
            else:
                news_publish_y = nodes[i].xpath('.//a/div[1]/h5/text()')[0].strip().replace('/', '-')
                news_publish_d= nodes[i].xpath('.//a/div[1]/h2/text()')[0].strip()
                news_publish_time = news_publish_y + '-' + news_publish_d
        except:
            news_publish_time = '2023-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = nodes[i].xpath('.//a/@href')[0].strip()
            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://cccst.org.cn/' + url_part2

            if 'https://mp.weixin.qq.co' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if '要闻动态' in news_classify:
                news_title = part_nodes[i].xpath('.//a/p[1]/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./a/@title')[0].strip()
        except Exception as e:
            logging.warning('Title Was Error: {}'.format(e))
            news_title = ''
        try:
            if '要闻动态' in news_classify:
                news_abstract = part_nodes[i].xpath('.//a/p[2]/text()')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()

        except:
            news_abstract = ''

        try:
            if '要闻动态' in news_classify:
                news_publish_d = part_nodes[i].xpath('.//h3/text()')[0]
                news_publish_y = part_nodes[i].xpath('.//h2/text()')[0]
                news_publish_time = news_publish_y + '-' + news_publish_d
            else:
                news_publish_time = part_nodes[i].xpath('./time/text()')[0].strip()
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if '要闻动态' not in news_classify:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0]
                else:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0]

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.imast.org.cn' + url_part2

            if 'https://mp.weixin.qq.com' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath(
            '//*[@class="content"]//p/span//text() |'
            '//*[@class="content"]/p//text() '
        )
        news_content = ''.join([x.strip() for x in news_content])

    except Exception as e:
        logging.warning("Article Content Xpath Was Error: {}".format(e))
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者: <b>(.*?)</', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="content"]//p//img/@src |'
                                        '//*[@class="content"]//span//img/@src | '
                                        '//*[@class="content"]//div//img/@src |'
                                        '//*[@class="content"]//input/@src |'
                                        '//*[@class="content"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="content"]//span//video/@src |'
                                         ' //*[@class="content"]//p//video/@src |'
                                         ' //*[@class="content"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r"来源：(.*?)</", content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_url = 'https://www.ciste.org.cn/api.php?op=count&id={}&modelid=15'.format(id)
        read_content = requests.get(read_url).text
        read_count = re.findall(r".html\('(.*?)'", read_content, re.M | re.S)[-1]
    except:
        # logging.warning('Read Count Was Error :{}'.format(e))
        read_count = ''

    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻动态': 'https://cccst.org.cn/zzcx/qnbrh/xwdt/index.html',
        '首页-通知公告': 'https://cccst.org.cn/zxyw/tzgg/index.html',
        '首页-中心要闻': 'https://cccst.org.cn/zxyw/index.html',
        '首页-媒体关注': 'https://cccst.org.cn/mtgz/index.html',
        '首页-党建工作': 'https://cccst.org.cn/djgz/index.html',
        '首页-内容建设': 'https://cccst.org.cn/nrjs/index.html',
        '首页-WFEO-CHINA': 'https://cccst.org.cn/WFEOCHINA/index.html',
    }
    for key, value in urls.items():
        news_classify = key
        pageId = get_pageId(value)
        for page in range(1, 50):
            html_text, status_code = get_html(value, page, pageId)

            if status_code != 200:
                break

            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break

            time.sleep(0.11)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q2')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-04-01')
    args = parser.parse_args()
    account_name = 'DU-10 中国科协科学技术传播中心'
    start_run(args.projectname, args.sinceyear, account_name)
