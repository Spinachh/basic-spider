# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-07-12

import random
import re
import io
import sys
import time
import json
import argparse
import urllib3
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content_new
    return collection


def get_pageId(url):
    html_text = requests.get(url).text
    pageId = re.findall(r"'pageId':'(.*?)'", html_text)[0]
    return pageId


def get_html(url, page, pageId):

    cookies = {
        'slb-route': '9d609105f9643d53d3650b76d4117012',
        'num': '1',
        '_ud_': '4a1f5a63ee1045ba963f3127f937168a',
    }

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/114.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
        'sec-ch-ua': '"Not.A/Brand";v="8", "Chromium";v="114", "Google Chrome";v="114"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    params = (
        ('webId', '5e8b4cb3e1f6470d9268db479c73ab78'),
        ('pageId', pageId),
        ('parseType', 'bulidstatic'),
        ('pageType', 'column'),
        ('tagId', '\u901A\u7528\u5217\u8868'),
        ('tplSetId', 'baaee4c6051c41ee942ca96cecb96f8b'),
        ('paramJson', '{"pageNo": %s,"pageSize":10}'% page),
    )

    response = requests.get('https://www.ciste.org.cn/api-gateway/jpaas-publish-server/front/page/build/unit',
                            headers=headers, params=params, cookies=cookies)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):

    content = json.loads(html_text)
    try:
        deadline = ree_data(content, news_classify, science_system,
                            mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    content_data = content['data']['html']
    # print(content_data)
    # breakpoint()
    selector = etree.HTML(content_data)
    nodes = selector.xpath('//*[@class="page-content"]//li')

    for i in range(len(nodes)):
        try:
            news_title = nodes[i].xpath('.//a/@title')[0].strip()
        except:
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = nodes[i].xpath('.//p[2]/text()')[0].strip()
            else:
                news_abstract = nodes[i].xpath('.//p/text()')[0].strip()
        except:
            news_abstract = news_title

        try:
            if 'special' not in news_classify:
                news_publish_y = nodes[i].xpath('.//span/text()')[1].strip().replace('.', '-')
                news_publish_d= nodes[i].xpath('.//span/text()')[0].strip()
                news_publish_time = news_publish_y + '-' + news_publish_d
            else:
                news_publish_y = nodes[i].xpath('.//a/div[1]/h5/text()')[0].strip().replace('/', '-')
                news_publish_d= nodes[i].xpath('.//a/div[1]/h2/text()')[0].strip()
                news_publish_time = news_publish_y + '-' + news_publish_d
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                url_part2 = nodes[i].xpath('.//a/@href')[0].strip()
            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'https' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.ciste.org.cn' + url_part2

            if 'mp.weixin.qq.com' in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_page_url)

            elif 'news.cn' in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_news_page_content(news_page_url)

            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath(
            '//*[@class="showtxt"]//p/span//text() |'
            '//*[@class="showtxt"]/p//text() '
        )
        news_content = ''.join([x.strip() for x in news_content])

    except Exception as e:
        logging.warning("Article Content Xpath Was Error: {}".format(e))
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者: <b>(.*?)</', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="showtxt"]//p//img/@src |'
                                        '//*[@class="showtxt"]//span//img/@src | '
                                        '//*[@class="showtxt"]//div//img/@src |'
                                        '//*[@class="showtxt"]//input/@src |'
                                        '//*[@class="showtxt"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="showtxt"]//span//video/@src |'
                                         ' //*[@class="showtxt"]//p//video/@src |'
                                         ' //*[@class="showtxt"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r"来源：(.*?)</", content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_url = 'https://www.ciste.org.cn/api.php?op=count&id={}&modelid=15'.format(id)
        read_content = requests.get(read_url).text
        read_count = re.findall(r".html\('(.*?)'", read_content, re.M | re.S)[-1]
    except:
        # logging.warning('Read Count Was Error :{}'.format(e))
        read_count = ''

    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_news_page_content(news_page_url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
    }

    try:
        html_response = requests.get(
            news_page_url, headers=headers,
            verify=False)
        html_response.encoding = 'utf-8'
        content_text = html_response.text
    except Exception as e:
        logging.warning('Article Content Was Error :{}'.format(e))
        content_text = ''
    # print('get news content text')
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = result.get('content')
    except Exception as e:
        news_content = selector_page.xpath(
            '//*[@id="detail"]//p/span//text() |'
            '//*[@id="detail"]//div//text() |'
            '//*[@id="detail"]/p//text() '
            '//*[@id="detail"]/p/font/text() '
        )
        news_content = ''.join([x.strip() for x in news_content])

    try:
        news_author = re.findall(r'作者: <b>(.*?)</', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@id="detail"]//p//img/@src |'
                                        '//*[@id="detail"]//span//img/@src | '
                                        '//*[@id="detail"]//div//img/@src |'
                                        '//*[@id="detail"]//input/@src |'
                                        '//*[@id="detail"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        # news_video = selector_page.xpath('//*[@id="detail"]//video/')
        news_video = re.findall(r'video_src="(.*?)"', content_text, re.M | re.S)[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video = ''
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r"来源：(.*?)</", content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r"阅读量：(.*?)</", content_text, re.M | re.S)[0].strip()
    except:
        # logging.warning('Read Count Was Error :{}'.format(e))
        read_count = ''

    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-工作动态-时政要闻': 'https://www.ciste.org.cn/gzdt/szyw/index.html',
        '首页-工作动态-中心动态': 'https://www.ciste.org.cn/gzdt/zxdt/index.html',
        '首页-工作动态-通知公告-special': 'https://www.ciste.org.cn/gzdt/tzgg/index.html',
        # '首页-工作动态-每日金句': 'https://www.ciste.org.cn/gzdt/mzjj/index.html',
        '首页-工作动态-党建工作-党总支': 'https://www.ciste.org.cn/gzdt/djgz/dzz/index.html',
        '首页-工作动态-党建工作-第一党支部': 'https://www.ciste.org.cn/gzdt/djgz/dydzb/index.html',
        '首页-工作动态-党建工作-第二党支部': 'https://www.ciste.org.cn/gzdt/djgz/dedzb/index.html',
        '首页-工作动态-党建工作-第三党支部': 'https://www.ciste.org.cn/gzdt/djgz/dsdzb/index.html',
        '首页-工作动态-党建工作-第四党支部': 'https://www.ciste.org.cn/gzdt/djgz/sdzb/index.html',
        '首页-工作动态-党建工作-第五党支部': 'https://www.ciste.org.cn/gzdt/djgz/dwdzb/index.html',

        '首页-国际科技组织-国际组织事务': 'https://www.ciste.org.cn/gjkjzz/gjzzsw/index.html',
        '首页-国际科技组织-中国科协联合国咨商': 'https://www.ciste.org.cn/gjkjzz/zgkxlhgzs/index.html',
        '首页-国际科技组织-一带一路科技组织合作': 'https://www.ciste.org.cn/gjkjzz/ydylkjzz/index.html',
        '首页-国际科技组织-国际组织招聘信息': 'https://www.ciste.org.cn/gjkjzz/gjzzzpxx/index.html',
        '首页-国际科技组织-专家履职服务': 'https://www.ciste.org.cn/gjkjzz/zjlzfw/index.html',
        '首页-国际科技组织-ISC-CHINA': 'https://www.ciste.org.cn/gjkjzz/ISCCHINA/index.html',

        '首页-科技人文交流-海智计划': 'https://www.ciste.org.cn/kjrwjl/hzjh/index.html',
        '首页-科技人文交流-科创中国行': 'https://www.ciste.org.cn/kjrwjl/kczgx/index.html',
        '首页-科技人文交流-双边民间科技合作': 'https://www.ciste.org.cn/kjrwjl/sbmjkjhz/index.html',
        '首页-科技人文交流-港澳台交流': 'https://www.ciste.org.cn/kjrwjl/sbmjkjhz/index.html',

        '首页-国际技术贸易-科创中国技术路演': 'https://www.ciste.org.cn/gjjsmy/kczgjsly/index.html',
        '首页-国际技术贸易-国际技术转移项目': 'https://www.ciste.org.cn/gjjsmy/gjjszyxm/index.html',
        '首页-国际技术贸易-国际技术交易联盟': 'https://www.ciste.org.cn/gjjsmy/gjjsjylm/index.html',
        '首页-国际技术贸易-国际研发社区': 'https://www.ciste.org.cn/gjjsmy/gjyfsq/index.html',
        '首页-国际技术贸易-对外交流': 'https://www.ciste.org.cn/gjjsmy/dwjl/index.html',
        '首页-国际科技外交-动态简讯': 'https://www.ciste.org.cn/gjkjwj/dtjx/index.html',
        '首页-国际科技外交-政策要点': 'https://www.ciste.org.cn/gjkjwj/zcyd/index.html',
        '首页-国际科技外交-智库观点': 'https://www.ciste.org.cn/gjkjwj/zkgd/index.html',

    }

    for key, value in urls.items():
        news_classify = key
        pageId = get_pageId(value)
        for page in range(1, 50):
            html_text, status_code = get_html(value, page, pageId)

            if status_code != 200:
                break

            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break

            time.sleep(0.11)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q2')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default',
                        default='2023-04-01')
    args = parser.parse_args()
    account_name = 'DU-09 中国国际科技交流中心'
    start_run(args.projectname, args.sinceyear, account_name)
