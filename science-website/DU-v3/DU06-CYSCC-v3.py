# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-07-12

import random
import re
import io
import sys
import time
import json
import argparse
import urllib3
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account


urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content_new3
    return collection


def get_pageId(url):
    html_text = requests.get(url).text
    pageId = re.findall(r"'pageId':'(.*?)'", html_text)[0]
    return pageId


def get_html(url, page, pageId):

    cookies = {
        'slb-route': '7c2e124cf7a0b76e6eb0d59985eaf2bb',
    }

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/114.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
        'sec-ch-ua': '"Not.A/Brand";v="8", "Chromium";v="114", "Google Chrome";v="114"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    params = (
        ('webId', 'e9e32aa8e0a34c64a738594e096f7ff7'),
        ('pageId', pageId),
        ('parseType', 'bulidstatic'),
        ('pageType', 'column'),
        ('tagId', '\u5F53\u524D\u680F\u76EElist'),
        ('tplSetId', 'aa0d0a6de7e843838075ff6f56ac729a'),
        ('paramJson', '{"pageNo":%s,"pageSize":"10"}' % page),
    )

    response = requests.get('https://www.scei.org.cn/api-gateway/jpaas-publish-server/front/page/build/ajax-info-list',
                            headers=headers, params=params, cookies=cookies)

    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    content = json.loads(html_text)
    try:
        deadline = ree_data(content, news_classify, science_system,
                            mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    content_data = content['data']
    # print(content_data)
    # breakpoint()

    for item in content_data:
        try:
            news_title = item.get('title')
        except:
            news_title = ''

        try:
            news_abstract = item.get('contentDescribe')
        except:
            news_abstract = news_title

        try:
            news_publish_stamp = str(item.get('publishTime'))[:10]
            news_publish_time = time.strftime("%Y-%m-%d", time.localtime(int(news_publish_stamp)))
        except:
            news_publish_time = '2023-01-01'
            news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        # news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = item.get('url')

            if 'http:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.scei.org.cn' + url_part2

            if 'mp.weixin.qq.com' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,'
                  '*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
    }

    try:
        html_response = requests.get(news_page_url,
                                     headers=headers,
                                     verify=False)
        html_response.encoding = 'utf-8'
        content_text = html_response.text
    except Exception as e:
        logging.warning('Article Content Was Error :{}'.format(e))
        content_text = ''

    try:
        redirect_url = re.findall(r'var link = "(.*?)"', content_text, re.M | re.S)[0].replace('\\', '')
    except:
        redirect_url = ''

    if 'news.cn' in redirect_url:
        news_content = get_news_page_content(redirect_url)
        return news_content
    else:
        selector_page = etree.HTML(content_text)
        extractor = GeneralNewsExtractor()

        try:
            result = extractor.extract(content_text)
        except:
            result = ''

        try:
            news_content = selector_page.xpath(
                '//*[@id="rongqi"]//p/span//text() |'
                '//*[@id="rongqi"]//div//text() |'
                '//*[@id="rongqi"]/p//text() '
            )
            news_content = ''.join([x.strip() for x in news_content])

        except Exception as e:
            logging.warning("Article Content Xpath Was Error: {}".format(e))
            news_content = result.get('content')

        try:
            news_author = re.findall(r'作者: <b>(.*?)</', content_text, re.M | re.S)[0].strip()
        except Exception as e:
            news_author = ''

        try:
            news_imgs = selector_page.xpath('//*[@id="rongqi"]//p//img/@src |'
                                            '//*[@id="rongqi"]//span//img/@src | '
                                            '//*[@id="rongqi"]//div//img/@src |'
                                            '//*[@id="rongqi"]//input/@src |'
                                            '//*[@id="rongqi"]//img/@src')[0]

            if news_imgs:
                news_content_type = 'text-img'
            else:
                news_content_type = 'text'

            if len(news_imgs) > 800:
                news_imgs = ''
        except:
            news_imgs = ''
            news_content_type = 'text'

        try:
            news_video = selector_page.xpath('//*[@id="rongqi"]//span//video/@src |'
                                             ' //*[@id="rongqi"]//p//video/@src |'
                                             ' //*[@id="rongqi"]//div//video/@src ')[0]
            if news_video:
                news_video_flag = 'video'
            else:
                news_video_flag = ''
        except:
            news_video_flag = ''

        news_content_type = news_content_type + '-' + news_video_flag

        try:
            source = re.findall(r"来源：(.*?)</", content_text, re.M | re.S)[0].strip()
        except:
            source = ''

        try:
            read_count = re.findall(r"阅读量：(.*?)</", content_text, re.M | re.S)[0].strip()
        except:
            # logging.warning('Read Count Was Error :{}'.format(e))
            read_count = ''

        try:
            click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
        except:
            click_count = ''

        # dr = re.compile(r'<[^>]+>', re.S)
        # content_text = dr.sub('', html_response.text).strip()
        # print(result)
        # print(content_text)

        return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_news_page_content(news_page_url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
    }

    try:
        html_response = requests.get(
            news_page_url, headers=headers,
            verify=False)
        html_response.encoding = 'utf-8'
        content_text = html_response.text
    except Exception as e:
        logging.warning('Article Content Was Error :{}'.format(e))
        content_text = ''
    # print('get news content text')
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = result.get('content')
    except Exception as e:
        news_content = selector_page.xpath(
            '//*[@id="detail"]//p/span//text() |'
            '//*[@id="detail"]//div//text() |'
            '//*[@id="detail"]/p//text() '
            '//*[@id="detail"]/p/font/text() '
        )
        news_content = ''.join([x.strip() for x in news_content])

    try:
        news_author = re.findall(r'作者: <b>(.*?)</', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@id="detail"]//p//img/@src |'
                                        '//*[@id="detail"]//span//img/@src | '
                                        '//*[@id="detail"]//div//img/@src |'
                                        '//*[@id="detail"]//input/@src |'
                                        '//*[@id="detail"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        # news_video = selector_page.xpath('//*[@id="detail"]//video/')
        news_video = re.findall(r'video_src="(.*?)"', content_text, re.M | re.S)[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video = ''
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r"来源：(.*?)</", content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r"阅读量：(.*?)</", content_text, re.M | re.S)[0].strip()
    except:
        # logging.warning('Read Count Was Error :{}'.format(e))
        read_count = ''

    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-工作动态-中心动态': 'https://www.scei.org.cn/gzdt/zxdt/index.html',
        '首页-通知公告': 'https://www.scei.org.cn/tztg/index.html',
        '首页-党建工作-党建动态': 'https://www.scei.org.cn/djgz/djdt/index.html',
        '首页-党建工作-学习园地': 'https://www.scei.org.cn/djgz/xxyd/index.html',
        '首页-党建工作-党史学习教育': 'https://www.scei.org.cn/djgz/dsxxjy/index.html',
        '首页-科协要闻-科协要闻': 'https://www.scei.org.cn/kxyw/index.html',
    }
    for key, value in urls.items():
        news_classify = key
        pageId = get_pageId(value)
        for page in range(1, 50):
            html_text, status_code = get_html(value, page, pageId)
            if status_code != 200:
                break

            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q2')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-04-01')
    args = parser.parse_args()
    account_name = 'DU-06 中国科协企业创新服务中心'
    start_run(args.projectname, args.sinceyear, account_name)
