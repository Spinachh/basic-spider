# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-07-05

import random
import re
import io
import sys
import time
import json
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_pageId(url):
    html_text = requests.get(url).text
    pageId = re.findall(r"'pageId':'(.*?)'", html_text)[0]
    return pageId


def get_html(url, page, pageId):

    cookies = {
        'slb-route': 'e497ac0402d4f09bfc4a79e4789cb64b',
    }

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.csas.org.cn/xw/xhyw/index.html',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/114.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    params = (
        ('webId', 'f51505ca33864944a80d97b026542f9e'),
        ('pageId', pageId),
        ('parseType', 'bulidstatic'),
        ('pageType', 'column'),
        ('tagId', '\u5217\u8868'),
        ('tplSetId', 'd2543054bdb84eb7890dc978e98fe371'),
        ('paramJson', '{"pageNo": %s ,"pageSize":15}'% page),
    )

    response = requests.get('http://www.csas.org.cn/api-gateway/jpaas-publish-server/front/page/build/unit',
                            headers=headers, params=params, cookies=cookies, verify=False)

    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    content = json.loads(html_text)
    try:
        deadline = ree_data(content, news_classify, science_system,
                            mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    content_data = content['data']['html']
    # print(content_data)
    # breakpoint()
    selector = etree.HTML(content_data)
    nodes = selector.xpath('//*[@class="page-content"]//li')

    for i in range(len(nodes)):
        try:
            news_title = nodes[i].xpath('./a/div[1]/@title')[0].strip()
        except:
            news_title = ''

        news_abstract = news_title
        try:
            news_publish_time = nodes[i].xpath('./a/div[2]/text()')[0].strip()
        except:
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = nodes[i].xpath('./a/@href')[0].strip()
            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.csas.org.cn' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="pd30"]//span/text() | '
                                           '//*[@class="pd30"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者(.*?)</', content_text, re.M | re.S)[0]
        news_author = news_author.split('>')[-3].split('<')[0]
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="pd30"]//span//img/@src |'
                                        ' //*[@class="pd30"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'document.write\("(.*?)"', content_text)[0]
    except:
        click_count = ''
    # print(click_count)
    # breakpoint()
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻-学会要闻': 'http://www.csas.org.cn/xw/xhyw/index.html',
        '首页-新闻-通知公告': 'http://www.csas.org.cn/xw/tzgg/index.html',
        '首页-新闻-分会动态': 'http://www.csas.org.cn/xw/fhdt/index.html',
        '首页-新闻-行业资讯': 'http://www.csas.org.cn/xw/xyzx/index.html',
        '首页-新闻-精彩图集': 'http://www.csas.org.cn/xw/jctj/index.html',
        '首页-新闻-精彩视频': 'http://www.csas.org.cn/xw/jcsp/index.html',
        '首页-党建-党建工作': 'http://www.csas.org.cn/dj/djgz/index.html',
        '首页-党建-科学精神': 'http://www.csas.org.cn/dj/kxjs/index.html',
        '首页-党建-党史学习': 'http://www.csas.org.cn/dj/dsxx/index.html',
        '首页-党建-理论学习': 'http://www.csas.org.cn/dj/llxx/index.html',
        '首页-党建-党的十二大精神': 'http://www.csas.org.cn/dj/ddesdjs/index.html',
        '首页-智库-智库动态': 'http://www.csas.org.cn/zk/zkdt/index.html',
        '首页-智库-智库成果专家观点': 'http://www.csas.org.cn/zk/zkcgzjgd/index.html',
        '首页-智库-专业分会': 'http://www.csas.org.cn/zk/zyfh/index.html',
        '首页-智库-期刊杂志': 'http://www.csas.org.cn/zk/qkzz/index.html',
        '首页-智库-智库成果': 'http://www.csas.org.cn/zk/zkcg/index.html',
        '首页-学术-学术活动': 'http://www.csas.org.cn/xs/xshd/index.html',
        '首页-学术-国内会议': 'http://www.csas.org.cn/xs/gnhy/index.html',
        '首页-学术-会议纪要': 'http://www.csas.org.cn/xs/hyjy/index.html',
        '首页-学术-国际会议': 'http://www.csas.org.cn/xs/gjhy/index.html',
        '首页-学术-杂志期刊': 'http://www.csas.org.cn/xs/zzqk/index.html',
        '首页-科普-科普新闻': 'http://www.csas.org.cn/kp/kpxw/index.html',
        '首页-科普-科普知识': 'http://www.csas.org.cn/kp/kpzs/index.html',
        '首页-科普-科普教育基地': 'http://www.csas.org.cn/kp/kpjyjd/index.html',

    }
    for key, value in urls.items():
        news_classify = key
        pageId = get_pageId(value)
        for page in range(1, 50):
            html_text, status_code = get_html(value, page, pageId)
            # print(html_text)
            # breakpoint()
            if status_code != 200:
                break

            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q2')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-04-01')
    args = parser.parse_args()
    account_name = 'D-07 中国解剖学会'
    start_run(args.projectname, args.sinceyear, account_name)
