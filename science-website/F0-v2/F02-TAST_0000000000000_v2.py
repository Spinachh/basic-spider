# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-04-28

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):
    response = requests.get(url, verify=False)
    status_code = response.status_code
    response.encoding = 'gb2312'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        if 'special' not in news_classify:
            part1_nodes = selector.xpath('//*[@class="cms_block_span"]/table')
        else:
            part1_nodes = selector.xpath(
                '//*[@style=" padding-left:40px;; line-height:38px; font-size:16px; "]/table/tr')

        deadline = xpath_data(part1_nodes, news_classify, science_system, mongo,
                              account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    # print(part_nodes)
    # breakpoint()
    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('.//a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('.//a/text()')[0].strip()

        except:
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = part_nodes[i].xpath('.//a/text()')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('.//a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_time = part_nodes[i].xpath('.//td[3]/text()')[0].replace('/', '-')
            else:
                article_url = part_nodes[i].xpath('.//a/@href')[0].split('system/')[-1].split('/')[:3]
                # http://www.tast.org.cn/xhdj/system/2022/08/17/030016789.shtml
                news_publish_time = '-'.join(article_url)
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2023-01-01'

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0]
                else:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0]

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.tast.org.cn/' + url_part2

            if 'https://mp.weixin.qq.com' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}.'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'gb2312'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="weiruan16 hanggao35"]//span//text() | '
                                           '//*[@class="weiruan16 hanggao35"]//p//text()'
                                           )
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'编辑：(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="weiruan16 hanggao35"]//span//img/@src |'
                                        '//*[@class="weiruan16 hanggao35"]//p//img/@src | '
                                        '//*[@class="weiruan16 hanggao35"]//div//img/@src | '
                                        '//*[@class="weiruan16 hanggao35"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="xq_con"]//span//video/@src |'
                                         ' //*[@class="xq_con"]//p//video/@src |'
                                         ' //*[@class="xq_con"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源：(.*?) ', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击数：(.*?) ', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    # print(news_page_url)
    # breakpoint()
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学会学术-学术交流': 'http://www.tast.org.cn/xhxs/xsjl/index.shtml-0004003',
        '首页-学会学术-学会治理': 'http://www.tast.org.cn/xhxs/xsjl/index.shtml-0004002',
        '首页-学会学术-办事指南': 'http://www.tast.org.cn/xhxs/gzzn/index.shtml-',
        '首页-科学普及-科普要讯': 'http://www.tast.org.cn/kxpj/gzdt/index.shtml-0010001',
        '首页-科学普及-全民科学素质行动': 'http://www.tast.org.cn/kxpj/gzdt/index.shtml-0010013',
        '首页-科学普及-各区科普': 'http://www.tast.org.cn/kxpj/jckp/index.shtml-0010006',
        '首页-科学普及-科普大使': 'http://www.tast.org.cn/kxpj/kpds/index.shtml-0010015',
        '首页-科学普及-科普基地': 'http://www.tast.org.cn/kxpj/kpjd/index.shtml-',
        '首页-科学普及-科普政策': 'http://www.tast.org.cn/kxpj/zcwj/index.shtml-0010004',
        '首页-科学普及-科技周': 'http://www.tast.org.cn/kxpj/kjz/index.shtml-',
        '首页-科学普及-科普日': 'http://www.tast.org.cn/kxpj/kpr/index.shtml-',
        '首页-决策咨询-智库建设': 'http://www.tast.org.cn/jczx/zkjs/index.shtml-0009003',
        '首页-决策咨询-建言献策': 'http://www.tast.org.cn/jczx/jyxc/index.shtml-0009001',
        '首页-人才服务-杰出人才': 'http://www.tast.org.cn/rcfw/jcrc/index.shtml-',
        '首页-人才服务-青年托举工程': 'http://www.tast.org.cn/rcfw/qntjgc/index.shtml-',
        '首页-人才服务-青年科技奖': 'http://www.tast.org.cn/rcfw/qnkjj/index.shtml-',
        '首页-人才服务-优秀工作者标兵': 'http://www.tast.org.cn/rcfw/bb/index.shtml-',
        '首页-人才服务-最美科技工作者': 'http://www.tast.org.cn/rcfw/zmkj/index.shtml-',
        '首页-人才服务-全国创新争先奖': 'http://www.tast.org.cn/rcfw/qgcxzx/index.shtml-',
        '首页-人才服务-海外人才': 'http://www.tast.org.cn/rcfw/hwrc/index.shtml-',
        '首页-人才服务-职称评审': 'http://www.tast.org.cn/rcfw/zcps/index.shtml-',
        '首页-创新服务-协同创新中心': 'http://www.tast.org.cn/cxqd/yszjgzz/index.shtml-0007003',
        '首页-创新服务-科创中国天津行动': 'http://www.tast.org.cn/cxqd/kczgtjxd/index.shtml-0007006',
        '首页-机关党建': 'http://www.tast.org.cn/jgdj/index.shtml-0006000',
        '首页-预决算公开': 'http://www.tast.org.cn/yjsgk/index.shtml-0023000',
        '首页-总书记重要论述': 'http://www.tast.org.cn/xjpyl/index.shtml-0028000',
        '首页-科协要闻': 'http://www.tast.org.cn/kxyw/index.shtml-0022000',
        '首页-通知公告': 'http://www.tast.org.cn/tzgg/index.shtml-0021000',
        '首页-工作动态': 'http://www.tast.org.cn/gzdt/index.shtml-0020000',
        '首页-媒体聚焦': 'http://www.tast.org.cn/mtjj/index.shtml-0019000',
        '首页-市级学会': 'http://www.tast.org.cn/sjxh/index.shtml-0018000',
        '首页-区级学会': 'http://www.tast.org.cn/qjkx/index.shtml-0003000',
        '首页-基层科协': 'http://www.tast.org.cn/jckx/index.shtml-0017000',
        '首页-学术学会-学会治理': 'http://www.tast.org.cn/xhxs/xhgg/index.shtml-0004002',
        '首页-学术学会-学会学术': 'http://www.tast.org.cn/xhxs/xsjl/-0004003',
        '首页-学会党建-工作动态-special': 'http://www.tast.org.cn/xhdj/gzdt/index.shtml-0013003',
        '首页-学会党建-工作指导-special': 'http://www.tast.org.cn/xhdj/gzzd/index.shtml-',
        '首页-学会党建-党内规章-special': 'http://www.tast.org.cn/xhdj/dngz/index.shtml-',
        '首页-学会党建-党员先锋-special': 'http://www.tast.org.cn/xhdj/dyxf/index.shtml-',
        '首页-学会党建-党建阵地-special': 'http://www.tast.org.cn/xhdj/djzd/index.shtml-',
    }

    for key, value in urls.items():
        # "http://www.tast.org.cn/xhxs/xsjl/index.shtml-0004003"
        block = re.findall(r'.cn/(.*?)/', value)[0]
        url_part2 = 'http://www.tast.org.cn/{}/system/count//'.format(block)
        news_classify = key
        url_part = value.split('-')
        # http://www.tast.org.cn/xhxs/system/count/0004003/000000000000/count_page_list_0004003000000000000.js
        count_url_js = 'http://www.tast.org.cn/{}/system/count/{}' \
                       '/000000000000/count_page_list_{}000000000000.js'.format(block, url_part[1], url_part[1])
        count_html_text = get_html(count_url_js)
        total_page = re.findall(r'var maxpage = (.*?);', count_html_text)[0]
        # print(total_page)
        # breakpoint()
        for page in range(1, int(total_page) + 1):
            if page == 1:
                url = url_part[0]
            else:
                if url_part[1] == '':
                    break
                else:
                    # http://www.tast.org.cn/xhxs/system/count/
                    # /0004003
                    # /000000000000/000/000
                    # /c
                    # 0004003
                    # 000000000000_00000000
                    # 5.shtml
                    url = url_part2 + url_part[1] + '/000000000000/000/000/' \
                          + 'c' + url_part[1] + '000000000000_0000000' + str(int(total_page) - page + 2) + '.shtml'
            # print(url)
            html_text, status_code = get_html(url)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'F-02 天津市科协'
    start_run(args.projectname, args.sinceyear, account_name)