# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-04-28


import random
import re
import io
import sys
import time
import json
import urllib3
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):
    response = requests.get(url, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        if 'special' not in news_classify:
            part1_nodes = selector.xpath('//*[@class="news_list layui-clear"]//li')
        else:
            part1_nodes = selector.xpath('//*[@class="news"]//li')

        deadline = xpath_data(part1_nodes, news_classify, science_system, mongo,
                              account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('.//a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./a/div/div[1]/text()')[0].strip()

        except:
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = part_nodes[i].xpath('.//a/text()')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('.//p/text()')[0].strip()

        except:
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_d = part_nodes[i].xpath('.//h3/text()')[0]
                news_publish_y = part_nodes[i].xpath('.//h2/text()')[0]
                news_publish_time = news_publish_y + '-' + news_publish_d
            else:
                news_publish_time = part_nodes[i].xpath('./a/div/div[2]/text()')[0].strip()
                news_publish_re = re.findall(r'\d+', news_publish_time)
                news_publish_time = '-'.join(news_publish_re)
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0]
                else:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0]

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.hbast.org.cn/' + url_part2

            if 'https://mp.weixin.qq.com' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="detail_body"]//span//text() | '
                                           '//*[@class="detail_body"]//p//text() | '
                                           '//*[@class="detail_body"]//div//text() | '
                                           )
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者：(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="detail_body"]//span//img/@src |'
                                        '//*[@class="detail_body"]//p//img/@src | '
                                        '//*[@class="detail_body"]//div//img/@src |'
                                        '//*[@class="detail_body"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="detail_body"]//span//video/@src |'
                                         ' //*[@class="detail_body"]//p//video/@src |'
                                         ' //*[@class="detail_body"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源: (.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_id = re.findall(r'(\d+)', news_page_url)[0]
        click_url = 'https://www.hbast.org.cn/japi/content-status?id={}'.format(click_id)
        click_content = requests.get(click_url).json()
        click_count = click_content.get('CLICKCOUNT')
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-科协信息-科协要闻': 'https://www.hbast.org.cn/list.thtml?cid=10940&pn=1',
        '首页-科协信息-通知公告': 'https://www.hbast.org.cn/list.thtml?cid=10941&pn=1',
        '首页-科协信息-综合信息': 'https://www.hbast.org.cn/list.thtml?cid=10942&pn=1',
        '首页-科技创新-学会动态': 'https://www.hbast.org.cn/list.thtml?cid=10943&pn=1',
        '首页-科技创新-创新发展': 'https://www.hbast.org.cn/list.thtml?cid=10944&pn=1',
        '首页-科技创新-人才服务': 'https://www.hbast.org.cn/list.thtml?cid=10967&pn=1',
        '首页-科学普及-科普动态': 'https://www.hbast.org.cn/list.thtml?cid=10946&pn=1',
        '首页-科学普及-科普视频': 'https://www.hbast.org.cn/list.thtml?cid=10947&pn=1',
        '首页-智库建设-智库动态': 'https://www.hbast.org.cn/list.thtml?cid=10948&pn=1',
        '首页-智库建设-智库成果': 'https://www.hbast.org.cn/list.thtml?cid=10949&pn=1',
        '首页-智库建设-智库专家': 'https://www.hbast.org.cn/list.thtml?cid=10950&pn=1',
        '首页-智库建设-专家建议': 'https://www.hbast.org.cn/list.thtml?cid=10951&pn=1',
        '首页-智库建设-政策文件': 'https://www.hbast.org.cn/list.thtml?cid=10969&pn=1',
        '首页-党的建设-党务知识': 'https://www.hbast.org.cn/dangjian-list.thtml?cid=10953&pn=1',
        '首页-党的建设-政治引领': 'https://www.hbast.org.cn/dangjian-list.thtml?cid=10954&pn=1',
        '首页-党的建设-党建动态': 'https://www.hbast.org.cn/dangjian-list.thtml?cid=10955&pn=1',
        '首页-专题专栏-二十大专题': 'https://www.hbast.org.cn/dangjian-list.thtml?cid=10970&pn=1',
        '首页-专题专栏-全国科技工作者日': 'https://www.hbast.org.cn/list.thtml?cid=10956&pn=1',
        '首页-专题专栏-科普日专栏': 'https://www.hbast.org.cn/list.thtml?cid=10957&pn=1',
        '首页-专题专栏-预决算公开栏': 'https://www.hbast.org.cn/list.thtml?cid=10958&pn=1',
        '首页-专题专栏-其他专题': 'https://www.hbast.org.cn/list.thtml?cid=10960&pn=1',

    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 300):
            url = value.replace('pn=1', 'pn=' + str(page))
            html_text, status_code = get_html(url)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'F-03 河北省科协'
    start_run(args.projectname, args.sinceyear, account_name)
