# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-04-20
import random
import re
import io
import sys
import time
import json
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash():

    cookies = {
        'ASP.NET_SessionId': 'ec1bpnn1rmufiaelrsqms2rh',
        'td_cookie': '2008652607',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://csol.qdio.ac.cn/XueHuidt.aspx?funId=185',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    params = (
        ('page', '2'),
        ('funId', '185'),
    )

    response = requests.get('http://csol.qdio.ac.cn/XueHuidt.aspx', headers=headers, params=params, cookies=cookies,
                            verify=False)
    html_text = response.text
    # print(html_text)
    # breakpoint()
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, page):

    cookies = {
        'td_cookie': get_supflash(),
        'insert_cookie': '49613759',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://csmpg.gyig.cas.cn/xhyw/index_{}.html'.format(page),
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers,
                            cookies=cookies, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time ):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0]
        except:
            news_title = 'not news_title'

        news_abstract = news_title
        try:
            news_publish_time = part_nodes[i].xpath('.//span/text()')[0]
        except:
            news_publish_time = '2022-01-01'
        # try:
        #     news_imgs = 'http://www.gsc.org.cn/' + part_nodes[i].xpath('./a/img/@src')
        # except:
        #     news_imgs = ''

        # print('http://csmpg.gyig.cas.cn/xhyw' + part_nodes[i].xpath('./a/@href')[0].strip().replace('./', '/'))
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            # if 'html' in url_part2:
            news_page_url = 'http://csol.qdio.ac.cn/' + url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title[:20], news_publish_time))
        else:
            deadline = True
            return deadline


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, supFlash, start_time):

    selector = etree.HTML(html_text)
    # part1
    try:
        part1_nodes = selector.xpath('//*[@class="rt_list a_333"]/ul/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        print('Classify： {} Part1 has not content: {}'.format(news_classify, e))

    # part2
    # try:
    #     part2_nodes = selector.xpath('//*[@width="50%"]')
    #     xpath_data(part2_nodes, page, supFlash, news_classify, science_system, mongo, account_name, project_time)
    # except:
    #     print('Classify： {} Part2 has not content: {}'.format(news_classify, e))


def get_page_content(news_page_url):

    cookies = {
        'ASP.NET_SessionId': 'ec1bpnn1rmufiaelrsqms2rh',
        'td_cookie': get_supflash(),
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    html_response = requests.get(news_page_url, headers=headers, cookies=cookies,
                            verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="nr"]//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'>来源：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''
    try:
        click_count = re.findall(r'>访问数量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '学会动态-通知公告': 'http://csol.qdio.ac.cn/XueHuidt.aspx?page=1&funId=185',
        '学会动态-综合新闻': 'http://csol.qdio.ac.cn/XueHuidt.aspx?page=1&funId=518',
        '学会动态-最新成果': 'http://csol.qdio.ac.cn/XueHuidt.aspx?page=1&funId=496',
        '学会动态-工作动态': 'http://csol.qdio.ac.cn/XueHuidt.aspx?page=1&funId=495',
        '学术交流-学术动态': 'http://csol.qdio.ac.cn/XueShujl.aspx?page=1&funId=491',
        '学术交流-学术活动': 'http://csol.qdio.ac.cn/XueShujl.aspx?page=1&funId=507',
        '科学普及-科普动态': 'http://csol.qdio.ac.cn/KeXuepj.aspx?page=1&funId=500',
        '科学普及-科普知识': 'http://csol.qdio.ac.cn/KeXuepj.aspx?page=1&funId=508',
        '科学普及-科普网站': 'http://csol.qdio.ac.cn/KeXuepj.aspx?page=1&funId=509',
        '党建工作-综合新闻': 'http://csol.qdio.ac.cn/DangJiangz.aspx?page=1',
    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            url = value.replace('page=1', 'page=' + str(page))
            html_text, status_code = get_html(url, page)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name, science_system,
                                mongo, project_time,start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'A-15 中国海洋湖沼学会'
    start_run(args.projectname, args.sinceyear, account_name)
