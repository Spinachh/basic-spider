# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-04-21
import random
import re
import io
import sys
import time
import json
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    content = re.findall(r'<record><!\[CDATA\[(.*?)]>', html_text, re.M | re.S)
    try:
        deadline = ree_data(content, news_classify, science_system,
                            mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(content)):
        try:
            news_title = re.findall(r'<div class="rfl">(.*?)</div>', content[i])[0]
        except:
            news_title = ''

        news_abstract = news_title

        try:
            news_publish_time = re.findall(r'<div class="rfr">(.*?)</div>', content[i])[0]
        except:
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = re.findall(r'class="li_link" href="(.*?)>', content[i])[0].replace('"', '')
            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.mscfungi.org.cn' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    # html_response = requests.get(news_page_url, verify=False)
    html_response = requests.get(news_page_url)
    # if 'www.cstp.org.cn' not in news_page_url:
    #     html_response.encoding = 'gb2312'
    # else:
    html_response.encoding = 'utf-8'

    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    try:
        news_content = selector_page.xpath('//*[@class="container"]///p/text()')
        # news_content = selector_page.xpath('//*[@class="cont_txt"]/span/text()')
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="container w1200"]//p//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'>来源：(.*?)　时间', content_text, re.M | re.S)[0].strip()
    except:
        source = ''


    try:
        read_count = re.findall(r'阅读量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    
    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学会新闻': 'http://www.mscfungi.org.cn/col/col1872/index.html',
        '首页-研究动态': 'http://www.mscfungi.org.cn/col/col1873/index.html',
        '首页-通知公告': 'http://www.mscfungi.org.cn/col/col1874/index.html',
        '首页-党建工作-党建活动': 'http://www.mscfungi.org.cn/col/col1912/index.html',
        '首页-党建工作-理论学习': 'http://www.mscfungi.org.cn/col/col1913/index.html',
        '首页-党建工作-党史学习': 'http://www.mscfungi.org.cn/col/col1914/index.html',
        '首页-党建工作-科学精神': 'http://www.mscfungi.org.cn/col/col1915/index.html',
        '首页-党建工作-党建专题': 'http://www.mscfungi.org.cn/col/col1877/index.html',
        '首页-学术-学术活动': 'http://www.mscfungi.org.cn/col/col1891/index.html',
        '首页-学术-会议通知': 'http://www.mscfungi.org.cn/col/col1892/index.html',
        '首页-学术-历年年会': 'http://www.mscfungi.org.cn/col/col6150/index.html',
        '首页-学术-精彩回顾': 'http://www.mscfungi.org.cn/col/col6151/index.html',
        '首页-学术-活动预告': 'http://www.mscfungi.org.cn/col/col1893/index.html',
        '首页-学术-品牌活动': 'http://www.mscfungi.org.cn/col/col1894/index.html',
        '首页-学术-活动图片': 'http://www.mscfungi.org.cn/col/col5778/index.html',
        '首页-学术-活动视频': 'http://www.mscfungi.org.cn/col/col5779/index.html',
        '首页-产业动态-产业动态': 'http://www.mscfungi.org.cn/col/col1888/index.html',
        '首页-产业动态-专题论坛': 'http://www.mscfungi.org.cn/col/col6228/index.html',
        '首页-产业动态-活动通知': 'http://www.mscfungi.org.cn/col/col6228/index.html',
        '首页-科普-科普动态': 'http://www.mscfungi.org.cn/col/col1896/index.html',
        '首页-科普-科普资讯': 'http://www.mscfungi.org.cn/col/col1869/index.html',
        '首页-科普-科普知识': 'http://www.mscfungi.org.cn/col/col1897/index.html',
    }
    for key, value in urls.items():
        news_classify = key
        html_text, status_code = get_html(value)
        if status_code == 404:
            break
        deadline = get_data(html_text, news_classify, account_name,
                            science_system, mongo, project_time, start_time)
        if deadline:
            break
        time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default',
                        default='2022-07-01')
    args = parser.parse_args()
    account_name = 'A-40 中国菌物学会'
    start_run(args.projectname, args.sinceyear, account_name)
