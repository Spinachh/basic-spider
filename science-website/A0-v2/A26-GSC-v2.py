# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-04-20
import random
import re
import io
import sys
import time
import json
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash():
    cookies = {
        'td_cookie': '2095021031',
        'Hm_lvt_ec22f37ea1cce8f86744c8d3c25d6c4e': '1667870775',
        'Hm_lpvt_ec22f37ea1cce8f86744c8d3c25d6c4e': '1667870812',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.botany.org.cn/xwzx/xwdt_/index_1.html',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get('http://www.botany.org.cn/xwzx/xwdt_/index.html', headers=headers, cookies=cookies,
                            verify=False)
    html_text = response.text
    # print(html_text)
    # breakpoint()
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url):

    cookies = {
        'td_cookie': '2358527208',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.gsc.ac.cn/zhxw/',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Mobile Safari/537.36',
    }

    response = requests.get(url, headers=headers, cookies=cookies)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    selector = etree.HTML(html_text)
    # part1
    try:
        part1_nodes = selector.xpath('//*[@valign="top"]/table//tr//tr')
        # print(part1_nodes)
        # breakpoint()
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        print('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./td[2]/a/text()')[0]
        except:
            news_title = ''

        news_abstract = news_title

        try:
            # news_publish_time = part_nodes[i].xpath('./text()')[-1].strip().replace('[', '').replace(']', '')
            news_publish_time = part_nodes[i].xpath('./td[3]/div/text()')[0]
        except:
            news_publish_time = '2022-01-01'
        # try:
        #     news_imgs = 'http://www.gsc.org.cn/' + part_nodes[i].xpath('./a/img/@src')
        # except:
        #     news_imgs = ''

        # print('http://csmpg.gyig.cas.cn/xhyw' + part_nodes[i].xpath('./a/@href')[0].strip().replace('./', '/'))
        # breakpoint()
        # print(news_title)
        # print(news_publish_time)
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))


        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = part_nodes[i].xpath('./td[2]/a/@href')[0].replace('./', '/')

            news_page_url = 'http://www.gsc.ac.cn/zhxw/' + url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())


            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@valign="top"]//p//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'>来源:(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-综合新闻': 'http://www.gsc.ac.cn/zhxw/index_1.html',
        '首页-通知公告': 'http://www.gsc.ac.cn/tzgg/index_1.html',
        '首页-走进学会-历史沿革': 'http://www.gsc.ac.cn/zjxh/lsyg/index_1.html',
        '首页-学术交流-中国遗传学会全国会员代表大会': 'http://www.gsc.ac.cn/xsjl/zgycxhqghydbdh/index_1.html',
        '首页-学术交流-中国遗传学会大会': 'http://www.gsc.ac.cn/xsjl/zgycxhdh/index_1.html',
        '首页-学术交流-专业委员会学术交流': 'http://www.gsc.ac.cn/xsjl/zywyhxsjl/index_1.html',
        '首页-学术交流-地方遗传学会联合会议': 'http://www.gsc.ac.cn/xsjl/dfycxhlhhy/index_1.html',
        '首页-学术交流-学会综合交叉会议': 'http://www.gsc.ac.cn/xsjl/xhzhjchy/index_1.html',
        '首页-学术交流-专题会议': 'http://www.gsc.ac.cn/xsjl/pizthy/index_1.html',
        '首页-学术交流-国际会议': 'http://www.gsc.ac.cn/xsjl/gjhy/index_1.html',
        '首页-表彰奖励-中国遗传学会李汝祺动物遗传奖': 'http://www.gsc.ac.cn/bzjl/zgycxhlrqdwycj/index_1.html',
        '首页-表彰奖励-中国遗传学会李振声植物遗传奖': 'http://www.gsc.ac.cn/bzjl/zgycxhlzszwycj/index_1.html',
        '首页-表彰奖励-中国遗传学会吴旻医学遗传奖': 'http://www.gsc.ac.cn/bzjl/zgycxhwmyxycj/index_1.html',
        '首页-表彰奖励-中国遗传学会谈家桢遗传教育奖': 'http://www.gsc.ac.cn/bzjl/zgycxhtjzycjyj/index_1.html',
        '首页-表彰奖励-学会推荐奖项': 'http://www.gsc.ac.cn/bzjl/xhtjjx/index_1.html',
        '首页-科普之窗-科普活动': 'http://www.gsc.ac.cn/kpzc/kphd/index_1.html',
        '首页-科普之窗-科普知识': 'http://www.gsc.ac.cn/kpzc/kpzs/index_1.html',
        '首页-会员之家-会员活动': 'http://www.gsc.ac.cn/hyzj/hyhd/index_1.html',
        '首页-会员之家-入会须知': 'http://www.gsc.ac.cn/hyzj/rhxz/index_1.html',
        '首页-下载中心': 'http://www.gsc.ac.cn/xzzx/index_1.html',

    }
    # supFlash = get_supflash()
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            if page == 1:
                url = value.replace('index_1', 'index')
            else:
                url = value.replace('_1.html', '_' + str(page - 1) + '.html')
            html_text, status_code = get_html(url)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default',
                        default='2022-07-01')
    args = parser.parse_args()
    account_name = 'A-26 中国遗传学会'
    start_run(args.projectname, args.sinceyear, account_name)
