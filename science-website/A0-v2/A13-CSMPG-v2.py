# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-04-20
import random
import re
import io
import sys
import time
import json
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash(url):


    cookies = {
        'insert_cookie': '49613759',
        'td_cookie': '1762987450',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://csmpg.gyig.cas.cn/',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)
    html_text = response.text
    # print(html_text)
    # breakpoint()
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, page):

    cookies = {
        'td_cookie': get_supflash(url),
        'insert_cookie': '49613759',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://csmpg.gyig.cas.cn/xhyw/index_{}.html'.format(page),
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers,
                            cookies=cookies, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time ):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a//font/text()')[0]
        except:
            news_title = part_nodes[i].xpath('./a/text()')[0]

        news_abstract = news_title
        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0].replace('[', '').replace(']', '')
        except:
            news_publish_time = '2022-01-01'
        # try:
        #     news_imgs = 'http://www.gsc.org.cn/' + part_nodes[i].xpath('./a/img/@src')
        # except:
        #     news_imgs = ''

        # print('http://csmpg.gyig.cas.cn/xhyw' + part_nodes[i].xpath('./a/@href')[0].strip().replace('./', '/'))
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = part_nodes[i].xpath('./a/@href')[0].strip().replace('./', '/')
            # if 'html' in url_part2:
            news_page_url = 'http://csmpg.gyig.cas.cn/xhyw' + url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title[:20], news_publish_time))
        else:
            deadline = True
            return deadline


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    selector = etree.HTML(html_text)
    # part1
    try:
        part1_nodes = selector.xpath('//*[@class="list-tab"]//li')
        deadline = xpath_data(part1_nodes, news_classify, science_system, mongo,
                              account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        print('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def get_page_content(news_page_url):

    cookies = {
        'td_cookie': get_supflash(news_page_url),
        'insert_cookie': '49613759',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    html_response = requests.get(news_page_url, headers=headers, cookies=cookies,
                            verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="textxl nrhei"]//p//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'>来源：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''
    try:
        click_count = re.findall(r'>阅读次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页内容-学会动态': 'http://csmpg.gyig.cas.cn/xhyw/index_1.html',
        '首页内容-通知公告': 'http://csmpg.gyig.cas.cn/tzgg2017/index_1.html',
        '首页内容-学会要闻': 'http://csmpg.gyig.cas.cn/sytpxw2017/index_1.html',
        '首页内容-科普园地-科普活动': 'http://csmpg.gyig.cas.cn/kpyd2017/kphd2017/index_1.html',
        '首页内容-科普园地-科普长廊': 'http://csmpg.gyig.cas.cn/kpyd2017/kpzl/index_1.html',
        '首页内容-学术交流-成果进展': 'http://csmpg.gyig.cas.cn/xsjl2017/cgjz2017/index_1.html',
        '首页内容-学术交流-会议纪要': 'http://csmpg.gyig.cas.cn/xsjl2017/hyjy2017/index_1.html',
        '首页内容-学术交流-会议通知': 'http://csmpg.gyig.cas.cn/xsjl2017/hytz_134984/index_1.html',
        '首页内容-学术交流-会议计划': 'http://csmpg.gyig.cas.cn/xsjl2017/hyjh/index_1.html',
        '首页内容-学术年会': 'http://csmpg.gyig.cas.cn/xsnh/2022/index_1.html',
        '首页内容-学会评奖-侯德封奖评选活动': 'http://csmpg.gyig.cas.cn/xhpj2017/hdfj_134991/pxhd_134994/index_1.html',
        '首页内容-党建工作-党组织生活': 'http://csmpg.gyig.cas.cn/djgz2017/dzzsh/index_1.html',
        '首页内容-党建工作-党的二十大精神': 'http://csmpg.gyig.cas.cn/djgz2017/ddesdjs/index_1.html',
    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            if page == 1:
                url = value.replace('index_1', 'index')
            else:
                url = value.replace('index_1', 'index_' + str(page - 1))
            html_text, status_code = get_html(url, page)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name, science_system,
                                mongo, project_time,start_time)
            if deadline:
                break


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default',
                        default='2022-07-01')
    args = parser.parse_args()
    account_name = 'A-13 中国矿物岩石地球化学学会'
    start_run(args.projectname, args.sinceyear, account_name)
