# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-04-20

import random
import re
import io
import sys
import time
import json
import cchardet
import urllib3
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash():
    cookies = {
        'td_cookie': '2095021031',
        'Hm_lvt_ec22f37ea1cce8f86744c8d3c25d6c4e': '1667870775',
        'Hm_lpvt_ec22f37ea1cce8f86744c8d3c25d6c4e': '1667870812',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.botany.org.cn/xwzx/xwdt_/index_1.html',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get('http://www.botany.org.cn/xwzx/xwdt_/index.html', headers=headers, cookies=cookies,
                            verify=False)
    html_text = response.text
    # print(html_text)
    # breakpoint()
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url):

    cookies = {
        'PHPSESSID': 'rc8pi86ipcut19v5v7imnqlgl2',
        'cookiesession1': '678A3E0DIJKLMNOPQRSTUVWXYZABFA99',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'none',
        'Sec-Fetch-User': '?1',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Mobile Safari/537.36',
        'sec-ch-ua': '"Chromium";v="106", "Google Chrome";v="106", "Not;A=Brand";v="99"',
        'sec-ch-ua-mobile': '?1',
        'sec-ch-ua-platform': '"Android"',
    }

    response = requests.get(url, headers=headers, cookies=cookies)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code

def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    selector = etree.HTML(html_text)
    # part1
    try:
        part1_nodes = selector.xpath('//*[@class="in_news"]/ul/li')
        # print(part1_nodes)
        # breakpoint()
        deadline = xpath_data(part1_nodes, news_classify, science_system, mongo,
                              account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        print('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./div/a/text()')[0]
        except:
            news_title = 'not news_title'

        news_abstract = news_title

        try:
            news_publish_time = part_nodes[i].xpath('./div/text()')[-1].strip().replace('[', '').replace(']', '')
        except:
            news_publish_time = '2022-01-01'
        # try:
        #     news_imgs = 'http://www.gsc.org.cn/' + part_nodes[i].xpath('./a/img/@src')
        # except:
        #     news_imgs = ''

        # print('http://csmpg.gyig.cas.cn/xhyw' + part_nodes[i].xpath('./a/@href')[0].strip().replace('./', '/'))
        # breakpoint()
        # print(news_title)
        # print(news_publish_time)
        # breakpoint()
        # print(news_title)
        # print(news_publish_time)
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))


        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = part_nodes[i].xpath('./div/a/@href')[0]
            # print(url_part2)
            # breakpoint()
            news_page_url = 'https://www.csbmb.org.cn/' + url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())


            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} '
                            .format(account_name, news_classify, news_title[:20], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="panel panel-default"]//p/img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'>来源：(.*?)　时间', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学会通知': 'https://www.csbmb.org.cn/newsmore/9_1.html',
        '首页-学会新闻': 'https://www.csbmb.org.cn/newsmore/10_1.html',
        '首页-学会公告': 'https://www.csbmb.org.cn/newsmore/11_1.html',
        '首页-学术交流-国际学术会议': 'https://www.csbmb.org.cn/newsmore/14_1.html',
        '首页-学术交流-地区性学术会议': 'https://www.csbmb.org.cn/newsmore/16_1.html',
        '首页-学术交流-专题会议': 'https://www.csbmb.org.cn/newsmore/17_1.html',
        '首页-组织建设-分支机构负责人会议': 'https://www.csbmb.org.cn/newsmore/19_1.html',
        '首页-组织建设-国际事务': 'https://www.csbmb.org.cn/newsmore/19_1.html',
        '首页-学会期刊-期刊动态': 'https://www.csbmb.org.cn/newsmore/22_1.html',
        '首页-科普与人才培养-其它科普活动': 'https://www.csbmb.org.cn/newsmore/24_1.html',
        '首页-科普与人才培养-继续教育': 'https://www.csbmb.org.cn/newsmore/24_1.html',
        '首页-地方学会-地方学会': 'https://www.csbmb.org.cn/newsmore/27_1.html',
        '首页-地方学会-地方学会动态': 'https://www.csbmb.org.cn/newsmore/28_1.html',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            url = value.replace('_1.html', '_' + str(page) + '.html')
            html_text, status_code = get_html(url)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'A-22 中国生物化学与分子生物学会'
    start_run(args.projectname, args.sinceyear, account_name)
