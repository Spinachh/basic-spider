# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-04-20

import re
import io
import sys
import time
import json
import cchardet
import urllib3
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_awards_html(url, page):
    headers = {
        'authority': 'www.chemsoc.org.cn',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'accept-language': 'zh-CN,zh;q=0.9',
        'cookie': 'Hm_lvt_1d2885a6e1b71130048fd6f33d0ebc15=1666668211; SZACSESSID=lmndsnv9jfdtm7d5ovf2c824cb; '
                  'Hm_lpvt_1d2885a6e1b71130048fd6f33d0ebc15=1666753984',
        'referer': 'https://www.chemsoc.org.cn/Awards/Home/newslist.php?page={}'.format(page),
        'sec-ch-ua': '"Chromium";v="106", "Google Chrome";v="106", "Not;A=Brand";v="99"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }

    params = (
        ('page', page),
    )

    response = requests.get(url, headers=headers, params=params)
    status_code = response.status_code
    return response.text, status_code


def get_meet_html(url, page):
    headers = {
        'authority': 'www.chemsoc.org.cn',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'accept-language': 'zh-CN,zh;q=0.9',
        'cookie': 'Hm_lvt_1d2885a6e1b71130048fd6f33d0ebc15=1666668211; SZACSESSID=lmndsnv9jfdtm7d5ovf2c824cb; '
                  'Hm_lpvt_1d2885a6e1b71130048fd6f33d0ebc15=1666753142',
        'sec-ch-ua': '"Chromium";v="106", "Google Chrome";v="106", "Not;A=Brand";v="99"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'none',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    params = (
        ('', ''),
        ('page', page),
    )

    response = requests.get(url, headers=headers, params=params)
    status_code = response.status_code
    return response.text, status_code


def get_A06_html(url, page):
    headers = {
        'authority': 'www.chemsoc.org.cn',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'accept-language': 'zh-CN,zh;q=0.9',
        'cache-control': 'max-age=0',
        'cookie': 'Hm_lvt_1d2885a6e1b71130048fd6f33d0ebc15=1666668211; SZACSESSID=lmndsnv9jfdtm7d5ovf2c824cb; '
                  'Hm_lpvt_1d2885a6e1b71130048fd6f33d0ebc15=1666748992',
        'referer': 'https://www.chemsoc.org.cn/meeting/home/announcement/?page={}'.format(page),
        'sec-ch-ua': '"Chromium";v="106", "Google Chrome";v="106", "Not;A=Brand";v="99"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    params = (
        ('page', page),
    )

    response = requests.get(url, headers=headers, params=params)
    status_code = response.status_code
    return response.text, status_code


def get_A06_data(html_text, news_classify, account_name, science_system, start_time, mongo, project_time):
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    selector = etree.HTML(html_text)
    nodes = selector.xpath('//*[@class="newslist"]/li')
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    for i in range(len(nodes)):
        try:
            news_title = nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = 'not news_title'

        try:
            news_abstract = nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = nodes[i].xpath('./span/text()')[0].strip().replace('/', '-')
        except:
            news_publish_time = '2023-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        news_page_url = 'https://www.chemsoc.org.cn/' + nodes[i].xpath('./a/@href')[0]

        if int(news_publish_stamp) >= int(start_time_stamp):
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title[:20], news_publish_time))
        else:
            deadline = True
            return deadline


def get_meet_data(html_text, news_classify, account_name, science_system,
                  start_time, mongo, project_time):
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    selector = etree.HTML(html_text)
    nodes = selector.xpath('//*[@class="d-meeting-list"]/li')
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    for i in range(len(nodes)):
        try:
            news_title = nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = 'not news_title'

        try:
            news_abstract = nodes[i].xpath('./span[1]/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = nodes[i].xpath('./span[3]/text()')[0].strip().split('-')[0]
        except:
            news_publish_time = '2023-01-01'
        # print(news_publish_time)

        try:
            news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y年%m月%d日'))
        except:
            news_publish_stamp = time.time()
        # print(news_publish_stamp)
        # breakpoint()
        # news_publish_time = time.strftime('%Y-%m-%d', str(news_publish_stamp))
        news_publish_time = re.sub(r'[\年, \月]', '-', news_publish_time)

        news_page_url = 'https://www.chemsoc.org.cn/' + nodes[i].xpath('./a[1]/@href')[0]

        if int(news_publish_stamp) >= int(start_time_stamp):

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} '
                            .format(account_name, news_classify, news_title[:20], news_publish_time))
        else:
            return ''


def get_awards_data(html_text, news_classify, account_name, science_system,
                    start_time, mongo, project_time):
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    selector = etree.HTML(html_text)
    nodes = selector.xpath('//*[@class="as-b1-c"]/ul/li')
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    for i in range(len(nodes)):
        try:
            news_title = nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = 'not news_title'

        try:
            news_abstract = nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = nodes[i].xpath('./p/text()')[0].strip()
        except:
            news_publish_time = '2023-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        news_page_url = 'https://www.chemsoc.org.cn/' + nodes[i].xpath('./a/@href')[0]

        if int(news_publish_stamp) >= int(start_time_stamp):

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title[:20], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    headers = {
        'authority': 'www.chemsoc.org.cn',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'accept-language': 'zh-CN,zh;q=0.9',
        'cache-control': 'max-age=0',
        'cookie': 'Hm_lvt_1d2885a6e1b71130048fd6f33d0ebc15=1666668211; SZACSESSID=lmndsnv9jfdtm7d5ovf2c824cb; '
                  'Hm_lpvt_1d2885a6e1b71130048fd6f33d0ebc15=1666748992',
        'referer': 'https://www.chemsoc.org.cn/meeting/home/announcement',
        'sec-ch-ua': '"Chromium";v="106", "Google Chrome";v="106", "Not;A=Brand";v="99"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }
    html_response = requests.get(news_page_url, headers=headers, verify=False)
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="news-con"]//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'>来源：(.*?)<', content_text, re.M | re.S)[0]
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览次数：(.*?)', content_text, re.M | re.S)[0]
    except:
        read_count = ''
    try:
        click_count = re.findall(r'阅读次数：(.*?)', content_text, re.M | re.S)[0]
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '学术会议-学术会议': 'https://www.chemsoc.org.cn/meeting/home/search.html?&page=1',
        '学术会议-会议通知': 'https://www.chemsoc.org.cn/meeting/home/announcement/?page=1',
        '学术会议-会议报道': 'https://www.chemsoc.org.cn/meeting/home/news/?page=1',
        '通知公告-通知公告': 'https://www.chemsoc.org.cn/notice/?page=1',
        '国际交流': 'https://www.chemsoc.org.cn/news/international/?page=1',
        '学会咨询-国内动态': 'https://www.chemsoc.org.cn/news/internal/?page=1',
        '奖励公告': 'https://www.chemsoc.org.cn/Awards/Home/newslist.php?page=1'
    }
    for key, value in urls.items():
        news_classify = key
        url = value
        for page in range(1, 50):
            url = url.replace('&p=1', '&p=' + str(page))
            if news_classify == '学术会议-学术会议':
                html_meeting_text, status_code = get_meet_html(url, page)
                if status_code == 404:
                    break
                deadline = get_meet_data(html_meeting_text, news_classify, account_name, science_system,
                                         start_time, mongo, project_time)
                if deadline:
                    break
                time.sleep(1)

            elif news_classify == '奖励公告':
                html_awards_text, status_code = get_awards_html(url, page)
                if status_code == 404:
                    break
                deadline = get_awards_data(html_awards_text, news_classify, account_name, science_system,
                                           start_time, mongo, project_time)
                if deadline:
                    break
                time.sleep(1)
            else:
                html_text, status_code = get_A06_html(url, page)
                deadline = get_A06_data(html_text, news_classify, account_name, science_system,
                                        start_time, mongo, project_time)
                if deadline:
                    break
                time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'A-06 中国化学会'
    start_run(args.projectname, args.sinceyear, account_name)
