# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-04-20

import re
import io
import sys
import time
import json
import cchardet
import urllib3
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, page):

    cookies = {
        'Hm_lvt_a9d4781b1703c5fbbaa8ee54f513df28': '1666340010',
        'Hm_lpvt_a9d4781b1703c5fbbaa8ee54f513df28': '1666578280',
        'JSESSIONID': '03a80a20-f130-4bc8-b039-8eabca67288c',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'https://www.cstam.org.cn/category/7329.html',
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-User': '?1',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
        'sec-ch-ua': '"Chromium";v="106", "Google Chrome";v="106", "Not;A=Brand";v="99"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)
    status_code = response.status_code
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, start_time, mongo, project_time):

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    selector = etree.HTML(html_text)
    nodes = selector.xpath('//*[@class="my-list"]/li')

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    for i in range(len(nodes)):
        try:
            news_title = nodes[i].xpath('.//a/@title')[0]
        except:
            news_title = 'not news_title'

        try:
            news_abstract = nodes[i].xpath('.//a/@alt')[0]
        except:
            news_abstract = ''

        try:
            news_publish_time = nodes[i].xpath('.//span/text()')[0]
        except:
            news_publish_time = '2023-01-01'
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        news_page_url = 'https://www.cstam.org.cn/' + nodes[i].xpath('.//a/@href')[0]

        if int(news_publish_stamp) >= int(start_time_stamp):
            news_author, news_imgs, news_content_type, news_content, source,\
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title[:20], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url)
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="txt-wp"]//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'文章来源：(.*?)', content_text, re.M | re.S)[0]
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览次数：(.*?)', content_text, re.M | re.S)[0]
    except:
        read_count = ''
    try:
        click_count = re.findall(r'阅读次数：(.*?)', content_text, re.M | re.S)[0]
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()

    science_system = read_science_account(account_name)

    urls = {
        '学会动态': 'https://www.cstam.org.cn/category/7329_1.html',
        '学会动态-学会工作信息——学会': 'https://www.cstam.org.cn/category/7318/7478_1.html',
        '学会动态-学会工作信息——期刊': 'https://www.cstam.org.cn/category/7318/7479_1.html',
        '学会动态-理事会': 'https://www.cstam.org.cn/category/7395_1.html',
        '学会动态-分支机构': 'https://www.cstam.org.cn/category/7347_1.html',
        '学会动态-地方学会': 'https://www.cstam.org.cn/category/7348_1.html',
        '学术动态': 'https://www.cstam.org.cn/category/7356_1.html',
        '学术动态-重要学术活动': 'https://www.cstam.org.cn/category/7357_1.html',
        '学术动态-国内外学术交流': 'https://www.cstam.org.cn/category/7358_1.html',
        '学术动态-会议通知': 'https://www.cstam.org.cn/category/7465_1.html',
        '学术动态-基金信息': 'https://www.cstam.org.cn/category/7466_1.html',
        '科技信息': 'https://www.cstam.org.cn/category/7468_1.html',
        '科技信息-招聘信息': 'https://www.cstam.org.cn/category/7469_1.html',
        '科普教育-科普教育动态': 'https://www.cstam.org.cn/category/7344_1.html',
        '科普教育-海峡两岸交流': 'https://www.cstam.org.cn/category/7344_1.html',
    }
    for key, value in urls.items():
        news_classify = key
        url = value
        for page in range(1, 50):
            url = url.replace('_1', '_' + str(page))
            html_text,status_code = get_html(url, page)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, start_time, mongo, project_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'A-03 中国力学学会'
    start_run(args.projectname, args.sinceyear, account_name)
