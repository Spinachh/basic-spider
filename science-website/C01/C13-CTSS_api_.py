# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-01-30
import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    content = json.loads(html_text)
    try:
        ree_data(content, news_classify, science_system, mongo,
                 account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
             account_name, project_time, start_time):
    results = content['varList']
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for item in results:
        try:
            news_title = item.get('PART_TITLE')
        except:
            news_title = ''
        try:
            news_abstract = item.get('PART_TITLE')
        except:
            news_abstract = ''

        try:
            news_publish_time = item.get('PUBLISH_TIME')
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_publish_time)
        # print(news_publish_stamp)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            news_page_url = 'https://chinatss.cn/teaApi/party/detail?PARTY_ID={}'.format(item.get('PARTY_ID'))
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    html_text = json.loads(html_response.text)
    content_text = html_text.get('data')
    # selector_page = etree.HTML(content_text)
    # extractor = GeneralNewsExtractor()
    # result = extractor.extract(content_text)
    try:
        news_content = content_text.get('PARTY_CONTENT')
        dr = re.compile(r'<[^>]+>', re.S)
        news_content = dr.sub('', news_content).strip()
    except:
        news_content = ''
    try:
        news_author = content_text.get('CREATE_BY')
    except:
        news_author = ''
    try:
        news_imgs = content_text.get('ATTACHMENT')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = content_text.get('PARTY_AUTHOR')
    except:
        source = ''

    try:
        read_count = re.findall(r'阅读量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-党建强会-党史学习': 'https://chinatss.cn/teaApi/copHistory/list?showCount=10&currentPage=1',
        '首页-党建强会-党建活动': 'https://chinatss.cn/teaApi/party/listPhone?showCount=10&currentPage=1',
        '首页-学术交流-通知公告': 'https://chinatss.cn/teaApi/notice/listPhone?showCount=10&currentPage=1',
        '首页-学术交流-交流成果': 'https://chinatss.cn/teaApi/exchangeResults/listPhone?showCount=5&currentPage=1',
        '首页-科技普及-科普常识': 'https://chinatss.cn/teaApi/knowledge/listPhone?showCount=6&currentPage=1',
        '首页-品质评价-评价要闻': 'https://chinatss.cn/teaApi/evaluationFocus/list?showCount=10&currentPage=1',
        '首页-成果评价-科技成果评价': 'https://chinatss.cn/teaApi/AEATNotice/list?showCount=10&currentPage=1',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            url = value.replace('currentPage=1', 'currentPage=' + str(page))
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)
            time.sleep(1)
            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'C-13 中国茶叶学会'
    start_run(args.projectname, args.sinceyear, account_name)
