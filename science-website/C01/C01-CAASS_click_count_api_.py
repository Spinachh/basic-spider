# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-01-28

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@id="ContentPane"]//li')
        xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/@title')[0].strip()
        except:
            news_title = ''
        try:
            news_abstract = part_nodes[i].xpath('./a/@title')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0]
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'http://' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://caass.org.cn/' + url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="jjjc_content_xilan"]//p//text()')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="jjjc_content_xilan"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?);', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：(.*?)</', content_text, re.M | re.S)[0]
    except:
        read_count = ''

    try:
        articlekey = news_page_url.split('/')[-2]
        cookies = {
            'JSESSIONID': '98D418262568EE914F610F76B3F4EC8B',
        }

        headers = {
            'Accept': 'text/plain, */*; q=0.01',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Connection': 'keep-alive',
            'Content-Length': '0',
            'Origin': 'http://caass.org.cn',
            'Referer': news_page_url,
            'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Mobile Safari/537.36',
            'X-Requested-With': 'XMLHttpRequest',
        }

        params = (
            ('pageId', '49172'),
            ('moduleId', '3'),
            ('columnId', '49335'),
            ('articleKey', articlekey),
            # ('struts.portlet.action', '/app/counting-front/u0021saveInfo.action'),
        )

        response = requests.post('http://caass.org.cn/eportal/ui?&struts.portlet.action=/app/counting-front!saveInfo.action',
                                 headers=headers, params=params, verify=False)
        # print(response.content)
        # breakpoint()
        click_count = response.text
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '主页-要闻': 'http://caass.org.cn/xbnxh/yw12/f15c8e65-1.html',
        '主页-通知公告': 'http://caass.org.cn/xbnxh/tzgg3337/6cf84987-1.html',
        '主页-工作动态-工作动态': 'http://caass.org.cn/xbnxh/gzdt56/1c1c2503-1.html',
        '主页-工作动态-办事机构': 'http://caass.org.cn/xbnxh/gzdt56/bsjg13/c4399c84-1.html',
        '主页-工作动态-分支机构': 'http://caass.org.cn/xbnxh/gzdt56/fzjg10/443ce69d-1.html',
        '主页-工作动态-地方学会': 'http://caass.org.cn/xbnxh/gzdt56/sjxh48/943c53c8-1.html',
        '主页-党的建设': 'http://caass.org.cn/xbnxh/ddjs90/7d84f23e-1.html',
        '主页-学术交流-学科发展': 'http://caass.org.cn/xbnxh/xsjl36/xkfz48/3970a9af-1.html',
        '主页-学术交流-学术会议': 'http://caass.org.cn/xbnxh/xsjl36/xshy0/1ead8c8a-1.html',
        '主页-学术交流-国际交流': 'http://caass.org.cn/xbnxh/xsjl36/gjjl95/bfe426a8-1.html',
        '主页-科学普及-科学素质提升': 'http://caass.org.cn/xbnxh/kxpj67/kxszts5/53447a6a-1.html',              
        '主页-三农建设': 'http://caass.org.cn/xbnxh/snjs/abcf2825-1.html',
        '主页-科技评价': 'http://caass.org.cn/xbnxh/kjpj54/e748bd24-1.html',
        '主页-人才培养': 'http://caass.org.cn/xbnxh/rcpy29/0dda46fe-1.html',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            url = value.replace('-1.html', '-' + str(page) + '.html')
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)

            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'C-01 中国农学会'
    start_run(args.projectname, args.sinceyear, account_name)
