# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-01-29

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url, verify=False,timeout=5)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="col-xs-12 rightcontent"]/ul/li')
        xpath_data(part1_nodes, news_classify,
                   science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./a/span/text()')[0]
            news_publish_time = re.sub(r'[\],\[]', '', news_publish_time)
            # print(news_publish_time)
            # breakpoint()
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'http://' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.sbxh.org' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source,\
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count,click_count , news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="content"]//span/text()')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = re.findall(r'作者:(.*?)</', content_text, re.M | re.S)[0].strip()

    try:
        news_imgs = selector_page.xpath('//*[@class="content"]//span//img/@src |'
                                        ' //*[@class="content"]//p//img/@src' )[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源:(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    # 这里的点击数是api接口获取，网站coder比较懒没有继续做
    try:
        aid = re.findall(r'\d+', news_page_url.split('/')[-1])
        click_url = 'http://www.sbxh.org/sbxhindex/plus/count.php?view=yes&aid={}&mid=2'.format(aid)
        click_response = requests.get(click_url).text
        click_count = re.findall(r'\d+', click_response)[0]
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '主页-关于学会': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xuehuixinwen/list_1_1.html',
        '主页-专业委员会': 'http://www.sbxh.org/sbxhindex/a/zhuanyeweiyuanhui/list_37_1.html',
        '主页-关于学会-省级学会': 'http://www.sbxh.org/sbxhindex/a/shengjixuehui/list_38_1.html',
        '主页-关于学会-专业委员会': 'http://www.sbxh.org/sbxhindex/a/guanyuxuehui/zhuanyeweiyuanhui/',
        '主页-通知公告-水平评价': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xuehuixinwen/list_33_1.html',
        '主页-通知公告-培训': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xuehuixinwen/list_34_1.html',
        '主页-通知公告-奖励': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xuehuixinwen/list_35_1.html',
        '主页-通知公告-其它': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xuehuixinwen/list_72_1.html',
        '主页-新闻资讯': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xuehuixinwen/list_9_1.html',
        '主页-新闻资讯-学会新闻': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xuehuixinwen/list_10_1.html',
        '主页-新闻资讯-行业新闻': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xingyexinwen/list_11_1.html',
        '主页-新闻资讯-社会新闻': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xingyexinwen/list_12_1.html',
        '主页-学术活动-国内活动': 'http://www.sbxh.org/sbxhindex/a/xinwenzixun/xingyexinwen/list_15_1.html',
        '主页-学术活动-国际活动': 'http://www.sbxh.org/sbxhindex/a/xueshuhuodong/guowaihuodong/',
        '主页-学术活动-成功评价': 'http://www.sbxh.org/sbxhindex/a/xueshuhuodong/chengguopingjia/',
        '主页-科普园地': 'http://www.sbxh.org/sbxhindex/a/kepuyuandi/kepuhuodong/list_17_1.html',
        '主页-科普园地-科普活动': 'http://www.sbxh.org/sbxhindex/a/kepuyuandi/kepuhuodong/list_18_1.html',
        '主页-科普园地-科普知识': 'http://www.sbxh.org/sbxhindex/a/kepuyuandi/kepuzhishi/',
        '主页-科普园地-科普基地': 'http://www.sbxh.org/sbxhindex/a/kepuyuandi/kepujidi/',
        '主页-水平评价-管理办法': 'http://www.sbxh.org/sbxhindex/a/zizhiguanli/guanlibanfa/',
        '主页-水平评价-通知公告': 'http://www.sbxh.org/sbxhindex/a/xiangguanwenjian/xiangguanwenjian/list_68_1.html',
        '主页-水平评价-相关材料': 'http://www.sbxh.org/sbxhindex/a/xiangguanwenjian/',
        '主页-表彰奖励-相关文件': 'http://www.sbxh.org/sbxhindex/a/biaozhangjiangli/xiangguanwenjian/',
        '主页-会员管理-通知公告': 'http://www.sbxh.org/sbxhindex/a/huiyuanguanli/xiangguancailiao/',
        '主页-党建工作-党的活动': 'http://www.sbxh.org/sbxhindex/a/dangjiangongzuo/dangdehuodong/',
        '主页-下载专区': 'http://www.sbxh.org/sbxhindex/a/xiangguanxiazai/',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            if '.html' in value:
                url = value.replace('_1.html', '_' + str(page) + '.html')
            else:
                url = value
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)

            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'C-12 中国水土保持学会'
    start_run(args.projectname, args.sinceyear, account_name)
