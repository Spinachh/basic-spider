# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-02-0
import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash():
    url = 'http://www.gxast.org.cn/kxxw/ttyw/p-1.html'
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.gxast.org.cn/kxxw/ttyw/p-1.html',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, verify=False)
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url):

    response = requests.get(url, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        if 'special' not in news_classify:
            part1_nodes = selector.xpath('//*[@class="NewsList"]/li/a')
            part2_nodes = selector.xpath('//*[@class="NewsList"]/li/span')

            xpath_data(part1_nodes, part2_nodes, news_classify, science_system, mongo,
                   account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part1_nodes, part2_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part1_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part1_nodes[i].xpath('./@title')[0].strip()
            else:
                news_title = part1_nodes[i].xpath('./h1/a/text()')[0].strip()
        except:
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = news_title
            else:
                news_abstract = part1_nodes[i].xpath('./h1/a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_time = part2_nodes[i].xpath('./text()')[0].strip()
            else:
                news_publish_time = part2_nodes[i].xpath('./div[2]/div/text()')[0].split(' ')[0]
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part1_nodes[i].xpath('./@href')[0].strip()
                else:
                    url_part2 = part1_nodes[i].xpath('./div[2]/a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.gast.org.cn' + url_part2

            if 'mp.weixin.qq.com' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    try:
        html_response = requests.get(news_page_url, verify=False)
        html_response.encoding = 'utf-8'
        content_text = html_response.text
    except Exception as e:
        logging.warning('Article Content Was Error :{}'.format(e))
        content_text = ''

    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath(
            '//*[@class="Article_zw"]//p//text() |'
            '//*[@class="Article_zw"]/p/span//text() '
        )
        news_content = ''.join([x.strip() for x in news_content])
    except Exception as e:
        logging.warning("Article Content Xpath Was Error: {}".format(e))
        news_content = result.get('content')
    try:
        news_author = re.findall(r'作者：(.*?)<', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="Article_zw"]//p//img/@src |'
                                        '//*[@class="Article_zw"]//span//img/@src | '
                                        '//*[@class="Article_zw"]//div//img/@src |'
                                        '//*[@class="Article_zw"]//input/@src |'
                                        '//*[@class="Article_zw"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'


    try:
        news_video = selector_page.xpath('//*[@class="Article_zw"]//span//video/@src |'
                                         ' //*[@class="Article_zw"]//p//video/@src |'
                                         ' //*[@class="Article_zw"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r" var wzly='(.*?)if", content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        docid = re.findall(r'_(.*?).html', news_page_url)[0]

        headers = {
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Connection': 'keep-alive',
            'Referer': news_page_url,
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36',
            'X-Requested-With': 'XMLHttpRequest',
        }

        params = (
            ('docId', docid),
        )

        read_response = requests.get('http://www.gast.org.cn/mgta/doc',
                                     headers=headers,
                                     params=params,
                                     verify=False).json()
        read_count = read_response.get('data')['totalPv']

    except Exception as e:
        logging.warning('Read Count Was Error :{}'.format(e))
        read_count = ''
    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-科协动态-头条': 'http://www.gast.org.cn/kxdt/tt/index_1.html',
        '首页-科协动态-要闻': 'http://www.gast.org.cn/kxdt/yw/index_1.html',
        '首页-科协动态-学会学术': 'http://www.gast.org.cn/kxdt/xhxs/index_1.html',
        '首页-科协动态-地方科协': 'http://www.gast.org.cn/kxdt/dfkx/index_1.html',
        '首页-科协动态-媒体聚焦': 'http://www.gast.org.cn/kxdt/mtjj/index_1.html',
        '首页-政务公开-通知公告': 'http://www.gast.org.cn/zwgk/tzgg/index_1.html',
        '首页-政务公开-公示专区': 'http://www.gast.org.cn/zwgk/gszq/index_1.html',
        '首页-政务公开-政策法规': 'http://www.gast.org.cn/zwgk/zcwj/index_1.html',
        '首页-政务公开-预决算公开': 'http://www.gast.org.cn/zwgk/yjsgk/index_1.html',
        '首页-党建工作-党建工作': 'http://www.gast.org.cn/djgz/index_1.html',
        '首页-科学普及-科普动态': 'http://www.gast.org.cn/kxpj/kpdt/index_1.html',
        '首页-科学普及-科普资料': 'http://www.gast.org.cn/kxpj/kpzl/index_1.html',
        '首页-组织建设-组织建设': 'http://www.gast.org.cn/zzjs/index_1.html',
        '首页-热点专题-二十大精神': 'http://www.gast.org.cn/rdzt/xxxcgcesdjs/index_1.html',
        '首页-热点专题-廉洁文化': 'http://www.gast.org.cn/rdzt/ljwh/index_1.html',
        '首页-热点专题-普法专栏': 'http://www.gast.org.cn/rdzt/pfzl/index_1.html',
        '首页-热点专题-新闻发布': 'http://www.gast.org.cn/rdzt/pfzl/index_1.html',
        '首页-热点专题-贵州反邪': 'http://www.gast.org.cn/rdzt/gzfx/index_1.html',

    }
    # supFlash = get_supflash()
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 25):
            if page == 1:
                url = value.replace('_1.html', '.html')
            else:
                url = value.replace('index_1', 'index_' + str(page-1))
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default', default='2023-01-01')
    args = parser.parse_args()
    account_name = 'F-24 贵州省科协'
    start_run(args.projectname, args.sinceyear, account_name)
