# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-02-07
import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url,):

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Mobile Safari/537.36',
    }

    response = requests.get(url, headers=headers,
                            verify=False)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        if 'special' not in news_classify:
            part1_nodes = selector.xpath('//*[@class="sub_box list"]//li')
        else:
            part1_nodes = selector.xpath('//*[@class="contentbody"]//li')
        xpath_data(part1_nodes, news_classify, science_system, mongo,
                   account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('.//p/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./a/text()')[0].strip()

        except:
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = part_nodes[i].xpath('.//p/text()')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()

        except:
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_time = part_nodes[i].xpath('.//i/text()')[0]
            else:
                news_publish_time = part_nodes[i].xpath('./div/text()')[0].strip()
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0]
                else:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0].replace('..', '')

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.jlstnet.net' + url_part2

            # print(news_page_url)
            # breakpoint()

            if 'https://mp.weixin.qq.com' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count,click_count , news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Mobile Safari/537.36',
    }
    html_response = requests.get(news_page_url, headers=headers, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="cont"]//span//text() | '
                                           '//*[@class="cont"]//p//text() | '
                                           '//*[@class="cont"]//div//text() | '
                                           )
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者：(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="cont"]//span//img/@src |'
                                        '//*[@class="cont"]//p//img/@src | '
                                        '//*[@class="cont"]//div//img/@src |'
                                        '//*[@class="cont"]//input/@src |'
                                        '//*[@class="cont"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="cont"]//span//video/@src |'
                                         ' //*[@class="cont"]//p//video/@src |'
                                         ' //*[@class="cont"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源：(.*?)</i', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-科普天地-前沿科技': 'http://www.jlstnet.net/science/list-29-1.html',
        '首页-科普天地-乡村E站': 'http://www.jlstnet.net/science/list-30-1.html',
        '首页-科普天地-青少年科技': 'http://www.jlstnet.net/science/list-36-1.html',
        '首页-科普天地-健康养生': 'http://www.jlstnet.net/science/list-31-1.html',
        '首页-科普天地-军事科技': 'http://www.jlstnet.net/science/list-37-1.html',
        '首页-科普天地-释疑解惑': 'http://www.jlstnet.net/science/list-38-1.html',
        '首页-科普天地-智慧女性': 'http://www.jlstnet.net/science/list-39-1.html',
        '首页-科普天地-科幻空间': 'http://www.jlstnet.net/science/list-77-1.html',
        '首页-科普天地-欣赏品读': 'http://www.jlstnet.net/science/list-78-1.html',
        '首页-科技工作者之家-法律法规': 'http://www.jlstnet.net/worker/list-54-1.html',
        '首页-科技工作者之家-政策科技': 'http://www.jlstnet.net/worker/list-53-1.html',
        '首页-科技工作者之家-人才政策': 'http://www.jlstnet.net/worker/list-72-1.html',
        '首页-科技工作者之家-创业创新': 'http://www.jlstnet.net/worker/list-73-1.html',
        '首页-科技工作者之家-社会保障政策': 'http://www.jlstnet.net/worker/list-74-1.html',
        '首页-科技工作者之家-社会农业政策': 'http://www.jlstnet.net/worker/list-75-1.html',
        '首页-科技工作者之家-以案说法': 'http://www.jlstnet.net/worker/list-76-1.html',
        '首页-科技工作者之家-道德学风建设': 'http://www.jlstnet.net/worker/list-56-1.html',
        '首页-科技工作者之家-考试及调查问卷系统': 'http://www.jlstnet.net/worker/list-58-1.html',
        '首页-创新驱动-学术交流': 'http://www.jlstnet.net/innovate/list-3-1.html',
        '首页-创新驱动-学会管理': 'http://www.jlstnet.net/innovate/list-64-1.html',
        '首页-创新驱动-乡村振兴专家服务栏目': 'http://www.jlstnet.net/innovate/list-4-1.html',
        '首页-创新驱动-青年科技人才托举工程': 'http://www.jlstnet.net/innovate/list-5-1.html',
        '首页-创新驱动-双创政策及解读': 'http://www.jlstnet.net/innovate/list-67-1.html',
        '首页-创新驱动-双创人物': 'http://www.jlstnet.net/innovate/list-68-1.html',
        '首页-创新驱动-产学研协同创新服务平台': 'http://www.jlstnet.net/innovate/list-69-1.html',
        '首页-智库建设-咨询要闻': 'http://www.jlstnet.net/build/list-41-1.html',
        '首页-智库建设-科技智库': 'http://www.jlstnet.net/build/list-42-1.html',
        '首页-智库建设-科技工作者状况调查': 'http://www.jlstnet.net/build/list-51-1.html',
        '首页-智库建设-咨询建议': 'http://www.jlstnet.net/build/list-50-1.html',
        '首页-党建工作-工作动态': 'http://www.jlstnet.net/party/list-17-1.html',
        '首页-党建工作-通知公告': 'http://www.jlstnet.net/party/list-16-1.html',
        '首页-党建工作-学习园地': 'http://www.jlstnet.net/party/list-14-1.html',
        '首页-党建工作-党章党规': 'http://www.jlstnet.net/party/list-15-1.html',
        '首页-党建工作-学习黄大年精神': 'http://www.jlstnet.net/party/list-142-1.html',
        '首页-专题专栏-活动专栏': 'http://www.jlstnet.net/special/list-27-1.html',
        '首页-专题专栏-媒体撷英': 'http://www.jlstnet.net/special/list-26-1.html',
        '首页-专题专栏-信息传播站': 'http://www.jlstnet.net/special/list-141-1.html',
        '首页-专题专栏-吉林省科学技术协会第十次代表大会': 'http://www.jlstnet.net/special/list-164-1.html',
        '首页-通知公告': 'http://www.jlstnet.net/party/list-16-1.html',
        '首页-弘扬科学家精神 涵养优良学风': 'http://www.jlstnet.net/news/list-101-1.html',
        '首页-省科协动态': 'http://www.jlstnet.net/news/list-100-1.html',
        '首页-全国科协动态': 'http://www.jlstnet.net/news/list-88-1.html',
        '首页-科技社团动态': 'http://www.jlstnet.net/news/list-89-1.html',
        '首页-市县科协动': 'http://www.jlstnet.net/news/list-90-1.html',
        '首页-视听画-视': 'http://www.jlstnet.net/video_paint/list-105-1.html',
        '首页-视听画-听': 'http://www.jlstnet.net/video_paint/list-106-1.html',
        '首页-视听画-画': 'http://www.jlstnet.net/video_paint/list-107-1.html',

    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            url = value.replace('-1.html', '-' + str(page) + '.html')
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)
            time.sleep(1)
            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'F-07 吉林省科协'
    start_run(args.projectname, args.sinceyear, account_name)
