# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-04-17

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import urllib3
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, page):
    pathId = re.findall(r'\d+', url)[0]
    headers = {
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/json',
        'Origin': 'https://www.snast.org.cn',
        'Referer': 'https://www.snast.org.cn/press-center',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
        'sec-ch-ua': '"Chromium";v="110", "Not A(Brand";v="24", "Google Chrome";v="110"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'tenantId': '1443413620536135680',
    }

    pid = {
        "1446647500584259584": "'1446647500584259584','1509063612701347840','1509063685967450112',"
                               "'1509063745207799808','1509063830574469120','1509063903207231488',"
                               "'1509063957271810048','1569519566093553664','1625757212020838400'",
        "1446647556435611648": "'1446647556435611648','1509065066040266752','1509065125649715200',"
                               "'1509065169576660992','1509065230821888000','1509065521692676096',"
                               "'1509065574700290048','1509065631554080768','1509065717080133632',"
                               "'1509065779231330304','1509065829726556160','1509065878279819264',"
                               "'1509065946516951040','1509065993291829248','1509066047914250240',"
                               "'1569519688277823488'",
        "1446647587251163136": "'1446647587251163136','1509066682587942912','1509066725642473472',"
                               "'1509066780424278016','1509066828843323392','1509066906546999296',"
                               "'1509066963350458368','1509067025300328448','1509067075531313152',"
                               "'1509067123207966720','1509067172495233024','1509067218221535232',"
                               "'1509067291386974208','1509067341240471552','1569519956738445312'",

        "1446647662387924992": "'1446647662387924992','1509070952532021248','1509071001433411584',"
                               "'1509071061659422720','1509071105812860928','1509071205381443584',"
                               "'1509071264286248960','1509071304236994560','1509071348776308736',"
                               "'1509071395035287552','1509071434180726784','1509071499884498944',"
                               "'1509071557434544128'",
        "1446652821750943744": "'1446652821750943744','1509069103850590208','1509069215490379776',"
                               "'1509069260222631936','1509069304334127104','1509069351956254720',"
                               "'1509069416737280000','1509069497087561728','1509069574636048384',"
                               "'1509069625915609088','1509069672652738560','1509069716130893824',"
                               "'1569520064817270784'",
        "1448115342424936448": "1448115342424936448",
    }
    try:
        typeIds = pid.get(pathId)
    except Exception as e:
        logging.warning('TypeIds Was Error :{}'.format(e))
        typeIds = pathId

    data = {
        "pageSize": "8",
        "pindex": page,
        "typeIds": typeIds,
    }
    data = json.dumps(data)
    response = requests.post('https://www.snast.org.cn/st365/content/viewList', headers=headers, data=data)

    response.encoding = 'utf-8'
    status_code = response.status_code
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    try:
        if 'special' in news_classify:
            html_text = re.findall(r'callback\((.*?)\)', html_text)[0]
            content = json.loads(html_text)
            ree_data(content, news_classify, science_system, mongo,
                     account_name, project_time, start_time)
        else:
            content = json.loads(html_text)
            deadline = ree_data(content, news_classify, science_system, mongo,
                                account_name, project_time, start_time)
            if deadline:
                return deadline

    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
             account_name, project_time, start_time):
    results = content['data']['records']

    news_dict_name = [
        'news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
        'news_content_type', 'news_content', 'news_page_url', 'source',
        'news_author', 'read_count', 'click_count', 'news_classify',
        'crawl_time', 'account_name', 'science_system', 'project_time'
    ]

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for item in results:
        try:
            news_title = item.get('title').strip()
        except:
            news_title = ''

        try:
            news_abstract = item.get('detail').strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = item.get('publicDate').split(' ')[0]
        except Exception as e:
            logging.warning('Publish Time Was Error: {}'.format(e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            id = item.get('id')
            news_page_url = 'https://www.snast.org.cn/newsDetail?id={}'.format(id)
            news_api_url = 'https://www.snast.org.cn/st365/contentMould/viewDetail?id={}'.format(id)

            if 'mp.weixin.qq.co' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_api_url, id)
            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_api_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title[:20], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_api_url, id):

    headers = {
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': news_api_url,
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
        'sec-ch-ua': '"Chromium";v="110", "Not A(Brand";v="24", "Google Chrome";v="110"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'tenantId': '1443413620536135680',
    }

    params = (
        ('id', id),
    )

    html_response = requests.get('https://www.snast.org.cn/st365/contentMould/viewDetail',
                                 headers=headers, params=params)
    html_response.encoding = 'utf-8'
    content_text = html_response.json().get('data')
    # print(content_text['result']['article'])
    # breakpoint()
    selector_page = etree.HTML(html_response.text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(html_response)
    except:
        result = ''

    try:
        # text = re.findall(r'[\u4e00-\u9fa5]', content_text['result']['article']['content'])
        text = re.findall(r'[\u3002\uff1b\uff0c\uff1a\u201c\u201d\uff08\uff09\u3001\uff1f\u300a\u300b\u4e00-\u9fa5]',

                          content_text['detail'])
        news_content = ''.join([x.strip() for x in text])
    except:
        news_content = ''

    try:
        news_author = content_text['author']
    except:
        news_author = ''

    try:
        news_imgs = re.findall(r'src="(.*?)"', str(content_text['detail']))[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="xq_con"]//span//video/@src |'
                                         ' //*[@class="xq_con"]//p//video/@src |'
                                         ' //*[@class="xq_con"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = content_text['publicName']
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击数：(.*?) ', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻中心-科协要闻': 'https://www.snast.org.cn/press-center?pathId=1446647500584259584',
        '首页-新闻中心-新闻头条': 'https://www.snast.org.cn/press-center?pathId=1446647556435611648',
        '首页-新闻中心-工作动态': 'https://www.snast.org.cn/press-center?pathId=1446647587251163136',
        '首页-新闻中心-省级学会': 'https://www.snast.org.cn/press-center?pathId=1448115342424936448',
        '首页-新闻中心-市区科协': 'https://www.snast.org.cn/press-center?pathId=1446647662387924992',
        '首页-信息公开-通知公告': 'https://www.snast.org.cn/infor-disclosure?pathId=1446652821750943744',
        '首页-科协党建-机关党建': 'https://www.snast.org.cn/infor-disclosure?pathId=1446653382873321472',
        '首页-学术-学术交流': 'https://www.snast.org.cn/infor-disclosure?pathId=1456432193156419584',
        '首页-学术-科技交流与合作': 'https://www.snast.org.cn/infor-disclosure?pathId=1456432231555272704',
        '首页-学术-学会管理与改革': 'https://www.snast.org.cn/infor-disclosure?pathId=1456432267164913664',
        '首页-学术-科技经济融合行动': 'https://www.snast.org.cn/infor-disclosure?pathId=1456432316221493248',
        '首页-科普-品牌科普活动': 'https://www.snast.org.cn/infor-disclosure?pathId=1456432399365181440',
        '首页-科普-科普展教': 'https://www.snast.org.cn/infor-disclosure?pathId=1456432443619282944',
        '首页-科普-基层科普': 'https://www.snast.org.cn/infor-disclosure?pathId=1456432493355339776',
        '首页-智库-智惠三秦': 'https://www.snast.org.cn/infor-disclosure?pathId=1450343747451097088',
        '首页-智库-决策咨询建议': 'https://www.snast.org.cn/infor-disclosure?pathId=1450343814077616128',
        '首页-智库-科技工作者状况调查': 'https://www.snast.org.cn/infor-disclosure?pathId=1450343876233007104',
        '首页-热点专题-党史学习教育': 'https://www.snast.org.cn/infor-disclosure?pathId=1466282101061455872',
        '首页-热点专题-全国科学家精神教育基地': 'https://www.snast.org.cn/infor-disclosure?pathId=1582666659238776832',
        '首页-热点专题-科协10年优秀工作案例': 'https://www.snast.org.cn/infor-disclosure?pathId=1583007118121242624',
        '首页-热点专题-国家网络安全': 'https://www.snast.org.cn/infor-disclosure?pathId=1567341744373633024',
        '首页-热点专题-美好生活民法典': 'https://www.snast.org.cn/infor-disclosure?pathId=1528994801943842816',
        '首页-热点专题-5.30全国科技工作者日': 'https://www.snast.org.cn/infor-disclosure?pathId=1528986935115649024',
        '首页-热点专题-普法宣传': 'https://www.snast.org.cn/infor-disclosure?pathId=1589520977258614784',
        '首页-热点专题-作风建设永远在路上': 'https://www.snast.org.cn/infor-disclosure?pathId=1574263379643076608',

    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 100):
            html_text,status_code = get_html(value, page)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'F-26 陕西省科协'
    start_run(args.projectname, args.sinceyear, account_name)
