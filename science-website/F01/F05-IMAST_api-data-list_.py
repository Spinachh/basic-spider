# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-02-07
import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):

    try:
        if 'special' in news_classify:
            html_text = re.findall(r'callback\((.*?)\)', html_text)[0]
            content = json.loads(html_text)
            ree_data(content, news_classify, science_system, mongo,
                     account_name, project_time, start_time)
        else:
            selector = etree.HTML(html_text)
            part1_nodes = selector.xpath('//*[@class="kx-content"]//li')
            xpath_data(part1_nodes, news_classify, science_system, mongo,
                   account_name, project_time, start_time)

    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
             account_name, project_time, start_time):
    results = content['data']['list']
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for item in results:
        try:
            news_title = item.get('docTitle')
        except:
            news_title = ''
        try:
            news_abstract = item.get('docAbstract').strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = item.get('docReltime').split(' ')[0]
        except Exception as e:
            logging.warning('Publish Time Was Error: {}'.format(e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_publish_time)
        # print(news_publish_stamp)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            news_page_url = item.get('docPuburl')

            if 'https://mp.weixin.qq.com' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_page_url)



            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if '要闻动态' in news_classify:
                news_title = part_nodes[i].xpath('.//a/p[1]/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./a/@title')[0].strip()
        except Exception as e:
            logging.warning('Title Was Error: {}'.format(e))
            news_title = ''
        try:
            if '要闻动态' in news_classify:
                news_abstract = part_nodes[i].xpath('.//a/p[2]/text()')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()

        except:
            news_abstract = ''

        try:
            if '要闻动态' in news_classify:
                news_publish_d = part_nodes[i].xpath('.//h3/text()')[0]
                news_publish_y = part_nodes[i].xpath('.//h2/text()')[0]
                news_publish_time = news_publish_y + '-' + news_publish_d
            else:
                news_publish_time = part_nodes[i].xpath('./time/text()')[0].strip()
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if '要闻动态' not in news_classify:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0]
                else:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0]

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.imast.org.cn' + url_part2

            if 'https://mp.weixin.qq.com' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''
    try:
        news_content = selector_page.xpath('//*[@class="details-page"]//span//text() | '
                                           '//*[@class="details-page"]//p//text() | '
                                           '//*[@class="details-page"]//div//text() | '
                                           )
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者：(.*?) ', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="details-page"]//span//img/@src |'
                                        '//*[@class="details-page"]//p//img/@src | '
                                        '//*[@class="details-page"]//div//img/@src |'
                                        '//*[@class="details-page"]//img/@src')[0]
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="details-page"]//span//video/@src |'
                                         ' //*[@class="details-page"]//p//video/@src |'
                                         ' //*[@class="details-page"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip().split('来源')[-1]
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        if '.shtml' in news_page_url:
            ID = news_page_url.split('/')[-1].split('.')[0]
            click_url = 'https://www.imast.org.cn//zcms/front/counter?Type=Article&ID={}&DomID=hitcount{}'\
                .format(ID, ID)
            # https://www.imast.org.cn//zcms/front/counter?Type=Article&ID=22621&DomID=hitcount22621
            click_content = requests.get(click_url).text
            click_count = re.findall(r'innerHTML=(.*?);', click_content)[0]
        else:
            click_count = ''
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-要闻动态-时政要闻-special': 'https://www.nmg.gov.cn/portal/open/getDocumentList.do?docChannel=3197&pageNum=1&pageSize=7&callback=callback',
        '首页-要闻动态-科协要闻': 'https://www.imast.org.cn/ywdt/kxyw/index_1.shtml',
        '首页-要闻动态-领导动态': 'https://www.imast.org.cn/ywdt/lddt/index_1.shtml',
        '首页-要闻动态-重要文章': 'https://www.imast.org.cn/ywdt/zywz/index_1.shtml',
        '首页-要闻动态-盟市科协': 'https://www.imast.org.cn/ywdt/mskx/index_1.shtml',
        '首页-科协普及-科普活动': 'https://www.imast.org.cn/kxpj/kphd/index_1.shtml',
        '首页-科协普及-科技先锋': 'https://www.imast.org.cn/kxpj/kjxf/index_1.shtml',
        '首页-科协普及-科技竞赛': 'https://www.imast.org.cn/kxpj/kjjs/index_1.shtml',
        '首页-科协普及-科普知识': 'https://www.imast.org.cn/kxpj/kpzx/index_1.shtml',
        '首页-学会学术-学会动态': 'https://www.imast.org.cn/xsxh/xhdt/index_1.shtml',
        '首页-学会学术-学会指南': 'https://www.imast.org.cn/xsxh/xhzn/index_1.shtml',
        '首页-学会学术-智库人才': 'https://www.imast.org.cn/xsxh/zkrc/rwfc/index_1.shtml',
        '首页-信息公开-通知': 'https://www.imast.org.cn/xxgk/tzgg/tz/index_1.shtml',
        '首页-信息公开-公告': 'https://www.imast.org.cn/xxgk/tzgg/gg/index_1.shtml',
        '首页-信息公开-科技政策': 'https://www.imast.org.cn/xxgk/kjzc/index_1.shtml',
        '首页-信息公开-法律法规': 'https://www.imast.org.cn/xxgk/fgzd/index_1.shtml',
        '首页-信息公开-财务公开': 'https://www.imast.org.cn/xxgk/cwgk/index_1.shtml',
        '首页-机关党建-思想建设': 'https://www.imast.org.cn/jgdj/sxjs/index_1.shtml',
        '首页-机关党建-廉政建设': 'https://www.imast.org.cn/jgdj/lzjs/index_1.shtml',
        '首页-机关党建-党建活动': 'https://www.imast.org.cn/jgdj/djhd/index_1.shtml',
        '首页-机关党建-离休干部工作': 'https://www.imast.org.cn/jgdj/lgbgz/index_1.shtml',
    }
    for key, value in urls.items():
        news_classify = key

        for page in range(1, 11):
            if 'special' in news_classify:
                channel = re.findall(r'docChannel=(.*?)&', value)[0]
                url = value.replace('pageNum=1', 'pageNum=' + str(page))
            else:
                if page == 1:
                    url = value.replace('index_1.shtml', 'index.shtml')
                else:
                    url = value.replace('index_1.shtml', 'index_' + str(page) + '.shtml')

            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            try:
                get_data(html_text, news_classify, account_name,
                         science_system, mongo, project_time, start_time)
            except Exception as e:
                logging.warning('Page {} Get Data Was Error:{}'.format(page, e))
                break

            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'F-05 内蒙古科协'
    start_run(args.projectname, args.sinceyear, account_name)
    """
    盟市科协，页数较多，该网站页数从11页开始，url结构又有变化
    盟市科协：https://www.imast.org.cn//zcms/ui/catalog/42/pc/index_11.shtml
    """