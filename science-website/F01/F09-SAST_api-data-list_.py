# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-02-08
import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, page):
    did = url.split('=')[-1]

    cookies = {
        'Hm_lvt_751dd404d549b24c7842cf599b4bd396': '1675404328',
        'Hm_lpvt_751dd404d549b24c7842cf599b4bd396': '1675410053',
    }

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Mobile Safari/537.36',
        'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
        'sec-ch-ua-mobile': '?1',
        'sec-ch-ua-platform': '"Android"',
    }
    params = (
        ('id', did),
        ('pageNo', page),
        ('pageSize', '20'),
        ('getFirstChildList', ''),
        ('getChildCol', ''),
    )

    response = requests.get('https://www.sast.gov.cn/api/article/list',
                            headers=headers, params=params, cookies=cookies, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):

    try:
        if 'special' in news_classify:
            html_text = re.findall(r'callback\((.*?)\)', html_text)[0]
            content = json.loads(html_text)
            ree_data(content, news_classify, science_system, mongo,
                     account_name, project_time, start_time)
        else:
            content = json.loads(html_text)
            ree_data(content, news_classify, science_system, mongo,
                   account_name, project_time, start_time)

    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
             account_name, project_time, start_time):
    results = content['result']['articles']
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for item in results:
        try:
            news_title = item.get('title').strip()
        except:
            news_title = ''
        try:
            news_abstract = item.get('title').strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = item.get('createTime').split(' ')[0]
        except Exception as e:
            logging.warning('Publish Time Was Error: {}'.format(e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            columnId = item.get('types')
            did = item.get('id')
            news_page_url = 'https://www.sast.gov.cn/content.html?id={}&cid={}'.format(did, columnId)
            news_api_url = 'https://www.sast.gov.cn/api/article/queryById?id={}&columnId={}'.format(did, columnId)

            if 'https://mp.weixin.qq.com' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_page_content(news_api_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_api_url)



            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_api_url):

    html_response = requests.get(news_api_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.json()
    # print(content_text['result']['article'])
    # breakpoint()
    selector_page = etree.HTML(html_response.text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(html_response)
    except:
        result = ''

    try:
        # text = re.findall(r'[\u4e00-\u9fa5]', content_text['result']['article']['content'])
        text = re.findall(r'[\u3002\uff1b\uff0c\uff1a\u201c\u201d\uff08\uff09\u3001\uff1f\u300a\u300b\u4e00-\u9fa5]',

                          content_text['result']['article']['content'])
        news_content = ''.join([x.strip() for x in text])
    except:
        news_content = ''
    try:
        news_author = re.findall(r'作者：(.*?) ', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = re.findall(r'src="(.*?)"', str(content_text['result']['article']))[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="xq_con"]//span//video/@src |'
                                         ' //*[@class="xq_con"]//p//video/@src |'
                                         ' //*[@class="xq_con"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源：(.*?) ', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击数：(.*?) ', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻-通知公告': 'https://www.sast.gov.cn/list.html?id=1381464132464926722',
        '首页-新闻-学会动态': 'https://www.sast.gov.cn/list.html?id=109',
        '首页-新闻-媒体报道': 'https://www.sast.gov.cn/list.html?id=185',
        '首页-新闻-科协动态': 'https://www.sast.gov.cn/list.html?id=36',
        '首页-新闻-基层动态': 'https://www.sast.gov.cn/list.html?id=37',
        '首页-新闻-老干部风采': 'https://www.sast.gov.cn/list.html?id=504',
        '首页-新闻-相关资料': 'https://www.sast.gov.cn/list.html?id=1372366026574102530',
        '首页-学术-学术交流': 'https://www.sast.gov.cn/list.html?id=43',
        '首页-学术-上海科坛': 'https://www.sast.gov.cn/list.html?id=1369206150775570433',
        '首页-学术-通知公告': 'https://www.sast.gov.cn/list.html?id=1369180671829999617',
        '首页-学术-学风建设': 'https://www.sast.gov.cn/list.html?id=1369158857326198785',
        '首页-学术-科技会客厅': 'https://www.sast.gov.cn/list.html?id=1369158714665336834',
        '首页-科普-全民科学素质教育': 'https://www.sast.gov.cn/list.html?id=58',
        '首页-科普-全国科普日上海地区': 'https://www.sast.gov.cn/list.html?id=51',
        '首页-科普-通知公告': 'https://www.sast.gov.cn/list.html?id=1369180843167318018',
        '首页-科普-科普资源库': 'https://www.sast.gov.cn/list.html?id=1369163130583777282',
        '首页-科普-社区书院': 'https://www.sast.gov.cn/list.html?id=1369162956604047362',
        '首页-科普-青少年科技教育': 'https://www.sast.gov.cn/list.html?id=1369162869828091905',
        '首页-创新-相关文件': 'https://www.sast.gov.cn/list.html?id=1369178030274117633',
        '首页-创新-通知公告': 'https://www.sast.gov.cn/list.html?id=1369181004761268226',
        '首页-创新-企业科协组织': 'https://www.sast.gov.cn/list.html?id=358',
        '首页-创新-院士专家工作站': 'https://www.sast.gov.cn/list.html?id=359',
        '首页-创新-科技评价': 'https://www.sast.gov.cn/list.html?id=360',
        '首页-人才-上海市科技精英': 'https://www.sast.gov.cn/list.html?id=121',
        '首页-人才-成长服务': 'https://www.sast.gov.cn/list.html?id=1369178231885922306',
        '首页-人才-文化建设': 'https://www.sast.gov.cn/list.html?id=1369178446147747842',
        '首页-人才-法律服务': 'https://www.sast.gov.cn/list.html?id=1369178527190089729',
        '首页-人才-最美科技工作者': 'https://www.sast.gov.cn/list.html?id=1369178714948108290',
        '首页-人才-通知公告': 'https://www.sast.gov.cn/list.html?id=1369181041432068097',
        '首页-人才-上海青年科技英才': 'https://www.sast.gov.cn/list.html?id=1369483217282924546',
        '首页-智库-专家观点': 'https://www.sast.gov.cn/list.html?id=1369178854425493505',
        '首页-智库-智库成果': 'https://www.sast.gov.cn/list.html?id=1369178999611326466',
        '首页-智库-通知公告': 'https://www.sast.gov.cn/list.html?id=1369181085111549954',
        '首页-智库-参政议政': 'https://www.sast.gov.cn/list.html?id=98',
        '首页-调查与研究': 'https://www.sast.gov.cn/list.html?id=100',
        '首页-合作交流': 'https://www.sast.gov.cn/list.html?id=101',
        '首页-创新': 'https://www.sast.gov.cn/list.html?id=1369121378527215617',
        '首页-新闻': 'https://www.sast.gov.cn/list.html?id=1369121551382872066',

    }
    for key, value in urls.items():
        news_classify = key

        for page in range(1, 11):
            if 'special' in news_classify:
                url = value.replace('pageNum=1', 'pageNum=' + str(page))
            else:
                url = value
            html_text = get_html(url, page)
            # print(html_text)
            # breakpoint()
            try:
                get_data(html_text, news_classify, account_name,
                         science_system, mongo, project_time, start_time)
            except Exception as e:
                logging.warning('Page {} Get Data Was Error:{}'.format(page, e))
                break

            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'F-09 上海市科协'
    start_run(args.projectname, args.sinceyear, account_name)
