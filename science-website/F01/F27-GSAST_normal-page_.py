# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-02-28

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash():
    url = 'http://www.gxast.org.cn/kxxw/ttyw/p-1.html'
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.gxast.org.cn/kxxw/ttyw/p-1.html',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, verify=False)
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url):

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, verify=False)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        if 'special' not in news_classify:
            part1_nodes = selector.xpath('//*[@class="newsList"]/li')
        else:
            part1_nodes = selector.xpath('//*[@class="w945"]/div')
        xpath_data(part1_nodes, news_classify, science_system, mongo,
                   account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('./a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./h1/a/text()')[0].strip()
        except:
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('./h1/a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_time = part_nodes[i].xpath('./span/text()')[0].strip().replace('.', '-')
            else:
                news_publish_time = part_nodes[i].xpath('./div[2]/div/text()')[0].split(' ')[0]
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('./a/@href')[0].strip()
                else:
                    url_part2 = part_nodes[i].xpath('./div[2]/a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.gsast.org.cn' + url_part2

            if 'mp.weixin.qq.co' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    try:
        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,'
                      '*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Cache-Control': 'max-age=0',
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                          'Chrome/110.0.0.0 Safari/537.36',
        }

        html_response = requests.get('http://www.gsast.org.cn/kxyw/content_130351',
                                     headers=headers,
                                     verify=False)
        html_response.encoding = 'utf-8'
        content_text = html_response.text
    except Exception as e:
        logging.warning('Article Content Was Error :{}'.format(e))
        content_text = ''

    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath(
            '//*[@class="conTxt"]//p//text() |'
            '//*[@class="conTxt"]/p/span//text() '
        )
        news_content = ''.join([x.strip() for x in news_content])
    except Exception as e:
        logging.warning("Article Content Xpath Was Error: {}".format(e))
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者：(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="conTxt"]//p//img/@src |'
                                        '//*[@class="conTxt"]//span//img/@src | '
                                        '//*[@class="conTxt"]//div//img/@src |'
                                        '//*[@class="conTxt"]//input/@src |'
                                        '//*[@class="conTxt"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="conTxt"]//span//video/@src |'
                                         ' //*[@class="conTxt"]//p//video/@src |'
                                         ' //*[@class="conTxt"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r"来源： (.*?)</", content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        # 'http://www.gsast.org.cn/kxyw/content_130351'
        article_id = news_page_url.split('_')[-1]

        cookies = {
            'edr_session_verify': '4fe593a9d541866b0cc96831ba8561e4',
            '__RequestVerificationToken': 'CMtIzdjPooMCATnvPUdDQG2Y7lRRb3c-09843_lQn3IbiWECWo3_y_h60oZ0FvvaUJzBSULUgIOplQXp4IWPIplZSiqqV-Hq45IeRIiFtNw1',
            'PowerUniqueVisitor': '728dca5d-9984-44a5-815f-998b0713ce6c_02%2F16%2F2023%2000%3A00%3A00',
        }

        headers = {
            'Accept': '*/*',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Connection': 'keep-alive',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Origin': 'http://www.gsast.org.cn',
            'Referer': news_page_url,
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                          'Chrome/110.0.0.0 Safari/537.36',
            'X-Requested-With': 'XMLHttpRequest',
        }

        data = {
            '__requestverificationtoken': 'p2MjnaM7W-TotF97XK5pPULYZtHO3YU_P6hs7fprnKrQ6_t8pvFzb4GMYUZ-laq89hrUjlIx_9Gcn_Zin0JsP6p6UiXOtRmutwF05y7Axps1',
            'mold': 'article',
            'id': article_id,
            'isShowHit': 'true'
        }

        response = requests.post('http://www.gsast.org.cn/contentmanage/ajax/getandupdate', headers=headers,
                                 cookies=cookies, data=data, verify=False).json()

        read_count = response.get('hits')

    except Exception as e:
        logging.warning('Read Count Was Error :{}'.format(e))
        read_count = ''
    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-科协要闻': 'http://www.gsast.org.cn/kxyw_',
        '首页-通知公告': 'http://www.gsast.org.cn/tzgg_',
        '首页-走进科协-甘肃省科协党建网-党建要闻': 'http://www.gsast.org.cn/zjkx/kxdj/djyw_',
        '首页-走进科协-甘肃省科协党建网-通知公告': 'http://www.gsast.org.cn/zjkx/kxdj/tzgg1_',
        '首页-走进科协-甘肃省科协党建网-党务公开': 'http://www.gsast.org.cn/zjkx/kxdj/dwgk_',
        '首页-走进科协-甘肃省科协党建网-在线学习': 'http://www.gsast.org.cn/zjkx/kxdj/zxxx_',
        '首页-走进科协-甘肃省科协党建网-专家辅导': 'http://www.gsast.org.cn/zjkx/kxdj/jcdj_',
        '首页-走进科协-甘肃省科协党建网-党务问答': 'http://www.gsast.org.cn/zjkx/kxdj/dwwd_',
        '首页-走进科协-甘肃省科协党建网-党风廉政': 'http://www.gsast.org.cn/zjkx/kxdj/dflz_',
        '首页-走进科协-甘肃省科协党建网-党章党规': 'http://www.gsast.org.cn/zjkx/kxdj/dzdg_',
        '首页-走进科协-甘肃省科协党建网-理论思想': 'http://www.gsast.org.cn/zjkx/kxdj/zcjd_',
        '首页-走进科协-甘肃省科协党建网-党员先锋': 'http://www.gsast.org.cn/zjkx/kxdj/dydw_',
        '首页-走进科协-甘肃省科协党建网-党建专题': 'http://www.gsast.org.cn/zjkx/kxdj/djzt_',
        '首页-走进科协-大事记': 'http://www.gsast.org.cn/zjkx/dsj_',
        '首页-走进科协-制度公开': 'http://www.gsast.org.cn/zjkx/zwgk/zdgk_',
        '首页-走进科协-统计信息': 'http://www.gsast.org.cn/zjkx/zwgk/tjxx_',
        '首页-走进科协-规划计划': 'http://www.gsast.org.cn/zjkx/zwgk/ghjh_',
        '首页-走进科协-项目建设': 'http://www.gsast.org.cn/zjkx/zwgk/xmjs_',
        '首页-走进科协-人事信息': 'http://www.gsast.org.cn/zjkx/zwgk/rsxx_',
        '首页-走进科协-预决算公开': 'http://www.gsast.org.cn/zjkx/zwgk/czzj_',
        '首页-走进科协-工作报告': 'http://www.gsast.org.cn/zjkx/zwgk/gzbg_',
        '首页-走进科协-政府采购': 'http://www.gsast.org.cn/zjkx/zwgk/zfcg_',
        '首页-走进科协-表彰奖励': 'http://www.gsast.org.cn/zjkx/zwgk/bzjl_',
        '首页-走进科协-科协改革': 'http://www.gsast.org.cn/zjkx/zwgk/kxgg2_',
        '首页-走进科协-科协工会': 'http://www.gsast.org.cn/zjkx/kxgh_',
        '首页-走进科协-创建文明单位': 'http://www.gsast.org.cn/zjkx/kxgh2_',
        '首页-学术学会-省级学会': 'http://www.gsast.org.cn/xsxh/sjxh_',
        '首页-学术学会-学术活动': 'http://www.gsast.org.cn/xsxh/xshd_',
        '首页-学术学会-创新助力': 'http://www.gsast.org.cn/xsxh/cxcl_',
        '首页-学术学会-青年人才': 'http://www.gsast.org.cn/xsxh/qnrc_',
        '首页-科学普及-纲要实施': 'http://www.gsast.org.cn/kxpj/gyss_',
        '首页-科学普及-信息话建设': 'http://www.gsast.org.cn/kxpj/xxhjs_',
        '首页-科学普及-科普惠民': 'http://www.gsast.org.cn/kxpj/kphm_',
        '首页-科学普及-科技馆建设': 'http://www.gsast.org.cn/kxpj/kjgjs_',
        '首页-科学普及-基层科普行动计划': 'http://www.gsast.org.cn/kxpj/jckphdjh_',
        '首页-科学普及-青少年科技活动': 'http://www.gsast.org.cn/kxpj/qsnkjhd_',
        '首页-组织宣传-人才举荐': 'http://www.gsast.org.cn/zzxc/rcjj_',
        '首页-组织宣传-科学道德和学风建设': 'http://www.gsast.org.cn/zzxc/kxdd_',
        '首页-组织宣传-院士工作站建设': 'http://www.gsast.org.cn/zzxc/ysgzzjs_',
        '首页-组织宣传-组织建设': 'http://www.gsast.org.cn/zzxc/zzjs_',
        '首页-组织宣传-智库建设': 'http://www.gsast.org.cn/zzxc/zkjs_',
        '首页-基层动态-兰州市科协-市州新闻': 'http://www.gsast.org.cn/jcdt/lzskx3/szxw1_',
        '首页-基层动态-兰州市科协-基层活动': 'http://www.gsast.org.cn/jcdt/lzskx3/jchd_',
        '首页-基层动态-兰州市科协-基层建设': 'http://www.gsast.org.cn/jcdt/lzskx3/jcjs_',
        '首页-基层动态-天水市科协-市州新闻': 'http://www.gsast.org.cn/jcdt/tsskx3/szxw2_',
        '首页-基层动态-天水市科协-基层活动': 'http://www.gsast.org.cn/jcdt/tsskx3/jchd1_',
        '首页-基层动态-天水市科协-基层建设': 'http://www.gsast.org.cn/jcdt/tsskx3/jcjs1_',

        '首页-基层动态-嘉峪关市科协-市州新闻': 'http://www.gsast.org.cn/jcdt/jygskx3/szxw3_',
        '首页-基层动态-嘉峪关市科协-基层活动': 'http://www.gsast.org.cn/jcdt/jygskx3/jchd2_',
        '首页-基层动态-嘉峪关市科协-基层建设': 'http://www.gsast.org.cn/jcdt/jygskx3/jcjs2_',

        '首页-基层动态-武威市科协-市州新闻': 'http://www.gsast.org.cn/jcdt/wwskx3/szxw4_',
        '首页-基层动态-武威市科协-基层活动': 'http://www.gsast.org.cn/jcdt/wwskx3/jchd3_',
        '首页-基层动态-武威市科协-基层建设': 'http://www.gsast.org.cn/jcdt/wwskx3/jcjs3_',

        '首页-基层动态-酒泉市科协-市州新闻': 'http://www.gsast.org.cn/jcdt/jqskx3/szxw6_',
        '首页-基层动态-酒泉市科协-基层活动': 'http://www.gsast.org.cn/jcdt/jqskx3/jchd5_',
        '首页-基层动态-酒泉市科协-基层建设': 'http://www.gsast.org.cn/jcdt/jqskx3/jcjs5_',

        '首页-基层动态-金昌市科协-市州新闻': 'http://www.gsast.org.cn/jcdt/jcskx3/szxw5_',
        '首页-基层动态-金昌市科协-基层活动': 'http://www.gsast.org.cn/jcdt/jcskx3/jchd4_',
        '首页-基层动态-金昌市科协-基层建设': 'http://www.gsast.org.cn/jcdt/jcskx3/jcjs4_',

        '首页-基层动态-张掖市科协-市州新闻': 'http://www.gsast.org.cn/jcdt/zyskx3/szxw7_',
        '首页-基层动态-张掖市科协-基层活动': 'http://www.gsast.org.cn/jcdt/zyskx3/jchd6_',
        '首页-基层动态-张掖市科协-基层建设': 'http://www.gsast.org.cn/jcdt/zyskx3/jcjs6_',

        '首页-基层动态-庆阳市科协-市州新闻': 'http://www.gsast.org.cn/jcdt/qyskx3/szxw8_',
        '首页-基层动态-庆阳市科协-基层活动': 'http://www.gsast.org.cn/jcdt/qyskx3/jchd7_',
        '首页-基层动态-庆阳市科协-基层建设': 'http://www.gsast.org.cn/jcdt/qyskx3/jcjs7_',

        '首页-基层动态-平凉市科协-市州新闻': 'http://www.gsast.org.cn/jcdt/plskx3/szxw9_',
        '首页-基层动态-平凉市科协-基层活动': 'http://www.gsast.org.cn/jcdt/plskx3/jchd8_',
        '首页-基层动态-平凉市科协-基层建设': 'http://www.gsast.org.cn/jcdt/plskx3/jcjs8_',

        '首页-基层动态-白银市科协-市州新闻': 'http://www.gsast.org.cn/jcdt/byskx3/szxw10_',
        '首页-基层动态-白银市科协-基层活动': 'http://www.gsast.org.cn/jcdt/byskx3/jchd9_',
        '首页-基层动态-白银市科协-基层建设': 'http://www.gsast.org.cn/jcdt/byskx3/jcjs9_',

        '首页-基层动态-定西市科协-市州新闻': 'http://www.gsast.org.cn/jcdt/dxskx3/szxw11_',
        '首页-基层动态-定西市科协-基层活动': 'http://www.gsast.org.cn/jcdt/dxskx3/jchd10_',
        '首页-基层动态-定西市科协-基层建设': 'http://www.gsast.org.cn/jcdt/dxskx3/jcjs10_',

        '首页-基层动态-陇南市科协-市州新闻': 'http://www.gsast.org.cn/jcdt/lnskx3/szxw12_',
        '首页-基层动态-陇南市科协-基层活动': 'http://www.gsast.org.cn/jcdt/lnskx3/jchd11_',
        '首页-基层动态-陇南市科协-基层建设': 'http://www.gsast.org.cn/jcdt/lnskx3/jcjs11_',

        '首页-基层动态-临夏州科协-市州新闻': 'http://www.gsast.org.cn/jcdt/lxzkx3/szxw13_',
        '首页-基层动态-临夏州科协-基层活动': 'http://www.gsast.org.cn/jcdt/lxzkx3/jchd12_',
        '首页-基层动态-临夏州科协-基层建设': 'http://www.gsast.org.cn/jcdt/lxzkx3/jcjs12_',

        '首页-基层动态-甘南州科协-市州新闻': 'http://www.gsast.org.cn/jcdt/gnzkx3/szxw14_',
        '首页-基层动态-甘南州科协-基层活动': 'http://www.gsast.org.cn/jcdt/gnzkx3/jchd13_',
        '首页-基层动态-甘南州科协-基层建设': 'http://www.gsast.org.cn/jcdt/gnzkx3/jcjs13_',

    }
    # supFlash = get_supflash()
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            url = value + str(page)
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'F-27 甘肃省科协'
    start_run(args.projectname, args.sinceyear, account_name)
