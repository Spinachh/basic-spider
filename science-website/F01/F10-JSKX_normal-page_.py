# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-02-09
import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        if 'special' not in news_classify:
            part1_nodes = selector.xpath('//*[@class="ywkd_more"]//tr')
        else:
            part1_nodes = selector.xpath('//*[@class="contentbody"]//li')
        xpath_data(part1_nodes, news_classify, science_system, mongo,
                   account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('.//a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./a/text()')[0].strip()

        except:
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = part_nodes[i].xpath('.//a/text()')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()

        except:
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_time = part_nodes[i].xpath('./td[3]/text()')[0]
            else:
                news_publish_time = part_nodes[i].xpath('./div/text()')[0].strip()
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0].strip()
                else:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.jskx.org.cn' + url_part2

            # print(news_page_url)
            # breakpoint()

            if 'https://mp.weixin.qq.com' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count,click_count , news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="nrys"]//span//text() | '
                                           '//*[@class="nrys"]//p//text() | '
                                           '//*[@class="nrys"]//div//text() | '
                                           )
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者：(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="nrys"]//span//img/@src |'
                                        '//*[@class="nrys"]//p//img/@src | '
                                        '//*[@class="nrys"]//div//img/@src |'
                                        '//*[@class="nrys"]//input/@src |'
                                        '//*[@class="nrys"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="nrys"]//span//video/@src |'
                                         ' //*[@class="nrys"]//p//video/@src |'
                                         ' //*[@class="nrys"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源: (.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'阅读次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻中心-要闻': 'http://www.jskx.org.cn/web/more/ywkd/',
        '首页-新闻中心-头条': 'http://www.jskx.org.cn/web/more/ttxw/',
        '首页-新闻中心-特别关注': 'http://www.jskx.org.cn/web/more/tbgz/',
        '首页-新闻中心-深化改革': 'http://www.jskx.org.cn/web/more/shgg/',
        '首页-新闻中心-通知公告': 'http://www.jskx.org.cn/web/more/tzgg/',
        '首页-新闻中心-图片新闻': 'http://www.jskx.org.cn/web/more/tpxw/',
        '首页-组织人才-工作动态': 'http://www.jskx.org.cn/web/talents/gzdt/',
        '首页-组织人才-表彰举荐': 'http://www.jskx.org.cn/web/talents/bzjj/',
        '首页-组织人才-教育培训': 'http://www.jskx.org.cn/web/talents/jypx/',
        '首页-组织人才-维权服务': 'http://www.jskx.org.cn/web/talents/wqfw/',
        '首页-组织人才-政策服务': 'http://www.jskx.org.cn/web/talents/zcfw/',
        '首页-组织人才-道德建设': 'http://www.jskx.org.cn/web/talents/ddjs/',
        '首页-组织人才-组织建设-学会党建': 'http://www.jskx.org.cn/web/zzjs/xhdj/',
        '首页-组织人才-组织建设-基层科协': 'http://www.jskx.org.cn/web/zzjs/jckx/',
        '首页-组织人才-组织建设-机关事业单位': 'http://www.jskx.org.cn/web/zzjs/jgsy/',
        '首页-宣传调研-通知公告': 'http://www.jskx.org.cn/web/dyxclist/2230/',
        '首页-宣传调研-工作动态': 'http://www.jskx.org.cn/web/dyxclist/2229/',
        '首页-宣传调研-建言献策': 'http://www.jskx.org.cn/web/dyxclist/2231/',
        '首页-宣传调研-科技英才': 'http://www.jskx.org.cn/web/dyxclist/2232/',
        '首页-科协普及-工作动态': 'http://www.jskx.org.cn/web/kepulist/43/',
        '首页-科协普及-通知公告': 'http://www.jskx.org.cn/web/kepulist/44/',
        '首页-科协普及-全民科学素质行动': 'http://www.jskx.org.cn/web/kepulist/2312/',
        '首页-科协普及-示范创建': 'http://www.jskx.org.cn/web/kepulist/45/',
        '首页-科协普及-品牌活动': 'http://www.jskx.org.cn/web/kepulist/2313/',
        '首页-对外交流-海智要闻': 'http://www.jskx.org.cn/web/commlist/2304/',
        '首页-对外交流-通知公告': 'http://www.jskx.org.cn/web/commlist/2305/',
        '首页-对外交流-港澳台交流': 'http://www.jskx.org.cn/web/commlist/51/',
        '首页-对外交流-联盟动态': 'http://www.jskx.org.cn/web/commlist/2306/',
        '首页-对外交流-海外团体': 'http://www.jskx.org.cn/web/commlist/2307/',
        '首页-党的建设-党建动态': 'http://www.jskx.org.cn/web/build/djdt/',
        '首页-党的建设-文件公告': 'http://www.jskx.org.cn/web/build/wjgg/',
        '首页-党的建设-党务知识': 'http://www.jskx.org.cn/web/build/dwzs/',
        '首页-党的建设-纪检之窗': 'http://www.jskx.org.cn/web/build/jjzc/',
        '首页-党的建设-学习园地': 'http://www.jskx.org.cn/web/build/xxyd/',
        '首页-党的建设-党建风采': 'http://www.jskx.org.cn/web/build/djfc/',
        '首页-党的建设-两学一做': 'http://www.jskx.org.cn/web/build/lxyz/',
        '首页-信息公开-预决算信息': 'http://www.jskx.org.cn/web/message/yjsxx/',
        '首页-信息公开-专项资金分配信息': 'http://www.jskx.org.cn/web/message/zjfp/',
        '首页-信息公开-政府采购信息': 'http://www.jskx.org.cn/web/message/zfcg/',

    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            url = value + str(page)
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)
            time.sleep(1)
            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'F-10 江苏省科协'
    start_run(args.projectname, args.sinceyear, account_name)
