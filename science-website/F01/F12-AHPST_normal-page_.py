# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-02-11
import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        if 'special' not in news_classify:
            part1_nodes = selector.xpath('//*[@class="m-cglist m-liststyle1 f-md-mb15"]//li')
        else:
            part1_nodes = selector.xpath('//*[@class="contentbody"]//li')
        xpath_data(part1_nodes, news_classify, science_system, mongo,
                   account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('./a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./a/text()')[0].strip()

        except:
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()

        except:
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_time = part_nodes[i].xpath('./span/text()')[0]
            else:
                news_publish_time = part_nodes[i].xpath('./div/text()')[0].strip()
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('./a/@href')[0].strip()
                else:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.ahpst.net.cn' + url_part2

            # print(news_page_url)
            # breakpoint()

            if 'https://mp.weixin.qq.com' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count,click_count , news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            logging.warning(news_dict_content)
            breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="m-dtfonts"]//span//text() | '
                                           '//*[@class="m-dtfonts"]//p//text() | '
                                           '//*[@class="m-dtfonts"]//div//text() | '
                                           )
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者：(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="m-dtfonts"]//span//img/@src |'
                                        '//*[@class="m-dtfonts"]//p//img/@src | '
                                        '//*[@class="m-dtfonts"]//div//img/@src |'
                                        '//*[@class="m-dtfonts"]//input/@src |'
                                        '//*[@class="m-dtfonts"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="m-dtfonts"]//span//video/@src |'
                                         ' //*[@class="m-dtfonts"]//p//video/@src |'
                                         ' //*[@class="m-dtfonts"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'信息来源：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        aid = re.findall(r'\d+', news_page_url)[0]
        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Cache-Control': 'max-age=0',
            'Connection': 'keep-alive',
            'Sec-Fetch-Dest': 'document',
            'Sec-Fetch-Mode': 'navigate',
            'Sec-Fetch-Site': 'same-origin',
            'Sec-Fetch-User': '?1',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
            'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
        }

        response = requests.get('https://www.ahpst.net.cn/Visit/Content/{}.js'.format(aid),
                                headers=headers).text

        read_count = re.findall(r"innerHTML = '(.*?)'", response, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-头条新闻': 'https://www.ahpst.net.cn/News/showList/148/page_1.html',
        '首页-科协要闻': 'https://www.ahpst.net.cn/News/showList/149/page_1.html',
        '首页-通知公告': 'https://www.ahpst.net.cn/News/showList/150/page_1.html',
        '首页-工作动态': 'https://www.ahpst.net.cn/News/showList/151/page_1.html',
        '首页-各级学会': 'https://www.ahpst.net.cn/News/showList/152/page_1.html',
        '首页-市县科协': 'https://www.ahpst.net.cn/News/showList/153/page_1.html',
        '首页-基层组织': 'https://www.ahpst.net.cn/News/showList/154/page_1.html',
        '首页-科技人才-先进典型': 'https://www.ahpst.net.cn/News/showList/180/page_1.html',
        '首页-科技人才-科学家精神': 'https://www.ahpst.net.cn/News/showList/179/page_1.html',
        '首页-科技人才-科学传播职称': 'https://www.ahpst.net.cn/News/showList/221/page_1.html',
        '首页-科创安徽-学术活动': 'https://www.ahpst.net.cn/News/showList/182/page_1.html',
        '首页-科创安徽-科创试点': 'https://www.ahpst.net.cn/News/showList/181/page_1.html',
        '首页-科创安徽-对外交流': 'https://www.ahpst.net.cn/News/showList/183/page_1.html',
        '首页-科普安徽-科普活动': 'https://www.ahpst.net.cn/News/showList/184/page_1.html',
        '首页-科普安徽-示范引领': 'https://www.ahpst.net.cn/News/showList/185/page_1.html',
        '首页-科普安徽-科普资源-原创': 'https://www.ahpst.net.cn/News/showList/193/page_1.html',
        '首页-科普安徽-科普资源-科普头条': 'https://www.ahpst.net.cn/News/showList/194/page_1.html',
        '首页-科普安徽-科普资源-专题科普': 'https://www.ahpst.net.cn/News/showList/195/page_1.html',
        '首页-科普安徽-科普资源-科普V视': 'https://www.ahpst.net.cn/News/showList/196/page_1.html',
        '首页-科普安徽-科普资源-科学辟谣': 'https://www.ahpst.net.cn/News/showList/197/page_1.html',
        '首页-决策咨询-咨询活动': 'https://www.ahpst.net.cn/News/showList/187/page_1.html',
        '首页-决策咨询-科技政策': 'https://www.ahpst.net.cn/News/showList/186/page_1.html',
        '首页-党的建设-党建动态': 'https://www.ahpst.net.cn/News/showList/141/page_1.html',
        '首页-党的建设-党纪党规': 'https://www.ahpst.net.cn/News/showList/142/page_1.html',
        '首页-党的建设-反腐倡廉': 'https://www.ahpst.net.cn/News/showList/143/page_1.html',
        '首页-政务公开': 'https://www.ahpst.net.cn/News/showList/134/page_1.html',

    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            url = value.replace('page_1.html', 'page_' + str(page) + '.html')
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)
            time.sleep(1)
            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'F-12 安徽省科协'
    start_run(args.projectname, args.sinceyear, account_name)
