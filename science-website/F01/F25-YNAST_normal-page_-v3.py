# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-07-10

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content_new
    return collection


def get_supflash():
    url = 'http://www.gxast.org.cn/kxxw/ttyw/p-1.html'
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.gxast.org.cn/kxxw/ttyw/p-1.html',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, verify=False)
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url):
    response = requests.get(url, verify=False)
    response.encoding = 'utf-8'
    status_code = response.status_code
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        if 'special' not in news_classify:
            part1_nodes = selector.xpath('//*[@class="idx_list"]/li')
        else:
            part1_nodes = selector.xpath('//*[@class="w945"]/div')
        deadline = xpath_data(part1_nodes, news_classify, science_system, mongo,
                              account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('./a/@title')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./h1/a/text()')[0].strip()
        except:
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = part_nodes[i].xpath('./a/@title')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('./h1/a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_time = part_nodes[i].xpath('./span/text()')[0].strip().replace('.', '-')
            else:
                news_publish_time = part_nodes[i].xpath('./div[2]/div/text()')[0].split(' ')[0]
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('./a/@href')[0].strip()
                else:
                    url_part2 = part_nodes[i].xpath('./div[2]/a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.ynast.cn' + url_part2

            if 'mp.weixin.qq.com' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    try:
        html_response = requests.get(news_page_url, verify=False)
        html_response.encoding = 'utf-8'
        content_text = html_response.text
    except Exception as e:
        logging.warning('Article Content Was Error :{}'.format(e))
        content_text = ''

    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath(
            '//*[@class="editbox"]//p//text() |'
            '//*[@class="editbox"]/p/span//text() '
        )
        news_content = ''.join([x.strip() for x in news_content])
    except Exception as e:
        logging.warning("Article Content Xpath Was Error: {}".format(e))
        news_content = result.get('content')
    try:
        news_author = re.findall(r'作者：(.*?)<', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="editbox"]//p//img/@src |'
                                        '//*[@class="editbox"]//span//img/@src | '
                                        '//*[@class="editbox"]//div//img/@src |'
                                        '//*[@class="editbox"]//input/@src |'
                                        '//*[@class="editbox"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="editbox"]//span//video/@src |'
                                         ' //*[@class="editbox"]//p//video/@src |'
                                         ' //*[@class="editbox"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r" var wzly='(.*?)if", content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        # 'http://www.ynast.cn/site/yunast/tbbd/info/2022/de9ca74e-26b5-4943-9611-8895887838e0.html'
        article_id = news_page_url.split('/')[-1].split('.')[0]

        cookies = {
            'JSESSIONID': '2C8C631AA97DA59CB957C798D03F4FDE',
        }

        headers = {
            'Accept': 'text/plain, */*; q=0.01',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Connection': 'keep-alive',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Origin': 'http://www.ynast.cn',
            'Referer': news_page_url,
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36',
            'X-Requested-With': 'XMLHttpRequest',
        }

        data = {
            'id': article_id,
        }

        response = requests.post('http://www.ynast.cn/infoAjaxClick.do',
                                 headers=headers,
                                 data=data,
                                 verify=False).text
        read_count = response

    except Exception as e:
        logging.warning('Read Count Was Error :{}'.format(e))
        read_count = ''
    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '科协首页-科协要闻-特别报道': 'http://www.ynast.cn/site/yunast/tbbd/index_1.html',
        '科协首页-科协要闻-省级动态': 'http://www.ynast.cn/site/yunast/sjdt/index_1.html',
        '科协首页-科协要闻-基层资讯': 'http://www.ynast.cn/site/yunast/kxywjczx/index_1.html',
        '科协首页-科协要闻-全国科协要闻': 'http://www.ynast.cn/site/yunast/qgkxyw/index_1.html',
        '科协首页-党的建设-党建学习': 'http://www.ynast.cn/site/yunast/qgkxyw/index_1.html',
        '科协首页-党的建设-党建工作': 'http://www.ynast.cn/site/yunast/qzlxgzdt/index_1.html',
        '科协首页-党的建设-群团建设': 'http://www.ynast.cn/site/yunast/ghzj/index_1.html',
        '科协首页-党的建设-强国复兴有我': 'http://www.ynast.cn/site/yunast/qgfxyw/index_1.html',
        '科协首页-人才工作-院士专家风采': 'http://www.ynast.cn/site/yunast/ysfc/index_1.html',
        '科协首页-人才工作-评先评优': 'http://www.ynast.cn/site/yunast/bzjl/index_1.html',
        '科协首页-人才工作-科学家精神': 'http://www.ynast.cn/site/yunast/kxjjs/index_1.html',
        '科协首页-人才工作-人才托举': 'http://www.ynast.cn/site/yunast/rctj/index_1.html',
        '科协首页-人才工作-培训动态': 'http://www.ynast.cn/site/yunast/pxjh/index_1.html',
        '科协首页-学术学会-学会活动': 'http://www.ynast.cn/site/yunast/xhhd/index_1.html',
        '科协首页-学术学会-学会治理': 'http://www.ynast.cn/site/yunast/xhggycxfz/index_1.html',
        '科协首页-学术学会-学会党建': 'http://www.ynast.cn/site/yunast/kjlt/index_1.html',
        '科协首页-学术学会-通知公告': 'http://www.ynast.cn/site/yunast/xhtzgg/index_1.html',
        '科协首页-科学普及-科普活动': 'http://www.ynast.cn/site/yunast/kphd/index_1.html',
        '科协首页-科学普及-科素工作': 'http://www.ynast.cn/site/yunast/ksts/index_1.html',
        '科协首页-科学普及-青少年科普': 'http://www.ynast.cn/site/yunast/qsnkp/index_1.html',
        '科协首页-科学普及-农村科普': 'http://www.ynast.cn/site/yunast/nckp/index_1.html',
        '科协首页-科学普及-农函大工作': 'http://www.ynast.cn/site/yunast/nhdgz/index_1.html',
        '科协首页-科学普及-科普大蓬车': 'http://www.ynast.cn/site/yunast/kpdpc/index_1.html',
        '科协首页-科学普及-科普百汇-科普热点': 'http://www.ynast.cn/site/yunast/kprd/index_1.html',
        '科协首页-科学普及-科普百汇-科普挂图': 'http://www.ynast.cn/site/yunast/kplt/index_1.html',
        '科协首页-对外交流-交流动态': 'http://www.ynast.cn/site/yunast/jldt/index_1.html',
        '科协首页-对外交流-合作项目': 'http://www.ynast.cn/site/yunast/hzxm/index_1.html',
        '科协首页-其他-通知公告': 'http://www.ynast.cn/site/yunast/tzgg/index_1.html',
        '科协首页-其他-预决算公开专栏': 'http://www.ynast.cn/site/yunast/yjs/index_1.html',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 200):
            if page == 1:
                url = value.replace('_1.html', '.html')
            else:
                url = value.replace('index_1', 'index_' + str(page - 1))
            html_text, status_code = get_html(url)
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q2')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default',
                        default='2023-04-01')
    args = parser.parse_args()
    account_name = 'F-25 云南省科协'
    start_run(args.projectname, args.sinceyear, account_name)
