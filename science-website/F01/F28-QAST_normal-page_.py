# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-03-01

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash():
    url = 'http://www.gxast.org.cn/kxxw/ttyw/p-1.html'
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.gxast.org.cn/kxxw/ttyw/p-1.html',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, verify=False)
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url):

    response = requests.get(url, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        if 'special' not in news_classify:
            part1_nodes = selector.xpath('//*[@class="col-md-12 col-xs-12 media-list"]/div')
        else:
            part1_nodes = selector.xpath('//*[@class="w945"]/div')
        xpath_data(part1_nodes, news_classify, science_system, mongo,
                   account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('./div[1]/a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./h1/a/text()')[0].strip()
        except:
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = part_nodes[i].xpath('./div[1]/a/text()')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('./h1/a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            read_count = part_nodes[i].xpath('./div[2]/a[3]/i/text()')[0].strip()
        except Exception as e:
            logging.warning('Read Count Was Error :{}'.format(e))
            read_count = ''

        try:
            source = part_nodes[i].xpath('./div[2]/a[2]/span/text()')[0].strip()
        except Exception as e:
            logging.warning('Source Was Error :{}'.format(e))
            source = ''

        try:
            if 'special' not in news_classify:
                news_publish_time = part_nodes[i].xpath('./div[2]/a[1]/i/text()')[0].split(' ')[0]
            else:
                news_publish_time = part_nodes[i].xpath('./div[2]/div/text()')[0].split(' ')[0]
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('./div[1]/a/@href')[0].strip()
                else:
                    url_part2 = part_nodes[i].xpath('./div[2]/a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.qast.org.cn' + url_part2

            if 'mp.weixin.qq.c' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, \
                click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    try:
        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,'
                      '*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                          'Chrome/110.0.0.0 Safari/537.36',
        }

        html_response = requests.get(news_page_url,
                                     headers=headers,
                                     verify=False)
        html_response.encoding = 'utf-8'
        content_text = html_response.text
    except Exception as e:
        logging.warning('Article Content Was Error :{}'.format(e))
        content_text = ''
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        if 'www.qast.org.cn' in news_page_url:
            news_content = selector_page.xpath(
                '//*[@class="content"]//p//text() |'
                '//*[@class="content"]/p/span//text() '
            )
            news_content = ''.join([x.strip() for x in news_content])
        else:
            news_content = result.get('content')

    except Exception as e:
        logging.warning("Article Content Xpath Was Error: {}".format(e))

    try:
        news_author = re.findall(r'作者：(.*?)</span', content_text, re.M | re.S)[0].strip()
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="content"]//p//img/@src |'
                                        '//*[@class="content"]//span//img/@src | '
                                        '//*[@class="content"]//div//img/@src |'
                                        '//*[@class="content"]//input/@src |'
                                        '//*[@class="content"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="content"]//span//video/@src |'
                                         ' //*[@class="content"]//p//video/@src |'
                                         ' //*[@class="content"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    '''
    try:
        source = re.findall(r"来源： (.*?)</", content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        # 'http://www.gsast.org.cn/kxyw/content_130351'
        article_id = news_page_url.split('_')[-1]

        cookies = {
            'edr_session_verify': '4fe593a9d541866b0cc96831ba8561e4',
            '__RequestVerificationToken': 'CMtIzdjPooMCATnvPUdDQG2Y7lRRb3c-09843_lQn3IbiWECWo3_y_h60oZ0FvvaUJzBSULUgIOplQXp4IWPIplZSiqqV-Hq45IeRIiFtNw1',
            'PowerUniqueVisitor': '728dca5d-9984-44a5-815f-998b0713ce6c_02%2F16%2F2023%2000%3A00%3A00',
        }

        headers = {
            'Accept': '*/*',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Connection': 'keep-alive',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Origin': 'http://www.gsast.org.cn',
            'Referer': news_page_url,
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                          'Chrome/110.0.0.0 Safari/537.36',
            'X-Requested-With': 'XMLHttpRequest',
        }

        data = {
            '__requestverificationtoken': 'p2MjnaM7W-TotF97XK5pPULYZtHO3YU_P6hs7fprnKrQ6_t8pvFzb4GMYUZ-laq89hrUjlIx_9Gcn_Zin0JsP6p6UiXOtRmutwF05y7Axps1',
            'mold': 'article',
            'id': article_id,
            'isShowHit': 'true'
        }

        response = requests.post('http://www.gsast.org.cn/contentmanage/ajax/getandupdate', headers=headers,
                                 cookies=cookies, data=data, verify=False).json()

        read_count = response.get('hits')

    except Exception as e:
        logging.warning('Read Count Was Error :{}'.format(e))
        read_count = ''
    '''

    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, click_count


def get_weixin_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻中心': 'http://www.qast.org.cn/channel/5d5362c01e82991550fa7d11.html?p=2',
        '首页-工作动态': 'http://www.qast.org.cn/channel/5d5362db1e82991550fa7d13.html?p=2',
        '首页-通知公告': 'http://www.qast.org.cn/channel/5d5362d01e82991550fa7d12.html?p=2',
        '首页-智库建设': 'http://www.qast.org.cn/channel/5d53630f1e82991550fa7d15.html?p=2',
        '首页-学术交流': 'http://www.qast.org.cn/channel/5d53631b1e82991550fa7d16.html?p=2',
        '首页-科学普及': 'http://www.qast.org.cn/channel/5d53637a1e82991550fa7d17.html?p=2',
        '首页-党建': 'http://www.qast.org.cn/channel/5d5363a01e82991550fa7d19.html?p=2',
        '首页-数据': 'http://www.qast.org.cn/channel/5d5363a71e82991550fa7d1a.html?p=2',
        '首页-法宣': 'http://www.qast.org.cn/channel/60f8c03f9cd3fb37166b69cc.html?p=2',
        '首页-服务': 'http://www.qast.org.cn/channel/5d5363b11e82991550fa7d1b.html?p=2',
        '首页-财务公开': 'http://www.qast.org.cn/channel/620b29d99cd3fb37166b6d50.html?p=2',

    }
    # supFlash = get_supflash()
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            if page == 1:
                url = value.replace('?p=2', '')
            else:
                url = value + str(page)
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'F-28 青海省科协'
    start_run(args.projectname, args.sinceyear, account_name)
