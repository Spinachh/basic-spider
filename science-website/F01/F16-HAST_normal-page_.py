# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-02-15
import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        if 'special' not in news_classify:
            part1_nodes = selector.xpath('//*[@class="news-list"]/div')
        else:
            part1_nodes = selector.xpath('//*[@class="contentbody"]//li')
        xpath_data(part1_nodes, news_classify, science_system, mongo,
                   account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('.//b/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./a/text()')[0].strip()

        except:
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = part_nodes[i].xpath('.//p/text()')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()

        except:
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_time = part_nodes[i].xpath('.//span[2]/text()')[0]
            else:
                news_publish_time = part_nodes[i].xpath('./div/text()')[0].strip()
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0].strip()
                else:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.hast.net.cn' + url_part2

            # print(news_page_url)
            # breakpoint()

            if 'https://mp.weixin.qq.com' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count,click_count , news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="show"]//span//text() | '
                                           '//*[@class="show"]//p//text() | '
                                           '//*[@class="show"]//div//text() | '
                                           )
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者：(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="show"]//span//img/@src |'
                                        '//*[@class="show"]//p//img/@src | '
                                        '//*[@class="show"]//div//img/@src |'
                                        '//*[@class="show"]//input/@src |'
                                        '//*[@class="show"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="show"]//span//video/@src |'
                                         ' //*[@class="show"]//p//video/@src |'
                                         ' //*[@class="show"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源:(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'阅读次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-综合信息-新闻头条': 'https://www.hast.net.cn/general/index?cid=187&page=2&per-page=15',
        '首页-综合信息-要闻聚焦': 'https://www.hast.net.cn/general/index?cid=129&page=2&per-page=15',
        '首页-综合信息-工作通知公告': 'https://www.hast.net.cn/general/index?cid=313&page=2&per-page=15',
        '首页-综合信息-项目通知公告': 'https://www.hast.net.cn/general/index?cid=314&page=2&per-page=15',
        '首页-综合信息-工作指导': 'https://www.hast.net.cn/general/index?cid=133&page=2&per-page=15',
        '首页-综合信息-科协工作动态': 'https://www.hast.net.cn/general/index?cid=188&page=2&per-page=15',
        '首页-综合信息-学会工作动态': 'https://www.hast.net.cn/general/index?cid=189&page=2&per-page=15',
        '首页-综合信息-媒体聚焦': 'https://www.hast.net.cn/general/index?cid=131&page=2&per-page=15',
        '首页-综合信息-影像科协': 'https://www.hast.net.cn/general/index?cid=132&page=2&per-page=15',
        '首页-党的建设-工作动态': 'https://www.hast.net.cn/party/index?cid=126&page=2&per-page=15',
        '首页-党的建设-从严治党': 'https://www.hast.net.cn/party/index?cid=171&page=2&per-page=15',
        '首页-党的建设-经验交流': 'https://www.hast.net.cn/party/index?cid=172&page=2&per-page=15',
        '首页-党的建设-精神文明': 'https://www.hast.net.cn/party/index?cid=173&page=2&per-page=15',
        '首页-党的建设-学习园地': 'https://www.hast.net.cn/party/index?cid=170&page=2&per-page=15',
        '首页-人才服务-工作动态': 'https://www.hast.net.cn/personnel/index?cid=123&page=2&per-page=15',
        '首页-人才服务-联络服务': 'https://www.hast.net.cn/personnel/index?cid=157&page=2&per-page=15',
        '首页-人才服务-学风建设': 'https://www.hast.net.cn/personnel/index?cid=158&page=2&per-page=15',
        '首页-组织建设-工作动态': 'https://www.hast.net.cn/organization/index?cid=207&page=2&per-page=15',
        '首页-组织建设-市县科协': 'https://www.hast.net.cn/organization/index?cid=204&page=2&per-page=15',
        '首页-组织建设-高校科协': 'https://www.hast.net.cn/organization/index?cid=205&page=2&per-page=15',
        '首页-组织建设-企业科协': 'https://www.hast.net.cn/organization/index?cid=206&page=2&per-page=15',
        # '首页-组织建设-政策法规': 'https://www.hast.net.cn/organization/index?cid=214&page=2&per-page=15',
        '首页-学会学术-综合信息': 'https://www.hast.net.cn/learning/index?cid=121&page=2&per-page=15',
        '首页-学会学术-通知公告': 'https://www.hast.net.cn/learning/index?cid=182&page=2&per-page=15',
        '首页-学会学术-一市一品产业技术发展大会': 'https://www.hast.net.cn/learning/index?cid=185&page=2&per-page=15',
        # '首页-学会学术-科创中原工程': 'https://www.hast.net.cn/learning/index?cid=183&page=2&per-page=15',
        '首页-学会学术-科技社团党建': 'https://www.hast.net.cn/learning/index?cid=321&page=2&per-page=15',
        '首页-学会学术-学术会议': 'https://www.hast.net.cn/learning/index?cid=215&page=2&per-page=15',
        '首页-学会学术-科技服务团': 'https://www.hast.net.cn/learning/index?cid=323&page=2&per-page=15',
        '首页-学会学术-学会治理与服务改革': 'https://www.hast.net.cn/learning/index?cid=144&page=2&per-page=15',
        '首页-学会学术-一企一创新': 'https://www.hast.net.cn/learning/index?cid=324&page=2&per-page=15',
        '首页-学会学术-学术与产业发展年会': 'https://www.hast.net.cn/learning/index?cid=145&page=2&per-page=15',
        '首页-学会学术-一业一会': 'https://www.hast.net.cn/learning/index?cid=147&page=2&per-page=15',
        # '首页-学会学术-计划指南': 'https://www.hast.net.cn/learning/index?cid=316&page=2&per-page=15',
        '首页-学会学术-学术引领工程': 'https://www.hast.net.cn/learning/index?cid=146&page=2&per-page=15',
        '首页-学会学术-青年人才托举工程': 'https://www.hast.net.cn/learning/index?cid=213&page=2&per-page=15',
        '首页-学会学术-自然科学学术奖': 'https://www.hast.net.cn/learning/index?cid=184&page=2&per-page=15',
        '首页-学会学术-万人助万企': 'https://www.hast.net.cn/learning/index?cid=325&page=2&per-page=15',
        '首页-科学普及-工作动态': 'https://www.hast.net.cn/universal/index?cid=119&page=2&per-page=15',
        '首页-科学普及-主题科普活动': 'https://www.hast.net.cn/universal/index?cid=135&page=2&per-page=15',
        '首页-科学普及-中原科普讲坛': 'https://www.hast.net.cn/universal/index?cid=319&page=2&per-page=15',
        '首页-科学普及-科普队伍建设': 'https://www.hast.net.cn/universal/index?cid=196&page=2&per-page=15',
        '首页-科学普及-科普信息化': 'https://www.hast.net.cn/universal/index?cid=197&page=2&per-page=15',
        '首页-科学普及-青少年科技': 'https://www.hast.net.cn/universal/index?cid=200&page=2&per-page=15',
        '首页-科学普及-科学素质建设': 'https://www.hast.net.cn/universal/index?cid=201&page=2&per-page=15',
        '首页-科学普及-农村电商人才培训': 'https://www.hast.net.cn/universal/index?cid=208&page=2&per-page=15',
        '首页-科学普及-河南省农村专业技术协会': 'https://www.hast.net.cn/universal/index?cid=221&page=2&per-page=15',
        '首页-调研宣传-工作动态': 'https://www.hast.net.cn/research/index?cid=122&page=2&per-page=15',
        '首页-调研宣传-智库建设': 'https://www.hast.net.cn/research/index?cid=150&page=2&per-page=15',
        '首页-调研宣传-智库论坛': 'https://www.hast.net.cn/research/index?cid=152&page=2&per-page=15',
        '首页-调研宣传-调查站点': 'https://www.hast.net.cn/research/index?cid=151&page=2&per-page=15',
        '首页-调研宣传-课题发布': 'https://www.hast.net.cn/research/index?cid=212&page=2&per-page=15',
        '首页-调研宣传-新媒体宣传': 'https://www.hast.net.cn/research/index?cid=326&page=2&per-page=15',
        '首页-调研宣传-媒体动态': 'https://www.hast.net.cn/research/index?cid=327&page=2&per-page=15',
        '首页-对外交流-工作动态': 'https://www.hast.net.cn/exchange/index?cid=199&page=2&per-page=15',
        '首页-对外交流-海外计划': 'https://www.hast.net.cn/exchange/index?cid=161&page=2&per-page=15',
        '首页-对外交流-外事管理': 'https://www.hast.net.cn/exchange/index?cid=162&page=2&per-page=15',
        '首页-对外交流-民间科技交流': 'https://www.hast.net.cn/exchange/index?cid=163&page=2&per-page=15',
        '首页-对外交流-港澳台科技交流与合作': 'https://www.hast.net.cn/exchange/index?cid=164&page=2&per-page=15',
        '首页-计划财务-工作动态': 'https://www.hast.net.cn/finance/index?cid=125&page=2&per-page=15',
        '首页-计划财务-规章制度': 'https://www.hast.net.cn/finance/index?cid=166&page=2&per-page=15',
        '首页-计划财务-教育培训': 'https://www.hast.net.cn/finance/index?cid=168&page=2&per-page=15',
        '首页-计划财务-预算公开': 'https://www.hast.net.cn/finance/index?cid=209&page=2&per-page=15',

    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            url = value.replace('page=2', 'page=' + str(page))
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)
            time.sleep(1)
            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'F-16 河南省科协'
    start_run(args.projectname, args.sinceyear, account_name)
