# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-03-02

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash():
    url = 'http://www.nxdzkj.org.cn/kxsz/nxkxyq/index_1.html'
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, verify=False)
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, supFlash):
    cookies = {
        'td_cookie': supFlash,
        '__51uvsct__JgbWGPks1JZm1qcG': '1',
        '__51vcke__JgbWGPks1JZm1qcG': '34853752-8e82-535b-810d-fa51bc009abe',
        '__51vuft__JgbWGPks1JZm1qcG': '1676527922915',
        'Hm_lvt_500b290e16aa8ad3240e76380883488a': '1676527923',
        '__vtins__JgbWGPks1JZm1qcG': '%7B%22sid%22%3A%20%22be6f56e4-02b8-5f2e-966f-0d508f54c755%22%2C%20%22vd%22%3A'
                                     '%206%2C%20%22stt%22%3A%2041206%2C%20%22dr%22%3A%2023943%2C%20%22expires%22%3A'
                                     '%201676529764115%2C%20%22ct%22%3A%201676527964115%7D',
        'Hm_lpvt_500b290e16aa8ad3240e76380883488a': '1676527964',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        if 'special' not in news_classify:
            part1_nodes = selector.xpath('//*[@class="list"]/li')
        else:
            part1_nodes = selector.xpath('//*[@class="w945"]/div')
        xpath_data(part1_nodes, news_classify, science_system, mongo,
                   account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('.//h3/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./h1/a/text()')[0].strip()
        except:
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = part_nodes[i].xpath('.//p/text()')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('./h1/a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_time = part_nodes[i].xpath('.//span/text()')[0].replace('/', '-')
            else:
                news_publish_time = part_nodes[i].xpath('./div[2]/div/text()')[0].split(' ')[0]
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('./a/@href')[0].strip()
                else:
                    url_part2 = part_nodes[i].xpath('./div[2]/a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.nxdzkj.org.cn' + url_part2

            if 'mp.weixin.qq.co' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):
    try:
        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,'
                      '*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                          'Chrome/110.0.0.0 Safari/537.36',
        }

        html_response = requests.get(news_page_url,
                                     headers=headers,
                                     verify=False)
        html_response.encoding = 'utf-8'
        content_text = html_response.text
    except Exception as e:
        logging.warning('Article Content Was Error :{}'.format(e))
        content_text = ''
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath(
            '//*[@class="zw"]//p//text() |'
            '//*[@class="zw"]/p/span//text() '
        )
        news_content = ''.join([x.strip() for x in news_content])

    except Exception as e:
        logging.warning("Article Content Xpath Was Error: {}".format(e))
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者:(.*?)</span', content_text, re.M | re.S)[0].strip()
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="zw"]//p//img/@src |'
                                        '//*[@class="zw"]//span//img/@src | '
                                        '//*[@class="zw"]//div//img/@src |'
                                        '//*[@class="zw"]//input/@src |'
                                        '//*[@class="zw"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="zw"]//span//video/@src |'
                                         ' //*[@class="zw"]//p//video/@src |'
                                         ' //*[@class="zw"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r"来源:(.*?)&nbsp;", content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r"阅读两:(.*?) ;", content_text, re.M | re.S)[0].strip()
    except Exception as e:
        logging.warning('Read Count Was Error :{}'.format(e))
        read_count = ''

    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-科协时政-科协要情': 'http://www.nxdzkj.org.cn/kxsz/nxkxyq/index_1.html',
        '首页-科协时政-基层动态': 'http://www.nxdzkj.org.cn/kxsz/jcdt/index_1.html',
        '首页-科协时政-通知公告': 'http://www.nxdzkj.org.cn/kxsz/tzgg/index_1.html',
        '首页-科协时政-媒体看科协': 'http://www.nxdzkj.org.cn/kxsz/mtkkx/index_1.html',
        '首页-科协时政-头版头条': 'http://www.nxdzkj.org.cn/kxsz/tbtt/index_1.html',
        '首页-科协时政-图说科协': 'http://www.nxdzkj.org.cn/kxsz/tskx/index_1.html',
        '首页-科协时政-领导讲话': 'http://www.nxdzkj.org.cn/kxsz/ldjh/index_1.html',
        '首页-科协时政-重要文件': 'http://www.nxdzkj.org.cn/kxsz/zywj/index_1.html',
        '首页-科协时政-预决算公开': 'http://www.nxdzkj.org.cn/kxsz/yjsgk/index_1.html',
        '首页-学会学术-学术交流': 'http://www.nxdzkj.org.cn/xsxh/xsjl/index_1.html',
        '首页-学会学术-服务创新': 'http://www.nxdzkj.org.cn/xsxh/fwcx/index_1.html',
        '首页-学会学术-学会建设': 'http://www.nxdzkj.org.cn/xsxh/xhjs/index_1.html',
        '首页-学会学术-学会科普': 'http://www.nxdzkj.org.cn/xsxh/xhkp/index_1.html',
        '首页-科学普及-社区科普': 'http://www.nxdzkj.org.cn/kxpj/kpqj/sqkp/index_1.html',
        '首页-科学普及-农村科普': 'http://www.nxdzkj.org.cn/kxpj/kpqj/nckp/index_1.html',
        '首页-科学普及-青少年科普': 'http://www.nxdzkj.org.cn/kxpj/kpqj/qsnkp/index_1.html',
        '首页-科学普及-科普周日': 'http://www.nxdzkj.org.cn/kxpj/pphd/kpzr/index_1.html',
        '首页-科学普及-科技展览': 'http://www.nxdzkj.org.cn/kxpj/pphd/kpzl/index_1.html',
        '首页-科学普及-科技教育活动': 'http://www.nxdzkj.org.cn/kxpj/pphd/kjcxds/index_1.html',
        '首页-科学普及-科普场馆': 'http://www.nxdzkj.org.cn/kxpj/kpcg/index_1.html',
        '首页-科学普及-新冠病毒防控': 'http://www.nxdzkj.org.cn/kxpj/xgfy/index_1.html',
        '首页-调研宣传-党组中心理论学习': 'http://www.nxdzkj.org.cn/kjgzzzj/dzzxzllxx/index_1.html',
        '首页-调研宣传-宁夏科技人物': 'http://www.nxdzkj.org.cn/kjgzzzj/kjrw/index_1.html',
        '首页-调研宣传-科技智库': 'http://www.nxdzkj.org.cn/kjgzzzj/kjzk/index_1.html',
        '首页-党旗下的科协-党务动态': 'http://www.nxdzkj.org.cn/dqxdkx/dj/dwdt/index_1.html',
        '首页-党旗下的科协-党的建设': 'http://www.nxdzkj.org.cn/dqxdkx/dj/ddjs/index_1.html',

    }
    supFlash = get_supflash()
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            if page == 1:
                url = value.replace('index_1.html', 'index.html')
            else:
                url = value.replace('index_1', 'index_' + str(page))
            html_text = get_html(url, supFlash)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'F-29 宁夏科协'
    start_run(args.projectname, args.sinceyear, account_name)
