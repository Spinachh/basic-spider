# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-02-12

import random
import re
import io
import sys
import time
import argparse
import urllib3
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_page1_html(url, id, news_classify):

    headers = {
        'authority': 'www.fjkx.org',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'accept-language': 'zh-CN,zh;q=0.9',
        'cache-control': 'max-age=0',
        'cookie': 'Hm_lvt_bd2ab1b3518db49ed6dea1275a751104=1679383752; Hm_lpvt_bd2ab1b3518db49ed6dea1275a751104=1679469755',
        'referer': 'https://www.fjkx.org/index.aspx',
        'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
    }

    params = (
        ('id', id),
    )
    if 'special' not in news_classify:
        data_content = requests.get('https://www.fjkx.org/newslist.aspx', headers=headers, params=params).text
        viewstate = re.findall(r'id="__VIEWSTATE" value="(.*?)"', data_content, re.M | re.S)[0]
        viewstategenerator = re.findall(r'id="__VIEWSTATEGENERATOR" value="(.*?)" />', data_content, re.M | re.S)[0]
        eventvalidation = re.findall(r'id="__EVENTVALIDATION" value="(.*?)"', data_content, re.M | re.S)[0]

        return data_content, viewstate, viewstategenerator, eventvalidation
    else:
        data_content = requests.get('https://www.fjkx.org/ztzl/newslist.aspx', headers=headers, params=params).text
        viewstate = re.findall(r'id="__VIEWSTATE" value="(.*?)"', data_content, re.M | re.S)[0]
        viewstategenerator = re.findall(r'id="__VIEWSTATEGENERATOR" value="(.*?)" />', data_content, re.M | re.S)[0]

        return data_content, viewstate, viewstategenerator



def get_page_html(url, id, page, viewstate, viewstategenerator, eventvalidation, news_classify):

    headers = {
        'authority': 'www.fjkx.org',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'accept-language': 'zh-CN,zh;q=0.9',
        'cache-control': 'max-age=0',
        'content-type': 'application/x-www-form-urlencoded',
        'cookie': 'Hm_lvt_bd2ab1b3518db49ed6dea1275a751104=1679383752; Hm_lpvt_bd2ab1b3518db49ed6dea1275a751104=1679468321',
        'origin': 'https://www.fjkx.org',
        'referer': url,
        'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/111.0.0.0 Safari/537.36',
    }

    params = (
        ('id', id),
    )

    data = {
        '__EVENTTARGET': 'plpage',
        '__EVENTARGUMENT': page,
        '__VIEWSTATE': viewstate,
        '__EVENTVALIDATION': eventvalidation,
        '__VIEWSTATEGENERATOR': viewstategenerator,
        'Top$search': '',
        'plpage_input': page -1

    }
    if 'special' not in news_classify:
        data_content = requests.post('https://www.fjkx.org/newslist.aspx', headers=headers, params=params, data=data).text
        viewstate = re.findall(r'id="__VIEWSTATE" value="(.*?)"', data_content, re.M | re.S)[0]
        viewstategenerator = re.findall(r'id="__VIEWSTATEGENERATOR" value="(.*?)" />', data_content, re.M | re.S)[0]
        eventvalidation = re.findall(r'id="__EVENTVALIDATION" value="(.*?)"', data_content, re.M | re.S)[0]

        return data_content, viewstate, viewstategenerator, eventvalidation
    else:
        data_content = requests.post('https://www.fjkx.org/ztzl/newslist.aspx', headers=headers, params=params,
                                     data=data).text

        viewstate = re.findall(r'id="__VIEWSTATE" value="(.*?)"', data_content, re.M | re.S)[0]
        viewstategenerator = re.findall(r'id="__VIEWSTATEGENERATOR" value="(.*?)" />', data_content, re.M | re.S)[0]
        # eventvalidation = re.findall(r'id="__EVENTVALIDATION" value="(.*?)"', data_content, re.M | re.S)[0]

        return data_content, viewstate, viewstategenerator


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="am-cf newslist-main"]/div/a')
        xpath_data(part1_nodes, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify：{} Get_Data Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./div[2]/div[1]/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./div[2]/div[2]/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_d = part_nodes[i].xpath('./div[1]/div[1]/text()')[0]
            news_publish_y = part_nodes[i].xpath('./div[1]/div[2]/text()')[0]
            news_publish_time = news_publish_y + '-' + news_publish_d
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./@href')[0]
            if 'cateid/' in url_part2:
                break
            elif 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.fjkx.org/' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):
    id = re.findall(r'id=(\w+)', news_page_url)[0]

    headers = {
        'authority': 'www.fjkx.org',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
        'accept-language': 'zh-CN,zh;q=0.9',
        'cache-control': 'max-age=0',
        'cookie': 'Hm_lvt_bd2ab1b3518db49ed6dea1275a751104=1679383752; Hm_lpvt_bd2ab1b3518db49ed6dea1275a751104=1679473940',
        'sec-ch-ua': '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'none',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
    }

    params = (
        ('id', id),
    )

    html_response = requests.get('https://www.fjkx.org/newsinfo.aspx', headers=headers, params=params)

    html_response.encoding = 'gb2312'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    # print(result)
    # breakpoint()
    try:
        news_content = selector_page.xpath('//*[@class="infopage-content"]//p//text() |'
                                           '//*[@class="infopage-content"]//p/span/text() |'
                                           '//*[@class="infopage-content"]//span//text() |')
        news_content = ''.join(x.strip() for x in news_content)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="infopage-content"]//p//img/@src |'
                                        '//*[@class="infopage-content"]//span//img/@src' )[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来稿：(.*?)&nbsp', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'阅读<span>(.*?)</', content_text, re.M | re.S)[0]
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        # '首页-工作动态-头条新闻': 'https://www.fjkx.org/newslist.aspx?id=8716FF1EED330100',
        # '首页-工作动态-重要新闻': 'https://www.fjkx.org/newslist.aspx?id=9FA983E4ECC4D527',
        # '首页-工作动态-机关直属': 'https://www.fjkx.org/newslist.aspx?id=596A75F7B09BA0B3',
        # '首页-工作动态-省级学会': 'https://www.fjkx.org/newslist.aspx?id=A630E805538C9113',
        # '首页-工作动态-市县科协': 'https://www.fjkx.org/newslist.aspx?id=1AFBB4C6752B437E',
        # '首页-党建-党的建设': 'https://www.fjkx.org/newslist.aspx?id=C102D8F47555B5E3',
        # '首页-创新驱动-智汇福建': 'https://www.fjkx.org/newslist.aspx?id=967913A8328CDBF2',
        # '首页-科学普及-科普动态': 'https://www.fjkx.org/newslist.aspx?id=6C2E88B853656A74',
        # '首页-科学普及-科普知识': 'https://www.fjkx.org/newslist.aspx?id=02B20D91BC56B332',
        # '首页-科学普及-青少年科技活动': 'https://www.fjkx.org/newslist.aspx?id=9E33E308D4468FE7',
        # '首页-学术交流-交流动态': 'https://www.fjkx.org/newslist.aspx?id=40312B82975BAFF5',
        # '首页-学术交流-闽台港澳': 'https://www.fjkx.org/newslist.aspx?id=4D19EDD647DC676D',
        # '首页-科技人才-人才资讯': 'https://www.fjkx.org/newslist.aspx?id=478F3E9BF2CC04BC',
        # '首页-科技人才-科技榜样': 'https://www.fjkx.org/newslist.aspx?id=75B708ADFAFE6A96',
        '首页-专题专栏-党的二十大精神-special': 'https://www.fjkx.org/ztzl/newslist.aspx?id=00A891B0CC6FD327',
        '首页-专题专栏-学习再研究-special': 'https://www.fjkx.org/ztzl/newslist.aspx?id=6E15CAF96FC5DBBD',
        '首页-专题专栏-回信精神-special': 'https://www.fjkx.org/ztzl/newslist.aspx?id=023094993B946119',
        # '首页-专题专栏-喜迎二十大-special': 'https://www.fjkx.org/ztzl/index.aspx?id=15E8956847C05A87',
        '首页-专题专栏-社会主义核心价值观-special': 'https://www.fjkx.org/ztzl/newslist.aspx?id=280DAB6AABD30574',
    }
    for key, value in urls.items():
        news_classify = key
        id = re.findall(r'id=(\w+)', value)[0]

        view_dict = {}

        for page in range(1, 15):
            if 'special' not in news_classify:
                if page == 1:
                    data_content, viewstate, viewstategenerator, eventvalidation = get_page1_html(value, id, news_classify)
                    get_data(data_content, news_classify, account_name,
                             science_system, mongo, project_time, start_time)
                    view_dict['viewstate'] = viewstate
                    view_dict['viewstategenerator'] = viewstategenerator
                    view_dict['eventvalidation'] = eventvalidation

                else:
                    viewstate = view_dict.get('viewstate')
                    viewstategenerator = view_dict.get('viewstategenerator')
                    eventvalidation = view_dict.get('eventvalidation')

                    data_content, viewstate, viewstategenerator, eventvalidation =\
                        get_page_html(value, id, page, viewstate, viewstategenerator, eventvalidation, news_classify)

                    view_dict['viewstate'] = viewstate
                    view_dict['viewstategenerator'] = viewstategenerator
                    view_dict['eventvalidation'] = eventvalidation

                    get_data(data_content, news_classify, account_name,
                             science_system, mongo, project_time, start_time)
            else:
                if page == 1:
                    data_content, viewstate, viewstategenerator = get_page1_html(value, id, news_classify)
                    get_data(data_content, news_classify, account_name,
                             science_system, mongo, project_time, start_time)
                    view_dict['viewstate'] = viewstate
                    view_dict['viewstategenerator'] = viewstategenerator

                else:
                    viewstate = view_dict.get('viewstate')
                    viewstategenerator = view_dict.get('viewstategenerator')
                    eventvalidation = ''
                    data_content, viewstate, viewstategenerator = \
                        get_page_html(value, id, page, viewstate, viewstategenerator, eventvalidation, news_classify)

                    view_dict['viewstate'] = viewstate
                    view_dict['viewstategenerator'] = viewstategenerator

                    get_data(data_content, news_classify, account_name,
                             science_system, mongo, project_time, start_time)

            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2023-01-01')
    args = parser.parse_args()
    account_name = 'F-13 福建省科协'
    start_run(args.projectname, args.sinceyear, account_name)
