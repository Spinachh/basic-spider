# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-02-05

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, columnid, uid, startrecord, endrecord):


    headers = {
        'Accept': 'application/xml, text/xml, */*; q=0.01',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'https://www.bast.net.cn',
        # 'Referer': 'https://www.bast.net.cn/col/col31584/index.html?uid=96562&pageNum=2',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Mobile Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
        'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
        'sec-ch-ua-mobile': '?1',
        'sec-ch-ua-platform': '"Android"',
    }

    params = (
        ('startrecord', '1'),
        ('endrecord', '45'),
        ('perpage', '15'),
    )

    data = {
        'col': '1',
        'webid': '48',
        'path': 'http://www.bast.net.cn/',
        'columnid': columnid,
        'sourceContentType': '1',
        'unitid': '96562',
        'webname': '\u5317\u4EAC\u5E02\u79D1\u5B66\u6280\u672F\u534F\u4F1A',
        'permissiontype': '0'
    }

    response = requests.post('https://www.bast.net.cn/module/web/jpage/dataproxy.jsp', headers=headers,
                             params=params, data=data)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    content = re.findall(r'<record><!\[CDATA\[(.*?)\]\>', html_text, re.M | re.S)
    try:
        if news_classify == '首页-会员之家':
            content_lst = content[1:]
        else:
            content_lst = content

        flag = ree_data(content_lst, news_classify, science_system, mongo, account_name, project_time, start_time)
        return flag

    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(content)):
        try:
            news_title = re.findall(r"title='(.*?)>", content[i])[0]
        except Exception as e:
            logging.warning('{} {} Article Title Was Error :{}'.format(account_name, news_classify, e))
            news_title = ''

        try:
            news_abstract = re.findall(r"'>							(.*?)</a", content[i])[1].strip()
        except Exception as e:
            logging.warning('{} {} Article Abstract Was Error :{}'.format(account_name, news_classify, e))
            news_abstract = ''

        try:
            # news_publish_time = re.findall(r'/art/(.*?)/art_', content[i])[0].replace('/', '-')
            news_publish_d = re.findall(r'<h2 style="font-size:24px; font-weight:bold">(.*?)</h2>', content[i])[0].strip()
            news_publish_y = re.findall(r'<h5 style="font-size:13px;">(.*?)</h5>', content[i])[0].strip().replace('/', '-')
            news_publish_time = news_publish_y + '-' + news_publish_d
            print(news_publish_time)
            breakpoint()

        except Exception as e:
            logging.warning('{} {} Article Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                url_part2 = re.findall(r'href="(.*?)"', content[i])[0]
            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.bast.net.cn' + url_part2

            if 'https://mp.weixin.qq.com/' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="arccont"]//span/text() | '
                                           '//*[@class="arccont"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者:(.*?)</', content_text, re.M | re.S)[0]
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="arccont"]//span//img/@src |'
                                        ' //*[@class="arccont"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'信息来源：(.*?)</', content_text, re.M | re.S)[0].split('>')[2].split('<')[0]
    except:
        source = ''
    try:
        read_count = re.findall(r'浏览：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # print(click_count)
    # breakpoint()
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    # print(news_page_url)
    # breakpoint()
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学会组织-典型学会': 'https://www.bast.net.cn/col/col31608/index.html',
        '首页-学术交流-学术动态': 'https://www.bast.net.cn/col/col31530/index.html?uid=96562&pageNum=2',
        '首页-学术交流-活动预告': 'https://www.bast.net.cn/col/col31627/index.html',
        '首页-学术交流-科学道德和学风建设': 'https://www.bast.net.cn/col/col31446/index.html',
        '首页-科学普及-科普动态': 'https://www.bast.net.cn/col/col31584/index.html?uid=96562&pageNum=2',
        '首页-科学普及-科普活动': 'https://www.bast.net.cn/col/col31513/index.html?uid=96562&pageNum=2',
        '首页-科学普及-全民素质行动': 'https://www.bast.net.cn/col/col31488/index.html',
        '首页-科学普及-品牌活动': 'https://www.bast.net.cn/col/col31469/index.html',
        '首页-科创服务-通知公告': 'https://www.bast.net.cn/col/col33942/index.html',
        '首页-科创服务-学术交流': 'https://www.bast.net.cn/col/col33962/index.html',
        '首页-科创服务-人才培养': 'https://www.bast.net.cn/col/col34262/index.html',
        '首页-科创服务-决策咨询': 'https://www.bast.net.cn/col/col33982/index.html',
        '首页-科创服务-国际合作': 'https://www.bast.net.cn/col/col33963/index.html',
        '首页-人才高地-人才动态': 'https://www.bast.net.cn/col/col33622/index.html',
        '首页-人才高地-科技人才': 'https://www.bast.net.cn/col/col31473/index.html',
        '首页-人才高地-科学传播专业职称': 'https://www.bast.net.cn/col/col33562/index.html?uid=96562&pageNum=2',
        '首页-人才高地-最美工作者': 'https://www.bast.net.cn/col/col33582/index.html?uid=96562&pageNum=2',
        '首页-人才高地-科学家奖项': 'https://www.bast.net.cn/col/col31515/index.html',
        '首页-人才高地-科学家精神': 'https://www.bast.net.cn/col/col35762/index.html?uid=96562&pageNum=2',
        '首页-人才高地-智库': 'https://www.bast.net.cn/col/col33602/index.html?uid=96562&pageNum=2',
        '首页-党建引领-机关党建': 'https://www.bast.net.cn/col/col31586/index.html?uid=96562&pageNum=2',
        '首页-党建引领-科技社团党建': 'https://www.bast.net.cn/col/col31587/index.html',
        '首页-党建引领-党员学习': 'https://www.bast.net.cn/col/col33642/index.html',
        '首页-信息公开-政策法规-综合管理': 'https://www.bast.net.cn/col/col31522/index.html?uid=97666&pageNum=2',
        '首页-信息公开-政策法规-人事管理': 'https://www.bast.net.cn/col/col31542/index.html?uid=97666&pageNum=2',
        '首页-信息公开-政策法规-财税管理': 'https://www.bast.net.cn/col/col31504/index.html?uid=97666&pageNum=2',
        '首页-信息公开-公报': 'https://www.bast.net.cn/col/col31534/index.html',

    }

    startrecord = 1
    endrecord = 45

    for key, value in urls.items():
        news_classify = key
        columnid = re.findall(r'col\/col(.*?)\/index', value)[0]
        try:
            uid = re.findall(r'uid=(.*?)&', value)[0]
        except:
            uid = ''

        while True:
            html_text = get_html(value, columnid, uid, startrecord, endrecord)
            flag = get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time)
            if flag != '':
                startrecord = endrecord + 1
                endrecord = endrecord + 15
                logging.warning('startrecord:{}'.format(startrecord))
                logging.warning('endrecord:{}'.format(endrecord))
            else:
                break


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2019-07-01')
    args = parser.parse_args()
    account_name = 'F-01 北京市科协'
    start_run(args.projectname, args.sinceyear, account_name)
