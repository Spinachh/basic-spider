# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-02-08
import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, page):

    cookies = {
        'td_cookie': '1028923564',
    }

    headers = {
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'http://www.hljkx.org.cn',
        'Referer': 'http://www.hljkx.org.cn/portal/news/xwyw?categoryId={}&url=news'.format(url),
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Mobile Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    data = {
        'categoryId': url,
        'offset': page,
        'limit': '5'
    }

    response = requests.post('http://www.hljkx.org.cn/portal/news/newList',
                             headers=headers, data=data, verify=False)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):

    try:
        if 'special' in news_classify:
            html_text = re.findall(r'callback\((.*?)\)', html_text)[0]
            content = json.loads(html_text)
            ree_data(content, news_classify, science_system, mongo,
                     account_name, project_time, start_time)
        else:
            content = json.loads(html_text)
            ree_data(content, news_classify, science_system, mongo,
                   account_name, project_time, start_time)

    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
             account_name, project_time, start_time):
    results = content['list']
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for item in results:
        try:
            news_title = item.get('title').strip()
        except:
            news_title = ''
        try:
            news_abstract = item.get('description').strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = item.get('createDate').split(' ')[0]
        except Exception as e:
            logging.warning('Publish Time Was Error: {}'.format(e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            news_page_url = 'http://www.hljkx.org.cn/portal/news/newsDetails?articleId={}'.format(item.get('id'))

            if 'https://mp.weixin.qq.com' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_page_url)



            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''
    try:
        news_content = selector_page.xpath('//*[@class="xwDetails"]//span//text() | '
                                           '//*[@class="xwDetails"]//p//text() | '
                                           '//*[@class="xwDetails"]//div//text() | '
                                           )
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者：(.*?) ', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="xwDetails"]//span//img/@src |'
                                        '//*[@class="xwDetails"]//p//img/@src | '
                                        '//*[@class="xwDetails"]//div//img/@src |'
                                        '//*[@class="xwDetails"]//img/@src')[0]
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="xwDetails"]//span//video/@src |'
                                         ' //*[@class="xwDetails"]//p//video/@src |'
                                         ' //*[@class="xwDetails"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip().split('来源')[-1]
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览次数：<span>(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻动态-时政要闻': '81f1c085994a4a019a8191b4f5488877',
        '首页-新闻动态-科协要闻': '73c5540ddb56436a81cf0dee41ebb6c0',
        '首页-新闻动态-通知公告': '7802ec33f7464503940dbbc04d6e4c41',
        '首页-新闻动态-公示': 'a59a2f48671049cbb4dfad89a9abdba8',
        '首页-新闻动态-市地科协动态': '6180ccb5a3134227b479aee917d0e0bb',
        '首页-新闻动态-媒体声音': 'af481e67d5574e2b860d6ff46c22029e',
        '首页-新闻动态-上级精神': '75595c3ba0474aa2b39c278f2ee46134',
        '首页-新闻动态-法律法规': '050045b058c5492285b9edd72956c123',
        '首页-党建工作-党建要闻': '06dbde7512f64ed684f6b64a58e4efe7',
        '首页-党建工作-党支部建设': '3a33f2dfeb834139bd086ad494e89f86',
        '首页-党建工作-廉洁文化建设': '4191347496fe4e1aa67508c00117850e',
        '首页-党建工作-地方科协和学会党建': '5eff3e36bbb04e50927ed85deb6a25a7',
        '首页-党建工作-党务公开': 'e049e8a116e84fa5bd86e039758fd015',
        '首页-党建工作-党章党规': '9d7bf8a55a4642ae8712495632cb04f4',
        '首页-党建工作-能力作风建设-活动报道': 'badb59ab3a3846f194a9ae43f8211134',
        '首页-党建工作-能力作风建设-市地科协活动': '3a662d58f1d04382bc97e63633af6ffd',
        '首页-党建工作-能力作风建设-树典型展风采': '32ef2d482aed4d03b0a6379ea4a85144',
        '首页-党建工作-能力作风建设-五优基层党组织': '88b19737e8ea47e1901d05c10acb3395',
        '首页-党建工作-能力作风建设-五优党员干部': 'd51750dee9de4be197e1f06e203cb20e',
        '首页-学术学会-学会工作': '629b454728f44b1f9094849b347d4971',
        '首页-学术学会-学术动态': 'e5dbe4752ae0442894a7291a4036ba93',
        '首页-学术学会-通知公告': '68d695eba0b1445daeb8d35e3612ca68',
        '首页-学术学会-政策法规': '746627dac99a4751ac52e4f6a83000dd',
        '首页-学术学会-文件通知': 'fccf7c06879f4f17870dd425c653baae',
        '首页-科学普及-工作动态': '23082c51769b4690bebb2ca32e1bff0d',
        '首页-科学普及-科普活动': '6b7bc230de524725ad75e9ef822beb4a',
        '首页-科学普及-工作通知': 'f02a252d948641c3bbd2bf120665edf8',
        '首页-科学普及-公告公示': '4cf2d4085a9941f7959987be15d7c71c',
        '首页-科学普及-科普阵地': 'c1c3a88d3a314fe2b0aa27630633012e',
        '首页-科学普及-科普资源': 'ea3955ec717d4963a80c9a85ff3ccc99',
        '首页-科学普及-科普志愿服务': 'a5bfe7069e55496a9d601feada4e0ee4',
        '首页-科学普及-科普传播融媒体': 'b2e77a8c33d4497b86c55fbeb6093b2c',
        '首页-调研宣传-工作动态': '4a6240610a8847bb8716e140960d0327',
        '首页-调研宣传-媒体声音': '9126ebded26a401f91715d1c56e6d688',
        '首页-调研宣传-通知公告': '87aed93255f449768156d0ceb4b372dc',
        '首页-调研宣传-专家建议': 'aa4ca0fe5093458093848115aae09477',
        '首页-调研宣传-最美科技工作者': '40ef201fbe2b414d90bb9aa906b2d928',
        '首页-调研宣传-科技激励扶持政策': '7a3c7ef859494d7abe2a5db18334bed9',
        '首页-组织人才-工作动态': '927d7f3ba33744eca9fc0a2ef8391648',
        '首页-组织人才-通知公告': '382e64169cbf449f95e6decc91eeb00f',
        '首页-组织人才-龙江创客': 'b3a7cf2441d54b089f138efda8cb3043',
        '首页-组织人才-文件精神': '950851c4770d4433a8ce5b5abae6c057',
    }
    for key, value in urls.items():
        news_classify = key

        for page in range(1, 11):
            if 'special' in news_classify:
                url = value.replace('pageNum=1', 'pageNum=' + str(page))
            else:
                url = value
            html_text = get_html(url, page)
            # print(html_text)
            # breakpoint()
            try:
                get_data(html_text, news_classify, account_name,
                         science_system, mongo, project_time, start_time)
            except Exception as e:
                logging.warning('Page {} Get Data Was Error:{}'.format(page, e))
                break

            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'F-08 黑龙江省科协'
    start_run(args.projectname, args.sinceyear, account_name)
