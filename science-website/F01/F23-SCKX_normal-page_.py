# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-02-24
import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash():
    url = 'http://www.gxast.org.cn/kxxw/ttyw/p-1.html'
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.gxast.org.cn/kxxw/ttyw/p-1.html',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, verify=False)
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url):

    response = requests.get(url, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        if 'special' not in news_classify:
            part1_nodes = selector.xpath('//*[@class="center-content"]//li/div')
        else:
            part1_nodes = selector.xpath('//*[@class="w945"]/div')
        xpath_data(part1_nodes, news_classify, science_system, mongo,
                   account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('./a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./h1/a/text()')[0].strip()
        except:
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('./h1/a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_time = part_nodes[i].xpath('./span/text()')[0].strip()
                news_publish_time = re.sub(r'[\],\[]', '', news_publish_time)
            else:
                news_publish_time = part_nodes[i].xpath('./div[2]/div/text()')[0].split(' ')[0]
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('./a/@href')[0].strip()
                else:
                    url_part2 = part_nodes[i].xpath('./div[2]/a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.sckx.org.cn' + url_part2

            if 'mp.weixin.qq.com' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            logging.warning(news_dict_content)
            breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    try:
        html_response = requests.get(news_page_url, verify=False)
        html_response.encoding = 'utf-8'
        content_text = html_response.text
    except Exception as e:
        logging.warning('Article Content Was Error :{}'.format(e))
        content_text = ''

    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath(
            '//*[@class="center-content"]//p//text() |'
            '//*[@class="center-content"]/p/span//text() '
        )
        news_content = ''.join([x.strip() for x in news_content])
    except Exception as e:
        logging.warning("Article Content Xpath Was Error: {}".format(e))
        news_content = result.get('content')
    try:
        news_author = re.findall(r'作者：(.*?)<', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="center-content"]//p//img/@src |'
                                        '//*[@class="center-content"]//span//img/@src | '
                                        '//*[@class="center-content"]//div//img/@src |'
                                        '//*[@class="center-content"]//input/@src |'
                                        '//*[@class="center-content"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'


    try:
        news_video = selector_page.xpath('//*[@class="center-content"]//span//video/@src |'
                                         ' //*[@class="center-content"]//p//video/@src |'
                                         ' //*[@class="center-content"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源：(.*?)>(.*?)</', content_text, re.M | re.S)[0][1]
    except:
        source = ''

    try:
        read_count = re.findall(r'阅读:(.*?)&', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        logging.warning('Read Count Was Error :{}'.format(e))
        read_count = ''
    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-科协要闻': 'http://www.sckx.org.cn/views/news/list/808b4671-12ca-44a2-aa6a-1735c0c08818/15/1.html',
        '首页-通知公告': 'http://www.sckx.org.cn/views/news/list/f3951b78-4cd6-4b80-b9eb-7ec3bbe78c92/15/1.html',
        '首页-党的建设': 'http://www.sckx.org.cn/views/news/list/07638f3c-e3c5-4935-ad0c-67085005d507/15/1.html',
        '首页-市县科协': 'http://www.sckx.org.cn/views/news/list/889c4b11-feb5-4c5b-b8e8-2a8de65f8b07/15/1.html',
        '首页-天府科技云服务': 'http://www.sckx.org.cn/views/news/list/f13002af-0536-4ab6-ae87-b37ff5831e72/15/1.html',
        '首页-学术学会-学术动态': 'http://www.sckx.org.cn/views/news/list/7a0a1f21-c650-4caa-a29f-71af96ede610/15/1.html',
        '首页-学术学会-学会管理': 'http://www.sckx.org.cn/views/news/list/759756ef-1ec2-400f-97c5-96955c5330ab/15/1.html',
        '首页-学术学会-企事业科协': 'http://www.sckx.org.cn/views/news/list/cad3c75d-69c4-4181-89ba-035a136c6cde/15/1.html',
        '首页-学术学会-创新驱动力': 'http://www.sckx.org.cn/views/news/list/5fcfeac0-d7c6-4729-be48-e62e4f0d6d42/15/1.html',
        '首页-学术学会-院士工作站': 'http://www.sckx.org.cn/views/news/list/e14ac887-803f-46a4-b1fc-610f4a2f04b6/15/1.html',
        '首页-学术学会-天府院士': 'http://www.sckx.org.cn/views/news/list/57d90257-bbe3-49da-bff0-32a0fd0f37d2/15/1.html',
        '首页-学术学会-其他': 'http://www.sckx.org.cn/views/news/list/a35e51ee-2986-4c65-804f-2d39ae6cfd2e/15/1.html',
        '首页-科普工作-科普动态': 'http://www.sckx.org.cn/views/news/list/20d2facb-a3c4-4ad8-a123-78c7ae99f0a6/15/1.html',
        '首页-科普工作-全民科学素质行动': 'http://www.sckx.org.cn/views/news/list/66af673b-2487-4b0c-8e1e-58c93127f3bf/15/1.html',
        '首页-科普工作-科技助力乡村振兴': 'http://www.sckx.org.cn/views/news/list/540f0979-cefd-4298-b426-a9415f0b2826/15/1.html',
        '首页-科普工作-其他': 'http://www.sckx.org.cn/views/news/list/15cbebce-901e-4bf4-b93f-261c3893620f/15/1.html',
        '首页-组织人才-人事动态': 'http://www.sckx.org.cn/views/news/list/76909be9-53ec-4440-b547-1d314eca674f/15/1.html',
        '首页-组织人才-组织建设': 'http://www.sckx.org.cn/views/news/list/1535ca6d-a351-42d6-88c6-92c92fecb90c/15/1.html',
        '首页-组织人才-表彰举荐': 'http://www.sckx.org.cn/views/news/list/9e98910d-cb91-4feb-bc94-9f2de5c7eeef/15/1.html',
        '首页-组织人才-教育培训': 'http://www.sckx.org.cn/views/news/list/3e576871-1246-4a6f-8c31-6d96fa5fdc31/15/1.html',

    }
    # supFlash = get_supflash()
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 15):
            url = value.replace('1.html', str(page) + '.html')
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'F-23 四川省科协'
    start_run(args.projectname, args.sinceyear, account_name)
