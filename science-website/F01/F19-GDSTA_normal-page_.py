# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-02-20
import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, page):
    cookies = {
        'Power%3A%3ASiteUniqueVisitorKey': '%2F1%2F',
        '.AspNetCore.Antiforgery.0_wwJG3ngag': 'CfDJ8JQNodTp7BpFhFEZrTnFPTAOce-t1dKoLLa0Q0V2XPJkmydLnb8sMIkFlg_69Q3EtuOmtzU_9imBBD_0jS8UJcmwgSBkLyZyrE26csiQl90n_uAtfNxxsOQJaqVJkJq9pNmjYwnYz9r7hG5lhapXYQ0',
        'PowerNodeUniqueVisitor': '3b3c7403-ef31-45c6-a605-5c945bc9cbdd_1105',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Referer': url,
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-User': '?1',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
        'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    response = requests.get(url, headers=headers, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        if 'special' not in news_classify:
            part1_nodes = selector.xpath('//*[@class="newsList"]/li')
        else:
            part1_nodes = selector.xpath('//*[@class="pageTPList"]/li')
        xpath_data(part1_nodes, news_classify, science_system, mongo,
                   account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('./a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./div[1]/a/text()')[0].strip()
        except:
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('./div[3]/div/text()')[0].strip()
        except:
            news_abstract = ''

        '''
        try:
            click_content = part_nodes[i].xpath('./a/@title')[0]
            click_count = re.findall(r'点击数：(.*?)发表时间', click_content, re.M | re.S)[0].strip()
        except:
            click_count = ''
        '''

        try:
            if 'special' not in news_classify:
                news_publish_time = part_nodes[i].xpath('./span/text()')[0].strip()
            else:
                news_publish_time = part_nodes[i].xpath('./div[3]//span/text()')[0].strip()
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('./a/@href')[0].strip()
                else:
                    url_part2 = part_nodes[i].xpath('./div[1]/a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.gdsta.cn' + url_part2

            # print(news_page_url)
            # breakpoint()

            if 'https://mp.weixin.qq.com' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'none',
        'Sec-Fetch-User': '?1',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
        'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }
    html_response = requests.get(news_page_url, headers=headers, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath(
            '//*[@class="conTxt"]/p/text() |'
            '//*[@class="conTxt"]//span/text() '
        )
        news_content = ''.join([x.strip() for x in news_content])
    except Exception as e:
        logging.warning("Article Content Xpath Was Error: {}".format(e))
        news_content = result.get('content')
    try:
        news_author = re.findall(r'作者：(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="conTxt"]//p//img/@src |'
                                        '//*[@class="conTxt"]//span//img/@src | '
                                        '//*[@class="conTxt"]//div//img/@src |'
                                        '//*[@class="conTxt"]//input/@src |'
                                        '//*[@class="conTxt"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'


    try:
        news_video = selector_page.xpath('//*[@class="conTxt"]//span//video/@src |'
                                         ' //*[@class="conTxt"]//p//video/@src |'
                                         ' //*[@class="conTxt"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        article_id = re.findall(r'\d+', news_page_url)[0]
        read_count = re.findall(r'阅读次数：(.*?)</', content_text, re.M | re.S)
        # https://www.gdsta.cn/contentmanage/ajax/getandupdate?mold=article&id=36618&isShowHit=true&nodeId=12&ispreview=

        headers = {
            'Accept': '*/*',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Connection': 'keep-alive',
            'Referer': news_page_url,
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-origin',
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
            'X-Requested-With': 'XMLHttpRequest',
            'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
        }

        params = (
            ('mold', 'article'),
            ('id', article_id),
            ('isShowHit', 'true'),
            ('nodeId', '12'),
            ('ispreview', ''),
        )

        read_text = requests.get('https://www.gdsta.cn/contentmanage/ajax/getandupdate',
                                 headers=headers, params=params, verify=False).json()
        read_count = read_text.get('hits')
    except Exception as e:
        logging.warning('Read Count Was Error :{}'.format(e))
        read_count = ''
    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-大字头条': 'https://www.gdsta.cn/dztt_',
        '首页-通知公告': 'https://www.gdsta.cn/tzgg_',
        '首页-科技工作者': 'https://www.gdsta.cn/kjgzz_',
        '首页-学会之窗-学会动态': 'https://www.gdsta.cn/xhzc/xhdt162_',
        '首页-学会之窗-学会党建': 'https://www.gdsta.cn/xhzc/sjxhdj1_',
        '首页-走进科协-科协简报': 'https://www.gdsta.cn/zjkx/kxjb_',
        '首页-走进科协-大事记': 'https://www.gdsta.cn/zjkx/kxdsj_',
        '首页-科协新闻-要闻-special': 'https://www.gdsta.cn/kxxw/yw_',
        '首页-科协新闻-动态-special': 'https://www.gdsta.cn/kxxw/dt_',
        '首页-科协新闻-领导动态-special': 'https://www.gdsta.cn/kxxw/lddt_',
        '首页-智库-special': 'https://www.gdsta.cn/zk_',
        '首页-合作交流-special': 'https://www.gdsta.cn/hzjl_',
        '首页-党建工作-special': 'https://www.gdsta.cn/djgz_',
        '首页-学术-学会动态-special': 'https://www.gdsta.cn/xs/xhdt_',
        '首页-学术-政策法规-special': 'https://www.gdsta.cn/xs/zcfg_',
        '首页-学术-岭南科协论坛-special': 'https://www.gdsta.cn/xs/lnkxlt_',
        '首页-科普-科普活动-special': 'https://www.gdsta.cn/kp/kphd_',
        '首页-科普-相关文件-special': 'https://www.gdsta.cn/kp/xgwj_',
        '首页-科普-典型示范-special': 'https://www.gdsta.cn/kp/dxsf_',
        '首页-科普-资源设施-special': 'https://www.gdsta.cn/kp/zyss_',
        '首页-基层科协-广州-special': 'https://www.gdsta.cn/jckx/gz_',
        '首页-基层科协-深圳-special': 'https://www.gdsta.cn/jckx/sz_',
        '首页-基层科协-珠海-special': 'https://www.gdsta.cn/jckx/zh_',
        '首页-基层科协-汕头-special': 'https://www.gdsta.cn/jckx/st_',
        '首页-基层科协-佛山-special': 'https://www.gdsta.cn/jckx/s_',
        '首页-基层科协-韶关-special': 'https://www.gdsta.cn/jckx/sg_',
        '首页-基层科协-河源-special': 'https://www.gdsta.cn/jckx/hy_',
        '首页-基层科协-梅州-special': 'https://www.gdsta.cn/jckx/mz_',
        '首页-基层科协-惠州-special': 'https://www.gdsta.cn/jckx/hz_',
        '首页-基层科协-东莞-special': 'https://www.gdsta.cn/jckx/dg_',
        '首页-基层科协-中山-special': 'https://www.gdsta.cn/jckx/zs_',
        '首页-基层科协-江门-special': 'https://www.gdsta.cn/jckx/jm_',
        '首页-基层科协-阳江-special': 'https://www.gdsta.cn/jckx/yj_',
        '首页-基层科协-湛江-special': 'https://www.gdsta.cn/jckx/zj_',
        '首页-基层科协-茂名-special': 'https://www.gdsta.cn/jckx/mm_',
        '首页-基层科协-肇庆-special': 'https://www.gdsta.cn/jckx/zq_',
        '首页-基层科协-清远-special': 'https://www.gdsta.cn/jckx/qy_',
        '首页-基层科协-潮州-special': 'https://www.gdsta.cn/jckx/cz_',
        '首页-基层科协-揭阳-special': 'https://www.gdsta.cn/jckx/jy_',
        '首页-基层科协-云浮-special': 'https://www.gdsta.cn/jckx/yf_',
    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 10):
            url = value + str(page)
            html_text = get_html(url, page)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)
            time.sleep(1)
            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'F-19 广东省科协'
    start_run(args.projectname, args.sinceyear, account_name)
