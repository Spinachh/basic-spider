# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-02-17
import random
import re
import io
import sys
import time
import json
import urllib3
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url,):
    response = requests.get(url)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        if 'special' in news_classify:
            part1_nodes = selector.xpath('//*[@class="gg-zhenwen"]/div')

        elif 'other' in news_classify:
            part1_nodes = selector.xpath('//*[@id="content_list"]/div')
        else:
            part1_nodes = selector.xpath('//*[@class="wp"]/ul/li')

        xpath_data(part1_nodes, news_classify, science_system, mongo,
                   account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            if 'special' in news_classify:
                news_title = part_nodes[i].xpath('.//a/text()')[0].strip()
            elif 'other' in news_classify:
                news_title = part_nodes[i].xpath('.//a/p/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./div[2]/h4/a/text()')[0].strip()
        except Exception as e:
            logging.warning('News Title Was Error: {}'.format(e))
            news_title = ''

        try:
            if 'special' in news_classify:
                news_abstract = part_nodes[i].xpath('.//a/text()')[0].strip()
            elif 'other' in news_classify:
                news_abstract = part_nodes[i].xpath('./div[1]/div//p/text()')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('./div[2]/div[3]//text()')[0].strip()

        except Exception as e:
            logging.warning('News Abstract Was Error: {}'.format(e))
            news_abstract = ''

        try:
            if 'special' in news_classify:
                news_publish_time = part_nodes[i].xpath('.//span/text()')[0].split('发布时间：')[-1].strip()
            elif 'other' in news_classify:
                news_publish_time = part_nodes[i].xpath('./div[2]/p/span[2]/font/text()')[0].strip()
            else:
                news_publish_y = part_nodes[i].xpath('./div[2]/div[1]/text()')[1].strip()
                news_publish_d = part_nodes[i].xpath('./div[2]/div[1]/span/text()')[0].strip()
                news_publish_time = news_publish_y + '-' + news_publish_d
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' in news_classify:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0].replace('..', '')
                elif 'other' in news_classify:
                    categoryNo = news_classify.split('-')[-1]
                    # http://www.hnast.org.cn/portal/article/detail.action?categoryno=36&contentno=8291
                    contentno = part_nodes[i].xpath('./@onclick')[0]
                    contentno = re.findall(r'\d+', contentno)[0]
                    url_part2 = '/portal/article/detail.action?categoryno={}&contentno={}'\
                        .format(categoryNo, contentno)
                else:
                    url_part2 = part_nodes[i].xpath('./div[2]/h4/a/@href')[0]

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.hnast.org.cn' + url_part2

            if 'https://mp.weixin.qq.com' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count,click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath(
            '//*[@class="m-txt2"]//p//text() | ' 
            '//*[@class="m-txt2"]//strong/text() | '
            '//*[@class="m-txt2"]//span/text() | '
        )
        news_content = ''.join([x.strip() for x in news_content])
    except Exception as e:
        logging.warning("Article Content Xpath Was Error: {}".format(e))
        news_content = result.get('content')
    try:
        news_author = re.findall(r'作者：(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="txt"]//p//img/@src |'
                                        '//*[@class="txt"]//span//img/@src | '
                                        '//*[@class="txt"]//div//img/@src |'
                                        '//*[@class="txt"]//input/@src |'
                                        '//*[@class="txt"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="txt"]//span//video/@src |'
                                         ' //*[@class="txt"]//p//video/@src |'
                                         ' //*[@class="txt"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'阅读次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻中心-时政要闻': 'https://www.hnast.org.cn/channels/386_2.html',
        '首页-新闻中心-科协要闻': 'https://www.hnast.org.cn/channels/387_2.html',
        '首页-新闻中心-工作动态': 'https://www.hnast.org.cn/channels/388_2.html',
        '首页-新闻中心-省级学会': 'https://www.hnast.org.cn/channels/389_2.html',
        '首页-新闻中心-市州科协': 'https://www.hnast.org.cn/channels/390_2.html',
        '首页-新闻中心-基层科协': 'https://www.hnast.org.cn/channels/392_2.html',
        '首页-新闻中心-通知公告': 'https://www.hnast.org.cn/channels/394_2.html',
        '首页-组织建设-组织建设': 'https://www.hnast.org.cn/channels/384_2.html',
        '首页-组织建设-人才服务': 'https://www.hnast.org.cn/channels/382_2.html',
        '首页-党建引领-机关党建': 'https://www.hnast.org.cn/channels/379_2.html',
        '首页-党建引领-学会党建': 'https://www.hnast.org.cn/channels/380_2.html',
        '首页-科技创新-对外交流': 'https://www.hnast.org.cn/channels/375_2.html',
        '首页-科技创新-决策咨询': 'https://www.hnast.org.cn/channels/376_2.html',
        '首页-科技创新-学会风采': 'https://www.hnast.org.cn/channels/419_2.html',
        '首页-科技创新-政策制度': 'https://www.hnast.org.cn/channels/420_2.html',
        '首页-科学普及-科普湖南': 'https://www.hnast.org.cn/channels/371_2.html',
        '首页-科学普及-全民素质行动': 'https://www.hnast.org.cn/channels/370_2.html',
        '首页-专题专栏-小小科普员': 'https://www.hnast.org.cn/groups/12_2.html',
        '首页-专题专栏-2022年最美科学阅读': 'https://www.hnast.org.cn/groups/16_2.html',
        '首页-专题专栏-科创筑梦1979': 'https://www.hnast.org.cn/groups/13_2.html',

    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 25):
            if page == 1:
                url = value.replace('_2', '')
            else:
                url = value.replace('_2', '_' + str(page -1))
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2023-01-01')
    args = parser.parse_args()
    account_name = 'F-18 湖南省科协'
    start_run(args.projectname, args.sinceyear, account_name)
