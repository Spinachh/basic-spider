# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-02-10

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, columnid, uid, startrecord, endrecord):

    headers = {
        'Accept': 'application/xml, text/xml, */*; q=0.01',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'https://www.zast.org.cn',
        # 'Referer': url,
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Mobile Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
        'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
        'sec-ch-ua-mobile': '?1',
        'sec-ch-ua-platform': '"Android"',
    }

    params = (
        ('startrecord', startrecord),
        ('endrecord', endrecord),
        ('perpage', '8'),
    )

    data = {
        'col': '1',
        'appid': '1',
        'webid': '2807',
        'path': '/',
        'columnid': columnid,
        'sourceContentType': '1',
        'unitid': uid,
        'webname': '\u5927\u4F17\u79D1\u6280\u7F51',
        'permissiontype': '0'
    }

    response = requests.post('https://www.zast.org.cn/module/jpage/dataproxy.jsp',
                             headers=headers, params=params,
                             data=data)

    # response = requests.get(url, params=params)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    content = re.findall(r'<record><!\[CDATA\[(.*?)\]\>', html_text, re.M | re.S)
    try:
        if news_classify == '首页-会员之家':
            content_lst = content[1:]
        else:
            content_lst = content

        flag = ree_data(content_lst, news_classify, science_system, mongo, account_name, project_time, start_time)
        return flag
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(content)):
        try:
            news_title = re.findall(r'target="_blank"><span>(.*?)</', content[i])[0]
        except Exception as e:
            logging.warning('{} {} Article Title Was Error :{}'.format(account_name, news_classify, e))
            news_title = ''

        try:
            news_abstract = re.findall(r'target="_blank"><span>(.*?)</', content[i])[0]
        except Exception as e:
            logging.warning('{} {} Article Abstract Was Error :{}'.format(account_name, news_classify, e))
            news_abstract = ''

        try:
            news_publish_time = re.findall(r'/art/(.*?)/art_', content[i])[0].replace('/', '-')
            # news_publish_d = re.findall(r'<h2 style="font-size:24px; font-weight:bold">(.*?)</h2>', content[i])[0].strip()
            # news_publish_y = re.findall(r'<h5 style="font-size:13px;">(.*?)</h5>', content[i])[0].strip().replace('/', '-')
            # news_publish_time = news_publish_y + '-' + news_publish_d

        except Exception as e:
            logging.warning('{} {} Article Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                url_part2 = re.findall(r'href="(.*?)"', content[i])[0]
            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.zast.org.cn' + url_part2

            if 'mp.weixin.qq.com/' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_page_content(news_page_url)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="mian-text"]//span/text() | '
                                           '//*[@class="mian-text"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'<span id="author">(.*?)</', content_text, re.M | re.S)[0]
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="mian-text"]//span//img/@src |'
                                        ' //*[@class="mian-text"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''
    try:
        read_count = re.findall(r'浏览：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # print(click_count)
    # breakpoint()
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    # print(news_page_url)
    # breakpoint()
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-工作动态': 'https://www.zast.org.cn/col/col1673857/index.html?uid=7604179&pageNum=1',
        '首页-媒体报道': 'https://www.zast.org.cn/col/col1673864/index.html?uid=7604179&pageNum=2',
        '首页-通知公告': 'https://www.zast.org.cn/col/col1673860/index.html?uid=7688022&pageNum=1',
        '首页-省级学会': 'https://www.zast.org.cn/col/col1673858/index.html?uid=7604179&pageNum=1',
        '首页-智库-建言献策': 'https://www.zast.org.cn/col/col1229632040/index.html?uid=7602650&pageNum=1',
        '首页-智库-科技工作者建议': 'https://www.zast.org.cn/col/col1229630943/index.html?uid=7602650&pageNum=1',
        '首页-学术-学术动态': 'https://www.zast.org.cn/col/col1229630953/index.html?uid=7602650&pageNum=1',
        '首页-学术-政策法规': 'https://www.zast.org.cn/col/col1229631977/index.html?uid=7606039&pageNum=1',
        '首页-科普-科普服务': 'https://www.zast.org.cn/col/col1229632059/index.html?uid=7602650&pageNum=1',
        '首页-科普-品牌活动': 'https://www.zast.org.cn/col/col1229632058/index.html?uid=7602650&pageNum=1',
        '首页-科普-青少年科技教育': 'https://www.zast.org.cn/col/col1229632057/index.html?uid=7602650&pageNum=2',
        '首页-人才-最新动态': 'https://www.zast.org.cn/col/col1229630958/index.html?uid=7602650&pageNum=2',
        '首页-人才-优秀科技工作者': 'https://www.zast.org.cn/col/col1229630959/index.html?uid=7602650&pageNum=2',
        '首页-人才-浙里科学家': 'https://www.zast.org.cn/col/col1229631487/index.html?uid=7602650&pageNum=2',
        '首页-人才-科技追梦人': 'https://www.zast.org.cn/col/col1229631488/index.html?uid=7602650&pageNum=2',
        '首页-党建-党建动态': 'https://www.zast.org.cn/col/col1229630960/index.html?uid=7602650&pageNum=2',
        '首页-党建-学习动态': 'https://www.zast.org.cn/col/col1229631161/index.html?uid=7602650&pageNum=2',
        '首页-党建-学习资料': 'https://www.zast.org.cn/col/col1229631162/index.html?uid=7602650&pageNum=2',
        '首页-院士专家-最新动态': 'https://www.zast.org.cn/col/col1229631561/index.html?uid=7613237&pageNum=2',
        '首页-院士专家-三站平台建设': 'https://www.zast.org.cn/col/col1229631562/index.html?uid=7602650&pageNum=2',
        '首页-院士专家-青年高层次人才': 'https://www.zast.org.cn/col/col1229631567/index.html',
        '首页-院士专家-千博助千企”促共富行动': 'https://www.zast.org.cn/col/col1229631565/index.html',
        '首页-院士专家-院士专家智力集聚': 'https://www.zast.org.cn/col/col1229631568/index.html',
        '首页-市县科协-杭州': 'https://www.zast.org.cn/col/col1674356/index.html?uid=7602650&pageNum=2',
        '首页-市县科协-宁波': 'https://www.zast.org.cn/col/col1674358/index.html?uid=7602650&pageNum=2',
        '首页-市县科协-温州': 'https://www.zast.org.cn/col/col1674359/index.html?uid=7602650&pageNum=2',
        '首页-市县科协-绍兴': 'https://www.zast.org.cn/col/col1674360/index.html?uid=7602650&pageNum=2',
        '首页-市县科协-湖州': 'https://www.zast.org.cn/col/col1674361/index.html?uid=7602650&pageNum=2',
        '首页-市县科协-嘉兴': 'https://www.zast.org.cn/col/col1674362/index.html?uid=7602650&pageNum=2',
        '首页-市县科协-金华': 'https://www.zast.org.cn/col/col1674363/index.html?uid=7602650&pageNum=2',
        '首页-市县科协-衢州': 'https://www.zast.org.cn/col/col1674368/index.html?uid=7602650&pageNum=2',
        '首页-市县科协-台州': 'https://www.zast.org.cn/col/col1674369/index.html?uid=7602650&pageNum=2',
        '首页-市县科协-丽水': 'https://www.zast.org.cn/col/col1674370/index.html?uid=7602650&pageNum=2',
        '首页-市县科协-舟山': 'https://www.zast.org.cn/col/col1674371/index.html?uid=7602650&pageNum=2',

    }
    startrecord = 1
    endrecord = 24
    for key, value in urls.items():
        news_classify = key
        columnid = re.findall(r'col\/col(.*?)\/index', value)[0]
        try:
            uid = re.findall(r'uid=(.*?)&', value)[0]
        except:
            uid = ''

        while True:
            html_text = get_html(value, columnid, uid, startrecord, endrecord)
            # print(html_text)
            # breakpoint()
            flag = get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time)
            if flag != '':
                startrecord = endrecord + 1
                endrecord = endrecord + 24
                logging.warning('startrecord:{}'.format(startrecord))
                logging.warning('endrecord:{}'.format(endrecord))
            else:
                break


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'F-11 浙江省科协'
    start_run(args.projectname, args.sinceyear, account_name)
