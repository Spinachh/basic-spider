# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-04-12
import random
import re
import io
import sys
import time
import json
import argparse
import urllib3
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content_jiangsu
    return collection


def get_html(url, page, cid):


    headers = {
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/json;charset=UTF-8',
        'Origin': 'http://www.jsxhw.org',
        'Referer': url,
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/111.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    data = '{"pageNum":%s,"pageSize":20, "channelId":%s}'%(page, cid)
    response = requests.post('http://www.jsxhw.org/api/news/article/information/pageList', headers=headers,
                             data=data, verify=False)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time, cid):

    try:
        if 'special' in news_classify:
            html_text = re.findall(r'callback\((.*?)\)', html_text)[0]
            content = json.loads(html_text)

            ree_data(content, news_classify, science_system, mongo,
                     account_name, project_time, start_time)
        else:
            content = json.loads(html_text)
            deadline = ree_data(content, news_classify, science_system, mongo,
                                account_name, project_time, start_time, cid)
            if deadline:
                return deadline

    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
             account_name, project_time, start_time, cid):
    results = content['data']['list']
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for item in results:
        try:
            news_title = item.get('mainTitle').strip()
        except:
            news_title = ''
        try:
            news_abstract = item.get('mainTitle').strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = item.get('publish_time').split(' ')[0]
        except Exception as e:
            logging.warning('Publish Time Was Error: {}'.format(e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            columnId = item.get('types')
            id = item.get('id')
            # http://www.jsxhw.org/index.html?to=artDetail&mod=r&id=63532&cid=5
            news_page_url = 'https://www.sast.gov.cn/content.html?id={}&cid={}'.format(id, columnId)
            news_api_url = 'http://www.jsxhw.org/api/news/article/information/articleInfo?channel_id={}&id={}'\
                .format(cid, id)

            if 'https://mp.weixin.qq.com' not in news_api_url:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get2_page_content(news_api_url, cid, id)
            else:
                news_author, news_imgs, news_content_type, news_content, source,\
                read_count, click_count = get_weixin_page_content(news_api_url)



            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get2_page_content(news_api_url, cid, id):

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': news_api_url,
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/111.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    params = (
        ('channel_id', cid),
        ('id', id),
    )

    html_response = requests.get('http://www.jsxhw.org/api/news/article/information/articleInfo', headers=headers,
                            params=params, verify=False)

    html_response.encoding = 'utf-8'
    content_text = html_response.json()
    content_json = content_text['data'][0]
    # print(content_text['result']['article'])
    # breakpoint()
    selector_page = etree.HTML(html_response.text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(html_response)
    except:
        result = ''

    try:
        # text = re.findall(r'[\u4e00-\u9fa5]', content_text['result']['article']['content'])
        text = re.findall(r'[\u3002\uff1b\uff0c\uff1a\u201c\u201d\uff08\uff09\u3001\uff1f\u300a\u300b\u4e00-\u9fa5]',

                          content_json['content'])
        news_content = ''.join([x.strip() for x in text])
    except:
        news_content = ''
    try:
        news_author = content_json['author']
    except:
        news_author = ''

    try:
        news_imgs = re.findall(r'src="(.*?)"', content_json)[0]
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="xq_con"]//span//video/@src |'
                                         ' //*[@class="xq_con"]//p//video/@src |'
                                         ' //*[@class="xq_con"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source =content_json['source']
    except:
        source = ''

    try:
        read_count = content_json['browse']
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击数：(.*?) ', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学术学会-学会要闻': 'http://www.jsxhw.org/index.html?to=artList&mod=r&cid=5',
        '首页-学术学会-通知公告': 'http://www.jsxhw.org/index.html?to=artList&mod=r&cid=1',
        '首页-学术学会-学会党建': 'http://www.jsxhw.org/index.html?to=artList&mod=r&cid=70',
        '首页-学术学会-学会科普': 'http://www.jsxhw.org/index.html?to=artList&mod=r&cid=7',
        '首页-学术学会-科技服务': 'http://www.jsxhw.org/index.html?to=artList&mod=r&cid=8',
        '首页-学术学会-公共服务': 'http://www.jsxhw.org/index.html?to=artList&mod=r&cid=148',
        '首页-学术学会-组织建设': 'http://www.jsxhw.org/index.html?to=artList&mod=r&cid=9',
    }
    for key, value in urls.items():
        news_classify = key
        cid = re.findall(r'&cid=(\d+)', value)[0]
        for page in range(1, 50):
            if 'special' in news_classify:
                url = value.replace('pageNum=1', 'pageNum=' + str(page))
            else:
                url = value
            html_text = get_html(url, page, cid)
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time, cid)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2023-01-01')
    args = parser.parse_args()
    account_name = 'F-10 江苏省科协'
    start_run(args.projectname, args.sinceyear, account_name)
