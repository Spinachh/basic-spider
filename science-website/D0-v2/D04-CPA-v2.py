# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-06-26

import random
import re
import io
import sys
import time
import json
import urllib3
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):
    response = requests.get(url, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@id="clist"]//li')
        deadline = xpath_data(part1_nodes, news_classify,
                              science_system, mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/@title')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/@title')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0].replace('/', '-')
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'https://' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.cpa.org.cn/' + url_part2
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    # print(content_text)
    # breakpoint()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@id="clist"]//span/text() | '
                                           '//*[@id="clist"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@id="clist"]//span//img/@src |'
                                        ' //*[@id="clist"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻中心-学会动态': 'https://www.cpa.org.cn/?do=infolist&classid=213&page=1',
        '首页-新闻中心-通知公告': 'https://www.cpa.org.cn/?do=infolist&classid=214&page=1',
        '首页-新闻中心-业界新闻': 'https://www.cpa.org.cn/?do=infolist&classid=215&page=1',
        '首页-新闻中心-法律法规': 'https://www.cpa.org.cn/?do=infolist&classid=216&page=1',
        '首页-关于学会-理事会': 'https://www.cpa.org.cn/?do=infolist&classid=252&page=1',
        '首页-关于学会-组织结构-专业委员会': 'https://www.cpa.org.cn/?do=infolist&classid=353&page=1',
        '首页-关于学会-全国会员代表大会': 'https://www.cpa.org.cn/?do=infolist&classid=255&page=1',
        '首页-关于学会-年度报告': 'https://www.cpa.org.cn/?do=infolist&classid=256&page=1',
        '首页-关于学会-规章制度': 'https://www.cpa.org.cn/?do=infolist&classid=251&page=1',
        '首页-会员之家-中国药学会会讯': 'https://www.cpa.org.cn/?do=infolist&classid=260&page=1',
        '首页-会员之家-会员须知': 'https://www.cpa.org.cn/?do=infolist&classid=259&page=1',
        '首页-学术活动-会议通知': 'https://www.cpa.org.cn/?do=infolist&classid=270&page=1',
        '首页-学术活动-信息报道': 'https://www.cpa.org.cn/?do=infolist&classid=272&page=1',
        '首页-学术活动-计划总结': 'https://www.cpa.org.cn/?do=infolist&classid=273&page=1',
        '首页-国际交流-会议通知': 'https://www.cpa.org.cn/?do=infolist&classid=277&page=1',
        '首页-国际交流-信息报道': 'https://www.cpa.org.cn/?do=infolist&classid=278&page=1',
        '首页-国际交流-合作项目': 'https://www.cpa.org.cn/?do=infolist&classid=279&page=1',
        '首页-国际交流-计划总结': 'https://www.cpa.org.cn/?do=infolist&classid=281&page=1',
        '首页-编辑出版-活动通知': 'https://www.cpa.org.cn/?do=infolist&classid=285&page=1',
        '首页-编辑出版-信息报道': 'https://www.cpa.org.cn/?do=infolist&classid=286&page=1',
        '首页-编辑出版-图书编纂': 'https://www.cpa.org.cn/?do=infolist&classid=288&page=1',
        '首页-编辑出版-计划总结': 'https://www.cpa.org.cn/?do=infolist&classid=289&page=1',
        '首页-继续教育-活动通知': 'https://www.cpa.org.cn/?do=infolist&classid=293&page=1',
        '首页-继续教育-项目申报': 'https://www.cpa.org.cn/?do=infolist&classid=294&page=1',
        '首页-继续教育-信息报道': 'https://www.cpa.org.cn/?do=infolist&classid=295&page=1',
        '首页-继续教育-计划总结': 'https://www.cpa.org.cn/?do=infolist&classid=296&page=1',
        '首页-科学普及-活动通知': 'https://www.cpa.org.cn/?do=infolist&classid=300&page=1',
        '首页-科学普及-活动报道': 'https://www.cpa.org.cn/?do=infolist&classid=301&page=1',
        '首页-科学普及-科普知识': 'https://www.cpa.org.cn/?do=infolist&classid=394&page=1',
        '首页-科学普及-计划总结': 'https://www.cpa.org.cn/?do=infolist&classid=302&page=1',
        '首页-表彰奖励-通知公告': 'https://www.cpa.org.cn/?do=infolist&classid=324&page=1',
        '首页-表彰奖励-信息报道': 'https://www.cpa.org.cn/?do=infolist&classid=325&page=1',
        '首页-表彰奖励-管理制度': 'https://www.cpa.org.cn/?do=infolist&classid=332&page=1',
        '首页-表彰奖励-人才举荐': 'https://www.cpa.org.cn/?do=infolist&classid=333&page=1',
        '首页-表彰奖励-青年人才托举': 'https://www.cpa.org.cn/?do=infolist&classid=357&page=1',
        '首页-科技咨询-通知公告': 'https://www.cpa.org.cn/?do=infolist&classid=306&page=1',
        '首页-科技咨询-信息报道': 'https://www.cpa.org.cn/?do=infolist&classid=307&page=1',
        '首页-科技咨询-课题项目': 'https://www.cpa.org.cn/?do=infolist&classid=309&page=1',
        '首页-党群建设-党支部': 'https://www.cpa.org.cn/?do=infolist&classid=311&page=1',
        '首页-党群建设-工会': 'https://www.cpa.org.cn/?do=infolist&classid=312&page=1',
        '首页-党群建设-共青团': 'https://www.cpa.org.cn/?do=infolist&classid=313&page=1',
        '首页-奖项介绍-历届获奖项目介绍': 'https://www.cpa.org.cn/?do=infolist&classid=33&5page=1',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            url = value.replace('page=1', 'page=' + str(page))
            html_text, status_code = get_html(url)
            if status_code != 200:
                logging.warning('Has Not Got the Html\'s Correct Response')
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')

    args = parser.parse_args()
    account_name = 'D-04 中国药学会'
    start_run(args.projectname, args.sinceyear, account_name)
