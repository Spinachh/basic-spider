# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-06-26

import random
import re
import io
import sys
import time
import urllib3
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, columnid):
    response = requests.get(url, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    content = re.findall(r'<record><!\[CDATA\[(.*?)\]\>', html_text, re.M | re.S)
    try:
        deadline = ree_data(content, news_classify, science_system,
                            mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
             account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(content)):
        try:
            news_title = re.findall(r'target="_blank">(.*?)<p', content[i])[0]
        except:
            news_title = ''

        news_abstract = news_title

        try:
            news_publish_time = re.findall(r'/art/(.*?)/art_', content[i])[0].replace('/', '-')
            # print(news_publish_time)
            # breakpoint()
        except:
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = re.findall(r'<a href="(.*?)"', content[i])[0]

            if 'http://' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.cma.org.cn' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="bt_content"]//span/text() | '
                                           '//*[@class="bt_content"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者(.*?)</', content_text, re.M | re.S)[0]
        news_author = news_author.split('>')[-3].split('<')[0]
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="bt_content"]//span//img/@src |'
                                        ' //*[@class="bt_content"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip()
        source = source.split('>')[-3].split('<')[0]
    except:
        source = ''

    # read_count是一个接口
    try:
        read_count = re.findall(r'浏览：(.*?)<', content_text, re.M | re.S)[0].strip()
        # print(read_count)
        # breakpoint()
    except:
        read_count = ''

    try:
        columnid = re.findall(r'art_(.*?)_', news_page_url)[0]
        articleid_text = news_page_url.split('_')[-1]
        articleid = re.findall(r'\d+', articleid_text)[0]

        params = (
            ('i_columnid', columnid),
            ('i_articleid', articleid),
        )
        # 'https://www.cma.org.cn/art/2020/11/24/art_26_36647.html'
        response = requests.get('https://www.cma.org.cn/vc/vc/interface/artcount/artcount.jsp',
                                params=params, verify=False).text
        click_count = re.findall(r'document.write\("(.*?)"', response)[0]
    except:
        click_count = ''
    # print(click_count)
    # breakpoint()
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-重要通知': 'https://www.cma.org.cn/col/col128/index.html',
        '首页-党建工作-工作动态': 'https://www.cma.org.cn/col/col26/index.html',
        '首页-党建工作-党风廉政': 'https://www.cma.org.cn/col/col27/index.html',
        '首页-学术交流-会议计划': 'https://www.cma.org.cn/col/col31/index.html',
        '首页-学术交流-学术会议概览': 'https://www.cma.org.cn/col/col3124/index.html',
        '首页-学术交流-会议通知': 'https://www.cma.org.cn/col/col103/index.html',
        '首页-学术交流-征文通知': 'https://www.cma.org.cn/col/col102/index.html',
        '首页-组织建设-工作动态': 'https://www.cma.org.cn/col/col1003/index.html',
        '首页-组织建设-下载专区': 'https://www.cma.org.cn/col/col90/index.html',
        '首页-继续教育-政策资讯': 'https://www.cma.org.cn/col/col32/index.html',
        '首页-继续教育-培训动态': 'https://www.cma.org.cn/col/col33/index.html',
        '首页-继续教育-特色专科': 'https://www.cma.org.cn/col/col34/index.html',
        '首页-继续教育-培训报名': 'https://www.cma.org.cn/col/col69/index.html',
        '首页-继续教育-下载专区': 'https://www.cma.org.cn/col/col37/index.html',
        '首页-科技评审-中华医学科技奖': 'https://www.cma.org.cn/col/col38/index.html',
        '首页-科技评审-举荐优秀人才和成功': 'https://www.cma.org.cn/col/col39/index.html',
        '首页-科技评审-临床医学资金申报': 'https://www.cma.org.cn/col/col76/index.html',
        '首页-科技评审-基数规范': 'https://www.cma.org.cn/col/col41/index.html',
        '首页-科技评审-委托项目评价': 'https://www.cma.org.cn/col/col42/index.html',
        '首页-对外交流-多边交流与合作': 'https://www.cma.org.cn/col/col94/index.html',
        '首页-对外交流-双边交流与合作': 'https://www.cma.org.cn/col/col95/index.html',
        '首页-对外交流-台港澳交流与合作': 'https://www.cma.org.cn/col/col96/index.html',
        '首页-对外交流-政策文件': 'https://www.cma.org.cn/col/col54/index.html',
        '首页-对外交流-合作机构': 'https://www.cma.org.cn/col/col55/index.html',
        '首页-对外交流-国际组织': 'https://www.cma.org.cn/col/col56/index.html',
        '首页-科普与健康-科普活动': 'https://www.cma.org.cn/col/col66/index.html',
        '首页-科普与健康-健康常识': 'https://www.cma.org.cn/col/col68/index.html',
        '首页-科普与健康-科普视频': 'https://www.cma.org.cn/col/col982/index.html',
        '首页-科普与健康-科普图文': 'https://www.cma.org.cn/col/col68/index.html',
        '首页-医疗鉴定-法律法规': 'https://www.cma.org.cn/col/col58/index.html',
        '首页-医疗鉴定-工作动态': 'https://www.cma.org.cn/col/col59/index.html',
        '首页-医疗鉴定-通知公告': 'https://www.cma.org.cn/col/col60/index.html',
        '首页-医疗鉴定-案例': 'https://www.cma.org.cn/col/col62/index.html',
        '首页-医疗鉴定-业务介绍': 'https://www.cma.org.cn/col/col61/index.html',
        '首页-全继办-工作动态': 'https://www.cma.org.cn/col/col50/index.html',
        '首页-全继办-通知公告-国家级继续医学教育项目': 'https://www.cma.org.cn/col/col91/index.html',
        '首页-全继办-通知公告-六学（协）会Ⅰ类学分继续医学教育项目相关': 'https://www.cma.org.cn/col/col93/index.html',
        '首页-全继办-政策文件': 'https://www.cma.org.cn/col/col49/index.html',
        '首页-全继办-常见问题解答': 'https://www.cma.org.cn/col/col51/index.html',
        '首页-全继办-下载专区': 'https://www.cma.org.cn/col/col52/index.html',

    }
    for key, value in urls.items():
        news_classify = key
        url = value
        columnid = re.findall(r'col(\d+)/index', value)[0]
        html_text, status_code = get_html(url, columnid)
        if status_code != 200:
            logging.warning('Has Not Got the Html\'s Correct Response')
            break
        deadline = get_data(html_text, news_classify, account_name, science_system,
                            mongo, project_time, start_time)
        if deadline:
            break
        time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'D-01 中华医学会'
    start_run(args.projectname, args.sinceyear, account_name)
