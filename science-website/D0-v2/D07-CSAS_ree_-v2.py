# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-06-27

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, columnid):

    cookies = {
        'JSESSIONID': 'A3519E83D32D26D031E9913D89443A85',
    }

    headers = {
        'Accept': 'application/xml, text/xml, */*; q=0.01',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'http://www.csas.org.cn',
        'Referer': url,
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/109.0.0.0 Mobile Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    params = (
        ('startrecord', '1'),
        ('endrecord', '120'),
        ('perpage', '40'),
    )

    data = {
        'col': '1',
        'appid': '1',
        'webid': '31',
        'path': '/',
        'columnid': columnid,
        'sourceContentType': '1',
        'unitid': '15566',
        'webname': '\u4E2D\u56FD\u89E3\u5256\u5B66\u4F1A',
        'permissiontype': '0'
    }

    response = requests.post('http://www.csas.org.cn/module/web/jpage/dataproxy.jsp',
                             headers=headers, params=params,
                             cookies=cookies, data=data, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    content = re.findall(r'<record><!\[CDATA\[(.*?)\]\>', html_text, re.M | re.S)
    try:
        deadline = ree_data(content, news_classify, science_system,
                            mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(content)):
        try:
            news_title = re.findall(r'<div class="rfl">(.*?)</div', content[i])[0]
        except:
            news_title = ''

        news_abstract = news_title

        try:
            news_publish_time = re.findall(r'/art/(.*?)/art_', content[i])[0].replace('/', '-')
            # print(news_publish_time)
            # breakpoint()
        except:
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = re.findall(r'<a class="li_link" href="(.*?)"', content[i])[0]

            if 'http://' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.csas.org.cn' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="pd30"]//span/text() | '
                                           '//*[@class="pd30"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者(.*?)</', content_text, re.M | re.S)[0]
        news_author = news_author.split('>')[-3].split('<')[0]
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="pd30"]//span//img/@src |'
                                        ' //*[@class="pd30"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'document.write\("(.*?)"', content_text)[0]
    except:
        click_count = ''
    # print(click_count)
    # breakpoint()
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻-学会要闻': 'http://www.csas.org.cn/col/col2924/index.html',
        '首页-新闻-通知公告': 'http://www.csas.org.cn/col/col2924/index.html',
        '首页-新闻-分会动态': 'http://www.csas.org.cn/col/col2926/index.html',
        '首页-新闻-行业资讯': 'http://www.csas.org.cn/col/col2927/index.html',
        '首页-新闻-精彩图集': 'http://www.csas.org.cn/col/col2928/index.html',
        '首页-新闻-精彩视频': 'http://www.csas.org.cn/col/col2929/index.html',
        '首页-党建-党的二十大精神': 'http://www.csas.org.cn/col/col8197/index.html',
        '首页-党建-科学精神': 'http://www.csas.org.cn/col/col4360/index.html',
        '首页-党建-党史学习': 'http://www.csas.org.cn/col/col4359/index.html',
        '首页-党建-理论学习': 'http://www.csas.org.cn/col/col2938/index.html',
        '首页-党建-党建学习': 'http://www.csas.org.cn/col/col2937/index.html',
        '首页-智库-智库动态': 'http://www.csas.org.cn/col/col2941/index.html',
        '首页-智库-智库成果专家观点': 'http://www.csas.org.cn/col/col2942/index.html',
        '首页-智库-专业分会': 'http://www.csas.org.cn/col/col2943/index.html',
        '首页-智库-期刊杂志': 'http://www.csas.org.cn/col/col2944/index.html',
        '首页-智库-智库成果': 'http://www.csas.org.cn/col/col2991/index.html',
        '首页-学术-学术活动': 'http://www.csas.org.cn/col/col2945/index.html',
        '首页-学术-国内会议': 'http://www.csas.org.cn/col/col2946/index.html',
        '首页-学术-会议纪要': 'http://www.csas.org.cn/col/col2947/index.html',
        '首页-学术-国际会议': 'http://www.csas.org.cn/col/col2948/index.html',
        '首页-学术-杂志期刊': 'http://www.csas.org.cn/col/col4461/index.html',
        '首页-科普-科普新闻': 'http://www.csas.org.cn/col/col2999/index.html',
        '首页-科普-科普知识': 'http://www.csas.org.cn/col/col3000/index.html',
        '首页-科普-科普教育基地': 'http://www.csas.org.cn/col/col4454/index.html',

    }
    for key, value in urls.items():
        news_classify = key
        url = value
        columnid = re.findall(r'col\/col(.*?)\/index', value)[0]
        html_text, status_code = get_html(url, columnid)
        if status_code != 200:
            logging.warning('Has Not Got the Html\'s Correct Response')
            break
        deadline = get_data(html_text, news_classify, account_name,
                            science_system, mongo, project_time, start_time)
        if deadline:
            break
        time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'D-07 中国解剖学会'
    start_run(args.projectname, args.sinceyear, account_name)
