# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-04-10

import random
import re
import io
import sys
import time
import json
import argparse
import urllib3
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash(url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, verify=False)
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, page):
    if 'c1' in url:
        c1 = re.findall(r'c1=(.*?)&', url)
        params = (
            ('c1', c1),
            ('p', page),
        )
    elif 'c2' in url:
        c2 = re.findall(r'c2=(.*?)&', url)
        params = (
            ('c2', c2),
            ('p', page),
        )

    headers = {
        'authority': 'rczx.cast.org.cn',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'accept-language': 'zh-CN,zh;q=0.9',
        # 'cookie': 'PSESS=eu9jr175ngkof892kv6lctap65; Hm_lvt_c740bb5f417a795bcd133529f2cd64b4=1678085940; Hm_lpvt_c740bb5f417a795bcd133529f2cd64b4=1678151766',
        'referer': 'https://rczx.cast.org.cn/index/article_list/c1/1',
        'sec-ch-ua': '"Chromium";v="110", "Not A(Brand";v="24", "Google Chrome";v="110"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
    }
    response = requests.get('https://rczx.cast.org.cn/index/article_list.html',
                            headers=headers, params=params)
    response.encoding = 'utf-8'
    status_code = response.status_code
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        if 'special' not in news_classify:
            part1_nodes = selector.xpath('//*[@class="item-box"]')
        else:
            part1_nodes = selector.xpath('//*[@class="listadd wrap"]/ul/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system, mongo,
                              account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('./div/div/div[1]/p/text()')[0]
            else:
                news_title = part_nodes[i].xpath('.//a/div[2]/h2/text()')[0].strip()
        except Exception as e:
            logging.warning('News Title Was Error: {}'.format(e))
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = part_nodes[i].xpath('./div/div/div[2]/p/text()')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('.//a/div[2]/p/text()')[0].strip()
        except Exception as e:
            logging.warning('News Abstract Was Error: {}'.format(e))
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_time = part_nodes[i].xpath('./div/div/div[3]/p/text()')[0].split(' ')[0]
            else:
                news_publish_d = part_nodes[i].xpath('.//h5/text()')[0]
                news_publish_y = part_nodes[i].xpath('.//h6/text()')[0]
                news_publish_time = news_publish_y + '-' + news_publish_d
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('./div/@onclick')[0]
                    url_part2 = re.findall(r"window.open\('(.*?)'\)", url_part2)[0]
                else:
                    url_part2 = part_nodes[i].xpath('./a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'https' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://rczx.cast.org.cn/' + url_part2

            if 'mp.weixin.qq.c' in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                    read_count, click_count = get_weixin_page_content(news_page_url)

            elif 'news.cn' in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                    read_count, click_count = get_news_page_content(news_page_url)

            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                    read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}.'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
    }

    try:
        html_response = requests.get(news_page_url, headers=headers, verify=False, )
        html_response.encoding = 'utf-8'
        content_text = html_response.text
    except Exception as e:
        logging.warning('Article Content Was Error :{}'.format(e))
        content_text = ''
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath(
            '//*[@class="content"]//p/span//text() |'
            '//*[@class="content"]/div//p//text() |'
            '//*[@class="content"]/p//text() '
        )
        news_content = ''.join([x.strip() for x in news_content])

    except Exception as e:
        logging.warning("Article Content Xpath Was Error: {}".format(e))
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者: <b>(.*?)</', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="content"]//p//img/@src |'
                                        '//*[@class="content"]//span//img/@src | '
                                        '//*[@class="content"]//div//img/@src |'
                                        '//*[@class="content"]//input/@src |'
                                        '//*[@class="content"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="content"]//span//video/@src |'
                                         ' //*[@class="content"]//p//video/@src |'
                                         ' //*[@class="content"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r"来源：(.*?)</", content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r"阅读量：(.*?)</", content_text, re.M | re.S)[-1]
    except:
        # logging.warning('Read Count Was Error :{}'.format(e))
        read_count = ''

    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_news_page_content(news_page_url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
    }

    try:
        html_response = requests.get(
            news_page_url, headers=headers,
            verify=False)
        html_response.encoding = 'utf-8'
        content_text = html_response.text
    except Exception as e:
        logging.warning('Article Content Was Error :{}'.format(e))
        content_text = ''
    # print('get news content text')
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = result.get('content')
    except Exception as e:
        news_content = selector_page.xpath(
            '//*[@id="detail"]//p/span//text() |'
            '//*[@id="detail"]//div//text() |'
            '//*[@id="detail"]/p//text() '
            '//*[@id="detail"]/p/font/text() '
        )
        news_content = ''.join([x.strip() for x in news_content])

    try:
        news_author = re.findall(r'作者: <b>(.*?)</', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@id="detail"]//p//img/@src |'
                                        '//*[@id="detail"]//span//img/@src | '
                                        '//*[@id="detail"]//div//img/@src |'
                                        '//*[@id="detail"]//input/@src |'
                                        '//*[@id="detail"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        # news_video = selector_page.xpath('//*[@id="detail"]//video/')
        news_video = re.findall(r'video_src="(.*?)"', content_text, re.M | re.S)[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video = ''
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r"来源：(.*?)</", content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r"阅读量：(.*?)</", content_text, re.M | re.S)[0].strip()
    except:
        # logging.warning('Read Count Was Error :{}'.format(e))
        read_count = ''

    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-工作动态-要闻': 'https://rczx.cast.org.cn/index/article_list.html?c1=1&p=',
        '首页-工作动态-中心动态': 'https://rczx.cast.org.cn/index/article_list.html?c1=2&p=',
        '首页-工作动态-通知公告': 'https://rczx.cast.org.cn/index/article_list.html?c1=3&p=',
        '首页-工作动态-政策法规': 'https://rczx.cast.org.cn/index/article_list.html?c1=4&p=',
        '首页-培训研修-干部培训': 'https://rczx.cast.org.cn/column/school_list.html?c2=2&p=',
        '首页-培训研修-人才研修': 'https://rczx.cast.org.cn/column/school_list.html?c2=3&p=',
        '首页-奖励举荐-奖励动态': 'https://rczx.cast.org.cn/reward/reward_list.html?c2=5&p=',
        '首页-工程能力建设-中国工程师联合体': 'https://rczx.cast.org.cn/column/build_list.html?c2=9&p=',
        '首页-工程能力建设-国际交流合作': 'https://rczx.cast.org.cn/column/build_list.html?c2=12&p=',
        '首页-工程能力建设-国际交组织动态': 'https://rczx.cast.org.cn/column/build_list.html?c2=18&p=',

    }
    for key, value in urls.items():
        news_classify = key

        for page in range(1, 50):
            url = value + str(page)
            html_text, status_code = get_html(url, page)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                logging.warning('{} Account: {} Classfiy: {} DeadLine.{}'
                                .format('*' * 15, account_name, news_classify, '*' * 15))
                break


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default', default='2023-01-01')
    args = parser.parse_args()
    account_name = 'DU-12 中国科协培训和人才服务中心'
    start_run(args.projectname, args.sinceyear, account_name)
