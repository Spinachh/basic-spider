# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-03-07

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, v):

    params = (
        ('v', v),
    )
    # url = 'https://www.scei.org.cn/js/9/mi4_sub_articles_20230217.js?'
    response = requests.get(url, params=params)
    response.encoding = 'utf-8'
    status_code = response.status_code
    html_text = response.text
    if '404 Not Found' not in html_text:
        return status_code, html_text
    else:
        html_text = ''
        status_code = 404
        return status_code, html_text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    try:
        if 'special' in news_classify:
            html_text = re.findall(r'callback\((.*?)\)', html_text)[0]
            content = json.loads(html_text)
            ree_data(content, news_classify, science_system, mongo,
                     account_name, project_time, start_time)
        else:
            html_text = re.findall(r'var MI4_PAGE_ARTICLE = \[(.*?)\]$', html_text)
            ree_data(html_text, news_classify, science_system, mongo,
                     account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(results, news_classify, science_system, mongo,
             account_name, project_time, start_time):

    news_dict_name = [
        'news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
        'news_content_type', 'news_content', 'news_page_url', 'source',
        'news_author', 'read_count', 'click_count', 'news_classify',
        'crawl_time', 'account_name', 'science_system', 'project_time'
    ]
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for item in results:
        item = json.loads(item)

        try:
            news_title = item.get('title').strip()
        except:
            news_title = ''

        try:
            news_abstract = item.get('miSummary').strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = item.get('pub_date').split(' ')[0]
        except Exception as e:
            logging.warning('Publish Time Was Error: {}'.format(e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            news_page_url = item.get('url')
            news_api_url = item.get('url')

            if 'mp.weixin.qq.' not in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_api_url, item)
            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_api_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_api_url, item):

    try:
        news_content = item.get('miContentTxt')

    except:
        news_content = ''

    try:
        news_author = item.get('miRespAuthor')
    except:
        news_author = ''

    try:

        news_imgs = item.get('miCover11')

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = item.get('miSource')
    except:
        source = ''

    try:
        read_count = item.get('miRead')
    except:
        read_count = ''

    try:
        click_count = item.get('miClick')
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def get_date(science_system, mongo, project_time, start_time, news_classify, value, month, day):

    v_dic = {
        '首页-工作动态-中心动态': '20230217110935',
        '首页-工作动态-通知公告': '20221125172242',
        '首页-党建工作-党建动态': '20220621181321',
        '首页-党建工作-学习园地': '20220621181321',
        '首页-科协要闻-科协要闻': '20221018082641',
    }
    v = v_dic.get(news_classify)

    url = value + '2023{}{}.js?v={}'.format(month, day, v)
    crontrol_time = '2023-{}-{}'.format(month, day)
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    crontrol_time_stamp = time.mktime(time.strptime(crontrol_time, '%Y-%m-%d'))

    if int(start_time_stamp) < int(crontrol_time_stamp) < int(time.time()):
        status_code, html_text = get_html(url, v)
        if status_code == 200:
            try:
                get_data(html_text, news_classify, account_name,
                         science_system, mongo, project_time, start_time)
            except Exception as e:
                logging.warning('Get Data Was Error:{}'.format(e))
                return


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-工作动态-中心动态': 'https://www.scei.org.cn/js/9/mi4_sub_articles_',
        '首页-工作动态-通知公告': 'https://www.scei.org.cn/js/23/mi4_sub_articles_',
        '首页-党建工作-党建动态': 'https://www.scei.org.cn/js/53/mi4_sub_articles_',
        '首页-党建工作-学习园地': 'https://www.scei.org.cn/js/28/mi4_sub_articles_',
        '首页-科协要闻-科协要闻': 'https://www.scei.org.cn/js/8/mi4_sub_articles_',
    }
    month_31 = ['01', '03', '05', '07', '08', '10', '12']
    month_30 = ['04', '06', '09', '11']
    days = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17',
            '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'
            ]
    for key, value in urls.items():
        news_classify = key
        for month in range(1, 13):
            if month < 10:
                month = '0{}'.format(month)
            if month in month_31:
                for day in days:
                    get_date(science_system, mongo, project_time, start_time, news_classify, value, month, day)
            elif month in month_30:
                for day in days[0:-1]:
                    get_date(science_system, mongo, project_time, start_time, news_classify, value, month, day)
            else:
                for day in days[0:-3]:
                    get_date(science_system, mongo, project_time, start_time, news_classify, value, month, day)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2022-07-01')
    args = parser.parse_args()
    account_name = 'DU-06 中国科协企业创新服务中心'
    start_run(args.projectname, args.sinceyear, account_name)
