# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-03-09

import random
import re
import io
import sys
import time
import json
import argparse
import urllib3
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account


sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash(url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, verify=False)
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url):

    response = requests.get(url, verify=False,)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time, column_name):
    selector = etree.HTML(html_text)

    try:
        if 'special' not in news_classify:
            part1_nodes = selector.xpath('//*[@id="sub"]/li')
        else:
            part1_nodes = selector.xpath('//*[@class="listadd wrap"]/ul/li')
        xpath_data(part1_nodes, news_classify, science_system, mongo,
                   account_name, project_time, start_time, column_name)
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time, column_name):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('.//h4/text()')[0]
            else:
                news_title = part_nodes[i].xpath('.//a/div[2]/h2/text()')[0].strip()
        except Exception as e:
            logging.warning('News Title Was Error: {}'.format(e))
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = part_nodes[i].xpath('.//p/text()')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('.//a/div[2]/p/text()')[0].strip()
        except Exception as e:
            logging.warning('News Abstract Was Error: {}'.format(e))
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_d = part_nodes[i].xpath('.//h5/text()')[0]
                news_publish_y = part_nodes[i].xpath('.//h6/text()')[0]
                news_publish_time = news_publish_y + '-' + news_publish_d
            else:
                news_publish_time = part_nodes[i].xpath('./a/div[2]/p[1]/text()')[0].strip()

        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('./a/@href')[0].replace('./', '/')
                else:
                    url_part2 = part_nodes[i].xpath('./a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'https' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.agritech.org.cn' + column_name + url_part2

            if 'mp.weixin.qq.co' in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_page_url)

            elif 'news.cn' in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_news_page_content(news_page_url)

            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:

            return ''


def get_page_content(news_page_url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
    }

    try:
        html_response = requests.get(news_page_url, headers=headers, verify=False,)
        html_response.encoding = 'utf-8'
        content_text = html_response.text
    except Exception as e:
        logging.warning('Article Content Was Error :{}'.format(e))
        content_text = ''
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath(
            '//*[@class="TRS_Editor"]//p/span//text() |'
            '//*[@class="TRS_Editor"]/p//text() '
        )
        news_content = ''.join([x.strip() for x in news_content])

    except Exception as e:
        logging.warning("Article Content Xpath Was Error: {}".format(e))
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者: <b>(.*?)</', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="TRS_Editor"]//p//img/@src |'
                                        '//*[@class="TRS_Editor"]//span//img/@src | '
                                        '//*[@class="TRS_Editor"]//div//img/@src |'
                                        '//*[@class="TRS_Editor"]//input/@src |'
                                        '//*[@class="TRS_Editor"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="TRS_Editor"]//span//video/@src |'
                                         ' //*[@class="TRS_Editor"]//p//video/@src |'
                                         ' //*[@class="TRS_Editor"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r"来源：(.*?)</", content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r"阅读量：(.*?)</", content_text, re.M | re.S)[-1]
    except :
        # logging.warning('Read Count Was Error :{}'.format(e))
        read_count = ''

    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_news_page_content(news_page_url):

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
    }

    try:
        html_response = requests.get(
            news_page_url, headers=headers,
            verify=False)
        html_response.encoding = 'utf-8'
        content_text = html_response.text
    except Exception as e:
        logging.warning('Article Content Was Error :{}'.format(e))
        content_text = ''
    # print('get news content text')
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = result.get('content')
    except Exception as e:
        news_content = selector_page.xpath(
            '//*[@id="detail"]//p/span//text() |'
            '//*[@id="detail"]//div//text() |'
            '//*[@id="detail"]/p//text() '
            '//*[@id="detail"]/p/font/text() '
        )
        news_content = ''.join([x.strip() for x in news_content])

    try:
        news_author = re.findall(r'作者: <b>(.*?)</', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@id="detail"]//p//img/@src |'
                                        '//*[@id="detail"]//span//img/@src | '
                                        '//*[@id="detail"]//div//img/@src |'
                                        '//*[@id="detail"]//input/@src |'
                                        '//*[@id="detail"]//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        # news_video = selector_page.xpath('//*[@id="detail"]//video/')
        news_video = re.findall(r'video_src="(.*?)"', content_text, re.M | re.S)[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video = ''
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r"来源：(.*?)</", content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r"阅读量：(.*?)</", content_text, re.M | re.S)[0].strip()
    except :
        # logging.warning('Read Count Was Error :{}'.format(e))
        read_count = ''

    try:
        click_count = re.findall(r'点击量：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-时政要闻': 'https://www.agritech.org.cn/szyw/index_1.html',
        '首页-科协要闻': 'https://www.agritech.org.cn/yw/index_1.html',
        '首页-工作动态': 'https://www.agritech.org.cn/gzdt/index_1.html',
        '首页-通知公告': 'https://www.agritech.org.cn/tzgg/index_1.html',
        '首页-政策文件': 'https://www.agritech.org.cn/zcwj/index_1.html',
        '首页-科技志愿-工作动态': 'https://www.agritech.org.cn/kjzy/gzdt_kjzy/index_1.html',
        '首页-科技志愿-政策文件': 'https://www.agritech.org.cn/kjzy/zcwj_kjzy/index_1.html',
        '首页-科技志愿-使用指南': 'https://www.agritech.org.cn/kjzy/syzn/index_1.html',
        '首页-乡村振兴-工作动态': 'https://www.agritech.org.cn/xczx/gzdt_xczx/index_1.html',
        '首页-乡村振兴-通知公告': 'https://www.agritech.org.cn/xczx/tzgg_xczx/index_1.html',
        '首页-乡村振兴-政策文件': 'https://www.agritech.org.cn/xczx/zcwj_xczx/index_1.html',
        '首页-乡村振兴-典型示范': 'https://www.agritech.org.cn/xczx/dxsf/index_1.html',
        '首页-农技协-通知公告': 'https://www.agritech.org.cn/njx/tzgg_njx/index_1.html',
        '首页-农技协-协会动态': 'https://www.agritech.org.cn/njx/xhdt/index_1.html',
        '首页-基层科普-工作动态': 'https://www.agritech.org.cn/jckp/jckp_gzdt/index_1.html',
        '首页-基层科普-通知公告': 'https://www.agritech.org.cn/jckp/tzgg/index_1.html',
        '首页-基层科普-活动展示': 'https://www.agritech.org.cn/jckp/hdzs/index_1.html',

    }
    for key, value in urls.items():
        news_classify = key
        # supFlash = get_supflash(value)
        for page in range(1, 10):
            if page == 1:
                url = value.replace('_1', '')
            else:
                url = value.replace('_1', '_' + str(page))
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            column_name = re.sub(r'https://www.agritech.org.cn', '', value).replace('/index_1.html', '')
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time, column_name)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'DU-11 中国科协农村专业技术服务中心'
    start_run(args.projectname, args.sinceyear, account_name)
