# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-07-03

import random
import re
import io
import sys
import time
import json
import cchardet
import urllib3
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash():
    cookies = {
        'td_cookie': '2095021031',
        'Hm_lvt_ec22f37ea1cce8f86744c8d3c25d6c4e': '1667870775',
        'Hm_lpvt_ec22f37ea1cce8f86744c8d3c25d6c4e': '1667870812',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.botany.org.cn/xwzx/xwdt_/index_1.html',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get('http://www.botany.org.cn/xwzx/xwdt_/index.html', headers=headers, cookies=cookies,
                            verify=False)
    html_text = response.text
    # print(html_text)
    # breakpoint()
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, id):
    cookies = {
        'td_cookie': '2614018894',
        'Hm_lvt_bfcbb2206a4b6c6d5c8423271d6878d8': '1668389754',
        'Hm_lvt_1c9b587bc335ba8b679b57a33771c324': '1668389754',
        'Hm_lpvt_bfcbb2206a4b6c6d5c8423271d6878d8': '1668389812',
        'Hm_lpvt_1c9b587bc335ba8b679b57a33771c324': '1668389812',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.chinacses.org/xw/szyw/',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Mobile Safari/537.36',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time, url_part):
    selector = etree.HTML(html_text)
    # part1
    try:
        part1_nodes = selector.xpath('//*[@class="inbox"]/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system, mongo,
                              account_name, project_time, start_time, url_part)
        if deadline:
            return deadline
    except Exception as e:
        print('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time, url_part):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/@title')[0]
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0]
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0]
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0].replace('./', '')

            if 'mp.weixin.qq.com' in url_part2:
                news_page_url = url_part2
            elif 'www.mee.gov.cn' in url_part2:
                news_page_url = url_part2
            else:
                url = url_part + url_part2
                news_page_url = 'http://www.chinacses.org' + url

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="TRS_Editor"]//div/img/@src | //*[@class="TRS_Editor"]//p/img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        read_count = re.findall(r'浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        source = re.findall(r'>来源：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        click_count = re.findall(r'>点击量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻-时政要闻': 'http://www.chinacses.org/xw/szyw/index_1.shtml',
        '首页-新闻-环境要闻': 'http://www.chinacses.org/xw/sthjbfb/index_1.shtml',
        '首页-新闻-学会动态': 'http://www.chinacses.org/xw/xhdt/index_1.shtml',
        '首页-新闻-培训通知': 'http://www.chinacses.org/xw/pxtz/index_1.shtml',
        '首页-新闻-会议通知': 'http://www.chinacses.org/xw/hytz/index_1.shtml',
        '首页-新闻-公示公告': 'http://www.chinacses.org/xw/gsgg/index_1.shtml',
        '首页-新闻-秘书处通知': 'http://www.chinacses.org/xw/msctz/index_1.shtml',
        '首页-新闻-分支机构动态': 'http://www.chinacses.org/xw/fzjgdt/index_1.shtml',
        '首页-新闻-地方动态学会': 'http://www.chinacses.org/xw/dfxhdt/index_1.shtml',
        '首页-新闻-科普天地': 'http://www.chinacses.org/xw/kptd/index_1.shtml',
        '首页-新闻-分支机构通知': 'http://www.chinacses.org/xw/fzjgtz/index_1.shtml',
        '首页-新闻-地方学会通知': 'http://www.chinacses.org/xw/dfxhtz/index_1.shtml',
        '首页-新闻-党建强会-党建动态': 'http://www.chinacses.org/xw/djqh/djdt/index_1.shtml',
        '首页-新闻-党建强会-党史学习教育': 'http://www.chinacses.org/xw/djqh/dsxxjy/index_1.shtml',
        '首页-学术交流-专题议会': 'http://www.chinacses.org/xsjl/zthy/index_1.shtml',
        '首页-学术交流-专题议会-全年会议安排': 'http://www.chinacses.org/xsjl/zthy/qnhyyp/index_1.shtml',
        '首页-学术交流-专题议会-会议报道': 'http://www.chinacses.org/xsjl/zthy/hybd/index_1.shtml',
        '首页-学术交流-专题议会-下载中心': 'http://www.chinacses.org/xsjl/zthy/xzzx/index_1.shtml',
        '首页-学术交流-科学技术交流': 'http://www.chinacses.org/xsjl/kxjsnh/index_1.shtml',
        '首页-学术交流-科学技术交流-历届回顾': 'http://www.chinacses.org/xsjl/kxjsnh/ljhg/index_1.shtml',
        '首页-学术交流-科学技术交流-下载中心': 'http://www.chinacses.org/xsjl/kxjsnh/xzzx_86/index_1.shtml',
        '首页-环境科普-环保科普新闻': 'http://www.chinacses.org/hjkp_23038/hbkpxw/index_1.shtml',
        '首页-环境科普-科普奖励': 'http://www.chinacses.org/hjkp_23038/kpjl/index_1.shtml',
        '首页-国际交流-新闻动态': 'http://www.chinacses.org/gjjl_23121/xwdt_108/index_1.shtml',
        '首页-国际交流-CSES-ACS联合科学论坛': 'http://www.chinacses.org/gjjl_23121/lhkxlt/index_1.shtml',
        '首页-国际交流-CSES-SG会议': 'http://www.chinacses.org/gjjl_23121/sghuiyi/index_1.shtml',
        '首页-国际交流-CSES-两岸四地': 'http://www.chinacses.org/gjjl_23121/lasd/index_1.shtml',
        '首页-国际交流-国际环保门户': 'http://www.chinacses.org/gjjl_23121/gjhbmh/index_1.shtml',
        '首页-国际交流-损害鉴定': 'http://www.chinacses.org/shjd/index_1.shtml',
        '首页-咨询评价-工程中心-工作动态': 'http://www.chinacses.org/zxpj/gczx/gzdt/index_1.shtml',
        '首页-咨询评价-工程中心-业绩展示': 'http://www.chinacses.org/zxpj/gczx/yjzs/index_1.shtml',
        '首页-咨询评价-工程中心-业绩展示-科技进展': 'http://www.chinacses.org/zxpj/gczx/yjzs/kjjz/index_1.shtml',
        '首页-咨询评价-工程中心-业绩展示-能力建设': 'http://www.chinacses.org/zxpj/gczx/yjzs/nljs/index_1.shtml',
        '首页-咨询评价-工程中心-业绩展示-管理服务': 'http://www.chinacses.org/zxpj/gczx/yjzs/glfw/index_1.shtml',
        '首页-咨询评价-工程中心-业绩展示-合作交流': 'http://www.chinacses.org/zxpj/gczx/yjzs/hzyjl/index_1.shtml',
        '首页-咨询评价-工程中心-业绩展示-社会化服务': 'http://www.chinacses.org/zxpj/gczx/yjzs/shhfw/index_1.shtml',
        '首页-咨询评价-工程中心-业绩展示-获奖荣誉': 'http://www.chinacses.org/zxpj/gczx/yjzs/jxry/index_1.shtml',
        '首页-咨询评价-工程中心-工程中心-信息公开': 'http://www.chinacses.org/zxpj/gczx/xxgk/index_1.shtml',
    }

    for key, value in urls.items():
        news_classify = key
        url_part = re.findall(r'chinacses.org(.*?)index_1', value)[0]
        for page in range(1, 50):
            if page == 1:
                url = value.replace('index_1', 'index')
            else:
                url = value.replace('index_1', 'index_' + str(page - 1))
            html_text, status_code = get_html(url, id)
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name, science_system, mongo,
                                project_time, start_time, url_part)
            if deadline:
                break


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q2')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default',
                        default='2023-04-01')
    args = parser.parse_args()
    account_name = 'A-29 中国环境科学学会'
    start_run(args.projectname, args.sinceyear, account_name)
