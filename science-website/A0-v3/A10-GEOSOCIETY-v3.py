# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-07-03

import random
import re
import io
import sys
import time
import json
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_supflash(url):
    cookies = {
        'td_cookie': '1579202279',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.geosociety.org.cn/?',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    params = (
        ('geomcat', 'c3Vi'),
        ('geomenuS', 'am9iZ2Vv'),
    )

    response = requests.get(url, headers=headers, params=params, cookies=cookies, verify=False)
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, us, page):
    cookies = {
        'td_cookie': get_supflash(url),
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Referer': 'http://www.geosociety.org.cn/?geomcat=c3Vi&geomenuS={}&geopage={}'.format(us, page),
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    params = (
        ('geomcat', 'c3Vi'),
        ('geomenuS', us),
        ('geopage', page),
    )

    response = requests.get(url, headers=headers, params=params,
                            cookies=cookies, verify=False)
    status_code = response.status_code
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo,
             project_time, page, start_time):
    selector = etree.HTML(html_text)

    # part1
    try:
        part1_nodes = selector.xpath('//*[@style="text-align:justify;padding:10px;"]')
        deadline = xpath_data(part1_nodes, page, news_classify, science_system, mongo,
                              account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        print('Part1 has not content: {}'.format(e))

    # part2
    try:
        part2_nodes = selector.xpath('//*[@style="height:160px;border:1px #dddddd solid;padding:20px;"]//tr/td')
        # print(part2_nodes)
        # breakpoint()
        deadline = xpath_data(part2_nodes, page, news_classify, science_system, mongo,
                              account_name, project_time, start_time)
        if deadline:
            return deadline
    except:
        print('Classfiy: {} Part2 has not content'.format(news_classify))


def xpath_data(part_nodes, page, news_classify, science_system, mongo, account_name, project_time,
               start_time):
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    for i in range(len(part_nodes)):
        news_title = part_nodes[i].xpath('./a/text()')[0]
        news_abstract = part_nodes[i].xpath('./a/text()')[0]
        news_publish_time = part_nodes[i].xpath('./text()')[-1].strip()
        # print(news_publish_time)
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        if int(news_publish_stamp) >= int(start_time_stamp):
            news_page_url = 'http://www.geosociety.org.cn/' + part_nodes[i].xpath('./a/@href')[0]

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url, page)
            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))

            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} '
                            .format(account_name, news_classify, news_title[:20], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url, page):
    cookies = {
        'td_cookie': get_supflash(news_page_url)
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.geosociety.org.cn/?geomcat=ZGV0YWls&geomenuS=am9iZ2Vv&geoshort=cHJvdmluOmpvYjEzMTMw'
                   '&geoshow=dHh0aW1n&geopage={}'.format(page),
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    params = (
        ('geomcat', 'ZGV0YWls'),
        ('geomenuS', 'am9iZ2Vv'),
        ('geoshort', 'cHJvdmluOmpvYjEzMTMw'),
        ('geoshow', 'dHh0aW1n'),
        ('geopage', '1'),
    )

    html_response = requests.get(news_page_url, headers=headers, cookies=cookies,
                                 verify=False)
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = re.findall(r'<img src(.*?)>', content_text, re.M | re.S)
        if len(news_imgs) > 17:
            news_content_type = 'text-img'
            news_imgs = news_imgs[17:19]
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'>来源 : (.*?) ', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''
    try:
        click_count = re.findall(r'>阅读次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        # '首页-工作动态': 'http://www.geosociety.org.cn/?geomcat=c3Vi&geomenuS=am9iZ2Vv&geopage=1',
        # '首页-通知公告': 'http://www.geosociety.org.cn/?geomcat=c3Vi&geomenuS=bm90aWNl&geopage=1',
        # '首页-图文视频': 'http://www.geosociety.org.cn/?geomcat=c3Vi&geomenuS=aW1nbXY=&geopage=1',
        # '首页-分支机构工作动态': 'http://www.geosociety.org.cn/?geomcat=c3Vi&geomenuS=am9iYnJhbmNo&geopage=1',
        # '首页-省级协会工作动态': 'http://www.geosociety.org.cn/?geomcat=c3Vi&geomenuS=am9icHJvdmlu&geopage=1',
        # '首页-会议-通告通知': 'http://www.geosociety.org.cn/?geomcat=c3Vi&geomenuS=bWVldGluZ25vdGljZQ==&geopage=1',
        # '首页-会议-会议要闻': 'http://www.geosociety.org.cn/?geomcat=c3Vi&geomenuS=bWVldGluZ2pvYg==&geopage=1',
        '首页-学术交流-国内学术交流': 'http://www.geosociety.org.cn/?geomcat=c3Vi&geomenuS=ZXhjaGFuZ2Vpbg==&geopage=1',
        '首页-学术交流-国外学术交流': 'http://www.geosociety.org.cn/?geomcat=c3Vi&geomenuS=ZXhjaGFuZ2VvdXQ=&geopage=1',
        '首页-表彰奖励通知公告': 'http://www.geosociety.org.cn/?geomcat=c3Vi&geomenuS=cmV3YXJkbm90aWNl&geopage=1',
        '首页-中国地址学会会士': 'http://www.geosociety.org.cn/?geomcat=c3Vi&geomenuS=cmV3YXJkZmVsbG93&geopage=1',
        '首页-期刊集群通知公告': 'http://www.geosociety.org.cn/?geomcat=c3Vi&geomenuS=cHVibm90aWNl&geopage=1',
        '首页-期刊集群工作动态': 'http://www.geosociety.org.cn/?geomcat=c3Vi&geomenuS=cHViam9i&geopage=1',
        '首页-会员服务': 'http://www.geosociety.org.cn/?geomcat=c3Vi&geomenuS=c2VydmljZQ==&geopage=1',
        '首页-诚信体系': 'http://www.geosociety.org.cn/?geomcat=c3Vi&geomenuS=aG9uZXN0eQ==&geopage=1',
        '首页-科技咨询': 'http://www.geosociety.org.cn/?geomcat=c3Vi&geomenuS=YWR2aWNl&geopage=1',
        '首页-党建': 'http://www.geosociety.org.cn/?geomcat=c3Vi&geomenuS=cGFydHk=&geopage=1',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            url = value.replace('geopage=1', 'geopage=' + str(page))
            us = re.findall(r'geomenuS=(.*?)&', url)[0].strip()
            html_text, status_code = get_html(url, us, page)
            # print(html_text)
            # breakpoint()
            if status_code == 404:
                break
            deadline = get_data(html_text, news_classify, account_name, science_system, mongo, project_time, page,
                                start_time)
            if deadline:
                break
        time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q2')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters,'
                                                  ' has default',
                        default='2023-04-01')
    args = parser.parse_args()
    account_name = 'A-10 中国地质学会'
    start_run(args.projectname, args.sinceyear, account_name)
