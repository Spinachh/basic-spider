# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-01-31

import random
import re
import io
import sys
import time
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, columnid):

    cookies = {
        'JSESSIONID': 'A3519E83D32D26D031E9913D89443A85',
    }

    headers = {
        'Accept': 'application/xml, text/xml, */*; q=0.01',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'http://www.csas.org.cn',
        'Referer': url,
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Mobile Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    params = (
        ('startrecord', '1'),
        ('endrecord', '120'),
        ('perpage', '40'),
    )

    data = {
        'col': '1',
        'appid': '1',
        'webid': '31',
        'path': '/',
        'columnid': columnid,
        'sourceContentType': '1',
        'unitid': '15566',
        'webname': '\u4E2D\u56FD\u89E3\u5256\u5B66\u4F1A',
        'permissiontype': '0'
    }

    response = requests.post('http://www.csas.org.cn/module/web/jpage/dataproxy.jsp',
                             headers=headers, params=params,
                             cookies=cookies, data=data, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    content = re.findall(r'<record><!\[CDATA\[(.*?)\]\>', html_text, re.M | re.S)
    try:
        if news_classify == '首页-会员之家-会员公告':
            content_lst = content[1:]
        else:
            content_lst = content

        ree_data(content_lst, news_classify, science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(content)):
        try:
            news_title = re.findall(r'class="rfl">(.*?)</div', content[i])[0]
        except Exception as e:
            logging.warning('{} {} Article Title Was Error :{}'.format(account_name, news_classify, e))
            news_title = ''

        try:
            news_abstract = news_title
        except Exception as e:
            logging.warning('{} {} Article Abstract Was Error :{}'.format(account_name, news_classify, e))
            news_abstract = ''

        try:
            news_publish_time = re.findall(r'/art/(.*?)/art_', content[i])[0].replace('/', '-')
            # print(news_publish_time)
            # breakpoint()
        except Exception as e:
            logging.warning('{} {} Article Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                url_part2 = re.findall(r'href="(.*?)"', content[i])[0]
            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http:' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.carm.org.cn' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="list05"]//span/text() | '
                                           '//*[@class="list05"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者(.*?)</', content_text, re.M | re.S)[0]
        news_author = news_author.split('>')[-3].split('<')[0]
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="list05"]//span//img/@src |'
                                        ' //*[@class="list05"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'document.write\("(.*?)"', content_text)[0]
    except:
        click_count = ''
    # print(click_count)
    # breakpoint()
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻中心-学会动态': 'http://www.carm.org.cn/col/col6690/index.html',
        '首页-新闻中心-图片新闻': 'http://www.carm.org.cn/col/col6795/index.html',
        '首页-新闻中心-分支机构动态': 'http://www.carm.org.cn/col/col6762/index.html',
        '首页-新闻中心-地方学会动态': 'http://www.carm.org.cn/col/col6692/index.html',
        '首页-党建工作-通知公告': 'http://www.carm.org.cn/col/col6695/index.html',
        '首页-党建工作-党建动态': 'http://www.carm.org.cn/col/col6696/index.html',
        '首页-党建工作-康复服务行': 'http://www.carm.org.cn/col/col6734/index.html',
        '首页-党建工作-学习专栏': 'http://www.carm.org.cn/col/col6698/index.html',
        '首页-党建工作-康复科技工作者之家': 'http://www.carm.org.cn/col/col6697/index.html',
        '首页-通知公告': 'http://www.carm.org.cn/col/col6672/index.html',
        '首页-科研学术-通知公告': 'http://www.carm.org.cn/col/col6705/index.html',
        '首页-科研学术-学术动态': 'http://www.carm.org.cn/col/col6704/index.html',
        '首页-科研学术-科技奖励': 'http://www.carm.org.cn/col/col6706/index.html',
        '首页-科研学术-综合学术年会': 'http://www.carm.org.cn/col/col6707/index.html',
        '首页-科研学术-全国康复医疗资源情况调查': 'http://www.carm.org.cn/col/col6708/index.html',
        '首页-科研学术-团体标准': 'http://www.carm.org.cn/col/col7913/index.html',
        '首页-科研学术-中央财政支持社会组织项目': 'http://www.carm.org.cn/col/col7914/index.html',
        '首页-教育培训-培训资讯': 'http://www.carm.org.cn/col/col6710/index.html',
        '首页-教育培训-通知公告': 'http://www.carm.org.cn/col/col6711/index.html',
        '首页-教育培训-继教项目': 'http://www.carm.org.cn/col/col6712/index.html',
        '首页-教育培训-培训基地': 'http://www.carm.org.cn/col/col6713/index.html',
        '首页-科普公益-科普动态': 'http://www.carm.org.cn/col/col6716/index.html',
        '首页-科普公益-科普宣传': 'http://www.carm.org.cn/col/col6717/index.html',
        '首页-会员之家-会员公告': 'http://www.carm.org.cn/col/col6699/index.html',
        '首页-会员之家-会员服务': 'http://www.carm.org.cn/col/col6700/index.html',
        '首页-会员之家-单位会员名录': 'http://www.carm.org.cn/col/col6701/index.html',
        '首页-会员之家-会员风采': 'http://www.carm.org.cn/col/col6702/index.html',
        '首页-会员之家-会员之窗': 'http://www.carm.org.cn/col/col6703/index.html',
        '首页-标准制度-学会法规制度': 'http://www.carm.org.cn/col/col6719/index.html',
        '首页-标准制度-其他法规制度': 'http://www.carm.org.cn/col/col6720/index.html',
        '首页-标准制度-团体标准': 'http://www.carm.org.cn/col/col6721/index.html',

    }

    for key, value in urls.items():
        news_classify = key
        url = value
        columnid = re.findall(r'col\/col(.*?)\/index', value)[0]
        html_text = get_html(url, columnid)
        # print(html_text)
        # breakpoint()
        get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time)
        time.sleep(1)
        time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'D-19 中国康复医学会'
    start_run(args.projectname, args.sinceyear, account_name)
