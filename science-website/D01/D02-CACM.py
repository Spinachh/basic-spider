# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-01-30
import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    try:
        part1_nodes = selector.xpath('//*[@class="Listpage-cent"]//li')
        xpath_data(part1_nodes, news_classify,
                   science_system, mongo, account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span/text()')[0]
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'https://' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.cacm.org.cn' + url_part2
            news_author, news_imgs, news_content_type, news_content, source,\
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count,click_count , news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="Imgtxt-cent"]//span/text() | '
                                           '//*[@class="Imgtxt-cent"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="Imgtxt-cent"]//span//img/@src |'
                                        ' //*[@class="Imgtxt-cent"]//p//img/@src' )[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源:(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-行业要闻': 'https://www.cacm.org.cn/category/hyyw/page/2/',
        '首页-学会动态': 'https://www.cacm.org.cn/category/xhdt/page/2/',
        '首页-通知公告': 'https://www.cacm.org.cn/category/tzgg/page/2/',
        '首页-学术园地-会议通知': 'https://www.cacm.org.cn/category/zyzn/xsyd/hytzsj/page/2/',
        '首页-学术园地-学术动态': 'https://www.cacm.org.cn/category/zyzn/xsyd/xsdtsj/page/2/',
        '首页-学术园地-会议纪要': 'https://www.cacm.org.cn/category/zyzn/xsyd/hyjy/page/2/',
        '首页-科技评审-工作动态': 'https://www.cacm.org.cn/category/zyzn/kjps/gzdt/page/2/',
        '首页-科技评审-奖励展示': 'https://www.cacm.org.cn/category/zyzn/kjps/jlzs/page/2/',
        '首页-科技评审-奖励办法': 'https://www.cacm.org.cn/category/zyzn/kjps/jlbf/page/2/',
        '首页-师承继教-继教动态': 'https://www.cacm.org.cn/category/zyzn/jjkp/jjdt/page/2/',
        '首页-科学普及-科普园地': 'https://www.cacm.org.cn/category/zyzn/kxpj/kpyd/page/2/',
        '首页-标准化-标准化': 'https://www.cacm.org.cn/category/zyzn/bzh/bzhsj/page/2/',
        '首页-会员服务-会员入会': 'https://www.cacm.org.cn/category/zyzn/hyfw/hyrh/page/2/',
        '首页-会员服务-资料下载': 'https://www.cacm.org.cn/category/zyzn/hyfw/zlxz/page/2/',
        '首页-国际交流-国际交流': 'https://www.cacm.org.cn/category/zyzn/gjjl/gjjlsj/page/2/',
        '首页-期刊-主办期刊': 'https://www.cacm.org.cn/category/zyzn/zbqk/zbqksj/page/2/',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            if page == 1:
                url = value.replace('page/2/', '')
            else:
                url = value.replace('page/2', 'page/' + str(page))
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)
            time.sleep(1)
            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'D-02 中华中医药学会'
    start_run(args.projectname, args.sinceyear, account_name)
