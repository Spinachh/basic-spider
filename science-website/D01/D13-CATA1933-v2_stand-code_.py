# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-04-10
import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):
    response = requests.get(url, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        # part1_nodes = selector.xpath('//*[@class="lv2NsList "]//li')
        content_part_nodes = selector.xpath('//*[@class="subNewsListTitle_1"]')
        publish_part_nodes = selector.xpath('//*[@class="subNewsListDate_1"]')
        deadline = xpath_data(content_part_nodes, publish_part_nodes, news_classify, science_system, mongo,
                              account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(content_part_nodes, publish_part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(1, len(content_part_nodes)):
        try:
            news_title = content_part_nodes[i].xpath('.//font/text()')[0].strip()
        except:
            news_title = ''

        news_abstract = news_title

        try:
            news_publish_time = publish_part_nodes[i].xpath('./text()')[0]
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                url_part2 = content_part_nodes[i].xpath('./a[2]/@href')[0]
            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.cata1933.cn' + url_part2
            news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {}.'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="lv3Box"]//span//text() | '
                                           '//*[@class="lv3Box"]//p//text() | '
                                           '//*[@class="lv3Box"]//div//text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="lv3Box"]//span//img/@src |'
                                        '//*[@class="lv3Box"]//p//img/@src | '
                                        '//*[@class="lv3Box"]//div//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'新闻来源：(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)
    urls = {
        '首页-通知公告': 'http://www.cata1933.cn/showNewsList.asp?smlId=19&bigId=14&page=1',
        '首页-科学普及': 'http://www.cata1933.cn/showNewsList.asp?smlId=9&bigId=5&page=1',
        '首页-新闻中心': 'http://www.cata1933.cn/showNewsList.asp?smlId=23&bigId=2&page=1',
        '首页-学术交流': 'http://www.cata1933.cn/showNewsList.asp?smlId=8&bigId=4&page=1',
        '首页-会员之家': 'http://www.cata1933.cn/showNewsList.asp?smlId=31&bigId=6&page=1',
        '首页-下载专区': 'http://www.cata1933.cn/showNewsList.asp?smlId=20&bigId=15&page=1',
        '首页-项目动态': 'http://www.cata1933.cn/showNewsList.asp?smlId=39&bigId=20&page=1',
        '首页-学会期刊': 'http://www.cata1933.cn/showNewsList.asp?smlId=15&bigId=12&page=1',
        '首页-表彰奖励': 'http://www.cata1933.cn/showNewsList.asp?bigId=10&smlId=14&page=1',
        '首页-继续教育': 'http://www.cata1933.cn/showNewsList.asp?bigId=9&page=1',
        '首页-青年理事会': 'http://www.cata1933.cn/showNewsList.asp?bigId=19&page=1',
        '首页-最美防痨人': 'http://www.cata1933.cn/showNewsList.asp?smlId=57&bigId=23&page=1',
        '首页-党的二十大': 'http://www.cata1933.cn/showNewsList.asp?smlId=83&bigId=28&page=1',
        '首页-一带一路培训': 'http://www.cata1933.cn/showNewsList.asp?bigId=27&smlId=&page=1',
        '首页-双千行动': 'http://www.cata1933.cn/showNewsList.asp?bigId=20&smlId=37&page=1',
        '首页-中国防痨联合体': 'http://www.cata1933.cn/showNewsList.asp?bigId=18&page=1',
        '首页-抗击新冠病毒肺炎': 'http://www.cata1933.cn/showNewsList.asp?smlId=55&bigId=22&page=1',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 35):
            url = value.replace('page=1', 'page=' + str(page))
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                logging.warning('Account: {} Classfiy: {} DeadLine.'
                                .format(account_name, news_classify))
                break
            time.sleep(2)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q1')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-01-01')
    args = parser.parse_args()
    account_name = 'D-13 中国防痨协会'
    start_run(args.projectname, args.sinceyear, account_name)
