# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-02-01
import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url, jcp, page):

    cookies = {
        '_wafSiteType': '2',
        '_cliid': '5Pffeb233m_tdxtr',
        '_siteStatId': 'b3ba37ed-5494-4389-9f68-bb7b5151adde',
        '_siteStatDay': '20230201',
        '_siteStatVisitorType': 'visitorType_9494320',
        '_siteStatRedirectUv': 'redirectUv_9494320',
        '_siteStatVisit': 'visit_9494320',
        '_checkSiteLvBrowser': 'true',
        '_siteStatReVisit': 'reVisit_9494320',
        '_siteStatVisitTime': '1675238453254',
    }

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'http://www.zgsmyjh.org',
        'Referer': url,
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Mobile Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    data = {
        'cmd': 'getWafNotCk_getAjaxPageModuleInfo',
        '_colId': '21',
        '_extId': '0',
        'moduleId': '31',
        'href': '/nr.jsp?_jcp={}&m31pageno={}'.format(jcp, page),
        'newNextPage': 'false',
        'needIncToVue': 'false'
    }

    response = requests.post('http://www.zgsmyjh.org/ajax/ajaxLoadModuleDom_h.jsp', headers=headers,
                             data=data, verify=False)

    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    content = json.loads(html_text)
    try:
        ree_data(content, news_classify, science_system, mongo,
                 account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
             account_name, project_time, start_time):
    results = content['domStr']
    results_len = re.findall(r"newsName='(.*?)'", results, re.M | re.S)
    results_time = re.findall(r"target=_blank >(.*?)</a>", results, re.M | re.S)
    results_article_url = re.findall(r'href="(.*?)"', results, re.M | re.S)
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for item in range(len(results_len)):
        try:
            news_title = results_len[item]
        except:
            news_title = ''

        try:
            news_abstract = results_len[item]
        except:
            news_abstract = ''

        try:
            news_publish_time = results_time[item]
        except Exception as e:
            logging.warning('Publish Time Was Error: {}'.format(e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_publish_time)
        # print(news_publish_stamp)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            news_page_url = 'http://www.zgsmyjh.org/' + results_article_url[item]
            # http://www.zgsmyjh.org/nd.jsp?id=806#_jcp=4_1
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Referer': 'http://www.zgsmyjh.org/',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Mobile Safari/537.36',
    }
    # id = re.findall(r'(id=(.*?)#)', news_page_url)[0]
    # params = (
    #     ('id',  ['{}'.format(id), '{}'.format(id)]),
    # )

    # html_response = requests.get('http://m.zgsmyjh.org/nd.jsp', headers=headers,
    #                              params=params, verify=False)
    html_response = requests.get(news_page_url, headers=headers)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        text = re.findall(r'[\u3002\uff1b\uff0c\uff1a\u201c\u201d\uff08\uff09\u3001\uff1f\u300a\u300b\u4e00-\u9fa5]',
                          content_text)

        news_content = ''.join([x.strip() for x in text])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者：(.*?) ', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = re.findall(r'img src="(.*?)"', content_text)[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="xq_con"]//span//video/@src |'
                                         ' //*[@class="xq_con"]//p//video/@src |'
                                         ' //*[@class="xq_con"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源：(.*?) ', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击数：(.*?) ', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻资讯-研究会新闻': 'http://www.zgsmyjh.org/nr.jsp?_jcp=4_1',
        '首页-新闻资讯-政策资讯': 'http://www.zgsmyjh.org/nr.jsp?_jcp=4_2',
        '首页-新闻资讯-活动信息': 'http://www.zgsmyjh.org/nr.jsp?_jcp=4_14',
        '首页-新闻资讯-睡眠产业动态': 'http://www.zgsmyjh.org/nr.jsp?_jcp=4_13',
        '首页-新闻资讯-社会关注热点': 'http://www.zgsmyjh.org/nr.jsp?_jcp=4_15',
        '首页-学术会议-会议通知': 'http://www.zgsmyjh.org/nr.jsp?_jcp=4_12',
        '首页-学术会议-培训活动': 'http://www.zgsmyjh.org/nr.jsp?_jcp=4_12',
        '首页-学术会议-培训通知': 'http://www.zgsmyjh.org/nr.jsp?_jcp=4_28',
        '首页-学术会议-科学前沿': 'http://www.zgsmyjh.org/nr.jsp?_jcp=4_17',
        '首页-学术会议-科学进展': 'http://www.zgsmyjh.org/nr.jsp?_jcp=4_18',
        '首页-学术会议-学术会议': 'http://www.zgsmyjh.org/nr.jsp?_jcp=4_19',
        '首页-学术会议-学术论文': 'http://www.zgsmyjh.org/nr.jsp?_jcp=4_31',
        '首页-国际交流-国际交流': 'http://www.zgsmyjh.org/nr.jsp?_jcp=4_16',
        '首页-科研成果-科研成果': 'http://www.zgsmyjh.org/nr.jsp?_jcp=4_37',
        '首页-大众科普-科普知识': 'http://www.zgsmyjh.org/nr.jsp?_jcp=4_20',
        '首页-大众科普-科普宣传知识': 'http://www.zgsmyjh.org/nr.jsp?_jcp=4_21',
        '首页-大众科普-信息纵览': 'http://www.zgsmyjh.org/nr.jsp?_jcp=4_22',
        '首页-创新合作-企业之窗': 'http://www.zgsmyjh.org/nr.jsp?_jcp=4_32',
    }
    for key, value in urls.items():
        news_classify = key
        jcp = value.split('=')[-1]
        for page in range(1, 5):
            html_text = get_html(value, jcp, page)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)
            time.sleep(1)
            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'D-27W 中国睡眠研究会'
    start_run(args.projectname, args.sinceyear, account_name)
