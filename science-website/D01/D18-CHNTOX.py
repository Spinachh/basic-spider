# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-01-31
import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        part1_nodes = selector.xpath('//*[@class="list_con"]/li')
        xpath_data(part1_nodes, news_classify, science_system, mongo,
                   account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i+1].xpath('./a/i/text()')[0].replace('/', '-')
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))


        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                url_part2 = part_nodes[i].xpath('./a/@href')[0]
            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.chntox.org/' + url_part2
            news_author, news_imgs, news_content_type, news_content, source,\
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count,click_count , news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="xq_con"]//span//text() | '
                                           '//*[@class="xq_con"]//p//text() | '
                                           '//*[@class="xq_con"]//div//text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="xq_con"]//span//img/@src |'
                                        '//*[@class="xq_con"]//p//img/@src | '
                                        '//*[@class="xq_con"]//div//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="xq_con"]//span//video/@src |'
                                         ' //*[@class="xq_con"]//p//video/@src |'
                                         ' //*[@class="xq_con"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'发布人：(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击量： <ucapsource>(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-工作动态-重要新闻': 'https://www.chntox.org/newslist.aspx?p=1&t=3&pid=25&o=0',
        '首页-工作动态-学会动态': 'https://www.chntox.org/newslist.aspx?p=1&t=3&pid=26&o=0',
        '首页-工作动态-党建工作': 'https://www.chntox.org/newslist.aspx?p=1&t=3&pid=27&o=0',
        '首页-工作动态-专业委员会': 'https://www.chntox.org/newslist.aspx?p=1&t=3&pid=28&o=0',
        '首页-工作动态-省市学会': 'https://www.chntox.org/newslist.aspx?p=1&t=3&pid=29&o=0',
        '首页-学术会议-会议通知': 'https://www.chntox.org/newslist.aspx?p=1&t=4&pid=35&o=0',
        '首页-学术会议-既往会议': 'https://www.chntox.org/newslist.aspx?p=1&t=4&pid=36&o=1',
        '首页-继续教育-技术培训': 'https://www.chntox.org/newslist.aspx?p=1&t=5&pid=39&o=0',
        '首页-继续教育-资格认证': 'https://www.chntox.org/newslist.aspx?p=1&t=5&pid=40&o=0',
        '首页-科普园地-热点关注': 'https://www.chntox.org/newslist.aspx?p=1&t=6&pid=42&o=0',
        '首页-人才奖励-奖励办法': 'https://www.chntox.org/newslist.aspx?p=1&t=7&pid=47&o=0',
        '首页-人才奖励-科技奖励': 'https://www.chntox.org/newslist.aspx?p=1&t=7&pid=48&o=0',
        '首页-人才奖励-科技奖励-获奖人员': 'https://www.chntox.org/newslist.aspx?p=1&t=7&pid=108&o=0',
        '首页-科技咨询-社团标准': 'https://www.chntox.org/newslist.aspx?p=1&t=8&pid=51&o=0',
        '首页-科技咨询-成果转化': 'https://www.chntox.org/newslist.aspx?p=1&t=8&pid=52&o=0',
        '首页-科技咨询-专家智库': 'https://www.chntox.org/newslist.aspx?p=1&t=8&pid=49&o=0',
        '首页-科技咨询-科技评估': 'https://www.chntox.org/newslist.aspx?p=1&t=8&pid=50&o=0',


    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            url = value.replace('p=1', 'p=' + str(page))
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)
            time.sleep(1)
            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'D-18 中国毒理学会'
    start_run(args.projectname, args.sinceyear, account_name)
