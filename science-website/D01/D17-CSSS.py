# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-02-01
import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content
    return collection


def get_html(url):

    response = requests.get(url, verify=False)
    response.encoding = 'utf-8'
    return response.text


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        part1_nodes = selector.xpath('//*[@class="cols-1"]/li')
        xpath_data(part1_nodes, news_classify, science_system, mongo,
                   account_name, project_time, start_time)
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i+1].xpath('./span/text()')[0].replace('/', '-')
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))


        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                url_part2 = part_nodes[i].xpath('./a/@href')[0]
            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.csss.cn/' + url_part2
            news_author, news_imgs, news_content_type, news_content, source,\
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count,click_count , news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            return ''


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="content"]//span//text() | '
                                           '//*[@class="content"]//p//text() | '
                                           '//*[@class="content"]//div//text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="content"]//span//img/@src |'
                                        '//*[@class="content"]//p//img/@src | '
                                        '//*[@class="content"]//div//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="content"]//span//video/@src |'
                                        '//*[@class="content"]//p//video/@src | '
                                        '//*[@class="content"]//div//video/@src')[0]
        if news_video:
            news_video_flag = 'video'
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击量： <ucapsource>(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-通知公告': 'http://www.csss.cn/c194?page=1',
        '首页-学术会议': 'http://www.csss.cn/c190?page=1',
        '首页-科学普及': 'http://www.csss.cn/c186?page=1',
        '首页-科技奖励': 'http://www.csss.cn/c207?page=1',
        '首页-教育培训': 'http://www.csss.cn/c206?page=1',
        '首页-体育标准': 'http://www.csss.cn/c203?page=1',
        '首页-学会刊物': 'http://www.csss.cn/c205?page=1',
        '首页-学术议会项目': 'http://www.csss.cn/c195?page=1',
        '首页-企业风采': 'http://www.csss.cn/c227?page=1',
        '首页-全民健身活动-冰雪运动装备': 'http://www.csss.cn/c239?page=1',
        '首页-全民健身活动-老年人防跌倒': 'http://www.csss.cn/c238?page=1',
        '首页-全民健身活动-幼儿动作发展': 'http://www.csss.cn/c237?page=1',
        '首页-全民健身活动-体育工程': 'http://www.csss.cn/c236?page=1',
        '首页-全民健身活动-跑步科学': 'http://www.csss.cn/c235?page=1',
        '首页-全民健身活动-心理健康': 'http://www.csss.cn/c234?page=1',
        '首页-全民健身活动-体姿改善': 'http://www.csss.cn/c233?page=1',
        '首页-全民健身活动-科学减脂': 'http://www.csss.cn/c232?page=1',
        '首页-全民健身活动-近视防控': 'http://www.csss.cn/c230?page=1',

    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 5):
            url = value.replace('p=1', 'p=' + str(page))
            html_text = get_html(url)
            # print(html_text)
            # breakpoint()
            get_data(html_text, news_classify, account_name,
                     science_system, mongo, project_time, start_time)
            time.sleep(1)
            time.sleep(random.randint(1, 3))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default', default='2022-07-01')
    args = parser.parse_args()
    account_name = 'D-17 中国体育科学学会'
    start_run(args.projectname, args.sinceyear, account_name)
