# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-10-23

import re
import io
import sys
import time
import json
import urllib3
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_supflash():
    cookies = {
        'td_cookie': '2024582640',
        'Hm_lvt_cd95a8df179883e066fb9134e1c44636': '1667800101',
        'Hm_lpvt_cd95a8df179883e066fb9134e1c44636': '1667800101',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Referer': 'http://czs.ioz.cas.cn/xwdt/xhtz/index.html',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get('http://czs.ioz.cas.cn/xwdt/xhtz/index.html', headers=headers, cookies=cookies,
                            verify=False)
    html_text = response.text
    # print(html_text)
    # breakpoint()
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url):
    cookies = {
        'td_cookie': get_supflash(),
        'Hm_lvt_cd95a8df179883e066fb9134e1c44636': '1667800101',
        'Hm_lpvt_cd95a8df179883e066fb9134e1c44636': '1667800101',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers,
                            cookies=cookies, verify=False)
    response.encoding = 'utf-8'
    status_code = response.status_code
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    # part1
    try:
        part1_nodes = selector.xpath('/html/body/table[4]/tr/td[2]/table/tr['
                                     '2]/td/table/tr/td/table[1]/tbody/tr')
        deadline = xpath_data(part1_nodes, news_classify, science_system, mongo,
                              account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        print('Classify： {} Part1 has not content: {}'.format(news_classify, e))



def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./td[1]/a/@title')[0]
        except:
            news_title = 'not news_title'

        news_abstract = news_title
        try:
            news_publish_time = part_nodes[i].xpath('./td[2]/span/text()')[0].strip()
        except:
            news_publish_time = '2022-01-01'
        # try:
        #     news_imgs = 'http://www.gsc.org.cn/' + part_nodes[i].xpath('./a/img/@src')
        # except:
        #     news_imgs = ''

        # print('http://csmpg.gyig.cas.cn/xhyw' + part_nodes[i].xpath('./a/@href')[0].strip().replace('./', '/'))
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = part_nodes[i].xpath('./td[1]/a/@href')[0].replace('./', '/')
            news_page_url = 'http://czs.ioz.cas.cn/xwdt/xhtz/' + url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    cookies = {
        'td_cookie': get_supflash(),
        'Hm_lvt_cd95a8df179883e066fb9134e1c44636': '1667800101',
        'Hm_lpvt_cd95a8df179883e066fb9134e1c44636': '1667800101',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': news_page_url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    html_response = requests.get(news_page_url, headers=headers,
                                 cookies=cookies, verify=False)

    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="TRS_Editor"]//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'>来源：(.*?)　时间', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        did = news_page_url.split('_')[-1].split('.')[0]
        headers = {
            'Accept': '*/*',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Connection': 'keep-alive',
            'Referer': 'http://czs.ioz.cas.cn/',
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
        }

        params = (
            ('cascallback', 'jQuery112107319043038680142_1667803370948'),
            ('pid', '4'),
            ('sid', '378'),
            ('did', did),
            ('_', '1667803370949'),
        )

        response = requests.get('http://api2.cas.cn/app/click/count.json', headers=headers, params=params,
                                cookies=cookies, verify=False)

        click_count = re.findall(r'"data":(.*?),', response.text)[0]
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻动态-学会通知': 'http://czs.ioz.cas.cn/xwdt/xhtz/index_1.html',
        '首页-新闻动态-科技动态': 'http://czs.ioz.cas.cn/xwdt/kjdt/index_1.html',
        '首页-交流合作-交流动态': 'http://czs.ioz.cas.cn/jlhz/jldt/index_1.html',
        '首页-交流合作-国内学术交流': 'http://czs.ioz.cas.cn/jlhz/gn/index_1.html',
        '首页-交流合作-国际学术交流': 'http://czs.ioz.cas.cn/jlhz/gj/index_1.html',
        '首页-交流合作-海峡两岸学术交流': 'http://czs.ioz.cas.cn/jlhz/hxla/index_1.html',
        '首页-交流合作-科研论文': 'http://czs.ioz.cas.cn/jlhz/kylw/index_1.html',
        '首页-科学普及-科普动态': 'http://czs.ioz.cas.cn/kxpj/kpdt/index_1.html',
        '首页-科学普及-科普活动': 'http://czs.ioz.cas.cn/kxpj/kphd/index_1.html',
        '首页-科学普及-科普作品': 'http://czs.ioz.cas.cn/kxpj/kpzp/index_1.html',
        '首页-科学普及-科普知识': 'http://czs.ioz.cas.cn/kxpj/kpzs/index_1.html',
        '首页-科学普及-科普文章': 'http://czs.ioz.cas.cn/kxpj/kpwz/index_1.html',
        '首页-生物学竞赛-通知': 'http://czs.ioz.cas.cn/swxjs/tz/index_1.html',
        '首页-生物学竞赛-章程及实施细则': 'http://czs.ioz.cas.cn/swxjs/zcjssxz/index_1.html',
        '首页-生物学竞赛-生物联赛': 'http://czs.ioz.cas.cn/swxjs/swls/index_1.html',
        '首页-生物学竞赛-全国竞赛': 'http://czs.ioz.cas.cn/swxjs/qgjs/index_1.html',
        '首页-生物学竞赛-冬令营选拔赛': 'http://czs.ioz.cas.cn/swxjs/dlyxbs/index_1.html',
        '首页-生物学竞赛-国际奥赛': 'http://czs.ioz.cas.cn/swxjs/gjas/index_1.html',
        '首页-生物学竞赛-试题下载': 'http://czs.ioz.cas.cn/swxjs/stxz/index_1.html',
        '首页-继续教育-通知': 'http://czs.ioz.cas.cn/jxjy/tz/index_1.html',
        '首页-继续教育-培训动态': 'http://czs.ioz.cas.cn/jxjy/pxdt/index_1.html',
        '首页-继续教育-专家介绍': 'http://czs.ioz.cas.cn/jxjy/zjjs/index_1.html',
        '首页-继续教育-地方学会继续教育': 'http://czs.ioz.cas.cn/jxjy/dfxhjxjy/index_1.html',
        '首页-继续教育-分会继续教育': 'http://czs.ioz.cas.cn/jxjy/fhjxjy/index_1.html',
        '首页-会员专栏-党群园地': 'http://czs.ioz.cas.cn/jxjy/fhjxjy/index_1.html',
    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            if page == 1:
                url = value.replace('index_1', 'index')
            else:
                url = value.replace('index_1', 'index_' + str(page - 1))
            html_text, status_code = get_html(url)
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                logging.warning('Classify： {} has not content'.format(news_classify))
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'A-18 中国动物学会'
    start_run(args.projectname, args.sinceyear, account_name)
