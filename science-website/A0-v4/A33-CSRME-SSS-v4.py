# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-10-24

import re
import io
import sys
import time
import json
import urllib3
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content1
    collection = db['{}'.format(c_name)]
    return collection


def get_supflash():
    cookies = {
        'td_cookie': '2095021031',
        'Hm_lvt_ec22f37ea1cce8f86744c8d3c25d6c4e': '1667870775',
        'Hm_lpvt_ec22f37ea1cce8f86744c8d3c25d6c4e': '1667870812',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.botany.org.cn/xwzx/xwdt_/index_1.html',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get('http://www.botany.org.cn/xwzx/xwdt_/index.html', headers=headers, cookies=cookies,
                            verify=False)
    html_text = response.text
    # print(html_text)
    # breakpoint()
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url):

    cookies = {
        'think_language': 'zh-CN',
        'PHPSESSID': '971b65d07c2c8605c9c749e80e6d4e01',
        'Hm_lvt_46b91e72342be059f10d992059ad0744': '1668567326',
        'Hm_lpvt_46b91e72342be059f10d992059ad0744': '1668567367',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Mobile Safari/537.36',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    selector = etree.HTML(html_text)
    # part1
    try:
        part1_nodes = selector.xpath('//*[@class="news-list"]/ul/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        print('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/h3/span/text()')[0]
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/p/text()')[0]
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./a/span/text()')[0].split(' ')[0]
        except:
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_publish_time)
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))


        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'mp.weixin.qq.com' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.csrme.com/' + url_part2


            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    cookies = {
        'think_language': 'zh-CN',
        'PHPSESSID': '971b65d07c2c8605c9c749e80e6d4e01',
        'Hm_lvt_46b91e72342be059f10d992059ad0744': '1668567326',
        'Hm_lpvt_46b91e72342be059f10d992059ad0744': '1668567746',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Mobile Safari/537.36',
    }

    html_response = requests.get(news_page_url, headers=headers, cookies=cookies, verify=False)

    html_response.encoding = 'utf-8'
    content_text = html_response.text

    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    # print(result)
    # breakpoint()

    try:
        news_contents = selector_page.xpath('//*[@class="news-body"]///p/text()')
        news_content = ''.join(news_contents)
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="news-body"]//p//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
        
    try:
        read_count = re.findall(r'>浏览数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        source = re.findall(r'>来源：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        click_count = re.findall(r'>点击量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻动态': 'http://www.csrme.com/Home/Content/index/cateid/134/p/1.do',
        '首页-通知公告': 'http://www.csrme.com/Home/Content/index/cateid/162/p/1.do',
        '首页-视频荟萃': 'http://www.csrme.com/Home/Content/index/cateid/222/p/1.do',
        '首页-理论学习': 'http://www.csrme.com/Home/Content/index/cateid/533/p/1.do',
        '首页-科协防控,共渡难关': 'http://www.csrme.com/Home/Content/index/cateid/139/p/1.do',
        '组织宣传-新闻通告-新闻信息': 'http://www.csrme.com/Home/Content/index/cateid/306/p/1.do',
        '组织宣传-新闻通告-通知公告': 'http://www.csrme.com/Home/Content/index/cateid/305/p/1.do',
        '组织宣传-科学家精神-科学家精神': 'http://www.csrme.com/Home/Content/index/cateid/504/p/1.do',
        '组织宣传-学会党委-党委工作': 'http://www.csrme.com/Home/Content/index/cateid/429/p/1.do',
        '组织宣传-学会党委-分支机构党组织': 'http://www.csrme.com/Home/Content/index/cateid/233/p/1.do',
        '组织宣传-分支机构-新闻': 'http://www.csrme.com/Home/Content/index/cateid/384/p/1.do',
        '组织宣传-地方学会-新闻': 'http://www.csrme.com/Home/Content/index/cateid/369/p/1.do',
        '人才强会-新闻通告-新闻信息': 'http://www.csrme.com/Home/Content/index/cateid/296/p/1.do',
        '人才强会-新闻通告-通知公告': 'http://www.csrme.com/Home/Content/index/cateid/297/p/1.do',
        '人才强会-人才举荐-院士推选': 'http://www.csrme.com/Home/Content/index/cateid/375/p/1.do',
        '人才强会-人才举荐-光华奖': 'http://www.csrme.com/Home/Content/index/cateid/107/p/1.do',
        '人才强会-青托人才-青托工作': 'http://www.csrme.com/Home/Content/index/cateid/383/p/1.do',
        '人才强会-继教培训': 'http://www.csrme.com/Home/Content/index/cateid/117/p/1.do',
        '学术引领-新闻信息': 'http://www.csrme.com/Home/Content/index/cateid/258/p/1.do',
        '学术引领-通知公告': 'http://www.csrme.com/Home/Content/index/cateid/310/p/1.do',
        '学术引领-ChinaRock': 'http://www.csrme.com/Home/Content/index/cateid/1029/p/1.do',
        '学术引领-高层论坛': 'http://www.csrme.com/Home/Content/index/cateid/265/p/1.do',
        '学术引领-学术讲座': 'http://www.csrme.com/Home/Content/index/cateid/362/p/1.do',
        '学术引领-学科发展报告': 'http://www.csrme.com/Home/Content/index/cateid/272/p/1.do',
        '学术引领-难题征集': 'http://www.csrme.com/Home/Content/index/cateid/275/p/1.do',
        '学术引领-系列专题会议': 'http://www.csrme.com/Home/Content/index/cateid/87/p/1.do',
        '科技奖励-奖励新闻': 'http://www.csrme.com/Home/Content/index/cateid/341/p/1.do',
        '科技奖励-通知公告': 'http://www.csrme.com/Home/Content/index/cateid/342/p/1.do',
        '科技奖励-学会科学技术奖': 'http://www.csrme.com/Home/Content/index/cateid/343/p/1.do',
        '科技奖励-科技竞赛': 'http://www.csrme.com/Home/Content/index/cateid/111/p/1.do',
        '科技奖励-文件下载': 'http://www.csrme.com/Home/Content/index/cateid/358/p/1.do',
        '科学普及-新闻信息': 'http://www.csrme.com/Home/Content/index/cateid/318/p/1.do',
        '科学普及-通知公告': 'http://www.csrme.com/Home/Content/index/cateid/319/p/1.do',
        '科学普及-科普活动': 'http://www.csrme.com/Home/Content/index/cateid/311/p/1.do',
        '国际合作-新闻信息': 'http://www.csrme.com/Home/Content/index/cateid/322/p/1.do',
        '国际合作-通知公告': 'http://www.csrme.com/Home/Content/index/cateid/323/p/1.do',
        '国际合作-国际会议': 'http://www.csrme.com/Home/Content/index/cateid/160/p/1.do',
        '国际合作-国际交流合作': 'http://www.csrme.com/Home/Content/index/cateid/333/p/1.do',
        '国际合作-国际组织': 'http://www.csrme.com/Home/Content/index/cateid/154/p/1.do',
        '会员服务-新闻信息': 'http://www.csrme.com/Home/Content/index/cateid/320/p/1.do',
        '会员服务-通知公告': 'http://www.csrme.com/Home/Content/index/cateid/321/p/1.do',
        '会员服务-会员风采': 'http://www.csrme.com/Home/Content/index/cateid/166/p/1.do',
        '综合管理-新闻信息': 'http://www.csrme.com/Home/Content/index/cateid/314/p/1.do',
        '综合管理-规章制度': 'http://www.csrme.com/Home/Content/index/cateid/241/p/1.do',
        '计划财务-国家财务制度': 'http://www.csrme.com/Home/Content/index/cateid/385/p/1.do',
        '计划财务-学会财务制度': 'http://www.csrme.com/Home/Content/index/cateid/386/p/1.do',
        '计划财务-145规划': 'http://www.csrme.com/Home/Content/index/cateid/288/p/1.do',
        '计划财务-科协项目集汇': 'http://www.csrme.com/Home/Content/index/cateid/289/p/1.do',
        '计划财务-计划总结': 'http://www.csrme.com/Home/Content/index/cateid/286/p/1.do',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 15):
            url = value.replace('1.do', str(page) + '.do')
            html_text, status_code = get_html(url)
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                logging.warning('CLASSIFY：{} PAGE: {} CONTENT DEADLINE'.format(news_classify, page))
                break
            time.sleep(0.5)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'A-33 中国岩石力学与工程学会'
    start_run(args.projectname, args.sinceyear, account_name)
