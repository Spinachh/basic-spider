# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-10-25

import re
import io
import sys
import time
import json
import cchardet
import argparse
import requests
import logging
import pymongo
import urllib3
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_supflash(url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/117.0.0.0 Safari/537.36',
    }
    response = requests.get(url, headers=headers, verify=False)
    html_text = response.text
    # print(html_text)
    # breakpoint()
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, catid, page):

    cookies = {
        'UM_distinctid': '1891a88dfea3ea-0a351d30aad0c7-26031d51-1fa400-1891a88dfeb10be',
        'CNZZDATA1264453274': '727031895-1688367326-%7C1688367326',
        '_csrf-frontend': 'ee63b4e5c48462463ffdb9e7fdc8d02c4f8e6747b8edd378b953b56a80100613a%3A2%3A%7Bi%3A0%3Bs%3A14%3A%22_csrf-frontend%22%3Bi%3A1%3Bs%3A32%3A%226Su5sFWl2XszTiahO_ZPDrzuvJWgsld4%22%3B%7D',
        'Hm_lvt_58f8062829aa3584351cc9c8e1455716': '1697099554',
        'Hm_lpvt_58f8062829aa3584351cc9c8e1455716': '1697099616',
        'td_cookie': get_supflash(url),
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/117.0.0.0 Safari/537.36',
    }

    params = (
        ('cid', catid),
        ('page', page),
        ('per-page', '12'),
    )

    response = requests.get('http://www.calas.org.cn/article', headers=headers, params=params, cookies=cookies,
                            verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    selector = etree.HTML(html_text)
    # part1
    try:
        part1_nodes = selector.xpath('//*[@class="channel_list"]/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        print('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/@title')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/@title')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./a/div[2]/text()')[0].strip()
        except:
            news_publish_time = '2023-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_publish_time)
        # breakpoint()
        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'mp.weixin.qq.com' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.calas.org.cn' + url_part2

            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="text_content"]//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
        
    try:
        read_count = re.findall(r'浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        source = re.findall(r'>来源：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        click_count = re.findall(r'> 点击率：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '学会资讯-学会动态': 'http://www.calas.org.cn/article?cid=8_19&page=2&per-page=12',
        '学会资讯-通知公告': 'http://www.calas.org.cn/article?cid=8_20&page=2&per-page=12',
        '学会资讯-行业资讯': 'http://www.calas.org.cn/article?cid=8_22&page=2&per-page=12',
        '学会资讯-政策法规': 'http://www.calas.org.cn/article?cid=8_95&page=2&per-page=12',
        '学会资讯-电子通讯': 'http://www.calas.org.cn/article?cid=8_100&page=2&per-page=12',
        '学术交流-国内学术交流': 'http://www.calas.org.cn/article?cid=9_47&page=2&per-page=12',
        '学术交流-国际学术交流': 'http://www.calas.org.cn/article?cid=9_48&page=2&per-page=12',
        '教育培训-政策法规': 'http://www.calas.org.cn/article?cid=10_50&page=2&per-page=12',
        '教育培训-中国实验动物从业人员资格等级培训': 'http://www.calas.org.cn/article?cid=10_94&page=2&per-page=12',
        '科技奖励-中国实验动物学会科学技术奖': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=11&page=1',
        '科技奖励-中国实验动物学会突出贡献奖': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=53&page=1',
        '科技奖励-实验动物青年科学家奖': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=54&page=1',
        '科技奖励-举荐奖励': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=55&page=1',
        '科学普及-科普活动': 'http://www.calas.org.cn/article?cid=12_56&page=2&per-page=12',
        '科学普及-科普知识': 'http://www.calas.org.cn/article?cid=12_23&page=2&per-page=12',

        '科技服务-行业标准': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=13&page=1',
        '科技服务-科技咨询': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=14&page=1',
        '科技服务-为单位服务': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=60&page=1',
        '科技服务-为企业服务': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=61&page=1',
        '科技服务-为会员服务': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=62&page=1',
        '科技服务-机构福利伦理评价': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=106&page=1',

        '期刊出版-中国实验动物学报': 'http://www.calas.org.cn/article?cid=15_63&page=2&per-page=12',
        '期刊出版-中国比较医学杂志': 'http://www.calas.org.cn/article?cid=15_64&page=2&per-page=12',
        '期刊出版-Animal Models and Experimental Medicine': 'http://www.calas.org.cn/article?cid=15_65',
        '期刊出版-实验动物图书': 'http://www.calas.org.cn/article?cid=15_66',

        '学会党建-党的二十大精神': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=16&page=1',
        '学会党建-中央精神': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=67&page=1',
        '学会党建-科协党建': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=97&page=1',
        '学会党建-党建动态': 'http://www.calas.org.cn/article?cid=16_68&page=2&per-page=12',
        '学会党建-党风廉政': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=69&page=1',
        '学会党建-党务课堂': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=70&page=1',
        '会员中心-入会指南': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=17&page=1',
        '会员中心-常见问题': 'http://www.calas.org.cn/index.php?m=content&c=index&a=lists&catid=83&page=1',

    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            catid = re.findall(r'cid=(.*?)&', value)[0]
            url = value.replace('page=1', 'page=' + str(page))
            html_text, status_code = get_html(url, catid, page)
            # print(html_text)
            # breakpoint()
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                logging.warning('CLASSIFY：{} PAGE: {} CONTENT DEADLINE'.format(news_classify, page))
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'A-36 中国实验动物学会'
    start_run(args.projectname, args.sinceyear, account_name)
