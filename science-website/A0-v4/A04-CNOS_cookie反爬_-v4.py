# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-10-12

import re
import io
import sys
import time
import json
import cchardet
import datetime
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_supflash(url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.cncos.org.cn/Content/index/catid/20.html?&p=4',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, verify=False)
    html_text = response.text
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, page):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.cncos.org.cn/Content/index/catid/20.html?&p={}'.format(page),
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }
    cookies = {
        'td_cookie': get_supflash(url),
        'PHPSESSID': 'nlss62tn73el4eierftqf99ba6',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)
    status_code = response.status_code
    return response.text, status_code


def get_data(html_text, news_classify, account_name,
             science_system, start_time, mongo, project_time):
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    selector = etree.HTML(html_text)
    nodes = selector.xpath('//*[@class="actlist"]/li')
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    for i in range(len(nodes)):
        try:
            news_title = nodes[i].xpath('.//p/text()')[0]
        except:
            news_title = 'not news_title'

        try:
            news_abstract = nodes[i].xpath('.//p/text()')[0]
        except:
            news_abstract = ''

        try:
            news_publish_time = nodes[i].xpath('.//span/text()')[0].strip()
        except:
            news_publish_time = '2023-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        news_page_url = 'http://www.cncos.org.cn/' + nodes[i].xpath('./a/@href')[0]

        if int(news_publish_stamp) >= int(start_time_stamp):
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSFIY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': news_page_url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }
    cookies = {
        'td_cookie': get_supflash(news_page_url),
        'PHPSESSID': 'sviinfuvdipq3jvqp33lboi667',
    }

    html_response = requests.get(news_page_url, headers=headers, cookies=cookies, verify=False)
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="detail"]//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'>文章来源：(.*?)<', content_text, re.M | re.S)[0]
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览次数：(.*?)', content_text, re.M | re.S)[0]
    except:
        read_count = ''
    try:
        click_count = re.findall(r'阅读次数：(.*?)', content_text, re.M | re.S)[0]
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '学会动态-学会工作信息': 'http://www.cncos.org.cn/Content/index/catid/761.html?&p=1',
        '学会动态-专委会地方学会动态': 'http://www.cncos.org.cn/Content/index/catid/20.html?&p=1',
        '学会动态-会员风采': 'http://www.cncos.org.cn/Content/index/catid/23.html?&p=1',
        '学会动态-行业动态': 'http://www.cncos.org.cn/Content/index/catid/17.html?&p=1',
        '会议展览-国内会议': 'http://www.cncos.org.cn/Content/index/catid/12.html?&p=1',
        '会议展览-国际会议': 'http://www.cncos.org.cn/Content/index/catid/13.html?&p=1',
        '会议展览-专业委员会会议': 'http://www.cncos.org.cn/Content/index/catid/768.html?&p=1',
        '会议展览-地方学会会议': 'http://www.cncos.org.cn/Content/index/catid/769.html?&p=1',
        '会议展览-国内展览': 'http://www.cncos.org.cn/Content/index/catid/15.html?&p=1',
        '会议展览-国际展览': 'http://www.cncos.org.cn/Content/index/catid/14.html?&p=1',
        '科普与讲座-国际光日动态': 'http://www.cncos.org.cn/Content/index/catid/765.html?&p=1',
        '科普与讲座-光学科普': 'http://www.cncos.org.cn/Content/index/catid/27.html?&p=1',
        '科普与讲座-中国科协年会科普': 'http://www.cncos.org.cn/Content/index/catid/45.html?&p=1',
        '科普与讲座-科普图书': 'http://www.cncos.org.cn/Content/index/catid/29.html?&p=1',
        '科普与讲座-地方专委会科普活动': 'http://www.cncos.org.cn/Content/index/catid/755.html?&p=1',
        '科普与讲座-中学生科普': 'http://www.cncos.org.cn/Content/index/catid/69.html?&p=1',
        '科技奖励-王大珩光学奖': 'http://www.cncos.org.cn/Content/index/catid/31.html?&p=1',
        '科技奖励-中国光学学会光学科技奖': 'http://www.cncos.org.cn/Content/index/catid/33.html?&p=1',
        '科技奖励-其他科技奖项': 'http://www.cncos.org.cn/Content/index/catid/35.html?&p=1',
        '会员服务-个人会员': 'http://www.cncos.org.cn/Content/index/catid/787.html?&p=1',
        '会员服务-团体会员': 'http://www.cncos.org.cn/Content/index/catid/789.html?&p=1',
        '公告-通知公告': 'http://www.cncos.org.cn/Content/index/catid/42.html?&p=1',
        '国际光日-国际光日动态': 'http://www.cncos.org.cn/Content/index/catid/765.html?&p=1',
        '学会党建-党的二十大': 'http://www.cncos.org.cn/Content/index/catid/791.html?&p=1',
        '学会党建-学会党建': 'http://www.cncos.org.cn/Content/index/catid/786.html?&p=1',

    }
    for key, value in urls.items():
        news_classify = key
        url = value
        for page in range(1, 50):
            url = url.replace('&p=1', '&p=' + str(page))
            html_text, status_code = get_html(url, page)
            # print(html_text)
            # breakpoint()
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name, science_system,
                                start_time, mongo, project_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'A-04 中国光学学会'
    start_run(args.projectname, args.sinceyear, account_name)
