# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-10-24

import re
import io
import sys
import time
import json
import urllib3
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_supflash():
    cookies = {
        'td_cookie': '2095021031',
        'Hm_lvt_ec22f37ea1cce8f86744c8d3c25d6c4e': '1667870775',
        'Hm_lpvt_ec22f37ea1cce8f86744c8d3c25d6c4e': '1667870812',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.botany.org.cn/xwzx/xwdt_/index_1.html',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get('http://www.botany.org.cn/xwzx/xwdt_/index.html', headers=headers, cookies=cookies,
                            verify=False)
    html_text = response.text
    # print(html_text)
    # breakpoint()
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, id):

    cookies = {
        'td_cookie': '2621395878',
    }

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Referer': 'http://www.csnr.org.cn/list.html?id={}'.format(id),
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Mobile Safari/537.36',
    }

    response = requests.get(url, headers=headers, cookies=cookies)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    selector = etree.HTML(html_text)
    # part1
    try:
        part1_nodes = selector.xpath('//*[@class="rightNews"]/li')
        deadline = xpath_data(part1_nodes, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        print('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))

    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./a/div[2]/h4/text()')[0]
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./a/div[2]/p/text()')[0]
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./a/div[2]/div/span[1]/text()')[0].split('发布时间：')[-1]
        except:
            news_publish_time = '2022-01-01'

        try:
            news_imgs = part_nodes[i].xpath('./a/div[1]/img/@src')
            if news_imgs:
                news_content_type = 'text-img'
            else:
                news_content_type = 'text'
        except:
            news_imgs = ''
            news_content_type = 'text'

        try:
            read_count = part_nodes[i].xpath('./a/div[2]/div/span[3]/text()')[0].split('浏览量：')[-1]
        except:
            read_count = ''

        # print(news_title)
        # print(news_publish_time)
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))


        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = part_nodes[i].xpath('./a/@href')[0]
            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http:' + url_part2

            news_author, news_content, source, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    '''
    try:
        news_imgs = selector_page.xpath('//*[@valign="top"]//p//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
        
    try:
        read_count = re.findall(r'浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''
    '''
    try:
        source = re.findall(r'>来源:(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        click_count = re.findall(r'>点击量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_content, source, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新首页-新闻动态': 'http://www.csnr.org.cn/list.html?id=37&pageIndex=1',
        '首页-新首页-地方学会': 'http://www.csnr.org.cn/list.html?id=63&pageIndex=1',
        '首页-新首页-通知公告': 'http://www.csnr.org.cn/list.html?id=65&pageIndex=1',
        '首页-学术活动-学术年会': 'http://www.csnr.org.cn/list.html?id=25&pageIndex=1',
        '首页-学术活动-青年科学家论坛': 'http://www.csnr.org.cn/list.html?id=26&pageIndex=1',
        '首页-学术活动-大学生大赛': 'http://www.csnr.org.cn/list.html?id=43&pageIndex=1',
        '首页-智库建设-智库报告': 'http://www.csnr.org.cn/list.html?id=75&pageIndex=1',
        '首页-智库建设-智库资讯': 'http://www.csnr.org.cn/list.html?id=70&pageIndex=1',
        '首页-智库建设-智库成果': 'http://www.csnr.org.cn/list.html?id=61&pageIndex=1',
        '首页-智库建设-智库观点': 'http://www.csnr.org.cn/list.html?id=71&pageIndex=1',
        '首页-智库建设-热点调查': 'http://www.csnr.org.cn/list.html?id=72&pageIndex=1',
        '首页-科普天地-资源百科': 'http://www.csnr.org.cn/list.html?id=44&pageIndex=1',
        '首页-科普天地-科普活动': 'http://www.csnr.org.cn/list.html?id=45&pageIndex=1',
        '首页-科普天地-科普基地': 'http://www.csnr.org.cn/list.html?id=46&pageIndex=1',
        '首页-科普天地-科普服务': 'http://www.csnr.org.cn/list.html?id=47&pageIndex=1',
        '首页-奖励表彰-科学技术奖': 'http://www.csnr.org.cn/list.html?id=20&pageIndex=1',
        '首页-奖励表彰-资源科学成就奖': 'http://www.csnr.org.cn/list.html?id=21&pageIndex=1',
        '首页-奖励表彰-学会工作先进个人': 'http://www.csnr.org.cn/list.html?id=22&pageIndex=1',
        '首页-奖励表彰-大学生优秀作品': 'http://www.csnr.org.cn/list.html?id=23&pageIndex=1',
        '首页-党建强会-学会党委': 'http://www.csnr.org.cn/list.html?id=18&pageIndex=1',
        '首页-党建强会-党建活动': 'http://www.csnr.org.cn/list.html?id=19&pageIndex=1',
        '首页-会员之家-志愿者工作站': 'http://www.csnr.org.cn/list.html?id=17&pageIndex=1',
    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            id = re.findall(r'id=(.*?)&', value)[0]
            url = value.replace('pageIndex=1', 'pageIndex=' + str(page))
            html_text, status_code = get_html(url, id)

            if status_code != 200:
                break

            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                logging.warning('CLASSIFY：{} PAGE: {} CONTENT DEADLINE'.format(news_classify, page))
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'A-30 中国自然资源学会'
    start_run(args.projectname, args.sinceyear, account_name)
