# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-10-25


import re
import io
import sys
import time
import json
import urllib3
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_pageId(url):
    html_text = requests.get(url).text
    pageId = re.findall(r"'pageId':'(.*?)'", html_text)[0]
    return pageId


def get_html(url, page, pageId):

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/114.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
    }

    params = (
        ('webId', '72535c7bc0424d8186caa8c18b77a248'),
        ('pageId', pageId),
        ('parseType', 'bulidstatic'),
        ('pageType', 'column'),
        ('tagId', '\u4FE1\u606F\u5217\u8868'),
        ('tplSetId', '32af2d43a3014df990f6f0ed41e2be7c'),
        ('paramJson', '{"pageNo":%s,"pageSize":"10"}' % page),
    )

    response = requests.get('http://www.mscfungi.org.cn/api-gateway/jpaas-publish-server/front/page/build/unit',
                            headers=headers, params=params, verify=False)
    response.encoding = 'utf-8'
    status_code = response.status_code

    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    content = json.loads(html_text)
    try:
        deadline = ree_data(content, news_classify, science_system,
                            mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):

    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    content_data = content['data']['html']
    # print(content_data)
    # breakpoint()
    selector = etree.HTML(content_data)
    nodes = selector.xpath('//*[@class="page-content"]//li')

    for i in range(len(nodes)):
        try:
            news_title = nodes[i].xpath('./a/div[1]/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = nodes[i].xpath('./p/text()')[0].strip()
        except:
            news_abstract = news_title

        try:
            news_publish_time = nodes[i].xpath('./a/div[2]/text()')[0].strip()
        except:
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            url_part2 = nodes[i].xpath('./a/@href')[0].strip()
            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.mscfungi.org.cn' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    # html_response = requests.get(news_page_url, verify=False)
    html_response = requests.get(news_page_url)
    # if 'www.cstp.org.cn' not in news_page_url:
    #     html_response.encoding = 'gb2312'
    # else:
    html_response.encoding = 'utf-8'

    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    try:
        news_content = selector_page.xpath('//*[@class="container"]///p/text()')
        # news_content = selector_page.xpath('//*[@class="cont_txt"]/span/text()')
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="container w1200"]//p//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        source = re.findall(r'>来源：(.*?)　时间', content_text, re.M | re.S)[0].strip()
    except:
        source = ''


    try:
        read_count = re.findall(r'阅读量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    
    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学会要闻': 'http://www.mscfungi.org.cn/xw/xhyw/index.html',
        '首页-研究动态': 'http://www.mscfungi.org.cn/xw/yjdt/index.html',
        '首页-通知公告': 'http://www.mscfungi.org.cn/xw/tzgg/index.html',
        '首页-党建工作-党建活动': 'http://www.mscfungi.org.cn/djgz/djhd/index.html',
        '首页-党建工作-理论学习': 'http://www.mscfungi.org.cn/djgz/llxx/index.html',
        '首页-党建工作-党史学习': 'http://www.mscfungi.org.cn/djgz/dsxx/index.html',
        '首页-党建工作-科学精神': 'http://www.mscfungi.org.cn/djgz/kxjs/index.html',
        '首页-党建工作-党建专题': 'http://www.mscfungi.org.cn/djgz/djzt/index.html',
        '首页-学术-学术活动': 'http://www.mscfungi.org.cn/xs/xshd/index.html',
        '首页-学术-会议通知': 'http://www.mscfungi.org.cn/xs/hytz/index.html',
        '首页-学术-历年年会': 'http://www.mscfungi.org.cn/xs/lnnh/index.html',
        '首页-学术-精彩回顾': 'http://www.mscfungi.org.cn/xs/jchg/index.html',
        '首页-学术-活动预告': 'http://www.mscfungi.org.cn/xs/hdyg/index.html',
        '首页-学术-品牌活动': 'http://www.mscfungi.org.cn/xs/pphd/index.html',
        '首页-学术-活动图片': 'http://www.mscfungi.org.cn/xs/hdtp/index.html',
        '首页-学术-活动视频': 'http://www.mscfungi.org.cn/xs/hdsp/index.html',
        '首页-产业动态-产业动态': 'http://www.mscfungi.org.cn/cydt/cydt/index.html',
        '首页-产业动态-专题论坛': 'http://www.mscfungi.org.cn/cydt/ztlt/index.html',
        '首页-产业动态-活动通知': 'http://www.mscfungi.org.cn/cydt/hdtz/index.html',
        '首页-科普-科普动态': 'http://www.mscfungi.org.cn/kp/kpdt/index.html',
        '首页-科普-科普资讯': 'http://www.mscfungi.org.cn/kp/kpzx/index.html',
        '首页-科普-科普知识': 'http://www.mscfungi.org.cn/kp/kpzs/index.html',
    }

    for key, value in urls.items():
        news_classify = key
        pageId = get_pageId(value)
        for page in range(1, 50):
            html_text, status_code = get_html(value, page, pageId)
            # print(html_text)
            # breakpoint()
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                logging.warning('CLASSIFY：{} PAGE: {} CONTENT DEADLINE'.format(news_classify, page))
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'A-40 中国菌物学会'
    start_run(args.projectname, args.sinceyear, account_name)
