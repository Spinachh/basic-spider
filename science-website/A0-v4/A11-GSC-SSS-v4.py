# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-10-20

import random
import re
import io
import sys
import time
import json
import cchardet
import datetime
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content1
    collection = db['{}'.format(c_name)]
    return collection


def get_supflash(url):

    cookies = {
        'ASP.NET_SessionId': 'qisopuwtgn1gxwem3xelysjk',
        'td_cookie': '1666073779',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.gsc.org.cn/channel.aspx?id=68',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)
    html_text = response.text
    # print(html_text)
    # breakpoint()
    try:
        supFlash = re.findall(r'supFlash\((.*?)\);', html_text, re.M | re.S)[0]
        # print(supFlash)       # 这里有一个小的发爬措施，cookie值是由js刷新获取的。
    except:
        supFlash = ''

    return supFlash


def get_html(url, id, page):

    cookies = {
        'ASP.NET_SessionId': 'qisopuwtgn1gxwem3xelysjk',
        'td_cookie': get_supflash(url),
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        # 'Referer': 'http://www.gsc.org.cn/channel.aspx?id=68&page={}'.format(page),
        'Referer': 'http://www.gsc.org.cn/channel.aspx?id=68',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers,
                            cookies=cookies, verify=False)
    status_code = response.status_code
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, page, start_time):

    selector = etree.HTML(html_text)

    # part1
    try:
        part1_nodes = selector.xpath('//*[@class="news"]//li')
        deadline = xpath_data(part1_nodes, page, news_classify, science_system,
                              mongo, account_name, project_time, start_time)
        if deadline:
            return deadline

    except Exception as e:
        print('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, page, news_classify, science_system, mongo,
               account_name, project_time, start_time ):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./div//a/text()')[0].strip()
        except:
            news_title = 'not news_title'
        news_abstract = part_nodes[i].xpath('./div/p/text()')[0]
        news_publish_time = part_nodes[i].xpath('./div//b/text()')[0].replace('/', '-')

        # try:
        #     news_imgs = 'http://www.gsc.org.cn/' + part_nodes[i].xpath('./a/img/@src')
        # except:
        #     news_imgs = ''

        # print(news_publish_time)
        # breakpoint()
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d %H:%M:%S'))
        if int(news_publish_stamp) >= int(start_time_stamp):
            news_page_url = 'http://www.gsc.org.cn/' + part_nodes[i].xpath('./a/@href')[0]
            # if news_imgs:
            #     news_content_type = 'text-img'
            # else:
            #     news_content_type = 'text'
            news_content_type = 'text-img'
            news_author, news_imgs, news_content, source, \
            read_count, click_count = get_page_content(news_page_url, page)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url, page):

    cookies = {
        'ASP.NET_SessionId': 'qisopuwtgn1gxwem3xelysjk',
        'td_cookie': get_supflash(news_page_url),
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': news_page_url,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    html_response = requests.get(news_page_url, headers=headers, cookies=cookies,
                            verify=False)

    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        news_imgs = selector_page.xpath('//*[@class="news"]//p/img/@src')
    except:
        news_imgs = ''
    try:
        source = re.findall(r'>来源：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'>浏览次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''
    try:
        click_count = re.findall(r'>阅读次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页内容-新闻动态': 'http://www.gsc.org.cn/channel.aspx?id=68&page=1',
        '学术交流-学术交流': 'http://www.gsc.org.cn/channel.aspx?id=13&page=1',
        '学术交流-国内交流': 'http://www.gsc.org.cn/channel.aspx?id=14&page=1',
        '学术交流-国际交流': 'http://www.gsc.org.cn/channel.aspx?id=15&page=1',
        '学术交流-两岸交流': 'http://www.gsc.org.cn/channel.aspx?id=16&page=1',
        '学术交流-国际交往': 'http://www.gsc.org.cn/channel.aspx?id=61&page=1',
        '学术交流-品牌活动': 'http://www.gsc.org.cn/channel.aspx?id=17&page=1',
        '智库建设-活动报道': 'http://www.gsc.org.cn/channel.aspx?id=62&page=1',
        '智库建设-机构建设': 'http://www.gsc.org.cn/channel.aspx?id=19&page=1',
        '智库建设-平台搭建': 'http://www.gsc.org.cn/channel.aspx?id=20&page=1',
        '智库建设-品牌活动': 'http://www.gsc.org.cn/channel.aspx?id=21&page=1',
        '科普天地-品牌活动': 'http://www.gsc.org.cn/channel.aspx?id=24&page=1',
        '科普天地-科普动态': 'http://www.gsc.org.cn/channel.aspx?id=25&page=1',
        '科普天地-科普资源': 'http://www.gsc.org.cn/channel.aspx?id=26&page=1',
        '科普天地-地理知识': 'http://www.gsc.org.cn/channel.aspx?id=27&page=1',
        '科普天地-科普专家': 'http://www.gsc.org.cn/channel.aspx?id=28&page=1',
        '科普天地-科普志愿者': 'http://www.gsc.org.cn/channel.aspx?id=29&page=1',
        '科普天地-科普教育基地': 'http://www.gsc.org.cn/channel.aspx?id=30&page=1',
        '党建强会-社会服务': 'http://www.gsc.org.cn/channel.aspx?id=90&page=1',
        '党建强会-工作动态': 'http://www.gsc.org.cn/channel.aspx?id=32&page=1',
        '党建强会-学界动态': 'http://www.gsc.org.cn/channel.aspx?id=60&page=1',
        '党建强会-承能服务': 'http://www.gsc.org.cn/channel.aspx?id=41&page=1',
        '期刊书籍-品牌活动': 'http://www.gsc.org.cn/channel.aspx?id=45&page=1',
        '学会奖励-中国地理科学成就奖': 'http://www.gsc.org.cn/channel.aspx?id=37&page=1',
        '学会奖励-全国优秀地理科技工作者': 'http://www.gsc.org.cn/channel.aspx?id=46&page=1',
        '学会奖励-全国青年地理科技奖': 'http://www.gsc.org.cn/channel.aspx?id=47&page=1',
        '学会奖励-全国优秀中学地理教育工作者': 'http://www.gsc.org.cn/channel.aspx?id=48&page=1',
        '学会奖励-全国优秀地理图书奖': 'http://www.gsc.org.cn/channel.aspx?id=52&page=1',
        '学会奖励-各类优秀论文奖': 'http://www.gsc.org.cn/channel.aspx?id=53&page=1',
        '学会奖励-其它奖励': 'http://www.gsc.org.cn/channel.aspx?id=54&page=1',
        '活动通知-会议通知': 'http://www.gsc.org.cn/channel.aspx?id=18&page=1',
        '活动通知-学术活动': 'http://www.gsc.org.cn/channel.aspx?id=64&page=1',
        '活动通知-科普活动': 'http://www.gsc.org.cn/channel.aspx?id=65&page=1',
        '活动通知-表彰奖励': 'http://www.gsc.org.cn/channel.aspx?id=66&page=1',
        '活动通知-公告公示': 'http://www.gsc.org.cn/channel.aspx?id=67&page=1',
        '会员服务-公告公示': 'http://www.gsc.org.cn/channel.aspx?id=67&page=1',
    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            url = value.replace('page=1', 'page=' + str(page))
            id = re.findall(r'aspx\?id=(.*?)&', url)[0].strip()
            html_text, status_code = get_html(url, id, page)
            # print(html_text)
            # breakpoint()
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name, science_system, mongo, project_time, page,
                                start_time)
            if deadline:
                break


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'A-11 中国地理学会'
    start_run(args.projectname, args.sinceyear, account_name)
