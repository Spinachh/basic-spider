# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-09-20

# FIRST PAGE INCLUDES ALL PAGE CONTENT OF THIS WEBSITE

import re
import io
import sys
import time
import json
import datetime
import cchardet
import argparse
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_html(url):
    cookies = {
        'PHPSESSID': '4ma67da4e7a5s2adid0t9g1opp',
        'think_language': 'zh-CN',
    }

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Referer': 'http://www.cps-net.org.cn/Partybuild/Partybuild/index.do',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/106.0.0.0 Safari/537.36',
    }

    response = requests.get(url, headers=headers, cookies=cookies, verify=False)
    status_code = response.status_code
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, start_time, mongo, project_time):
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    selector = etree.HTML(html_text)
    nodes = selector.xpath('//*[@class="trends-li"]')
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    for i in range(len(nodes)):
        try:
            news_title = nodes[i].xpath('.//div/a/@title')[0].strip()
        except:
            news_title = 'not news_title'

        try:
            news_abstract = nodes[i].xpath('./div[2]/div[2]/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_d = nodes[i].xpath('./div[2]/div[3]/span[1]/text()')
            news_publish_b = nodes[i].xpath('./div[2]/div[3]/span[2]/text()')
            news_publish_y = nodes[i].xpath('./div[2]/div[3]/span[3]/text()')
            news_publish_struct = news_publish_y + '-' + news_publish_b + '-' + news_publish_d
            news_publish_time = time.strftime('%Y-%b-%d', news_publish_struct)
            # news_publish_time =
        except:
            news_publish_time = '2023-01-01'
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):

            news_read = nodes[i].xpath('./div[1]/div/span/text()')[0]
            news_page_url = 'http://www.cps-net.org.cn/' + nodes[i].xpath('.//div/a/@href')[0]

            news_author, news_imgs, news_content_type, news_content, source, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, news_read, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]
            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # print(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSFIY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url)
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    result = extractor.extract(content_text)
    news_content = result.get('content')
    try:
        news_author = result.get('author')
    except:
        news_author = ''
    try:
        source = re.findall(r'文章来源：(.*?)', content_text, re.M | re.S)[0]
    except:
        source = ''

    try:
        news_imgs = selector_page.xpath('//*/article//img/@src')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'
    try:
        click_count = re.findall(r'阅读次数：(.*?)', content_text, re.M | re.S)[0]
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '新闻资讯-科研资讯': 'http://www.cps-net.org.cn/News/Content/index/cateid/59.do',
        '学会动态': 'http://www.cps-net.org.cn/Home/Content/index/cateid/75.do',
        '通知': 'http://www.cps-net.org.cn/Home/Content/index/cateid/76.do',
        '科研进展': 'http://www.cps-net.org.cn/Home/Content/index/cateid/80.do',
        '科普天地': 'http://www.cps-net.org.cn/Home/Content/index/cateid/81.do',
        '科学普及-科普动态': 'http://www.cps-net.org.cn/Science/Content/index/cateid/256.do',
        '奖励举荐-奖励动态': 'http://www.cps-net.org.cn/Science/Content/index/cateid/255.do',
        '学术会议-活动纪要': 'http://www.cps-net.org.cn/Meeting/Content/index/cateid/262.do',
        '学会党建-园地动态': 'http://www.cps-net.org.cn/News/Content/index/cateid/296.do',
    }

    for key, value in urls.items():
        news_classify = key
        url = value
        html_text, status_code = get_html(url)
        if status_code != 200:
            break
        deadline = get_data(html_text, news_classify, account_name, science_system,
                            start_time, mongo, project_time)

        if deadline:
            break
        time.sleep(1)

if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'A-02 中国物理学会'
    start_run(args.projectname, args.sinceyear, account_name)
