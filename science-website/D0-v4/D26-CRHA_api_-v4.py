# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-11-17

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
import urllib3
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_html(url, did, page):
    headers = {
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': 'http://www.crha.cn/',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Mobile Safari/537.36',
        'token': 'yjxyxh',
    }

    params = (
        ('id', did),
        ('type', '1'),
        ('pageNum', page),
        ('pageSize', '10'),
    )

    response = requests.get('http://www.crha.cn/service/content/getContentByTypeId', headers=headers,
                            params=params, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    content = json.loads(html_text)
    try:
        deadline = ree_data(content, news_classify, science_system, mongo,
                            account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
             account_name, project_time, start_time):
    results = content['data']['list']
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for item in results:
        try:
            news_title = item.get('title')
        except:
            news_title = ''
        try:
            news_abstract = item.get('title')
        except:
            news_abstract = ''

        try:
            news_publish = str(item.get('createTime'))[:10]
            news_publish_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(news_publish)))
        except Exception as e:
            logging.warning('Publish Time Was Error: {}'.format(e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = str(item.get('createTime'))[:10]

        # print(news_title)
        # print(news_publish_time)
        # print(news_publish_stamp)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            news_page_url = 'http://www.crha.cn/service/content/getContextInfo?contentId={}'.format(item.get('id'))
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    html_text = json.loads(html_response.text)
    content_text = html_text.get('data')
    # selector_page = etree.HTML(content_text)
    # extractor = GeneralNewsExtractor()
    # result = extractor.extract(content_text)
    try:
        news_content = content_text.get('detail')
        dr = re.compile(r'<[^>]+>', re.S)
        news_content = dr.sub('', news_content).strip()
    except:
        news_content = ''
    try:
        news_author = content_text.get('CREATE_BY')
    except:
        news_author = ''
    try:
        news_imgs = content_text.get('ATTACHMENT')
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = content_text.get('source')
    except:
        source = ''

    try:
        read_count = re.findall(r'阅读量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'>点击次数：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-学会动态': 'http://www.crha.cn/#/Secondary/Xhpage?type=%E5%AD%A6%E4%BC%9A%E5%8A%A8%E6%80%81&did=57&',
        '首页-通知公告': 'http://www.crha.cn/#/Secondary/Xhpage?type=%E5%85%AC%E5%91%8A%E9%80%9A%E7%9F%A5&did=51&',
        '首页-医学前言': 'http://www.crha.cn/#/Secondary/Xhpage?type=%E5%8C%BB%E5%AD%A6%E5%89%8D%E6%B2%BF&did=42&',
        '首页-健康科普': 'http://www.crha.cn/#/Secondary/Xhpage?type=%E5%81%A5%E5%BA%B7%E7%A7%91%E6%99%AE&did=43&',
        '首页-社会服务': 'http://www.crha.cn/#/Secondary/Xhpage?type=%E7%A4%BE%E4%BC%9A%E6%9C%8D%E5%8A%A1&did=37&',
        '首页-新闻聚焦': 'http://www.crha.cn/#/Secondary/Xhpage?type=%E6%96%B0%E9%97%BB%E8%81%9A%E7%84%A6&did=54&',
        '首页-组织建设': 'http://www.crha.cn/#/Secondary/Xhpage?type=%E7%BB%84%E7%BB%87%E5%BB%BA%E8%AE%BE&did=56&',
        '首页-标准规范': 'http://www.crha.cn/#/Secondary/Xhpage?did=45&type=%E6%A0%87%E5%87%86%E8%A7%84%E8%8C%83',
        '首页-管理制度': 'http://www.crha.cn/#/Secondary/Xhpage?did=61&type=%E7%AE%A1%E7%90%86%E5%88%B6%E5%BA%A6',
        '首页-党建工作-党建之声': 'http://www.crha.cn/#/Secondary/Xhpage?did=16&type=%E5%85%9A%E5%BB%BA%E4%B9%8B%E5%A3%B0',
        '首页-党建工作-学会党建': 'http://www.crha.cn/#/Secondary/Xhpage?did=17&type=%20%E5%AD%A6%E4%BC%9A%E5%85%9A%E5%BB%BA',
        '首页-党建工作-党纪党规': 'http://www.crha.cn/#/Secondary/Xhpage?did=18&type=%20%E5%85%9A%E7%BA%AA%E5%85%9A%E8%A7%84',
        '首页-政策法律': 'http://www.crha.cn/#/Secondary/Xhpage?did=12&type=%E6%94%BF%E7%AD%96%E6%B3%95%E5%BE%8B',
        '首页-理论研究': 'http://www.crha.cn/#/Secondary/Xhpage?did=14&type=%E7%90%86%E8%AE%BA%E7%A0%94%E7%A9%B6',
        '首页-创新转化': 'http://www.crha.cn/#/Secondary/Xhpage?did=28&type=%E5%88%9B%E6%96%B0%E8%BD%AC%E5%8C%96',
        '首页-学术交流': 'http://www.crha.cn/#/Secondary/Xhpage?did=46&type=%E5%AD%A6%E6%9C%AF%E4%BA%A4%E6%B5%81',
        '首页-人才举荐': 'http://www.crha.cn/#/Secondary/Xhpage?did=33&type=%E4%BA%BA%E6%89%8D%E4%B8%BE%E8%8D%90',
    }
    for key, value in urls.items():
        news_classify = key
        did = re.findall(r'did=(.*?)&', value)[0]
        for page in range(1, 50):
            html_text, status_code = get_html(value, did, page)
            if status_code != 200:
                logging.warning('Has Not Got the Html\'s Correct Response')
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'D-26T 中国研究型医院学会'
    start_run(args.projectname, args.sinceyear, account_name)
