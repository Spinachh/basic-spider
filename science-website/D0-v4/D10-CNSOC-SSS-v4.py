# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-11-14

import random
import re
import io
import sys
import time
import json
import urllib3
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content1
    collection = db['{}'.format(c_name)]
    return collection


def get_html(url):
    response = requests.get(url, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)
    news_classify_part = '-'.join(news_classify.split('-')[:-1])
    item_list = ['首页-健康中国行动', '首页-资讯中心-学会新闻', '首页-党建强会', '首页-会员天地']

    try:
        if news_classify_part not in item_list:
            part1_nodes = selector.xpath('//*[@class="notice_ul"]/div')
            deadline = xpath_data(part1_nodes, news_classify, science_system, mongo,
                                  account_name, project_time, start_time)
            if deadline:
                return deadline

        else:
            part2_nodes = selector.xpath('//*[@class="row news_info"]/div[2]')
            deadline = xpath2_data(part2_nodes, news_classify, science_system, mongo,
                                   account_name, project_time, start_time)
            if deadline:
                return deadline

        if news_classify_part in ['首页-党建强会-学会党建', '首页-党建强会-学习园地']:
            part3_nodes = selector.xpath('//*[@class="notice_li"]')
            deadline = xpath3_data(part3_nodes, news_classify, science_system, mongo,
                                   account_name, project_time, start_time)
            if deadline:
                return deadline

    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./span[2]/a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./span[2]/a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span[1]/text()')[0].replace('/', '-')
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./span[2]/a/@href')[0]
            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.cnsoc.org' + url_part2
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def xpath2_data(part_nodes, news_classify, science_system, mongo,
                account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./div[1]/span/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./div[3]/span/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./div[2]/span/text()')[0].strip()
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./div[4]//a/@href')[0]
            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.cnsoc.org' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def xpath3_data(part_nodes, news_classify, science_system, mongo,
                account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('./span[2]/a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./span[2]/a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('./span[1]/text()')[0].strip()
        except:
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            url_part2 = part_nodes[i].xpath('./span[2]/a/@href')[0]
            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.cnsoc.org' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="container"]//span/text() | '
                                           '//*[@class="container"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="container"]//span//img/@src |'
                                        '//*[@class="container"]//p//img/@src | '
                                        '//*[@class="container"]//div//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        # '首页-资讯中心-公告-科学普及': 'https://www.cnsoc.org/scienpopulg/page1.html',
        # '首页-资讯中心-公告-继续教育': 'https://www.cnsoc.org/contineducg/page1.html',
        # '首页-资讯中心-公告-培训认证': 'https://www.cnsoc.org/traincertig/page1.html',
        # '首页-资讯中心-公告-科技项目': 'https://www.cnsoc.org/scientecprog/page1.html',

        # '首页-资讯中心-公告-学术交流': 'https://www.cnsoc.org/acadconfg/page1.html',
        # '首页-资讯中心-公告-人才服务': 'https://www.cnsoc.org/talentserviceg/page1.html',
        # '首页-资讯中心-公告-其他': 'https://www.cnsoc.org/otherNotice/page1.html',

        '首页-资讯中心-学会新闻-学术交流': 'https://www.cnsoc.org/acadconfn/page1.html',
        # '首页-资讯中心-学会新闻-科学普及': 'https://www.cnsoc.org/scienpopuln/page1.html',
        # '首页-资讯中心-学会新闻-继续教育': 'https://www.cnsoc.org/contineducn/page1.html',
        # '首页-资讯中心-学会新闻-培训认证': 'https://www.cnsoc.org/traincertin/page1.html',
        '首页-资讯中心-学会新闻-科技项目': 'https://www.cnsoc.org/scientecpron/page1.html',
        '首页-资讯中心-学会新闻-人才服务': 'https://www.cnsoc.org/talentservicen/page1.html',
        # '首页-资讯中心-学会新闻-其他': 'https://www.cnsoc.org/othernews/page1.html',
        '首页-资讯中心-学会新闻-最新成果': 'https://www.cnsoc.org/latesachie/page1.html',
        # '首页-资讯中心-学会新闻-工作计划': 'https://www.cnsoc.org/workplan/page1.html',
        # '首页-资讯中心-学会新闻-视频专区': 'https://www.cnsoc.org/videoarea/page1.html',

        # '首页-学术交流-国际交流': 'https://www.cnsoc.org/internexch/page1.html',
        # '首页-学术交流-国际会讯': 'https://www.cnsoc.org/interconnews/page1.html',

        # '首页-健康中国行动-政策解读': 'https://www.cnsoc.org/chpolicy/page1.html',
        # '首页-健康中国行动-权威声音-特殊': 'https://www.cnsoc.org/chAuthority/page1.html',
        # '首页-健康中国行动-公益活动-特殊': 'https://www.cnsoc.org/chExperts/page1.html',
        # '首页-健康中国行动-媒体报道': 'https://www.cnsoc.org/chMedia/page1.html',
        # '首页-健康中国行动-精彩瞬间': 'https://www.cnsoc.org/chMoment/page1.html',

        # '首页-科技服务-政策标准': 'https://www.cnsoc.org/policys/page1.html',
        # '首页-党建强会-学习贯彻党的二十大精神': 'https://www.cnsoc.org/casrs/page1.html',

        # '首页-党建强会-学会党建-特殊': 'https://www.cnsoc.org/xhparty/page1.html',
        # '首页-党建强会-学习园地-特殊': 'https://www.cnsoc.org/learngarden/page1.html',
        # '首页-党建强会-图片集锦': 'https://www.cnsoc.org/photoslide/page1.html',
        # '首页-会员天地-会员活动': 'https://www.cnsoc.org/photoslide/page1.html',
        # '首页-公益活动-特殊': 'https://www.cnsoc.org/photoslide/page1.html',

    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            url = value.replace('page1', 'page' + str(page))
            html_text, status_code = get_html(url)
            if status_code != 200:
                logging.warning('Has Not Got the Html\'s Correct Response')
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'D-10 中国营养学会'
    start_run(args.projectname, args.sinceyear, account_name)
