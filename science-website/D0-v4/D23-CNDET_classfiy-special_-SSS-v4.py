# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-11-16

import random
import re
import io
import sys
import time
import json
import urllib3
import argparse
import cchardet
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content1
    collection = db['{}'.format(c_name)]
    return collection


def get_html(url):
    response = requests.get(url, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        if 'special' not in news_classify:
            # part1_nodes = selector.xpath('//*[@id="content"]/div/div')
            part1_nodes = selector.xpath('//*[@class="wpb_wrapper"]/div/div/div')
        else:
            part1_nodes = selector.xpath('//*[@class="two_list"]/li')[1:]

        deadline = xpath_data(part1_nodes, news_classify, science_system, mongo,
                              account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('./article/div[2]//a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('./a/text()')[0].strip()

        except:
            news_title = ''

        try:
            if 'special' not in news_classify:
                news_abstract = part_nodes[i].xpath('./article/div[2]//a/text()')[0].strip()
            else:
                news_abstract = part_nodes[i].xpath('./a/text()')[0].strip()

        except:
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_time = part_nodes[i].xpath('./@data-date')[0].split('T')[0]
            else:
                news_publish_time = part_nodes[i].xpath('./span/text()')[0].strip()
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('./article/div[2]//a/@href')[0]
                else:
                    url_part2 = part_nodes[i].xpath('./a/@href')[0]

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                if 'special' not in news_classify:
                    news_page_url = 'http://www.cndent.com' + url_part2
                else:
                    news_page_url = 'https://hy.cndent.com' + url_part2
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url, news_classify)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url, news_classify):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        if 'special' not in news_classify:
            news_content = selector_page.xpath('//*[@class="wf-container-main"]//span//text() | '
                                               '//*[@class="wf-container-main"]//p//text()'
                                               )
        else:
            news_content = selector_page.xpath('//*[@class="NewsText"]//span//text() | '
                                               '//*[@class="NewsText"]//p//text()'
                                               )
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者：(.*?) ', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        if 'special' not in news_classify:
            news_imgs = selector_page.xpath('//*[@class="wf-container-main"]//span//img/@src |'
                                            '//*[@class="wf-container-main"]//p//img/@src | '
                                            '//*[@class="wf-container-main"]//div//img/@src')[0]
        else:
            news_imgs = selector_page.xpath('//*[@class="NewsText"]//span//img/@src |'
                                            '//*[@class="NewsText"]//p//img/@src | '
                                            '//*[@class="NewsText"]//div//img/@src')[0]
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="xq_con"]//span//video/@src |'
                                         ' //*[@class="xq_con"]//p//video/@src |'
                                         ' //*[@class="xq_con"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        if 'special' not in news_classify:
            source = re.findall(r'来源:(.*?)</span', content_text, re.M | re.S)[0].strip()
        else:
            source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击数：(.*?) ', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        # '首页-会员中心-新闻资讯-special': 'https://hy.cndent.com/yxerkh/index_2.html',
        # '首页-口腔资讯-通知公告': 'http://www.cndent.com/archives/category/%e5%ad%a6%e4%bc%9a%e5%8a%a8%e6%80%81/%e9%80%9a%e7%9f%a5%e5%85%ac%e5%91%8a/page/1',
        # '首页-口腔资讯-业界新闻': 'http://www.cndent.com/archives/category/%e5%8f%a3%e8%85%94%e8%b5%84%e8%ae%af/%e4%b8%9a%e7%95%8c%e6%96%b0%e9%97%bb/page/1',
        # '首页-口腔资讯-学会动态': 'http://www.cndent.com/archives/category/%e5%8f%a3%e8%85%94%e8%b5%84%e8%ae%af/%e5%ad%a6%e4%bc%9a%e5%8a%a8%e6%80%81/page/1',
        # '首页-口腔资讯-学会周报': 'http://www.cndent.com/archives/category/%e5%ad%a6%e4%bc%9a%e5%91%a8%e6%8a%a5/page/1',
        # '首页-口腔资讯-公益活动-科普园地-科普新闻': 'http://www.cndent.com/%E7%A7%91%E6%99%AE%E6%96%B0%E9%97%BB/page/1',
        '首页-口腔资讯-公益活动-科普园地-大众口腔': 'http://www.cndent.com/%e5%a4%a7%e4%bc%97%e5%8f%a3%e8%85%94/page/1',
        '首页-口腔资讯-公益活动-科普园地-专家访谈': 'http://www.cndent.com/%e4%b8%93%e5%ae%b6%e8%ae%bf%e8%b0%88/page/1',
        '首页-口腔资讯-公益活动-科普园地-大学生科普作品': 'http://www.cndent.com/%e5%a4%a7%e5%ad%a6%e7%94%9f%e7%a7%91%e6%99%ae%e4%bd%9c%e5%93%81/page/1',
        '首页-党的建设-special': 'https://www.cpma.org.cn/zhyfyxh/ddjs/new_piclist_1.shtml',
    }

    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            if 'special' not in news_classify:
                url = value.replace('/page/1', '/page/' + str(page))
            else:
                if page == 1:
                    url = value.replace('index_2.html', 'index.html')
                else:
                    url = value.replace('index_2.html', 'index_' + str(page) + 'html')

            html_text, status_code = get_html(url)
            # print(html_text)
            # breakpoint()
            if status_code != 200:
                logging.warning('Has Not Got the Html\'s Correct Response')
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'D-23T 中华口腔医学会'
    start_run(args.projectname, args.sinceyear, account_name)
