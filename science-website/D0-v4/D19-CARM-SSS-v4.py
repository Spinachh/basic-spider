# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-11-15

import random
import re
import io
import sys
import time
import json
import argparse
import urllib3
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content1
    collection = db['{}'.format(c_name)]
    return collection


def get_pageId(url):
    html_text = requests.get(url).text
    pageId = re.findall(r"'pageId':'(.*?)'", html_text)[0]
    return pageId


def get_html(url, page, pageId):

    cookies = {
        'slb-route': 'c05fb4c0c162890a04df316c99c8484c',
        'UM_distinctid': '18924057848753-0719f48b22821d-26031d51-1fa400-189240578491823',
        'CNZZDATA1280253506': '1255845425-1635811662-%7C1635811662',
    }

    headers = {
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Referer': url,
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/114.0.0.0 Safari/537.36',
        'X-Requested-With': 'XMLHttpRequest',
        'sec-ch-ua': '"Not.A/Brand";v="8", "Chromium";v="114", "Google Chrome";v="114"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    params = (
        ('parseType', 'bulidstatic'),
        ('webId', '1cff144c34894edb988d70271059414f'),
        ('tplSetId', '6ff177203c09488da9b013a65a224187'),
        ('pageId', pageId),
        ('parseType', 'bulidstatic'),
        ('pageType', 'column'),
        ('tagId', '\u4FE1\u606F\u5217\u8868'),
        ('tplSetId', '6ff177203c09488da9b013a65a224187'),
        ('paramJson', '{"pageNo": %s,"pageSize":"15"}' % page),
    )

    response = requests.get('https://www.carm.org.cn/api-gateway/jpaas-publish-server/front/page/build/unit',
                            headers=headers, params=params, cookies=cookies)

    response.encoding = 'utf-8'
    status_code = response.status_code
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system, mongo, project_time, start_time):

    content = json.loads(html_text)
    try:
        deadline = ree_data(content, news_classify, science_system,
                            mongo, account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
               account_name, project_time, start_time):


    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    content_data = content['data']['html']
    # print(content_data)
    # breakpoint()
    selector = etree.HTML(content_data)
    nodes = selector.xpath('//*[@class="page-content"]//li')

    for i in range(len(nodes)):
        try:
            news_title = nodes[i].xpath('./div[2]//a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = nodes[i].xpath('./div[2]//p/text()')[0].strip()
        except:
            news_abstract = news_title

        try:
            news_publish_year = nodes[i].xpath('./div[1]/span[3]/text()')[0].strip().replace('.', '-')
            news_publish_day = nodes[i].xpath('./div[1]/span[1]/text()')[0].strip()
            news_publish_time = news_publish_year + '-' + news_publish_day
        except:
            news_publish_time = '2022-01-01'

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                url_part2 = nodes[i].xpath('.//a/@href')[0].strip()
            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.carm.org.cn' + url_part2
            # print(news_page_url)
            # breakpoint()
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    # print(content_text)
    # breakpoint()
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="list05"]//span/text() | '
                                           '//*[@class="list05"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者(.*?)</', content_text, re.M | re.S)[0]
        news_author = news_author.split('>')[-3].split('<')[0]
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="list05"]//span//img/@src |'
                                        ' //*[@class="list05"]//p//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'document.write\("(.*?)"', content_text)[0]
    except:
        click_count = ''
    # print(click_count)
    # breakpoint()
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):

    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-新闻中心-学会动态': 'https://www.carm.org.cn/xwzx/xhdt/index.html',
        '首页-新闻中心-图片新闻': 'https://www.carm.org.cn/xwzx/tpxw/index.html',
        '首页-新闻中心-分支机构动态': 'https://www.carm.org.cn/xwzx/fzjgdt/index.html',
        '首页-新闻中心-地方学会动态': 'https://www.carm.org.cn/xwzx/dfxhdt/index.html',
        '首页-党建工作-通知公告': 'https://www.carm.org.cn/djgz/tzgg/index.html',
        '首页-党建工作-党建动态': 'https://www.carm.org.cn/djgz/djdt/index.html',
        '首页-党建工作-康复服务行': 'https://www.carm.org.cn/djgz/kffwx/index.html',
        '首页-党建工作-学习专栏': 'https://www.carm.org.cn/djgz/xxzl/index.html',
        '首页-党建工作-康复科技工作者之家': 'https://www.carm.org.cn/djgz/kfkjgzzzj/index.html',
        '首页-通知公告': 'https://www.carm.org.cn/tzgg/index.html',
        '首页-上级资讯': 'https://www.carm.org.cn/sjzx/index.html',
        '首页-科研学术-通知公告': 'https://www.carm.org.cn/kyxs/tzgg/index.html',
        '首页-科研学术-学术动态': 'https://www.carm.org.cn/kyxs/xsdt/index.html',
        '首页-科研学术-科技奖励': 'https://www.carm.org.cn/kyxs/kjjl/index.html',
        '首页-科研学术-综合学术年会': 'https://www.carm.org.cn/kyxs/zhxsnh/index.html',
        '首页-科研学术-全国康复医疗资源情况调查': 'https://www.carm.org.cn/kyxs/qgkfylzyqkdc/index.html',
        '首页-科研学术-团体标准': 'https://www.carm.org.cn/kyxs/ttbz/index.html',
        '首页-科研学术-中央财政支持社会组织项目': 'https://www.carm.org.cn/kyxs/zyczzcshzzxm/index.html',
        '首页-教育培训-培训资讯': 'https://www.carm.org.cn/jypx/pxzx/index.html',
        '首页-教育培训-通知公告': 'https://www.carm.org.cn/jypx/tzgg/index.html',
        '首页-教育培训-继教项目': 'https://www.carm.org.cn/jypx/jjxm/index.html',
        '首页-教育培训-培训基地': 'https://www.carm.org.cn/jypx/pxjd/index.html',
        '首页-科普公益-科普动态': 'https://www.carm.org.cn/kpgy/kpdt/index.html',
        '首页-科普公益-科普宣传': 'https://www.carm.org.cn/kpgy/kpxc/index.html',
        '首页-会员之家-会员公告': 'https://www.carm.org.cn/hyzj/hygg/index.html',
        '首页-会员之家-会员服务': 'https://www.carm.org.cn/hyzj/hyfw/index.html',
        '首页-会员之家-单位会员名录': 'https://www.carm.org.cn/hyzj/dwhyml/index.html',
        '首页-会员之家-会员风采': 'https://www.carm.org.cn/hyzj/hyfc/index.html',
        '首页-会员之家-会员之窗': 'https://www.carm.org.cn/hyzj/hyzc/index.html',
        '首页-标准制度-学会法规制度': 'https://www.carm.org.cn/bzzd/xhfgzd/index.html',
        '首页-标准制度-其他法规制度': 'https://www.carm.org.cn/bzzd/qtfgzd/index.html',
        '首页-标准制度-团体标准': 'https://www.carm.org.cn/bzzd/ttbz/index.html',

    }

    for key, value in urls.items():
        news_classify = key
        pageId = get_pageId(value)
        # print(pageId)
        for page in range(1, 50):
            html_text, status_code = get_html(value, page, pageId)
            print(html_text)
            breakpoint()
            if status_code != 200:
                break

            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'D-19 中国康复医学会'
    start_run(args.projectname, args.sinceyear, account_name)
