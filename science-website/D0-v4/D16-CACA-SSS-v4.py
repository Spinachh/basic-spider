# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-11-15

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import requests
import logging
import pymongo
import urllib3
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
urllib3.disable_warnings()


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content5
    collection = db['{}'.format(c_name)]
    return collection


def get_max_page(url):
    headers = {
        # 'Referer': 'http://www.caca.org.cn/xhdt/xhdt/',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/117.0.0.0 Safari/537.36',
    }

    max_response = requests.get(url, headers=headers)
    # print(max_response.text)
    # breakpoint()
    max_page = re.findall(r'var maxpage = (.*?);', max_response.text, re.M | re.S)[0]
    schannelId_one = re.findall(r'var schannelId= (.*?);', max_response.text, re.M | re.S)[0]
    schannelId = schannelId_one.replace('0013003', '')
    return int(max_page), schannelId_one, schannelId


def get_html(url):
    response = requests.get(url, verify=False)
    response.encoding = 'gb2312'
    status_code = response.status_code
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    selector = etree.HTML(html_text)

    try:
        if 'sp' not in news_classify:
            part1_nodes = selector.xpath('/html/body/table[3]/tr/td[2]/table[2]/tr[1]/td/table/tr')
        else:
            # part1_nodes = selector.xpath('//*[@valign="top"]/table//tr/td/table//tr[1]')
            part1_nodes = selector.xpath('/html/body/table[3]//tr/td[2]//tr[2]/td/table//table')    # 只针对HIO
        deadline = xpath_data(part1_nodes, news_classify, science_system, mongo,
                              account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Account: {} Classify：{} Get_Data Part1 has not content: {}'
                        .format(account_name, news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    # print(part_nodes)
    # breakpoint()
    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('.//a/text()')[0].strip()
            else:
                news_title = part_nodes[i].xpath('.//a/text()')[0].strip()

        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('.//a/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            if 'sp' not in news_classify:
                news_publish_time = part_nodes[i].xpath('.//td[3]/text()')[0]
                news_publish_time = '20' + news_publish_time

            else:
                news_publish_time = part_nodes[i].xpath('.//td[3]/text()')[0].replace('/', '-')
                news_publish_time = '20' + news_publish_time
        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2023-01-01'
        if '22-' in news_classify:
            break
        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'sp' not in news_classify:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0]
                else:
                    url_part2 = part_nodes[i].xpath('.//a/@href')[0]
            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://www.caca.org.cn' + url_part2
            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'gb2312'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@class="zi14 hanggao24"]//span//text() | '
                                           '//*[@class="zi14 hanggao24"]//p//text() | '
                                           '//*[@class="zi14 hanggao24"]//div//text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="zi14 hanggao24"]//span//img/@src |'
                                        '//*[@class="zi14 hanggao24"]//p//img/@src | '
                                        '//*[@class="zi14 hanggao24"]//div//img/@src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'稿源：(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击量： <ucapsource>(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-协会动态-通知公告': 'http://www.caca.org.cn/xhdt/tzgg/#####http://www.caca.org.cn/system/count//0013003/001000000000/000/000/c0013003001000000000',
        '首页-协会动态-协会动态': 'http://www.caca.org.cn/xhdt/xhdt/#####http://www.caca.org.cn/system/count//0013003/002000000000/000/000/c0013003002000000000',
        '首页-协会动态-省级抗癌协会动态': 'http://www.caca.org.cn/xhdt/sskaxhdt/#####http://www.caca.org.cn/system/count//0013003/005000000000/000/000/c0013003005000000000',
        '首页-协会动态-专业委员会动态': 'http://www.caca.org.cn/xhdt/zywyhdt/#####http://www.caca.org.cn/system/count//0013003/004000000000/000/000/c0013003004000000000',
        '首页-协会动态-中国抗癌协会通讯': 'http://www.caca.org.cn/xhdt/zgkaxhtx/#####http://www.caca.org.cn/system/count//0013003/003000000000/000/000/c0013003003000000000',
        '首页-会员服务-会员之声-sp': 'http://www.caca.org.cn/hyfw/hyzs/#####http://www.caca.org.cn/system/count//0013013/014000000000/000/000/c0013013014000000000',

        '首页-学术会议-会议通知': 'http://www.caca.org.cn/xshy/hytz/#####http://www.caca.org.cn/system/count//0013004/003000000000/000/000/c0013004003000000000',
        '首页-学术会议-CACA指南': 'http://www.caca.org.cn/xshy/cacazn/#####http://www.caca.org.cn/system/count//0013004/011000000000/000/000/c0013004011000000000',
        '首页-学术会议-CACA进校园': 'http://www.caca.org.cn/xshy/CACAjxy/#####http://www.caca.org.cn/system/count//0013004/012000000000/000/000/c0013004012000000000',
        '首页-学术会议-学术研讨': 'http://www.caca.org.cn/xshy/xsyt/#####http://www.caca.org.cn/system/count//0013004/006000000000/000/000/c0013004006000000000',
        '首页-学术会议-会议动态': 'http://www.caca.org.cn/xshy/hydt/#####http://www.caca.org.cn/system/count//0013004/004000000000/000/000/c0013004004000000000',
        # '首页-学术会议-继续教育': 'http://www.caca.org.cn/xshy/jxjy/#####',

        '首页-科普宣传-科普活动': 'http://www.caca.org.cn/kpxc/kphd/#####http://www.caca.org.cn/system/count//0013006/001000000000/000/000/c0013006001000000000',
        '首页-科普宣传-癌症知识': 'http://www.caca.org.cn/kpxc/azzs/#####http://www.caca.org.cn/system/count//0013006/002000000000/000/000/c0013006002000000000',
        '首页-科普宣传-癌症预防': 'http://www.caca.org.cn/kpxc/azyf/#####http://www.caca.org.cn/system/count//0013006/003000000000/000/000/c0013006003000000000',
        '首页-科普宣传-癌症早筛': 'http://www.caca.org.cn/kpxc/zs/#####http://www.caca.org.cn/system/count//0013006/010000000000/000/000/c0013006010000000000',

        # '首页-科普宣传-早诊早治': 'http://www.caca.org.cn/kpxc/zzzz/',
        # '首页-科普宣传-对话希望': 'http://www.caca.org.cn/kpxc/dhxwzt/',
        # '首页-科普宣传-控烟专题': 'http://www.caca.org.cn/kpxc/kongyan/',

        '首页-对外交流-通知公告': 'http://www.caca.org.cn/dwjl/gjhy/#####http://www.caca.org.cn/system/count//0013007/003000000000/000/000/c0013007003000000000',
        '首页-对外交流-外事动态': 'http://www.caca.org.cn/dwjl/wswl/#####http://www.caca.org.cn/system/count//0013007/001000000000/000/000/c0013007001000000000',

        # '首页-对外交流-对外合作': 'http://www.caca.org.cn/dwjl/dwhz/index.shtml',

        '首页-对外交流-HIO英文期刊-sp': 'http://www.caca.org.cn/dwjl/zhzlx/#####http://www.caca.org.cn/system/count//0013007/006000000000/000/000/c0013007006000000000',

        # '首页-癌症康复-康复知识': 'http://www.caca.org.cn/azkf/kfcs/',
        # '首页-癌症康复-饮食健康': 'http://www.caca.org.cn/azkf/ysykf/',
        # '首页-癌症康复-术后功能锻炼': 'http://www.caca.org.cn/azkf/shgndl/',
        # '首页-癌症康复-抗癌明星': 'http://www.caca.org.cn/azkf/kamx/',
        # '首页-癌症康复-癌症护理': 'http://www.caca.org.cn/azkf/azhl/',
        # '首页-癌症康复-抗癌护理': 'http://www.caca.org.cn/azkf/azhl/',

        # '首页-协会党建-工作动态-special': 'http://www.caca.org.cn/xhdj/gzdt/index.shtml',
        # '首页-协会党建-学习资料-special': 'http://www.caca.org.cn/xhdj/xxzl/index.shtml',
        # '首页-协会党建-党员风采-special': 'http://www.caca.org.cn/xhdj/yxdyfc/index.shtml',
        # '首页-协会党建-工作通知-special': 'http://www.caca.org.cn/xhdj/gztz/index.shtml',

    }
    for key, value in urls.items():
        news_classify = key
        req_url_one = value.split('#####')[0]
        req_url_two = value.split('#####')[1]

        #  'http://www.caca.org.cn/system/count/0013003/002000000000/count_page_list_0013003002000000000.js'
        req_max_page_url_one = req_url_two.split('_')[0].replace('/000/000/c', '/count_page_list_')
        req_max_page_url = req_max_page_url_one + '.js'
        max_page, schannelId_one, schannelId = get_max_page(req_max_page_url)

        for page in range(1, max_page + 1):
            if page == 1:
                html_text, status_code = get_html(req_url_one)
            else:
                # 'http://www.caca.org.cn/system/count//0013003/002000000000/000/000/c0013003002000000000_000000043.shtml'

                # PLAN A
                # req_url_two_rel = 'http://www.caca.org.cn/system/count//0013003/' + \
                #                   schannelId + '/000/000/' + schannelId_one + '_0000000' + str(max_page + 2 - page) + '.shtml'

                # PLAN B
                req_url_two_rel = req_url_two + '_0000000' + str(max_page + 2 - page) + '.shtml'
                # print(req_url_two_rel)
                html_text, status_code = get_html(req_url_two_rel)

            # print(html_text)
            # breakpoint()
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'D-16 中国抗癌协会'
    start_run(args.projectname, args.sinceyear, account_name)
