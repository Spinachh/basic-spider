# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-11-17

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import urllib3
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def season_num():
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))

    season_num_dict = {
        'Q1': ['02', '03', '04'],
        'Q2': ['05', '06', '07'],
        'Q3': ['08', '09', '10'],
        'Q4': ['11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        since_time = year_time + '-' + str(int(month_time) - 3) + '-' + '01'
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(int(year_time) - 1) + '-' + q_num
        since_time = str(int(year_time) - 1) + '-' + '10-01'
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content3
    collection = db['{}'.format(c_name)]
    return collection


def get_html(url, page):
    headers = {
        'Accept': 'application/json, text/plain, */*',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'contentType': 'application/json',
        'Host': 'api-stroke-society.mwcare.cn',
        'Origin': 'https://www.chinastroke.net',
        'Referer': 'https://www.chinastroke.net/',
        'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
        'sec-ch-ua-mobile': '?1',
        'sec-ch-ua-platform': '"Android"',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'cross-site',
        'token': '',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/109.0.0.0 Mobile Safari/537.36',
    }
    params = (
        ('limit', '10'),
        ('page', page),
        ('lanuage', 0),
    )
    response = requests.get('https://api-stroke-society.mwcare.cn/sw/swnotice/page?',
                            headers=headers, params=params, verify=False)
    status_code = response.status_code
    response.encoding = 'utf-8'
    return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    content = json.loads(html_text)
    try:
        deadline = ree_data(content, news_classify, science_system, mongo,
                            account_name, project_time, start_time)
        if deadline:
            return deadline
    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def ree_data(content, news_classify, science_system, mongo,
             account_name, project_time, start_time):
    results = content['data']['list']
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    for item in results:
        try:
            news_title = item.get('title').strip()
        except:
            news_title = ''
        try:
            news_abstract = item.get('title').strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = item.get('createDate').split(' ')[0]
        except Exception as e:
            logging.warning('Publish Time Was Error: {}'.format(e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):

            news_api_url = 'https://api-stroke-society.mwcare.cn/sw/swnotice/{}'.format(item.get('id'))
            news_page_url = 'https://www.chinastroke.net/#/notice?id={}'.format(item.get('id'))

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count = get_page_content(news_api_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} CLASSIFY: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_classify, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_api_url):
    html_response = requests.get(news_api_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.json()
    # print(content_text['data']['content'])
    # breakpoint()
    selector_page = etree.HTML(html_response.text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(html_response)
    except:
        result = ''

    try:
        text = re.findall(r'[\u3002\uff1b\uff0c\uff1a\u201c\u201d\uff08\uff09\u3001\uff1f\u300a\u300b\u4e00-\u9fa5]',
                          content_text['data']['content'])

        news_content = ''.join([x.strip() for x in text])
    except:
        news_content = ''
    try:
        news_author = re.findall(r'作者：(.*?) ', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = re.findall(r'src=(.*?)alt=', str(content_text['data']['content']))[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="xq_con"]//span//video/@src |'
                                         ' //*[@class="xq_con"]//p//video/@src |'
                                         ' //*[@class="xq_con"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源：(.*?) ', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'浏览：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击数：(.*?) ', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-通知': 'https://api-stroke-society.mwcare.cn/sw/swnotice/page?limit=10&page=1&lanuage=0',
        '首页-新闻公告': 'https://api-stroke-society.mwcare.cn/sw/swnotice/page?limit=10&page=1&lanuage=0',
        '首页-科技评审': 'https://api-stroke-society.mwcare.cn/sw/article/page?navId=1387326566472478721&page=1&limit=10&lanuage=0',
        '首页-项目活动-项目活动': 'https://api-stroke-society.mwcare.cn/sw/article/page?navId=1384755837210062849&page=1&limit=10&lanuage=0',
        '首页-学术会议-会议通知': 'https://api-stroke-society.mwcare.cn/sw/article/page?navId=1363760055321489410&page=1&limit=10&lanuage=0',
        '首页-关于学会-职能部门': 'https://api-stroke-society.mwcare.cn/sw/article/page?navId=1364486794150539265&page=1&limit=10&lanuage=0',
        '首页-关于学会-表单下载': 'https://api-stroke-society.mwcare.cn/sw/article/page?navId=1386190150115913729&page=1&limit=10&lanuage=0',
        '首页-关于学会-规章制度': 'https://api-stroke-society.mwcare.cn/sw/article/page?navId=1358312706939940866&page=1&limit=10&lanuage=0',
    }
    for key, value in urls.items():
        news_classify = key
        for page in range(1, 50):
            url = value.replace('page=1', 'page=' + str(page))
            html_text, status_code = get_html(url, page)
            if status_code != 200:
                logging.warning('Has Not Got the Html\'s Correct Response')
                break

            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            time.sleep(1)


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'D-28W 中国卒中学会'
    start_run(args.projectname, args.sinceyear, account_name)
