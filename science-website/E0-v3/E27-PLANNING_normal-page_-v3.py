# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2023-07-11

import random
import re
import io
import sys
import time
import json
import argparse
import cchardet
import urllib3
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
urllib3.disable_warnings()
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kexie_website_content
    collection = db.user_content_new2
    return collection


def get_html(cid, page, news_classify):

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/110.0.0.0 Safari/537.36',
    }
    part_node = {
        "news": "['首页-资讯-规划动态','首页-资讯-学会声音','首页-资讯-规划会客厅-special','首页-资讯-活动预告', '首页-资讯-深度报道', '首页-资讯-招标信息']",
        "solicity": "['首页-学会-学会动态','首页-学会-学委会动态', '首页-学会-技术服务','首页-学会-国际合作', '首页-学会-通知公告-special']",
        "report": "['首页-资源-会议报告']",
        "law": "[ '首页-资源-政策法规-政策解读']"
    }

    for k, v in part_node.items():
        if news_classify in v:
            part = k
            if '学会-通知公告' not in news_classify:
                params = (
                    ('cid', cid),
                    ('page', page),
                )

                response = requests.get('https://planning.org.cn/{}/newslist'.format(part),
                                        headers=headers, params=params,
                                        verify=False)
            elif '政策法规' in news_classify:
                params = (
                    ('cid', cid),
                    ('schtype', 'title'),
                    ('page', page),
                )

                response = requests.get('https://planning.org.cn/{}/newslist'.format(part),
                                        headers=headers, params=params,
                                        verify=False)
            else:
                params = (
                    ('page', page),
                )
                response = requests.get('https://planning.org.cn/{}/flyinfo_list'.format(part),
                                        headers=headers, params=params,
                                        verify=False)

            response.encoding = 'utf-8'
            status_code = response.status_code
            return response.text, status_code


def get_data(html_text, news_classify, account_name, science_system,
             mongo, project_time, start_time):
    try:
        if 'special' not in news_classify:
            selector = etree.HTML(html_text)
            if '会议报告' in news_classify:
                part_nodes = selector.xpath('//*[@class="fr w475 mr10 f12 l22"]')
            elif '政策法规-政策解读' in news_classify:
                part_nodes = selector.xpath('//*[@class="w1000 bc zoom mt20"]/div[1]/div')
            else:
                part_nodes = selector.xpath('//*[@class="w1000 bc zoom mt20"]/div[1]/div/div')[1:]

            deadline = xpath_data(part_nodes, news_classify, science_system, mongo,
                                  account_name, project_time, start_time)
            if deadline:
                return deadline
        else:
            selector = etree.HTML(html_text)
            part1_nodes = selector.xpath('//*[@class="w1000 bc zoom mt20"]/div[1]/div')
            deadline = xpath_data(part1_nodes, news_classify, science_system, mongo,
                                  account_name, project_time, start_time)
            if deadline:
                return deadline

    except Exception as e:
        logging.warning('Classify： {} Part1 has not content: {}'.format(news_classify, e))


def xpath_data(part_nodes, news_classify, science_system, mongo,
               account_name, project_time, start_time):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'news_classify', 'crawl_time',
                      'account_name', 'science_system', 'project_time']

    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    # print(part_nodes)
    # breakpoint()
    for i in range(len(part_nodes)):
        try:
            if 'special' not in news_classify:
                news_title = part_nodes[i].xpath('.//a/text()')[0].strip()
            else:
                if '学会-通知公告' not in news_classify:
                    news_title = part_nodes[i].xpath('./div[2]//a/@title')[0].strip()
                else:
                    news_title = part_nodes[i].xpath('.//a/@title')[0].strip()

        except Exception as e:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('.//p/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            if 'special' not in news_classify:
                news_publish_time = part_nodes[i].xpath('./p[2]/text()')[0].split(' ')[0]
            else:
                if '学会-通知公告' not in news_classify:
                    news_publish_time = part_nodes[i].xpath('./div[2]/p[2]/text()')[0].split(' ')[0]
                else:
                    news_publish_time = part_nodes[i].xpath('.//p[2]/text()')[0].split(' ')[0]

        except Exception as e:
            logging.warning('{} {} Publish Time Was Error :{}'.format(account_name, news_classify, e))
            news_publish_time = '2022-01-01'

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                if 'special' not in news_classify:
                    url_part2 = part_nodes[i].xpath('./h4/a/@href')[0]
                else:
                    url_part2 = part_nodes[i].xpath('.//h4/a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} {} Article Url Was Error :{}'.format(account_name, news_classify, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'http://planning.org.cn' + url_part2

            if 'mp.weixin.qq.co' in news_page_url:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_weixin_page_content(news_page_url)

            else:
                news_author, news_imgs, news_content_type, news_content, source, \
                read_count, click_count = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, news_classify, crawl_time,
                               account_name, science_system, project_time,
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))
            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('Account: {} Classfiy: {} Title :{} Publish: {} Was Finished!'
                            .format(account_name, news_classify, news_title, news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):
    html_response = requests.get(news_page_url)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''
    try:
        news_content = selector_page.xpath('//*[@class="theiaStickySidebar"]//span//text() | '
                                           '//*[@class="theiaStickySidebar"]//p//text() | '
                                           '//*[@class="theiaStickySidebar"]//p//span//text()'
                                           )
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'作者：</b>(.*?)</', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="theiaStickySidebar"]//span//img/@src |'
                                        '//*[@class="theiaStickySidebar"]//p//img/@src | '
                                        '//*[@class="theiaStickySidebar"]//div//img/@src |'
                                        '//*[@class="theiaStickySidebar"]//img/@src')[0]
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="theiaStickySidebar"]//span//video/@src |'
                                         ' //*[@class="theiaStickySidebar"]//p//video/@src |'
                                         ' //*[@class="theiaStickySidebar"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r'阅读量： (.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''
    # print(read_count)
    # breakpoint()

    try:
        click_count = re.findall(r'点击量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def get_weixin_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    urls = {
        '首页-资讯-规划动态': 'http://www.planning.org.cn/news/newslist?cid=12&page=',
        '首页-资讯-学会声音': 'http://www.planning.org.cn/news/newslist?cid=11&page=',
        '首页-资讯-规划会客厅-special': 'http://www.planning.org.cn/news/newslist?cid=15&page=',
        '首页-资讯-活动预告': 'http://www.planning.org.cn/news/newslist?cid=14&page=',
        '首页-资讯-深度报道': 'http://www.planning.org.cn/news/newslist?cid=13&page=',
        '首页-资讯-招标信息': 'http://www.planning.org.cn/news/newslist?cid=16&page=',
        '首页-学会-学会动态': 'http://www.planning.org.cn/solicity/newslist?cid=1&page=',
        '首页-学会-学委会动态': 'http://www.planning.org.cn/solicity/newslist?cid=2&page=',
        '首页-学会-技术服务': 'http://www.planning.org.cn/solicity/newslist?cid=4&page=',
        '首页-学会-国际合作': 'http://www.planning.org.cn/solicity/newslist?cid=3&page=',
        '首页-学会-通知公告-special': 'http://planning.org.cn/solicity/flyinfo_list?page=',
        '首页-资源-学会咨询': 'http://planning.org.cn/zx/list?page=',
        '首页-资源-会议报告': 'http://planning.org.cn/report/newslist?page=',
        '首页-资源-政策法规-政策解读': 'http://planning.org.cn/law/newslist?cid=17&page=',
    }

    for key, value in urls.items():
        news_classify = key
        try:
            cid = re.findall(r'cid=(.*?)&', value)[0]
        except:
            cid = ''

        for page in range(1, 50):
            html_text, status_code = get_html(cid, page, news_classify)
            # print(html_text)
            # breakpoint()
            if status_code != 200:
                break
            deadline = get_data(html_text, news_classify, account_name,
                                science_system, mongo, project_time, start_time)
            if deadline:
                break
            logging.warning('{} Page :{} Was Finished! {}'.format('*' * 10, page, '*' * 10))
            time.sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kexie-2023-Q2')
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='2023-04-01')
    args = parser.parse_args()
    account_name = 'E-27T 中国城市规划学会'
    start_run(args.projectname, args.sinceyear, account_name)
