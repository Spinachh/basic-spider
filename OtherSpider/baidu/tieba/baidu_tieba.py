# -*- coding:utf-8 -*-
# Author : mongoole
# Date : 2022/9/19

import os
import io
import re
import sys
import time
import pymongo
import asyncio
import cchardet
import requests
import logging
import ssl
from lxml import etree

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')  # 改变标准输出的默认编码
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongo():
    # 连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # 选择数据库
    db = client.OtherSPider
    # 选择操作的集合
    mongodb = db.content

    return mongodb


def get_response(url):
    cookies = {
        'BIDUPSID': '9F03427B90DB25BE295CF95D7E3393A9',
        'PSTM': '1638164780',
        'ab_jid': '6aeb18b7e35fb1f7fd3d4a18bf074704c251',
        'ab_jid_BFESS': '6aeb18b7e35fb1f7fd3d4a18bf074704c251',
        '__yjs_duid': '1_9304c9c7227a560cdb8b8d30f60c39231638348481078',
        'H_WISE_SIDS': '107312_110085_114550_127969_179348_184716_189755_190796_191067_191247_191371_194085_194511_194530_195343_195468_196050_196428_196528_197242_197711_197957_198257_198418_198929_199082_199177_199466_199574_200150_200738_200744_200993_201055_201191_201408_201534_201576_201699_201979_202058_202137_202476_202759_202823_202911_203190_203254_203360_203605_203989_204032_204131_204201_204405_204676_204779_204824_204859_204917_204940_204950_204973_205008_205086_205095_205218_205232_205239_205429_205580_205838_205909_205921',
        'BAIDUID': '698344C1A5B8247F2D02EB36FA6A20EB:SL=0:NR=10:FG=1',
        'newlogin': '1',
        'BDUSS': 'UpXTjVxMkpxMjhvYTkwc05JY2NKUXJ-OUYycTk2fjZsc09NdlhrQkpMa0ZqQWxqRVFBQUFBJCQAAAAAAAAAAAEAAAC2GSJdbW9uZ29vbGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAX~4WIF~-FiU',
        'BDUSS_BFESS': 'UpXTjVxMkpxMjhvYTkwc05JY2NKUXJ-OUYycTk2fjZsc09NdlhrQkpMa0ZqQWxqRVFBQUFBJCQAAAAAAAAAAAEAAAC2GSJdbW9uZ29vbGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAX~4WIF~-FiU',
        'BAIDUID_BFESS': '698344C1A5B8247F2D02EB36FA6A20EB:SL=0:NR=10:FG=1',
        'ZFY': 'LTPOg4u0AqRAL2vxGOXCmb7Mka7:A3LtUjimyK:AeQd28:C',
        'delPer': '0',
        'PSINO': '2',
        'BDRCVFR[feWj1Vr5u3D]': 'I67x6TjHwwYf0',
        'ZD_ENTRY': 'baidu',
        'H_WISE_SIDS_BFESS': '107312_110085_114550_127969_179348_184716_189755_190796_191067_191247_191371_194085_194511_194530_195343_195468_196050_196428_196528_197242_197711_197957_198257_198418_198929_199082_199177_199466_199574_200150_200738_200744_200993_201055_201191_201408_201534_201576_201699_201979_202058_202137_202476_202759_202823_202911_203190_203254_203360_203605_203989_204032_204131_204201_204405_204676_204779_204824_204859_204917_204940_204950_204973_205008_205086_205095_205218_205232_205239_205429_205580_205838_205909_205921',
        'MCITY': '-131%3A',
        'BA_HECTOR': '2k0g048h0hak2081852ki2dn1hiflil18',
        'H_PS_PSSID': '37147_36543_37300_36885_34812_36804_36789_37243_37317_26350_37277_37372',
        'BDORZ': 'B490B5EBF6F3CD402E515D22BCDA1598',
        'BAIDU_WISE_UID': 'wapp_1663555179218_24',
        'RT': 'z=1&dm=baidu.com&si=26e3e248-b90e-4788-bcbb-1cd354729079&ss=l885siob&sl=1a&tt=1jqd&bcn=https%3A%2F%2Ffclog.baidu.com%2Flog%2Fweirwood%3Ftype%3Dperf',
        'ab_bid': 'd18f4cdc46c6f493bf3b8dd3d91925f49ae2',
        'ab_sr': '1.0.1_Nzk2OWFiOTNiODdiYWJkYjMxMGY5NGE2MDQzYmZjY2IzZjIyZjAwYjAwMzc5Y2MwZjdkYzhiM2E4YjZjODgxODhlMmQ4ZTU2NDEwYzZmYTAwNGYwMGFmYmIyZDhlNzUyNGZlNmVlZjJhNjAzZTNhMWFlYjAyYTkwMDk3NDdjNmQ3MTY4NzExMzc5ZWMwOGQwNGJlOWVhOWI3ZGY4OGJkYzg2NDg3ZmY2MTdmOThjYjA0OGE4M2QyNzk1ZTQ2NTgx',
    }

    headers = {
        'Accept': 'image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8',
        'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8,eo;q=0.7',
        'Connection': 'keep-alive',
        'Sec-Fetch-Dest': 'image',
        'Sec-Fetch-Mode': 'no-cors',
        'Sec-Fetch-Site': 'same-site',
        'Sec-Fetch-User': '?1',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36',
        'sec-ch-ua': '"Google Chrome";v="105", "Not)A;Brand";v="8", "Chromium";v="105"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'Cache-Control': 'max-age=0',
        'Referer': 'https://tieba.baidu.com/',
        'authority': 'haokanupdate.cdn.bcebos.com',
        'accept': '*/*',
        'accept-language': 'zh-CN,zh;q=0.9,en;q=0.8,eo;q=0.7',
        'if-modified-since': 'Mon, 18 Jul 2022 06:48:14 GMT',
        'if-none-match': '"956fb603c1237c923d67d52a983de681"',
        'referer': 'https://tieba.baidu.com/',
        'sec-fetch-dest': 'script',
        'sec-fetch-mode': 'no-cors',
        'sec-fetch-site': 'cross-site',
        'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36',
        'If-None-Match': '7fd2f5487f3960df96a33c959e0396e5',
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'Origin': 'https://tieba.baidu.com',
        'content-type': 'text/plain',
        'cookie': 'BIDUPSID=9F03427B90DB25BE295CF95D7E3393A9; PSTM=1638164780; __yjs_duid=1_9304c9c7227a560cdb8b8d30f60c39231638348481078; H_WISE_SIDS=107312_110085_114550_127969_179348_184716_189755_190796_191067_191247_191371_194085_194511_194530_195343_195468_196050_196428_196528_197242_197711_197957_198257_198418_198929_199082_199177_199466_199574_200150_200738_200744_200993_201055_201191_201408_201534_201576_201699_201979_202058_202137_202476_202759_202823_202911_203190_203254_203360_203605_203989_204032_204131_204201_204405_204676_204779_204824_204859_204917_204940_204950_204973_205008_205086_205095_205218_205232_205239_205429_205580_205838_205909_205921; BAIDUID=698344C1A5B8247F2D02EB36FA6A20EB:SL=0:NR=10:FG=1; newlogin=1; BDUSS=UpXTjVxMkpxMjhvYTkwc05JY2NKUXJ-OUYycTk2fjZsc09NdlhrQkpMa0ZqQWxqRVFBQUFBJCQAAAAAAAAAAAEAAAC2GSJdbW9uZ29vbGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAX~4WIF~-FiU; BDUSS_BFESS=UpXTjVxMkpxMjhvYTkwc05JY2NKUXJ-OUYycTk2fjZsc09NdlhrQkpMa0ZqQWxqRVFBQUFBJCQAAAAAAAAAAAEAAAC2GSJdbW9uZ29vbGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAX~4WIF~-FiU; BAIDUID_BFESS=698344C1A5B8247F2D02EB36FA6A20EB:SL=0:NR=10:FG=1; ZFY=LTPOg4u0AqRAL2vxGOXCmb7Mka7:A3LtUjimyK:AeQd28:C; delPer=0; PSINO=2; BDRCVFR[feWj1Vr5u3D]=I67x6TjHwwYf0; ZD_ENTRY=baidu; H_WISE_SIDS_BFESS=107312_110085_114550_127969_179348_184716_189755_190796_191067_191247_191371_194085_194511_194530_195343_195468_196050_196428_196528_197242_197711_197957_198257_198418_198929_199082_199177_199466_199574_200150_200738_200744_200993_201055_201191_201408_201534_201576_201699_201979_202058_202137_202476_202759_202823_202911_203190_203254_203360_203605_203989_204032_204131_204201_204405_204676_204779_204824_204859_204917_204940_204950_204973_205008_205086_205095_205218_205232_205239_205429_205580_205838_205909_205921; MCITY=-131%3A; BA_HECTOR=2k0g048h0hak2081852ki2dn1hiflil18; H_PS_PSSID=37147_36543_37300_36885_34812_36804_36789_37243_37317_26350_37277_37372; BDORZ=B490B5EBF6F3CD402E515D22BCDA1598; BAIDU_WISE_UID=wapp_1663555179218_24; RT="z=1&dm=baidu.com&si=26e3e248-b90e-4788-bcbb-1cd354729079&ss=l885siob&sl=1a&tt=1jqd&bcn=https%3A%2F%2Ffclog.baidu.com%2Flog%2Fweirwood%3Ftype%3Dperf"; ab_sr=1.0.1_Nzk2OWFiOTNiODdiYWJkYjMxMGY5NGE2MDQzYmZjY2IzZjIyZjAwYjAwMzc5Y2MwZjdkYzhiM2E4YjZjODgxODhlMmQ4ZTU2NDEwYzZmYTAwNGYwMGFmYmIyZDhlNzUyNGZlNmVlZjJhNjAzZTNhMWFlYjAyYTkwMDk3NDdjNmQ3MTY4NzExMzc5ZWMwOGQwNGJlOWVhOWI3ZGY4OGJkYzg2NDg3ZmY2MTdmOThjYjA0OGE4M2QyNzk1ZTQ2NTgx',
        'origin': 'https://tieba.baidu.com',
        'Access-Control-Request-Headers': 'content-type,ev,m-version',
        'Access-Control-Request-Method': 'POST',
        'Ev': '99',
        'M-Version': '1.2.0',
        'Acs-Token': '1663484422262_1663556059400_iXJsV9BYxDohuE93vjDfWihphb17IoG3POMlejdOLZLOtxf+TR8TlTIH6qo3aHf1+SOhDZJW8p/MftrqQ1+0O93aMfx6p9FBLC6h6L6Vm3Vc1qVIleFGb39fHweUjyvXDYlS4crR+3C6m4/aehwclP/H3nxVDs+/P3GN4ALCsEaTy92KWdUwfM5uEM0IIoKs0oMDh5+xAd7jh9aPtfjnSg2pb/LT8j5j3SIvu1GU72XgXCVG4WV05dDVFbEN2DRfU4TcVxuQyj4LyJ9sE/c+Ti0EGcGC5wfuWw4Hw+03vsR1V7eAFKTYdQH7pRa6pzUhgejF5JutTO2yXi1gk5DmCQ==',
    }

    params = (
        ('kw', 'iphone14pro'),
        ('ie', 'utf-8'),
        ('pn', '50'),
    )

    try:
        response = requests.get(url, headers=headers, params=params, cookies=cookies, )
        logging.warning(response.status_code)
        if response.status_code == 200:
            html_content = response.text
            # print(html_content)
            # breakpoint()
            return html_content

    except Exception as e:
        print(e)


def get_data(response, mongodb):
    data_key = ['title', 'publish_time', 'author', 'author_id', 'content', 'reply_count']
    html_text = response

    title = re.findall(r'class="j_th_tit ">(.*?)</a>', html_text, re.S | re.M)
    publish_time = re.findall(r'title="创建时间">(.*?)</span>', html_text, re.S | re.M)

    author = re.findall(r'<span class="tb_icon_author "\r\n          title=(.*?)\r\n          data-field',
                        html_text, re.S | re.M)
    author_id = re.findall(r"data-field='{&quot;user_id&quot;(.*?)}", html_text, re.S | re.M)

    try:
        content = re.findall(r'<div class="threadlist_abs threadlist_abs_onlyline ">(.*?)</div>',
                             html_text, re.S | re.M)
        content = [item.strip() for item in content]

        # content2 = re.findall(
        #     r'<div class="threadlist_abs threadlist_abs_onlyline ">\n            (.*?)\n        </div>',
        #     html_text, re.S | re.M)
    except FileExistsError:
        content = ''
    try:
        reply_count = re.findall(r'title="回复">(.*?)</span>', html_text, re.S | re.M)
    except FileExistsError:
        reply_count = ''

    length_content = len(title)

    for i in range(length_content):
        title_ = title[i]
        publish_time_ = publish_time[i]
        author_ = author[i]
        author_id_ = author_id[i]
        content_ = content[i]
        reply_count_ = reply_count[i]
        data_value = [title_, publish_time_, author_, author_id_, content_, reply_count_]
        data_dict = dict(zip(data_key, data_value))
        logging.warning('Node :{} was finished.'.format(data_dict))
        mongodb.insert_one(data_dict)


def main():
    mongodb = mongo()
    url = 'https://tieba.baidu.com/f?'
    response = get_response(url)
    get_data(response, mongodb)


if __name__ == '__main__':
    main()
