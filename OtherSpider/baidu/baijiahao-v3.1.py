# !/usr/bin/python
# -*-coding:utf-8-*-
# Author：Mongoole
# Date：2020/9/17

import time
import re
import json
import requests
import pymongo
import ssl
import logging
from fake_useragent import UserAgent
import urllib3

urllib3.disable_warnings()
ssl._create_default_https_context = ssl._create_unverified_context


def insert_mongodb():

    # 连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # 选择数据库
    db = client.baijiahao_content
    # 选择操作的集合
    collection = db.user_content
    return collection


def get_user():
    with open('baijiahao_user_lst_kexie', 'r', encoding='utf-8') as f:
        # with open('baijiahao_user_lst_mainmedia','r',encoding='utf-8') as f:
        users = f.readlines()
    return users


def logger_init():

    # 日志输出格式
    logging.basicConfig(format='%(asctime)s,%(msecs)d %(levelname)-4s [%(filename)s:%(lineno)d] %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    # 打印日志级别
    # logging.getLogger().setLevel(logging.DEBUG)
    logging.getLogger().setLevel(logging.WARN)
    # logging.getLogger().setLevel(logging.CRITICAL)
    logger = logging.getLogger()

    return logger


class BaiJiaHao(object):
    since_time = "2020-01-01 00:00:00"
    until_time = "2020-12-01 00:00:00"
    project_name = ""
    project_timezone = ""

    def __init__(self):
        pass

    def get_response(self, url, mongodb_obejct, user_name):

        headers = {
            "user-agent": str(UserAgent().random),
            "Referer": "https://author.baidu.com",
            'Cookie': 'BIDUPSID=9F03427B90DB25BE295CF95D7E3393A9; PSTM=1638164780; BAIDUID=9F03427B90DB25BE98954DF4C666E841:FG=1; BDUSS=F6M0YyUC1ZemljbWNCSmVZSUQ4RmtZbG9XQzVIdkhuQXRqZDFhRUFCQmtBTXhoRVFBQUFBJCQAAAAAAAAAAAEAAAC2GSJdbW9uZ29vbGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGRzpGFkc6RhdX; BDUSS_BFESS=F6M0YyUC1ZemljbWNCSmVZSUQ4RmtZbG9XQzVIdkhuQXRqZDFhRUFCQmtBTXhoRVFBQUFBJCQAAAAAAAAAAAEAAAC2GSJdbW9uZ29vbGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGRzpGFkc6RhdX; __yjs_duid=1_9304c9c7227a560cdb8b8d30f60c39231638348481078; BDRCVFR[feWj1Vr5u3D]=I67x6TjHwwYf0; delPer=0; BAIDUID_BFESS=F697F4F38BFF7B073AE927BF9C5991A4:FG=1; H_PS_PSSID=34444_35296_35105_31254_35239_35489_34584_35490_34871_35233_35321_26350_35562; PSINO=6; RT="z=1&dm=baidu.com&si=qa4vlxqf8g&ss=kxwrn7jr&sl=2&tt=1s4&bcn=https%3A%2F%2Ffclog.baidu.com%2Flog%2Fweirwood%3Ftype%3Dperf&ld=22n&ul=lpd&hd=lpp"; Hm_lvt_fa2206905a842c313cdae058ab9f770f=1641103636; Hmery-Time=2212247356; ab_sr=1.0.1_NGYzNWM1Zjg2ZDJmMWRjZGRmYmI5ODk2YjJhMjIxZWNkZjE4ZTU2ZGY4ZWViYTZmMWNiMTM0YTI1ZTMxZDM2NTUxMDkyMjUwNjcxNWUzYzg1ZGExMjRlM2YyYzEwZmVmNzE2NjFkYjdkYTc3NmEwNTU1MGY3YmVlZjQwNzk4YWU0Y2M3YzZjYzFiNGEwMmJjYTY3NjIxNzQzMjJmMjIwNw==; Hm_lpvt_fa2206905a842c313cdae058ab9f770f=1641113600',
        }

        _jsonp = requests.get(url, headers=headers, verify=False).text
        try:
            # print(json.loads(re.match(".*?({.*}).*",_jsonp,re.S).group(1)))
            content = json.loads(re.match(".*?({.*}).*", _jsonp, re.S).group(1)).get('data')
            # print(content)
            # breakpoint()
            for item in content.get('list'):

                target_dict = {}
                target_dict['user_name'] = user_name
                target_dict['feed_id'] = item['feed_id']
                target_dict['thread_id'] = item['thread_id']
                stamp_publish_time = item['dynamic_ctime']
                str_publish_time = time.localtime(int(stamp_publish_time))
                content_publish_time = time.strftime("%Y-%m-%d %H:%M:%S", str_publish_time)
                target_dict['created'] = content_publish_time

                # new add code
                if content_publish_time >= self.since_time:
                    # if self.until_time >= content_publish_time >= self.since_time:

                    target_dict['updated'] = content_publish_time

                    if 'url' in item['itemData'].keys():
                        target_dict['url'] = item['itemData']['url']
                    else:
                        target_dict['url'] = ''

                    if 'title' in item['itemData'].keys():

                        target_dict['title'] = item['itemData']['title']
                    else:
                        target_dict['title'] = ''

                    if 'article_id' in item['itemData'].keys():
                        target_dict['article_id'] = item['itemData']['article_id']
                    else:
                        target_dict['article_id'] = ''

                    if 'content' in item['itemData'].keys():
                        target_dict['content'] = item['itemData']['content']
                    else:
                        target_dict['content'] = ''

                    if 'duration' in item['itemData'].keys():
                        target_dict['duration'] = item['itemData']['duration']
                    else:
                        target_dict['duration'] = ''

                    if 'source' in item['itemData'].keys():
                        target_dict['source'] = item['itemData']['source']
                    else:
                        target_dict['source'] = ''

                    target_dict['type'] = item['itemType']

                    '''
                    获取文章的详情数据：阅读量等
                    '''
                    origin_url = 'https://mbd.baidu.com/webpage?type=homepage' \
                                 '&action=interact' \
                                 '&format=jsonp' \
                                 '&Tenger-Mhor=2245291379' \
                                 '&uk=nfVcq7tJjMOYtV0Jyqq0xw'
                    params = {
                        "user_type": item["user_type"],
                        "feed_id": item["feed_id"],
                        "thread_id": item["thread_id"],
                        "dynamic_id": item["dynamic_id"],
                        "dynamic_type": item["dynamic_type"],
                        "dynamic_sub_type": item["dynamic_sub_type"],
                        "dynamic_ctime": item["dynamic_ctime"],
                        "is_top": item["is_top"],
                    }
                    url = origin_url + '&params=[' + str(json.dumps(params)) + ']'
                    # print(url)

                    detail_jsonp = requests.get(url, headers=headers).text
                    # print(detail_jsonp.text)
                    try:
                        detail_content = json.loads(re.match(".*?({.*}).*", detail_jsonp, re.S).group(1)).get(
                            'data').get('user_list')
                        for k, v in detail_content.items():
                            detail = v
                        target_dict['praise_num'] = detail['praise_num']
                        target_dict['comment_num'] = detail['comment_num']
                        target_dict['read_num'] = detail['read_num']
                        target_dict['is_praise'] = detail['is_praise']
                        target_dict['forward_num'] = detail['forward_num']
                        target_dict['live_back_num'] = detail['live_back_num']
                    except:
                        # raise ValueError('Invalid Input')
                        target_dict['praise_num'] = ''
                        target_dict['comment_num'] = ''
                        target_dict['read_num'] = ''
                        target_dict['is_praise'] = ''
                        target_dict['forward_num'] = ''
                        target_dict['live_back_num'] = ''
                    finally:
                        target_dict['project_name'] = self.project_name
                        target_dict['project_timezone'] = self.project_timezone
                        mongodb_obejct.insert_one(target_dict)

                else:
                    flag = "0"
                    ctime = content.get('query').get('ctime')
                    return flag, ctime
            flag = str(content.get('hasMore'))
            ctime = content.get('query').get('ctime')
            return flag, ctime
        except Exception as e:
            print("Function get_response Error:{}".format(e))

    def get_data(self, content):
        pass

    def cron_user(self, mongodb_obejct, user_key, user_name):
        # start_ctime = '16410345316865'  #默认为0
        start_ctime = '0'  # 默认为0, 可以调整数值（长时间采集出错情况下）
        page_num = 0
        # user_key = 'nfVcq7tJjMOYtV0Jyqq0xw'
        while True:
            url = 'https://mbd.baidu.com/webpage?tab=main' \
                  '&num=10' \
                  '&uk={}' \
                  '&source=pc' \
                  '&ctime={}' \
                  '&type=newhome' \
                  '&action=dynamic' \
                  '&format=jsonp' \
                  '&otherext=h5_20211229111642' \
                  '&Tenger-Mhor=199188240'.format(user_key, start_ctime)
            # h5_20211229111642&Tenger-Mhor=2212247356&callback=__jsonp41641104243933
            # print(url)
            # breakpoint()
            flag, ctime = self.get_response(url, mongodb_obejct, user_name)

            start_ctime = ctime
            mm = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(start_ctime[:10])))

            logger_init().warning('Article HasMore:{} Time is:{}.TimeStamp :{}'.format(flag, mm, ctime))
            since_time = time.mktime(time.strptime(self.since_time, '%Y-%m-%d %H:%M:%S'))

            if int(since_time) > int(start_ctime[:10]):
                break

            if flag == "0" or flag == 'None':
                break
            page_num += 1
            logger_init().info('{} Page {} was finished!time is {}.'.format(user_name, page_num, mm, ))
            # time.sleep(30)
            time.sleep(15)

    def run(self):

        mongodb_object = insert_mongodb()
        users = get_user()

        for user in users:
            user = user.split('#####')
            user_key = user[1].strip()
            user_name = user[0].strip()
            logger_init().warning('{}'.format(user_name))
            self.cron_user(mongodb_object, user_key, user_name)
            time.sleep(30)


if __name__ == '__main__':
    BaiJiaHaoSpider = BaiJiaHao()
    BaiJiaHaoSpider.since_time = "2022-01-01 00:00:00"  # 指定起始时间，默认时间为2020-01-01
    # BaiJiaHaoSpider.until_time = "2022-01-10 00:00:00"  # 指定终止时间，默认时间为2020-12-01
    # BaiJiaHaoSpider.project_name = "MainMediaResearch"
    BaiJiaHaoSpider.project_name = "KeXieProject"
    BaiJiaHaoSpider.project_timezone = "2022-first-quarter"
    logger_init().info("Spider is crawling!")
    BaiJiaHaoSpider.run()
