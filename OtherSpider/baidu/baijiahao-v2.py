# !/usr/bin/python
# -*-coding:utf-8-*-
# Author：Mongoole
# Date：2020/9/17

import time
import re
import json
import requests
import pymongo
import ssl
import logging
from fake_useragent import UserAgent
import urllib3

urllib3.disable_warnings()
ssl._create_default_https_context = ssl._create_unverified_context

class BaiJiaHao(object):

    since_time = "2020-01-01 00:00:00"
    project_name = ""

    def __init__(self):
        pass

    def get_user(self):
        # with open('baijiahao_user_lst_kexie','r',encoding='utf-8') as f:
        with open('baijiahao_user_lst_mainmedia','r',encoding='utf-8') as f:
            users = f.readlines()
        return users


    def insert_mongodb(self):

        # 连接数据库mongodb
        client = pymongo.MongoClient(host='127.0.0.1', port=27017)
        # 选择数据库
        db = client.baijiahao_content
        # 选择操作的集合
        collection = db.user_content
        return collection


    def get_response(self,url,mongodb_obejct,user_name):
        # self.since_time = '2020-12-30 00:00:00'

        headers = {
            "user-agent": str(UserAgent().random),
            # "Referer": "https://author.baidu.com/home?type=profile&action=profile&mthfr=box_share&context=%7B%22from%22%3A%22ugc_share%22%2C%22app_id%22%3A%221610557152577197%22%7D&qq-pf-to=pcqq.c2c",
            "Referer": "https://author.baidu.com",
            "Cookie":"PSTM=1595320484; BIDUPSID=4018CF966C08495747DE582321A3F75C; BDSFRCVID=en4OJeCmHCSV_ArrZXb97nl4O2KK0gOTHlxkQQH-9EI3JgCVJeC6EG0Ptf8g0KubfD0gogKK0gOTH6KF_2uxOjjg8UtVJeC6EG0Ptf8g0M5; H_BDCLCKID_SF=tJ4DVCIhfID3fJrYhPIV-PAt-U4Xat0XKKOLVb58Jp7keqOzyMb63l4FKl5zXPD8JKoQ_pA-Whk2ep72y-vWMp_uy2F80xFeWDvm-qcH0KQpsIJM5-DWbT8U5f5h0TOMaKviaKJjBMb1MlvDBT5h2M4qMxtOLR3pWDTm_q5TtUJMeCnTDMFhe4tX-NFJJ60tJfK; BDSFRCVID_BFESS=en4OJeCmHCSV_ArrZXb97nl4O2KK0gOTHlxkQQH-9EI3JgCVJeC6EG0Ptf8g0KubfD0gogKK0gOTH6KF_2uxOjjg8UtVJeC6EG0Ptf8g0M5; H_BDCLCKID_SF_BFESS=tJ4DVCIhfID3fJrYhPIV-PAt-U4Xat0XKKOLVb58Jp7keqOzyMb63l4FKl5zXPD8JKoQ_pA-Whk2ep72y-vWMp_uy2F80xFeWDvm-qcH0KQpsIJM5-DWbT8U5f5h0TOMaKviaKJjBMb1MlvDBT5h2M4qMxtOLR3pWDTm_q5TtUJMeCnTDMFhe4tX-NFJJ60tJfK; __yjs_duid=1_af594e0cd474ab1bcb26edf13a00e89b1623896333277; BAIDUID=18C7AF9246F855855ED31F5EFE28160C:FG=1; MCITY=-%3A; delPer=0; H_PS_PSSID=34401_31253_34376_34004_34092_34094_26350_34418_34319_34247_34390_34368; ZD_ENTRY=baidu; PSINO=2; BDUSS=d-VE1hLTVEZVJxTzZWNG1CVzRoRUstNE9XRTlZS3ItR0JVfm5xT2o4ZW5DbDFoRVFBQUFBJCQAAAAAAAAAAAEAAAC2GSJdbW9uZ29vbGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKd9NWGnfTVhVE; BDUSS_BFESS=d-VE1hLTVEZVJxTzZWNG1CVzRoRUstNE9XRTlZS3ItR0JVfm5xT2o4ZW5DbDFoRVFBQUFBJCQAAAAAAAAAAAEAAAC2GSJdbW9uZ29vbGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKd9NWGnfTVhVE; Hmery-Time=171199279; ab_sr=1.0.1_ZTkyYTIxZTBlMWMyZjEzYjU2YTU3MzE0NTA3YTI5ZmEyMmEyZjdjNTI5OGI0YzJiZWIyYWI4ZWQ4MGZiODk2YTUyNDg4YjJhMGZlYWFiYjMzY2U4M2YzMGRmZTA4ZmRmZTE3NTE4MjE2YjFhYWY2NTIwNWRiNDlhZjQ4ZjE1MTVlM2Y4OWNhYzZmYThkN2E2YjU3YTlmOGZlN2EzZDlkZg=="
            }

        _jsonp = requests.get(url,headers=headers,verify=False).text
        # print(_jsonp)
        # breakpoint()
        try:
            #print(json.loads(re.match(".*?({.*}).*",_jsonp,re.S).group(1)))
            content = json.loads(re.match(".*?({.*}).*",_jsonp,re.S).group(1)).get('data')
            # print(content,type(content))
            # breakpoint()
            if content.get('list'):
                for item in content.get('list'):
                    target_dict = {}
                    target_dict['user_name'] = user_name
                    target_dict['feed_id'] = item['feed_id']
                    target_dict['thread_id'] = item['thread_id']
                    stamp_publish_time = item['dynamic_ctime']
                    str_publish_time = time.localtime(int(stamp_publish_time))
                    content_publish_time = time.strftime("%Y-%m-%d %H:%M:%S", str_publish_time)
                    target_dict['created'] = content_publish_time

                    stamp_update_time = item['dynamic_ctime']
                    str_update_time = time.localtime(int(stamp_update_time))
                    content_update_time = time.strftime("%Y-%m-%d %H:%M:%S", str_update_time)

                    #new add code
                    if content_update_time >= self.since_time:

                        target_dict['updated'] = content_update_time
                        if 'url' in item['itemData'].keys():
                            target_dict['url'] = item['itemData']['url']
                        else:
                            target_dict['url'] = ''

                        if 'title' in item['itemData'].keys():

                            target_dict['title'] = item['itemData']['title']
                        else:
                            target_dict['title'] = ''

                        if 'article_id' in item['itemData'].keys():
                            target_dict['article_id'] = item['itemData']['article_id']
                        else:
                            target_dict['article_id'] = ''

                        if 'content' in item['itemData'].keys():
                            target_dict['content'] = item['itemData']['content']
                        else:
                            target_dict['content'] = ''

                        if 'duration' in item['itemData'].keys():
                            target_dict['duration'] = item['itemData']['duration']
                        else:
                            target_dict['duration'] = ''

                        if 'source' in item['itemData'].keys():
                            target_dict['source'] = item['itemData']['source']
                        else:
                            target_dict['source'] = ''

                        target_dict['type'] = item['itemType']


                        '''
                        获取文章的详情数据：阅读量等
                        '''
                        origin_url = 'https://mbd.baidu.com/webpage?type=homepage' \
                              '&action=interact' \
                              '&format=jsonp' \
                              '&Tenger-Mhor=2245291379' \
                              '&uk=nfVcq7tJjMOYtV0Jyqq0xw'
                        params = {
                                    "user_type":item["user_type"],
                                    "feed_id":item["feed_id"],
                                    "thread_id":item["thread_id"],
                                    "dynamic_id":item["dynamic_id"],
                                    "dynamic_type":item["dynamic_type"],
                                    "dynamic_sub_type":item["dynamic_sub_type"],
                                    "dynamic_ctime":item["dynamic_ctime"],
                                    "is_top":item["is_top"],
                        }
                        url = origin_url + '&params=[' + str(json.dumps(params)) + ']'
                        # print(url)

                        detail_jsonp = requests.get(url,headers=headers).text
                        # print(detail_jsonp.text)
                        try:
                            detail_content = json.loads(re.match(".*?({.*}).*",detail_jsonp,re.S).group(1)).get('data').get('user_list')
                            for k,v in detail_content.items():
                                detail = v
                            target_dict['praise_num'] = detail['praise_num']
                            target_dict['comment_num'] = detail['comment_num']
                            target_dict['read_num'] = detail['read_num']
                            target_dict['is_praise'] = detail['is_praise']
                            target_dict['forward_num'] = detail['forward_num']
                            target_dict['live_back_num'] = detail['live_back_num']
                        except:
                            # raise ValueError('Invalid Input')
                            target_dict['praise_num'] = ''
                            target_dict['comment_num'] = ''
                            target_dict['read_num'] = ''
                            target_dict['is_praise'] = ''
                            target_dict['forward_num'] = ''
                            target_dict['live_back_num'] = ''
                        finally:
                            target_dict['project_name'] = self.project_name
                            # target_dict['project_name'] = project_name
                            mongodb_obejct.insert_one(target_dict)

                    else:
                        flag = "0"
                        ctime = content.get('query').get('ctime')
                        return flag,ctime

            flag = str(content.get('hasMore'))
            # print(flag,type(flag))
            # breakpoint()
            ctime = content.get('query').get('ctime')
            return flag,ctime

        except:
            raise ValueError('Invalid Input')


    def cron_user(self,mongodb_obejct,user_key,user_name):
        start_ctime = 0
        page_num = 0
        # user_key = 'nfVcq7tJjMOYtV0Jyqq0xw'
        while True:
            url = 'https://mbd.baidu.com/webpage?tab=main' \
                  '&num=10' \
                  '&uk={}' \
                  '&ctime={}' \
                  '&type=newhome' \
                  '&action=dynamic' \
                  '&format=jsonp' \
                  '&otherext=h5_20210907112413' \
                  '&Tenger-Mhor=2245291379'.format(user_key,start_ctime)

            flag,ctime = self.get_response(url,mongodb_obejct,user_name)

            start_ctime = ctime
            # print(ctime)
            # print(mm)
            # breakpoint()
            mm = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(start_ctime[:10])))

            BaiJiaHao.logger_init().info('Article HasMore:{} Time is:{}'.format(flag,mm))

            if flag == "0" or flag == 'None':
                break
            page_num += 1
            BaiJiaHao.logger_init().info('{} Page {} was finished!time is {}'.format(user_name,page_num,mm))
            # print('{} User {} {} Page {} is finished! {}'.format('-'*10,user_name,'-'*10,page_num,'-'*10))
            time.sleep(50)


    @classmethod
    def logger_init(cls):

        # 日志输出格式
        logging.basicConfig(format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S')

        # 打印日志级别
        # logging.getLogger().setLevel(logging.DEBUG)
        logging.getLogger().setLevel(logging.INFO)
        # logging.getLogger().setLevel(logging.CRITICAL)
        logger = logging.getLogger()

        return logger


    def run(self):

        mongodb_object = self.insert_mongodb()

        users = self.get_user()
        for user in users:
            user = user.split('#####')
            user_key = user[1].strip()
            user_name = user[0].strip()
            BaiJiaHao.logger_init().info('{}'.format(user_name))

            self.cron_user(mongodb_object,user_key,user_name)
            time.sleep(30)


if __name__ == '__main__':
    BaiJiaHaoSpider = BaiJiaHao()
    BaiJiaHaoSpider.since_time= "2021-09-01 00:00:00"     #指定起始时间，默认时间为2020-01-01
    BaiJiaHaoSpider.project_name = "MainMediaResearch"
    BaiJiaHaoSpider.logger_init().info('Spider is crawling!')
    BaiJiaHaoSpider.run()
