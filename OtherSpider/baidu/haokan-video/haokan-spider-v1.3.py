# !/usr/bin/python3
# -*- coding:utf-8 -*-
# Author : mongooses
# Date : 2022/9/09

import os
import io
import re
import sys
import time
import json
import pymongo
import asyncio
import cchardet
import requests
import logging

file_name = time.strftime('%Y-%m-%d', time.localtime(time.time()))
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030') # 改变标准输出的默认编码
logging.basicConfig(filename='haokan_video_user_log_{}'.format(file_name),
                    level=logging.WARN, filemode='a+',
                    format='%(asctime)s %(message)s')


def mongo():
    # 连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # 选择数据库
    db = client.baidu_content
    # 选择操作的集合
    mongodb = db.haokanvideo_user_content

    return mongodb


def get_page_response(url):
    # Cookie 是必须要有
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
        'Cookie': 'BIDUPSID=32E095E5D24803078EB30E4C028774BD; PSTM=1661335076; BAIDUID=32E095E5D2480307437B94CA048DF21C:FG=1; PC_TAB_LOG=video_details_page; COMMON_LID=9096f85c78adcca04b59df093e3fb7ba; Hm_lvt_4aadd610dfd2f5972f1efee2653a2bc5=1661479737; MCITY=-131%3A; BDORZ=B490B5EBF6F3CD402E515D22BCDA1598; H_PS_PSSID=36555_37300_36885_36570_36805_37174_37259_26350_37277_37203; BA_HECTOR=2g2h05ag8l8h8h0l2l25iukh1hhl62g18; BAIDUID_BFESS=32E095E5D2480307437B94CA048DF21C:FG=1; ZFY=lu6WamHEEhgr4dHWWRWJNFzciC:BdLYV8fpglIQQqsVU:C; delPer=0; PSINO=2; BCLID=11017353301307405461; BCLID_BFESS=11017353301307405461; BDSFRCVID=wX-OJexroG06Lmnjy7QV7nl4O3qMFyTTDYLEJs2qYShnrsPVJeC6EG0PtoWQkz--EHtdogKK0mOTHUkF_2uxOjjg8UtVJeC6EG0Ptf8g0M5; BDSFRCVID_BFESS=wX-OJexroG06Lmnjy7QV7nl4O3qMFyTTDYLEJs2qYShnrsPVJeC6EG0PtoWQkz--EHtdogKK0mOTHUkF_2uxOjjg8UtVJeC6EG0Ptf8g0M5; H_BDCLCKID_SF=tRk8oK-atDvDqTrP-trf5DCShUFs-l3TB2Q-XPoO3Kt-qMIRMlQb2tvWhxkJW5jiWCrX2Mbgy4op8P3y0bb2DUA1y4vpKMJIW2TxoUJ2XhrPDfcoqtnWhfkebPRiJPr9QgbPBhQ7tt5W8ncFbT7l5hKpbt-q0x-jLTnhVn0MBCK0hD0wDT8hD6PVKgTa54cbb4o2WbCQLfnr8pcN2b5oQTOBKbueaqofMGCfWJ42BKovOJokXpOUWJDkXpJvQnJjt2JxaqRC5hOGOp5jDh3MBUCzhJ6ie4ROamby0hvctn6cShnaMUjrDRLbXU6BK5vPbNcZ0l8K3l02V-bIe-t2XjQhDHt8J50ttJ3aQ5rtKRTffjrnhPF3yqIfXP6-hnjy3b4qQUIb2fjbJq_6h4kMXTDUyf5XWh3Ry6r42-39LPO2hpRjyxv4bUtuyPoxJpOJX2olWxccHR7WDqnvbURvD-ug3-7PaM5dtjTO2bc_5KnlfMQ_bf--QfbQ0hOhqP-jBRIEoC0XtK-hhCvPKITD-tFO5eT22-usJHAL2hcHMPoosItCKhu5Ml_W2fCf2tAJBbriaRo8BMbUotoHXnJi0btQDPvxBf7p527Iah5TtUJM_prDhq6vqt4bht7yKMnitIj9-pnG2hQrh459XP68bTkA5bjZKxtq3mkjbPbDfn028DKuDjRs-t4f-fIX5to05TIX3b7Ef-3iKq7_bJ7KhUbQj4vL2tT8Ja6p_brsJpA5qq6D2qOxQhFTQt6eJJokLKbXQtOxQqcqEIQHQT3m5-4i0fREeCr4WTnbWb3cWKJV8UbS3tRPBTD02-nBat-OQ6npaJ5nJq5nhMJmb67JDMr0exbH55uetbFt_MK; H_BDCLCKID_SF_BFESS=tRk8oK-atDvDqTrP-trf5DCShUFs-l3TB2Q-XPoO3Kt-qMIRMlQb2tvWhxkJW5jiWCrX2Mbgy4op8P3y0bb2DUA1y4vpKMJIW2TxoUJ2XhrPDfcoqtnWhfkebPRiJPr9QgbPBhQ7tt5W8ncFbT7l5hKpbt-q0x-jLTnhVn0MBCK0hD0wDT8hD6PVKgTa54cbb4o2WbCQLfnr8pcN2b5oQTOBKbueaqofMGCfWJ42BKovOJokXpOUWJDkXpJvQnJjt2JxaqRC5hOGOp5jDh3MBUCzhJ6ie4ROamby0hvctn6cShnaMUjrDRLbXU6BK5vPbNcZ0l8K3l02V-bIe-t2XjQhDHt8J50ttJ3aQ5rtKRTffjrnhPF3yqIfXP6-hnjy3b4qQUIb2fjbJq_6h4kMXTDUyf5XWh3Ry6r42-39LPO2hpRjyxv4bUtuyPoxJpOJX2olWxccHR7WDqnvbURvD-ug3-7PaM5dtjTO2bc_5KnlfMQ_bf--QfbQ0hOhqP-jBRIEoC0XtK-hhCvPKITD-tFO5eT22-usJHAL2hcHMPoosItCKhu5Ml_W2fCf2tAJBbriaRo8BMbUotoHXnJi0btQDPvxBf7p527Iah5TtUJM_prDhq6vqt4bht7yKMnitIj9-pnG2hQrh459XP68bTkA5bjZKxtq3mkjbPbDfn028DKuDjRs-t4f-fIX5to05TIX3b7Ef-3iKq7_bJ7KhUbQj4vL2tT8Ja6p_brsJpA5qq6D2qOxQhFTQt6eJJokLKbXQtOxQqcqEIQHQT3m5-4i0fREeCr4WTnbWb3cWKJV8UbS3tRPBTD02-nBat-OQ6npaJ5nJq5nhMJmb67JDMr0exbH55uetbFt_MK; ab_sr=1.0.1_NGU1NjdlMWM5NWEzNmVjMDYzNjZmMDM5MTBiM2RmODQ1Mjk0MGJhYjQ1M2FmMzI2MGQ0YzViZTEzMGMyMTJiNzVmZjM3ZTg2N2YxOTRjNTM3N2MzYjY5OGQ5ZDU1NjRmZDBhZDBkM2MyNDJkOTI3YzQ4MmYzOWM2ZDg2ZTg5ZWZjMGJlMmRjYzU4ZDQzZmQ3ZDgxZjg3MjcyNGVkM2I1ZQ==; reptileData=%7B%22data%22%3A%2281ff559db72fa6d955cd2b81e02c8247b735cb43bbf936153bb261c2fca9246b881d1b853a7499aa4c3c0cfd7aacf72daca90e75b4d31d3674e8f540a08a5dfc3d45eb2beeebc958b625cfdaea2db623e384b3fcfc0becc35a4c1f5cdbceb932%22%2C%22key_id%22%3A%2230%22%2C%22sign%22%3A%2293452d70%22%7D; hkpcSearch=%u5409%u6797%u79D1%u666E%24%24%24%u4E91%u5357%u5FAE%u79D1%u666E%24%24%24%u5317%u4EAC%u79D1%u534F; Hm_lpvt_4aadd610dfd2f5972f1efee2653a2bc5=1662688143; ariaDefaultTheme=undefined; RT="z=1&dm=baidu.com&si=ifovsm9eppk&ss=l7tt3mln&sl=b&tt=az7&bcn=https%3A%2F%2Ffclog.baidu.com%2Flog%2Fweirwood%3Ftype%3Dperf&ld=hohr&ul=hwvw"',
    }
    try:
        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            html_chariest = cchardet.detect(response.content)
            response.encoding = html_chariest["encoding"]
            html_content = response.text.encode("utf-8").decode("unicode_escape")
            result = re.findall(r'window.__PRELOADED_STATE__ = (.*?);     document.querySelector', html_content, re.S | re.M)[0]
            html_result = json.loads(result)
            return html_result

    except Exception as e:
        logging.warning('Function Get Page Response Error:{}'.format(e))


def get_response(url):

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
        'Cookie': 'BIDUPSID=32E095E5D24803078EB30E4C028774BD; PSTM=1661335076; BAIDUID=32E095E5D2480307437B94CA048DF21C:FG=1; PC_TAB_LOG=video_details_page; COMMON_LID=9096f85c78adcca04b59df093e3fb7ba; Hm_lvt_4aadd610dfd2f5972f1efee2653a2bc5=1661479737; MCITY=-131%3A; BDORZ=B490B5EBF6F3CD402E515D22BCDA1598; H_PS_PSSID=36555_37300_36885_36570_36805_37174_37259_26350_37277_37203; BA_HECTOR=2g2h05ag8l8h8h0l2l25iukh1hhl62g18; BAIDUID_BFESS=32E095E5D2480307437B94CA048DF21C:FG=1; ZFY=lu6WamHEEhgr4dHWWRWJNFzciC:BdLYV8fpglIQQqsVU:C; delPer=0; PSINO=2; BCLID=11017353301307405461; BCLID_BFESS=11017353301307405461; BDSFRCVID=wX-OJexroG06Lmnjy7QV7nl4O3qMFyTTDYLEJs2qYShnrsPVJeC6EG0PtoWQkz--EHtdogKK0mOTHUkF_2uxOjjg8UtVJeC6EG0Ptf8g0M5; BDSFRCVID_BFESS=wX-OJexroG06Lmnjy7QV7nl4O3qMFyTTDYLEJs2qYShnrsPVJeC6EG0PtoWQkz--EHtdogKK0mOTHUkF_2uxOjjg8UtVJeC6EG0Ptf8g0M5; H_BDCLCKID_SF=tRk8oK-atDvDqTrP-trf5DCShUFs-l3TB2Q-XPoO3Kt-qMIRMlQb2tvWhxkJW5jiWCrX2Mbgy4op8P3y0bb2DUA1y4vpKMJIW2TxoUJ2XhrPDfcoqtnWhfkebPRiJPr9QgbPBhQ7tt5W8ncFbT7l5hKpbt-q0x-jLTnhVn0MBCK0hD0wDT8hD6PVKgTa54cbb4o2WbCQLfnr8pcN2b5oQTOBKbueaqofMGCfWJ42BKovOJokXpOUWJDkXpJvQnJjt2JxaqRC5hOGOp5jDh3MBUCzhJ6ie4ROamby0hvctn6cShnaMUjrDRLbXU6BK5vPbNcZ0l8K3l02V-bIe-t2XjQhDHt8J50ttJ3aQ5rtKRTffjrnhPF3yqIfXP6-hnjy3b4qQUIb2fjbJq_6h4kMXTDUyf5XWh3Ry6r42-39LPO2hpRjyxv4bUtuyPoxJpOJX2olWxccHR7WDqnvbURvD-ug3-7PaM5dtjTO2bc_5KnlfMQ_bf--QfbQ0hOhqP-jBRIEoC0XtK-hhCvPKITD-tFO5eT22-usJHAL2hcHMPoosItCKhu5Ml_W2fCf2tAJBbriaRo8BMbUotoHXnJi0btQDPvxBf7p527Iah5TtUJM_prDhq6vqt4bht7yKMnitIj9-pnG2hQrh459XP68bTkA5bjZKxtq3mkjbPbDfn028DKuDjRs-t4f-fIX5to05TIX3b7Ef-3iKq7_bJ7KhUbQj4vL2tT8Ja6p_brsJpA5qq6D2qOxQhFTQt6eJJokLKbXQtOxQqcqEIQHQT3m5-4i0fREeCr4WTnbWb3cWKJV8UbS3tRPBTD02-nBat-OQ6npaJ5nJq5nhMJmb67JDMr0exbH55uetbFt_MK; H_BDCLCKID_SF_BFESS=tRk8oK-atDvDqTrP-trf5DCShUFs-l3TB2Q-XPoO3Kt-qMIRMlQb2tvWhxkJW5jiWCrX2Mbgy4op8P3y0bb2DUA1y4vpKMJIW2TxoUJ2XhrPDfcoqtnWhfkebPRiJPr9QgbPBhQ7tt5W8ncFbT7l5hKpbt-q0x-jLTnhVn0MBCK0hD0wDT8hD6PVKgTa54cbb4o2WbCQLfnr8pcN2b5oQTOBKbueaqofMGCfWJ42BKovOJokXpOUWJDkXpJvQnJjt2JxaqRC5hOGOp5jDh3MBUCzhJ6ie4ROamby0hvctn6cShnaMUjrDRLbXU6BK5vPbNcZ0l8K3l02V-bIe-t2XjQhDHt8J50ttJ3aQ5rtKRTffjrnhPF3yqIfXP6-hnjy3b4qQUIb2fjbJq_6h4kMXTDUyf5XWh3Ry6r42-39LPO2hpRjyxv4bUtuyPoxJpOJX2olWxccHR7WDqnvbURvD-ug3-7PaM5dtjTO2bc_5KnlfMQ_bf--QfbQ0hOhqP-jBRIEoC0XtK-hhCvPKITD-tFO5eT22-usJHAL2hcHMPoosItCKhu5Ml_W2fCf2tAJBbriaRo8BMbUotoHXnJi0btQDPvxBf7p527Iah5TtUJM_prDhq6vqt4bht7yKMnitIj9-pnG2hQrh459XP68bTkA5bjZKxtq3mkjbPbDfn028DKuDjRs-t4f-fIX5to05TIX3b7Ef-3iKq7_bJ7KhUbQj4vL2tT8Ja6p_brsJpA5qq6D2qOxQhFTQt6eJJokLKbXQtOxQqcqEIQHQT3m5-4i0fREeCr4WTnbWb3cWKJV8UbS3tRPBTD02-nBat-OQ6npaJ5nJq5nhMJmb67JDMr0exbH55uetbFt_MK; ab_sr=1.0.1_NGU1NjdlMWM5NWEzNmVjMDYzNjZmMDM5MTBiM2RmODQ1Mjk0MGJhYjQ1M2FmMzI2MGQ0YzViZTEzMGMyMTJiNzVmZjM3ZTg2N2YxOTRjNTM3N2MzYjY5OGQ5ZDU1NjRmZDBhZDBkM2MyNDJkOTI3YzQ4MmYzOWM2ZDg2ZTg5ZWZjMGJlMmRjYzU4ZDQzZmQ3ZDgxZjg3MjcyNGVkM2I1ZQ==; reptileData=%7B%22data%22%3A%2281ff559db72fa6d955cd2b81e02c8247b735cb43bbf936153bb261c2fca9246b881d1b853a7499aa4c3c0cfd7aacf72daca90e75b4d31d3674e8f540a08a5dfc3d45eb2beeebc958b625cfdaea2db623e384b3fcfc0becc35a4c1f5cdbceb932%22%2C%22key_id%22%3A%2230%22%2C%22sign%22%3A%2293452d70%22%7D; hkpcSearch=%u5409%u6797%u79D1%u666E%24%24%24%u4E91%u5357%u5FAE%u79D1%u666E%24%24%24%u5317%u4EAC%u79D1%u534F; Hm_lpvt_4aadd610dfd2f5972f1efee2653a2bc5=1662688143; ariaDefaultTheme=undefined; RT="z=1&dm=baidu.com&si=ifovsm9eppk&ss=l7tt3mln&sl=b&tt=az7&bcn=https%3A%2F%2Ffclog.baidu.com%2Flog%2Fweirwood%3Ftype%3Dperf&ld=hohr&ul=hwvw"',
    }
    try:
        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            html_chariest = cchardet.detect(response.content)
            # print(response.headers["content-type"])
            # print(response.encoding)
            # print(html_chardet)
            response.encoding = html_chariest["encoding"]
            # response = response.text.replace('\\"', '#').replace("\\n", "")  # 对json格式中的多余的引号去除
            html_content = response.text.encode("utf-8").decode("unicode_escape")
            html_result = json.loads(html_content)
            # print(html_result)
            # breakpoint()
            return html_result

    except Exception as e:
        print(e)


def get_data(html_result, mongodb):

    if 'video' in html_result.keys():
        video_result = html_result['video']
        has_more = video_result['has_more']
        c_time = video_result['ctime']
        parse_data(video_result, mongodb)

        return has_more, c_time

    elif 'data' in html_result.keys():
        video_result = html_result['data']
        has_more = video_result['has_more']
        c_time = video_result['ctime']
        parse_data(video_result, mongodb)

        return has_more, c_time


def parse_data(video_result, mongodb):

    y_time = time.strftime('%Y', time.localtime(time.time()))

    if 'results' in video_result.keys():
        videos = video_result['results']
        for video in videos:
            video_dict = {}
            video_info = video['content']
            video_vid = video_info['vid']
            video_dict['vid'] = video_vid

            if '年' not in video_info['publish_time']:
                publish_time = y_time + '年' + video_info['publish_time']
            else:
                publish_time = video_info['publish_time']
            video_dict['publish_time'] = publish_time
            video_dict['title'] = video_info['title']
            video_dict['cover_src'] = video_info['cover_src']
            video_dict['duration'] = video_info['duration']
            video_dict['poster'] = video_info['poster']
            video_dict['playcnt'] = video_info['playcnt']
            video_dict['playcntText'] = video_info['playcntText']
            video_url = 'https://haokan.baidu.com/v?vid={}'.format(video_vid)
            video_dict['video_url'] = video_url

            # detail video information
            detail_content = str(get_page_response(video_url))
            # print(detail_content)
            like_num = int(re.findall(r"'like': (.*?),", detail_content, re.S | re.M)[0])
            video_dict['like_num'] = like_num
            fmlike_num = re.findall(r"'fmlike_num': (.*?),", detail_content, re.S | re.M)[0]
            video_dict['fmlike_num'] = fmlike_num
            comment_num = int(re.findall(r"'comment': (.*?),", detail_content, re.S | re.M)[0])
            video_dict['comment_num'] = comment_num
            # print(video_dict)
            # breakpoint()
            mongodb.insert_one(video_dict)
            logging.warning('Title: {} PublishTime: {}'.format(video_info['title'], publish_time))


def run(app_id):
    mongodb = mongo()
    ctime = 0
    while True:
        url = 'https://haokan.baidu.com/web/author/listall?' \
              'app_id={}&ctime={}&rn=10'.format(app_id, ctime)

        if ctime == 0:
            url = 'https://haokan.baidu.com/author/{app_id}'.format(app_id=app_id)
            response = get_page_response(url)
            has_more, c_time = get_data(response, mongodb)
            time.sleep(3)
        else:
            response = get_response(url)
            has_more, c_time = get_data(response, mongodb)
            logging.warning('has_more:{} c_time: {}'.format(has_more, c_time))
            time.sleep(2)

        if has_more == 1:
            ctime = c_time
            continue
        else:
            break


def read_user_id():
    with open(r'haokan_video_user', encoding='utf-8') as f:
        results = f.readlines()
    return results


def main():
    user_ids = read_user_id()
    for app_id in user_ids:
        run(app_id)


if __name__ == '__main__':
    main()
