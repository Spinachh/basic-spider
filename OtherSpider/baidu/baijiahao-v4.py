# -*-coding:utf-8-*-
# Author：Mongoose
# Date：2022/07/05

import time
import re
import json
import requests
import pymongo
import ssl
import logging
from fake_useragent import UserAgent
import urllib3

urllib3.disable_warnings()
ssl._create_default_https_context = ssl._create_unverified_context


class BaiJiaHao(object):
    since_time = "2020-01-01 00:00:00"
    until_time = "2020-12-01 00:00:00"
    project_name = ""
    project_timezone = ""

    def __init__(self):
        pass

    @staticmethod
    def logger_init():
        # 日志输出格式
        logging.basicConfig(format='%(asctime)s,%(msecs)d %(levelname)-4s %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S')
        # 打印日志级别
        # logging.getLogger().setLevel(logging.DEBUG)
        logging.getLogger().setLevel(logging.WARN)
        # logging.getLogger().setLevel(logging.CRITICAL)
        logger = logging.getLogger()
        return logger

    @staticmethod
    def get_user():
        with open(r'user_scienceTec/author_scienceTec', 'r', encoding='utf-8') as f:
            users = f.readlines()
        return users

    @staticmethod
    def mongodb():
        # 连接数据库mongodb
        client = pymongo.MongoClient(host='127.0.0.1', port=27017)
        # 选择数据库
        db = client.baijiahao_content
        # 选择操作的集合
        collection = db.user_content_test
        return collection

    def get_response(self, url, user_name):
        # we need cookie
        headers = {
            "user-agent": str(UserAgent().random),
            "Referer": "https://author.baidu.com",
            'Cookie': 'BIDUPSID=9F03427B90DB25BE295CF95D7E3393A9; PSTM=1638164780; __yjs_duid=1_9304c9c7227a560cdb8b8d30f60c39231638348481078; MCITY=-131%3A; H_WISE_SIDS=107312_110085_114550_127969_179348_184716_189755_190796_191067_191247_191371_194085_194511_194530_195343_195468_196050_196428_196528_197242_197711_197957_198257_198418_198929_199082_199177_199466_199574_200150_200738_200744_200993_201055_201191_201408_201534_201576_201699_201979_202058_202137_202476_202759_202823_202911_203190_203254_203360_203605_203989_204032_204131_204201_204405_204676_204779_204824_204859_204917_204940_204950_204973_205008_205086_205095_205218_205232_205239_205429_205580_205838_205909_205921; BAIDUID=698344C1A5B8247F2D02EB36FA6A20EB:SL=0:NR=10:FG=1; delPer=0; BD_CK_SAM=1; PSINO=2; BAIDUID_BFESS=698344C1A5B8247F2D02EB36FA6A20EB:SL=0:NR=10:FG=1; ZFY=LTPOg4u0AqRAL2vxGOXCmb7Mka7:A3LtUjimyK:AeQd28:C; BD_HOME=1; baikeVisitId=ce1e9f4b-d1c0-4a41-9604-1c412b0e5151; COOKIE_SESSION=913_0_9_6_11_3_1_0_9_3_0_0_899_0_1_0_1655695431_0_1655695430%7C9%230_0_1655695430%7C1; ispeed_lsm=2; BDRCVFR[M7pOaqtZgJR]=I67x6TjHwwYf0; BD_UPN=12314353; sug=3; sugstore=1; ORIGIN=0; bdime=0; BA_HECTOR=852l21a10g800k85201hbso8514; H_PS_PSSID=36543_36726_36455_31253_34813_36691_36166_36693_36697_36570_36652_36746_26350_36467',
        }

        _jsonp = requests.get(url, headers=headers, verify=False).text
        try:
            content = json.loads(re.match(".*?({.*}).*", _jsonp, re.S).group(1)).get('data')
            # print(content)
            # breakpoint()
            for item in content.get('list'):
                target_dict = {'user_name': user_name, 'feed_id': item['feed_id'], 'thread_id': item['thread_id']}
                stamp_publish_time = item['dynamic_ctime']
                str_publish_time = time.localtime(int(stamp_publish_time))
                content_publish_time = time.strftime("%Y-%m-%d %H:%M:%S", str_publish_time)
                target_dict['created'] = content_publish_time

                # get deadline data
                if content_publish_time >= self.since_time:
                    # if self.until_time >= content_publish_time >= self.since_time:

                    target_dict['updated'] = content_publish_time

                    if 'url' in item['itemData'].keys():
                        target_dict['url'] = item['itemData']['url']
                    else:
                        target_dict['url'] = ''

                    if 'title' in item['itemData'].keys():

                        target_dict['title'] = item['itemData']['title']
                    else:
                        target_dict['title'] = ''

                    if 'article_id' in item['itemData'].keys():
                        target_dict['article_id'] = item['itemData']['article_id']
                    else:
                        target_dict['article_id'] = ''

                    if 'content' in item['itemData'].keys():
                        target_dict['content'] = item['itemData']['content']
                    else:
                        target_dict['content'] = ''

                    if 'duration' in item['itemData'].keys():
                        target_dict['duration'] = item['itemData']['duration']
                    else:
                        target_dict['duration'] = ''

                    if 'source' in item['itemData'].keys():
                        target_dict['source'] = item['itemData']['source']
                    else:
                        target_dict['source'] = ''

                    target_dict['type'] = item['itemType']

                    # get detail data: read、like、comment
                    origin_url = 'https://mbd.baidu.com/webpage?type=homepage' \
                                 '&action=interact' \
                                 '&format=jsonp' \
                                 '&Tenger-Mhor=2245291379' \
                                 '&uk=nfVcq7tJjMOYtV0Jyqq0xw'
                    params = {
                        "user_type": item["user_type"],
                        "feed_id": item["feed_id"],
                        "thread_id": item["thread_id"],
                        "dynamic_id": item["dynamic_id"],
                        "dynamic_type": item["dynamic_type"],
                        "dynamic_sub_type": item["dynamic_sub_type"],
                        "dynamic_ctime": item["dynamic_ctime"],
                        "is_top": item["is_top"],
                    }
                    url = origin_url + '&params=[' + str(json.dumps(params)) + ']'

                    detail_jsonp = requests.get(url, headers=headers).text
                    # print(detail_jsonp.text)
                    try:
                        detail_content = json.loads(re.match(".*?({.*}).*", detail_jsonp, re.S).group(1)).get(
                            'data').get('user_list')
                        for k, v in detail_content.items():
                            detail = v
                        target_dict['praise_num'] = detail['praise_num']
                        target_dict['comment_num'] = detail['comment_num']
                        target_dict['read_num'] = detail['read_num']
                        target_dict['is_praise'] = detail['is_praise']
                        target_dict['forward_num'] = detail['forward_num']
                        target_dict['live_back_num'] = detail['live_back_num']
                    except:
                        # raise ValueError('Invalid Input')
                        target_dict['praise_num'] = ''
                        target_dict['comment_num'] = ''
                        target_dict['read_num'] = ''
                        target_dict['is_praise'] = ''
                        target_dict['forward_num'] = ''
                        target_dict['live_back_num'] = ''
                    finally:
                        target_dict['project_name'] = self.project_name
                        target_dict['project_timezone'] = self.project_timezone
                        self.mongodb().insert_one(target_dict)

                else:
                    flag = "0"
                    ctime = content.get('query').get('ctime')
                    return flag, ctime

            flag = str(content.get('hasMore'))
            ctime = content.get('query').get('ctime')
            return flag, ctime

        except Exception as e:
            self.logger_init().warning("Function get_response Error:{}".format(e))

    def get_data(self, content):
        pass

    def cron_user(self, user_key, user_name):
        """
        :param user_key:
        :param user_name:
        :return:
        """
        start_ctime = '0'  # 默认为0, 可以调整数值（长时间采集出错情况下）
        page_num = 0
        # user_key = 'nfVcq7tJjMOYtV0Jyqq0xw'
        while True:
            url = 'https://mbd.baidu.com/webpage?tab=main' \
                  '&num=10' \
                  '&uk={}' \
                  '&source=pc' \
                  '&ctime={}' \
                  '&type=newhome' \
                  '&action=dynamic' \
                  '&format=jsonp' \
                  '&otherext=h5_20220511132503' \
                  '&Tenger-Mhor=4247014859'.format(user_key, start_ctime)  # API URL

            flag, ctime = self.get_response(url, user_name)

            start_ctime = ctime
            mm = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(start_ctime[:10])))

            self.logger_init().warning(
                'UserName: {} Article HasMore:{} Time is:{}.TimeStamp :{}'.format(user_name, flag, mm, ctime))
            since_time = time.mktime(time.strptime(self.since_time, '%Y-%m-%d %H:%M:%S'))

            if int(since_time) > int(start_ctime[:10]):
                break

            if flag == "0" or flag == 'None':
                break
            page_num += 1
            time.sleep(20)

    def start_run(self):
        users = self.get_user()
        for user in users:
            user = user.split('#####')
            user_key = user[1].strip()
            user_name = user[0].strip()
            self.logger_init().warning('{}'.format(user_name))
            self.cron_user(user_key, user_name)
            time.sleep(30)


if __name__ == '__main__':
    BaiJiaHaoSpider = BaiJiaHao()
    BaiJiaHaoSpider.since_time = "2022-07-01 00:00:00"  # 指定起始时间，默认时间为2020-01-01
    # BaiJiaHaoSpider.until_time = "2022-01-10 00:00:00"  # 指定终止时间，默认时间为2020-12-01
    BaiJiaHaoSpider.project_name = "KeXieProject"
    BaiJiaHaoSpider.project_timezone = "2022-Q3"
    BaiJiaHaoSpider.start_run()
