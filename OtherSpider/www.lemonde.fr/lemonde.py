#!/usr/bin/python3
#-*- coding:utf-8 -*-
# Author : Mongoole
# Date : 2020/12/25 
import os
import sys
import time
import re
import requests
from lxml import etree
from fake_useragent import UserAgent
from gne import GeneralNewsExtractor
import pymongo

def mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1',port=27017)
    #选择数据库
    db = client.OtherSPider
    #选择操作的集合
    p = db.foreign_article_2020_12_25

    return p

def get_response(url,page):

    ua = UserAgent().random

    headers = {
        'User-Agent' : ua,
    }

    params = {
        "search_keywords": "chine",
        "start_at": "31/01/2020",
        "end_at": "31/07/2020",
        "search_sort": "date_desc",
        "page": page,
    }
    try:
        response = requests.get(url, headers=headers,params=params)
        response = response.text
        return response
        # print(response)
    except Exception as e:
        print('GET HTML CONTENT WAS ERROR:{}'.format(e))

def handle_data(html,p,page):
    ua = UserAgent().random

    headers = {
        'User-Agent': ua,
        'Cookie':'critical-home-free-desktop=312d3e8e54fa5a1b0c9b8453d688ac4b; _iub_cs-80722398=%7B%22timestamp%22%3A%222020-12-25T02%3A52%3A44.979Z%22%2C%22version%22%3A%221.26.0%22%2C%22purposes%22%3A%7B%221%22%3Atrue%2C%222%22%3Atrue%2C%223%22%3Atrue%2C%224%22%3Atrue%2C%225%22%3Atrue%7D%2C%22id%22%3A80722398%7D; euconsent-v2=CO-9UNjO-9UfiB7D2BFRBGCsAP_AAH_AAAAAHMtd_X_fb39j-_59_9t0eY1f9_7_v-0zjgeds-8Nyd_X_L8X42M7vF36pq4KuR4Eu3LBIQFlHOHUTUmw6IkVrTPsak2Mr7NKJ7PEinMbe2dYGHtfn9VTuZKYr97s___z__-__v__75f_r-3_3_vp9X---_e-By4BJhqXwEWYljgSTRpVCiBCFcSHQCgAooRhaJrCAlcFOyuAj9BAwAQGoCMCIEGIKMWAQAAAABJREBIAeCARAEQCAAEAKkBCAAjQBBYASBgEAAoBoWAEUAQgSEGRwVHKYEBEi0UE8kYAlF3sYYQhlFgBQKP6AAAA; _cb_ls=1; _cb=DVtFj5DltCOUDu6I0N; OB-USER-TOKEN=a31cc369-03fe-46c0-aec7-d6093b423ad5; _gid=GA1.2.837515234.1608864768; lead=ea270a5f-b2e3-485e-ba07-462c0d18cd10; _fbp=fb.1.1608864771211.1931794505; lmd_lead=xsjIHZoesB1k9lPMW8OY%2FjajFTFf1cshY%2FPWgqp8EnPz0nQszAVEMqxrDvk5ibzDplMhPQ%3D%3D; critical-article-free-desktop=312d3e8e54fa5a1b0c9b8453d688ac4b; __gads=ID=52d0d05d6f51cbe4-22adfce350c50001:T=1608865619:S=ALNI_MZKARwA_wUb--n8f0UFnMD3fCZq-w; lmd_ab=8KwKwVr9mhbdEf2zaFEpoMmIG62UB1%2BS1rfixoTVvT75lQJORISyaAd5arVIoUoEEqj%2FL6TAu9oFe5JIkt%2BpshJZ; kw.session_ts=1608885652795; _sp_ses.fb3f=*; _cb_svref=null; measure={"contentLifetime":15,"mapResponseAuth":{"244044216595":{"host":"https://collecte.audience.acpm.fr/m/web/","access":"full","creationDate":1608885654669}}}; atidvisitor=%7B%22name%22%3A%22atidvisitor%22%2C%22val%22%3A%7B%22vrn%22%3A%22-448108-%22%7D%2C%22options%22%3A%7B%22path%22%3A%22%2F%22%2C%22session%22%3A15724800%2C%22end%22%3A15724800%7D%7D; _gcl_au=1.1.1850544129.1608885676; _hjFirstSeen=1; _hjid=f134f9c5-b907-4b19-9a3b-ba83d2777eb7; _hjTLDTest=1; user_session=ju6v4ke2v13a5u72hb9gpsshq3; lmd_random_for_sampling=1608885682305; lmd_a_s=jZhxWIoYGZ2wM6AEPlq3nXdUxcXI755yd2nSloPkqe5PtFsclRmnwJ%2BNmQNQ2%2FNG; lmd_a_m=ewOhRVpJbt02qEw%2FvrovzcLBEAOarUqyPfnpE7%2FzLZI%3D; lmd_a_ld=V0S4Q4pJ365FPXAbj9aSgmJdS6E6ni%2FFNR3LTBpnI88%3D; lmd_a_sp=jZhxWIoYGZ2wM6AEPlq3nXdUxcXI755yd2nSloPkqe5PtFsclRmnwJ%2BNmQNQ2%2FNG; lmd_stay_connected=1; lmd_sso_twipe=%7B%22token%22%3A%22ewOhRVpJbt02qEw%5C%2FvrovzcLBEAOarUqyPfnpE7%5C%2FzLZI%3D%22%7D; abo_referrer=https%3A%2F%2Fsecure.lemonde.fr%2F; _iub_cs-80722398-granular=%7B%7D; _scid=769b7220-1698-4a2d-a685-ec0e4f6d98d1; _sctr=1|1608825600000; critical-article-abo-desktop=312d3e8e54fa5a1b0c9b8453d688ac4b; lmd_cap=_pefz3uori; atidvisitor=%7B%22name%22%3A%22atidvisitor%22%2C%22val%22%3A%7B%22vrn%22%3A%22-448108-%22%2C%22ac%22%3A%22ESS%22%7D%2C%22options%22%3A%7B%22path%22%3A%22%2F%22%2C%22session%22%3A15724800%2C%22end%22%3A15724800%7D%7D; _chartbeat2=.1608864766136.1608886272754.1.DibhnMDiFdnHCWwB9ABwtluQBw1ict.9; kw.pv_session=9; amplitude_id_7dec6d6e90d5288af6e9a882f8def199lemonde.fr=eyJkZXZpY2VJZCI6IjczNGQ5NDFmLTRmZDItNDlkYy1iNjU0LWJhZTgxYmRmMWQ3YVIiLCJ1c2VySWQiOiIyOTYwNTg5MSIsIm9wdE91dCI6ZmFsc2UsInNlc3Npb25JZCI6MTYwODg4NTY1Mjc0MSwibGFzdEV2ZW50VGltZSI6MTYwODg4NjI5Nzc1MCwiZXZlbnRJZCI6NjYsImlkZW50aWZ5SWQiOjQ5LCJzZXF1ZW5jZU51bWJlciI6MTE1fQ==; _ga=GA1.1.1026158893.1608864767; _sp_id.fb3f=e32e1ea0-5883-40e8-9920-40da93533c33.1608864772.3.1608886342.1608879801.99dbb016-a131-41aa-bfba-fa896cbcc70a; _ga_KV15GK2KSR=GS1.1.1608885654.3.1.1608886343.0',
    }

    selector = etree.HTML(html)

    nodes = selector.xpath('//*[@class="js-river-search"]/section')

    for i in range(len(nodes)):
        dic = {}

        # title = nodes[i].xpath('.//section[@class="teaser teaser--inline-picture "]/a/h3[@class="teaser__title"]/text()')
        try:
            title = nodes[i].xpath('.//a/h3[@class="teaser__title"]/text()')[0]
            # print(title)
            dic['title'] = title
        except:
            print('NO CONTENT!')

        try:
            publish_time = nodes[i].xpath('./p/span[@class="meta__date"]/text()')[0]
            # print(publish_time)
            dic['publish_time'] = publish_time
        except:
            print('NO CONTENT!')

        try:
            url = nodes[i].xpath('.//a/@href')[0]
            # print(url)
            dic['news_url'] = url
        except:
            print('NO CONTENT!')


        content_response = requests.get(url,headers=headers).text
        # print(content)
        article_selector = etree.HTML(content_response)
        content = article_selector.xpath('//*[@class="article__content old__article-content-single"]/p/text()'
                                         '|//*[@class="article__content old__article-content-single"]/p/em/text()'
                                         '|//*[@class="article__content old__article-content-single"]//span/text()'
                                         '|//*[@class="article__content old__article-content-single"]//a/text()')
        content = "".join([item.replace('&nbsp','').replace('\n','').replace('\xa0','').strip() for item in content])
        dic['content'] = content
        extractor = GeneralNewsExtractor()
        result_publish_time = extractor.extract(content_response)
        # result_content = extractor.extract(content)
        # print(result)
        update_time = result_publish_time.get('publish_time').replace(':','-')

        dic['update_time'] = update_time

        with open(r'TXT/{}.txt'.format(update_time),'w',encoding='utf-8') as f:
            f.write(content)

        # print(dic)
        p.insert(dic)
        time.sleep(3)
        print('THIS item {} IS FINISHED!'.format(i))
        # breakpoint()


def main():
    p = mongo()
    # url = 'https://www.lemonde.fr/recherche/?search_keywords=chine&start_at=31/01/2020&end_at=31/07/2020&search_sort=date_desc&page=2'
    url = 'https://www.lemonde.fr/recherche/?search_keywords=chine'
    for page in range(1,52):
        html = get_response(url,page)
        handle_data(html,p,page)
        print('THIS  PAGE {} IS FINISH!'.format(page))
        time.sleep(10)

if __name__ == '__main__':
    main()