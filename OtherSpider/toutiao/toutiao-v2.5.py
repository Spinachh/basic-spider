# -*- coding:utf-8 -*-
# Author：Mongoole
# Date：2020/11/30
# change the coding 'gb18030' because 'utf-8'
# 2020-12-07###add article or weitoutiao or short_text detail_page's  publishtime
# 2020-12-09###merge the video or short_text function
# 2021-01-03###1.加代理，2.加西瓜视频的cookie

import re
import io
import random
import json
import sys
import time
import requests
import pymongo
import logging
from fake_useragent import UserAgent
from gne import GeneralNewsExtractor
import execjs

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongo1():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # database
    db = client.KeXietest
    collection_short_text_32 = db.short_text_32
    collection_short_text_56 = db.short_text_56
    collection_article = db.article
    collection_video = db.video
    collection_video_49 = db.video_49
    collection_aq = db.aq

    return (collection_short_text_32, collection_short_text_56,
            collection_article, collection_video, collection_video_49, collection_aq)


def mongo():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # database
    db = client.toutiao_content
    collection = db.user_content

    return collection


# toutiao_video
def cell_00(cell_num, item, data_dict, collection, project_name, project_timezone):
    try:
        data_dict['cell_type'] = cell_num
        data_dict['block_type'] = '视频_' + str(cell_num)
        # data_dict['behot_time'] = item['behot_time']
        data_dict['crawl_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        data_dict['content_id'] = item['group_id']
        data_dict['title'] = item['title']
        data_dict['share_url'] = item['display_url']
        data_dict['share_title'] = item['title']
        data_dict['share_desc'] = item['abstract']
        # data_dict['abstract'] = item['abstract']
        data_dict['create_time'] = item['behot_time']
        # data_dict['article_genre'] = item['article_genre']
        data_dict['content'] = None
        data_dict['comments_count'] = item['comments_count']
        # data_dict['digg_count'] = item['go_detail_count']
        data_dict['show_count'] = item['detail_play_effective_count']
        data_dict['read_count'] = item['detail_play_effective_count']
        data_dict['image_list'] = item['image_list']
        # data_dict['image_url'] = item['image_url']
        # data_dict['tag_url'] = item['tag_url']
        # data_dict['video_duration_str'] = item['video_duration_str']
        data_dict['tag'] = item['chinese_tag']
        data_dict['user_name'] = item['source']

    except Exception as e:
        print('-Function cell_00 inside Error!')

    try:
        # url = 'https://www.ixigua.com/' + data_dict['group_id']
        url = 'https://m.toutiao.com/i' + str(data_dict['content_id']) + '/info/'
        result = get_detail2_video(url)
        publish_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(result['publish_time'])))
        data_dict['digg_count'] = result['digg_count']
        data_dict['publish_time'] = publish_time
        data_dict['forward_count'] = result['repost_count']
        # data_dict['digg_count'] = result['digg_count']
        # result = get_detail_video(url)
        # data_dict['real_url'] = url
        # print('视频发布时间：{},'.format(result[0]))
        # data_dict['publish_time'] = str(result[0])
        # data_dict['digg_count'] = result[1]
    except Exception as e:
        logging.warning('THIS CELL_00 FUNCTION ERROR INFORMATION'.format(e))

    data_dict['project_name'] = project_name
    data_dict['project_timezone'] = project_timezone
    collection.insert_one(data_dict)
    logging.warning('{}-CONTENT WAS insert_one MONGODB!'.format('视频——' + str(cell_num)))


# toutiao_video_4
def cell_49(cell_num, item, data_dict, collection, project_name, project_timezone):
    try:
        data_dict['cell_type'] = cell_num
        data_dict['block_type'] = '视频_' + str(cell_num)
        # data_dict['behot_time'] = item['behot_time']
        data_dict['crawl_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        data_dict['content_id'] = item['group_id']
        data_dict['title'] = item['title']
        data_dict['share_url'] = item['display_url']
        data_dict['share_title'] = item['title']
        data_dict['share_desc'] = item['abstract']
        # data_dict['abstract'] = item['abstract']
        data_dict['create_time'] = item['behot_time']
        # data_dict['article_genre'] = item['article_genre']
        data_dict['content'] = None
        data_dict['comments_count'] = item['comments_count']
        # data_dict['digg_count'] = item['go_detail_count']
        data_dict['show_count'] = item['detail_play_effective_count']
        data_dict['read_count'] = item['detail_play_effective_count']
        data_dict['image_list'] = item['image_list']
        # data_dict['image_url'] = item['image_url']
        # data_dict['tag_url'] = item['tag_url']
        # data_dict['video_duration_str'] = item['video_duration_str']
        data_dict['tag'] = item['chinese_tag']
        data_dict['user_name'] = item['source']

    except Exception as e:
        logging.warning('-Function cell_00 inside Error!')

    try:
        '''
        url = 'https://www.ixigua.com/' + data_dict['group_id']
        result = get_detail_video(url)
        # data_dict['real_url'] = url
        # print('视频发布时间：{},'.format(result[0]))
        data_dict['publish_time'] = str(result[0])
        data_dict['digg_count'] = result[1]
        '''

        url = 'https://m.toutiao.com/i' + str(data_dict['content_id']) + '/info/'
        result = get_detail2_video(url)
        publish_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(result['publish_time'])))
        data_dict['digg_count'] = result['digg_count']
        data_dict['publish_time'] = publish_time
        data_dict['forward_count'] = result['repost_count']
    except Exception as e:
        logging.warning('THIS CELL_49 FUNCTION ERROR INFORMATION'.format(e))
    data_dict['project_name'] = project_name
    data_dict['project_timezone'] = project_timezone
    collection.insert_one(data_dict)
    logging.warning('{}-CONTENT WAS insert_one MONGODB'.format('视频'))


# smal_short_toutiao_first_32
def cell_32(item, data_dict, collection, project_name, project_timezone):
    """
    :param item: api数据列表中的一个item
    :param data_dict: 传入的空的字典
    :param collection: 该类型的数据集合
    :return:
    """

    # print(item)
    data_dict['cell_type'] = 32
    data_dict['block_type'] = '微头条_32'
    data_dict['crawl_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    data_json = item['concern_talk_cell']['packed_json_str']
    data_json = json.loads(data_json)

    data_dict['content_id'] = data_json['thread_id_str']
    data_dict['title'] = str(data_json['title'])
    data_dict['share_url'] = str(data_json['share_url']).replace('\\', '')
    data_dict['share_title'] = str(data_json['share']['share_title']).replace('\n', '')
    data_dict['share_desc'] = str(data_json['share']['share_desc'])

    data_dict['create_time'] = str(data_json['create_time'])
    data_dict['publish_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(data_json['publish_time'])))
    data_dict['content'] = str(data_json['content']).replace('\n', '')
    data_dict['img_lst'] = data_json['origin_image_list']
    data_dict['show_count'] = data_json['show_count']
    data_dict['comment_count'] = data_json['comment_count']
    data_dict['read_count'] = data_json['read_count']
    data_dict['digg_count'] = data_json['digg_count']
    data_dict['forward_count'] = data_json['forward_info']['forward_count']
    data_dict['tag'] = None
    # data_dict['user_id'] = data_json['user']['user_id']
    data_dict['user_name'] = data_json['user']['name']
    # data_dict['verified_content'] = str(data_json['user']['verified_content'])

    # print(data_dict)
    # breakpoint()
    '''
    try:
        url = 'https://m.toutiao.com/i' + str(data_dict['content_id']) + '/info/'
        result = get_detail2_article(url)
        # publish_time = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(int(result['publish_time'])))
        # data_dict['publish_time'] = publish_time
        data_dict['forward_count'] = result['repost_count']

    except Exception as e:
        print('THIS IS GOT CELL_32 FUNCTION ERROR INFORMAITON:{}'.format(e))
    '''
    data_dict['project_name'] = project_name
    data_dict['project_timezone'] = project_timezone
    collection.insert_one(data_dict)
    logging.warning('{}-CONTENT WAS insert_one MONGODB'.format('微头条_32'))


# smal_short_toutiao_second_56
def cell_56(item, data_dict, collection, project_name, project_timezone):
    data_dict['cell_type'] = '56'
    data_dict['block_type'] = '微头条_56'
    data_dict['crawl_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    # print('789,item:{}'.format(item))
    if item['stream_cell']['raw_data']:
        data_json = item['stream_cell']['raw_data']
    else:
        data_json = item
    # data_dict['account_reason'] = item['stream_cell']['ugc_recommend']['reason']
    # data_dict['create_time'] = item['stream_cell']['behot_time']
    # print(data_json)
    # breakpoint()
    # parse_data = data_json.encode('utf-8').decode('unicode_escape')
    # str_data = parse_data.replace('\\','')
    # data = json.loads(str_data)
    data_json = json.loads(data_json)

    # ###source_data
    '''
    # data_dict['source_group_id'] = data['origin_group']['group_id']
    # data_dict['source_user_id'] = data['origin_group']['user_id']
    # data_dict['source_account'] = data['origin_group']['source']
    # data_dict['source_title'] = data['origin_group']['title']
    # data_dict['source_verified_content'] = data['origin_group']['user_info']['verified_content']
    # data_dict['source_desc'] = data['origin_group']['user_info']['desc']
    # data_dict['source_article_url'] = data['origin_group']['article_url']
    '''

    # ###formal_information
    # data_dict['title_prefix'] = data['title_prefix']
    # data_dict['content_id_str'] = data['id_str']
    data_dict['content_id'] = data_json['comment_base']['id']
    try:
        data_dict['title'] = data_json['title']
    except:
        data_dict['title'] = ''

    data_dict['content'] = str(data_json['comment_base']['content'])
    # print('789,data_dict:{}'.format(data_dict))

    data_dict['share_title'] = data_json['comment_base']['share']['share_title']
    data_dict['share_url'] = data_json['comment_base']['share']['share_url']
    data_dict['share_desc'] = data_json['comment_base']['share']['share_desc']
    data_dict['create_time'] = data_json['comment_base']['create_time']
    # data_dict['publish_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(data_json['publish_time'])))

    data_dict['content'] = str(data_json['comment_base']['content']).replace('\n', '')
    data_dict['img_lst'] = data_json['comment_base']['share']['share_cover']['url_list']
    data_dict['show_count'] = data_json['comment_base']['action']['show_count']
    data_dict['comment_count'] = data_json['comment_base']['action']['comment_count']
    data_dict['read_count'] = data_json['comment_base']['action']['read_count']
    # data_dict['digg_count'] = data_json['comment_base']['action']['digg_count']
    data_dict['forward_count'] = data_json['comment_base']['action']['forward_count']
    data_dict['tag'] = None
    # data_dict['user_id'] = data_json['comment_base']['user']['info']['user_id']
    data_dict['user_name'] = data_json['comment_base']['user']['info']['name']

    # data_dict['fans_read_count_new'] = data['comment_base']['action']['fans_read_count_new']
    # data_dict['detail_read_count_merge'] = data['comment_base']['action']['detail_read_count_merge']
    # data_dict['repost_display_count'] = data['comment_base']['action']['repost_display_count']
    # data_dict['fans_read_count_old'] = data['comment_base']['action']['fans_read_count_old']
    # data_dict['bury_count'] = data['comment_base']['action']['bury_count']
    # data_dict['detail_read_count_new'] = data['comment_base']['action']['detail_read_count_new']
    # data_dict['repin_count'] = data['comment_base']['action']['repin_count']
    # data_dict['show_count'] = data['comment_base']['action']['show_count']

    try:
        url = 'https://m.toutiao.com/i' + str(data_dict['content_id']) + '/info/'
        # url = data_dictp['share_url']
        # result,html_content = get_detail_article(url)
        result = get_detail2_article(url)
        publish_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(result['publish_time'])))

        data_dict['publish_time'] = publish_time
        data_dict['forward_count'] = result['repost_count']
        data_dict['digg_count'] = result['digg_count']
        # html_content = result['content']
        # dr = re.compile(r'<[^>]+>', re.S)
        # content_text = dr.sub('', html_content).strip()
        # data_dict['content'] = content_text
    except Exception as e:
        logging.warning('THIS CELL_56 FUNCTION ERROR INFORMATION'.format(e))
    data_dict['project_name'] = project_name
    data_dict["project_timezone"] = project_timezone
    collection.insert_one(data_dict)
    logging.warning('{}-CONTENT WAS insert_one MONGODB'.format('微头条_56'))


# toutiao_article
def cell_60(item, data_dict, collection, project_name, project_timezone):
    data_dict['cell_type'] = 60
    data_dict['block_type'] = '文章_60'
    # data_dict['behot_time'] = item['behot_time']
    data_dict['crawl_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    data_dict['content_id'] = item['item_id']
    data_dict['title'] = item['title']

    data_dict['share_url'] = 'https://www.' + str(item['display_url']).split('//')[1]
    data_dict['share_title'] = item['title']
    data_dict['share_desc'] = item['abstract']
    data_dict['create_time'] = item['behot_time']
    # data_dict['abstract'] = item['abstract']
    # data_dict['article_genre'] = item['article_genre']

    if 'chinese_tag' in item.keys():
        data_dict['chinese_tag'] = item['chinese_tag']
    else:
        data_dict['chinese_tag'] = None

    data_dict['show_count'] = item['go_detail_count']
    data_dict['comments_count'] = item['comments_count']
    data_dict['read_count'] = item['go_detail_count']
    data_dict['image_list'] = item['image_list']
    data_dict['user_name'] = item['source']

    try:
        url = 'https://m.toutiao.com/i' + str(data_dict['content_id']) + '/info/'
        # url = data_dictp['share_url']
        # result,html_content = get_detail_article(url)
        result = get_detail2_article(url)
        publish_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(result['publish_time'])))

        data_dict['publish_time'] = publish_time
        data_dict['forward_count'] = result['repost_count']
        data_dict['digg_count'] = result['digg_count']

        html_content = result['content']

        dr = re.compile(r'<[^>]+>', re.S)
        content_text = dr.sub('', html_content).strip()
        data_dict['content'] = content_text
        # data_dict['content'] = result['content'].replace('\n','')
        # text = re.findall(r'<span>(.*?)</span>', html_content, re.S | re.M)
        # data_dict['forward_count'] = text[3]
        # data_dict['digg_count'] = text[0]

    except Exception as e:
        logging.warning('THIS CELL_60 FUNCTION ERROR INFORMATION'.format(e))
    # print(data_dict)
    # breakpoint()
    data_dict['project_name'] = project_name
    data_dict['project_timezone'] = project_timezone
    collection.insert_one(data_dict)
    logging.warning('{}-CONTENT WAS insert_one MONGODB!'.format('文章_60'))


# toutiao_wenda
def cell_202(item, data_dict, collection, project_name, project_timezone):
    data_dict['cell_type'] = 202
    data_dict['block_type'] = '问答_202'
    data_dict['account_reason'] = item['stream_cell']['ugc_recommend']['reason']
    data_dict['create_time'] = item['stream_cell']['behot_time']
    data_dict['crawl_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

    data_json = item['stream_cell']['raw_data']

    data = json.loads(data_json)
    data_dict['title'] = data['content']['question']['title']

    data_dict['publisth_time'] = time.strftime("%Y-%m-%d %H:%M:%S",
                                               time.localtime(int(data['content']['answer']['create_time'])))
    data_dict['group_id'] = data['group_id']
    data_dict['question_text'] = data['content']['question']['content']['text']
    data_dict['question_rich_text'] = data['content']['question']['content']['rich_text']
    data_dict['answer_abstract_text'] = data['content']['answer']['abstract_text']
    data_dict['brow_count'] = data['content']['answer']['brow_count']
    data_dict['digg_count'] = data['content']['answer']['digg_count']
    data_dict['comment_count'] = data['content']['answer']['comment_count']
    data_dict['forward_count'] = data['content']['answer']['forward_count']
    data_dict['forward_count'] = data['content']['answer']['forward_count']
    data_dict['user_id'] = data['content']['user']['user_id']
    data_dict['user_name'] = data['content']['user']['uname']
    data_dict['user_verify'] = data['content']['user']['user_intro']

    data_dict['project_name'] = project_name
    data_dict['project_timezone'] = project_timezone
    collection.insert_one(data_dict)
    logging.warning('{}-CONTENT WAS insert_one MONGODB!'.format('问答'))


def get_detail_article(url):
    session = requests.session()
    ua = ['ByteSpider', 'ToutiaoSpider']
    headers = {
        'User-Agent': random.choice(ua),
        # 'referer': 'https://www.toutiao.com/c/user/token/{}/?tab=weitoutiao'.format(user_token),
        'cookie': '__ac_signature=_02B4Z6wo00f01-.WjdQAAIDBV8Ty0oahWWvv9olAAJsJ7e; ttcid=014bf22e69d342c9afd81d9606ef716930; s_v_web_id=verify_ccad3fb16c4c7954895e7d36a5c7c82a; tt_webid=6992481721288263182; csrftoken=074b2fe9a1ffa5792a2aea9cf3b8d627; tt_webid=6992481721288263182; _S_WIN_WH=1920_937; _S_DPR=1; _S_IPAD=0; MONITOR_WEB_ID=b252ab1f-bbd4-454a-baf4-f244b6a8b338; tt_scid=FCICsbOQAm6aTclta55FYHQHC1T..oco5kKvYTI56HmR68yHEviSvhQ6FM9j-i1Pd5c3; ttwid=1%7CYaQOswfugEsJ3edRiSTjaD-VeLBlQw3zDpQ026FT61g%7C1628065803%7C32bd523a267df6b4030e9cda86ff6eb609d01392895c7b9af5d9e282f0814bc9'
    }
    headers = session.headers
    response = session.get(url)
    response.encoding = 'utf-8'
    html_content = response.text
    # html_content = response.replace(u'\xa0', u' ')

    extractor = GeneralNewsExtractor()
    result = extractor.extract(html_content)

    return result, html_content


def get_detail2_article(url):
    session = requests.session()
    ua = ['ByteSpider', 'ToutiaoSpider']
    headers = {
        'User-Agent': random.choice(ua),
        # 'referer': 'https://www.toutiao.com/c/user/token/{}/?tab=weitoutiao'.format(user_token),
        'cookie': '__ac_signature=_02B4Z6wo00f01-.WjdQAAIDBV8Ty0oahWWvv9olAAJsJ7e; ttcid=014bf22e69d342c9afd81d9606ef716930; s_v_web_id=verify_ccad3fb16c4c7954895e7d36a5c7c82a; tt_webid=6992481721288263182; csrftoken=074b2fe9a1ffa5792a2aea9cf3b8d627; tt_webid=6992481721288263182; _S_WIN_WH=1920_937; _S_DPR=1; _S_IPAD=0; MONITOR_WEB_ID=b252ab1f-bbd4-454a-baf4-f244b6a8b338; tt_scid=FCICsbOQAm6aTclta55FYHQHC1T..oco5kKvYTI56HmR68yHEviSvhQ6FM9j-i1Pd5c3; ttwid=1%7CYaQOswfugEsJ3edRiSTjaD-VeLBlQw3zDpQ026FT61g%7C1628065803%7C32bd523a267df6b4030e9cda86ff6eb609d01392895c7b9af5d9e282f0814bc9'
    }
    headers = session.headers
    response = session.get(url)
    response.encoding = 'utf-8'
    html_content = response.json()
    # html_content = response.replace(u'\xa0', u' ')
    result = html_content["data"]
    return result


def get_detail_video(url):
    session = requests.session()
    ua = ['ByteSpider', 'ToutiaoSpider']
    headers = {
        'User-Agent': random.choice(ua),
        # 'referer': url,
        'cookie': '_ga=GA1.2.746553625.1602235482; Hm_lvt_db8ae92f7b33b6596893cdf8c004a1a2=1609605699; Hm_lpvt_db8ae92f7b33b6596893cdf8c004a1a2=1609725915; MONITOR_WEB_ID=11efdde4-2af4-4491-9ce1-9304741f4217; ixigua-a-s=1; BD_REF=1; ttwid=1%7CWKcHle4lJAbLMusAPptWgS1rk4ZlfoZvNrC2vjInOsI%7C1625206683%7C8856360fff3417076e4ae1240dee64409dbdd6a2627b1c9d875f03c262cce0f8',
    }
    response = session.get(url, headers=headers, timeout=5)
    # breakpoint()
    response.encoding = 'utf-8'

    '''
    #gne
    extractor = GeneralNewsExtractor()
    result = extractor.extract(response.text)
    print(result)
    print(result['publish_time'])
    print(result['content'].replace('\n',''))
    '''

    # re
    try:
        publish_time = re.findall(r'"video_publish_time":(.*?),"video_like_count"', response.text, re.S | re.M)[0]
        video_like_count = re.findall(r'"video_like_count":(.*?),"video_abstract"', response.text, re.S | re.M)[0]
        publish_time = publish_time.replace('"', '')
        # print(video_like_count)
        publish_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(publish_time)))
        video_like_count = video_like_count
        logging.warning(publish_time, video_like_count)
        return publish_time, video_like_count

    except Exception as e:
        logging.warning('THIS VIDEO HAS NOT GOT THE PAGE INFORMATION.{}'.format(e))
        publish_time, video_like_count = '', ''

        return publish_time, video_like_count


def get_detail2_video(url):
    session = requests.session()
    ua = ['ByteSpider', 'ToutiaoSpider']
    headers = {
        'User-Agent': random.choice(ua),
        # 'referer': url,
        'cookie': '_ga=GA1.2.746553625.1602235482; Hm_lvt_db8ae92f7b33b6596893cdf8c004a1a2=1609605699; Hm_lpvt_db8ae92f7b33b6596893cdf8c004a1a2=1609725915; MONITOR_WEB_ID=11efdde4-2af4-4491-9ce1-9304741f4217; ixigua-a-s=1; BD_REF=1; ttwid=1%7CWKcHle4lJAbLMusAPptWgS1rk4ZlfoZvNrC2vjInOsI%7C1625206683%7C8856360fff3417076e4ae1240dee64409dbdd6a2627b1c9d875f03c262cce0f8',
    }
    response = session.get(url, headers=headers, timeout=5)
    # breakpoint()
    response.encoding = 'utf-8'
    html_content = response.json()
    # html_content = response.replace(u'\xa0', u' ')
    result = html_content["data"]
    return result


def parse_html(user_token, max_time):
    """
    :param user_token:用户的token信息
    :param max_time:翻页的请求参数
    :return:
    """
    # signature = "_02B4Z6wo00f01YCzZ2QAAIBAP5IK3uIhp9GAtmPAAD.ggYkqlDHrBfFkwIYr08.ORgSVJLm1hZap7gRnTt0LpMd2WCL0kKGPh7gD0UvgnnYCn0C1NE6mAHwo3hc9BeCgC92sFy8zRGLXWodn5c"
    # print(type(signature),signature)
    # breakpoint()
    url = 'https://www.toutiao.com/api/pc/feed/?'
    ua = ['ByteSpider', 'ToutiaoSpider']

    headers = {
        # "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36",
        "User-Agent": random.choice(ua),
        "referer": "https://www.toutiao.com/c/user/token/{}/?".format(user_token),
        # "Cookie": "msToken=Cf08sVuW4wdlfuu9Sr6vNQ92FgjxyP1kp9Heo3Iv7eiL9zt2pQmlu-gfSKvPuCqugMHZa05MuxRBwsLtxeEz5yuve_JRkpMBOha6P1DHp75j; __ac_signature=_02B4Z6wo00f01qeCfsAAAIDAH5ABx1o90IanonpAAMuy58; ttcid=3bd20adf38e34b1eb6dfb6f437211b3e11; tt_webid=7081504828254684685; local_city_cache=北京; _tea_utm_cache_24=undefined; csrftoken=92e8b2511024b1d4d9a9b994e7b4c906; s_v_web_id=verify_l1fzsgpt_lYmITAqf_NITF_4PjP_9kHM_SG31dso1y83A; _S_WIN_WH=1920_937; _S_DPR=1; _S_IPAD=0; MONITOR_WEB_ID=cc0fe159-1fb6-4c68-839e-dee789cb79b5; tt_scid=0gFB-A9.sjhMUPBOYWZk02Wx0FCn1NT0Oo2GBhnqG-UAPA5ZtnKMS-thmO0Ozb0R69d0; ttwid=1|CpO4zVfJFy2FJtVI2IzwUoRlsffradHh11rn_TU-yTE|1648989376|596fc5988cccc44cadda5f7e0ea27a302efeb62fd03bfb370b858dbe9c55280c"
    }
    # signature = get_signature()
    data = {
        # '_signature': signature,
        'category': 'profile_all',
        # 'category': 'pc_profile_ugc',
        # 'category': 'pc_profile_video',
        # 'category': 'pc_profile_article',
        'utm_source': 'toutiao',
        'visit_user_token': user_token,
        'max_behot_time': max_time,
    }
    html = requests.get(url, headers=headers, params=data, timeout=3)
    # print(html.text)
    # breakpoint()
    content = html.json()
    result = content['data']  # 已经获取到的api数据列表
    max_time = content['next']['max_behot_time']  # 下一页的时间参数
    has_more = content['has_more']  # 请求的成功标识
    # print(max_time, has_more)
    # breakpoint()
    return result, max_time, has_more


def parse_data(result, database, project_name, project_timezone):
    """
    :param project_timezone: 项目时间段
    :param project_name: 项目名
    :param result:获取到的api数据列表
    :param database: 传入的数据库
    :return:
    """
    # collection_short_text_32 = database[0]
    # collection_short_text_56 = database[1]
    # collection_article = database[2]
    # collection_video = database[3]
    # collection_video_49 = database[4]
    # collection_aq = database[5]

    for item in result:
        data_dict = {}
        cell_num = int(item['cell_type'])  # 获取列表项中的cell_type类型
        # print('456,cell_num:{}'.format(cell_num))
        if cell_num == 56:
            # cell_56(item,data_dict,collection_short_text_56)
            cell_56(item, data_dict, database, project_name, project_timezone)
            # print('else')

        elif cell_num == 32:
            # cell_32(item,data_dict,collection_short_text_32)
            cell_32(item, data_dict, database, project_name, project_timezone)

        elif cell_num == 60:
            # cell_60(item,data_dict,collection_article)
            cell_60(item, data_dict, database, project_name, project_timezone)

        elif cell_num == 0:
            # cell_00(cell_num,item, data_dict, collection_video)
            cell_00(cell_num, item, data_dict, database, project_name, project_timezone)

        elif cell_num == 49:
            # cell_49(cell_num,item, data_dict, collection_video_49)
            cell_49(cell_num, item, data_dict, database, project_name, project_timezone)

        elif cell_num == 202:
            # cell_202(item, data_dict, collection_aq)
            cell_202(item, data_dict, database, project_name, project_timezone)

        else:
            print('THIS PRINT CELL NUM.{}'.format(cell_num))

    logging.warning('This Page Was Finished!')


def get_hot_time(user_token, mongo_database, user_name, project_name, project_timezone):
    # max_time = '1646614800486'
    max_time = '0'
    since_time = time.mktime(time.strptime("2022-01-01 00:00:00", "%Y-%m-%d %H:%M:%S"))
    # until_time = time.mktime(time.strptime("2021-07-30 16:29:26","%Y-%m-%d %H:%M:%S"))

    while True:
        try:
            result = parse_html(user_token, max_time)
            # print(result)
            # breakpoint()
            behot_time = result[1]  # The direct last element will be the next page's hot_time
            has_more = result[2]
            content_data = result[0]
            # print(content_data)
            # breakpoint()
            struct_time = int(str(behot_time)[:10])
            behot_time_struct = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(struct_time))
            # print('123')
            parse_data(content_data, mongo_database, project_name, project_timezone)
            # print('234')
            logging.warning("UID：{}-MAX_BEHOT_TIME：{}-Detail Was Finished! Stamptime:{}"
                  .format(user_name, behot_time_struct, behot_time))
            time.sleep(15)
            # max_time = behot_time
            # if behot_time >= since_time and has_more == True: #choose direction_time for crawl data
            max_behot_time = int(str(behot_time)[:10])
            if max_behot_time > since_time:

                if has_more:
                    max_time = behot_time
                    continue
                else:
                    break
            else:
                break

        except Exception as e:
            logging.warning('The Function -"get_hot_time" -Output the error information：{}'.format(e))
            time.sleep(3)
            break


def get_user_token():
    with open('TouTiaoUserAccountKeXie', encoding='gb18030') as f:
        content = f.readlines()

    return content


def get_signature(token):
    url = "https://www.toutiao.com/toutiao/api/pc/feed/?" \
          "category=pc_profile_article&utm_source=toutiao&visit_user_token={}&max_behot_time=0".format(token)

    with open(r'new_signature.js', encoding='utf-8') as f:
        code = f.read()
    ctx = execjs.compile(code)
    result = ctx.call("signature", token, url)
    # print(result)
    return result


def main():
    project_name = "KeXieResearch"
    project_timezone = "2022-Q1"
    mongo_database = mongo()
    users_token = get_user_token()
    # users_token = ['中国自动化学会#####MS4wLjABAAAA4PCdWH0nz2wO9eWMrzw5mYRZFFlS4HeWRAjAxVX8Gwc']
    # users_token = ['中国绿发会#####MS4wLjABAAAA0NerfsGJ02gvLcLyFbZtPbLdH3_tIjVi3fEEdMqVtxY']
    # users_token = ['光明日报#####MS4wLjABAAAAoU4gUiwhMXddK0UI1XEgPRYH_N1Ce-LdU9wXPmCf3oU']
    # users_token = ['江苏省科协#####MS4wLjABAAAAFv1m3GUe7WdBWko0A37-uRtXzPvujYhP-Eqj3LJpdTw']

    for utoken in users_token:
        utoken = utoken.split('#####')
        user_name = utoken[0].strip()
        user_utoken = utoken[1].strip()
        get_hot_time(user_utoken, mongo_database, user_name, project_name, project_timezone)


def main1():
    get_signature()


if __name__ == '__main__':
    main()
