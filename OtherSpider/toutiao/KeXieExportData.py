# -*- coding:gb18030 -*-

# 可以使用mongodb自带的导出命令
# mongoexport.exe -d papers -c content --type=csv -f "_id","uid","title","abstract","publish_time","like_num","comment_num","url" -o E:\项目爬虫\澎湃新闻\20200225.csv
import time
import pymongo
import pandas as pd

client = pymongo.MongoClient("mongodb://127.0.0.1:27017")
db = client["toutiao_content"]
col = db["user_content_Q41"]
# doc = col.find({"$and": [{"user_name": "中国电子学会"},
#                          {"project_name": "KeXie"}, {"project_timezone": "2022-Q3"}]})
doc = col.find()
file_time = time.strftime("%Y-%m-%d", time.localtime(time.time()))
data = pd.DataFrame(list(doc))

# data.to_csv('KeXie/kexie-toutiao-中国电子学会-Q3-%s.csv' % file_time, encoding='gb18030')
data.to_csv('toutiao-%s.csv' % file_time, encoding='gb18030')

