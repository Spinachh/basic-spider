#-*- coding:utf-8 -*-
# Author：Mongoole
# Date：2020/11/30
# change the coding 'gb18030' because 'utf-8'
# 2020-12-07###add article or weitoutiao or short_text detail_page's  publishtime
# 2020-12-09###merge the video or short_text function
# 2021-01-03###1.加代理，2.加西瓜视频的cookie

import re
import io
import random
import json
import sys
import time
import requests
import pymongo
from fake_useragent import UserAgent
from gne import GeneralNewsExtractor
import execjs

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')

def mongo():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # database
    db = client.KeXietest
    collection_short_text_32 = db.short_text_32
    collection_short_text_56 = db.short_text_56
    collection_article = db.article
    collection_video = db.video
    collection_video_49 = db.video_49
    collection_aq = db.aq

    return (collection_short_text_32,collection_short_text_56,
            collection_article,collection_video,collection_video_49,collection_aq)

#toutiao_video
def cell_00(cell_num,item,data_dict,collection):
    try:
        data_dict['cell_type'] = cell_num
        data_dict['block_type'] = '视频_' + str(cell_num)
        # data_dict['behot_time'] = item['behot_time']
        data_dict['crawl_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        data_dict['content_id'] = item['group_id']
        data_dict['title'] = item['title']
        data_dict['share_url'] = item['display_url']
        data_dict['share_title'] = item['title']
        data_dict['share_desc'] = item['abstract']
        # data_dict['abstract'] = item['abstract']
        data_dict['create_time'] = item['behot_time']
        # data_dict['article_genre'] = item['article_genre']
        data_dict['content'] = None
        data_dict['comments_count'] = item['comments_count']
        data_dict['digg_count'] = item['go_detail_count']
        data_dict['show_count'] = item['detail_play_effective_count']
        data_dict['read_count'] = item['detail_play_effective_count']
        data_dict['image_list'] = item['image_list']
        # data_dict['image_url'] = item['image_url']
        # data_dict['tag_url'] = item['tag_url']
        # data_dict['video_duration_str'] = item['video_duration_str']
        data_dict['tag'] = item['chinese_tag']
        data_dict['user_name'] = item['source']

    except Exception as e:
        print('-Function cell_00 inside Error!')

    try:
        url = 'https://www.ixigua.com/' + data_dict['group_id']
        result = get_detail_video(url)
        # data_dict['real_url'] = url
        # print('视频发布时间：{},'.format(result[0]))
        data_dict['publish_time'] = str(result[0])
        data_dict['digg_count'] = result[1]
    except Exception as e:
        print('THIS CELL_00 FUNCTION ERROR INFORMATION'.format(e))
    collection.insert(data_dict)
    print('{}ThIS {}---CONTENT WAS INSERT MONGODB!{}'.format('-' * 10,'视频——' + str(cell_num),'-' * 10))

#toutiao_video_4
def cell_49(cell_num,item,data_dict,collection):
    try:
        data_dict['cell_type'] = cell_num
        data_dict['block_type'] = '视频_' + str(cell_num)
        # data_dict['behot_time'] = item['behot_time']
        data_dict['crawl_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        data_dict['content_id'] = item['group_id']
        data_dict['title'] = item['title']
        data_dict['share_url'] = item['display_url']
        data_dict['share_title'] = item['title']
        data_dict['share_desc'] = item['abstract']
        # data_dict['abstract'] = item['abstract']
        data_dict['create_time'] = item['behot_time']
        # data_dict['article_genre'] = item['article_genre']
        data_dict['content'] = None
        data_dict['comments_count'] = item['comments_count']
        data_dict['digg_count'] = item['go_detail_count']
        data_dict['show_count'] = item['detail_play_effective_count']
        data_dict['read_count'] = item['detail_play_effective_count']
        data_dict['image_list'] = item['image_list']
        # data_dict['image_url'] = item['image_url']
        # data_dict['tag_url'] = item['tag_url']
        # data_dict['video_duration_str'] = item['video_duration_str']
        data_dict['tag'] = item['chinese_tag']
        data_dict['user_name'] = item['source']

    except Exception as e:
        print('-Function cell_00 inside Error!')

    try:
        url = 'https://www.ixigua.com/' + data_dict['group_id']
        result = get_detail_video(url)
        # data_dict['real_url'] = url
        # print('视频发布时间：{},'.format(result[0]))
        data_dict['publish_time'] = str(result[0])
        data_dict['digg_count'] = result[1]
    except Exception as e:
        print('THIS CELL_49 FUNCTION ERROR INFORMATION'.format(e))

    collection.insert(data_dict)
    print('{}ThIS {}---CONTENT WAS INSERT MONGODB{}'.format('-' * 10, '视频', '-' * 10))

#smal_short_toutiao_first_32
def cell_32(item,data_dict,collection):
    """
    :param item: api数据列表中的一个item
    :param data_dict: 传入的空的字典
    :param collection: 该类型的数据集合
    :return:
    """

    data_dict['cell_type'] = 32
    data_dict['block_type'] = '微头条_32'
    data_dict['crawl_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    data_json = item['concern_talk_cell']['packed_json_str']
    data_json = json.loads(data_json)

    data_dict['content_id'] = data_json['thread_id_str']
    data_dict['title'] = str(data_json['title'])
    data_dict['share_url'] = str(data_json['share_url']).replace('\\','')
    data_dict['share_title'] = str(data_json['share']['share_title']).replace('\n','')
    data_dict['share_desc'] = str(data_json['share']['share_desc'])
    data_dict['create_time'] = str(data_json['create_time'])
    data_dict['publish_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(data_json['publish_time'])))
    data_dict['content'] = str(data_json['content']).replace('\n','')
    data_dict['img_lst'] = data_json['origin_image_list'][0]['url']
    data_dict['show_count'] = data_json['show_count']
    data_dict['comment_count'] = data_json['comment_count']
    data_dict['read_count'] = data_json['read_count']
    data_dict['digg_count'] = data_json['digg_count']
    data_dict['forward_count'] = data_json['forward_info']['forward_count']
    data_dict['tag'] = None
    # data_dict['user_id'] = data_json['user']['user_id']
    data_dict['user_name'] = data_json['user']['name']
    #data_dict['verified_content'] = str(data_json['user']['verified_content'])

    '''
    try:
        # url = 'https://www.toutiao.com/w/a' + str(data_dict['content_id'])    #原来的组装url
        url = data_dict['share_url']    #直接使用分享的url
        result = get_detail_article(url)
        # data_dict['real_url'] = url
        data_dict['publish_time'] = result['publish_time']
        data_dict['content'] = result['content'].replace('\n','')
    except Exception as e:
        print('THIS IS GOT CELL_32 FUNCTION ERROR INFORMAITON:{}'.format(e))
    '''

    collection.insert(data_dict)
    print('{}ThIS DATA{}---CONTENT WAS INSERT MONGODB{}'.format('-' * 10,'微头条_32','-' * 10))

#smal_short_toutiao_second_56
def cell_56(item,data_dict,collection):

    data_dict['cell_type'] = '56'
    data_dict['block_type'] = '微头条_56'
    data_dict['crawl_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    data_json = item['stream_cell']['raw_data']

    # data_dict['account_reason'] = item['stream_cell']['ugc_recommend']['reason']
    # data_dict['create_time'] = item['stream_cell']['behot_time']
    # print(data_json)
    # breakpoint()
    # parse_data = data_json.encode('utf-8').decode('unicode_escape')
    # str_data = parse_data.replace('\\','')
    # data = json.loads(str_data)
    data_json = json.loads(data_json)

    # ###source_data
    '''
    # data_dict['source_group_id'] = data['origin_group']['group_id']
    # data_dict['source_user_id'] = data['origin_group']['user_id']
    # data_dict['source_account'] = data['origin_group']['source']
    # data_dict['source_title'] = data['origin_group']['title']
    # data_dict['source_verified_content'] = data['origin_group']['user_info']['verified_content']
    # data_dict['source_desc'] = data['origin_group']['user_info']['desc']
    # data_dict['source_article_url'] = data['origin_group']['article_url']
    '''

    # ###formal_information
    # data_dict['title_prefix'] = data['title_prefix']
    # data_dict['content_id_str'] = data['id_str']
    data_dict['content_id'] = data_json['comment_base']['id']
    data_dict['title'] = data_json['origin_group']['title']
    data_dict['content'] = str(data_json['comment_base']['content'])
    data_dict['share_title'] = data_json['comment_base']['share']['share_title']
    data_dict['share_url'] = data_json['comment_base']['share']['share_url']
    data_dict['share_desc'] = data_json['comment_base']['share']['share_desc']
    data_dict['create_time'] = data_json['comment_base']['create_time']
    # data_dict['publish_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(data_json['publish_time'])))
    data_dict['content'] = str(data_json['comment_base']['content']).replace('\n','')
    data_dict['img_lst'] = data_json['comment_base']['share']['share_cover']['url_list']
    data_dict['show_count'] = data_json['comment_base']['action']['show_count']
    data_dict['comment_count'] = data_json['comment_base']['action']['comment_count']
    data_dict['read_count'] = data_json['comment_base']['action']['read_count']
    data_dict['digg_count'] = data_json['comment_base']['action']['digg_count']
    data_dict['forward_count'] = data_json['comment_base']['action']['forward_count']
    data_dict['tag'] = None
    # data_dict['user_id'] = data_json['comment_base']['user']['info']['user_id']
    data_dict['user_name'] = data_json['comment_base']['user']['info']['name']


    # data_dict['fans_read_count_new'] = data['comment_base']['action']['fans_read_count_new']
    # data_dict['detail_read_count_merge'] = data['comment_base']['action']['detail_read_count_merge']
    # data_dict['repost_display_count'] = data['comment_base']['action']['repost_display_count']
    # data_dict['fans_read_count_old'] = data['comment_base']['action']['fans_read_count_old']
    # data_dict['bury_count'] = data['comment_base']['action']['bury_count']
    # data_dict['detail_read_count_new'] = data['comment_base']['action']['detail_read_count_new']
    # data_dict['repin_count'] = data['comment_base']['action']['repin_count']
    # data_dict['show_count'] = data['comment_base']['action']['show_count']


    try:
        # url = 'https://www.toutiao.com/w/a' + str(data_dict['content_id'])
        url = data_dict['share_url']
        result,html_content = get_detail_article(url)
        data_dict['publish_time'] = result['publish_time']
    except Exception as e:
        print('THIS CELL_56 FUNCTION ERROR INFORMATION'.format(e))

    collection.insert(data_dict)
    print('{}ThIS DATA{}---CONTENT WAS INSERT MONGODB{}'.format('-' * 10,'微头条_56','-' * 10))

#toutiao_article
def cell_60(item,data_dict,collection):

    data_dict['cell_type'] = 60
    data_dict['block_type'] = '文章_60'
    # data_dict['behot_time'] = item['behot_time']
    data_dict['crawl_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    data_dict['content_id'] = item['item_id']
    data_dict['title'] = item['title']

    data_dict['share_url'] = 'https://www.' + str(item['display_url']).split('//')[1]
    data_dict['share_title'] = item['title']
    data_dict['share_desc'] = item['abstract']
    data_dict['create_time'] = item['behot_time']
    # data_dict['abstract'] = item['abstract']
    # data_dict['article_genre'] = item['article_genre']

    if 'chinese_tag' in item.keys():
        data_dict['chinese_tag'] = item['chinese_tag']
    else:
        data_dict['chinese_tag'] = None

    data_dict['show_count'] = item['go_detail_count']
    data_dict['comments_count'] = item['comments_count']
    data_dict['read_count'] = item['go_detail_count']
    data_dict['image_list'] = item['image_list']
    data_dict['user_name'] = item['source']

    try:
        # url = 'https://www.toutiao.com/i' + str(data_dict['item_id'])
        url = data_dictp['share_url']
        result,html_content = get_detail_article(url)
        data_dict['publish_time'] = result['publish_time']
        data_dict['content'] = result['content'].replace('\n','')
        text = re.findall(r'<span>(.*?)</span>', html_content, re.S | re.M)
        data_dict['forward_count'] = text[3]
        data_dict['digg_count'] = text[0]

    except Exception as e:
        print('THIS CELL_60 FUNCTION ERROR INFORMATION'.format(e))
    collection.insert(data_dict)
    print('{}ThIS DATA{}---CONTENT WAS INSERT MONGODB{}'.format('-' * 10,'文章_60','-' * 10))

#toutiao_wenda
def cell_202(item,data_dict,collection):

    data_dict['cell_type'] = 202
    data_dict['block_type'] = '问答_202'
    data_dict['account_reason'] = item['stream_cell']['ugc_recommend']['reason']
    data_dict['create_time'] = item['stream_cell']['behot_time']
    data_dict['crawl_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

    data_json = item['stream_cell']['raw_data']

    data = json.loads(data_json)
    data_dict['title'] = data['content']['question']['title']

    data_dict['publisth_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(data['content']['answer']['create_time'])))
    data_dict['group_id'] = data['group_id']
    data_dict['question_text'] = data['content']['question']['content']['text']
    data_dict['question_rich_text'] = data['content']['question']['content']['rich_text']
    data_dict['answer_abstract_text'] = data['content']['answer']['abstract_text']
    data_dict['brow_count'] = data['content']['answer']['brow_count']
    data_dict['digg_count'] = data['content']['answer']['digg_count']
    data_dict['comment_count'] = data['content']['answer']['comment_count']
    data_dict['forward_count'] = data['content']['answer']['forward_count']
    data_dict['forward_count'] = data['content']['answer']['forward_count']
    data_dict['user_id'] = data['content']['user']['user_id']
    data_dict['user_name'] = data['content']['user']['uname']
    data_dict['user_verify'] = data['content']['user']['user_intro']

    collection.insert(data_dict)
    print('{}THIS DATA{}---CONTENT WAS INSERT MONGODB{}'.format('-' * 10,'问答','-' * 10))

def get_detail_article(url):

    session = requests.session()
    ua = ['ByteSpider', 'ToutiaoSpider']
    headers = {
        'User-Agent': random.choice(ua),
        # 'referer': 'https://www.toutiao.com/c/user/token/{}/?tab=weitoutiao'.format(user_token),
        'cookie':'__ac_signature=_02B4Z6wo00f01-.WjdQAAIDBV8Ty0oahWWvv9olAAJsJ7e; ttcid=014bf22e69d342c9afd81d9606ef716930; s_v_web_id=verify_ccad3fb16c4c7954895e7d36a5c7c82a; tt_webid=6992481721288263182; csrftoken=074b2fe9a1ffa5792a2aea9cf3b8d627; tt_webid=6992481721288263182; _S_WIN_WH=1920_937; _S_DPR=1; _S_IPAD=0; MONITOR_WEB_ID=b252ab1f-bbd4-454a-baf4-f244b6a8b338; tt_scid=FCICsbOQAm6aTclta55FYHQHC1T..oco5kKvYTI56HmR68yHEviSvhQ6FM9j-i1Pd5c3; ttwid=1%7CYaQOswfugEsJ3edRiSTjaD-VeLBlQw3zDpQ026FT61g%7C1628065803%7C32bd523a267df6b4030e9cda86ff6eb609d01392895c7b9af5d9e282f0814bc9'
    }
    headers = session.headers
    response = session.get(url)
    response.encoding = 'utf-8'
    html_content = response.text
    # html_content = response.replace(u'\xa0', u' ')

    extractor = GeneralNewsExtractor()
    result = extractor.extract(html_content)

    return result,html_content


def get_detail_video(url):

    session = requests.session()
    ua = ['ByteSpider', 'ToutiaoSpider']
    headers = {
        'User-Agent': random.choice(ua),
        # 'referer': url,
        'cookie': '_ga=GA1.2.746553625.1602235482; Hm_lvt_db8ae92f7b33b6596893cdf8c004a1a2=1609605699; Hm_lpvt_db8ae92f7b33b6596893cdf8c004a1a2=1609725915; MONITOR_WEB_ID=11efdde4-2af4-4491-9ce1-9304741f4217; ixigua-a-s=1; BD_REF=1; ttwid=1%7CWKcHle4lJAbLMusAPptWgS1rk4ZlfoZvNrC2vjInOsI%7C1625206683%7C8856360fff3417076e4ae1240dee64409dbdd6a2627b1c9d875f03c262cce0f8',
    }
    response = session.get(url, headers=headers,timeout=5)
    # breakpoint()
    response.encoding = 'utf-8'

    '''
    #gne
    extractor = GeneralNewsExtractor()
    result = extractor.extract(response.text)
    print(result)
    print(result['publish_time'])
    print(result['content'].replace('\n',''))
    '''

    # re
    try:
        publish_time = re.findall(r'"video_publish_time":(.*?),"video_like_count"', response.text, re.S | re.M)[0]
        video_like_count = re.findall(r'"video_like_count":(.*?),"video_abstract"', response.text, re.S | re.M)[0]
        publish_time = publish_time.replace('"','')
        # print(video_like_count)
        publish_time = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(int(publish_time)))
        video_like_count = video_like_count
        print(publish_time,video_like_count)
        return publish_time,video_like_count

    except Exception as e:
        print('THIS VIDEO HAS NOT GOT THE PAGE INFORMATION.{}'.format(e))
        publish_time,video_like_count = '',''

        return publish_time,video_like_count

def parse_html(user_token,max_time):
    """
    :param user_token:用户的token信息
    :param max_time:翻页的请求参数
    :return:
    """
    # signature = "_02B4Z6wo00f01YCzZ2QAAIBAP5IK3uIhp9GAtmPAAD.ggYkqlDHrBfFkwIYr08.ORgSVJLm1hZap7gRnTt0LpMd2WCL0kKGPh7gD0UvgnnYCn0C1NE6mAHwo3hc9BeCgC92sFy8zRGLXWodn5c"
    # print(type(signature),signature)
    # breakpoint()
    url = 'https://www.toutiao.com/api/pc/feed/?'
    ua = ['ByteSpider','ToutiaoSpider']

    headers = {
        # "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36",
        "User-Agent": random.choice(ua),
        "referer": "https://www.toutiao.com/c/user/token/{}/".format(user_token),
        # "Cookie": "csrftoken=2da77e17d8d61c5d167ae330a9017f99; ttcid=393587d9aa03417f8e0ea34e5ae6593b17; s_v_web_id=verify_kcfvsxhe_wJ65ZbcR_8jBQ_4m06_80WV_UpEdZvED4sMb; tt_webid=6912831916657296904; csrftoken=2da77e17d8d61c5d167ae330a9017f99; tt_webid=6912831916657296904; __tasessionId=amdjp05ol1609519133330; passport_csrf_token=c4f9e2122870eb8203a7f1e16e3e9515; passport_csrf_token_default=c4f9e2122870eb8203a7f1e16e3e9515; n_mh=VoW9QuMcH7fz4JKWUYuouwsLSOUlQCGHm-mhZ8SE19w; sso_uid_tt=e773bf5827c5d03ddd878f0d5c3bf1dc; sso_uid_tt_ss=e773bf5827c5d03ddd878f0d5c3bf1dc; toutiao_sso_user=be441eef6cf620f9e4b15959479f930d; toutiao_sso_user_ss=be441eef6cf620f9e4b15959479f930d; sid_guard=42ab350a5d9bdffe8164b459f66385c1%7C1609519857%7C5184000%7CTue%2C+02-Mar-2021+16%3A50%3A57+GMT; uid_tt=5e3d9c8a0a286c695b21fa96a837d785; uid_tt_ss=5e3d9c8a0a286c695b21fa96a837d785; sid_tt=42ab350a5d9bdffe8164b459f66385c1; sessionid=42ab350a5d9bdffe8164b459f66385c1; sessionid_ss=42ab350a5d9bdffe8164b459f66385c1; MONITOR_WEB_ID=7031fd4e-954d-4c03-b6ae-e18251c5c571; tt_anti_token=5Rkjqku0mgnM-324337a65b06f9d9f3fd9a52a063ee1cce0a375d0a73880635394421b0538d8b; tt_scid=N7Gtu3MpJEU0fHazO85BmnlDKPRIpZ9f9URX5fdCKhZAC8.0qrTRZYg9Ed1RSATj031a",
    }
    # signature = get_signature()
    data = {
        # '_signature': signature,
        # 'category': 'profile_all',
        'category': 'pc_profile_ugc',
        # 'category': 'pc_profile_video',
        # 'category': 'pc_profile_article',
        'utm_source': 'toutiao',
        'visit_user_token': user_token,
        'max_behot_time': max_time,
    }
    html = requests.get(url,headers=headers,params=data,timeout=3)
    # print(html.text)
    # breakpoint()
    content = html.json()
    result = content['data']    #已经获取到的api数据列表
    max_time = content['next']['max_behot_time']    #下一页的时间参数
    has_more = content['has_more']  #请求的成功标识
    print(max_time,has_more)

    return (result,max_time,has_more)


def parse_data(result,database):
    """
    :param result:获取到的api数据列表
    :param database: 传入的数据库
    :return:
    """
    collection_short_text_32 = database[0]
    collection_short_text_56 = database[1]
    collection_article = database[2]
    collection_video = database[3]
    collection_video_49 = database[4]
    collection_aq = database[5]

    for item in result:
        data_dict = {}
        cell_num = int(item['cell_type'])    #获取列表项中的cell_type类型

        if cell_num == 56:
            cell_56(item,data_dict,collection_short_text_56)
            # print('else')

        elif cell_num == 32:
            cell_32(item,data_dict,collection_short_text_32)

        elif cell_num == 60:
            cell_60(item,data_dict,collection_article)

        elif cell_num == 0:
            cell_00(cell_num,item, data_dict, collection_video)

        elif cell_num == 49:
            cell_49(cell_num,item, data_dict, collection_video_49)

        elif cell_num == 202:
            cell_202(item, data_dict, collection_aq)

        else:
            print('THIS PRINT CELL NUM.{}'.format(cell_num))

    print('{} This Page Was Finished! {}'.format('-'*10,'-'*10))

def get_hot_time(user_token,mongo_database,user_name):
    max_time = 0
    since_time = time.mktime(time.strptime("2021-07-21 00:00:00","%Y-%m-%d %H:%M:%S"))
    until_time = time.mktime(time.strptime("2020-11-08 16:29:26","%Y-%m-%d %H:%M:%S"))

    while True:
        try:
            result = parse_html(user_token,max_time)
            # print(result)
            # breakpoint()
            behot_time = result[1]  #The direct last element will be the next page's hot_time
            has_more = result[2]
            content_data = result[0]
            # print(content_data)
            # breakpoint()
            parse_data(content_data, mongo_database)
            print("UID：{}---MAX_BEHOT_TIME：{}---Detail Coneten Was Finished!".format(user_name,behot_time))
            time.sleep(15)
            # max_time = behot_time
            # if behot_time >= since_time and has_more == True: #choose direction_time for crawl data
            max_behot_time = int(str(behot_time)[:10])
            if max_behot_time > since_time:

                if has_more == True:
                    max_time = behot_time
                    continue
                else:
                    break
            else:
                break


        except Exception as e:
            print('The Function -"get_hot_time" -Output the error information：{}'.format(e))
            time.sleep(3)
            break

def get_user_token():
    with open('TouTiaoUserAccount',encoding='gb18030') as f:
        content = f.readlines()

    return content

def get_signature(token):
    url = "https://www.toutiao.com/toutiao/api/pc/feed/?" \
          "category=pc_profile_article&utm_source=toutiao&visit_user_token={}&max_behot_time=0".format(token)


    with open(r'new_signature.js',encoding='utf-8') as f:
        code = f.read()
    ctx = execjs.compile(code)
    result = ctx.call("signature",token,url)
    # print(result)
    return result

def main():
    mongo_database = mongo()
    # users_token = get_user_token()
    # users_token = ['中国自动化学会#####MS4wLjABAAAA4PCdWH0nz2wO9eWMrzw5mYRZFFlS4HeWRAjAxVX8Gwc']
    users_token = ['中国绿发会#####MS4wLjABAAAA0NerfsGJ02gvLcLyFbZtPbLdH3_tIjVi3fEEdMqVtxY']
    # users_token = ['光明日报#####MS4wLjABAAAAoU4gUiwhMXddK0UI1XEgPRYH_N1Ce-LdU9wXPmCf3oU']
    # users_token = ['江苏省科协#####MS4wLjABAAAAFv1m3GUe7WdBWko0A37-uRtXzPvujYhP-Eqj3LJpdTw']

    for utoken in users_token:
        utoken = utoken.split('#####')
        user_name = utoken[0].strip()
        user_utoken = utoken[1].strip()
        get_hot_time(user_utoken,mongo_database,user_name)

def main1():
    get_signature()

if __name__ == '__main__':
    main()