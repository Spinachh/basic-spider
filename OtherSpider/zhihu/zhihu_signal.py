# -*- coding:utf-8 -*-

import time
import re
import requests
from fake_useragent import UserAgent
import pymongo

def mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1',port=27017)
    #选择数据库
    db = client.OtherSPider
    #选择操作的集合
    collection = db.zhihu_question

    return collection

def get_data(url,p):

    ua = UserAgent().random
    headers = {
        'User-Agent': ua,
        'Cookie': '_zap=64a19e3c-c4ed-4d4c-9950-d4f33eb1c614; d_c0="ALBUZiW2ChGPTjcuOjFCf3wc9GHH5kenKcc=|1585556613"; _ga=GA1.2.1017750751.1585556618; _xsrf=d1d675aa-ec3e-4a16-a269-1f89510f9fcc; Hm_lvt_98beee57fd2ef70ccdd5ca52b9740c49=1609640126; capsion_ticket="2|1:0|10:1609640127|14:capsion_ticket|44:NzBkNjBkNTlhM2FjNGY4NTkyNTY2YzczYjQ5NjYwM2E=|305a5cea53ccae9b93fd32e52caf1b67170d9eb6fbd3790d80a37d93bce40534"; z_c0="2|1:0|10:1609640254|4:z_c0|92:Mi4xMEVyZUhnQUFBQUFBc0ZSbUpiWUtFU1lBQUFCZ0FsVk5QbmZlWUFBekZxZkh1a0htVnFZRjZEU3hKSHdYS2pWTDVB|f4cc007defe8f093a7b3fc709401fe930fa8cca9a82cff72fe4ba183f5c7c690"; tst=r; q_c1=63f18584c26d4725af38180dfeec7139|1609641671000|1609641671000; Hm_lpvt_98beee57fd2ef70ccdd5ca52b9740c49=1609661119; KLBRSID=e42bab774ac0012482937540873c03cf|1609661426|1609661426',
    }
    data = requests.get(url,headers=headers)
    content = data.json()
    # print(content)
    data = {}

    data['title'] = content['title']
    create_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(content['created']))
    data["create_time"] = create_time
    # updated_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(content['updated']))
    # data['updated'] = updated_time
    if 'img_url' in content.keys():
        data['img_url'] = content['image_url']
    else:
        data['img_url'] = ''

    if 'author' in content.keys():
        data['name'] = content['author']['name']
    else:
        data['name'] = ''

    if 'content' in content.keys():
        answer_content = content['content']
        dr = re.compile(r'<[^>]+>', re.S)
        article_content = dr.sub('', answer_content).strip()
        data['content'] = article_content
    else:
        data['content'] = ''

    if 'voteup_count' in content.keys():
        data['voteup_count'] = content['voteup_count']
    else:
        data['voteup_count'] = ''

    if 'comment_count' in content.keys():
        data['comment_count'] = content['comment_count']
    else:
        data['comment_count'] = ''

    article_url = url.split('/')[-1]
    data['article_url'] = 'https://zhuanlan.zhihu.com/p/{}'.format(article_url)

    p.insert(data)
    print("----------Insert into MongoDB Sucessfully----------")


def read_file():

    with open(r'singal_url','r',encoding='utf-8') as f:
        urls = f.readlines()

    return urls

def main():
    urls = read_file()
    p = mongo()
    # url = 'https://api.zhihu.com/articles/148434294'
    for url in urls:
        url = url.strip()
        # print(url)
        # breakpoint()
        get_data(url,p)
        time.sleep(3)

if __name__ == '__main__':
    main()