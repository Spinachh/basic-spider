# -*- coding:utf-8 -*-

import sys
import io
import time
import re
import requests
from fake_useragent import UserAgent
import pymongo
import chardet

sys.stdout = io.TextIOWrapper(sys.stdout.buffer,encoding='gb18030')

def mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1',port=27017)
    #选择数据库
    db = client.OtherSPider
    #选择操作的集合
    collection = db.zhihu_question22

    return collection

def get_response(url):

    ua = UserAgent().random
    headers = {
        'User-Agent': ua,
        'referer': 'https://www.zhihu.com/search?q=%E7%96%AB%E6%83%85%E4%B8%8B2020%E5%BA%94%E5%B1%8A%E5%A4%A7%E5%AD%A6%E7%94%9F%E5%B0%B1%E4%B8%9A&type=content',
        'Cookie':'_zap=64a19e3c-c4ed-4d4c-9950-d4f33eb1c614; d_c0="ALBUZiW2ChGPTjcuOjFCf3wc9GHH5kenKcc=|1585556613"; _ga=GA1.2.1017750751.1585556618; _xsrf=d1d675aa-ec3e-4a16-a269-1f89510f9fcc; Hm_lvt_98beee57fd2ef70ccdd5ca52b9740c49=1609640126; capsion_ticket="2|1:0|10:1609640127|14:capsion_ticket|44:NzBkNjBkNTlhM2FjNGY4NTkyNTY2YzczYjQ5NjYwM2E=|305a5cea53ccae9b93fd32e52caf1b67170d9eb6fbd3790d80a37d93bce40534"; z_c0="2|1:0|10:1609640254|4:z_c0|92:Mi4xMEVyZUhnQUFBQUFBc0ZSbUpiWUtFU1lBQUFCZ0FsVk5QbmZlWUFBekZxZkh1a0htVnFZRjZEU3hKSHdYS2pWTDVB|f4cc007defe8f093a7b3fc709401fe930fa8cca9a82cff72fe4ba183f5c7c690"; tst=r; q_c1=63f18584c26d4725af38180dfeec7139|1609641671000|1609641671000; anc_cap_id=62fd5504c6fa4ea78316cf94322940af; SESSIONID=n7dPEMurkXi0xTezjHMLNpYDndZR0lmJuPUha91JOAS; JOID=VF4WAUL9LTte9GnCbPniJk5EWkp4kx9eBb4ksh-ZdVdvhzKnExsBwgX7bMZmQNOEst5Er5FC9ZeioE_g68ycGKg=; osd=VloQAE7_KT1f-GvGavjuJEpCW0Z6lxlfCbwgtB6Vd1Nphj6lFx0Azgf_asdqQteCs9JGq5dD-ZWmpk7s6ciaGaQ=; Hm_lpvt_98beee57fd2ef70ccdd5ca52b9740c49=1609771402; KLBRSID=fe0fceb358d671fa6cc33898c8c48b48|1609771428|1609767972',
        }
    data = requests.get(url,headers=headers)
    # print(chardet.detect(data.content))
    content = data.json()
    # print(content)
    next_url = content['paging']['next']

    return content,next_url

def get_data(content,p,id):
    if len(content['data']) > 0:
        content = content['data']
        flag = 0
        for item in content:

            data = {}

            data['title'] = item['question']['title']
            create_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(item['created_time']))
            data["create_time"] = create_time
            # updated_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(content['updated']))
            # data['updated'] = updated_time
            if 'img_url' in item['question'].keys():
                data['img_url'] = item['question']['image_url']
            else:
                data['img_url'] = ''

            if 'name' in item['author'].keys():
                data['name'] = item['author']['name']
            else:
                data['name'] = ''

            if 'content' in item.keys():
                answer_content = item['content']
                dr = re.compile(r'<[^>]+>', re.S)
                article_content = dr.sub('', answer_content).strip()
                data['content'] = article_content
            else:
                data['content'] = ''

            if 'voteup_count' in item.keys():
                data['voteup_count'] = item['voteup_count']
            else:
                data['voteup_count'] = ''

            if 'comment_count' in item.keys():
                data['comment_count'] = item['comment_count']
            else:
                data['comment_count'] = ''

            data['article_url'] = 'https://www.zhihu.com/question/{}'.format(id)
            p.insert(data)
            print("----------{} Insert into MongoDB Sucessfully----------".format(id))

        return flag
    else:
        flag = 1
        return flag


def read_file():

    with open(r'question_id','r',encoding='utf-8') as f:
        ids = f.readlines()

    return ids

def main():
    ids = read_file()
    p = mongo()
    for id in ids:
        id = id.strip()
        url = 'https://www.zhihu.com/api/v4/questions/{}/answers?' \
              'include=data%5B%2A%5D.is_normal%2Cadmin_closed_comment%2Creward_info%2Cis_collapsed%2Cannotation_action%2Cannotation_detail%2Ccollapse_reason%2Cis_sticky%2Ccollapsed_by%2Csuggest_edit%2Ccomment_count%2Ccan_comment%2Ccontent%2Ceditable_content%2Cattachment%2Cvoteup_count%2Creshipment_settings%2Ccomment_permission%2Ccreated_time%2Cupdated_time%2Creview_info%2Crelevant_info%2Cquestion%2Cexcerpt%2Cis_labeled%2Cpaid_info%2Cpaid_info_content%2Crelationship.is_authorized%2Cis_author%2Cvoting%2Cis_thanked%2Cis_nothelp%2Cis_recognized%3Bdata%5B%2A%5D.mark_infos%5B%2A%5D.url%3Bdata%5B%2A%5D.author.follower_count%2Cbadge%5B%2A%5D.topics%3Bdata%5B%2A%5D.settings.table_of_content.enabled' \
              '&limit=5&offset=0&platform=desktop&sort_by=default'.format(id)
        # print(url)
        # breakpoint()
        while True:
            content,next_url = get_response(url)
            flag = get_data(content,p,id)
            if flag == 1:
                break
            else:
                url = next_url
                continue
        time.sleep(3)
        break

if __name__ == '__main__':
    main()