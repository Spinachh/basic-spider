#!/usr/bin/python3
#-*- coding:utf-8 -*-
# Author : Mongoole
# Date : 2021/11/16

import io
import sys
import time
import re
import cchardet
import hashlib
import requests
import pymongo
import json
import logging
from fake_useragent import UserAgent
sys.stdout = io.TextIOWrapper(sys.stdout.buffer,encoding='gb18030')         #改变标准输出的默认编码
# urllib3.disable_warnings()
# ssl._create_default_https_context = ssl._create_unverified_context

class ZhiHu(object):

    since_time = "2020-01-01 00:00:00"
    ProjectName = ""

    def __init__(self):
        pass
    def mongo(self):
        #连接数据库mongodb
        client = pymongo.MongoClient(host='127.0.0.1', port=27017)
        #选择数据库
        db = client.zhihu
        #选择操作的集合
        collection = db.user_content

        return collection

    def get_user(self):
        with open("user_lst_normal",encoding="gb18030") as f:
            users = f.readlines()
        return users

    def get_response(self,url):
        ua = UserAgent().random
        headers = {
            "User-Agent": ua,
            # "Cookie": '_zap=a1f06069-97e0-4049-b570-41b4499a0c43; _xsrf=c2c588fe-1476-48b8-84fc-596717bb73bc; d_c0="AHDQA5llCBSPTm5tH-p2GGyKJxV7sOsqsFc=|1636940887"; captcha_session_v2="2|1:0|10:1636940893|18:captcha_session_v2|88:Q3BwNjk5Y0xIMXVlR085UTBwdUNiQi9Ld2hXZ3BSMS91Z3dFd1lyWU9FNGVBbFJtSDdraXNRZ0lBVTdXUWtqUw==|cbe2dd09f8faee7de666298fb01802e9db6b9f6ac0a73e24fe75ef896464b96f"; captcha_ticket_v2="2|1:0|10:1636941065|17:captcha_ticket_v2|704:eyJ2YWxpZGF0ZSI6IkNOMzFfVzh0bXRidExsSnNVSG14UmRZTVhxSFh3YkpwaVp2NWN1SzVHY2IuOHdCWUpOa1o3VVJZQ1JmQUY0ekNhS2lDaTY4Vy5QTU42R2thWi1HMUxGS2N4MWtBcm1jZnIxX3V3S095dC5xcVV3MV9aLm1RaURaOUZWMjB3ZVE4WTV2eTlQb2xFYzEtV3RpNmQ0Z2FEaGhQYXNkZHlmdFFKRlg5bG1kTTlZd2ZMWi5rZUg1N29pWFpRelo1Z2pXNXVDdlk2bnBFTTQxaGdsd1dmeWZwUFlzQjBVQWJVdUNDOUt6QmFvb3N1QkhmNEEuNHZSZnFZbUNoYkhiTEd2Nndveng0eDBUWDk0WlZ0d25ZN0ZCc0lLcnpoSzRfaUZJV1NEVFJLbS1FRXcxdkdhLnZOTDVob3dUaTlPVFVPNDlLbi1qMnJHYWp4a3pnNlRkVE5WazlsWllCdVFNVVZMd0NUcmd2TlJVRW5QcmJtOTg3c3AtdUFleXJscGFpTFN0clNKXzdTbGxQa0dfYlRHamFfem1udlZWeHlmNkNUQnJEQ2VMZGxrcmhrdFFNRU92aS1aY20tLkRraEk0S243OS1wZk5YZXg1VkowT3lLMEdsbnBVV19hSXlqRVFuMXltcDlHZmpMWEZKaDBjX3VkdEJucng3YWtDUmphVHNMUW5hMyJ9|d40ab993acb60529a7032fbcdbeefa2baab47c55c95e8e3d36f2d6fb045f1953"; z_c0="2|1:0|10:1636941082|4:z_c0|92:Mi4xS3p2d01RQUFBQUFBY05BRG1XVUlGQ1lBQUFCZ0FsVk5HZ3RfWWdEWHU4anZGeFhHNUpFenRGVFRJWGpXQ3BqbmtR|064c8b7956e41eba9c8623ee39da13d898840ac928654637991e31b06cf9c467"; tst=r; SESSIONID=CcWgBeR7wLG49gcQ731p8Gw9al3uB29MVHoMKokHETS; JOID=Vl4XBULMLo-4tkFLQM-x12wiJkxSjUnc0PkmHXO2YMyIgCsqFeiTFdu5R0tLBvBS-h_GIAVbXq8gEePJQuEJHWM=; osd=UloVAE7IKo29ukVPQsq902ggI0BWiUvZ3P0iH3a6ZMiKhScuEeqWGd-9RU5HAvRQ_xPCJAdeUqskE-bFRuULGG8=; Hm_lvt_98beee57fd2ef70ccdd5ca52b9740c49=1637027644,1637027886,1637030020,1637030028; NOT_UNREGISTER_WAITING=1; Hm_lpvt_98beee57fd2ef70ccdd5ca52b9740c49=1637044619; KLBRSID=81978cf28cf03c58e07f705c156aa833|1637044619|1637042252',
        }
        #此url为测试json数据中有多余引号的url
        # url = "https://www.zhihu.com/api/v3/moments/jing-ji-ri-bao-xin-wen-ke-hu-duan/activities?limit=7&session_id=1443515388440637440&after_id=1637108218&desktop=true"
        try:
            response = requests.get(url,headers=headers)
            if response.status_code == 200:
                html_chardet = cchardet.detect(response.content)
                response.encoding = html_chardet["encoding"]
                response = response.text.replace('\\\"', '#')       #对json格式中的多余的引号去除
                # html_content = response.text.encode("utf-8").decode("unicode_escape")
                html_content = response.encode("utf-8").decode("unicode_escape")
                return html_content

        except Exception as e:
            print(e)

    def get_data(self,response,mongodb,user_name):

        try:
            response = json.loads(response, strict=False)
            data = response["data"]
            next_page = response["paging"]["next"]
            is_end = response["paging"]["is_end"]
            after_id = re.findall(r"after_id=(.*?)&desktop",next_page)[0]
            for item in data:
                # item_cn_type = item["action_text"]
                item_en_type = item["verb"]
                if item_en_type == "ANSWER_CREATE":
                    self.get_answer(item,mongodb,user_name)

                elif item_en_type == "MEMBER_CREATE_ARTICLE":
                    self.get_article(item,mongodb,user_name)

                elif item_en_type == "MEMBER_CREATE_PIN":
                    self.get_create_pin(item,mongodb,user_name)
            return is_end,next_page,after_id

        except:
            is_end = True
            next_page = ""
            after_id = ""
            return is_end,next_page,after_id

    #问答
    def get_answer(self,item,mongodb,user_name):

        item_dict = {}
        item_dict["ProjectName"] = self.ProjectName
        item_dict["account_name"] = user_name
        item_dict["cn_type"] = item["action_text"]
        item_dict["en_type"] = item["verb"]

        created_time = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(int(item["created_time"])))
        item_dict["created_time"] = created_time

        updated_time = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(int(item["target"]["updated_time"])))
        item_dict["updated_time"] = updated_time
        item_dict["excerpt"] = item["target"]["excerpt"]
        item_dict["excerpt_new"] = item["target"]["excerpt_new"]
        item_dict["id"] = item["target"]["id"]
        item_dict["url"] = item["target"]["url"]

        #Uniquely identifies
        item_dict["hashvalue"] = hashlib.md5((item["target"]["url"]).encode("ascii")).hexdigest()

        content = item["target"]["content"]
        dr = re.compile(r'<[^>]+>', re.S)
        content_text = dr.sub('', content).strip()
        item_dict["content"] = content_text

        item_dict["comment_count"] = item["target"]["comment_count"]
        item_dict["voteup_count"] = item["target"]["voteup_count"]
        item_dict["thanks_count"] = item["target"]["thanks_count"]
        item_dict["type"] = item["target"]["type"]


        #question
        question_item = item["target"]["question"]
        item_dict["title"] = question_item["title"]
        item_dict["question_url"] = question_item["url"]

        question_created_time = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(int(question_item["created"])))
        item_dict["question_created_time"] = question_created_time
        item_dict["question_excerpt"] = question_item["excerpt"]
        item_dict["question_answer_count"] = question_item["answer_count"]
        item_dict["question_comment_count"] = question_item["comment_count"]
        item_dict["question_follower_count"] = question_item["follower_count"]

        mongodb.insert(item_dict)

    #文章
    def get_article(self,item,mongodb,user_name):

        item_dict = {}
        item_dict["ProjectName"] = self.ProjectName
        item_dict["account_name"] = user_name
        item_dict["cn_type"] = item["action_text"]
        item_dict["en_type"] = item["verb"]

        created_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(item["created_time"])))
        item_dict["created_time"] = created_time

        updated_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(item["target"]["updated"])))
        item_dict["updated_time"] = updated_time
        item_dict["excerpt"] = item["target"]["excerpt"]
        item_dict["excerpt_new"] = item["target"]["excerpt_new"]
        item_dict["id"] = item["target"]["id"]
        item_dict["url"] = item["target"]["url"]

        item_dict["hashvalue"] = hashlib.md5((item["target"]["url"]).encode("ascii")).hexdigest()


        content = item["target"]["content"]
        dr = re.compile(r'<[^>]+>', re.S)
        content_text = dr.sub('', content).strip()
        item_dict["content"] = content_text

        item_dict["comment_count"] = item["target"]["comment_count"]
        item_dict["voteup_count"] = item["target"]["voteup_count"]

        if "thanks_count" in item["target"].keys():
            item_dict["thanks_count"] = item["target"]["thanks_count"]
        else:
            item_dict["type"] = ""

        # question
        item_dict["title"] = item["target"]["title"]
        item_dict["question_url"] = ""
        item_dict["question_created_time"] = ""
        item_dict["question_excerpt"] = ""
        item_dict["question_answer_count"] = ""
        item_dict["question_comment_count"] = ""
        item_dict["question_follower_count"] = ""

        mongodb.insert(item_dict)

    #想法
    def get_create_pin(self,item,mongodb,user_name):

        item_dict = {}
        item_dict["ProjectName"] = self.ProjectName
        item_dict["account_name"] = user_name
        item_dict["cn_type"] = item["action_text"]
        item_dict["en_type"] = item["verb"]

        created_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(item["created_time"])))
        item_dict["created_time"] = created_time

        updated_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(item["target"]["updated"])))
        item_dict["updated_time"] = updated_time
        item_dict["excerpt"] = ""
        item_dict["excerpt_new"] = item["target"]["excerpt_new"]
        item_dict["id"] = item["target"]["id"]
        item_dict["url"] = item["target"]["url"]

        item_dict["hashvalue"] = hashlib.md5((item["target"]["url"]).encode("ascii")).hexdigest()

        content = item["target"]["preview_text"]
        dr = re.compile(r'<[^>]+>', re.S)
        content_text = dr.sub('', content).strip()
        item_dict["content"] = content_text

        item_dict["comment_count"] = item["target"]["comment_count"]

        if "voteup_count" in item["target"].keys():
            item_dict["voteup_count"] = item["target"]["voteup_count"]
        else:
            item_dict["voteup_count"] = ""

        item_dict["thanks_count"] = item["target"]["reaction_count"]

        # question
        item_dict["title"] = item["target"]["excerpt_title"]
        item_dict["question_url"] = ""
        item_dict["question_created_time"] = ""
        item_dict["question_excerpt"] = ""
        item_dict["question_answer_count"] = ""
        item_dict["question_comment_count"] = ""
        item_dict["question_follower_count"] = ""

        mongodb.insert(item_dict)

    def cron_user(self,mongodb,user_name,user_id):

        # since_time = time.mktime(time.strptime("2020-01-01", "%Y-%m-%d"))
        # until_time = time.mktime(time.strptime("2021-07-30 16:29:26","%Y-%m-%d %H:%M:%S"))

        after_id = 0
        page_num = 0

        url = "https://www.zhihu.com/api/v3/moments/{}/activities?limit=7" \
              "&session_id=1443515388440637440&after_id={}&desktop=true".format(user_id, after_id)

        while True:

            try:
                html_content = self.get_response(url)
                result = self.get_data(html_content,mongodb,user_name)
                is_end = result[0]
                next_page = result[1]
                next_id = result[2]
                # after_id = next_id
                url = next_page
                # next_time = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(int(next_id[:10])))
                since_time = time.mktime(time.strptime(self.since_time,'%Y-%m-%d %H:%M:%S'))
                if url == "":
                    break
                if int(next_id) < int(since_time):
                    break
                page_num += 1
                self.logger_init().info("{} Page {} was finished!stamptime is {}".format(user_name,page_num,next_id))
                time.sleep(4)
                # if is_end == True:
                #     break
            except Exception as e:
                print(e)


    def logger_init(cls):

        # 日志输出格式
        logging.basicConfig(format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S')

        # 打印日志级别
        # logging.getLogger().setLevel(logging.DEBUG)
        logging.getLogger().setLevel(logging.INFO)
        # logging.getLogger().setLevel(logging.CRITICAL)
        logger = logging.getLogger()

        return logger

    def run(self):

        mongodb = self.mongo()
        users = self.get_user()
        # users = ["经济日报新闻客户端#####https://www.zhihu.com/org/jing-ji-ri-bao-xin-wen-ke-hu-duan"]
        for user in users:
            uu = user.split("#####")
            user_name = uu[0].strip()
            user_id = uu[1].strip().split('/')[-1]
            # print(user_id.split('/')[-1])
            # breakpoint()
            self.cron_user(mongodb,user_name,user_id)



if __name__ == '__main__':
    ZhihuSpider = ZhiHu()
    ZhihuSpider.since_time = "2020-01-01 00:00:00"
    ZhihuSpider.ProjectName = "MainMediaResearch"
    ZhihuSpider.run()
    ZhihuSpider.logger_init().info("Spider is crawling!")