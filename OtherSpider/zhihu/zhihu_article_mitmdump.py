#!/usr/bin/python3
#-*- coding:utf-8 -*-
# Author : Mongoole
# Date : 2021/1/19 
import os
import json
import sys
import time
import re
import requests
import pymongo
from fake_useragent import UserAgent


def mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    #选择数据库
    db = client.zhihu
    #选择操作的集合
    p = db.zhihu_fourth_articles
    return p

collection = mongo()


def response(flow):
    since_time = '2020-09-30 00:00:00'
    
    if '/api/v4/members' in flow.request.url:
        print('{} We got The Data {}'.format('-'*20,'-'*20))
        text = flow.response.text
        content = json.loads(text)
        # print(content)
        results = content['data']
        for item in results:
            data = {}
            data['name'] = item['author']['name']
            data['title'] = item['title']
            data['excerpt'] = item['excerpt']
            create_time = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(item['updated']))
            if create_time > since_time:
                data['published_time'] = create_time
                data['comment_count'] = item['comment_count']
                data['voteup_count'] = item['voteup_count']
                data['url'] = item['url']
                data['voting'] = item['voting']
    
                collection.insert(data)
                print('{} Insert into MongoDB Sucessfully {}'.format('-'*20,'-'*20))
                
            else:
                print('{} Already Got DeadLine {}'.format('-'*20,'-'*20))
                

