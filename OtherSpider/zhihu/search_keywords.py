# -*- coding:gbk -*-
# Author: Mongoole
# Date: 2021-01-03

import json
import time
import re
import pymongo

def mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1',port=27017)
    #选择数据库
    db = client.OtherSPider
    #选择操作的集合
    collection_content = db.zhihu_search_2021_01_03
    return collection_content


collection = mongo()
#必须这样写！因为是要获取响应的内容，mitmdump规定必须这样写才能获取到数据,所以必须写response

def response(flow):

    #获取视频信息

    if "api/v4/search_v3?" in flow.request.url:
        print("----------We got The Data----------")
        text = flow.response.text
        content = json.loads(text)
        # print(content)
        results = content["data"]

        for item in results:
            data = {}
            type = item["type"]
            if type == "search_result":
                content = item["object"]
                if "question" in item["object"].keys():
                    id = content['question']['id']
                    question_url = "https://www.zhihu.com/question/{}".format(id)
                    data['question_url'] =question_url
                    title = content['question']['name'].replace('<em>','').replace('</em>','')
                    data['title'] = title

                else:
                    data['question_url'] = ''

                    title = content['title'].replace('<em>','').replace('</em>','')
                    data["title"] = title

                updated_time = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(content["updated_time"]))
                data["updated_time"] = updated_time
                data["share_url"] = content['url']

                collection.insert(data)
                print("----------Insert into MongoDB Sucessfully----------")
