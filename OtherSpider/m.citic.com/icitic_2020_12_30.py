#!/usr/bin/python3
#-*- coding:utf-8 -*-
# Author : Mongoole
# Date : 2020/12/29

import re
import time
import json
import requests
import pymongo
from progressbar import *
from fake_useragent import UserAgent

def mongo():
    '''
    MongoDB数据库
    :return:
    '''
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1',port=27017)
    #选择数据库
    db = client.OtherSPider
    #选择操作的集合
    p = db.article_2020_12_30

    return p

def get_response(url,headers):
    '''
    获取每页数据列表函数
    :param url: page url
    :param headers:
    :return:
    '''
    response = requests.get(url,headers=headers)
    # print(response.text)
    content = json.loads(response.text)     #字符串转换为json格式
    # print(type(response))
    return content

def get_data(content,headers,pm):
    '''
    处理获取到的每页数据的函数
    :param content: 每一页内容的数据字典
    :param headers:
    :param pm:
    :return:
    '''
    # page_head = content['responseHead']
    # print(content)
    try:    #加异常判断
        page_data = content['responseBody']['data']
        for item in page_data:
            data_dict = {}
            #title
            data_dict['title'] = item['title']

            #tag
            try:
                tag1 = [str(item['name'])]
            except:
                tag1 = ['空']

            try:
                #列表推导式，获取列表中多个字典元素的TagName值
                tag2 = [tag['secondTagName'] for tag in eval(item['tagContent'])]
            except:
                tag2 = ['空']
            data_dict['tag'] = ''.join(tag1 + tag2)

            #author
            data_dict['author'] = item['sources']

            #createDate
            data_dict['createDate'] = item['createDate']

            #sourcesDate
            data_dict['sourcesDate'] = item['sourcesDate']

            #commentCount
            data_dict['commentCount'] = item['contentCount']

            #storeCount
            data_dict['storeCount'] = item['storeCount']

            #likeCount
            data_dict['likeCount'] = item['likeCount']

            #articleId
            articleId = item['id']
            data_dict['articleId'] = articleId      #获取到文章的ID

            #拼接文章详情页面的访问链接
            page_url = 'https://m.citic.com/ixt-api/v1.0.0/commonArticle/getArticle/{}'.format(articleId)

            viewCount,articleText = get_page_content(page_url,headers=headers)
            # print(get_page_content(page_url,headers=headers))
            # breakpoint()

            data_dict['viewCount'] = viewCount
            data_dict['articleText'] = articleText

            pm.insert(data_dict)    #插入mongodb数据库
            # print('{} INSERT DATA INTO MONGODB SUCCESSFUL!{}'.format('-'*20,'-'*20))
            time.sleep(0.1)

    except Exception as e:

        print('THIS WAS GET DATA FUNCTION ERROR: {}'.format(e))


def get_page_content(url,headers):
    '''
    获取文章详情页面的函数
    :param url: 文章详情页url
    :param headers:
    :return:
    '''
    article_response = requests.get(url,headers=headers)
    article_response = json.loads(article_response.text)
    try:
        article_data = article_response['responseBody']
        article_msg = article_data['data']['msg']
        article_content = article_data['data']['data']
        if article_msg == '获取成功':
            article_detail = article_content['article']
            #viewCount
            viewCount = article_detail['hits']
            #articleText
            articlehtml = article_detail['articleData']['content']

            result = re.findall(r'<p>(.*?)</p>',articlehtml)    #正则匹配文本内容
            #使用列表推导式简单清洗数据
            articleText = [text.replace('&nbsp;','').replace('<br/>','') for text in result]
            articleText = ''.join(articleText)  #文本内容拼接

            return viewCount,articleText

    except Exception as e:
        print('THIS WAS GET PAGE DATA FUNCTION ERROR: {}'.format(e))


def main(): #主函数
    pm = mongo()
    # url = 'https://m.citic.com/ixt-api/v1.0.0/commonArticle/findByCategoryId/7324d3908db340aab640c66997ea0b5d/1/10'
    token = '57c822862344669aef2634fa9b8584ff'  #用户token值
    ua = UserAgent().random         #随机用户浏览器头
    headers = {
        'User-Agent': ua,
        # 'Cookie':,
        'token': token,
    }                 #划重点：就是访问头需要携带用户token

    progress = ProgressBar()    #进度条

    for page in progress(range(200)):
        url = 'https://m.citic.com/ixt-api/v1.0.0/commonArticle/findByCategoryId/7324d3908db340aab640c66997ea0b5d/{}/10'.format(page)
        response = get_response(url,headers)
        get_data(response,headers,pm)
        # print('THIS  PAGE {} IS FINISH!'.format(page))
        time.sleep(0.1)


if __name__ == '__main__':
    main()