# -*-coding:utf-8 -*-
# Project:
# Author:mongoole
# Date:2021/12/31

import io
import os
import re
import sys
import time
import random
import requests
import pymongo
from lxml import etree
from fake_useragent import UserAgent

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030') #change the default encoding of standard output

def mongodb():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1',port=27017)
    #选择数据库
    db = client.dnb_content
    #选择操作的集合
    company = db.dnb_company
    detail = db.company_detail
    return company,detail


def get_data(url):

    headers = {
        "user-agent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36',
        "Referer": "https://www.dnb.com/",
    }

    try:
        html_content = requests.get(url,headers=headers,verify=False)
        if html_content.status_code == 200:
            html_content = html_content.text
            return html_content

    except Exception as e:
        print("This is get_data function:{}".format(e))


def parse_html(html_content, mongo):
    selector = etree.HTML(html_content)
    contents = selector.xpath('//*[@class="col-md-12 data"]')
    for item in contents:
        data_dict = {}
        company_name = item.xpath('./div[1]/a/text()')[0].strip()
        company_url = 'https://www.dnb.com/' + item.xpath('./div[1]/a/@href')[0]
        company_location = item.xpath('./div[2]//text()')
        company_location = "".join([item.strip().replace(' ','') for item in company_location])
        company_sales = item.xpath('./div[3]/text()')
        company_sales = "".join([item.strip().replace(' ','') for item in company_sales])
        # print(company_name,company_url,company_location,company_sales)
        # breakpoint()
        data_dict['company_name'] = company_name
        data_dict['company_url'] = company_url
        data_dict['company_location'] = company_location
        data_dict['company_sales'] = company_sales
        mongo.insert(data_dict)


def detail_company(company_url):

    headers = {
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36',
        # 'Referer': 'https://www.dnb.com/business-directory/company-information.chemical_manufacturing.za.html?page=10',
        # 'Cookie': '_st_l=38.600|8662583217,8558992567,,+18558992567,0,1640971796|8786357419.4343612965.9492719953.1031387594.3982683314.8510566527.7502533514.66244690361.27118146526.9894053845.0978322870.67134104420.1704876474.25765448386.82890925314.0767448325.8694241282.1704876505.27123078519.2766162450; _st=5054d950-6a3d-11ec-ab1e-1dc01cb8cddd.5058d0f0-6a3d-11ec-ab1e-1dc01cb8cddd....0....1640971916.1640976867.600.10800.30.0....1....1.10,11..dnb^com.UA-18184345-1.1987423323^1640957207.38.; chosen_visitorFrom_cookie_new=DIR; HID=1640957205689; _mkto_trk=id:080-UQJ-704&token:_mch-dnb.com-1640957206144-18581; _gcl_au=1.1.1084138700.1640957206; _biz_uid=7b4f3a74b8a44a67ad6781d7f63ef976; AMCVS_8E4767C25245B0B80A490D4C@AdobeOrg=1; _ga=GA1.2.1987423323.1640957207; _gid=GA1.2.1712985927.1640957207; _biz_su=7b4f3a74b8a44a67ad6781d7f63ef976; _fbp=fb.1.1640957207640.1203706623; __ncuid=a0e63d9c-1377-42e6-96c3-8e48f05244ba; tbw_bw_uid=bito.AAOMsk7DTs4AACrzwwpeuQ; tbw_bw_sd=1640957208; _sp_ses.2291=*; s_cc=true; _st_bid=5054d950-6a3d-11ec-ab1e-1dc01cb8cddd; drift_aid=f0b0ed72-00a9-4709-86a0-d37e7fe3bda1; driftt_aid=f0b0ed72-00a9-4709-86a0-d37e7fe3bda1; w_AID=84375683020684190203316238115972255779; _biz_flagsA={"Version":1,"Mkto":"1","ViewThrough":"1","XDomain":"1","Ecid":"195908183"}; DMCP=1; __exponea_etc__=c9803c00-0fe7-463a-a7c6-c69415b8cd80; s_sq=[[B]]; AWSALB=K/wy991sQyg5hPRAPt7RrV1Xn1iYA9ghUuDODVjZyzOBno9Sgop0MZFO4IBdkwF+78uOjhhh6bG+EVcXoWBifUSmH+J1pow2ltq3GAkwpTGosbRl7fMZDhLrR6px; AWSALBCORS=K/wy991sQyg5hPRAPt7RrV1Xn1iYA9ghUuDODVjZyzOBno9Sgop0MZFO4IBdkwF+78uOjhhh6bG+EVcXoWBifUSmH+J1pow2ltq3GAkwpTGosbRl7fMZDhLrR6px; JSESSIONID=node0anin79fqrlkh12k8cnvmnbmwi4005203.node0; ak_bmsc=123107BED674E655B0B4B67A362F0E90~000000000000000000000000000000~YAAQVC43FzX58p99AQAAxV0pEQ7U0b/Qcv1C190Qi8S+/czND0XMxMzDPH0zzGEJ3vAeK9w5Ul/eYhrzKWdtoW5k/arihR+A4mJV3nQItPN6TSgCxTKs9I+fVe1gMV1xxFl4flFD/UNBWP8n7u37T5/UXYH92KkZIUg3FqK6EIIF/p7csKP464iA20hRbxlPkyiVL+UD6Qjq4cBM/3kT1GGq7yMDAUlYA661U1DyK/CErCZp8Od1I9T2w7wk9tC7P3UJU3UjuiWdSnmRuRxjsTSWYPbQ2Fb7aDLZ+ztEz35av8/ljf5OWTuD0yJwsjLribXqJzdAjJvRO8OK9FtYq6YjfHaXcHvrLXS4U1XCg7xLfgh2YLfcysT5+9I4+FCS67qjxa8/OKyofa7lneYBscVLyCl9sSYWSo8Vn+QSHbGlyt8YUOT+eUi8XYU47z6jasd/MishjTqsvrosR7UaAIrBwpfNsL4IP1Ak5eA+N5YoQ5CqsBE=; AMCV_8E4767C25245B0B80A490D4C@AdobeOrg=-1124106680|MCIDTS|18993|MCMID|84375683020684190203316238115972255779|MCAAMLH-1641570239|3|MCAAMB-1641570239|RKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y|MCOPTOUT-1640972640s|NONE|vVersion|5.2.0; bm_sv=376A8F193CC5176CB4D31FBF8901E1F6~J0Xakrf6dea5XYXO72g++1cYCea8GWej4Je8a7EjGF2CG1DeLL8N1r9Ynsc87R+GnSxRvz2WZduN6bjvAmdyGCQtYeqypUhvNjaAa1JVdzuvEOz/t3PUhU7oDtx+kvNOl7nIy/jSj0oxBHz4rmrxGw==; _biz_sid=96f97c; _biz_nA=33; s_nr30=1640971335496-Repeat; _dc_gtm_UA-18184345-1=1; _gat_UA-19892859-28=1; __exponea_time2__=-0.2510569095611572; _biz_pendingA=[]; _sp_id.2291=801f6e9a-6609-4985-95a4-c5e79669ec76.1640957208.1.1640971337.1640957208.fada0392-567a-4fca-946c-b03320cd9dd2; __gads=ID=20a9f07cf7d482bd-2230864b96cf00f0:T=1640957308:RT=1640971337:S=ALNI_MZkOBjZYo6APpYU0k0eXEwbK6logA; drift_campaign_refresh=d1e03f6c-2b67-494d-9797-2e8185741dc4; _st=5054d950-6a3d-11ec-ab1e-1dc01cb8cddd.5058d0f0-6a3d-11ec-ab1e-1dc01cb8cddd....0....1640971938.1640982138.600.10800.30.0....1....1.10,11..dnb^com.UA-18184345-1.1987423323^1640957207.38.; QSI_HistorySession=https://www.dnb.com/business-directory/company-profiles.brother_cisa_(pty)_ltd.94032a0dfc2636613c8c766c08c451aa.html~1640966068466|https://www.dnb.com/business-directory/company-profiles.exxaro_ferroalloys_(pty)_ltd.caf329749f16ebfc97ba096b9fe60d49.html~1640971339355; _st_l=38.600|8662583217,8558992567,,+18558992567,0,1640971938|8786357419.4343612965.9492719953.1031387594.3982683314.8510566527.7502533514.66244690361.27118146526.9894053845.0978322870.67134104420.1704876474.25765448386.82890925314.0767448325.8694241282.1704876505.27123078519.2766162450; _gat_ncAudienceInsightsGa=1; RT="z=1&dm=www.dnb.com&si=a1e14774-7976-4160-a127-10b8c2baf435&ss=kxufju6s&sl=a&tt=rre&obo=4&rl=1&ld=8f6mk&r=119b5vgd0&ul=8f6mk"',
    }
    response = requests.get(company_url,headers=headers,verify=False)
    try:
        if response.status_code == 200:

            html_content = response.text
            # print(html_content)
            # breakpoint()
            return html_content
    except Exception as e:
        print("This is detail_company function:{}".format(e))


def parse_detail(html_content, mongo, page_url):

    data_dict = {}
    selector = etree.HTML(html_content)

    company_name = selector.xpath('//*[@name="company_name"]/span/text()')[0]

    company_description = selector.xpath('//*[@name="company_description"]/span/text()')[0]
    company_industry1 = selector.xpath('//*[@name="industry_links"]/span/span/a/text()|'
                                       '//*[@name="industry_links"]/span/span/span/text()')
    
    company_industry1 = [item.strip().replace(',','').replace(' ','') for item in company_industry1]
    company_industry2 = selector.xpath('//*[@name="industry_links"]/span[3]//span/span/span/text()')
    company_industry2 = [item.strip().replace(',','').replace(' ','') for item in company_industry2]
    company_industry = company_industry1 + company_industry2

    company_address = selector.xpath('//*[@name="company_address"]/span/text()')[0].strip().replace(' ','')

    try:
        selector.xpath('//*[@name="company_phone"]')
        company_phone = selector.xpath('//*[@name="company_phone"]/span/text()')[0]

    except:
        company_phone = ''

    try:
        selector.xpath('//*[@name="company_website"]')
        company_website = selector.xpath('//*[@name="company_website"]//a/@href')[0]
    except:
        company_website = ''

    try:
        selector.xpath('//*[@name="employees_this_site"]')
        employees_this_site = selector.xpath('//*[@name="employees_this_site"]/span/text()')[0].replace(' ','')
    except:
        employees_this_site = ''

    try:
        selector.xpath('//*[@name="employees_all_site"]')
        employees_all_site = selector.xpath('//*[@name="employees_all_site"]/span/text()')[0].replace(' ','')
    except:
        employees_all_site = ''


    try:
        selector.xpath('//*[@name="revenue_in_us_dollar"]')
        revenue_in_us_dollar = selector.xpath('//*[@name="revenue_in_us_dollar"]/span/text()')[0]
    except:
        revenue_in_us_dollar = ''

    try:
        selector.xpath('//*[@name="fiscal_year_end"]')
        fiscal_year_end = selector.xpath('//*[@name="fiscal_year_end"]/span/text()')[0]
    except:
        fiscal_year_end = ''


    try:
        selector.xpath('//*[@name="year_started"]')
        year_started = selector.xpath('//*[@name="year_started"]/span/text()')[0]
    except:
        year_started = ''

    data_dict["company_name"] = company_name
    data_dict["company_description"] = company_description
    data_dict["company_industry"] = company_industry
    data_dict["company_address"] = company_address
    data_dict["company_phone"] = company_phone
    data_dict["company_website"] = company_website
    data_dict["employees_this_site"] = employees_this_site
    data_dict["employees_all_site"] = employees_all_site
    data_dict["revenue_in_us_dollar"] = revenue_in_us_dollar
    data_dict["fiscal_year_end"] = fiscal_year_end
    data_dict["year_started"] = year_started
    data_dict["page_url"] = page_url

    mongo.insert(data_dict)


def read_file():
    with open('account_url') as f:
        urls = f.readlines()

    return urls


def main():
    mongo = mongodb()
    for page in range(185,201):
        url = 'https://www.dnb.com/business-directory/company-information.chemical_manufacturing.za.html?page={}'.format(page)
        html_content = get_data(url)
        parse_html(html_content,mongo[0])
        print('{} Page {} was finished!{}'.format('*'*10,page,'*'*10))
        time.sleep(3)


def main1():
    mongo = mongodb()
    urls = read_file()
    for url in urls:
        url = url.strip()
        html_content = detail_company(url)
        parse_detail(html_content,mongo[1],url)
        print('{} url: {} was finished!{}'.format('*'*10,url,'*'*10))
        # time.sleep(1)

if __name__ == '__main__':
    main1()