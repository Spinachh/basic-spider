# !/usr/bin/python
# -*-coding:utf-8-*-
import json
import re
import time
from ..items import *
import scrapy

class StandSpider(scrapy.Spider):

    name = 'stand'
    start_urls = ['http://standard.sist.org.cn/StdSearch/Ajaxdata/StdSearch.ashx?OperType=1']
    # start_urls = ['https://www.baidu.com']

    def start_requests(self):
        url = 'http://standard.sist.org.cn/StdSearch/Ajaxdata/StdSearch.ashx?OperType=1'
        for page in range(1,1496):    #gb
        # for page in range(1,2822):      #final_content
            data = {
                'IsFirst': '1',
                'OnePageCount': '50',
                'CurentPage': str(page),
                'matchType': '标准',
                'submatchType': '标准',
                'ischangeWord': '1',
                'where':'标准组织||GB$$组织类别||$$包含作废标准||0$$',
                # 'where':'标准组织||GB$$标准状态||$$组织类别||$$包含作废标准||0$$',
                # 'where': '标准组织||(DB),(GM),AQ,BM,CAS,CBMF,CCES,CFDAB,CGMA,CJ,CCNAB,CNAS,CNCA,CSM,DA,DLGJ,EJ,FZ,GBZ,GH,GGHAF,HBC,HJ,HS,JB,JG,JJG,JT,JTJ,JZ,LB,LS,MH,MZ,NY,QC,QX,RRISC,SF,SJ,SY,TD,TY,WH,WJ,WS,XB,YYD,YY,ZY,ZC,YZ,YS,YC,YB,WW,WM,WHB,WB,TSG,TB,SW,SL,SH,SD,RHB,RB,QJ,QB,NB,MT,LY,LD,KY,JY,JTG,JR,JJF,JGJ,JC,HY,HJB,HG,HAD,GY,GD,GA,DZ,DL,CY,CSC,CNAT,CNAL,CMMA,CJB,CH,CGC,CCGF,CCEC,CB,CAB$$组织类别||$$包含作废标准||0$$',

            }
            yield scrapy.FormRequest(url,method='POST',formdata=data,callback=self.parse)
    def parse(self, response):
        #此时的response就是一个json格式，response.text,字符串形式
        content = json.loads(response.text)
        content = content['stdList']
        detail_url = 'http://standard.sist.org.cn/StdSearch/Ajaxdata/StdSearch.ashx?OperType=2'

        for item in content:
            data = item['a100']
            param = {
                'a100':data
            }
            yield scrapy.FormRequest(url=detail_url,method='POST',formdata=param,callback=self.parse_detail)

    def parse_detail(self,respnose):
        # print(respnose.text,type(respnose.text))
        item = StandItem()
        detail_response = json.loads(respnose.text)
        part1 = detail_response['Std']
        item['stand_num'] = part1['a100']  #标准编号
        item['stand_status'] = detail_response['a2001']   #标准状态
        item['cn_name'] = part1['a298']    #中文名称
        item['en_name'] = part1['a302']    #外文名称
        item['industry_name'] = part1['a108']  #行业名称
        if len(detail_response['a825List']) != 0:
            type_name = detail_response['a825List'][0].split('】')[0]  #类别名称
            item['type_name'] = type_name.split('【')[1]
        else:
            item['type_name'] = 'Null'
        if len(detail_response['a826List']) != 0:
            ics_name = detail_response['a826List'][0].split('】')[0]   #ICS分类
            item['ics_name'] = ics_name.split('【')[1]
        else:
            item['ics_name'] = 'Null'
        if part1['a101']:
            stamp_time = part1['a101']   #发布日期
            ss_time = re.findall(r'\d+',stamp_time)[0][:-3]
            try:
                item['publish_time'] = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(int(ss_time)))
            except:
                item['publish_time'] = 'Null'
        else:
            item['publish_time'] = 'Null'

        if part1['a205']:
            implement_stamp_time = part1['a205'] #实施日期
            implement_ss_time = re.findall(r'\d+',implement_stamp_time)[0][:-3]
            try:
                item['implement_time'] = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(int(implement_ss_time)))
            except:
                item['implement_time'] = 'Null'
        else:
            item['implement_time'] = 'Null'

        if part1['a206']:
            invalid_stamp_time = part1['a206']
            if '-' not in invalid_stamp_time:
                invalid_ss_time = re.findall(r'\d+',invalid_stamp_time)[0][:-3]
                try:
                    item['invalid_time'] = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(int(invalid_ss_time)))
                except:
                    item['invalid_time'] = 'Null'
            else:
                item['invalid_time'] = 'Null'
        else:
            item['invalid_time'] = 'Null'

        if detail_response['std_a462List']:         #被替代标准
            try:
                lst1 = []
                for i in detail_response['std_a462List']:
                    lst1.append(i['a462'])
                item['be_replaced_stand'] = ",".join(lst1)
            except:
                item['be_replaced_stand'] = 'Null'
        else:
            item['be_replaced_stand'] = 'Null'

        if detail_response['std_a462List1']:        #替代标准
            try:
                lst2 = []
                for i in detail_response['std_a462List1']:
                    lst2.append(i['a100'])
                item['replaced_stand'] = ",".join(lst2)
            except:
                item['replaced_stand'] = 'Null'
        else:
            item['replaced_stand'] = 'Null'

        if detail_response['std_a800List1']:        #采用标准
            try:
                lst3 = []
                for i in detail_response['std_a800List1']:
                    lst3.append(i['a100'])
                item['use_stand'] = ",".join(lst3)
            except:
                item['use_stand'] = 'Null'
        else:
            item['use_stand'] = 'Null'

        if detail_response['std_a823List']:     #修改记录
            try:
                lst4 = []
                for i in detail_response['std_a823List']:
                    lst4.append(i['a8231'])
                if detail_response['std_a8231List']:
                    lst4.append(detail_response['std_a8231List'][0])
                    item['modify_record'] = ",".join(lst4)
                else:
                    item['modify_record'] = ",".join(lst4)
            except:
                item['modify_record'] = 'Null'
        else:
            item['modify_record'] = 'Null'

        if part1['Abstract']:
            try:
                item['digest'] = part1['Abstract'] #摘要
            except:
                item['digest'] = 'Null'
        else:
            item['digest'] = 'Null'

        if part1['fbdw']:
            try:
                item['publish_company'] = part1['fbdw']    #发布单位
            except:
                item['publish_company'] = 'Null'
        else:
            item['publish_company'] = 'Null'

        if part1['qcdw']:
            try:
                item['draft_company'] = part1['qcdw']  #起草单位
            except:
                item['draft_company'] = 'Null'
        else:
            item['draft_company'] = 'Null'

        if part1['qcr']:
            try:
                item['draft_people'] = part1['qcr']   #起草人
            except:
                item['draft_people'] = 'Null'
        else:
            item['draft_people'] = 'Null'

        yield item


