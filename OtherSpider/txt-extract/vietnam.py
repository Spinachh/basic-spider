#-*-conding:utf-8-*-
import os
import random
import time
import requests
from lxml import etree

def get_content(url,headers):

    html = requests.get(url,headers=headers,verify=False).text
    # print(html)
    selector = etree.HTML(html)
    content = selector.xpath('//*[@class="l-content main-col"]//article')
    #//*[@class="l-content main-col"]//article/div[1]/time/text()
    # print(content)
    for item in content:
        publish_time = item.xpath('./div[1]/time/text()')[0]
        page_url_part = item.xpath('./a/@href')[0]
        # print(publish_time,page_url_part)
        page_detail(page_url_part,headers,publish_time)

def page_detail(page_url_part,headers,publish_time):
    page_url = 'https://www.vietnamplus.vn' + page_url_part
    html_content = requests.get(page_url,headers=headers,verify=False).text
    # print(html_content)
    selector = etree.HTML(html_content)
    content = selector.xpath('//*[@class="content article-body cms-body AdAsia"]/p/text()')
    # print('-------这是打印的内容：%s-------'%content)
    write_txt(publish_time,content)

def write_txt(publish_time,content):
    #05/07/2020 - 07:04
    pubs_time = publish_time.split(' - ')
    part1_time = pubs_time[0].replace('/','-')
    part2_time = pubs_time[1].replace(':','-')
    pub_time = part1_time + '-' + part2_time
    page_text = "".join(content)
    with open(r'result/%s.txt'%pub_time,'a',encoding='utf-8') as f:
        f.write(page_text)

def main():
    # base_url = 'https://www.vietnamplus.vn/chude/dich-viem-duong-ho-hap-cap-covid19/1070/trang3.vnp'
    base_url = 'https://www.vietnamplus.vn/chude/dich-viem-duong-ho-hap-cap-covid19/1070/trang'
    headers = {
        'User-Agent':'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; AcooBrowser; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
        # 'Referer': '',
    }

    for page in range(1,101):
        url = base_url + str(page) + '.vnp'
        get_content(url,headers)
        print('----------Page %s is Finshed----------'%page)
        time.sleep(random.randint(3,8))

if __name__ == '__main__':
    main()