#-*-conding:utf-8-*-
import random
import time
import requests
from lxml import etree

def get_content(url,headers):

    html = requests.get(url,headers=headers).text
    # print(html)
    selector = etree.HTML(html)
    content = selector.xpath('//*[@class="titList"]/ul/li')
    # print(content)
    for item in content:
        publish_time = item.xpath('./span/text()')[0]
        page_url_part = item.xpath('./a/@href')[0]
        # print(page_url)
        title = item.xpath('./a/text()')[0]
        # print(title)
        page_detail(page_url_part,headers,publish_time)

def page_detail(page_url_part,headers,publish_time):
    page_url = 'http://thai.cri.cn' + page_url_part
    html_content = requests.get(page_url,headers=headers).text
    # print(html_content)
    selector = etree.HTML(html_content)
    content = selector.xpath('//*[@class="abody"]/p/text()')
    # print('-------这是打印的内容：%s-------'%content)
    write_txt(publish_time,content)

def write_txt(publish_time,content):
    #2020-06-18 00:47:32
    pubs_time = publish_time.split(' ')
    part1_time = pubs_time[0]
    part2_time = pubs_time[1].replace(':','-')
    pub_time = part1_time + part2_time
    page_text = "".join(content)
    with open(r'result/%s.txt'%pub_time,'a',encoding='utf-8') as f:
        f.write(page_text)

def main():
    # base_url = 'http://thai.cri.cn/list/6eaf5874-162f-4c4c-9f60-a674fd27d4e0-5.html'
    base_url = 'http://thai.cri.cn/list/6eaf5874-162f-4c4c-9f60-a674fd27d4e0-'
    headers = {
        'User-Agent':'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; AcooBrowser; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
        'Referer': 'http://thai.cri.cn/list/6eaf5874-162f-4c4c-9f60-a674fd27d4e0.html',
    }

    for page in range(2,103):
        url = base_url + str(page) + '.html'
        get_content(url,headers)
        print('----------Page %s is Finshed----------'%page)
        time.sleep(random.randint(3,8))

if __name__ == '__main__':
    main()