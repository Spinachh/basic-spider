# -*- coding:utf-8 -*-
# Author: mongoose
# Date: 2022/07/08
# Version: v3(code review)

import requests
import time
from urllib.parse import urlparse
from lxml import etree
import pymongo
import logging
from gne import GeneralNewsExtractor
from fake_useragent import UserAgent

logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


class PaperNews(object):

    def __init__(self, pro_name, pro_timezone, sin_time):
        self.project_name = pro_name
        self.project_timezone = pro_timezone
        self.since_time = sin_time

    @staticmethod
    def mongodb():
        # 连接数据库mongodb
        client = pymongo.MongoClient(host='127.0.0.1', port=27017)
        # 选择数据库
        db = client.paper_news
        # 选择操作的集合
        collect = db.user_content_2022  # 科协的主要数据集合
        return collect

    @staticmethod
    def get_html(url, user_id, page_id):
        headers = {
            "User-Agent": UserAgent().random,
            "Referer": "https://www.thepaper.cn/gov_{}".format(user_id),
        }

        data = {
            "uid": user_id,
            "utype": 2,  # 该参数是gov的为2，user的为空
            "pageidx": page_id,
            "lastTime": "undefine",
        }
        html = requests.get(url, headers=headers, params=data).text
        select = etree.HTML(html)
        content = select.xpath('//*[@class="news_li"]')
        return content

    def parse_html(self, content, user_id, user_name):
        for item in content:
            content_dict = {}
            paper_url = item.xpath('a/@href')[0]
            paper_url = "https://www.thepaper.cn/" + paper_url

            if len(item.xpath('a/div/div')) == 2:
                # 获取该条文字的封面图片
                img_url = item.xpath('a/div/div[1]/img/@src')[0]
                # p = urlparse(img_url)  # urlparse处理获取到的url
                # img_url = p.netloc + p.path
                title = item.xpath('a/div/div[2]/text()')[0]
                abstract = item.xpath('div[2]/@data-description')[0]
            else:
                title = item.xpath('a/div/div[1]/text()')[0]
                abstract = item.xpath('div[2]/@data-description')[0]
                img_url = ''

            # publish_time = item.xpath('div[1]/div/div[1]/div[1]/text()')
            try:
                like_num = item.xpath('div[1]/div/div[1]/div[2]/span[2]/text()')[0]
            except Exception as e:
                like_num = '0'

            try:
                comment_num = item.xpath('div[1]/div/div[1]/div[3]/span[2]/text()')[0]   # 当有评论并在页面显示的时候可以获取
            except Exception as e:
                comment_num = '0'

            content_dict['uid'] = user_id
            content_dict['title'] = title.strip()
            content_dict['abstract'] = abstract
            content_dict['crawl_time'] = time.strftime("%Y-%m-%d", time.localtime())
            content_dict['img_background_url'] = img_url
            content_dict['like_num'] = like_num
            content_dict['comment_num'] = comment_num
            content_dict['url'] = paper_url
            content_dict['user_name'] = user_name

            try:
                result, content = self.get_detail_article(paper_url)
                update_time = result['publish_time']
                update_stamp = time.mktime(time.strptime(update_time, '%Y-%m-%d %H:%M'))
                if update_stamp > self.since_time:
                    content_dict['created_time'] = update_time
                    content_dict['content'] = result['content'].replace('\n', '')
                    content_dict["project_name"] = self.project_name
                    content_dict["project_timezone"] = self.project_timezone
                    self.mongodb().insert_one(content_dict)
                else:
                    break
            except Exception as e:
                logging.warning('Parse Html Article Has Error:{} !'.format(e))

    @staticmethod
    def get_detail_article(url):
        session = requests.session()
        headers = session.headers
        response = session.get(url)
        response.encoding = 'utf-8'
        html_content = response.text
        # html_content = response.replace(u'\xa0', u' ')

        extractor = GeneralNewsExtractor()
        result = extractor.extract(html_content)
        return result, html_content

    @staticmethod
    def get_user():
        with open(r'ScienceTecUser/scienceTec_paper_user') as f:
            lines = f.readlines()
        return lines

    def cron_page(self, user_id, user_name):
        for page_id in range(1, 51):
            url = 'https://www.thepaper.cn/load_pph_conts.jsp?'
            try:
                content = self.get_html(url, user_id, page_id)
            except Exception as e:
                logging.warning('UserName: {} PageNum: {} Has no content!Error:{}'.format(user_name, page_id, e))
                break
            if content:
                self.parse_html(content, user_id, user_name)
                logging.warning('UserName: {} PageNum: {} Has Finished！'.format(user_name, page_id))
            time.sleep(2)

    def start_run(self):
        # lines = self.get_user()
        lines = (item for item in self.get_user())
        # print(items)
        # breakpoint()
        for item in lines:
            user_id = int(item.split('_')[1])
            user_name = item.split('#####')[0]
            self.cron_page(user_id, user_name)


if __name__ == '__main__':
    project_name = 'KeXie'
    project_timezone = "2022-Q3"
    since_time = time.mktime(time.strptime('2022-07-01 00:00:00', '%Y-%m-%d %H:%M:%S'))
    PaperNewsSpider = PaperNews(project_name, project_timezone, since_time)
    PaperNewsSpider.start_run()
