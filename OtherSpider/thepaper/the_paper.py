import requests
import time
from urllib.parse import urlparse
from lxml import etree
import pymongo
from fake_useragent import UserAgent

def mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1',port=27017)
    #选择数据库
    db = client.KeXie
    #选择操作的集合
    p = db.content

    return p

def get_html(url,uid,pageidx):
    ua = UserAgent().random
    headers = {
        "User-Agent":ua,
        "Referer":"https://www.thepaper.cn/gov_{}".format(uid),
                   }

    data = {
        "uid": uid,
        "utype": 2,
        "pageidx": pageidx,
        "lastTime": "undefine",
    }
    # data = {
    #     "uid": uid,
    #     "utype": 2,
    #     "pageidx": page_num,
    #     "lastTime": "",
    #     "filterContIds": "",
    #
    # }

    # html  = requests.get(url,params=data,headers=headers).text
    html = requests.get(url,headers=headers,params=data).text
    # print(html)
    # breakpoint()
    select = etree.HTML(html)

    content = select.xpath('//*[@class="news_li"]')

    return content

def parse_html(content,uid):

    content_lst =[]

    for item in content:
        content_dict = {}
        paper_url = item.xpath('a/@href')[0]
        paper_url = "https://www.thepaper.cn/" + paper_url

        if len(item.xpath('a/div/div')) == 3:
            #获取该条文字的封面图片
            img_url = item.xpath('a/div/div[1]/img/@src')[0]
            p = urlparse(img_url)       #urlparse处理获取到的url
            img_url = p.netloc + p.path

            title = item.xpath('a/div/div[2]/text()')
            abstract = item.xpath('a/div/div[3]/text()')
            # print(abstract)
        else:
            title = item.xpath('a/div/div[1]/text()')
            abstract = item.xpath('a/div/div[2]/text()')
            # print(abstract)
        if len(item.xpath('div')) == 3:
            publish_time = item.xpath('div[2]/div/div[1]/div[1]/text()')
            like_num = item.xpath('div[2]/div/div[1]/div[2]/span[2]/text()')
        else:
            publish_time = item.xpath('div[1]/div/div[1]/div[1]/text()')
            like_num = item.xpath('div[1]/div/div[1]/div[2]/span[2]/text()')

        if len(like_num):
            like_num = like_num[0]
            # print(like_num)
        else:
            like_num == "0"

        #comment_num = item.xpath('div[1]/div/div[1]/div[3]/span[2]/text()')   #当有评论并在页面显示的时候可以获取
        comment_num = 0

        if len(title):
            title = title[0]
        else:
            title = ""
        if len(abstract):
            abstract = abstract[0]
        else:
            abstract = ""

        if len(publish_time):
            publish_time = publish_time[0]
            # if '天前' in publish_time:
            #     new_publish_time =  - publish_time.split('天前')[0]
        else:
            publish_time = ""


        content_dict['uid'] = uid
        content_dict['title'] = title
        content_dict['abstract'] = abstract
        content_dict['crawl_time'] = time.strftime("%Y-%m-%d",time.localtime())
        content_dict['publish_time'] = publish_time
        content_dict['like_num'] = like_num
        content_dict['comment_num'] = comment_num
        content_dict['url'] = paper_url

        content_lst.append(content_dict)
    return content_lst

if __name__ == '__main__':

    p = mongo()
    # uid = ["61444", "61429", "60735", "61419", "61357", "59850", "60768", "59825",
    #         "81335", "59840", "61382", "4605060", "60778", "60808", "59966", "61407",
    #         "61459", "83406", "61362", "60788", "4548886", "4355554", "61377", "59830",
    #         "61352", "60725", "60773", "28183", "57944","58037","65666","66501","67119",
    #         "64367","70399","68435","92392","4548886","4605060","4355554","64377","88378",
    #         "3716048","83250","41903",]

    uids = ["61444", "61429", "60735", "61419", "61357",
             "59850", "60768", "59825", "81335", "59840",
             "61382", "4605060", "60778", "60808",
             "59966", "61407", "61459", "83406",
             "61362", "60788", "4548886", "4355554",
             "61377", "59830", "61352", "60725", "60773",
             "28183", "57944",
             "58037","58037","65666","66501","67119","64367",
             "70399","68435","92392","4548886","4605060","4355554",
             "64377","88378","3716048","83250","41903","70134"
    ]
    # uids = ["61444"]
    for uid in uids:
        for idx in range(1,300):
            # url = 'https://www.thepaper.cn/load_pph_conts.jsp?uid={}' \
            #       '&utype=2&pageidx={}&lastTime=undefine&filterContIds='.format(uid,idx)

            url = 'https://www.thepaper.cn/load_pph_conts.jsp?'
            try:
                content = get_html(url,uid,idx)
            except:
                print('UID为：%s的账号第%s页没有内容'%(uid,idx))
                break
            if content:
                content_dict = parse_html(content,uid)
                p.insert(content_dict)
                print('UID为：%s的第%s页爬取完成！！！'%(uid,idx))



