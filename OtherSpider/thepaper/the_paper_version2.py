import requests
import time
from urllib.parse import urlparse
from lxml import etree
import pymongo
import logging
from gne import GeneralNewsExtractor
from fake_useragent import UserAgent

logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    # 连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # 选择数据库
    db = client.paper_news
    # 选择操作的集合
    p = db.user_content  # 科协的主要数据集合
    return p


def get_html(url, uid, pageidx):
    headers = {
        "User-Agent": UserAgent().random,
        "Referer": "https://www.thepaper.cn/gov_{}".format(uid),
    }

    data = {
        "uid": uid,
        "utype": 2,  # 该参数是gov的为2，user的为空
        "pageidx": pageidx,
        "lastTime": "undefine",
    }

    html = requests.get(url, headers=headers, params=data).text
    select = etree.HTML(html)
    content = select.xpath('//*[@class="news_li"]')

    return content


def parse_html(content, uid, user_name, pro_name, pro_timezone):
    content_lst = []

    for item in content:
        content_dict = {}
        paper_url = item.xpath('a/@href')[0]
        paper_url = "https://www.thepaper.cn/" + paper_url

        if len(item.xpath('a/div/div')) == 3:
            # 获取该条文字的封面图片
            img_url = item.xpath('a/div/div[1]/img/@src')[0]
            p = urlparse(img_url)  # urlparse处理获取到的url
            img_url = p.netloc + p.path

            title = item.xpath('a/div/div[2]/text()')
            abstract = item.xpath('a/div/div[3]/text()')
            # print(abstract)
        else:
            title = item.xpath('a/div/div[1]/text()')
            abstract = item.xpath('a/div/div[2]/text()')
            # print(abstract)
        if len(item.xpath('div')) == 3:
            publish_time = item.xpath('div[2]/div/div[1]/div[1]/text()')
            like_num = item.xpath('div[2]/div/div[1]/div[2]/span[2]/text()')
        else:
            publish_time = item.xpath('div[1]/div/div[1]/div[1]/text()')
            like_num = item.xpath('div[1]/div/div[1]/div[2]/span[2]/text()')

        if len(like_num):
            like_num = like_num[0]
            # print(like_num)
        else:
            like_num == "0"

        # comment_num = item.xpath('div[1]/div/div[1]/div[3]/span[2]/text()')   #当有评论并在页面显示的时候可以获取
        comment_num = 0

        if len(title):
            title = title[0]
        else:
            title = ""
        if len(abstract):
            abstract = abstract[0]
        else:
            abstract = ""

        if len(publish_time):
            publish_time = publish_time[0]
            # if '天前' in publish_time:
            #     new_publish_time =  - publish_time.split('天前')[0]
        else:
            publish_time = ""

        content_dict['uid'] = uid
        content_dict['title'] = abstract
        content_dict['abstract'] = title.strip()
        content_dict['crawl_time'] = time.strftime("%Y-%m-%d", time.localtime())
        content_dict['publish_time'] = publish_time
        content_dict['like_num'] = like_num
        content_dict['comment_num'] = comment_num
        content_dict['url'] = paper_url
        content_dict['user_name'] = user_name

        try:
            result, content = get_detail_article(paper_url)
            content_dict['created_time'] = result['publish_time']
            content_dict['content'] = result['content'].replace('\n', '')
        except Exception as e:
            print(e)

        content_dict["project_name"] = pro_name
        content_dict["project_timezone"] = pro_timezone
        # content_dict["project_name"] = "MainMediaResearch"
        content_lst.append(content_dict)

    return content_lst


def get_detail_article(url):
    session = requests.session()
    headers = session.headers
    response = session.get(url)
    response.encoding = 'utf-8'
    html_content = response.text
    # html_content = response.replace(u'\xa0', u' ')

    extractor = GeneralNewsExtractor()
    result = extractor.extract(html_content)
    # print(result)

    return result, html_content


def get_user():
    with open('paper_user_kexie') as f:
        user_lst = f.readlines()
    return user_lst


def main(pro_name, pro_timezone):
    mongo = mongodb()

    user_lst = get_user()  # 央视网#####https://www.thepaper.cn/user_3987611
    for item in user_lst:
        uid = int(item.split('_')[1])
        user_name = item.split('#####')[0]
        for idx in range(1, 100):

            url = 'https://www.thepaper.cn/load_pph_conts.jsp?'
            try:
                content = get_html(url, uid, idx)
            except Exception as e:
                logging.warning('{}账号第{}页没有内容!Error:{}'.format(user_name, idx, e))
                break
            if content:
                content_dict = parse_html(content, uid, user_name, pro_name, pro_timezone)
                mongo.insert(content_dict)
                logging.warning('{}第{}页采集完成！！！'.format(user_name, idx))
            time.sleep(2)


if __name__ == '__main__':
    project_name = 'KeXie'
    project_timezone = "2022-Q2"
    main(project_name, project_timezone)

    
    

