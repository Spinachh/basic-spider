# -*- coding:utf-8 -*-
# Author: mongoole
# Date: 2022/09/08
# Version: v3(code review)

import requests
import time
import json
import pymongo
import logging
from gne import GeneralNewsExtractor

logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


class PaperNews(object):

    def __init__(self, pro_name, pro_timezone, sin_time):
        self.project_name = pro_name
        self.project_timezone = pro_timezone
        self.since_time = sin_time

    @staticmethod
    def mongodb():
        # 连接数据库mongodb
        client = pymongo.MongoClient(host='127.0.0.1', port=27017)
        # 选择数据库
        db = client.paper_news
        # 选择操作的集合
        # collect = db.user_content_2022  # 科协的主要数据集合
        collect = db.user_content_2022_Q3  # 科协的主要数据集合
        return collect

    @staticmethod
    def get_html(url, page_num, user_id, start_time):
        headers = {
            'Aaccept': 'application/json',
            'Content-Type': 'application/json',
            'Referer': 'https://www.thepaper.cn/',
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
        }

        payload = {
            'contType': 0, 'excludeContIds': [], 'pageNum': page_num, 'pageSize': 10, 'pphId': user_id, 'startTime': start_time,
        }

        response = requests.post(url, headers=headers, data=json.dumps(payload))
        try:
            if response.status_code == 200:
                response_text = response.text
                json_data = json.loads(response_text).get('data')
                return json_data

        except Exception as e:
            logging.warning('Funtion Get Html has Error:{}'.format(e))

    def parse_html(self, content, user_id, user_name):
        try:
            data = content.get('list')
            next_start_time = content.get('startTime')
            for item in data:
                content_dict = {}
                cont_id = item.get('contId')
                cont_type = item.get('contType')
                cont_url = "https://www.thepaper.cn/" + "newsDetail_forward_" + cont_id
                cont_title = item.get('name')
                cont_summary = item.get('summary')
                cont_pic = item.get('pic')
                cont_praise_times = item.get('praiseTimes')
                cont_comment_num = item.get('isOutForword')
                cont_publish_time = item.get('pubTimeLong')

                content_dict['uid'] = user_id
                content_dict['title'] = cont_title
                content_dict['abstract'] = cont_summary
                content_dict['publish_time'] = time.strftime("%Y-%m-%d %H:%M:%S",
                                                             time.localtime(int(str(cont_publish_time)[:10])))
                content_dict['img_background_url'] = cont_pic
                content_dict['like_num'] = cont_praise_times
                content_dict['comment_num'] = cont_comment_num
                content_dict['url'] = cont_url
                content_dict['user_name'] = user_name

                try:
                    result, content = self.get_detail_article(cont_url)
                    update_time = result['publish_time']
                    update_stamp = time.mktime(time.strptime(update_time, '%Y-%m-%d %H:%M'))
                    if update_stamp > self.since_time:
                        content_dict['created_time'] = update_time
                        content_dict['content'] = result['content'].replace('\n', '')
                        content_dict["project_name"] = self.project_name
                        content_dict["project_timezone"] = self.project_timezone
                        self.mongodb().insert_one(content_dict)
                    else:
                        next_start_time = '-1'
                        return next_start_time
                except Exception as e:
                    logging.warning('Parse Html Article Has Error:{} !'.format(e))

            return next_start_time
        except Exception as e:
            logging.warning('Conten has error:{} !'.format(e))
            next_start_time = -1
            return next_start_time

    @staticmethod
    def get_detail_article(url):
        session = requests.session()
        headers = session.headers
        response = session.get(url)
        response.encoding = 'utf-8'
        html_content = response.text
        # html_content = response.replace(u'\xa0', u' ')

        extractor = GeneralNewsExtractor()
        result = extractor.extract(html_content)
        return result, html_content

    @staticmethod
    def get_user():
        with open(r'ScienceTecUser/scienceTec_paper_user') as f:
            lines = f.readlines()
        return lines

    def cron_page(self, user_id, user_name):
        start_time = 0
        page_num = 1
        while True:
            url = 'https://api.thepaper.cn/contentapi/cont/pph/gov'
            try:
                content = self.get_html(url, page_num, user_id, start_time)
            except Exception as e:
                logging.warning('UserName: {} PageNum: {} Has no content!Error:{}'.format(user_name, page_num, e))
                break
            if content:
                next_start_time = self.parse_html(content, user_id, user_name)
                logging.warning('UserName: {} PageNum: {} Has Finished NextTime: {}'
                                .format(user_name, page_num, next_start_time))
                if next_start_time == '-1':
                    break
                else:
                    start_time = next_start_time
                    page_num += 1
                time.sleep(2)
            else:
                logging.warning('UserName: {} has no Account!'.format(user_name))
                break

    def start_run(self):
        lines = (item for item in self.get_user())
        for item in lines:
            user_id = int(item.split('_')[1])
            user_name = item.split('#####')[0]
            self.cron_page(user_id, user_name)


if __name__ == '__main__':
    project_name = 'KeXie'
    project_timezone = "2022-Q3"
    since_time = time.mktime(time.strptime('2022-08-10 00:00:00', '%Y-%m-%d %H:%M:%S'))
    PaperNewsSpider = PaperNews(project_name, project_timezone, since_time)
    PaperNewsSpider.start_run()
