#coding:utf-8
import urllib.request,re,os,socket
from bs4 import BeautifulSoup

num = 0
# page_nums = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20}
page_nums = range(1,51)
page_urls = "http://www.budejie.com/pic/"
headers = {"User-Agent":"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36"}
for page_num in page_nums:
    page_url = page_urls + str(page_num)
    # print(page_url)
    request = urllib.request.Request(url=page_url,headers=headers)
    response = urllib.request.urlopen(request)
    data = response.read().decode("utf-8")

    #print(data)
    url_target = r'data-original="(\w.+?\.\w+)" title'
    url_targets = re.findall(url_target,data)
    # print(url_targets)

    path = "E:\\study\\budejie\\Images\\" + str(page_num) + "\\"
    if os.path.exists(path) == False:
        os.makedirs(path)

    # num = 0
    for url in url_targets:
        print(url + "下载完成")
        try:
            file_name = url.rsplit(".",1)[-1]
            urllib.request.urlretrieve(url,path + "%s"%num + "." + file_name)
            num += 1
        except:
            print("Failed")