# -*- coding:utf-8 -*-
# Author: Mongoole
# Date: 2020-12-27

import time
import requests
from lxml import etree
from fake_useragent import UserAgent

import pymongo

def mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1',port=27017)
    #选择数据库
    db = client.OtherSPider
    #选择操作的集合
    p = db.foreign_article_2020_12_25

    return p

def get_response(url):

    ua = UserAgent().random

    headers = {
        'User-Agent': ua,
        'Cookie':'JSESSIONID=EFEAF71D58E9935AA8C8AC0802D73A1E; session_id=57088CAB404B1646BA0361AA01E9D24E; s_session_id=34786A0A1482A1CA168A8194F556CB35; web_client_cache_guid=8002b4fe-cc53-4067-97ab-41d1d0768e12; JSESSIONID=71D8AD5C91A41394FBFC90EA0CC6B2F0',
    }

    response = requests.get(url,headers=headers)
    # print(response.text)

    html = response.text

    return html


def handle_html(html,p):
    selector = etree.HTML(html)
    nodes = selector.xpath('//*[@id="listContainer_databody"]/tr')

    for i in range(len(nodes)):

        dic_data = {}

        dic_data['bbs_name'] = 'HSS1006-logistics questions'

        try:
            # publish_time = nodes[i].xpath('.//[@id="listContainer_row:0"]/td[3]/span[2]/span/text()')
            publish_time = nodes[i].xpath('.//td[3]/span[2]/span/text()')[0]
            publish_time = publish_time.replace('\n','').strip()
            dic_data['publish_time'] = publish_time
            # print(publish_time)
        except Exception as e:
            publish_time = nodes[i].xpath('.//td[3]/span[2]/text()')[0]
            publish_time = publish_time.replace('\n','').strip()
            dic_data['publish_time'] = publish_time

        #话题
        try:
            topic = nodes[i].xpath('.//th/span/a/text()')[0]
            topic = topic.replace('\n','').strip()
            dic_data['topic'] = topic
        except Exception as e:
            topic = nodes[i].xpath('.//th/a/text()')[0]
            topic = topic.replace('\n','').strip()
            dic_data['topic'] = topic

        #作者
        # author = nodes[i].xpath('./td[4]//span[@class="profileCardAvatarThumb"]/text()')[0]
        # author = nodes[i].xpath('.//td[4]/span[2]/span/span/span/text()')[0]

        try:
            author = nodes[i].xpath('.//span[@class="unreadmessage"]//span/span/text()|.//span[@class="profileCardAvatarThumb"]/text()')[1]
            # print(author)
            # breakpoint()
            author = author.replace('\n','').strip()
            dic_data['author'] = author
        except Exception as e:
            # author = nodes[i].xpath('.//span[@class="profileCardAvatarThumb"]/text()')[1]
            # # print(author)
            # # breakpoint()
            # author = author.replace('\n','').strip()
            # dic_data['author'] = author
            print(e)
        #状态

        try:
            type = nodes[i].xpath('.//td[5]/span[2]/span/text()')[0]
            type  = type.replace('\n','').strip()
            dic_data['type '] = type
        except Exception as e:
            type = nodes[i].xpath('.//td[5]/span[2]/text()')[0]
            type = type.replace('\n', '').strip()
            dic_data['type '] = type

        try:
            #未读帖子
            un_read = nodes[i].xpath('.//td[6]/span[2]/a/span/text()')[0]
            un_read = un_read.replace('\n','').strip()
            dic_data['un_read'] = un_read
        except Exception as e:
            un_read = nodes[i].xpath('.//td[6]/span[2]/span/text()')[0]
            un_read = un_read.replace('\n','').strip()
            dic_data['un_read'] = un_read

        #对我回复
        try:
            response = nodes[i].xpath('.//td[7]/span[2]/span/text()')[0]
            response = response.replace('\n','').strip()
            dic_data['response'] = response
        except Exception as e:
            print(e)

        #帖子数量
        try:
            bbs_count = nodes[i].xpath('.//td[8]/span[2]/span/text()')[0]
            bbs_count = bbs_count.replace('\n', '').strip()
            dic_data['bbs_count'] = bbs_count
        except Exception as e:
            print(e)

        # print(dic_data)
        # breakpoint()
        p.insert(dic_data)
        print('{} INSERT DB INTO MONGODB SUCCESSFULLY! {}'.format('-'*20,'-'*20))
        # time.sleep(1.5)

def main():
    p = mongo()
    url = 'https://bb.cuhk.edu.cn/webapps/discussionboard/do/forum?action=list_threads&filterAction=ALL&showAll=true&course_id=_3971_1&nav=discussion_board_entry&conf_id=_9385_1&forum_id=_9484_1'
    html = get_response(url)
    handle_html(html,p)

if __name__ == '__main__':
    main()