#!/usr/bin/python3
#-*- coding:utf-8 -*-
# Author : Mongoole
# Date : 2021/6/16

import io
import sys
import time,datetime
import re
import requests
import pymongo
from fake_useragent import UserAgent

sys.stdout = io.TextIOWrapper(sys.stdout.buffer,encoding='gbk') #改变标准输出的默认编码

def mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    #选择数据库
    db = client.foregin
    #选择操作的集合
    p = db.vk

    return p


def get_response(url,offset):

    ua = UserAgent().random
    headers = {
        'User-Agent': ua,
    }

    data = {
            "act":"get_wall",
            "al":"1",
            "fixed":"",
            "offset":offset,
            "onlyCache":"false",
            "owner_id":"-17842936",
            "type":"own",
            "wall_start_from":offset
    }

    try:
        response = requests.post(url, headers=headers,data=data)
        if response.status_code == 200:
            html = response.text
            # print(html)
            # breakpoint()
            return html
    except Exception as e:
        print(e)


def get_data(response, p):

    sss = response.replace('\\', '').replace("&#26152;", "昨").replace("&#22825;", "天")\
        .replace("&#26376;", "月").replace("&#26085;", "日").replace('\n','')

    sss_pt = re.findall(r'<span class="rel_date">(.*?)</span>', sss)
    # print(sss_pt)


    sss_link = re.findall(r'<a  class="post_link"  href=(.*?)  onclick=', sss)
    # sss_link = re.findall(r'<div class="post_date">(.*?)</div>',sss)
    # print(sss_link)

    sss_text = re.findall(r'<div class="wall_post_text">(.*?)</div>', sss)
    # print(sss_text)

    #Get The Source Video Information
    # sss_source_video = re.findall(r'<div class="post_video_views_count">(.*?)</div>',sss)

    sss_count = re.findall(r'<div class="like_button_count">(.*?)</div>', sss)
    # print(sss_count)
    sss_like_count = sss_count[::2]
    sss_share_count = sss_count[1::2]

    sss_like_views = re.findall(r'<div class="like_views _views" onmouseover="Likes.updateViews(.*?);">(.*?)</div>',
                                sss)
    # print(sss_like_views)

    # print(list(zip(sss_pt, sss_link, sss_text, sss_like_count, sss_share_count, sss_like_views)))
    results = list(zip(sss_pt, sss_link, sss_text, sss_like_count, sss_share_count, sss_like_views))

    # print(results)
    # breakpoint()
    today = time.strftime('%Y-%m-%d',time.localtime(time.time()))
    yesterday = (datetime.datetime.now() - datetime.timedelta(days=1)).strftime('%Y-%m-%d')
    year = time.strftime('%Y-',time.localtime(time.time()))

    for item in results:
        data_dict = {}

        pt = item[0]
        if '今' in pt:
            data_dict['article_pub'] = today + pt

        elif "昨" in pt:
            pt = pt.replace('昨天',yesterday)
            data_dict['article_pub'] = pt

        elif "2020" or "2019" or "2018" or "2017" in pt:
            data_dict['article_pub'] = pt

        else:
            data_dict['article_pub'] = year + pt

        data_dict['article_url'] = 'https://vk.com' + item[1].replace('"','')

        if '<img class="emoji"' in item[2]:
            ss = re.search(r'<img (.*?)>', item[2])
            ss_result = ss.group()
            ss = sss.replace(ss_result, '')
            data_dict['article_text'] = ss.replace(ss_result,'').replace('br','').replace('</div>','').replace('<>','')
        else:
            data_dict['article_text'] = item[2].replace('<br>','')

        data_dict['like_count'] = item[3]
        data_dict['share_count'] = item[4]
        data_dict['like_views'] = item[5][1]
        data_dict['unique_id'] = item[5][0]
        # print(data_dict)
        p.insert(data_dict)


def main():
    p = mongo()
    url = 'https://vk.com/al_wall.php?act=get_wall'
    for off in range(1,500):
        offset = 10 * int(off)
        response = get_response(url,offset)
        get_data(response, p)
        print("{} Page {} 完成{}".format('-', off, '-'))
        time.sleep(20)


if __name__ == '__main__':
    main()