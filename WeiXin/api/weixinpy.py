
from api import *


class WeiXin:
    def __init__(self, uin):
        self.uin = uin

    def get_history_articles(self, biz, key):
        return get_history_api(biz=biz, uin=self.uin, key=key, offset=0)

    def get_article_comment(self, biz, comment_id, key):
        return get_article_comments_api(biz=biz, comment_id=comment_id, uin=self.uin, key=key)

    def get_article_detail(self, biz, key, comment_id, **kwargs):
        return get_article_read_like_api(uin=self.uin, biz=biz, key=key, comment_id=comment_id, **kwargs)

    def auto(self, biz, key):
        histories = self.get_history_articles(biz, key)
        for article_item in histories["results"]["article_infos"]:
            article_url = article_item["article_content_url"]
            comment_id = get_article_comment_id_api(article_url)
            print(comment_id, article_url)
            print(self.get_article_comment(biz, comment_id, key))
            print(self.get_article_detail(biz, key, comment_id, **split_article_url2mis(article_url)))


if __name__ == '__main__':
    test_key = "1528f3467aaa6c49234737203ef88139c79995c87fba170db5f156891a53f312c54c42b19c25fb496da5fe039262cc8f07792fb03b43fb9202737736bc9cfa5990421f96a7f4b1da06b84cbbe97d5784"
    weixin = WeiXin("ODQwNTQ2NjEx")
    weixin.auto("MzA4NTI0MTg2Mg==", test_key)
