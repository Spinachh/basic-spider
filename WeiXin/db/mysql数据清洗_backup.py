import time
import pymysql
from lxml import etree
import csv
import re

#2019-09-30的时间戳时间：1569772800.0，转换方式：time.mktime(time.strptime('2019-09-30','%Y-%m-%d'))
#2020-01-01的时间戳时间：1577808000,2020-04-04的时间戳：1585929600

def operate_mysql():
    # db =pymysql.connect("127.0.0.1","root","mongoole","weixin_spider")
    db =pymysql.connect("127.0.0.1","root","","weixin_spider")

    cursor = db.cursor()
    '''
    cursor.execute("select article_title,article_author,article_publish_time,"
                   "article_copy_right,article_digest,"
                   "comment_count,read_count,"
                   "like_count,article_content_url,"
                   "article_html,account_id from wx_article where  account_id  and article_publish_time between 1577808000 and 1585929600")
    '''
    cursor.execute(" select account_name,article_title,article_author,"
                   "article_publish_time,article_copy_right,article_digest,"
                   "article_html,article_content_url,read_count,like_count,digg_count,"
                   "new_comment_count from wx_account left join  wx_article on "
                   "wx_account.id=wx_article.account_id where wx_account.id between 1 and 332;")


    # data = cursor.fetchone()  #查询一条测试使用
    all_data = cursor.fetchall()
    return all_data

def handle_data(all_data):
    file_name = time.strftime("%Y-%m-%d", time.localtime(time.time()))
    with open('kexie-third-quarter-weixin-content-%s.csv'%file_name, 'a', newline='', encoding='utf-8-sig') as f:
        f_csv = csv.writer(f)
        f_csv.writerow(('account_name','article_title','article_author','article_publish_time',
                        'article_copy_right','article_digest','article_url',
                        'read_count','like_count','comment_count','article_type',
                        'article_content'))        #csv的title头

        for data in all_data:
            account_name = data[0]
            content_title = data[1]
            content_author = data[2]
            if data[3]:
                stamp_publish_time = data[3]
                str_publish_time = time.localtime(int(stamp_publish_time))
                content_publish_time = time.strftime("%Y-%m-%d %H:%M:%S",str_publish_time)
            else:
                content_publish_time = None
            content_copy_right = data[4]        #1代表原创，0代表非原创
            content_digest = data[5]
            content_comment_count = data[10]
            content_read_count = data[8]
            content_like_count = data[9]
            content_url = data[7]
            try:
                selector = etree.HTML(data[6])
            except:
                print(data[1])

            result = selector.xpath('//*[@id="js_content"]//p//text()')     #获取的文字内容
            if not result or result[0] == ' ':
                result = selector.xpath('//*[@id="js_content"]//section//text()')  # 获取的文字内容

            img_data = selector.xpath('//*[@id="js_content"]//img')
            video_data1 = selector.xpath('//*[@class="video_iframe rich_pages"]')
            video_data2 = selector.xpath('//*[@class="js_tx_video_container"]')

            if len(img_data) >= 3 and result and video_data1 or video_data2:
                content_type = 'img_text_viedo'
            elif len(img_data) >= 3 and len(result) >= 2:
                content_type = 'img_text'
            elif len(img_data) >= 3 and video_data1 or video_data2:
                content_type = 'img_video'
            elif result and video_data1 or video_data2:
                content_type = 'text_video'
            elif len(img_data) < 3 and result and not video_data1 and not video_data2:
                content_type = 'text'
            elif not result and not video_data1 or not video_data2:
                content_type = 'img'
            elif not result and not img_data:
                content_type = 'video'

            # content_text = "".join([x.strip() for x in result])
            if data[6]:
                html = data[6]
                dr = re.compile(r'<[^>]+>', re.S)
                content_text = dr.sub('',html).strip()

            data_lst = [account_name,content_title,content_author,content_publish_time,content_copy_right,
                        content_digest,content_url,content_read_count,content_like_count,
                        content_comment_count,content_type,content_text]

            f_csv.writerow(data_lst)

if __name__ == '__main__':
    all_data = operate_mysql()
    handle_data(all_data)