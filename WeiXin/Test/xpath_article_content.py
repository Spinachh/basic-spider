from lxml import etree

with open('HTML/weixin_content2.html',mode='r',encoding='utf-8') as f:
    content = f.read()
'''
print(type(content))
result = re.findall('<span style=".*">(.*?)</span>',content)
print(result)
'''

selector = etree.HTML(content)
# result = selector.xpath('//*[@id="js_content"]//p/span/text()')   #获取的文字较少
result = selector.xpath('//*[@id="js_content"]//p//text()')     #获取的文字内容比较全面
data = "".join([x.strip() for x in result])
# print(data)
