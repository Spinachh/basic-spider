import re
import requests
from lxml import etree

url = "https://mp.weixin.qq.com/s?__biz=MzAwNTI0NTc0OQ==&mid=2650890864&idx=2&sn=d0048406d7aa7e97ed44a5e662375db3"

html = requests.get(url).text

selector = etree.HTML(html)
# result = selector.xpath('//*[@id="js_profile_qrcode"]/div/p[2]/span/text()')
# result = re.findall("<span class=\"profile_meta_value\">(.*?)</span>", html) #匹配微信号和简介
result = re.findall("<span class=\"profile_meta_value\">([\d\D]*?)</span>", html) #此处是为了匹配\t\n
print(result[0],result[1].strip())