1.启动顺序
1）启动redis#####G:\Wechat\withoutinstall\redis>redis-server.exe
2）windows本地配置代理，启动抓包程序###G:\Wechat\weixin-spider-master\tools>mitmdump -s ./addons.py
3）启动manage
4）启动监控程序wx_monitor
5）登录微信pc端，打开文件传输助手

修改版本（2021-04-01）：
1.wx_monitor.py
	获取文章评论id函数更新。

三种情况：
    a.只有文章获取评论id（id正常）
    b.有视频无文字获取评论id（id正常）
    c.有视频有文字获取评论id（此时有部分id为0）更新
2020-05
	.新增再看数据字段和全部评论量数据字段。like_num,new_commment_count
2022-07
	.新增ReadLike类中（自动采集）功能
		.当article_list=0时自动采集下一个账号
2022-08-26
	.新增ReadLikeReload类
		.一定时间内重新获取文章阅读等的数据，缩短数据误差
2022-10-13
	.新增打赏数据字段.reward_total_count
