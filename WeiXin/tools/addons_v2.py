
import json
import re
from urllib.parse import unquote
import hashlib
import logging
import mitmproxy as mp
import redis


class WeiXinProxy:
    WX_REDIS_CONFIG = {
        'host': 'localhost',
        'password': None,
        'port': 6379,
        'db': 0,
        'decode_responses': True,
    }
    redis_server = redis.StrictRedis(connection_pool=redis.ConnectionPool(**WX_REDIS_CONFIG))

    def __init__(self):
        pass

    def uin_md5(self, uin):
        if "%" in uin:
            uin = self.uin_md5(unquote(uin))
        return uin

    def request(self, flow: mp.http.HTTPFlow):
        if flow.request.host == "mp.weixin.qq.com":
            url_path = flow.request.path
            if "uin=" in url_path and "key=" and "appmsg_token" in url_path:
                appmsg_token = re.search(r"appmsg_token=([^&]+)&?", url_path).group(1)
                biz = self.uin_md5(re.search(r"__biz=([^&]+)&?", url_path).group(1))
                key = re.search(r"key=([^&]+)&?", url_path).group(1)
                uin = self.uin_md5(re.search(r"uin=([^&]+)&?", url_path).group(1))
                # new_add_code(get the comment_user_nickname)
                try:
                    pass_ticket = re.search(r"pass_ticket=([^&]+)&?", url_path).group(1)
                except:
                    pass_ticket = None
                try:
                    exportkey = re.search(r"exportkey=([^&]+)&?", url_path).group(1)
                except:
                    exportkey = None
                hash_key = hashlib.md5(biz.encode("utf-8")).hexdigest()
                print("抓到了：", hash_key, biz, uin, key, pass_ticket, appmsg_token)

                if not self.redis_server.exists(hash_key):
                    self.redis_server.set(hash_key, json.dumps({
                        "uin": uin,
                        "key": key,
                        "pass_ticket": pass_ticket,
                        "appmsg_token": appmsg_token,
                    }, ensure_ascii=False))


addons = [
    WeiXinProxy()
]

if __name__ == "__main__":
    pass
    # WeiXinProxy()