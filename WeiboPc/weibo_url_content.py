# -*-coding:utf-8 -*-
# Project:
# Author:mongoose
# Date:2022/7/9

import io
import sys
import time
import re
import requests
import pymongo
import logging

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')  # change the default encoding of standard output
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


class WeiBoContent(object):
    @staticmethod
    def mongodb():
        # 连接数据库mongodb
        client = pymongo.MongoClient(host='127.0.0.1', port=27017)
        # 选择数据库
        db = client.weibo_publish_content
        # 选择操作的集合
        p = db.weibo_content_2022_07_09
        return p

    @staticmethod
    def get_response(url):
        headers = {
            'headers': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36",
            'Cookie': 'SINAGLOBAL=8236036726604.133.1584281383998; YF-V5-G0=125128c5d7f9f51f96971f11468b5a3f; _s_tentry=-; Apache=7895264763162.955.1590251114205; Ugrow-G0=cf25a00b541269674d0feadd72dce35f; YF-V-WEIBO-G0=b09171a17b2b5a470c42e2f713edace0; XSRF-TOKEN=dELDzROpFRQaU8wp4tGcHZul; login_sid_t=8f48547ac1fa1da6f8a43cb2f8a6f1fd; cross_origin_proto=SSL; TC-V5-G0=799b73639653e51a6d82fb007f218b2f; TC-Page-G0=aadf640663623d805b6612f3dfe1e2c0|1598141201|1598141190; SSOLoginState=1599879597; YF-Page-G0=aedd5f0bc89f36e476d1ce3081879a4e|1600597927|1600597927; TC-V-WEIBO-G0=b09171a17b2b5a470c42e2f713edace0; UOR=,,login.sina.com.cn; ULV=1652627516855:1:1:1:7895264763162.955.1590251114205:; SUBP=0033WrSXqPxfM725Ws9jqgMF55529P9D9WFjsmyPTykYiKzxz71MaOPT5JpX5KMhUgL.Fo-RSh5XSKMpSKB2dJLoIEnLxK-LBKML1K2LxK-L1h5LB-2LxK-LBoMLBoBLxK-LB-qL1h9zdc4r; ALF=1688897099; SCF=Aj5xopQKjYmxZiKNL0XbTTyRR7uo1-uviJIU59Rql3n0cMS_jSRhVS4DmQxUVC6FmqQ4gvXaPdtcqG-CdgJuvmc.; SUB=_2A25PzSKbDeRhGeNG71IV9SnNzjiIHXVsuxNTrDV8PUNbmtANLWnVkW9NS0z2xJhVRHI-UH68CYHMNjKppPTY--yt; wvr=6; wb_view_log_5840457154=1440*25601; PC_TOKEN=f2f621e6a1; webim_unReadCount={"time":1657372336786,"dm_pub_total":0,"chat_group_client":0,"chat_group_notice":0,"allcountNum":22,"msgbox":0}',
        }
        html_response = requests.get(url, headers=headers)
        try:
            if html_response.status_code == 200:
                response = html_response.text
                response_text = response.replace('\\', '')
                return response_text
        except Exception as e:
            print(e)

    @staticmethod
    def get_urls():
        with open(r'weibo_content_url', 'r', encoding='utf-8') as f:
            lines = f.readlines()
            return lines

    def get_data(self, url, response):
        data_dict = {}
        # content = re.findall(r'nick-name="百验易经风水" >(.*?)</div>', response, re.M|re.S)
        content = re.findall(r'<div class="WB_text W_f14" node-type="feed_list_content"(.*?)</div>'
                             , response, re.M | re.S)[0]
        # data = content[0].strip().replace('<br>', '').replace('n', '').replace('</div>', '').replace(' ', '')
        dr = re.compile(r'<[^>]+>', re.S)
        content_text = dr.sub('', content).strip()
        data_dict["url"] = url
        data_dict["data"] = content_text
        # print(content_text)
        # breakpoint()
        self.mongodb().insert_one(data_dict)
        logging.warning('Url :{} was finished!'.format(url))

    def start_run(self):
        for line in self.get_urls():
            url = line.strip()
            # url = 'https://weibo.com/5453051735/LB8X6BW7w'
            # print('print url:{}'.format(url))
            # breakpoint()
            response = self.get_response(url)
            self.get_data(url, response)
            time.sleep(1)


if __name__ == '__main__':
    wbcspider = WeiBoContent()
    wbcspider.start_run()