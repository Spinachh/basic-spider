#!/usr/bin/python3
#-*- coding:utf-8 -*-
# Author : Mongoole
# Date : 2020/12/24 

import time
import pymongo
import pandas as pd

myclient = pymongo.MongoClient("mongodb://127.0.0.1:27017")
mydb = myclient["WeiBo_Search"]

mycol = mydb["weibo_direction_time3"]
mydoc = mycol.find()
# myquery1 = {"user_id":{"$eq":"6506770761"}}
# mydoc = mycol.find({"$and":[{"user_id":{"$eq":"6506770761"}},{"created_at":{"$gte":"2019-10-01"}},{"created_at":{"$lte":"2019-12-01"}}]})
# data = pd.DataFrame(list(mydoc))

# mydoc = mycol.find({"$and": [{"user_id": {"$eq": line}}, {"created_at": {"$gte": "2019-09-30"}},{"created_at": {"$lte": "2020-01-01"}}]})
file_time = time.strftime("%Y-%m-%d",time.localtime(time.time()))
data = pd.DataFrame(list(mydoc))
data.to_csv(r'content_result/新冠病毒-content3-%s.csv'%file_time, encoding='utf-8')
