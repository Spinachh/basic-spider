# !/usr/bin/python3
# -*- coding:utf-8 -*-
# Author : Mongoose
# Date : 2022-08-08

import io
import sys
import time
import datetime
import re
import logging
import pymongo
import requests
import cchardet
from lxml import etree

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')  # change the default encoding of standard output


class DirectionKeyWordWeiBO(object):
    file_name = time.strftime('%Y-%m-%d', time.localtime(time.time()))
    logging.basicConfig(level=logging.WARN,
                        format='%(asctime)s - %(levelname)s: %(message)s')

    def __init__(self, keywords, st_time, en_time, Cookie, user_flag):
        self.keywords = keywords
        self.start_time = st_time
        self.end_time = en_time
        self.cookie = Cookie
        self.pages = (page for page in range(1, 51))

    @staticmethod
    def mongo():
        # 连接数据库mongodb
        client = pymongo.MongoClient(host='127.0.0.1', port=27017)
        # 选择数据库
        db = client.weibo_pc_content
        # 选择操作的集合
        user_keyword_content = db.weibo_direction_time_test
        user_general_information = db.weibo_user_general_information
        user_office_information = db.weibo_user_office_information

        return user_keyword_content, user_general_information, user_office_information

    def get_page_response(self, keyword, direction_time, page):
        url = 'https://s.weibo.com/weibo?q='

        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36',
            'Cookie': self.Cookie,
            'Referer': 'https://s.weibo.com/weibo?q=%E6%96%B0%E5%86%A0%E7%97%85%E6%AF%92&typeall=1&suball=1'
                       '&timescope=custom:{}&Refer=SWeibo_box'.format(direction_time),

        }

        params = {
            'q': keyword,
            'typeall': 1,
            'suball': 1,
            # 'timescope': 'custom:2020-01-23-0:2020-01-24-0',
            'timescope': 'custom:{}'.format(direction_time),
            'Refer': 'g',
            'page': page,
        }

        try:
            response = requests.get(url, headers=headers, params=params)
            if response.status_code == 200:
                response.encoding = cchardet.detect(response.content).get('encoding')
                response_text = response.text
                return response_text
        except Exception as e:
            print('Get Page Response Error ! {}'.format(e))

    def get_page_data(self, response, keyword, userflag):
        user_keyword_content = self.mongo()[0]

        selector = etree.HTML(response)  # etree parse response html
        nodes = selector.xpath('//*[@id="pl_feedlist_index"]/div[2]/div')

        for i in range(len(nodes)):
            publish_info = {}

            # user account name
            # user_name = nodes[i].xpath(".//div[@class='feed_content wbcon']/a[@class='W_texta W_fb']").text
            try:
                user_name = nodes[i].xpath(".//div[@class='content']/p[@class='txt']/@nick-name")[0]
            except IndexError:
                user_name = ''

            publish_info['user_name'] = user_name

            # user page url
            # user_page_url = nodes[i].xpath(".//*[@class='avator']/a/@href").get_attribute("href")
            try:
                user_page_url = nodes[i].xpath(".//div[@class='content']/p[@class='from']/a[1]/@href")[0]
            except IndexError:
                user_page_url = ''

            publish_info['user_page'] = user_page_url

            # publish content
            # user_publish_content = nodes[i].xpath(".//div[@class='content']/p[@class='txt']/text()")[0]

            result_text = nodes[i].xpath(".//div[@class='content']/p[@class='txt']/em/text()|"
                                         ".//div[@class='content']/p[@class='txt']/a/text()|"
                                         ".//div[@class='content']/p[@class='txt']/text()")
            # long text part folded
            result_long_text = nodes[i].xpath(
                ".//div[@class='content']/p[@node-type='feed_list_content_full']/em/text()|"
                ".//div[@class='content']/p[@node-type='feed_list_content_full']/a/text()|"
                ".//div[@class='content']/p[@node-type='feed_list_content_full']/text()")

            if result_text:
                user_publish_content = "".join([x.strip().replace('\n', '') for x in result_text])
                publish_info['content'] = user_publish_content
                if keyword in user_publish_content:
                    publish_info['key'] = 1
                else:
                    publish_info['key'] = 0

            elif result_long_text:
                user_publish_content = "".join([x.strip().replace('\n', '') for x in result_long_text])
                publish_info['content'] = user_publish_content
                if keyword in user_publish_content:
                    publish_info['key'] = 1
                else:
                    publish_info['key'] = 0

            # publish time
            user_publish_time = nodes[i].xpath(".//div[@class='content']/p[@class='from']/a[1]/text()")[0]
            user_publish_time = user_publish_time.strip().replace('\n', '')
            # publish_info['publish_time'] = '2020年' + user_publish_time    #冒出的问题

            if '年' not in user_publish_time:
                year = str(time.strftime('%Y', time.localtime(time.time())))
                publish_info['publish_time'] = year + '年' + user_publish_time
            else:
                publish_info['publish_time'] = user_publish_time

            # publish tools
            try:
                content_source = nodes[i].xpath(".//div[@class='content']/p[@class='from']/a[2]/text()")[0]
            except IndexError:
                content_source = ''

            publish_info['tools'] = content_source

            # forward count
            try:
                content_forward_text = nodes[i].xpath(".//div[@class='card-act']/ul/li[2]/a/text()")[0].strip()
                if content_forward_text == '转发':
                    forward_count = 0
                else:
                    forward_count = int(content_forward_text.split('转发')[1])
            except IndexError:
                forward_count = 0

            publish_info['forward_count'] = str(forward_count)

            # comment count
            try:
                content_comment_text = nodes[i].xpath(".//div[@class='card-act']/ul/li[3]/a/text()")[0].strip()
                if content_comment_text == '评论':
                    comment_count = 0
                else:
                    comment_count = int(content_comment_text.split('评论')[-1])
            except IndexError:
                comment_count = 0
            publish_info['comment_count'] = str(comment_count)

            # like count
            try:
                content_like_text = nodes[i].xpath(".//div[@class='card-act']/ul/li[4]/a/em/text()")[0]
                if content_like_text == '':
                    like_count = 0
                else:
                    like_count = int(content_like_text)
            except IndexError:
                like_count = 0
            publish_info['like_count'] = str(like_count)

            # user publish content url or comment url
            try:
                content_url = nodes[i].xpath(".//div[@class='content']/p[@class='from']/a[1]/@href")[0]
            except IndexError:
                content_url = ''

            publish_info['url'] = str(content_url)
            publish_info['keyword'] = keyword

            user_keyword_content.insert_one(publish_info)

            # UserAccount DetailInformation
            if userflag == 1:
                user_uid = user_page_url.split('/')[-2]
                user_publish_content_url = 'https:' + user_page_url

                flag, user_page_id, user_home_follow_information = self.get_user_page_id(user_publish_content_url)
                # logging.warning('User Flag: {} UserPageId: {}'.format(flag, user_page_id))
                self.get_user_information(flag, user_page_id, user_uid, keyword, user_home_follow_information)

    def get_user_information(self, flag, user_page_id, user_uid, keyword, user_home_follow_information):
        user_general_information, user_office_information = self.mongo()[1:]

        if flag == 0:
            user_home_url = 'https://weibo.com/p/{}/info?mod=pedit_more'.format(user_page_id)
            user_home_content = self.get_general_user_home(user_home_url, user_uid)
            if len(user_home_follow_information) == 3:
                user_home_content['follow'] = user_home_follow_information[0]
                user_home_content['followers'] = user_home_follow_information[1]
                user_home_content['publish_counts'] = user_home_follow_information[2]
                user_home_content['home_url'] = user_home_url
                user_home_content['tag'] = [keyword]
                user_home_content['ico'] = 'general__ico'
                user_general_information.insert_one(user_home_content)

        elif flag == 1:
            user_home_url = 'https://weibo.com/{}/about'.format(user_uid)
            user_home_content = self.get_blue_user_home(user_home_url, user_uid)
            if len(user_home_follow_information) == 3:
                user_home_content['follow'] = user_home_follow_information[0]
                user_home_content['followers'] = user_home_follow_information[1]
                user_home_content['publish_counts'] = user_home_follow_information[2]
                user_home_content['home_url'] = user_home_url
                user_home_content['tag'] = [keyword]
                user_home_content['ico'] = 'office_ico'
                user_office_information.insert_one(user_home_content)

    def get_user_page_id(self, user_publish_content_url):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36',
            'Cookie': self.Cookie,
        }

        while True:
            user_detail_html = requests.get(user_publish_content_url, headers=headers).text
            user_detail_html_re = user_detail_html.replace('\\', '').replace('rn', '').replace(' ', '')
            try:
                user_page_id = re.findall(r"\$CONFIG\['page_id'\]\='(.*?)';", user_detail_html_re, re.M | re.S)[0]
            except IndexError:
                continue

            time.sleep(0.2)
            user_home_follow_information = re.findall(r'class="W_f\d+">(.*?)</strong>',
                                                      user_detail_html_re, re.M | re.S)

            # user home follow information 出现过不能正常获取的情况，还需观察
            # logging.warning('User Home Page BasicInformation:{}'.format(user_home_follow_information))

            if 'W_icon icon_approve_co' and '微博官方认证' in user_detail_html:  # office ico
                flag = 1
                return flag, user_page_id, user_home_follow_information

            elif 'W_f14 W_fb S_txt1' and '微博官方认证' in user_detail_html:  # office ico
                flag = 1
                return flag, user_page_id, user_home_follow_information

            else:
                flag = 0
                return flag, user_page_id, user_home_follow_information

    def get_general_user_home(self, user_home_url, user_id):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36',
            'Cookie': self.Cookie,
            'Referer': 'https://weibo.com/{}/Lqh77beSc?refer_flag=1001030103_&type=comment'.format(user_id),
        }
        user_detail_html = requests.get(url=user_home_url, headers=headers).text
        user_detail_html_re = user_detail_html.replace('\\', '').replace('rn', '').replace(' ', '')
        # user_basic_header = re.findall(r'class="pt_titleS_txt2">(.*?)</span>', user_detail_html_re, re.M | re.S)
        # user_basic_information = re.findall(r'class="pt_detail">(.*?)</span>', user_detail_html_re, re.M | re.S)
        # print(user_detail_html_re)
        # Block 1 BasicInformation
        # breakpoint()
        try:
            user_ip_location = re.findall(r'ipRegion">(.*?)<', user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_ip_location = ''
        try:
            user_gender = \
                re.findall(r'>性别：</span><spanclass="pt_detail">(.*?)</span>', user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_gender = ''

        try:
            user_nickname = re.findall('>昵称：</span><spanclass="pt_detail">(.*?)</span>',
                                       user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_nickname = ''

        try:
            user_true_name = re.findall(r'>真实姓名：</span><spanclass="pt_detail">(.*?)</span>', user_detail_html_re,
                                        re.M | re.S)[0]
        except IndexError:
            user_true_name = ''

        try:
            user_location = re.findall(r'>所在地：</span><spanclass="pt_detail">(.*?)</span>',
                                       user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_location = ''

        try:
            sex_content = re.findall(r'>性取向：</span><spanclass="pt_detail">(.*?)</span>',
                                     user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            sex_content = ''

        try:
            user_single_flag = re.findall(r'>感情状况：</span><spanclass="pt_detail">(.*?)</span>',
                                          user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_single_flag = ''

        try:
            user_birthday = re.findall(r'>生日：</span><spanclass="pt_detail">(.*?)</span>',
                                       user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_birthday = ''

        try:
            user_blood = re.findall(r'>血型：</span><spanclass="pt_detail">(.*?)</span>',
                                    user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_blood = ''

        try:
            user_profile = re.findall(r'>简介：</span><spanclass="pt_detail">(.*?)</span>',
                                      user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_profile = ''

        try:
            user_register = re.findall(r'>注册时间：</span><spanclass="pt_detail">(.*?)</span>',
                                       user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_register = ''

        try:
            user_blog_url = re.findall(r'>博客：</span><ahref="(.*?)">',
                                       user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_blog_url = ''

        try:
            user_domain_url = re.findall(r'>个性域名：</span><spanclass="pt_detail"><ahref="(.*?)">',
                                         user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_domain_url = ''

        try:
            user_msn_address = re.findall(r'>邮箱：</span><spanclass="pt_detail">(.*?)</span>',
                                          user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_msn_address = ''

        try:
            user_qq_number = re.findall(r'>QQ：</span><spanclass="pt_detail">(.*?)</span>',
                                        user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_qq_number = ''

        # Block 2 University
        try:
            user_university_content = re.findall(r'>教育信息</h2>(.*?)<!--//模块', user_detail_html_re, re.M | re.S)[0]
            # print(user_university_content)
            user_university_part1 = re.findall(r'loc=infedu">(.*?)</span', user_university_content)
            user_university_content = user_university_part1
        except IndexError:
            user_university_content = ''

        # Block 3 Company

        try:
            user_work_company = re.findall(r'>公司：</span><spanclass="pt_detail">(.*?)<br/>',
                                           user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_work_company = ''

        try:
            user_work_company_department = re.findall(r'<br/>职位：(.*?)</span>',
                                                      user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_work_company_department = ''

        try:
            user_work_time = re.findall(r'>工作时间：(.*?)</span>',
                                        user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_work_time = ''

        try:
            user_work_location = re.findall(r'>工作地点：(.*?)</span>',
                                            user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_work_location = ''

        # Block 4 Labels
        try:
            user_label_content = re.findall(r'<emclass="S_bg2_br"></em></span>(.*?)</a>',
                                            user_detail_html_re, re.M | re.S)
            user_label_content = user_label_content[2:]
        except IndexError:
            user_label_content = ''

        user_basic_header = ['user_nickname', 'user_true_name', 'user_location', 'user_ip_location',
                             'user_gender', 'sex_content', 'user_single_flag', 'user_birthday',
                             'user_blood', 'user_profile', 'user_register',
                             'user_blog_url', 'user_domain_url', 'user_msn_address',
                             'user_msn_address', 'user_qq_number', 'user_university_content',
                             'user_work_company', 'user_work_company_department', 'user_work_time',
                             'user_work_location', 'user_label_content', '', '']
        user_basic_information = [user_nickname, user_true_name, user_location, user_ip_location,
                                  user_gender, sex_content, user_single_flag, user_birthday,
                                  user_blood, user_profile, user_register,
                                  user_blog_url, user_domain_url, user_msn_address,
                                  user_msn_address, user_qq_number, user_university_content,
                                  user_work_company, user_work_company_department,
                                  user_work_time, user_work_location, user_label_content]

        user_information = dict(zip(user_basic_header, user_basic_information))
        return user_information

    def get_blue_user_home(self, user_home_url, user_id):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36',
            'Cookie': self.Cookie,
            'Referer': 'https://weibo.com/{}/Lqh77beSc?refer_flag=1001030103_&type=comment'.format(user_id),
        }
        user_detail_html = requests.get(url=user_home_url, headers=headers).text
        user_detail_html_re = user_detail_html.replace('\\t', '').replace('\\n', '').replace('\\', '')
        # print(user_detail_html_re)
        # Blue User RegisterBasicInformation

        try:
            account_title = re.findall(r'<em title= (.*?)class=', user_detail_html_re, re.M | re.S)[0].strip()
        except IndexError:
            account_title = ''

        try:
            profession_category = re.findall(r'from=profile&wvr=6">(.*?)</a>',
                                             user_detail_html_re, re.M | re.S)[0].replace('t', '')
        except IndexError:
            profession_category = ''

        # Blue User Basic Profile
        try:
            account_profile = re.findall(r'<p class="p_txt">(.*?)</p>r</div>',
                                         user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            account_profile = ''
        try:
            user_call_people = re.findall(r'>联系人：</span>r<span class="pt_detail">(.*?)</span>',
                                          user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_call_people = ''

        try:
            user_phone_number = re.findall(r'S_txt2">电话：</span>r<span class="pt_detail">(.*?)</span>',
                                           user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_phone_number = ''

        try:
            user_msn_address = re.findall(r'S_txt2">邮箱：</span>r<span class="pt_detail">(.*?)</span>',
                                          user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_msn_address = ''

        try:
            user_friend_url_str = re.findall(r'S_txt2">友情链接：</span>r<span class="pt_detail"><a href=(.*?)</span>r</li>',
                                             user_detail_html_re, re.M | re.S)[0]
            # print(user_friend_url_str)
            user_friend_url = re.findall(r'//(.*?) alt', user_friend_url_str)
        except IndexError:
            user_friend_url = ''

        try:
            user_ip_location = re.findall(r'class="pt_detail ipRegion">(.*?)<span',
                                          user_detail_html_re, re.M | re.S)[0]
        except IndexError:
            user_ip_location = ''

        user_basic_header = ['account_title', 'profession_category', 'account_profile',
                             'user_call_people', 'user_phone', 'user_msn_address',
                             'user_friend_url', 'user_ip_location', ]

        user_basic_information = [account_title, profession_category, account_profile,
                                  user_call_people, user_phone_number, user_msn_address,
                                  user_friend_url, user_ip_location]

        user_information = dict(zip(user_basic_header, user_basic_information))
        # logging.warning(user_information)
        return user_information

    def start_run(self):
        key_words = (key for key in self.keywords)
        days = (day for day in range(start_time, end_time))
        for keyword in key_words:
            for day in days:  # 2022-1-25 ~2022-04-25
                since = (datetime.datetime.now() - datetime.timedelta(days=day)).strftime("%Y-%m-%d-23")
                until = (datetime.datetime.now() - datetime.timedelta(days=day)).strftime("%Y-%m-%d-00")
                direction_time = str(until) + ":" + str(since)
                for page in self.pages:
                    response = self.get_page_response(keyword, direction_time, page)
                    if '未找到“{}”相关结果'.format(keyword) in response:
                        logging.warning('In Html element Has not found page data.')
                        break
                    self.get_page_data(response, keyword)
                    time.sleep(5)
                    logging.warning(
                        'KeyWord: {} DirectionTime :{} The Page {} finished.'.format(keyword, direction_time, page))


if __name__ == '__main__':
    keywords = ['空气污染']
    start_time = 1  # 1天前
    end_time = 5  # 前1-5
    Cookie = ''
    user_flag = 1   # flag 1 是采集用户信息
    direction_key_wb = DirectionKeyWordWeiBO(keywords, start_time, end_time, Cookie, user_flag)
    direction_key_wb.start_run()
