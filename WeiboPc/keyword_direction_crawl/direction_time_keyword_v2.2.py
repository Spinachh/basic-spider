# !/usr/bin/python3
# -*- coding:utf-8 -*-
# Author : Mongoose
# Date : 2022-05-28

import os
import io
import sys
import time
import datetime
import re
import logging
import pymongo
import requests
import lxml
from lxml import etree
from fake_useragent import UserAgent

logging.basicConfig(level=logging.WARN, format='%(asctime)s - %(levelname)s: %(message)s')
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')  # change the default encoding of standard output


def mongo():
    # 连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # 选择数据库
    db = client.weibo_pc_content
    # 选择操作的集合
    p = db.weibo_direction_time_20220712

    return p


def get_response(keyword, direction_time, page):
    ua = UserAgent().random

    # url = 'https://s.weibo.com/weibo?q={}&typeall=1&suball=1&timescope=custom:{}&Refer=SWeibo_box&page={}'.format(keyword,direction_time,page)
    url = 'https://s.weibo.com/weibo?q='

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36',
        'Cookie': '_s_tentry=weibo.com; Apache=2609702625353.072.1638340008644; SINAGLOBAL=2609702625353.072.1638340008644; ULV=1638340008647:1:1:1:2609702625353.072.1638340008644:; WBtopGlobal_register_version=2021120114; SSOLoginState=1642384589; UOR=,,login.sina.com.cn; wvr=6; SUBP=0033WrSXqPxfM725Ws9jqgMF55529P9D9WFjsmyPTykYiKzxz71MaOPT5JpX5KMhUgL.Fo-RSh5XSKMpSKB2dJLoIEnLxK-LBKML1K2LxK-L1h5LB-2LxK-LBoMLBoBLxK-LB-qL1h9zdc4r; ALF=1689146240; SCF=AgYQMOR3eoIWuR9J8pcSSuq-KHu_fXg-Utse5ZOmqHSWp1GN5yZ4XU7zkHWg9YvJE-5MV21sToG8PGU0xUDhpHY.; SUB=_2A25PyVBUDeRhGeNG71IV9SnNzjiIHXVsv8acrDV8PUNbmtB-LUzFkW9NS0z2xAcHD6wt-lEazc-PBMLcmbpvOwEB; PC_TOKEN=eae066d7e5; webim_unReadCount={"time":1657616455195,"dm_pub_total":1,"chat_group_client":0,"chat_group_notice":0,"allcountNum":15,"msgbox":0}',
        'Referer': 'https://s.weibo.com/weibo?q=%E6%96%B0%E5%86%A0%E7%97%85%E6%AF%92&typeall=1&suball=1'
                   '&timescope=custom:{}&Refer=SWeibo_box'.format(direction_time),

    }

    params = {
        'q': keyword,
        'typeall': 1,
        'suball': 1,
        # 'timescope': 'custom:2020-01-23-0:2020-01-24-0',
        'timescope': 'custom:{}'.format(direction_time),
        'Refer': 'g',
        'page': page,
    }

    # url2 = 'https://s.weibo.com/weibo?q=%E6%96%B0%E5%86%A0%E7%97%85%E6%AF%92&typeall=1&suball=1&timescope=custom:2020-03-02-0:2020-03-04&Refer=SWeibo_box&page=2'
    # print('zhe shi url1?:{}'.format(url))
    # print('zhe shi url2?:{}'.format(url2))
    # breakpoint()
    try:

        response = requests.get(url, headers=headers, params=params)
        # response = requests.get(url, headers=headers)
        if response.status_code == 200:
            response = response.text
            # print(response)
            return response
    except Exception as e:
        print('This is GET Response error ! {}'.format(e))


def handle_data(html, p, page, keyword):
    selector = etree.HTML(html)
    nodes = selector.xpath('//*[@id="pl_feedlist_index"]/div[2]/div')

    for i in range(len(nodes)):
        dic = {}

        # 博主昵称
        # BZNC = nodes[i].xpath(".//div[@class='feed_content wbcon']/a[@class='W_texta W_fb']").text
        BZNC = nodes[i].xpath(".//div[@class='content']/p[@class='txt']/@nick-name")[0]
        # print(BZNC)
        dic['user_name'] = BZNC
        # breakpoint()
        # 博主主页
        # BZZY = nodes[i].xpath(".//*[@class='avator']/a/@href").get_attribute("href")
        BZZY = nodes[i].xpath(".//div[@class='content']/p[@class='from']/a[1]/@href")[0]
        # print(BZZY)
        dic['user_page'] = BZZY

        # 微博内容
        # WBNR = nodes[i].xpath(".//div[@class='content']/p[@class='txt']/text()")[0]

        result1 = nodes[i].xpath(".//div[@class='content']/p[@class='txt']/em/text()|"
                                 ".//div[@class='content']/p[@class='txt']/a/text()|"
                                 ".//div[@class='content']/p[@class='txt']/text()")

        result2 = nodes[i].xpath(".//div[@class='content']/p[@node-type='feed_list_content_full']/em/text()|"
                                 ".//div[@class='content']/p[@node-type='feed_list_content_full']/a/text()|"
                                 ".//div[@class='content']/p[@node-type='feed_list_content_full']/text()")
        if result1:
            WBNR = "".join([x.strip().replace('\n', '') for x in result1])
            dic['content'] = WBNR
            if keyword in WBNR:
                dic['key'] = 1
            else:
                dic['key'] = 0

        elif result2:
            WBNR = "".join([x.strip().replace('\n', '') for x in result2])
            dic['content'] = WBNR
            if keyword in WBNR:
                dic['key'] = 1
            else:
                dic['key'] = 0

        # 发布时间
        FBSJ = nodes[i].xpath(".//div[@class='content']/p[@class='from']/a[1]/text()")[0]
        # print(FBSJ)
        FBSJ = FBSJ.strip().replace('\n', '')
        # dic['publish_time'] = '2020年' + FBSJ    #冒出的问题
        if '年' not in FBSJ:
            year = str(time.strftime('%Y',time.localtime(time.time())))
            dic['publish_time'] = year + '年' + FBSJ
        else:
            dic['publish_time'] = FBSJ

        # 微博来源
        try:
            WBLY = nodes[i].xpath(".//div[@class='content']/p[@class='from']/a[2]/text()")[0]
        except:
            WBLY = ''
        # print(WBLY)
        dic['tools'] = WBLY

        # 微博转发量
        try:
            ZF_TEXT = nodes[i].xpath(".//div[@class='card-act']/ul/li[2]/a/text()")[0]
            if ZF_TEXT == '':
                ZF = 0
            else:
                ZF = int(ZF_TEXT.split(' ')[1])
        except:
            ZF = 0
        # print(ZF)
        dic['repost_count'] = str(ZF)

        # 微博评论量
        try:
            PL_TEXT = nodes[i].xpath(".//div[@class='card-act']/ul/li[3]/a/text()")[0]  # 可能没有em元素
            if PL_TEXT == '':
                PL = 0
            else:
                PL = int(PL_TEXT.split(' ')[1])
        except:
            PL = 0
        # print(PL)
        dic['comment_count'] = str(PL)

        # 微博赞的量
        try:
            ZAN_TEXT = nodes[i].xpath(".//div[@class='card-act']/ul/li[4]/a/em/text()")[0]  # 可为空
            if ZAN_TEXT == '':
                ZAN = 0

            else:
                ZAN = int(ZAN_TEXT)
        except:
            ZAN = 0
        # print(ZAN)
        dic['digg_count'] = str(ZAN)

        # 微博url
        try:
            URL = nodes[i].xpath(".//div[@class='content']/p[@class='from']/a[1]/@href")[0]  # 可为空

        except:
            URL = ''

        dic['url'] = str(URL)

        dic['keyword'] = keyword
        # print(dic)
        p.insert(dic)


def main():
    p = mongo()
    # keywords = ['铁链女', '妇女拐卖', '杨某侠', '小花梅']
    keywords = ['盲盒玩具', '潮玩玩具']
    # since = (datetime.datetime.now() - datetime.timedelta(days=208)).strftime("%Y-%m-%d 23:59:00")
    # until = (datetime.datetime.now() - datetime.timedelta(days=1)).strftime("%Y-%m-%d 00:00:00")
    for keyword in keywords:
        for day in range(105, 193):   # 2022-1-25 ~2022-04-25
            since = (datetime.datetime.now() - datetime.timedelta(days=day)).strftime("%Y-%m-%d-23")
            until = (datetime.datetime.now() - datetime.timedelta(days=day)).strftime("%Y-%m-%d-00")
            direction_time = str(until) + ":" + str(since)
            for page in range(1, 51):
                html = get_response(keyword, direction_time, page)
                # print(html) #抱歉，未找到“小花梅”相关结果
                # breakpoint()
                if '未找到“{}”相关结果'.format(keyword) in html:
                    break
                handle_data(html, p, page, keyword)
                time.sleep(5)
                logging.warning('KeyWord: {} DirectionTime :{} The Page {} finished.'.format(keyword, direction_time, page))


if __name__ == '__main__':
    main()
