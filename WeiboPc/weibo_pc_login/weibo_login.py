# !/usr/bin/python3
# -*- coding:utf-8 -*-
# Author : mongooses
# Date : 2022/8/10 

import os
import io
import re
import sys
import time
import json
import pymongo
import asyncio
import cchardet
import requests
import logging
import execjs

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')  # 改变标准输出的默认编码
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def get_su_js(user_name):
    # user_name = 'mongoole@163.com'
    with open('loginss.js', encoding='utf-8') as f:
        js = f.read()
    login_js = execjs.compile(js)
    su = login_js.call('su_', user_name)
    return su


def get_prelogin_data(user_name):
    server_time = int(str(time.time())[0:14].replace('.', ''))
    su = get_su_js(user_name)

    prelogin_url = 'https://login.sina.com.cn/sso/prelogin.php?entry=weibo&callback=sinaSSOController.' \
                   'preloginCallBack&su={su}&' \
                   'rsakt=mod&checkpin=1' \
                   '&client=ssologin.js(v1.4.19)&_={server_time}'.format(su=su, server_time=server_time)
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36',
        'referer': 'https://weibo.com/',
    }
    data = {
        'entry': 'weibo',
        'callback': 'sinaSSOController.preloginCallBack',
        'su': su,
        'rsakt': 'mod',
        'checkpin': '1',
        'client': 'ssologin.js(v1.4.19)',
        '_': server_time,
    }
    prelogin_data = requests.get(prelogin_url, headers=headers, data=data).text
    prelogin_json_data = json.loads(re.findall(r'\((.*?)\)', prelogin_data)[0])
    # print(prelogin_json_data)
    # breakpoint()
    nonce = prelogin_json_data.get('nonce')
    pubkey = prelogin_json_data.get('pubkey')
    rsakv = prelogin_json_data.get('rsakv')

    return su, nonce, pubkey, rsakv


def get_sp_js(password, server_time, nonce, pubkey):
    with open('loginss.js', encoding='utf-8') as f:
        js = f.read()
    login_js = execjs.compile(js)
    sp = login_js.call('pwd', password, server_time, nonce, pubkey)
    return sp


def get_response(url, user_name, password):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36',
        'referer': 'https://login.sina.com.cn/signup/signin.php?entry=sso',

    }

    su, nonce, pubkey, rsakv = get_prelogin_data(user_name)
    server_time = int(time.time())
    sp = get_sp_js(password, server_time, nonce, pubkey)
    form_data = {
        'entry': 'sso',
        'gateway': 1,
        'from': 'null',
        'savestate': 30,
        'useticket': 0,
        'pagerefer': 'https://login.sina.com.cn/sso/login.php?client=ssologin.js(v1.4.19)',
        'vsnf': '1',
        'su': su,  # dynamic
        'service': 'sso',
        'servertime': server_time,  # dynamic
        'nonce': nonce,  # dynamic
        'pwencode': 'rsa2',
        'rsakv': '1330428213',
        'sp': sp,  # dynamic
        'sr': '1920*1080',
        'encoding': 'UTF-8',
        'cdult': '3',
        'domain': 'sina.com.cn',
        'prelt': '36',
        'returntype': 'TEXT',
    }

    print(form_data)
    breakpoint()
    try:
        response = requests.post(url, headers=headers, form_data=form_data)
        if response.status_code == 200:
            print(response.text)
            breakpoint()

    except Exception as e:
        print(e)


def get_data(response, p):
    data = response['data']
    goe_dict = {}


def main():
    url = ''
    response = get_response(url)


if __name__ == '__main__':
    get_response('', 'mongoole@163.com', '123456789')
