#!/usr/bin/python3
#-*- coding:utf-8 -*-
# Author : Mongoole
# Date : 2021/11/24 
import os
import io
import sys
import time
import asyncio
import re
import cchardet
import json
import requests
import pymongo
from lxml import etree
from fake_useragent import UserAgent

sys.stdout = io.TextIOWrapper(sys.stdout.buffer,encoding='gb18030') #改变标准输出的默认编码


def mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    #选择数据库
    db = client.weibo_publish_content
    #选择操作的集合
    p = db.singal_url_comment

    return p


def get_response(url):
    ua = UserAgent().random
    headers = {
        'User-Agent': ua,
        'Cookie':'XSRF-TOKEN=_wACCGRM3Lxbr91mEtmqTV-5; _s_tentry=weibo.com; Apache=2609702625353.072.1638340008644; SINAGLOBAL=2609702625353.072.1638340008644; ULV=1638340008647:1:1:1:2609702625353.072.1638340008644:; SUB=_2A25Mo2WpDeRhGeNG71IV9SnNzjiIHXVv2dBhrDV8PUNbmtAKLWn8kW9NS0z2xIX5LBjEbkkdcnWX-n1tyj50UyvQ; SUBP=0033WrSXqPxfM725Ws9jqgMF55529P9D9WFjsmyPTykYiKzxz71MaOPT5JpX5KzhUgL.Fo-RSh5XSKMpSKB2dJLoIEnLxK-LBKML1K2LxK-L1h5LB-2LxK-LBoMLBoBLxK-LB-qL1h9zdc4r; ALF=1669876089; SSOLoginState=1638340089; WBPSESS=WqOG_E4EvBEfAuZtu9-APMfY375JZvPvK-u8Jp2zIxG3K2KRiGtQDXn8qKYHP3SzY1KdTps_OO9FGFZjyBSqdug9KtCf--AGE6KPkdEWknG1KVfShxCgrqza1fdw42owAlAGaqNlgdX13DMsusULiA==; wvr=6; wb_view_log_5840457154=1920*10801; webim_unReadCount=%7B%22time%22%3A1638342135149%2C%22dm_pub_total%22%3A0%2C%22chat_group_client%22%3A0%2C%22chat_group_notice%22%3A0%2C%22allcountNum%22%3A0%2C%22msgbox%22%3A0%7D',
    }
    try:
        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            html_chardet = cchardet.detect(response.content)
            # print(response.headers["content-type"])
            # print(response.encoding)
            # print(html_chardet)
            response.encoding = html_chardet["encoding"]
            response = response.text.replace('\\"', '#').replace("\\n","")  # 对json格式中的多余的引号去除
            html_content = response.encode("utf-8").decode("unicode_escape")
            return html_content

    except Exception as e:
        print(e)


def get_data(html_content, mongodb,page,root_comment_url):

    html_content = json.loads(html_content)
    # print(html_content)
    # breakpoint()
    html_comment = html_content['data']['html']
    selector = etree.HTML(html_comment)
    comment_lst = selector.xpath('//*[@node-type="#root_comment#"]')

    for item in comment_lst:
        data_dict = {}
        comment_id = item.xpath('./@comment_id')[0].replace('#', '')
        # print(comment_id)
        data_dict['comment_id'] = comment_id

        user_name = item.xpath('./div[@class="#WB_face"]/a/img/@alt')[0].replace('#', '')
        # user_name = item.xpath('./div[@class="#WB_face"]/a/img/@alt')
        # print(user_name)
        data_dict['user_name'] = user_name

        user_img = item.xpath('./div[@class="#WB_face"]/a/img/@src')[0].replace('#', '')
        # print(user_img)
        data_dict['user_name'] = user_name

        user_id = item.xpath('./div[@class="#WB_face"]/a/img/@usercard')[0].replace('#', '')
        # print(user_id)
        data_dict['user_id'] = user_id

        user_url = item.xpath('./div[@class="#WB_face"]/a/@href')[0].replace('#', '')
        # print(user_url)
        data_dict['user_url'] = user_url

        user_comment_content = item.xpath('./div[@class="#list_con#"]/div/text()')
        user_comment_content = "".join(user_comment_content).replace("\n", "").replace(" ","")
        # print(user_comment_content)
        data_dict['user_comment_content'] = user_comment_content

        published_time = item.xpath('.//div[@class="#WB_from"]/text()')[0]
        # print(published_time)
        data_dict['published_time'] = published_time

        like_count = item.xpath('.//span[@node-type="#like_status#"]/em/text()')[1]
        # print(like_count)
        data_dict['like_count'] = like_count

        data_dict['root_comment_url'] = root_comment_url
        # print(data_dict)
        # breakpoint()
        mongodb.insert(data_dict)


def main():

    mongodb = mongo()
    root_comment_url = 'https://weibo.com/7264589101/L3RO9vlb7?filter=hot&root_comment_id=0&type=comment'
    for page in range(1, 16):

        if page == 1:
            url = "https://weibo.com/aj/v6/comment/big?ajwvr=6&id=4709557898838386&from=singleWeiBo&__rnd=1638340450723"

        else:
            url = 'https://weibo.com/aj/v6/comment/big?ajwvr=6&id=4709557898838386' \
                  '&root_comment_max_id=139543237039181&root_comment_max_id_type=0' \
                  '&root_comment_ext_param=&page={}&filter=hot&sum_comment_number=47' \
                  '&filter_tips_before=0&from=singleWeiBo&__rnd=1638340216009'.format(page)

        response = get_response(url)
        get_data(response, mongodb, page, root_comment_url)
        print("{} Page {} was finished!{}".format('*' * 40, page, '*' * 40))
        time.sleep(3)


if __name__ == '__main__':
    main()