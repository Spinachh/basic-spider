# !/usr/bin/python3
# -*- coding:utf-8 -*-
# Author : mongoose
# Date : 2022/8/10

import os
import io
import re
import sys
import time
import json
import pymongo
import requests
import logging

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')  # 改变标准输出的默认编码
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


class CommentNewWeiBO(object):

    def __init__(self, comment_root_id, user_id):
        self.max_id = 0
        self.comment_root_id = comment_root_id
        self.uid = user_id

    @staticmethod
    def mongodb():
        # 连接数据库mongodb
        client = pymongo.MongoClient(host='127.0.0.1', port=27017)
        # 选择数据库
        db = client.weibo_pc_comment_test
        # 选择操作的集合
        mongodb = db.comment_test

        return mongodb

    @staticmethod
    def get_response(url):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36',
            'Cookie': 'XSRF-TOKEN=_wACCGRM3Lxbr91mEtmqTV-5; _s_tentry=weibo.com; Apache=2609702625353.072.1638340008644; SINAGLOBAL=2609702625353.072.1638340008644; ULV=1638340008647:1:1:1:2609702625353.072.1638340008644:; TC-V-WEIBO-G0=b09171a17b2b5a470c42e2f713edace0; SSOLoginState=1642384589; UOR=,,login.sina.com.cn; cross_origin_proto=SSL; wvr=6; PC_TOKEN=8642cf29de; SCF=AgYQMOR3eoIWuR9J8pcSSuq-KHu_fXg-Utse5ZOmqHSWz0ROg_g5d6d2JZPWoG_HKCTAzEVuBTY2kHBIzDygL5U.; SUB=_2A25P92zJDeRhGeNG71IV9SnNzjiIHXVshdkBrDV8PUJbmtAKLWTWkW9NS0z2xFuoTxeCzcG6809c9N-_zKmxlNba; SUBP=0033WrSXqPxfM725Ws9jqgMF55529P9D9WFjsmyPTykYiKzxz71MaOPT5JpX5K-hUgL.Fo-RSh5XSKMpSKB2dJLoIEnLxK-LBKML1K2LxK-L1h5LB-2LxK-LBoMLBoBLxK-LB-qL1h9zdc4r; ALF=1691635708; wb_view_log_5840457154=1920*10801; webim_unReadCount={"time":1660099756063,"dm_pub_total":2,"chat_group_client":0,"chat_group_notice":0,"allcountNum":35,"msgbox":0}; WBPSESS=WqOG_E4EvBEfAuZtu9-APMfY375JZvPvK-u8Jp2zIxG3K2KRiGtQDXn8qKYHP3SzzuV8mghXfKGxHUcXSifs3yszRs9WvIK4fkLAT9tYihztkkoj8rKiGnIiR2wFuKjH1u196HUXEoaJs65idKyuTw==',
        }
        try:
            response = requests.get(url, headers=headers)
            if response.status_code == 200:
                html_content = response.text
                html_content = json.loads(html_content)
                return html_content

        except Exception as e:
            print(e)

    def get_data(self, response, mongodb):
        if 'data' in response.keys():
            response_data = response['data']
            max_id_return = response.get('max_id')
            comment_header = ['nickname', 'text_raw', 'created_at', 'source',
                              'like_counts', 'comment_root_id', 'user_id', 'location']

            for item in response_data:
                created_at = item.get('created_at')
                like_counts = item.get('like_counts')
                comment_root_id = item.get('rootidstr')
                source = item.get('source')
                text_raw = item.get('text_raw')

                # comment user information
                usr_information = item.get('user')
                nickname = usr_information['screen_name']
                user_id = usr_information['idstr']
                location = usr_information['location']

                comment_text = [nickname, text_raw, created_at, source,
                                like_counts, comment_root_id, user_id, location]

                item_dict = dict(zip(comment_header, comment_text))
                # logging.warning(item_dict)
                mongodb.insert_one(item_dict)

                # comment floor true or false
                self.get_floor_comment(comment_root_id, user_id, mongodb)

            logging.warning('The next max id :{}'.format(max_id_return))
            return max_id_return

    def get_floor_comment(self, comment_root_id, user_id, mongodb):
        max_id = 0
        comment_header = ['nickname', 'text_raw', 'created_at', 'source',
                          'like_counts', 'comment_root_id', 'user_id', 'location']
        while True:
            floor_comment_id = comment_root_id
            floor_url = 'https://weibo.com/ajax/statuses/buildComments?flow=0' \
                        '&is_reload=1&id={floor_comment_id}&is_show_bulletin=2' \
                        '&is_mix=1&fetch_level=1&' \
                        'max_id={max_id}&count=20' \
                        '&uid={user_id}'.format(floor_comment_id=floor_comment_id, max_id=max_id
                                                , user_id=user_id)
            floor_response = self.get_response(floor_url)
            if 'data' in floor_response.keys():
                floor_response_data = floor_response['data']
                max_id_return = floor_response.get('max_id')
                for item in floor_response_data:
                    created_at = item.get('created_at')
                    like_counts = item.get('like_counts')
                    comment_root_id = item.get('rootidstr')
                    source = item.get('source')
                    text_raw = item.get('text_raw')

                    # comment user information
                    usr_information = item.get('user')
                    nickname = usr_information['screen_name']
                    user_id = usr_information['idstr']
                    location = usr_information['location']

                    comment_text = [nickname, text_raw, created_at, source,
                                    like_counts, comment_root_id, user_id, location]
                    item_dict = dict(zip(comment_header, comment_text))
                    mongodb.insert_one(item_dict)

                if int(max_id_return) != 0:
                    max_id = max_id_return
                    time.sleep(1)
                else:
                    logging.warning('Comment Root : {} second floor was finished!'.format(floor_comment_id))
                    break
            else:
                break

    def start_run(self):
        """
        flow: 0 为按热度, 1为按时间
        :return:
        """
        mongodb = self.mongodb()
        while True:
            url = 'https://weibo.com/ajax/statuses/buildComments?flow=0' \
                  '&is_reload=1&max_id={max_id}&' \
                  'id={comment_root_id}&is_show_bulletin=2&is_mix=0&count=20' \
                  '&uid={uid}'.format(max_id=self.max_id,
                                      comment_root_id=self.comment_root_id,
                                      uid=self.uid)

            response = self.get_response(url)
            max_id_return = self.get_data(response, mongodb)

            if int(max_id_return) != 0:
                max_id = max_id_return
                logging.warning('Comment Page Next Max Id : {}'.format(max_id))
                time.sleep(1)
                continue
            else:
                break


if __name__ == '__main__':

    # https://weibo.com/ajax/statuses/buildComments?is_reload=1&id=4798842924435336&is_show_bulletin=2&is_mix=1&fetch_level=1&max_id=0&count=20&uid=6548368832
    comment_root_id = '4798842924435336'
    user_id = '6548368832'
    comment_new_spider = CommentNewWeiBO(comment_root_id, user_id)
    comment_new_spider.start_run()
