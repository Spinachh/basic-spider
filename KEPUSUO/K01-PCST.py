# -*- coding:utf-8 -
# Project : KeXie
# Author : mongoole
# Date : 2024-01-18

import random
import re
import io
import sys
import time
import datetime
import json
import argparse
import cchardet
import urllib3
import requests
import logging
import pymongo
from lxml import etree
from gne import GeneralNewsExtractor
from science_system_account import science_account

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
urllib3.disable_warnings()
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def season_num():
    # since_time = (datetime.datetime.now() - datetime.timedelta(days=1)).strftime("%Y-%m-%d 00:00:00")
    year_time = time.strftime('%Y', time.localtime(time.time()))
    month_time = time.strftime('%m', time.localtime(time.time()))
    since_time = (datetime.datetime.now() - datetime.timedelta(days=30)).strftime("%Y-%m-%d")

    season_num_dict = {
        'Q1': ['01', '02', '03'],
        'Q2': ['04', '05', '06'],
        'Q3': ['07', '08', '09'],
        'Q4': ['10', '11', '12'],
    }

    if month_time in season_num_dict.get('Q1'):
        q_num = 'Q1'
        c_name = year_time + '-' + q_num
        return c_name, since_time

    elif month_time in season_num_dict.get('Q2'):
        q_num = 'Q2'
        c_name = year_time + '-' + q_num
        return c_name, since_time

    elif month_time in season_num_dict.get('Q3'):
        q_num = 'Q3 '
        c_name = year_time + '-' + q_num
        return c_name, since_time

    else:
        q_num = 'Q4'
        c_name = str(year_time) + '-' + q_num
        return c_name, since_time


def mongodb():
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    db = client.kepusuo_foreign_keyword
    collection = db['{}'.format(c_name)]
    return collection


def save_html(url, page, text, keyword):
    domain_name = url.replace('https://', '').replace('/', '-')
    with open('{}-page-{}-{}.txt'.format(domain_name, page, keyword), 'w', encoding='utf-8') as f:
        f.write(text)


def get_html(url, page, keyword):
    cookies = {
        '_ga': 'GA1.1.156052043.1705557996',
        'PHPSESSID': 'j5bm3al0non942mot79spah05t',
        'aviaCookieConsent': '7d509d13fad590df662142509e94c78a||v1.0',
        'aviaPrivacyRefuseCookiesHideBar': 'true',
        'aviaPrivacyEssentialCookiesEnabled': 'true',
        '_ga_41RKGPQ7ZG': 'GS1.1.1705557996.1.1.1705558122.0.0.0',
    }

    headers = {
        'authority': 'www.pcst.network',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'accept-language': 'zh-CN,zh;q=0.9',
        'referer': 'https://www.pcst.network/?s={}'.format(keyword),
        'sec-ch-ua': '"Not_A Brand";v="8", "Chromium";v="120", "Google Chrome";v="120"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/120.0.0.0 Safari/537.36',
    }

    params = {'s': keyword}

    # req_url = url + '?s={}'.format(keyword)
    # print(req_url)
    # print(url)
    # breakpoint()
    response = requests.get(url, headers=headers, params=params, verify=False)
    response.encoding = 'utf-8'
    status_code = response.status_code
    # print(response.text)
    # breakpoint()
    if status_code == 200:
        save_html(url, page, response.text, keyword)

    return response.text, status_code


def get_data(html_text, account_name, science_system, mongo, project_time, start_time, keyword):
    try:
        selector = etree.HTML(html_text)
        part1_nodes = selector.xpath('//*[@class="container"]/main/article')
        # print(part1_nodes)
        # breakpoint()
        deadline = xpath_data(part1_nodes, science_system, mongo,
                              account_name, project_time, start_time, keyword)
        if deadline:
            return deadline

    except Exception as e:
        logging.warning('KEYWORD ： {} PART1 HAS NOT CONTENT: {}'.format(keyword,e))


def xpath_data(part_nodes, science_system, mongo, account_name, project_time, start_time, keyword):
    news_dict_name = ['news_title', 'news_abstract', 'news_imgs', 'news_publish_time',
                      'news_content_type', 'news_content', 'news_page_url', 'source',
                      'news_author', 'read_count', 'click_count', 'crawl_time',
                      'account_name', 'science_system', 'project_time', 'keyword']
    start_time_stamp = time.mktime(time.strptime(start_time, '%Y-%m-%d'))
    # print(part_nodes)
    # print('okkkk')
    # breakpoint()
    for i in range(len(part_nodes)):
        try:
            news_title = part_nodes[i].xpath('.//h2/a/text()')[0].strip()
        except:
            news_title = ''

        try:
            news_abstract = part_nodes[i].xpath('./div/div/p/text()')[0].strip()
        except:
            news_abstract = ''

        try:
            news_publish_time = part_nodes[i].xpath('.//time/@datetime')[0].strip().split('T')[0]
        except:
            news_publish_time = ''

        # print(news_title)
        # print(news_abstract)
        # print(news_publish_time)
        # breakpoint()

        news_publish_stamp = time.mktime(time.strptime(news_publish_time, '%Y-%m-%d'))
        if int(news_publish_stamp) >= int(start_time_stamp):
            try:
                url_part2 = part_nodes[i].xpath('.//h2/a/@href')[0].strip()

            except Exception as e:
                logging.warning('{} ARTICLE URL WAS ERROR :{}'.format(account_name, e))
                break

            if 'http' in url_part2:
                news_page_url = url_part2
            else:
                news_page_url = 'https://www.pcst.network/' + url_part2

            news_author, news_imgs, news_content_type, news_content, source, \
            read_count, click_count, news_publish_time = get_page_content(news_page_url)

            crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

            news_dict_value = [news_title, news_abstract, news_imgs, news_publish_time,
                               news_content_type, news_content, news_page_url, source,
                               news_author, read_count, click_count, crawl_time,
                               account_name, science_system, project_time, keyword
                               ]

            news_dict_content = dict(list(zip(news_dict_name, news_dict_value)))

            # logging.warning(news_dict_content)
            # breakpoint()
            mongo.insert_one(news_dict_content)
            logging.warning('ACCOUNT: {} TITLE :{} PUBLISH: {}'
                            .format(account_name, news_title[:10], news_publish_time))
        else:
            deadline = True
            return deadline


def get_page_content(news_page_url):

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.7',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/120.0.0.0 Safari/537.36',
    }

    html_response = requests.get(news_page_url, headers=headers, verify=False)

    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()

    try:
        result = extractor.extract(content_text)
    except:
        result = ''
    try:
        news_content = selector_page.xpath('//*[@class="entry-content"]//span//text() | '
                                           '//*[@class="entry-content"]//p//text() | '
                                           '//*[@class="entry-content"]//p//span//text()'
                                           )
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = re.findall(r'Author:</strong> (.*?)</>', content_text, re.M | re.S)[0].strip().replace(' ', '')
    except:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@class="entry-content"]//span//img/@src |'
                                        '//*[@class="entry-content"]//p//img/@src | '
                                        '//*[@class="entry-content"]//div//img/@src |'
                                        '//*[@class="entry-content"]//img/@src')[0]
        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        news_video = selector_page.xpath('//*[@class="nr_left"]//span//video/@src |'
                                         ' //*[@class="nr_left"]//p//video/@src |'
                                         ' //*[@class="nr_left"]//div//video/@src ')[0]
        if news_video:
            news_video_flag = 'video'
        else:
            news_video_flag = ''
    except:
        news_video_flag = ''

    news_content_type = news_content_type + '-' + news_video_flag

    try:
        source = re.findall(r'来源：(.*?)&nbsp', content_text, re.M | re.S)[0].strip()
    except:
        source = ''

    try:
        read_count = re.findall(r';阅读数：(.*?)&nbsp', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''
    # print(read_count)
    # breakpoint()

    try:
        click_count = re.findall(r'点击量：(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''

    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    try:
        news_publish_time = re.findall(r'日期：(.*?) <div', content_text, re.M | re.S)[0].strip()
    except:
        news_publish_time = '2023-01-01'

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count, news_publish_time


def get_weixin_page_content(news_page_url):
    html_response = requests.get(news_page_url, verify=False)
    html_response.encoding = 'utf-8'
    content_text = html_response.text
    selector_page = etree.HTML(content_text)
    extractor = GeneralNewsExtractor()
    try:
        result = extractor.extract(content_text)
    except:
        result = ''

    try:
        news_content = selector_page.xpath('//*[@data-role="outer"]//span/text() | '
                                           '//*[@data-role="outer"]//p/text() ')
        news_content = ''.join([x.strip() for x in news_content])
    except:
        news_content = result.get('content')

    try:
        news_author = result.get('author')
    except Exception as e:
        news_author = ''

    try:
        news_imgs = selector_page.xpath('//*[@data-role="outer"]//span//img/@data-src |'
                                        '//*[@data-role="outer"]//p//img/@data-src')[0]

        if news_imgs:
            news_content_type = 'text-img'
        else:
            news_content_type = 'text'

        if len(news_imgs) > 800:
            news_imgs = ''
    except:
        news_imgs = ''
        news_content_type = 'text'

    try:
        source = re.findall(r'id="ctis">来源：(.*?);', content_text, re.M | re.S)[0].strip()
    except Exception as e:
        source = ''
    try:
        read_count = re.findall(r'浏览次数：(.*?)</', content_text, re.M | re.S)[0].strip()
    except:
        read_count = ''

    try:
        click_count = re.findall(r'点击：<span style="color:#000;">(.*?)<', content_text, re.M | re.S)[0].strip()
    except:
        click_count = ''
    # dr = re.compile(r'<[^>]+>', re.S)
    # content_text = dr.sub('', html_response.text).strip()
    # print(result)
    # print(content_text)
    news_publish_time = re.findall(r"var createTime = '(.*?)'", content_text, re.M | re.S)[0].strip()

    return news_author, news_imgs, news_content_type, news_content, source, read_count, click_count, news_publish_time


def read_science_account(account_name):
    global science_system
    for key, value in science_account.items():
        if account_name in value:
            science_system = key

    return science_system


def start_run(project_time, start_time, account_name):
    mongo = mongodb()
    science_system = read_science_account(account_name)

    org_url = 'https://www.pcst.network/page/'

    keywords = ["Science", "Technology", "Innovation", "Science Literacy", "Scientific Literacy", "Science Education",
                "Science Communication", "Science Popularization", "Public", "Digital Literacy", "SDGs", "Knowledge",
                "Understanding", "Scientific inquiry", "Critical thinking", "Competence", "Agency", "Journalism",
                "Museum", "Center/Centre", "Venues", "Workshops", "Climate Literacy", "Information Literacy",
                "Media Literacy", "Computer Literacy", "Electronic Literacy", "Sustainable Development",
                "Modernization", "Cooperation/Collaboration",
                ]
    for keyword in keywords:
        for page in range(1, 50):
            url = org_url + str(page) + '/'
            html_text, status_code = get_html(url, page, keyword)
            if status_code != 200:
                break
            deadline = get_data(html_text, account_name, science_system,
                                mongo, project_time, start_time, keyword)
            if deadline:
                break
            time.sleep(1)
            logging.warning('KEYWORD：{} PAGE: {} CONTENT DEADLINE'.format(keyword, page))


if __name__ == '__main__':
    c_name, since_time = season_num()
    parser = argparse.ArgumentParser(description='Process Argparse')
    parser.add_argument('--projectname', '-n', help='name Attributes, required parameters',
                        default='kepusuo-keyword-{}'.format(c_name))
    parser.add_argument('--sinceyear', '-y', help='year Attributes, required parameters, has default',
                        default='{}'.format(since_time))
    args = parser.parse_args()
    account_name = 'K-01 国际公众科技传播学会'
    start_run(args.projectname, args.sinceyear, account_name)
