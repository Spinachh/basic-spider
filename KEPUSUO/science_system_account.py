# -*- coding:utf-8 -*-
# Project : KeXie
# Author : mongoole
# Date : 

import re
import io
import sys
import time
import json
import cchardet
import requests
from lxml import etree


science_account = {
    'FN': [
        'K-01 国际公众科技传播学会', 'K-02 优睿科', 'K-03 北美科学中心协会',
        'K-04 美国科学促进会', 'K-05 美国科学基金会', 'K-06 美国科学教师协会',
        'K-07 德国科学对话组织', 'K-08 澳大利亚科学传播者协会', 'K-09 国际科学理事会',
        'K-10 阿拉伯世界科技促进会', ],
}

account_name = 'A-04 中国光学学会'
# for key, value in science_account.items():
#     print(key)
#     if account_name in value:
#         print('ok')
#         science_system = key
#         print(science_system)