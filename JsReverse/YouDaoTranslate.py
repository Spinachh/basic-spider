# !/usr/bin/python
# -*-coding:utf-8-*-
# Author：Mongoole
# Date：2020/11/9

import time
import os
import re
import json
import requests
from fake_useragent import UserAgent
import hashlib
import random

def get_response(url,keyword):
    useragent = UserAgent()
    ua = useragent.random
    headers = {
        "user-agent": ua,

    }

    bv = hashlib.md5(ua.encode(encoding='utf-8')).hexdigest()
    lts = str(time.time() * 1000).split('.')[0]
    salt = lts + str(random.randint(1,10))
    sign = hashlib.md5(("fanyideskweb" + keyword + salt + "]BjuETDhU)zqSxf-=B#7m").encode(encoding='utf-8')).hexdigest()

    data = {
        "i": keyword,
        "from": "AUTO",
        "to": "AUTO",
        "smartresult": "dict",
        "client": "fanyideskweb",
        "salt": salt,
        "sign": sign,
        "lts": lts,
        "bv": bv,
        "doctype": "json",
        "version": "2.1",
        "keyfrom": "fanyi.web",
        "action": "FY_BY_REALTlME",
    }

    response = requests.post(url,headers=headers,data=data)

    try:
        if response.status_code == 200:
            print(response.json())
    except Exception as e:
        print(e)


def main():
    # url = 'http://fanyi.youdao.com/translate_o?smartresult=dict&smartresult=rule'
    url = 'http://fanyi.youdao.com/translate?smartresult=dict&smartresult=rule'
    keyword = input('Please input your keyword:' + '\n')
    get_response(url,keyword)


if __name__ == '__main__':
    main()