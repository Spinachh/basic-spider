#!/bin/python3

import time
import os
import datetime
import twint
c = twint.Config()

# 代理设置
c.Proxy_host='127.0.0.1'
c.Proxy_port='10801'
c.Proxy_type='http'
# today = datetime.date.today()
# yesterday = today - datetime.timedelta(days=1)
c.Since = '2020-08-30'  # Yestarday
c.Until = '2020-10-02'  # Now-Today

with open('tweet_account.txt','r',encoding='utf-8') as f:
    for line in f:
        # print(c)
        line = line.strip()
        c.Username = line
        # c.Limit=3000 # 最大获取

        # 导出文件
        mm = time.strftime('%Y-%m-%d', time.localtime(time.time()))
        c.Store_csv = True
        c.Output = c.Username + '-' + mm + '.csv'
        twint.run.Search(c)

        #time.sleep(10)