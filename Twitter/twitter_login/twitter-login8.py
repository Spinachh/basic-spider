#!/bin/python3
# -*-coding:utf-8 -*-
# Date:2023-07-17
# Author: Mongoole
import json

# 1.时间需要加8小时（北京时间为东八区，与格林尼治时间差8小时。北京时间=格林尼治时间+8小时）采集的时间是格林尼治时间（世界时）
# 2.csv格式文件写入使用"gb18030"（部分字符容易出现乱码）,舍弃"emo"字段
# 3.需修改采集时间段
# 4.目前只采集账号推文
# 5.无vpn的情况下才使用proxy代理，本身就可以访问代理不需要配置


from selenium import webdriver
from selenium.webdriver.common.by import By
import time


def operate_login_page(driver, user_email, user_password, user_name):
    driver.find_element(By.XPATH, '//*[@id="layers"]/div/div/div/div/div/div/div[2]/div[2]/div/div/div[2]/div['
                                  '2]/div/div/div/div[5]/label/div/div[2]/div/input').send_keys(user_email)
    time.sleep(5)
    driver.find_element(By.XPATH, '//*[@id="layers"]/div/div/div/div/div/div/div[2]/div[2]/div/div/div[2]/div['
                                  '2]/div/div/div/div[6]').click()
    time.sleep(5)

    try:
        driver.find_element(By.XPATH, '//*[@id="layers"]/div/div/div/div/div/div/div[2]/div[2]/div/div/div[2]/div[2]/div['
                                      '1]/div/div/div[3]/div/label/div/div[2]/div[1]/input').send_keys(user_password)
        time.sleep(2)
        driver.find_element(By.XPATH, '//*[@id="layers"]/div/div/div/div/div/div/div[2]/div[2]/div/div/div[2]/div[2]/div['
                                      '2]/div/div[1]/div/div/div').click()
    except:
        time.sleep(2)
        driver.find_element(By.XPATH, '//*[@id="layers"]/div/div/div/div/div/div/div[2]/div[2]/div/div/div[2]/div[2]/div['
                                      '1]/div/div[2]/label/div/div[2]/div/input').send_keys(user_name)
        time.sleep(2)
        driver.find_element(By.XPATH, '//*[@id="layers"]/div/div/div/div/div/div/div[2]/div[2]/div/div/div[2]/div[2]/div['
                                      '2]/div/div/div/div').click()
        time.sleep(2)
        driver.find_element(By.XPATH, '//*[@id="layers"]/div/div/div/div/div/div/div[2]/div[2]/div/div/div[2]/div[2]/div['
                                      '1]/div/div/div[3]/div/label/div/div[2]/div[1]/input').send_keys(user_password)
        time.sleep(2)
        driver.find_element(By.XPATH, '//*[@id="layers"]/div/div/div/div/div/div/div[2]/div[2]/div/div/div[2]/div[2]/div['
                                      '2]/div/div[1]/div/div/div').click()
    time.sleep(20)

    get_logined_cookie(driver)


def get_logined_cookie(driver):
    cookie_loginned = driver.get_cookies()   # 获取登录后cookie
    # print(cookie_loginned)   # 打印登录后cookie信息
    jsonCookie = json.dumps((cookie_loginned))
    with open('login_cookie', 'w') as f:
        f.write(jsonCookie)
    driver.quit()


def get_driver():
    driver = webdriver.Chrome()
    driver.get('https://twitter.com/login')
    driver.maximize_window()
    time.sleep(10)
    return driver


def main():
    driver = get_driver()
    user_email = 'kwamenprix9@hotmail.com'
    user_password = 'ybmq9644'
    user_name = 'HaleR24287'
    operate_login_page(driver, user_email, user_password, user_name)


if __name__ == '__main__':
    main()