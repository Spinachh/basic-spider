import time
import datetime
import twint

c = twint.Config()
# print(c)

# c.Username = "ajenews"
c.Search = "china covid-19"
# c.Limit=3000 # 最大获取

# 代理设置
c.Proxy_host='127.0.0.1'
c.Proxy_port='10801'
c.Proxy_type='http'

# today = datetime.date.today()
# yesterday = today - datetime.timedelta(days=1)
#
# c.Since = str(yesterday)     #Yestarday
# c.Until = str(today)    #Now-Today

c.Since = '2020-02-12'
c.Until = '2020-04-10'
# 导出文件
mm = time.strftime('%Y-%m-%d-%H',time.localtime(time.time()))

c.Store_csv = True
c.Output = 'result-output/' + "-".join(c.Search.split(' ')) + '-' + mm + '.csv'

twint.run.Search(c)
