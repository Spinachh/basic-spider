#!/usr/bin/python3
# -*- coding:utf-8 -*-
# Author : Mongoole
# Date : 2023/2/9

import os
import io
import re
import csv
import sys
import time
import datetime
import json
import asyncio
import urllib3
import logging
import subprocess
import cchardet
import requests


urllib3.disable_warnings()
file_name = time.strftime('%Y-%m-%d', time.localtime(time.time()))
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')  # 改变标准输出的默认编码
logging.basicConfig(level=logging.INFO, filename=r'log/am/am-{}.log'.format(file_name),
                    format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s')


def get_token():
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:93.0) Gecko/20100101 Firefox/93.0',
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2',
        'x-guest-token': '',
        'x-twitter-client-language': 'zh-cn',
        'x-twitter-active-user': 'yes',
        'x-csrf-token': '25ea9d09196a6ba850201d47d7e75733',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'authorization': 'Bearer AAAAAAAAAAAAAAAAAAAAANRILgAAAAAAnNwIzUejRCOuH5E6I8xnZz4puTs%3D1Zv7ttfk8LF81IUq16cHjhLTvJu4FA33AGWWjCpTnA',
        'Referer': 'https://twitter.com/',
        'Connection': 'keep-alive',
    }
    response = requests.post('https://api.twitter.com/1.1/guest/activate.json',
                             headers=headers, verify=False)
    content = json.loads(response.text)
    guest_token = content['guest_token']

    return guest_token


def get_response(url, user_id, cursor, guest_token):

    headers = {
        'authority': 'api.twitter.com',
        'accept': '*/*',
        'accept-language': 'zh-CN,zh;q=0.9',
        'authorization': 'Bearer AAAAAAAAAAAAAAAAAAAAANRILgAAAAAAnNwIzUejRCOuH5E6I8xnZz4puTs%3D1Zv7ttfk8LF81IUq16cHjhLTvJu4FA33AGWWjCpTnA',
        'content-type': 'application/json',
        'cookie': 'guest_id_marketing=v1%3A167584191522991876; guest_id_ads=v1%3A167584191522991876; personalization_id="v1_lhtRwgF0mJjTvscCNfyQnA=="; guest_id=v1%3A167584191522991876; gt=1623224758979657730; ct0=d24b5eaa162a749b846e12c734922d86; _ga=GA1.2.1289706566.1675841925; _gid=GA1.2.626132637.1675841925; at_check=true; mbox=session#05dd35de14b6496a8744808d5a546e82#1675844646|PC#05dd35de14b6496a8744808d5a546e82.38_0#1739087586',
        'origin': 'https://twitter.com',
        'referer': 'https://twitter.com/',
        'sec-ch-ua': '"Not_A Brand";v="99", "Google Chrome";v="109", "Chromium";v="109"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-site',
        'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
        'x-csrf-token': 'd24b5eaa162a749b846e12c734922d86',
        'x-guest-token': guest_token,
        'x-twitter-active-user': 'yes',
        'x-twitter-client-language': 'zh-cn',
    }

    params = {
        'variables': '{"userId":"%(user_id)s","count":40,"cursor":"%(cursor)s","includePromotedContent":true,'
                     '"withQuickPromoteEligibilityTweetFields":true,"withSuperFollowsUserFields":true,'
                     '"withDownvotePerspective":false,"withReactionsMetadata":false,"withReactionsPerspective":false,'
                     '"withSuperFollowsTweetFields":true,"withVoice":true,"withV2Timeline":true'
                     '}' % {'user_id': user_id, 'cursor': cursor},
        'features': '{"responsive_web_twitter_blue_verified_badge_is_enabled":true,'
                    '"responsive_web_graphql_exclude_directive_enabled":false,"verified_phone_label_enabled":false,'
                    '"responsive_web_graphql_timeline_navigation_enabled":true,'
                    '"responsive_web_graphql_skip_user_profile_image_extensions_enabled":false,'
                    '"longform_notetweets_consumption_enabled":true,"tweetypie_unmention_optimization_enabled":true,'
                    '"vibe_api_enabled":true,"responsive_web_edit_tweet_api_enabled":true,'
                    '"graphql_is_translatable_rweb_tweet_is_translatable_enabled":true,'
                    '"view_counts_everywhere_api_enabled":true,"freedom_of_speech_not_reach_appeal_label_enabled":false,'
                    '"standardized_nudges_misinfo":true,'
                    '"tweet_with_visibility_results_prefer_gql_limited_actions_policy_enabled":false,'
                    '"interactive_text_enabled":true,"responsive_web_text_conversations_enabled":false,'
                    '"responsive_web_enhance_cards_enabled":false}',
    }

    '''
    params = {
        'variables': '{"userId": "%(user_id)s", "count": 40, "includePromotedContent": "true","cursor":"%(cursor)s",'
                     '"withQuickPromoteEligibilityTweetFields": "true", '
                     '"withSuperFollowsUserFields": "true","withDownvotePerspective": "false", '
                     '"withReactionsMetadata": "false", "withReactionsPerspective": "false",'
                     '"withSuperFollowsTweetFields": "true", "withVoice": "true", '
                     '"withV2Timeline": "false","__fs_dont_mention_me_view_api_enabled": "false", '
                     '"__fs_interactive_text": "false",'
                     '"__fs_responsive_web_uc_gql_enabled": "false"}' % {'user_id': user_id, 'cursor': cursor},
    }
    '''

    try:
        # response = requests.get(url, headers=headers, params=params, verify=False)
        response = requests.get(url, headers=headers, params=params, verify=False)
        if response.status_code == 200:
            html_chariest = cchardet.detect(response.content)
            # print(response.headers["content-type"])
            # print(response.encoding)
            # print(html_chardet)
            response.encoding = html_chariest["encoding"]
            # response = response.text.replace('\\"', '#').replace("\\n","")  # 对json格式中的多余的引号去除
            html_content = response.text.encode("utf-8").decode("unicode_escape")
            html_content = response.text
            html_content = json.loads(html_content)
            return html_content

    except Exception as e:
        print(e)


def get_data(response, writer_csv, since_time):
    # items = response['data']['user']['result']['timeline']['timeline']['instructions'][0]['entries']
    # items = response['data']['user']['result']['timeline']['timeline']['instructions']
    items = response['data']['user']['result']['timeline_v2']['timeline']['instructions']
    for lst in items:
        if lst.get('type') == 'TimelineAddEntries':
            itemise = lst['entries']
            for item in itemise:
                item_dict = {}
                data = item['content']
                if 'itemContent' in data.keys():
                    try:
                        content_flag = data['itemContent']['tweet_results']['result']
                        if 'tweet' not in content_flag.keys():
                            content_result = data['itemContent']['tweet_results']['result']
                            content = content_result['legacy']
                            user_core = data['itemContent']['tweet_results']['result']['core']['user_results']['result']['legacy']
                        else:
                            content_result = data['itemContent']['tweet_results']['result']['tweet']
                            content = content_result['legacy']
                            user_core = data['itemContent']['tweet_results']['result']['tweet']['core']['user_results']['result']['legacy']
                        user_name = user_core['name']
                        user_screen_name = user_core['screen_name']
                        postdate = content['created_at']
                        utc = datetime.datetime.strptime(postdate, '%a %b %d %H:%M:%S +0000 %Y')  # 格林尼治时间的格式化
                        published_time = str(utc + datetime.timedelta(hours=8))

                        if published_time >= since_time:
                            # item_dict['conversation_id'] = content['conversation_id_str']
                            created_at = published_time
                            item_dict['created_at'] = created_at
                            full_text = content['full_text'].replace('\n', '')
                            item_dict['full_text'] = full_text
                            favorite_count = content['favorite_count']
                            item_dict['favorite_count'] = favorite_count
                            quote_count = content['quote_count']
                            item_dict['quote_count'] = quote_count
                            reply_count = content['reply_count']
                            item_dict['reply_count'] = reply_count
                            retweet_count = content['retweet_count']
                            item_dict['retweet_count'] = retweet_count
                            # retweeted = content['retweeted']
                            # item_dict['retweeted'] = retweeted
                            is_quote_status = content['is_quote_status']
                            item_dict['is_quote_status'] = is_quote_status
                            # retweeted_flag = content['retweeted']
                            if 'retweeted_status_result' in content.keys():
                                retweeted = 'True'
                                try:
                                    retweet_user_information = content['retweeted_status_result']['result']['core']
                                except:
                                    retweet_user_information = content['retweeted_status_result']['result']['tweet']['core']
                                retweet_username = retweet_user_information['user_results']['result']['legacy']['screen_name']
                                # retweet_useridstr = content['retweeted_status_result']['result']['legacy']['user_id_str']
                                # retweet_favorite = content['retweeted_status_result']['result']['legacy']['favorite_count']
                                # retweet_replay = content['retweeted_status_result']['result']['legacy']['reply_count']
                                # retweet_retweet = content['retweeted_status_result']['result']['legacy']['retweet_count']
                                # retweet_quote = content['retweeted_status_result']['result']['legacy']['quote_count']
                                # retweet_published = content['retweeted_status_result']['result']['legacy']['created_at']
                                # retweet_utc = datetime.datetime.strptime(postdate, '%a %b %d %H:%M:%S +0000 %Y')  # 格林尼治时间的格式化
                                # retweet_published_time = str(retweet_utc + datetime.timedelta(hours=8))

                            else:
                                retweeted = 'False'
                                retweet_username = ''
                                # retweet_useridstr = ''
                                # retweet_favorite = ''
                                # retweet_replay = ''
                                # retweet_retweet = ''
                                # retweet_quote = ''
                                # retweet_published_time = ''

                            try:
                                source = re.findall(r'rel="nofollow">(.*?)</a', content_result['source'])[0]
                            except:
                                source = content['source']

                            item_dict['source'] = source
                            user_id_str = content['user_id_str']
                            item_dict['user_id_str'] = user_id_str
                            # item_dict['id_str'] = content['id_str']
                            tweet_url = 'https://twitter.com/' + user_screen_name + '/status/' + content['id_str']
                            item_dict['tweet_url'] = tweet_url
                            item_dict['user_name'] = user_name
                            item_dict['user_screen_name'] = user_screen_name
                            # mongodb.insert(item_dict)
                            item_lst = [retweeted, user_screen_name, retweet_username,
                                        created_at, full_text, favorite_count, quote_count,
                                        reply_count, retweet_count, source, user_id_str, tweet_url]
                            writer_csv.writerow(item_lst)
                        else:
                            break
                    except Exception as e:
                        logging.info('itemContent Info was Error: {}'.format(e))
                        if data['itemContent']['tweet_results']['result']['tombstone']:
                            text = data['itemContent']['tweet_results']['result']['tombstone']['text']['text']
                            logging.info('Text: {}'.format(text))

                else:
                    if data['cursorType'] == 'Bottom':
                        cursor = data['value']
                        return cursor


def loop_user(writer_csv, user_id, user_name):
    guest_token = get_token()
    # url = 'https://twitter.com/i/api/graphql/tXFFoOYy6ovwDaAnr9iT8g/UserTweets?'
    url = 'https://api.twitter.com/graphql/E-dqdIghLd9KohBJXqTsWg/UserTweets?'
    cursor = '0'
    # since_time = time.strftime('%Y-%m-%d 09:00:00', time.localtime(time.time()))
    since_time = (datetime.datetime.now() - datetime.timedelta(days=4)).strftime("%Y-%m-%d 20:00:00")
    while True:
        response = get_response(url, user_id, cursor, guest_token)
        # print(response)
        # breakpoint()
        bottom_cursor = get_data(response, writer_csv, since_time)
        if bottom_cursor == cursor:  # Max Limit 850 tweet
            break
        logging.info('UserName: {} Cursor: {}'.format(user_name, bottom_cursor))
        cursor = bottom_cursor
        if not bottom_cursor:
            break
        time.sleep(3)


def get_account():
    # user_id = "380648579"
    with open(r'twitter_user_account_br', encoding='utf-8') as f:
        user_accounts = f.readlines()
    # user_accounts = ['LuisEnr33483555##########1242798099610906625']
    return user_accounts


def start_run(user_accounts, writer_csv):

    for user in user_accounts:
        user = user.strip()
        user_id = user.split('##########')[1]
        user_name = user.split('##########')[0]
        loop_user(writer_csv, user_id, user_name)


def main():
    user_accounts = get_account()

    with open(r'results/am/br/{}-all-am.csv'.format(file_name), 'a+', newline='', encoding='gb18030') as f:
        fieldnames = ['retweeted', 'user_screen_name', 'retweet_username', 'created_at',
                      'full_text', 'favorite_count', 'quote_count', 'reply_count',
                      'retweet_count', 'source', 'user_id_str', 'tweet_url'
                      ]

        writer_csv = csv.writer(f)
        writer_csv.writerow(fieldnames)

        start_run(user_accounts, writer_csv)


if __name__ == '__main__':
    main()
    subprocess.run(['sh', '-x', 'am-zzzip.sh'])
