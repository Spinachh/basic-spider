# from tweet_api.tweets import api,id
# from tweets import api,id
import csv
import tweepy
from settings import consumer_key,consumer_secret,access_token,access_token_secret

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)

with open('user_lst','r',encoding='utf-8') as f:
    data = f.readlines()

with open('part1-information.csv', 'a', newline='', encoding='utf-8-sig') as f:
    f_csv = csv.writer(f)
    f_csv.writerow(('user_name','screen_name','description','user_location',
                    'time_zone','followers','friends','statuses',
                    'user+url','article_url',))
    for username in data:
        username = username.strip()
        try:  # for a valid user id
            user = api.get_user(username
                )  # returns information about the specified user as an object(stored in variable 'user') of the class API
            screen_name = user.screen_name
            print("Screen name: ", user.screen_name)

            user_name = user.name
            print("User name: ", user.name)

            description = user.description
            print("Description: ", user.description)

            location = user.location
            print("User location: ", user.location)

            timezone = user.time_zone
            print("Time zone: ", user.time_zone)

            followers = user.followers_count
            print("Number of followers: ", user.followers_count)

            friends = user.friends_count
            print("Number of friends: ", user.friends_count)

            status = user.statuses_count
            print("Number of statuses: ", user.statuses_count)

            user_url = user.url
            print("User URL: ", user.url)
            # print(user.twitter_count)

            data_lst = [user_name,screen_name,description,location,timezone,followers,friends,status,user_url]
            f_csv.writerow(data_lst)
        except Exception as e:  # if the user Id is not valid
            print(e)
