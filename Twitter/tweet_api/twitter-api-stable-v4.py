#! /usr/bin/python3
# -*- coding:utf-8 -*-
# Author : Mongoose
# Date : 2022/7/12
# Version : v4(code review)

import os
import io
import re
import csv
import sys
import time
import datetime
import json
import asyncio
import logging
import subprocess
import cchardet
import pymongo
import requests
from twitter_pm_monitor import run

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')  # 改变标准输出的默认编码

file_name = time.strftime('%Y-%m-%d', time.localtime(time.time()))
logging.basicConfig(level=print, filename=r'pm-{}.log'.format(file_name),
                    format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s')


class Twitter(object):

    def __init__(self, sin_time):
        self.since_time = sin_time

    @staticmethod
    def mongodb():
        client = pymongo.MongoClient(host='127.0.0.1', port=27017)
        db = client.toutiao_content
        collection = db.user_content
        return collection

    @staticmethod
    def get_token():
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:93.0) Gecko/20100101 Firefox/93.0',
            'Accept': '*/*',
            'Accept-Language': 'zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2',
            'x-guest-token': '',
            'x-twitter-client-language': 'zh-cn',
            'x-twitter-active-user': 'yes',
            'x-csrf-token': '25ea9d09196a6ba850201d47d7e75733',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-origin',
            'authorization': 'Bearer AAAAAAAAAAAAAAAAAAAAANRILgAAAAAAnNwIzUejRCOuH5E6I8xnZz4puTs%3D1Zv7ttfk8LF81IUq16cHjhLTvJu4FA33AGWWjCpTnA',
            'Referer': 'https://twitter.com/',
            'Connection': 'keep-alive',
        }
        response = requests.post('https://api.twitter.com/1.1/guest/activate.json', headers=headers, verify=False)
        content = json.loads(response.text)
        guest_token = content['guest_token']
        return guest_token

    def get_response(self, url, user_id, cursor):

        headers = {
            'accept-encoding': 'gzip, deflate,',
            'accept-language': 'zh-CN,zh;q=0.9,en;q=0.8,eo;q=0.7',
            'authorization': 'Bearer AAAAAAAAAAAAAAAAAAAAANRILgAAAAAAnNwIzUejRCOuH5E6I8xnZz4puTs%3D1Zv7ttfk8LF81IUq16cHjhLTvJu4FA33AGWWjCpTnA',
            'cache-control': 'no-cache',
            'content-type': 'application/json',
            'cookie': 'personalization_id="v1_mDeNNOMjYy4/VdV538Xjjw=="; guest_id=v1%3A160324540784635715; _ga=GA1.2.1429506506.1603245416; ads_prefs="HBERAAA="; kdt=vzRkRYI61yeQ5z3ukO1zxA3px95NfGytwarQt2eQ; remember_checked_on=1; _twitter_sess=BAh7DyIKZmxhc2hJQzonQWN0aW9uQ29udHJvbGxlcjo6Rmxhc2g6OkZsYXNo%250ASGFzaHsABjoKQHVzZWR7ADoPY3JlYXRlZF9hdGwrCE2h30h1AToMY3NyZl9p%250AZCIlODAyZTNjNjVjMjllNGZjZjRlOTI4OWFiZDViMTc3NDk6B2lkIiU2Mzlh%250AYzAzNDE4MzhkZmU4NTY1MjhkOTU2NTJjZWJhYyIJcHJycCIAOgl1c2VybCsJ%250AAODU8m8ppQs6CHByc2kMOghwcnVsKwkA4NTybymlCzoIcHJsIitBNmUyZUk2%250AaWtRQ0NJQVVSMlVUa3pFeWZSamtGOVBUTnp6alNoSjoIcHJhaQY%253D--e61e05f991b5b835b1991cfc27ff609b4e7b9c16; lang=en; guest_id_marketing=v1%3A160324540784635715; guest_id_ads=v1%3A160324540784635715; ct0=2a1f6e5c0821d7cbd5a190c8c4563d07; gt=1491235281303126016',
            'pragma': 'no-cache',
            'referer': 'https://twitter.com/bbcnews',
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="98", "Google Chrome";v="98"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.82 Safari/537.36',
            'x-csrf-token': '2a1f6e5c0821d7cbd5a190c8c4563d07',
            'x-guest-token': '{}'.format(self.get_token()),
            'x-twitter-active-user': 'yes',
            'x-twitter-client-language': 'zh-cn',
        }

        params = {
            'variables': '{"userId": "%(user_id)s", "count": 40, "includePromotedContent": "true","cursor":"%(cursor)s",'
                         '"withQuickPromoteEligibilityTweetFields": "true", '
                         '"withSuperFollowsUserFields": "true","withDownvotePerspective": "false", '
                         '"withReactionsMetadata": "false", "withReactionsPerspective": "false",'
                         '"withSuperFollowsTweetFields": "true", "withVoice": "true", '
                         '"withV2Timeline": "false","__fs_dont_mention_me_view_api_enabled": "false", '
                         '"__fs_interactive_text": "false",'
                         '"__fs_responsive_web_uc_gql_enabled": "false"}' % {'user_id': user_id, 'cursor': cursor},
        }
        # print(params)
        # breakpoint()
        try:
            response = requests.get(url, headers=headers, params=params, verify=False)
            if response.status_code == 200:
                html_chariest = cchardet.detect(response.content)
                # print(response.headers["content-type"])
                # print(response.encoding)
                # print(html_chardet)
                response.encoding = html_chariest["encoding"]
                # response = response.text.replace('\\"', '#').replace("\\n","")  # 对json格式中的多余的引号去除
                # html_content = response.text.encode("utf-8").decode("unicode_escape")
                html_content = response.text
                html_content = json.loads(html_content)
                # print(response.content)
                # breakpoint()
                return html_content

        except Exception as e:
            logging.warning('This get_response function ErrorInformation:{}'.format(e))

    def get_data(self, response, writer_csv, media_name):
        # items = response['data']['user']['result']['timeline']['timeline']['instructions'][0]['entries']
        items = response['data']['user']['result']['timeline']['timeline']['instructions']
        # print(response['data']['user']['result'].keys())
        # breakpoint()
        for lst in items:
            if lst.get('type') == 'TimelineAddEntries':
                itemise = lst['entries']
                for item in itemise:
                    item_dict = {}
                    data = item['content']
                    if 'itemContent' in data.keys():
                        try:
                            content = data['itemContent']['tweet_results']['result']['legacy']
                            user_core = \
                                data['itemContent']['tweet_results']['result']['core']['user_results']['result'][
                                    'legacy']
                            user_name = user_core['name']
                            user_screen_name = user_core['screen_name']
                            postdate = content['created_at']
                            utc = datetime.datetime.strptime(postdate, '%a %b %d %H:%M:%S +0000 %Y')  # 格林尼治时间的格式化
                            published_time = str(utc + datetime.timedelta(hours=8))

                            if published_time >= self.since_time:
                                # item_dict['conversation_id'] = content['conversation_id_str']
                                created_at = published_time
                                item_dict['created_at'] = created_at
                                full_text = content['full_text'].replace('\n', '')
                                item_dict['full_text'] = full_text
                                favorite_count = content['favorite_count']
                                item_dict['favorite_count'] = favorite_count
                                quote_count = content['quote_count']
                                item_dict['quote_count'] = quote_count
                                reply_count = content['reply_count']
                                item_dict['reply_count'] = reply_count
                                retweet_count = content['retweet_count']
                                item_dict['retweet_count'] = retweet_count
                                # retweeted = content['retweeted']
                                # item_dict['retweeted'] = retweeted
                                is_quote_status = content['is_quote_status']
                                item_dict['is_quote_status'] = is_quote_status

                                if 'retweeted_status_result' in content.keys():
                                    retweeted = 'True'
                                    retweet_user_information = content['retweeted_status_result']['result']['core']
                                    retweet_username = retweet_user_information['user_results']['result']['legacy'][
                                        'screen_name']
                                    retweet_useridstr = content['retweeted_status_result']['result']['legacy'][
                                        'user_id_str']
                                    retweet_favorite = content['retweeted_status_result']['result']['legacy'][
                                        'favorite_count']
                                    retweet_replay = content['retweeted_status_result']['result']['legacy'][
                                        'reply_count']
                                    retweet_retweet = content['retweeted_status_result']['result']['legacy'][
                                        'retweet_count']
                                    retweet_quote = content['retweeted_status_result']['result']['legacy'][
                                        'quote_count']
                                    retweet_published = content['retweeted_status_result']['result']['legacy'][
                                        'created_at']
                                    retweet_utc = datetime.datetime.strptime(postdate,
                                                                             '%a %b %d %H:%M:%S +0000 %Y')  # 格林尼治时间的格式化
                                    retweet_published_time = str(retweet_utc + datetime.timedelta(hours=8))

                                else:
                                    retweeted = 'False'
                                    retweet_username = ''
                                    retweet_useridstr = ''
                                    retweet_favorite = ''
                                    retweet_replay = ''
                                    retweet_retweet = ''
                                    retweet_quote = ''
                                    retweet_published_time = ''

                                try:
                                    source = re.findall(r'rel="nofollow">(.*?)</a', content['source'])[0]
                                except:
                                    source = content['source']

                                item_dict['source'] = source
                                user_id_str = content['user_id_str']
                                item_dict['user_id_str'] = user_id_str
                                # item_dict['id_str'] = content['id_str']
                                tweet_url = 'https://twitter.com/' + user_screen_name + '/status/' + content['id_str']
                                item_dict['tweet_url'] = tweet_url
                                item_dict['user_name'] = user_name
                                item_dict['user_screen_name'] = user_screen_name
                                self.mongodb().insert(item_dict)
                                item_lst = [media_name, retweeted, user_screen_name, retweet_username,
                                            created_at, full_text, favorite_count, quote_count,
                                            reply_count, retweet_count, source, user_id_str, tweet_url]
                                writer_csv.writerow(item_lst)
                            else:
                                break
                        except:
                            if data['itemContent']['tweet_results']['result']['tombstone']:
                                text = data['itemContent']['tweet_results']['result']['tombstone']['text']['text']
                                logging.warning('Text: {}'.format(text))
                    else:
                        if data['cursorType'] == 'Bottom':
                            cursor = data['value']
                            return cursor

    def loop_user(self, writer_csv, user_id, user_name, media_name):
        # print(get_token())
        # breakpoint()
        url = 'https://twitter.com/i/api/graphql/tXFFoOYy6ovwDaAnr9iT8g/UserTweets?'
        cursor = '0'
        # since_time = time.strftime('%Y-%m-%d 09:00:00', time.localtime(time.time()))
        # since_time = time.strftime('%Y-%m-%d 12:00:00', time.localtime(time.time()))
        while True:
            try:
                response = self.get_response(url, user_id, cursor)
                bottom_cursor = self.get_data(response, writer_csv, media_name)
                if bottom_cursor == cursor:  # Max Limit 850 tweet
                    break
                logging.warning('UserName: {} Cursor: {}'.format(user_name, bottom_cursor))
                cursor = bottom_cursor
                if not bottom_cursor:
                    break
                time.sleep(3)
            except Exception as e:
                logging.warning('This loop_user function ErrorInformation:{}'.format(e))

    @staticmethod
    def get_account():
        with open(r'twitter_user_account', encoding='utf-8') as f:
            user_accounts = f.readlines()
        return user_accounts

    def start_run(self, user_accounts, writer_csv):
        media_json = {
            "bbcafrica": "BBC", "bbcbreaking": "BBC", "BBCMOTD": "BBC", "bbcnews": "BBC",
            "bbcpolitics": "BBC", "bbcsport": "BBC", "bbctms": "BBC", "bbcworld": "BBC",
            "ap": "美联社", "AP_Africa": "美联社", "ap_europe": "美联社", "ap_politics": "美联社",
            "AP_Sports": "美联社", "AP_Top25": "美联社", "APBusiness": "美联社", "APEntertainment": "美联社",
            "cnnpolitics": "CNN", "cnnbrk": "CNN", "cnni": "CNN", "cnn": "CNN", "TheLeadCNN": "CNN",
            "CNNSitRoom": "CNN",
            "reutersworld": "路透社", "reutersuk": "路透社", "reuters": "路透社", "reuterspolitics": "路透社",
            "reuterschina": "路透社",
            "afp": "法新社", "AFPphoto": "法新社",
            "rt_america": "俄罗斯RT", "rt_com": "俄罗斯RT", "rtuknews": "俄罗斯RT",
            "cgtnafrica": "CGTN", "cgtnamerica": "CGTN", "cgtneurope": "CGTN", "cgtnofficial": "CGTN",
            "AJafricadirect": "AJ", "ajenglish": "AJ", "ajenews": "AJ",
        }
        for user in user_accounts:
            user = user.strip()
            user_id = user.split('##########')[1].strip()
            user_name = user.split('##########')[0].strip()
            media_name = media_json.get(user_name)
            self.loop_user(writer_csv, user_id, user_name, media_name)
            time.sleep(1)

    def create_file(self):
        file_time = time.strftime('%Y-%m-%d', time.localtime(time.time()))
        user_accounts = self.get_account()
        with open(r'{}-all.csv'.format(file_time), 'a+', newline='', encoding='gb18030') as file:
            fieldnames = ['media_name', 'retested', 'user_screen_name', 'retested_username', 'created_at',
                          'full_text', 'favorite_count', 'quote_count', 'reply_count',
                          'retested_count', 'source', 'user_id_str', 'tweet_url'
                          ]

            writer_csv = csv.writer(file)
            writer_csv.writerow(fieldnames)

            self.start_run(user_accounts, writer_csv)


if __name__ == '__main__':
    since_time = time.strftime('%Y-%m-%d 09:00:00', time.localtime(time.time()))
    TwitterSpider = Twitter(since_time)
    TwitterSpider.create_file()
