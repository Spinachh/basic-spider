#!/usr/bin/python3
# -*- coding:utf-8 -*-
# Author : Mongoole
# Date : 2022/3/25

import os
import sys
import time
import logging
import subprocess


def monitor(file_path, file_name):

    if os.path.exists(file_path + file_name):
        logging.info('File was exist! We can have a rest now!')
        return
    else:
        logging.info('File was not exist! Start crawl now!')
        subprocess.run(['python3', 'twitter-api-pm.py'])


def run():
    file_path = r'/home/vsftpd/hqyq/pm_twitter/'
    file_time = time.strftime('%Y-%m-%d', time.localtime(time.time()))
    file_name = file_time + "-all-pm.csv"
    logging.basicConfig(level=logging.INFO, filename=r'log/pm/monitor-pm.log',
                        format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s')
    monitor(file_path, file_name)


if __name__ == '__main__':
    run()
