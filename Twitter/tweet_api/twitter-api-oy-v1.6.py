#!/usr/bin/python3
# -*- coding:utf-8 -*-
# Author : Mongoole
# Date : 2024/02/29

import os
import io
import re
import csv
import sys
import time
import random
import datetime
import json
import asyncio
import urllib3
import logging
import subprocess
import cchardet
import requests

urllib3.disable_warnings()
file_name = time.strftime('%Y-%m-%d', time.localtime(time.time()))
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')  # 改变标准输出的默认编码
logging.basicConfig(level=logging.WARN, filename=r'log/am/am-{}.log'.format(file_name),
                    format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s')

agents = [
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36 "
    "OPR/26.0.1656.60",
    "Opera/8.0 (Windows NT 5.1; U; en)",
    "Mozilla/5.0 (Windows NT 5.1; U; en; rv:1.8.1) Gecko/20061208 Firefox/2.0.0 Opera 9.50",
    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; en) Opera 9.50",
    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0",
    "Mozilla/5.0 (X11; U; Linux x86_64; zh-CN; rv:1.9.2.10) Gecko/20100922 Ubuntu/10.10 (maverick) Firefox/3.6.10",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36",
    "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11",
    "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.133 "
    "Safari/534.16",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36",
    "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.11 TaoBrowser/2.0 "
    "Safari/536.11",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.71 Safari/537.1 "
    "LBBROWSER",
    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; QQDownload 732; .NET4.0C; .NET4.0E)",
    "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.84 Safari/535.11 SE 2.X "
    "MetaSr 1.0",
    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; SV1; QQDownload 732; .NET4.0C; .NET4.0E; SE 2.X "
    "MetaSr 1.0) ",
]


def get_token():
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:93.0) Gecko/20100101 Firefox/93.0',
        'Accept': '*/*',
        'Accept-Language': 'zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2',
        'x-guest-token': '',
        'x-twitter-client-language': 'zh-cn',
        'x-twitter-active-user': 'yes',
        'x-csrf-token': '25ea9d09196a6ba850201d47d7e75733',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'authorization': 'Bearer AAAAAAAAAAAAAAAAAAAAANRILgAAAAAAnNwIzUejRCOuH5E6I8xnZz4puTs'
                         '%3D1Zv7ttfk8LF81IUq16cHjhLTvJu4FA33AGWWjCpTnA',
        'Referer': 'https://twitter.com/',
        'Connection': 'keep-alive',
    }
    response = requests.post('https://api.twitter.com/1.1/guest/activate.json',
                             headers=headers, verify=False)
    content = json.loads(response.text)
    guest_token = content['guest_token']
    return guest_token


def get_response(url, user_id, cursor):
    params = {
        'variables':
            '{"userId":"%(user_id)s","count":20,"includePromotedContent":true,"cursor":"%(cursor)s",'
            '"withQuickPromoteEligibilityTweetFields":true,"withVoice":true,"withV2Timeline":true}' % {'user_id': user_id, 'cursor': cursor},
        'features':
            '{"rweb_lists_timeline_redesign_enabled":true,"responsive_web_graphql_exclude_directive_enabled":true,'
            '"verified_phone_label_enabled":false,"creator_subscriptions_tweet_preview_api_enabled":true,'
            '"responsive_web_graphql_timeline_navigation_enabled":true,'
            '"responsive_web_graphql_skip_user_profile_image_extensions_enabled":false,'
            '"tweetypie_unmention_optimization_enabled":true,"responsive_web_edit_tweet_api_enabled":true,'
            '"graphql_is_translatable_rweb_tweet_is_translatable_enabled":true,'
            '"view_counts_everywhere_api_enabled":true,"longform_notetweets_consumption_enabled":true,'
            '"responsive_web_twitter_article_tweet_consumption_enabled":false,"tweet_awards_web_tipping_enabled":false,'
            '"freedom_of_speech_not_reach_fetch_enabled":true,"standardized_nudges_misinfo":true,'
            '"tweet_with_visibility_results_prefer_gql_limited_actions_policy_enabled":true,'
            '"longform_notetweets_rich_text_read_enabled":true,"longform_notetweets_inline_media_enabled":true,'
            '"responsive_web_media_download_video_enabled":false,"responsive_web_enhance_cards_enabled":false}',
        'fieldToggles': '{"withAuxiliaryUserLabels":false,"withArticleRichContentState":false}',
    }

    headers = {
        'authority': 'twitter.com',
        'accept': '*/*',
        'accept-language': 'zh-CN,zh;q=0.9',
        'authorization': '',
        'content-type': 'application/json',
        'cookie': '',
        'referer': 'https://twitter.com/',
        'sec-ch-ua': '"Not A(Brand";v="99", "Google Chrome";v="121", "Chromium";v="121"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36',
        'x-client-transaction-id': '',
        'x-client-uuid': '',
        'x-csrf-token': '',
        'x-twitter-active-user': 'yes',
        'x-twitter-auth-type': 'OAuth2Session',
        'x-twitter-client-language': 'zh-cn',
    }

    params1 = (
        ('variables',
         '{"userId":"%s","count":20,"includePromotedContent":true,"withQuickPromoteEligibilityTweetFields":true,'
         '"withVoice":true,"withV2Timeline":true, "cursor":"%s"}' % (user_id, cursor)),
        ('features',
         '{"responsive_web_graphql_exclude_directive_enabled":true,"verified_phone_label_enabled":false,"creator_subscriptions_tweet_preview_api_enabled":true,"responsive_web_graphql_timeline_navigation_enabled":true,"responsive_web_graphql_skip_user_profile_image_extensions_enabled":false,"c9s_tweet_anatomy_moderator_badge_enabled":true,"tweetypie_unmention_optimization_enabled":true,"responsive_web_edit_tweet_api_enabled":true,"graphql_is_translatable_rweb_tweet_is_translatable_enabled":true,"view_counts_everywhere_api_enabled":true,"longform_notetweets_consumption_enabled":true,"responsive_web_twitter_article_tweet_consumption_enabled":true,"tweet_awards_web_tipping_enabled":false,"freedom_of_speech_not_reach_fetch_enabled":true,"standardized_nudges_misinfo":true,"tweet_with_visibility_results_prefer_gql_limited_actions_policy_enabled":true,"rweb_video_timestamps_enabled":true,"longform_notetweets_rich_text_read_enabled":true,"longform_notetweets_inline_media_enabled":true,"responsive_web_enhance_cards_enabled":false}'),
    )

    proxy = {
        "https": '127.0.0.1:55375',
    }
    try:
        # response = requests.get(url, headers=headers, params=params, verify=False)
        response = requests.get('https://twitter.com/i/api/graphql/WmvfySbQ0FeY1zk4HU_5ow/UserTweets',
                                headers=headers, params=params1, proxies=proxy, verify=False)
        # print('response:', response.text)
        # breakpoint()
        if response.status_code == 200:
            html_chariest = cchardet.detect(response.content)
            # print(response.headers["content-type"])
            # print(response.encoding)
            # print(html_chardet)
            response.encoding = html_chariest["encoding"]
            # response = response.text.replace('\\"', '#').replace("\\n","")  # 对json格式中的多余的引号去除
            html_content = response.text.encode("utf-8").decode("unicode_escape")
            html_content = response.text
            # print(html_content)
            # breakpoint
            html_content = json.loads(html_content)
            return html_content

    except Exception as e:
        print(e)


def get_data(response, writer_csv, since_time):
    # items = response['data']['user']['result']['timeline']['timeline']['instructions'][0]['entries']
    # items = response['data']['user']['result']['timeline']['timeline']['instructions']
    items = response['data']['user']['result']['timeline_v2']['timeline']['instructions']
    # print(response)
    # breakpoint
    for lst in items:
        if lst.get('type') == 'TimelineAddEntries':
            itemise = lst['entries']
            # print(itemise)
            # breakpoint()
            for item in itemise:
                item_dict = {}
                data = item['content']
                entryId = item['entryId']
                if 'itemContent' in data.keys() and 'promoted-tweet' not in entryId:
                    try:
                        content_flag = data['itemContent']['tweet_results']['result']
                        if 'tweet' not in content_flag.keys():
                            content_result = data['itemContent']['tweet_results']['result']
                            content = content_result['legacy']
                            user_core = \
                            data['itemContent']['tweet_results']['result']['core']['user_results']['result']['legacy']
                        else:
                            content_result = data['itemContent']['tweet_results']['result']['tweet']
                            content = content_result['legacy']
                            user_core = \
                            data['itemContent']['tweet_results']['result']['tweet']['core']['user_results']['result'][
                                'legacy']
                        user_name = user_core['name']
                        user_screen_name = user_core['screen_name']
                        postdate = content['created_at']
                        utc = datetime.datetime.strptime(postdate, '%a %b %d %H:%M:%S +0000 %Y')  # 格林尼治时间的格式化
                        published_time = str(utc + datetime.timedelta(hours=8))
                        # print(postdate)
                        # print(published_time)
                        # breakpoint()
                        if published_time >= since_time:
                            # item_dict['conversation_id'] = content['conversation_id_str']
                            created_at = published_time
                            item_dict['created_at'] = created_at
                            full_text = content['full_text'].replace('\n', '')
                            item_dict['full_text'] = full_text
                            favorite_count = content['favorite_count']
                            item_dict['favorite_count'] = favorite_count
                            quote_count = content['quote_count']
                            item_dict['quote_count'] = quote_count
                            reply_count = content['reply_count']
                            item_dict['reply_count'] = reply_count
                            retweet_count = content['retweet_count']
                            item_dict['retweet_count'] = retweet_count
                            # retweeted = content['retweeted']
                            # item_dict['retweeted'] = retweeted
                            is_quote_status = content['is_quote_status']
                            item_dict['is_quote_status'] = is_quote_status
                            # retweeted_flag = content['retweeted']
                            # print(item_dict)
                            # breakpoint()
                            if 'retweeted_status_result' in content.keys():
                                retweeted = 'True'
                                try:
                                    retweet_user_information = content['retweeted_status_result']['result']['core']
                                except:
                                    retweet_user_information = content['retweeted_status_result']['result']['tweet'][
                                        'core']
                                retweet_username = retweet_user_information['user_results']['result']['legacy'][
                                    'screen_name']
                                # retweet_useridstr = content['retweeted_status_result']['result']['legacy']['user_id_str']
                                # retweet_favorite = content['retweeted_status_result']['result']['legacy']['favorite_count']
                                # retweet_replay = content['retweeted_status_result']['result']['legacy']['reply_count']
                                # retweet_retweet = content['retweeted_status_result']['result']['legacy']['retweet_count']
                                # retweet_quote = content['retweeted_status_result']['result']['legacy']['quote_count']
                                # retweet_published = content['retweeted_status_result']['result']['legacy']['created_at']
                                # retweet_utc = datetime.datetime.strptime(postdate, '%a %b %d %H:%M:%S +0000 %Y')  # 格林尼治时间的格式化
                                # retweet_published_time = str(retweet_utc + datetime.timedelta(hours=8))

                            else:
                                retweeted = 'False'
                                retweet_username = ''
                                # retweet_useridstr = ''
                                # retweet_favorite = ''
                                # retweet_replay = ''
                                # retweet_retweet = ''
                                # retweet_quote = ''
                                # retweet_published_time = ''

                            try:
                                source = re.findall(r'rel="nofollow">(.*?)</a', content_result['source'])[0]
                            except:
                                source = content['source']

                            item_dict['source'] = source
                            user_id_str = content['user_id_str']
                            item_dict['user_id_str'] = user_id_str
                            # item_dict['id_str'] = content['id_str']
                            tweet_url = 'https://twitter.com/' + user_screen_name + '/status/' + content['id_str']
                            item_dict['tweet_url'] = tweet_url
                            item_dict['user_name'] = user_name
                            item_dict['user_screen_name'] = user_screen_name
                            # mongodb.insert(item_dict)
                            item_lst = [retweeted, user_screen_name, retweet_username,
                                        created_at, full_text, favorite_count, quote_count,
                                        reply_count, retweet_count, source, user_id_str, tweet_url]
                            # print(item_lst)
                            # breakpoint()
                            # with open('aaa.txt', 'a', encoding='utf-8') as f:
                            #     f.write(str(item_lst) + '\n')
                            writer_csv.writerow(item_lst)
                            # logging.warning(item_lst)
                            # breakpoint()
                        else:
                            break
                    except Exception as e:
                        logging.warning('itemContent Info was Error: {}'.format(e))
                        if data['itemContent']['tweet_results']['result']['tombstone']:
                            text = data['itemContent']['tweet_results']['result']['tombstone']['text']['text']
                            logging.info('Text: {}'.format(text))

                else:
                    try:
                        if data['cursorType'] == 'Bottom':
                            cursor = data['value']
                            return cursor
                    except:
                        pass


def loop_user(writer_csv, user_id, user_name):
    # guest_token = get_token()
    # url = 'https://twitter.com/i/api/graphql/tXFFoOYy6ovwDaAnr9iT8g/UserTweets?'
    # url = 'https://api.twitter.com/graphql/E-dqdIghLd9KohBJXqTsWg/UserTweets?'
    url = 'https://twitter.com/i/api/graphql/WmvfySbQ0FeY1zk4HU_5ow/UserTweets?'
    cursor = '0'
    # since_time = time.strftime('%Y-%m-%d 09:00:00', time.localtime(time.time()))
    since_time = (datetime.datetime.now() - datetime.timedelta(days=1)).strftime("%Y-%m-%d 00:00:00")
    while True:
        response = get_response(url, user_id, cursor)
        # print(response)
        # breakpoint()
        bottom_cursor = get_data(response, writer_csv, since_time)
        if bottom_cursor == cursor:  # Max Limit 850 tweet
            break
        logging.warning('LOOP UserName: {} Cursor: {}'.format(user_name, bottom_cursor))
        cursor = bottom_cursor
        if not bottom_cursor:
            break
        time.sleep(3)


def get_account():
    # user_id = "380648579"
    with open(r'twitter_user_account_oy', encoding='utf-8') as f:
        user_accounts = f.readlines()
    # user_accounts = ['LuisEnr33483555##########1242798099610906625']
    return user_accounts


def start_run(user_accounts, writer_csv):
    # user_accounts = ['cgtnofficial##########1115874631']
    for user in user_accounts:
        user = user.strip()
        user_id = user.split('##########')[1]
        user_name = user.split('##########')[0]
        loop_user(writer_csv, user_id, user_name)
        time.sleep(60)


def main():
    user_accounts = get_account()

    with open(r'results/am/oy/{}-all-am.csv'.format(file_name), 'a+', newline='', encoding='gb18030') as f:
        fieldnames = ['retweeted', 'user_screen_name', 'retweet_username', 'created_at',
                      'full_text', 'favorite_count', 'quote_count', 'reply_count',
                      'retweet_count', 'source', 'user_id_str', 'tweet_url'
                      ]

        writer_csv = csv.writer(f)
        writer_csv.writerow(fieldnames)

        start_run(user_accounts, writer_csv)


if __name__ == '__main__':
    main()
    # try:
    #     subprocess.run(['sh', '-x', 'am-zzzip.sh'])
    # except Exception as e:
    #     logging.ingo('Subprocess was Error: {}'.format(e))
