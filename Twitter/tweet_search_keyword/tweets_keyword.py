import pymongo
import tweepy  # open source library to access Twitter's API

from tweet_api.settings import consumer_key,consumer_secret,access_token,access_token_secret

def mongo():
    # 连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # 选择数据库
    db = client.Twitter
    # 选择操作的集合
    coll = db.search_keyword
    return coll

def get_keyword_content(api,keyword,coll):

    ##Perfect Soved This Problem(1.Full long Text! 2.Count of per Page)
    try:
        tweets = tweepy.Cursor(api.search,count=10,q=keyword,tweet_mode='extended').items()
        for tweet in tweets:
            tweets = {}
            tweets['id'] = tweet.id
            tweets['text'] = tweet.full_text.split("https:")[0].strip().strip()
            tweets['created_at'] = tweet.created_at
            tweets['source'] = tweet.source
            tweets['user_name'] = tweet.user.screen_name          #repost tweet user
            tweets['repost_name'] = tweet.user.screen_name
            tweets['favorite_count'] = tweet.favorite_count
            tweets['retweet_count'] = tweet.retweet_count
            tweets['full_url'] = 'https://twitter.com/' + tweet.user.screen_name + '/status/' + str(tweet.id)

            coll.insert(tweets)
            print("==========%s Insert Into MongoDB Successfully!=========="%tweets['text'])
    except:
        print("==========Only has got These information! Thank You!==========")

def main():
    coll = mongo()
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)
    keyword = 'once upon a time in hollywood'
    content = get_keyword_content(api,keyword,coll)

if __name__ == '__main__':
    main()