import time
import json
import base64
import pandas as pd
import matplotlib.pyplot as plt
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from PIL import Image

# 输入你的账号
username = ''
# 输入你的密码
password = ''

driver = webdriver.Chrome()


class Start:
    def __init__(self):
        self.url = 'https://passport.bilibili.com/login'
        self.browser = driver
        self.wait = WebDriverWait(self.browser, 20)
        self.name = username
        self.pw = password

    def get_login_button(self):
        """
        获取初始登录按钮
        :return: 按钮对象
        """
        button = self.wait.until(
            EC.presence_of_element_located((By.XPATH, "//a[contains(@class,'btn') and contains(@class, 'btn-login')]")))
        return button

    def get_slider_button(self):
        """
        获取拖动碎片的地方
        :return: 拖动对象
        """
        sliderbutton = self.wait.until(
            EC.presence_of_element_located((By.XPATH, "//div[@class='geetest_slider_button']")))
        return sliderbutton

    def get_login_input(self):
        """
        获取登陆输入框(用户名/密码)
        :return: 输入框对象
        """
        user_login = self.wait.until(EC.presence_of_element_located((By.XPATH, "//input[@id='login-username']")))
        pw_login = self.wait.until(EC.presence_of_element_located((By.XPATH, "//input[@id='login-passwd']")))
        return user_login, pw_login

    def save_pic(self, data, filename):
        """
        解码获取到的base64再写入到文件中，保存图片
        :return:
        """
        data = data.split(',')[1]
        data = base64.b64decode(data)
        with open(filename, 'wb') as f:
            f.write(data)

    def get_pic(self):
        """
        获取无缺口图片和有缺口图片
        :return: 图片对象
        """
        # 图片对象的类名
        # 首先需要这个东西已经出现了，我们才能去执行相关的js代码
        picName = ['full.png', 'slice.png']
        className = ['geetest_canvas_fullbg', 'geetest_canvas_bg']
        # canvas标签中的图片通过js代码获取base64编码
        for i in range(len(className)):
            js = "var change = document.getElementsByClassName('" + className[i] \
                 + "'); return change[0].toDataURL('image/png');"
            im_info = self.browser.execute_script(js)
            self.save_pic(im_info, picName[i])

    def is_pixel_equal(self, image1, image2, x, y):
        """
        判断两个像素点是否是相同
        :param image1: 不带缺口图片
        :param image2: 带缺口图片
        :param x: 像素点的x坐标
        :param y: 像素点的y坐标
        :return:
        """
        pixel1 = image1.load()[x, y]
        pixel2 = image2.load()[x, y]
        threshold = 40
        if abs(pixel1[0] - pixel2[0]) < threshold \
                and abs(pixel1[1] - pixel2[1]) < threshold \
                and abs(pixel1[2] - pixel2[2]) < threshold:
            return True
        else:
            return False

    def get_gap(self, image1, image2):
        """
        获取缺口偏移量
        :param image1: 不带缺口图片
        :param image2: 带缺口图片
        :return:
        """
        # 这个可以自行操作一下，如果发现碎片对不准，可以调整
        left = 10
        for i in range(left, image1.size[0]):
            for j in range(image1.size[1]):
                if not self.is_pixel_equal(image1, image2, i, j):
                    left = i
                    return left
        return left

    def get_track(self, distance):
        """
        根据偏移量获取移动轨迹
        :param self:
        :param distance: 偏移量
        :return: 移动轨迹
        """
        # 移动轨迹
        track = []
        # 当前位移
        current = 0
        # 因为老对不的不准确，所以自行调整一下distance
        distance = distance - 9
        # 减速阈值 -> 也就是加速到什么位置的时候开始减速
        mid = distance * 4 / 5
        # 计算间隔
        t = 0.2
        # 初速度
        v = 0

        while current < distance:
            if current < mid:
                # 加速度为正2
                a = 2
            else:
                # 加速度为负3
                a = -3
            v0 = v
            v = v0 + a * t
            move = v0 * t + 1 / 2 * a * t * t
            current += move
            track.append(round(move))
        return track

    def test(self):
        '''
        # 输入用户名和密码
        self.browser.get(self.url)
        user_login, pw_login = self.get_login_input()
        user_login.send_keys(self.name)
        pw_login.send_keys(self.pw)
        # 点击按钮对象
        button = self.get_login_button()
        button.click()
        # 保存图片
        time.sleep(3)
        self.get_pic()
        image1 = Image.open('full.png')
        image2 = Image.open('slice.png')
        left = self.get_gap(image1, image2)
        track = self.get_track(left)
        slider = self.get_slider_button()
        self.move_to_gap(slider, track, self.browser)
        time.sleep(1)
        '''
        i = 1
        content_lst = []
        self.get_danmu(i, content_lst)
        self.browser.close()
        # print(content_dict)
        self.matp(content_lst)

    def move_to_gap(self, slider, tracks, browser):
        """
        拖动滑块到缺口处
        :param self:
        :param slider: 滑块
        :param tracks: 轨迹
        :return:
        """
        # click_and_hold()点击鼠标左键，不松开
        ActionChains(self.browser).click_and_hold(slider).perform()
        for x in tracks:
            # move_by_offset()鼠标从当前位置移动到某个坐标
            ActionChains(self.browser).move_by_offset(xoffset=x, yoffset=0).perform()
        time.sleep(0.5)
        # release()在某个元素位置松开鼠标左键
        ActionChains(self.browser).release().perform()

    def get_danmu(self, i, content_lst):
        while i != 5:
            JS = 'window.open("https://www.bilibili.com/video/BV1Hx411G7eg?p=%s");' % i
            driver.execute_script(JS)
            time.sleep(5)
            windows = driver.window_handles
            driver.switch_to_window(windows[0])
            driver.close()
            windows = driver.window_handles
            driver.switch_to_window(windows[-1])
            danmaku_count = driver.find_element_by_class_name('bilibili-player-video-info-danmaku-number').text
            # print(danmaku_count)
            content_lst.append(int(danmaku_count))
            i += 1

    def matp(self, content_lst):
        import requests
        from matplotlib.pylab import style
        # 坐标标签设置为中文显示
        style.use('ggplot')
        plt.rcParams['font.sans-serif'] = ['SimHei']
        plt.rcParams['axes.unicode_minus'] = False

        # 设置x轴名称竖立显示
        plt.xticks(rotation=90)

        # 获取x轴上的视频文件名称
        response = requests.get(url='https://api.bilibili.com/x/player/pagelist?bvid=BV1Hx411G7eg&jsonp=jsonp').json()
        data = response['data']
        x_lst = [name['part'] for name in data][0:4]
        print(x_lst)
        print(content_lst)
        # 写入Excel
        if len(content_lst) == len(x_lst):
            dict1 = {'name':x_lst,'num':content_lst}
            pd.DataFrame(dict1).to_excel('%s.xlsx' % (x_lst[0]))


        # print(content_lst)
        # content_lst = [84, 25, 42, 14]
        # x_lst = ['p' + str(x) for x in range(1,len(content_lst)+1)]

        for rect in plt.bar(range(1, len(content_lst) + 1), content_lst, width=0.6, color='rgb', tick_label=x_lst):
            height = rect.get_height()
            plt.text(rect.get_x() + rect.get_width() / 2, 1.02 * height, '%s' % int(height))
        plt.title('Danmaku Relationship Chart', fontsize=24)
        plt.ylabel('Danmaku Count', fontsize=18)
        plt.xlabel('Pvideo Number', fontsize=18)
        plt.show()

Start().test()