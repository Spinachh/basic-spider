# -*- coding:gb18030 -*-
# author:mongoole
# Date:2023-07-21
# Version: v4(code review w_rid)

import io
import json
import random
import sys
import time
import requests
import pymongo
import redis
import hashlib
import logging
import httpx
from ws_rid import encWbi

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')
HEADERS = {"User-Agent": "Mozilla/5.0", "Referer": "https://www.bilibili.com"}


class BiliBili(object):

    def __init__(self, sin_time, pro_name, pro_timezone):
        self.sin_time = sin_time
        self.pro_name = pro_name
        self.pro_timezone = pro_timezone
        self.crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

    # @staticmethod
    # def logger_init():
    #     # log format
    #     logging.basicConfig(format='%(asctime)s,%(msecs)d %(levelname)-4s %(message)s',
    #                         datefmt='%Y-%m-%d %H:%M:%S')
    #     # log level
    #     # logging.getLogger().setLevel(logging.DEBUG)
    #     logging.getLogger().setLevel(logging.WARN)
    #     # logging.getLogger().setLevel(logging.CRITICAL)
    #     logger = logging.getLogger()

    @staticmethod
    def conn_redis():
        pool = redis.ConnectionPool(host="127.0.0.1", port=6379)
        conn = redis.Redis(connection_pool=pool)
        return conn

    @staticmethod
    def redis_filter(conn, url):
        # url����sha256����
        hash_str = hashlib.sha256(url.encode()).hexdigest()
        # redis����url��hashֵ
        redis_save_res = conn.sadd('Bilibili_video_url_hash', hash_str)
        return redis_save_res

    @staticmethod
    def proxy():
        # ����������
        tunnel = "tps121.kdlapi.com:15818"

        # ����id������
        username = "t17535136923494"
        password = "lwUvJ8dK6All"
        proxies = {
            "http": "http://%(user)s:%(pwd)s@%(proxy)s/" % {"user": username, "pwd": password, "proxy": tunnel},
            "https": "http://%(user)s:%(pwd)s@%(proxy)s/" % {"user": username, "pwd": password, "proxy": tunnel}
        }

        return proxies

    @staticmethod
    def read_file():
        with open(r'user_scienceTec/author_scienceTec2') as f:
            lines = f.readlines()
        return lines

    @staticmethod
    def mongodb():
        # �������ݿ�mongodb
        client = pymongo.MongoClient(host='127.0.0.1', port=27017)
        # ѡ�����ݿ�
        db = client.bilibili_content
        # ѡ������ļ���
        mongo = db.user_content_2023_Q2
        return mongo

    @staticmethod
    def get_page_response(uid, page=1):
        url = 'https://api.bilibili.com/x/space/wbi/arc/search'

        url_params = {
            "mid": uid,
            "ps": "30",
            "tid": "0",
            "pn": page,
            "keyword": "",
            "order": "pubdate",
            "platform": "web",
            "web_location": "1550101",
            "order_avoided": "true",
        }
        w_rid, wts = encWbi(url_params)

        params = {
            "w_rid": w_rid,
            "wts": wts,
        }
        params.update(url_params)

        resp = httpx.get(url, headers=HEADERS, params=params)
        return resp

    def get_page_num(self, uid):
        resp = self.get_page_response(uid)
        content = json.loads(resp.text)
        if 'page' in content['data'].keys():
            page_count = content['data']['page']['count']
            page_num = (page_count + 30) // 30
        else:
            page_num = 0
            logging.warning('THIS ACCOUNT HAS NOT CONTENT!')

        return page_num

    @staticmethod
    def parse_once_page(video_url):

        resp = requests.get(url=video_url, headers=HEADERS)
        video_detail = json.loads(resp.text)

        detail_informatin = {}
        if 'stat' in video_detail['data'].keys():
            # coin , share, like
            detail_info = video_detail["data"]["stat"]
            detail_informatin["aid"] = video_detail["data"]["aid"]
            detail_informatin["title"] = video_detail["data"]["title"]
            detail_informatin["description"] = video_detail["data"]["desc"]
            detail_informatin["bvid"] = video_detail["data"]["bvid"]
            detail_informatin["length"] =  video_detail["data"]["duration"]
            detail_informatin['true_view'] = detail_info['view']
            detail_informatin['danmaku'] = detail_info['danmaku']
            detail_informatin['reply'] = detail_info['reply']
            detail_informatin['favorite'] = detail_info['favorite']
            detail_informatin['coin'] = detail_info['coin']
            detail_informatin['share'] = detail_info['share']
            detail_informatin['like'] = detail_info['like']
            detail_informatin['dislike'] = detail_info['dislike']
            time.sleep(0.1)

        else:
            logging.warning('THIS VIDEO URL {} HAS NOT DETAIL INFORMATION.'.format(video_url))

        return detail_informatin

    def get_data_lst(self, uid, page_num, uname):

        for page in range(1, int(page_num) + 1):
            resp = self.get_page_response(uid, page)
            content_lst = json.loads(resp.text)  # ��ȡ����Nҳ����Ƶ�б�
            if 'vlist' in content_lst['data']['list'].keys():
                video_lst = content_lst['data']['list']['vlist']
                created_time = int(video_lst[0]['created'])
                if created_time > int(self.sin_time):
                    for video in video_lst:
                        video_lst_dict = {}
                        # crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
                        video_created = video["created"]
                        struct_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(video_created))
                        video_lst_dict["author"] = video["author"]
                        video_lst_dict["created"] = struct_time

                        video_lst_dict["play"] = video["play"]
                        video_lst_dict["video_review"] = video["video_review"]
                        video_lst_dict["comment"] = video["video_review"]

                        video_url = 'https://api.bilibili.com/x/web-interface/view?aid={}'.format(video["aid"])
                        video_lst_dict["video_url"] = video_url

                        # get video detail information (coin_count,read_count,)
                        video_detail_result = self.parse_once_page(video_url)
                        time.sleep(3)

                        video_lst_dict.update(video_detail_result)
                        video_lst_dict["crawl_time"] = self.crawl_time
                        video_lst_dict["pro_name"] = self.pro_name
                        video_lst_dict["pro_timezone"] = self.pro_timezone
                        try:
                            self.mongodb().insert_one(video_lst_dict)
                            # logging.warning(video_lst_dict)
                            logging.warning('UNAME:{} TITLE:{} WAS FINISHED.'.format(uname, video['title']))
                            time.sleep(1)
                        except:
                            logging.warning('UNAME:{} UID:{} HAS EMPTY DATA.'.format(uname, uid))
                else:
                    logging.warning('UNAME: {} UID: {} HAS NOT MATCH DATA.'.format(uname, uid))
                    break

    def start_run(self):
        users = self.read_file()
        for user in users:
            item = user.split("#####")
            uid = item[0].split("/")[-2]
            uname = item[1]
            uid = uid.strip()
            page_num = self.get_page_num(uid)
            # print(page_num)
            # breakpoint()
            if page_num != 0:
                self.get_data_lst(uid, page_num, uname)
            else:
                break

            time.sleep(3)


if __name__ == '__main__':
    since_time = time.mktime(time.strptime('2023-04-01 00:00:00', '%Y-%m-%d %H:%M:%S'))
    project_name = "kexie"
    project_timezone = "2023-Q2"
    BiliSpider = BiliBili(since_time, project_name, project_timezone)
    BiliSpider.start_run()
