# -*- coding:gb18030 -*-

import time
import chardet
import re
import requests
import pymongo
import redis
import hashlib
from fake_useragent import UserAgent


def conn_redis():
    pool = redis.ConnectionPool(host="127.0.0.1", port=6379)
    conn = redis.Redis(connection_pool=pool)
    return conn


def redis_filter(conn, url):
    # url进行sha256加密
    hash_str = hashlib.sha256(url.encode()).hexdigest()
    # redis存入url的hash值
    redis_save_res = conn.sadd('Bilibili_video_url_hash', hash_str)
    return redis_save_res


def conn_mongo():
    # 连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # 选择数据库
    db = client.bilibili_content
    # 选择操作的集合
    p = db.user_content
    return p


def get_page_num(url, headers):
    html = requests.get(url, headers=headers)
    content = html.json()
    if content:
        if 'data' in content:
            page_count = content['data']['page']['count']
            # print(page_count,type(page_count))
            page_num = (page_count + 30) // 30

    return page_num


def proxy():
    # 隧道服务器
    tunnel = "tps121.kdlapi.com:15818"

    # 隧道id和密码
    username = "t17535136923494"
    password = "lwUvJ8dK6All"
    proxies = {
        "http": "http://%(user)s:%(pwd)s@%(proxy)s/" % {"user": username, "pwd": password, "proxy": tunnel},
        "https": "http://%(user)s:%(pwd)s@%(proxy)s/" % {"user": username, "pwd": password, "proxy": tunnel}
    }

    return proxies


def parse_one_html(url, project_name, project_timezone):
    since_time = time.mktime(time.strptime('2022-01-01 00:00:00', '%Y-%m-%d %H:%M:%S'))
    # since_time = time.mktime(time.strptime('2012-06-30 00:00:00','%Y-%m-%d %H:%M:%S'))
    headers = {
        'User-Agent': str(UserAgent().random),
        'referer': 'https://space.bilibili.com/',
    }
    html = requests.get(url, headers=headers)
    # print(html.text)
    # breakpoint()
    content = html.json()

    if 'vlist' in content['data']['list'].keys():
        video_lst = content['data']['list']['vlist']

        created_time = video_lst[0]['created']

        if created_time > since_time:
            flag = 1
            for item in video_lst:
                video_created = item['created']
                struct_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(video_created))
                item["created"] = struct_time
                # 获取详情视频的三连数据。

                aid = item['aid']
                video_url = 'https://api.bilibili.com/x/web-interface/view?aid=%s' % aid

                response = requests.get(video_url, headers=headers)
                if response.status_code == 200:
                    video_detail = response.json()
                    # 主要的一键三连数据
                    item['true_view'] = video_detail['data']['stat']['view']
                    item['danmaku'] = video_detail['data']['stat']['danmaku']
                    item['favorite'] = video_detail['data']['stat']['favorite']
                    item['coin'] = video_detail['data']['stat']['coin']
                    item['share'] = video_detail['data']['stat']['share']
                    item['like'] = video_detail['data']['stat']['like']
                    item['dislike'] = video_detail['data']['stat']['dislike']
                    # item["project_name"] = "MainMediaResearch"
                    item["project_name"] = project_name
                    item["project_timezone"] = project_timezone
                    time.sleep(2)

                else:
                    print('THIS URL  {}  HAS NOT CONTENT !'.format(video_url))

            return video_lst, flag

        else:
            flag = 0
            return '', flag


def read_file():
    with open('bilibili_user_kexie') as f:
        # with open('bilibili_user_lst_mainmedia') as f:
        lines = f.readlines()

    return lines


def main(project_name, project_timezone):
    p = conn_mongo()
    lines = read_file()

    for line in lines:
        # 海南日报#####https://space.bilibili.com/1853377955/video

        # item = line.split("#####")
        # user_name = item[0]
        # uid = item[1].split("/")[-2]
        # uid = uid.strip()

        uid = line.strip()
        # uid = re.findall(r'com\/(.*?)\?from',uid)[0]
        try:
            count_url = 'https://api.bilibili.com/x/space/arc/search?' \
                        'mid={}&ps=30&tid=0&pn=1&keyword=&order=pubdate&jsonp=jsonp'.format(uid)

            pn = get_page_num(count_url, headers={'User-Agent': str(UserAgent().random)})

            for page in range(1, pn + 1):

                url = 'https://api.bilibili.com/x/space/arc/search?' \
                      'mid={}' \
                      '&ps=30' \
                      '&pn={}' \
                      '&jsonp=jsonp'.format(uid, page)

                result, flag = parse_one_html(url, project_name, project_timezone)
                p.insert_many(result)
                print('UID：{}用户的第{}页已经爬取完成！！！'.format(uid, page))
                time.sleep(20)

                if flag == 0:
                    break

        except Exception as e:
            print(e)


if __name__ == '__main__':

    project_name = "KeXieResearch"
    project_timezone = "2022-Q1"
    main(project_name, project_timezone)
