import time
import requests
import pymongo
from redis import Redis

#D:\mongodb4\bin>mongod.exe --dbpath F:\spider_data\bilibili_data

# def redis():
#     #url进行hash
#     hash_str = hashlib.sha256(url.encode()).hexdigest()
#     #redis存入url的hash值
#     redis_save_res = conn.sadd('weibo_long_url_hash', hash_str)
#
#     return redis_save_res

def mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1',port=27017)
    #选择数据库
    db = client.bilibili
    #选择操作的集合
    p = db.content
    return p

def parse_html(url,headers):

    html = requests.get(url,headers=headers)
    content = html.json()
    if content:
        if 'data' in content:
            page_count = content['data']['page']['count']
            # print(page_count,type(page_count))
            page_num = (page_count + 30) // 30

    return page_num

def parse_one_html(url,headers):

    html = requests.get(url, headers=headers)
    content = html.json()
    # print(content)
    if content:
        if 'data' in content:
            video_lst = content['data']['list']['vlist']

            for item in video_lst:
                # videos_dict["type_id"] = item['typeid']
                # videos_dict["video_comment"] = item['comment']
                # videos_dict["video_play_count"] = item['play']
                # videos_dict["video_pic"] = item['pic']
                # videos_dict["video_description"] = item['description']
                # videos_dict["video_title"] = item['title']
                # videos_dict["video_author"] = item['author']
                # videos_dict["video_length"] = item['length']
                video_created = item['created']
                struct_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(video_created))
                item["created"] = struct_time

                # # 隧道服务器
                # tunnel = "tps121.kdlapi.com:15818"
                #
                # # 隧道id和密码
                # username = "t17535136923494"
                # password = "lwUvJ8dK6All"
                # proxies = {
                #     "http": "http://%(user)s:%(pwd)s@%(proxy)s/" % {"user": username, "pwd": password, "proxy": tunnel},
                #     "https": "http://%(user)s:%(pwd)s@%(proxy)s/" % {"user": username, "pwd": password, "proxy": tunnel}
                # }
                #获取详情视频的三连数据。

                aid = item['aid']
                video_url_detail = 'https://api.bilibili.com/x/web-interface/view?aid=%s'%aid
                # response = requests.get(video_url_detail,headers=headers,proxies=proxies)
                response = requests.get(video_url_detail,headers=headers)

                if response.status_code == 200:
                    video_detail = response.json()
                    #主要的一键三连数据
                    item['true_view'] = video_detail['data']['stat']['view']
                    item['danmaku'] = video_detail['data']['stat']['danmaku']
                    item['favorite'] = video_detail['data']['stat']['favorite']
                    item['coin'] = video_detail['data']['stat']['coin']
                    item['share'] = video_detail['data']['stat']['share']
                    item['like'] = video_detail['data']['stat']['like']
                    item['dislike'] = video_detail['data']['stat']['dislike']
                time.sleep(5)
        return video_lst

def main():
    p = mongo()
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36",
    }

    with open('bilibili_user_lst_kepusuo','r',encoding='utf-8') as f:
        uids = f.readlines()

    for uid in uids:
        uid = uid.strip()
        count_url = 'https://api.bilibili.com/x/space/arc/search?mid=%s&ps=30&tid=0&pn=1&keyword=&order=pubdate&jsonp=jsonp'%(uid)
        try:
            pn = parse_html(count_url,headers)
            for page in range(1,pn+1):
                url = 'https://api.bilibili.com/x/space/arc/search?' \
                      'mid={}' \
                      '&ps=25' \
                      '&pn={}' \
                      '&jsonp=jsonp'.format(uid,page)

                result = parse_one_html(url,headers)
                p.insert(result)
                print("UID：{}用户的第{}页已经爬取完成！！！".format(uid,page))
                time.sleep(3)

        except:
            print("UID：{}的没有内容!!!!!".format(uid))

if __name__ == '__main__':
    # uid = "75806856"
    main()
