# -*- coding:utf-8 -*-
# Author: mongo
# Date: 2023/7/13

import hashlib
import time
from functools import reduce

import httpx

HEADERS = {"User-Agent": "Mozilla/5.0", "Referer": "https://www.bilibili.com"}


def getMixinKey(ae):
    oe = [46, 47, 18, 2, 53, 8, 23, 32, 15, 50, 10, 31, 58, 3, 45, 35, 27, 43, 5, 49, 33, 9, 42, 19, 29, 28, 14, 39, 12,
          38, 41,
          13, 37, 48, 7, 16, 24, 55, 40, 61, 26, 17, 0, 1, 60, 51, 30, 4, 22, 25, 54, 21, 56, 59, 6, 63, 57, 62, 11, 36,
          20, 34, 44, 52]

    le = reduce(lambda s, i: s + ae[i], oe, "")
    return le[:32]


def encWbi(params: dict):
    resp = httpx.get("https://api.bilibili.com/x/web-interface/nav")
    wbi_img: dict = resp.json()["data"]["wbi_img"]
    img_url: str = wbi_img.get("img_url")
    sub_url: str = wbi_img.get("sub_url")
    img_value = img_url.split("/")[-1].split(".")[0]
    sub_value = sub_url.split("/")[-1].split(".")[0]
    me = getMixinKey(img_value + sub_value)
    wts = int(time.time())
    params["wts"] = wts
    params = dict(sorted(params.items()))
    Ae = "&".join([f'{key}={value}' for key, value in params.items()])
    w_rid = hashlib.md5((Ae + me).encode(encoding='utf-8')).hexdigest()

    return w_rid, wts


def main():
    mid = 523959693

    url_params = {
        "mid": mid,
        "ps": "30",
        "tid": "0",
        "pn": 3,
        "keyword": "",
        "order": "pubdate",
        "platform": "web",
        "web_location": "1550101",
        "order_avoided": "true",
    }

    w_rid, wts = encWbi(url_params)

    params = {
        "w_rid": w_rid,
        "wts": wts,
    }
    params.update(url_params)
    resp = httpx.get(url, params=params, headers=HEADERS)
    print(resp.text)


if __name__ == "__main__":
    # url = "https://api.bilibili.com/x/space/wbi/acc/info"
    url = "https://api.bilibili.com/x/space/wbi/arc/search"
    main()
    # w_rid, wts = encWbi({"mid": 644515})
    # print(w_rid)
    # print(wts)


