# -*- coding:gb18030 -*-
# author:mongoose
# Date:2022/07/06
# Version: v3(code review)

import time
import requests
import pymongo
import redis
import hashlib
import logging
from fake_useragent import UserAgent


class BiliBili(object):

    def __init__(self, sin_time, pro_name, pro_timezone):
        self.sin_time = sin_time
        self.pro_name = pro_name
        self.pro_timezone = pro_timezone

    @staticmethod
    def logger_init():
        # log format
        logging.basicConfig(format='%(asctime)s,%(msecs)d %(levelname)-4s %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S')
        # log level
        # logging.getLogger().setLevel(logging.DEBUG)
        logging.getLogger().setLevel(logging.WARN)
        # logging.getLogger().setLevel(logging.CRITICAL)
        logger = logging.getLogger()

    @staticmethod
    def conn_redis():
        pool = redis.ConnectionPool(host="127.0.0.1", port=6379)
        conn = redis.Redis(connection_pool=pool)
        return conn

    @staticmethod
    def redis_filter(conn, url):
        # url进行sha256加密
        hash_str = hashlib.sha256(url.encode()).hexdigest()
        # redis存入url的hash值
        redis_save_res = conn.sadd('Bilibili_video_url_hash', hash_str)
        return redis_save_res

    @staticmethod
    def mongodb():
        # 连接数据库mongodb
        client = pymongo.MongoClient(host='127.0.0.1', port=27017)
        # 选择数据库
        db = client.bilibili_content
        # 选择操作的集合
        mongo = db.user_content_test
        return mongo

    @staticmethod
    def get_page_num(url, headers):
        html = requests.get(url, headers=headers)
        content = html.json()
        if content:
            if 'data' in content:
                page_count = content['data']['page']['count']
                page_num = (page_count + 30) // 30
                return page_num

    @staticmethod
    def proxy():
        # 隧道服务器
        tunnel = "tps121.kdlapi.com:15818"

        # 隧道id和密码
        username = "t17535136923494"
        password = "lwUvJ8dK6All"
        proxies = {
            "http": "http://%(user)s:%(pwd)s@%(proxy)s/" % {"user": username, "pwd": password, "proxy": tunnel},
            "https": "http://%(user)s:%(pwd)s@%(proxy)s/" % {"user": username, "pwd": password, "proxy": tunnel}
        }

        return proxies

    @staticmethod
    def parse_one_html(url, sin_time, pro_name, pro_timezone):
        headers = {
            'User-Agent': str(UserAgent().random),
            'referer': 'https://space.bilibili.com/',
        }
        html = requests.get(url, headers=headers)
        content = html.json()
        if 'vlist' in content['data']['list'].keys():
            video_lst = content['data']['list']['vlist']
            try:
                created_time = video_lst[0]['created']
                if created_time > sin_time:
                    for item in video_lst:
                        video_created = item['created']
                        struct_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(video_created))
                        item["created"] = struct_time
                        # get video detail information (coin_count,read_count,)
                        aid = item['aid']
                        video_url = 'https://api.bilibili.com/x/web-interface/view?aid=%s' % aid

                        response = requests.get(video_url, headers=headers)
                        if response.status_code == 200:
                            video_detail = response.json()
                            # coin , share, like
                            item['true_view'] = video_detail['data']['stat']['view']
                            item['danmaku'] = video_detail['data']['stat']['danmaku']
                            item['favorite'] = video_detail['data']['stat']['favorite']
                            item['coin'] = video_detail['data']['stat']['coin']
                            item['share'] = video_detail['data']['stat']['share']
                            item['like'] = video_detail['data']['stat']['like']
                            item['dislike'] = video_detail['data']['stat']['dislike']
                            item["project_name"] = pro_name
                            item["project_timezone"] = pro_timezone
                            time.sleep(2)

                        else:
                            print('THIS URL  {}  HAS NOT CONTENT !'.format(video_url))
                    return video_lst
            except Exception as e:
                logging.warning(e)
                return

    @staticmethod
    def read_file():
        with open(r'user_scienceTec/author_scienceTec') as f:
            lines = f.readlines()
        return lines

    def get_data(self, uid, page_num):
        for page in range(1, page_num + 1):
            url = 'https://api.bilibili.com/x/space/arc/search?' \
                  'mid={}' \
                  '&ps=30' \
                  '&pn={}' \
                  '&jsonp=jsonp'.format(uid, page)
            result = self.parse_one_html(url, since_time, project_name, project_timezone)
            try:
                self.mongodb().insert_many(result)
                logging.warning('Uid:{} Num:{} was finished！！！'.format(uid, page))
            except Exception as e:
                logging.warning('Uid:{} {}'.format(uid, e))
                return

    def start_run(self):
        users = self.read_file()
        for user in users:
            item = user.split("#####")
            uid = item[0].split("/")[-2]
            uid = uid.strip()
            content_url = 'https://api.bilibili.com/x/space/arc/search?' \
                          'mid={}&ps=30&tid=0&pn=1&keyword=&order=pubdate&jsonp=jsonp'.format(uid)
            page_num = self.get_page_num(content_url, headers={'User-Agent': str(UserAgent().random)})
            self.get_data(uid, page_num)
            time.sleep(20)


if __name__ == '__main__':
    since_time = time.mktime(time.strptime('2022-04-01 00:00:00', '%Y-%m-%d %H:%M:%S'))
    project_name = "KeXieResearch"
    project_timezone = "2022-Q2"
    BiliSpider = BiliBili(since_time, project_name, project_timezone)
    BiliSpider.start_run()
