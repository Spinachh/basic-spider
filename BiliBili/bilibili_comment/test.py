import time
import json
import random
import requests
import re
from bilibili_comment.setting import User_Agent

# rrange = "".join(random.sample([str(x) for x in range(10)],3))
now_time = ''.join(str(time.time()).split('.'))[:13]
pageNum = 1
#callback中的jQuery17205690838378040064_1590380076151 后面这串数'151'字可变可不变
url = 'https://api.bilibili.com/x/v2/reply?' \
      'callback=jQuery17205690838378040064_1590380076151' \
      '&jsonp=jsonp' \
      '&pn=%s' \
      '&type=1' \
      '&oid=582952251' \
      '&sort=2' \
      '&_=%s'%(pageNum,now_time)
# print(now_time)
user_agent = random.choice(User_Agent)

headers = {
    'User-Agent':user_agent,
    'referer': 'https://www.bilibili.com/bangumi/play/ss33288'
}

#这里获取到的html为jsonp格式需要解析
html = requests.get(url,headers=headers,verify=False).text

content = json.loads(re.match(".*?({.*}).*",html,re.S).group(1))
# print(content)
content = content['data']

for item in content:
    cm_data = {}
    for hot in item['hots']:
        cm_data['hot_comment_content'] = hot['content']['message']
        cm_data['hot_comment_device'] = hot['content']['device']
        ss_time = hot['ctime']
        cm_data['hot_comment_ctime'] = time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(ss_time))
        cm_data['hot_comment_like'] = hot['like']
        cm_data['hot_comment_like'] = hot['like']

