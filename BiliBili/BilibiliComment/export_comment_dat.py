import time
import pymongo
import pandas as pd

myclient = pymongo.MongoClient("mongodb://127.0.0.1:27017")
mydb = myclient["bilibili"]

mycol1 = mydb["comment"]
mycol2 = mydb["comment_reply"]
mydoc1 = mycol1.find()
mydoc2 = mycol2.find()
# myquery1 = {"user_id":{"$eq":"6506770761"}}
# mydoc = mycol.find({"$and":[{"user_id":{"$eq":"6506770761"}},{"created_at":{"$gte":"2019-10-01"}},{"created_at":{"$lte":"2019-12-01"}}]})
# data = pd.DataFrame(list(mydoc))

# mydoc = mycol.find({"$and": [{"user_id": {"$eq": line}}, {"created_at": {"$gte": "2019-09-30"}},{"created_at": {"$lte": "2020-01-01"}}]})
file_time = time.strftime("%Y-%m-%d",time.localtime(time.time()))
data1 = pd.DataFrame(list(mydoc1))
data2 = pd.DataFrame(list(mydoc2))
data1.to_csv('bilibili-comment-%s.csv'%file_time, encoding='utf-8')
data2.to_csv('bilibili-comment-reply-%s.csv'%file_time, encoding='utf-8')
