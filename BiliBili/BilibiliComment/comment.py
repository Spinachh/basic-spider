import time
import json
import random
import requests
import re
import pymongo
from bilibili_comment.setting import User_Agent


def mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1',port=27017)
    #选择数据库
    db = client.bilibili
    #选择操作的集合
    p1 = db.comment
    p2 = db.comment_reply
    return p1,p2


def get_content(user_agent,url,p):

    headers = {
        'User-Agent':user_agent,
        'referer': 'https://www.bilibili.com/bangumi/play/ss33288'
    }

    #这里获取到的html为jsonp格式需要解析
    html = requests.get(url,headers=headers,verify=False).text
    # print(html)
    content = json.loads(re.match(".*?({.*}).*",html,re.S).group(1))
    # print(content)
    content = content['data']['replies']

    for item in content:
        cm_data = {}
        cm_data['comment_rpid'] = item['rpid']
        cm_data['comment_content'] = item['content']['message']
        # cm_data['comment_device'] = item['content']['device']
        ss_time = item['ctime']
        cm_data['comment_ctime'] = time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(ss_time))
        cm_data['comment_like'] = item['like']
        p[0].insert(cm_data)
        print('===========Insert Comment into MongoDB successfully!=========')

        if int(item['rcount']) != 0:
            replies_comment = item['replies']
            for reply in replies_comment:
                cm_reply_data = {}
                cm_reply_data['comment_replies_rpid'] = reply['parent']
                reply_ss_time = reply['ctime']
                cm_reply_data['comment_replies_ctime'] = time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(reply_ss_time))
                cm_reply_data['comment_replies_rpid'] = reply['parent']
                cm_reply_data['comment_replies_content'] = reply['content']['message']
                cm_reply_data['comment_replies_like'] = reply['like']
                p[1].insert(cm_reply_data)
                print('===========Insert CommentReplies into MongoDB successfully!=========')


def main():
    p = mongo()
    user_agent = random.choice(User_Agent)

    # rrange = "".join(random.sample([str(x) for x in range(10)],3))
    now_time = ''.join(str(time.time()).split('.'))[:13]
    # pageNum = 1
    # callback中的jQuery17205690838378040064_1590380076151 后面这串数'151'字可变可不变
    ##必须有的参数：callback,oid,pn
    for pageNum in range(1,80):
        url = 'https://api.bilibili.com/x/v2/reply?' \
              'callback=jQuery17208708507719536722_1598493100566' \
              '&jsonp=jsonp' \
              '&pn=%s' \
              '&type=1' \
              '&oid=626796144' \
              '&sort=2' \
              '&_=%s' % (pageNum, now_time)
        # print(now_time)
        get_content(user_agent,url,p)
        time.sleep(3)

if __name__ == '__main__':
    main()