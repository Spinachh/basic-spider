#-*-coding:utf-8-*-
#bilibili弹幕的api接口由之前的cid直接转换为字符串格式
#在目标视频网页下抓包并获取到该cid,url格式为：
#https://api.bilibili.com/x/player/pagelist?bvid=BV12t4y1U7yD&jsonp=jsonp
import time
import json
import random
import requests
import re
import pymongo
import requests
import re

def mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1',port=27017)
    #选择数据库
    db = client.bilibili
    #选择操作的集合
    p = db.danmu
    return p

def get_content(url,p):
    r = requests.get(url)

    # print(r.content.decode("utf-8"))
    # ret = re.findall(r'<d(.*?)/d>',r.content.decode("utf-8"))
    #' p="385.97600,1,25,16777215,1565097766,0,3fa6c8ee,19899661036290052">弹幕呢？<'
    result = re.findall(r'<d\sp=(.*?)>(.*?)</d>',r.content.decode("utf-8"))
    # print(result)
    for item in result:
        danmu_dict = {}
        #处理完成为秒级单位
        part1 = item[0].split(',')
        part2 = item[1]
        # print(part1)
        video_time = int(float(part1[0].replace('"',"")))
        # print(video_time,type(video_time))
        if video_time > 60.0:
            mtime_point = video_time // 60
            stime_point = video_time % 60
            danmu_dict['time_point'] = '%s分%s秒'%(mtime_point,stime_point)
        else:
            video_time = part1[0].split('.')[0]
            danmu_dict['time_point'] = '%s秒'%video_time

        publish_stime = int(part1[4])
        publish_time = time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(publish_stime))
        danmu_dict['publish_time'] = publish_time
        danmu_dict['danmu_content'] = part2

        p.insert(danmu_dict)
        print('===========Insert into MongoDB successfully!=========')

def main():
    p = mongo()
    url = "https://comment.bilibili.com/228208038.xml"
    get_content(url,p)


if __name__ == '__main__':
    main()
