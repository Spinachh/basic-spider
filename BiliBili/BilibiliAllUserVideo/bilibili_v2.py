import time
import requests
import pymongo
import redis
import hashlib
from fake_useragent import UserAgent

def conn_redis():
    pool = redis.ConnectionPool(host="127.0.0.1",port=6379)
    conn = redis.Redis(connection_pool=pool)
    return conn

def redis_filter(conn,url):

    #url进行sha256加密
    hash_str = hashlib.sha256(url.encode()).hexdigest()
    # redis存入url的hash值
    redis_save_res = conn.sadd('Bilibili_video_url_hash', hash_str)
    return redis_save_res

def conn_mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1',port=27017)
    #选择数据库
    db = client.bilibili
    #选择操作的集合
    p = db.content
    return p

def get_page_num(url,headers):

    html = requests.get(url,headers=headers)
    content = html.json()
    if content:
        if 'data' in content:
            page_count = content['data']['page']['count']
            # print(page_count,type(page_count))
            page_num = (page_count + 30) // 30

    return page_num

def proxy():

    # 隧道服务器
    tunnel = "tps121.kdlapi.com:15818"

    # 隧道id和密码
    username = "t17535136923494"
    password = "lwUvJ8dK6All"
    proxies = {
        "http": "http://%(user)s:%(pwd)s@%(proxy)s/" % {"user": username, "pwd": password, "proxy": tunnel},
        "https": "http://%(user)s:%(pwd)s@%(proxy)s/" % {"user": username, "pwd": password, "proxy": tunnel}
    }

    return proxies

def parse_one_html(url,cnn_redis,proxies):
    headers = {'User-Agent': str(UserAgent().random)}
    html = requests.get(url, headers=headers)
    content = html.json()
    if 'vlist' in content['data']['list'].keys():
        video_lst = content['data']['list']['vlist']
        for item in video_lst:
            video_created = item['created']
            struct_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(video_created))
            item["created"] = struct_time
            #获取详情视频的三连数据。

            aid = item['aid']
            video_url = 'https://api.bilibili.com/x/web-interface/view?aid=%s'%aid

            redis_save_value = redis_filter(cnn_redis,video_url)
            if redis_save_value == 1:
                response = requests.get(video_url,headers=headers,proxies=proxies)
                if response.status_code == 200:
                    video_detail = response.json()
                    #主要的一键三连数据
                    item['true_view'] = video_detail['data']['stat']['view']
                    item['danmaku'] = video_detail['data']['stat']['danmaku']
                    item['favorite'] = video_detail['data']['stat']['favorite']
                    item['coin'] = video_detail['data']['stat']['coin']
                    item['share'] = video_detail['data']['stat']['share']
                    item['like'] = video_detail['data']['stat']['like']
                    item['dislike'] = video_detail['data']['stat']['dislike']
                # time.sleep(3)

                else:
                    print('This Proxy-IP has Invaild !!')
            else:
                print('This Video is already Crawled !! ')

    return video_lst

def main():
    p = conn_mongo()
    proxies = proxy()
    cnn_redis = conn_redis()
    with open('bilibili_user_lst_kepusuo','r',encoding='utf-8') as f:
        uids = f.readlines()

    for uid in uids:
        uid = uid.strip()
        count_url = 'https://api.bilibili.com/x/space/arc/search?mid=%s&ps=30&tid=0&pn=1&keyword=&order=pubdate&jsonp=jsonp'%(uid)
        pn = get_page_num(count_url,headers={'User-Agent':str(UserAgent().random)})
        for page in range(1,pn+1):
            url = 'https://api.bilibili.com/x/space/arc/search?' \
                  'mid={}' \
                  '&ps=25' \
                  '&pn={}' \
                  '&jsonp=jsonp'.format(uid,page)
            result = parse_one_html(url,cnn_redis,proxies)
            p.insert(result)
            print('UID：{}用户的第{}页已经爬取完成！！！'.format(uid,page))
            time.sleep(3)


if __name__ == '__main__':
    # uid = "75806856"
    main()
