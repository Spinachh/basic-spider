# -*-coding:utf-8 -*-
import sys,io
import time
import requests
import pymongo
import traceback
import logging
from fake_useragent import UserAgent
import execjs

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongo():
    # 连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # 选择数据库
    db = client.xigua_content
    # 选择操作的集合
    p = db.user_content
    return p


def get_signature():
    with open(r'crawl.js',encoding='utf-8') as f:
        code = f.read()
    ctx = execjs.compile(code)
    result = ctx.call("byted_acrawler.sign", "", "")
    # print(result)
    return result


def proxy():

    # 隧道服务器
    tunnel = "tps121.kdlapi.com:15818"

    # 隧道id和密码
    username = "t17535136923494"
    password = "lwUvJ8dK6All"
    proxies = {
        "http": "http://%(user)s:%(pwd)s@%(proxy)s/" % {"user": username, "pwd": password, "proxy": tunnel},
        "https": "http://%(user)s:%(pwd)s@%(proxy)s/" % {"user": username, "pwd": password, "proxy": tunnel}
    }

    return proxies


def parse_html(user_id, maxBehotTime, collection, offset, project_name, project_timezone):

    url = 'https://www.ixigua.com/api/videov2/author/new_video_list?'
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36",
        "referer": "https://www.ixigua.com/home/{}/".format(user_id),
    }
    signature = get_signature()
    data = {
        '_signature': signature,
        # 'author_id': user_id,
        'offset': offset,
        'limit': 30,
        'to_user_id': user_id,
        'type': 'video',
        'maxBehotTime': maxBehotTime,
    }
    html = requests.get(url, headers=headers, params=data, timeout=5)
    content = html.json()
    # print(content)
    # breakpoint()
    try:
        if len(content['data']) != 0:
            video_lst = content['data']['videoList']
            code_status = content['code']

            for item in video_lst:
                video_new_dict = {}
                video_new_dict['title'] = item['title']
                video_new_dict['tag'] = item['tag']
                video_new_dict['abstract'] = item['abstract']
                video_new_dict['publish_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(item['publish_time']))
                video_new_dict['categories'] = item['categories']
                video_new_dict['comment_count'] = item['comment_count']
                video_new_dict['composition'] = item['composition']
                video_new_dict['danmaku_count'] = item['danmaku_count']
                video_new_dict['digg_count'] = item['digg_count']
                video_new_dict['repin_count'] = item['repin_count']
                video_new_dict['share_count'] = item['share_count']
                video_new_dict['video_watch_count'] = item['video_detail_info']['video_watch_count']
                video_new_dict['video_like_count'] = item['video_like_count']
                video_new_dict['video_duration'] = item['video_duration']
                video_new_dict['display_url'] = item['display_url']
                try:
                    video_new_dict['first_frame_image'] = item['first_frame_image']['url']
                except:
                    video_new_dict['first_frame_image'] = 'null'
                video_new_dict['has_video'] = item['has_video']
                try:
                    video_new_dict['impress_count'] = item['impress_count']
                except:
                    video_new_dict['impress_count'] = 'null'

                video_new_dict['is_original'] = item['is_original']
                video_new_dict['is_subscribe'] = item['is_subscribe']
                video_new_dict['media_name'] = item['media_name']
                video_new_dict['share_url'] = item['share_url']
                video_new_dict['show_portrait'] = item['show_portrait']
                video_new_dict['source'] = item['source']
                video_new_dict['author_desc'] = item['user_info']['author_desc']
                video_new_dict['user_avatar_url'] = item['user_info']['avatar_url']
                video_new_dict['user_description'] = item['user_info']['description']
                video_new_dict['user_follower_count'] = item['user_info']['follower_count']
                video_new_dict['user_name'] = item['user_info']['name']
                video_new_dict['user_video_total_count'] = item['user_info']['video_total_count']
                video_new_dict['user_verified'] = item['user_verified']
                if 'verified_content' in item.keys():
                    video_new_dict['user_verified_content'] = item['verified_content']
                else:
                    video_new_dict['user_verified_content'] = None

                # video_new_dict['project_name'] = 'KexieResearch'
                video_new_dict['project_name'] = project_name
                video_new_dict['project_timezone'] = project_timezone

                collection.insert_one(video_new_dict)

            return video_lst[-1]['behot_time'], code_status
        else:
            return 'None', 201
    except Exception as e:
        # exc_type,exc_value,exc_obj = sys.exc_info()
        # traceback.print_tb(exc_obj)
        return 'None', 'false'


def get_hot_time(user_id, collection, project_name, project_timezone):
    since_time = time.mktime(time.strptime('2022-01-01 00:00:00', '%Y-%m-%d %H:%M:%S'))
    maxBehotTime = 0
    offset = 0
    while True:
        try:
            result = parse_html(user_id, maxBehotTime, collection, offset, project_name, project_timezone)
            # print(result)
            # breakpoint()
            behot_time = result[0]  # 字典列表中最后一个字典元素的hot_time值就是下一页的值
            code_status = result[1]
            logging.warning("BehotTime:{} CodeStatus:{}".format(behot_time, code_status))
            logging.warning("UID：{} MaxBehotTime：{} 详情页爬取完成".format(user_id, behot_time))
            time.sleep(3)
            if int(behot_time) < int(since_time):
                break
            else:
                if code_status == 200:
                    maxBehotTime = behot_time
                    offset += 30
                    continue
                else:
                    break

        except Exception as e:
            # logging.warning('输出错误结果：{}'.format(e))
            logging.warning('Uid：{}没有内容'.format(user_id))
            time.sleep(5)
            break


def read_user_file():

    with open(r'ixigua_user_lst', 'r', encoding='utf-8') as f:
        uids = f.readlines()
    return uids


def main():
    project_name = "KeXie"
    project_timezone = "2022-Q1"
    
    collection = mongo()

    '''
    uids = ['51669749939','6721771772','4195358834426413',
            '2678058810348637','50222294888','6041795457',
            '1204719427259911','54646471720']  #kepuability
    '''

    # uids = ["4174955406"]
    uids = read_user_file()
    for uid in uids:
        user_id = uid.strip()
        get_hot_time(user_id, collection, project_name, project_timezone)


if __name__ == '__main__':

    main()
