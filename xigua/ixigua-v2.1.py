# -*-coding:utf-8 -*-
# Author:mongoose
# Date:2022/11/02
# Version: v2(code review)
import random
import sys
import io
import time
import requests
import pymongo
import logging
from fake_useragent import UserAgent
import execjs

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


class XiGua(object):

    def __init__(self, pro_name, pro_timezone, sin_time):
        self.project_name = pro_name
        self.project_timezone = pro_timezone
        self.since_time = sin_time
        self.crawl_time = ''

    @staticmethod
    def mongo():
        # 连接数据库mongodb
        client = pymongo.MongoClient(host='127.0.0.1', port=27017)
        # 选择数据库
        db = client.xigua_content
        # 选择操作的集合
        p = db.user_content_Q4
        return p

    @staticmethod
    def get_signature():
        with open(r'crawl.js', encoding='utf-8') as f:
            code = f.read()
        ctx = execjs.compile(code)
        result = ctx.call("byted_acrawler.sign", "", "")
        return result

    @staticmethod
    def proxy():
        # 隧道服务器
        tunnel = "tps121.kdlapi.com:15818"

        # 隧道id和密码
        username = "t17535136923494"
        password = "lwUvJ8dK6All"
        proxies = {
            "http": "http://%(user)s:%(pwd)s@%(proxy)s/" % {"user": username, "pwd": password, "proxy": tunnel},
            "https": "http://%(user)s:%(pwd)s@%(proxy)s/" % {"user": username, "pwd": password, "proxy": tunnel}
        }

        return proxies

    def parse_html(self, user_id, MaxBeHotTime, offset):

        url = 'https://www.ixigua.com/api/videov2/author/new_video_list?'

        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 "
                          "(KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36",
            "referer": "https://www.ixigua.com/home/{}/".format(user_id),
        }
        signature = self.get_signature()
        data = {
            '_signature': signature,
            # 'author_id': user_id,
            'offset': offset,
            'limit': 30,
            'to_user_id': user_id,
            'type': 'video',
            'maxBehotTime': MaxBeHotTime,
        }
        html = requests.get(url, headers=headers, params=data, timeout=5)
        content = html.json()
        try:
            if len(content['data']) != 0:
                video_lst = content['data']['videoList']
                code_status = content['code']
                for item in video_lst:
                    self.crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
                    video_new_dict = \
                        {'title': item['title'], 'tag': item['tag'], 'abstract': item['abstract'],
                         'publish_time': time.strftime("%Y-%m-%d %H:%M:%S",
                                                       time.localtime(item['publish_time'])),
                         'categories': item['categories'], 'comment_count': item['comment_count'],
                         'composition': item['composition'], 'danmaku_count': item['danmaku_count'],
                         'digg_count': item['digg_count'], 'repin_count': item['repin_count'],
                         'share_count': item['share_count'],
                         'video_watch_count': item['video_detail_info']['video_watch_count'],
                         'video_like_count': item['video_like_count'],
                         'video_duration': item['video_duration'], 'display_url': item['display_url']}
                    try:
                        video_new_dict['first_frame_image'] = item['first_frame_image']['url']
                    except:
                        video_new_dict['first_frame_image'] = 'null'
                    video_new_dict['has_video'] = item['has_video']
                    try:
                        video_new_dict['impress_count'] = item['impress_count']
                    except:
                        video_new_dict['impress_count'] = 'null'

                    video_new_dict['is_original'] = item['is_original']
                    video_new_dict['is_subscribe'] = item['is_subscribe']
                    video_new_dict['media_name'] = item['media_name']
                    video_new_dict['share_url'] = item['share_url']
                    video_new_dict['show_portrait'] = item['show_portrait']
                    video_new_dict['source'] = item['source']
                    video_new_dict['author_desc'] = item['user_info']['author_desc']
                    video_new_dict['user_avatar_url'] = item['user_info']['avatar_url']
                    video_new_dict['user_description'] = item['user_info']['description']
                    video_new_dict['user_follower_count'] = item['user_info']['follower_count']
                    video_new_dict['user_name'] = item['user_info']['name']
                    video_new_dict['user_video_total_count'] = item['user_info']['video_total_count']
                    video_new_dict['user_verified'] = item['user_verified']
                    if 'verified_content' in item.keys():
                        video_new_dict['user_verified_content'] = item['verified_content']
                    else:
                        video_new_dict['user_verified_content'] = None
                    video_new_dict['crawl_time'] = self.crawl_time
                    video_new_dict['project_name'] = project_name
                    video_new_dict['project_timezone'] = project_timezone

                    XiGua.mongo().insert_one(video_new_dict)
                    time.sleep(random.randint(1, 5))
                return video_lst[-1]['behot_time'], code_status
            else:
                return 'None', 201
        except Exception as e:
            logging.warning('e:{}'.format(e))
            return 'None', 'false'

    def get_hot_time(self, user_id):
        MaxBeHotTime = 0
        offset = 0
        while True:
            try:
                hot_time, code_status = self.parse_html(user_id, MaxBeHotTime, offset)
                # hot_time列表中最后一个字典元素的hot_time值就是下一页的hot_time值
                logging.warning("BeHotTime:{} CodeStatus:{}".format(hot_time, code_status))
                logging.warning("Uid：{} MaxBeHotTime：{} detail information was finished!!".format(user_id, hot_time))
                time.sleep(3)
                if int(hot_time) < int(self.since_time):
                    break
                else:
                    if code_status == 200:
                        MaxBeHotTime = hot_time
                        offset += 30
                        continue
                    else:
                        break
            except Exception as e:
                logging.warning('Uid：{}没有内容'.format(user_id))
                time.sleep(5)
                break

    @staticmethod
    def read_user_file():
        with open(r'scienceTec_user/XiGuaScienceTecVideo', 'r', encoding='gb18030') as f:
            lines = f.readlines()
        return lines

    def start_run(self):
        lines = self.read_user_file()
        for uid in lines:
            user_id = uid.strip()
            user_id = user_id.split('#####')[0].split('/')[-2]
            self.get_hot_time(user_id)
            time.sleep(5)


if __name__ == '__main__':
    project_name = "KeXie"
    project_timezone = "2022-Q4"
    since_time = time.mktime(time.strptime('2022-10-01 00:00:00', '%Y-%m-%d %H:%M:%S'))
    XiGuaSpider = XiGua(project_name, project_timezone, since_time)
    XiGuaSpider.start_run()
