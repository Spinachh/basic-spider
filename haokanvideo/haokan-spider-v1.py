# !/usr/bin/python3
# -*- coding:utf-8 -*-
# Author : mongooses
# Date : 2022/4/13 

import os
import io
import re
import sys
import time
import json
import pymongo
import asyncio
import cchardet
import requests
import logging
from fake_useragent import UserAgent

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030') #改变标准输出的默认编码
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongo():
    # 连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # 选择数据库
    db = client.baidu_content
    # 选择操作的集合
    mongodb = db.haokanvideo_user_content

    return mongodb


def get_page_response(url):

    ua = UserAgent().random
    headers = {
        'User-Agent': ua,
        # 'Cookie': '',
    }
    try:
        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            html_chariest = cchardet.detect(response.content)
            # print(response.headers["content-type"])
            # print(response.encoding)
            # print(html_chardet)
            response.encoding = html_chariest["encoding"]
            # response = response.text.replace('\\"', '#').replace("\\n", "")  # 对json格式中的多余的引号去除
            html_content = response.text.encode("utf-8").decode("unicode_escape")
            result = re.findall(r'window.__PRELOADED_STATE__ = (.*?);     document.querySelector', html_content, re.S | re.M)[0]
            html_result = json.loads(result)
            # print(html_result)
            # breakpoint()
            return html_result

    except Exception as e:
        print(e)


def get_response(url):
    # print('get_response', url)
    ua = UserAgent().random
    headers = {
        'User-Agent': ua,
        # 'Cookie': '',
    }
    try:
        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            html_chariest = cchardet.detect(response.content)
            # print(response.headers["content-type"])
            # print(response.encoding)
            # print(html_chardet)
            response.encoding = html_chariest["encoding"]
            # response = response.text.replace('\\"', '#').replace("\\n", "")  # 对json格式中的多余的引号去除
            html_content = response.text.encode("utf-8").decode("unicode_escape")
            html_result = json.loads(html_content)
            # print(html_result)
            # breakpoint()
            return html_result

    except Exception as e:
        print(e)


def get_data(html_result, mongodb):

    if 'video' in html_result.keys():
        video_result = html_result['video']
        has_more = video_result['has_more']
        c_time = video_result['ctime']
        parse_data(video_result, mongodb)

        return has_more, c_time

    elif 'data' in html_result.keys():
        video_result = html_result['data']
        has_more = video_result['has_more']
        c_time = video_result['ctime']
        parse_data(video_result, mongodb)

        return has_more, c_time


def parse_data(video_result, mongodb):

    y_time = time.strftime('%Y', time.localtime(time.time()))

    if 'results' in video_result.keys():
        videos = video_result['results']
        for video in videos:
            video_dict = {}
            video_info = video['content']
            video_vid = video_info['vid']
            video_dict['vid'] = video_vid

            if '年' not in video_info['publish_time']:
                publish_time = y_time + '年' + video_info['publish_time']
            else:
                publish_time = video_info['publish_time']
            video_dict['publish_time'] = publish_time
            video_dict['title'] = video_info['title']
            video_dict['cover_src'] = video_info['cover_src']
            video_dict['duration'] = video_info['duration']
            video_dict['poster'] = video_info['poster']
            video_dict['playcnt'] = video_info['playcnt']
            video_dict['playcntText'] = video_info['playcntText']
            video_url = 'https://haokan.baidu.com/v?vid={}'.format(video_vid)
            video_dict['video_url'] = video_url

            # detail video information
            detail_content = str(get_page_response(video_url))
            # print(detail_content)
            like_num = int(re.findall(r"'like': (.*?),", detail_content, re.S | re.M)[0])
            video_dict['like_num'] = like_num
            fmlike_num = re.findall(r"'fmlike_num': (.*?),", detail_content, re.S | re.M)[0]
            video_dict['fmlike_num'] = fmlike_num
            comment_num = int(re.findall(r"'comment': (.*?),", detail_content, re.S | re.M)[0])
            video_dict['comment_num'] = comment_num
            # print(video_dict)
            # breakpoint()
            mongodb.insert_one(video_dict)
            logging.warning('Title: {} PublishTime: {}'.format(video_info['title'], publish_time))


def run():
    mongodb = mongo()
    ctime = 0
    while True:
        url = 'https://haokan.baidu.com/web/author/listall?' \
              'app_id=1593743208952652&ctime={}&rn=10'.format(ctime)

        if ctime == 0:
            url = 'https://haokan.baidu.com/author/1593743208952652'
            response = get_page_response(url)
            has_more, c_time = get_data(response, mongodb)
            time.sleep(3)
        else:
            response = get_response(url)
            has_more, c_time = get_data(response, mongodb)
            time.sleep(2)

        if has_more == 1:
            ctime = c_time
            continue
        else:
            break


def main():
    run()


if __name__ == '__main__':
    main()
