# !/usr/bin/python
# -*- coding:utf-8 -*-
# Author：Mongoole
# Date：2020/11/6

import time
import os
import io
import sys
import re
import json
import random
import pymongo
import requests
from snownlp import SnowNLP
import urllib3

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030') #change the default encoding of standard output

try:
    from UserAgentPool import User_Agent
except:
    from ContentApi.UserAgentPool import User_Agent

def mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1',port=27017)
    #选择数据库
    # db = client.weibo_comment
    db = client.WeiboSearch
    #选择操作的集合
    pcsc = db.content
    # pcmm = db.comment

    return pcsc

def get_response(url):

    # response = requests.get(url,verify=False)
    response = requests.get(url)
    try:
        if response.status_code == 200:
            content = response.json()
            # print(content)
            # breakpoint()
            if content['cards']:
                results = content['cards']
                return results
            else:
                print('This function get_response has error:{}'.format(content))
                flag = 1
                return flag

    except Exception as e:
        print(e)


def handle_data(results,page,p):
    for result in results:
        data_dict = {}
        # print(result.keys())
        # breakpoint()
        # if 'mblog' in result.keys() and 'title' not in result['mblog'].keys():    #去除置顶的微博
        if 'mblog' in result.keys() and 'title' not in result['mblog'].keys():
            # print(result)
            item = result["mblog"]
            # print(item)
            # breakpoint()
            # data_dict["text"] = item["text"]        #文本内容
            text = item["text"]
            data_dict["text"] = text
            # nlp = SnowNLP(text)
            # data_dict['score'] = nlp.sentiments

            created_at = item["created_at"]
            struct_time = time.strptime(created_at, "%a %b %d %H:%M:%S %z %Y")
            format_time = time.strftime("%Y-%m-%d %H:%M:%S", struct_time)
            format_time2 = time.strftime("%Y-%m-%d", struct_time)
            stamp_time = time.mktime(time.strptime(format_time, "%Y-%m-%d %H:%M:%S"))

            ##This Judge time direction
            # if since_time < format_time:
            # if since_time < format_time and format_time < until_time:

            data_dict["created_at"] = format_time       #发布时间
            data_dict["created_at2"] = format_time2      #发布时间
            data_dict["comments_count"] = item["comments_count"]    #评论数量
            data_dict["attitude_count"] = item["attitudes_count"]   #正文点赞
            data_dict["reposts_count"] = item["reposts_count"]      #转发数量
            data_dict["idstr"] = item["idstr"]                      #该条微博的唯一标识ID
            # 是否是转repost
            if "retweeted_status" in item.keys():
                retweeted = item["retweeted_status"]

                data_dict["retweeted_flag"] = "转发"

                if "text" in retweeted.keys():
                    # data_dict["retweeted_text"] = retweeted["text"]
                    retweeted_text = retweeted["text"]
                    data_dict["retweeted_text"] = retweeted_text
                    retweeted_nlp = SnowNLP(retweeted_text)
                    data_dict['retweeted_score'] = retweeted_nlp.sentiments

                else:
                    data_dict["retweeted_text"] = None
                    data_dict['retweeted_score'] = None

                if "attitudes_count" in retweeted.keys():
                    data_dict["retweeted_attitude"] = retweeted["attitudes_count"]
                else:
                    data_dict["retweeted_attitude"] = None

                if "comments_count" in retweeted.keys():
                    data_dict["retweeted_comment"] = retweeted["comments_count"]
                else:
                    data_dict["retweeted_comment"] = None

                re_created_at = retweeted["created_at"]
                re_struct_time = time.strptime(re_created_at, "%a %b %d %H:%M:%S %z %Y")
                re_format_time = time.strftime("%Y-%m-%d %H:%M:%S", re_struct_time)
                stamp_time = time.mktime(time.strptime(re_format_time, "%Y-%m-%d %H:%M:%S"))
                data_dict["retweeted_created_at"] = re_format_time

                if "pic_num" in retweeted.keys():
                    retweeted_pic_num = retweeted["pic_num"]
                else:
                    retweeted_pic_num = None

                # print(type(retweeted_pic_num))
                data_dict["retweeted_pic_num"] = retweeted_pic_num

                if "pic_ids" in retweeted.keys():
                    re_pic_ids = retweeted["pic_ids"]
                else:
                    re_pic_ids = None

                # print(re_pic_ids)

                if "pic_infos" in retweeted.keys():
                    retweeted_pic = retweeted["pic_infos"]
                    pic_lst = [retweeted_pic[pic_ids]["large"]["url"] for pic_ids in re_pic_ids]
                    data_dict["retweeted_imgs"] = pic_lst
                else:
                    data_dict["retweeted_imgs"] = None

                if "reposts_count" in retweeted.keys():
                    data_dict["retweeted_repost"] = retweeted["reposts_count"]
                else:
                    data_dict["retweeted_repost"] = None

                if "source" in retweeted.keys():
                    source = item["source"]
                    # print(source)
                    tools = re.findall(r'"rel="nofollow">(.*?)</a>', source)
                    data_dict["retweeted_tools"] = tools
                else:
                    data_dict["retweeted_tools"] = None

                if "user" in retweeted.keys():
                    data_dict["retweeted_account"] = retweeted["user"]["name"]

                    re_account_id = retweeted['user']['idstr']
                    re_scheme = retweeted["scheme"].split("=")[1]
                    data_dict["retweeted_url"] = "https://weibo.com/{}/".format(re_account_id) + re_scheme
                    # print(data_dict["retweeted_url"])
                else:
                    data_dict["retweeted_account"] = None
                    data_dict["retweeted_url"] = None


            else:
                data_dict["retweeted_text"] = None
                data_dict["retweeted_attitude"] = None
                data_dict["retweeted_comment"] = None
                data_dict["retweeted_created_at"] = None
                data_dict["retweeted_pic_num"] = None
                data_dict["retweeted_repost"] = None
                data_dict["retweeted_account"] = None
                data_dict["retweeted_url"] = None
                data_dict["retweeted_flag"] = "原创"

            pic_num = item["pic_num"]
            data_dict["pic_num"] = pic_num

            if pic_num != 0:
                pic_ids = item["pic_ids"]
                content_pic = item["pic_infos"]
                pic_lst = [content_pic[ids]["large"]["url"] for ids in pic_ids]
                data_dict["content_imgs"] = pic_lst
            else:
                data_dict["content_imgs"] = None


            if "source" in item.keys():
                source = item["source"]
                # print(source)
                tools = re.findall(r'rel=\"nofollow\">(.*?)</a>', source)
                data_dict["tools"] = tools
            else:
                data_dict["tools"] = None

            data_dict["id"] = item["idstr"]
            data_dict["mblogid"] = item["mblogid"]

            if 'page_info' in item.keys():
                try:
                    data_dict["visit_count"] = item["page_info"]["media_info"]["online_users"]
                    data_dict["content_type"] = 'text_video'
                except:
                    data_dict["visit_count"] = None
                    if pic_num == '0':
                        data_dict["content_type"] = 'text'
                    else:
                        data_dict["content_type"] = 'text_img'
            else:
                data_dict["visit_count"] = None
                if pic_num == '0':
                    data_dict["content_type"] = 'text'
                else:
                    data_dict["content_type"] = 'text_img'

            data_dict['account'] = item['user']['name']
            account_id = item['user']['idstr']
            scheme = item["scheme"].split("=")[1]
            data_dict["weibo_url"] = "https://weibo.com/{}/".format(account_id) + scheme

            # data_dict["category"] = "subject of talk"
            data_dict["category"] = "search keyword"
            # data_dict["talk_content"] = "#河南暴雨紧急求助通道##河南暴雨求助#"
            data_dict["talk_content"] = "物资"

            p.insert(data_dict)
            # print("-{}-Insert into MongoDB Sucessfully-{}-".format('-' * 40,'-' * 40))


def get_app_uid():
    # with open('UserAccountFidKePuAbility',encoding='gbk') as f:
    with open('UserAccountFidKeXie',encoding='gbk') as f:
        all = f.readlines()

    return all


def get_page(p):

    NetworkPool = ['wifi','4G','5G']
    # 此处的url,每一个账号的containerid都是不一样的
    # for page in range(1, 500):
    for page in range(1, 1000):
        url = 'https://api.weibo.cn/2/searchall?networktype={network}' \
              '&extparam=phototab_style%3Dtrue&sensors_device_id=none' \
              '&orifid=231619%24%24100303type%3D1%26t%3D3&uicode=10000003&moduleID=708&featurecode=10000085' \
              '&wb_version={wb}&c=android&s=85a290af&ft=0&ua={ua}&wm=2468_1001' \
              '&aid=01A5bWyxVdBbINO3LYKfrE4PdCyGNRi_rdI10mF_L7xphh1Zs.' \
              '&ext=orifid%3A231619%24%24100303type%3D1%26t%3D3%7Coriuicode%3A10000010_10000003' \
              '&fid=100103type%3D1%26q%3D%E7%89%A9%E8%B5%84%26t%3D0&lat=39.903285&lon=116.400925&uid=7490293470' \
              '&v_f=2&v_p=76&from=1098495010' \
              '&gsid=_2A25N2EPIDeRxGeFK4lIT-S3IzDyIHXVszNAArDV6PUJbkdANLWijkWpNQtkA4KE9HC6nhNbbeZcx5I3fyGjcF6kY' \
              '&imsi=460033091264872&lang=zh_CN&lfid=100303type%3D1%26t%3D3' \
              '&page={page}&skin=default&count=10&oldwm=2468_1001&sflag=1&oriuicode=10000010_10000003' \
              '&containerid=100103type%3D1%26q%3D%E7%89%A9%E8%B5%84%26t%3D0&ignore_inturrpted_error=true' \
              '&luicode=10000003&sensors_mark=0&android_id=74827df6acded925&client_key=d41d8cd98f00b204e9800998ecf8427e' \
              '&need_new_pop=1&sensors_is_first_day=none' \
              '&container_ext=newhistory%3A0%7Cnettype%3Awifi%7Cshow_topic%3A1%7Cgps_timestamp%3A1627529614161' \
              '&need_head_cards=1&cum=93D4C907'.format(network=random.choice(NetworkPool),wb=random.choice(range(4033,5033)),
                                                       ua=random.choice(User_Agent),page=page
                                                       )

        results = get_response(url)
        # print(results)
        time.sleep(12)
        if results != 1:
            handle_data(results,page,p)
            print("-{}-Page-{}-Finished-{}-".format('-' * 40, page,'-' * 40))
        else:
            break

def main():

    p = mongo()
    get_page(p)

if __name__ == '__main__':
    main()