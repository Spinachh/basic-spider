# !/usr/bin/python
# -*-coding:utf-8-*-
# Author：Mongoole
# Date：2020/11/11

from os import path
import jieba
import collections

# 源码所在目录
d = path.dirname(__file__)

# txt = open(path.join(d, '../CommentContentTxt/comment_content')).read()

with open(r'E:\study\python\Python-Exercise\project-spider-data\WeiboApp\CommentContentTxt\longlingmiku.txt','r',encoding='utf-8') as f:
    comment_content = f.readlines()

txt = "".join(comment_content)

# for ch in '-#$%^&*()@:{}_+[]~\n':
#     txt = txt.replace(ch, " ")  # 将文本中的特殊字符转换为空格替代

my_stop_words = []
stop_words = open(r"E:\study\python\Python-Exercise\project-spider-data\stopwords-master\cn_stopwords.txt", "r", encoding='UTF-8').read().split("\n")

wordsls = jieba.lcut(txt)

object_list = []
for i in wordsls:
    if len(i) != 1:
        # if i not in my_stop_words:
        if i not in stop_words:
            object_list.append(i)

word_counts = collections.Counter(object_list)  # 对分词做词频统计
#All WordFrenquency
# with open('WordFrenquency.txt','w',encoding='utf-8') as f:
#     f.write(str(word_counts))

#
word_frequency = word_counts.most_common(5)  # 获取前n最高频的词
word_frequency_10 = word_counts.most_common(10)  # 获取前n最高频的词
word_frequency_15 = word_counts.most_common(15)  # 获取前n最高频的词
word_frequency_25 = word_counts.most_common(25)  # 获取前n最高频的词
word_frequency_35 = word_counts.most_common(35)  # 获取前n最高频的词
word_frequency_50 = word_counts.most_common(50)  # 获取前n最高频的词
word_frequency_70 = word_counts.most_common(70)  # 获取前n最高频的词
word_frequency_100 = word_counts.most_common(100)  # 获取前n最高频的词

# print(word_counts_top10)  # 输出检查
print('前5：{}'.format(word_frequency))
print('前10：{}'.format(word_frequency_10))
print('前15：{}'.format(word_frequency_15))
print('前25：{}'.format(word_frequency_25))
print('前35：{}'.format(word_frequency_35))
print('前50：{}'.format(word_frequency_50))
print('前70：{}'.format(word_frequency_70))
print('前100：{}'.format(word_frequency_100))
