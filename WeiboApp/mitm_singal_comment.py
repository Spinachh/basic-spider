# !/usr/bin/python
# -*-coding:utf-8-*-
# Author：Mongoole
# Date：2020/10/22

import json
import time
import pymongo
from snownlp import SnowNLP


def mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1',port=27017)
    #选择数据库
    db = client.weibo_comment
    #选择操作的集合
    p = db.comment_content_test
    return p

p = mongo()

#必须这样写！因为是要获取响应的内容，mitmdump规定必须这样写才能获取到数据,所以必须写response
def response(flow):

    #获取评论内容
    if "/2/comments/build_comments?" in flow.request.url:
        print("-{}-We got The Data-{}-".format('-'*50,'-'*50))
        text = flow.response.text
        content = json.loads(text)
        if "datas" in content:
            results = content["datas"]
            # print(results[0])
            for result in results:
                data_dict = {}
                if 'adType' not in result:
                    item = result["data"]
                    data_dict["comment_id"] = item["id"]
                    data_dict["comment_user_name"] = item["user"]["name"]
                    text = item["text"]
                    data_dict["text"] = text
                    nlp = SnowNLP(text)
                    data_dict['score'] = nlp.sentiments

                    data_dict["time"] = item["created_at"]
                    if "like_counts" in item.keys():
                        data_dict["like_counts"] = item["like_counts"]
                    else:
                        data_dict["like_counts"] = item["attitudes_count"]

                    data_dict["account_name"] = "人民日报-2020-1-30 09:59"
                    data_dict["comment_url"] = "https://m.weibo.cn/status/4466372166266897?"
                    p.insert(data_dict)
                    print("-{}-Insert into MongoDB Sucessfully-{}-".format('-'*40,'-'*40))
        else:
            results = content["root_comments"]
            # print(results[0])
            for item in results:
                data_dict = {}
                if 'adType' not in item:
                    # item = result["data"]
                    data_dict["comment_id"] = item["id"]
                    data_dict["comment_user_name"] = item["user"]["name"]
                    text = item["text"]
                    data_dict["text"] = text
                    nlp = SnowNLP(text)
                    data_dict['score'] = nlp.sentiments

                    data_dict["time"] = item["created_at"]
                    if "like_counts" in item.keys():
                        data_dict["like_counts"] = item["like_counts"]
                    else:
                        data_dict["like_counts"] = item["attitudes_count"]
                    data_dict["account_name"] = "人民日报-2020-1-30 09:59"
                    data_dict["comment_url"] = "https://m.weibo.cn/status/4466372166266897?"
                    p.insert(data_dict)
                    print("-{}-Insert into MongoDB Sucessfully-{}-".format('-'*40,'-'*40))