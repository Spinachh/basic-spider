# !/usr/bin/python
# -*- coding:utf-8 -*-
# Author：Mongoole
# Date：2020/11/6

import time
import os
import io
import sys
import re
import json
import random
import pymongo
import requests
import urllib3
import logging

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030') #change the default encoding of standard output

try:
    from UserAgentPool import User_Agent
except:
    from ContentApi.UserAgentPool import User_Agent

def mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1',port=27017)
    #选择数据库
    db = client.weibo_publish_content
    #选择操作的集合
    p = db.topicword_publish_content
    return p

def get_response(url):

    # response = requests.get(url,verify=False)
    response = requests.get(url)
    try:
        if response.status_code == 200:
            content = response.json()
            # print(content)
            # breakpoint()
            if content['cards']:
                cards = content['cards']

                return cards
            else:
                print('This function get_response has error:{}'.format(content))
                flag = 1
                return flag

    except Exception as e:
        print(e)


def handle_data(cards,page,p,user_name):

    since_time = '2021-3-31 00:00:00'
    until_time = '2021-01-02 00:00:00'

    #用户昵称，用户发帖标识，发文时间，发文内容，点赞量，评论量，转发量，来源
    for card in cards:
        if 'card_group' in card.keys():
            results = card['card_group']
            for result in results:
                data_dict = {}

                if 'mblog' in result.keys():
                    item = result["mblog"]
                    data_dict['account'] = item['user']['name']
                    if 'title' in item.keys():
                        try:
                            data_dict['account_type'] = item['title']['text']
                        except:
                            data_dict['account_type'] = ''

                    created_at = item["created_at"]
                    struct_time = time.strptime(created_at, "%a %b %d %H:%M:%S %z %Y")
                    format_time = time.strftime("%Y-%m-%d %H:%M:%S", struct_time)
                    created_stamp_time = time.mktime(time.strptime(format_time, "%Y-%m-%d %H:%M:%S"))
                    since_stamp_time = time.mktime(time.strptime(since_time, "%Y-%m-%d %H:%M:%S"))
                    until_stamp_time = time.mktime(time.strptime(until_time, "%Y-%m-%d %H:%M:%S"))

                    data_dict["created_at"] = format_time       #发布时间
                    data_dict["text"] = item["text"]        #文本内容

                    data_dict["comments_count"] = item["comments_count"]    #评论数量
                    data_dict["attitude_count"] = item["attitudes_count"]   #正文点赞
                    data_dict["reposts_count"] = item["reposts_count"]      #转发数量



                    if "source" in item.keys():
                        source = item["source"]
                        # print(source)
                        tools = re.findall(r'rel="nofollow">(.*?)</a>', source)[0]
                        data_dict["tools"] = tools
                    else:
                        data_dict["tools"] = None

                    data_dict["id"] = item["idstr"]
                    data_dict["mblogid"] = item["mblogid"]


                    account_id = item['user']['idstr']
                    scheme = item["scheme"].split("=")[1]
                    data_dict["weibo_url"] = "https://weibo.com/{}/".format(account_id) + scheme

                    data_dict["topicname"] = user_name

                    p.insert(data_dict)

                # else:
                #     print('{}WE DIDN\'T NEED THIS PAGE {} !{}'.format('-'*20,page,'-'*20))
                #     flag = 1
                #     return flag

def get_app_uid():
    with open('UserAccountFidMainMedia',encoding='gbk') as f:
        all = f.readlines()

    return all


def get_page(user_name,p):

    NetworkPool = ['wifi','4G']
    NumPool = [1,2]
    # 此处的url,每一个账号的containerid都是不一样的
    # for page in range(1, 500):
    for page in range(1, 400):
        #huawei
        # url = 'https://api.weibo.cn/{num}/page?networktype={network}&extparam=%E5%8D%8E%E4%B8%BA&page_reform_enable=1&sensors_device_id=none&page_interrupt_enable=1&orifid=231619%24%24100303type%3D1%26q%3D%E5%8D%8E%E4%B8%BA%E8%B6%85%E8%AF%9D%26t%3D0&uicode=10000011&page_id=100808d63e7ea059dc68e97e2ef16531e47e6c&moduleID=708&featurecode=10000085&feed_mypage_card_remould_enable=true&just_followed=false&wb_version=4033&c=android&s=5c4f0bff&ft=0&ua={ua}&wm=2468_1001&aid=01Az61c9YF05xgx3kYZaYCVaztYMZANbI60EkQqwHsBvw3Ig4.&ext=orifid%3A231619%24%24100303type%3D1%26q%3D%E5%8D%8E%E4%B8%BA%E8%B6%85%E8%AF%9D%26t%3D0%7Coriuicode%3A10000010_10000003&fid=100808d63e7ea059dc68e97e2ef16531e47e6c_-_feed&v_f=2&v_p=76&from=1098495010&gsid=_2A25MrsxdDeRxGeNH4lIY-C_FyDSIHXVt-liVrDV6PUJbkdANLRXtkWpNSnNFMRXZ9WOXcbKrF7jZMMkCARiajyEc&imsi=460066773510948&lang=zh_CN&lfid=100303type%3D1%26q%3D%E5%8D%8E%E4%B8%BA%E8%B6%85%E8%AF%9D%26t%3D0' \
        #       '&page={page}&skin=default&count=15&oldwm=2468_1001&sflag=1&oriuicode=10000010_10000003' \
        #       '&containerid=100808d63e7ea059dc68e97e2ef16531e47e6c_-_sort_time&ignore_inturrpted_error=true' \
        #       '&luicode=10000003&sensors_mark=0&android_id=3192ca97e726dcf5&header_skin_enable=0' \
        #       '&sensors_is_first_day=none&page_common_ext=topicPrompt%3A1%7Cpage%3Afeed%3D1%26sort_time%3D2&cum=1FFEFAD8'.format(
        #         num=random.choice(NumPool),
        #         network=random.choice(NetworkPool),
        #         ua=random.choice(User_Agent),
        #         page=page,)


        #vivo

        url = 'https://api.weibo.cn/{num}/page?networktype={network}&extparam=vivo&page_reform_enable=1' \
              '&sensors_device_id=none&page_interrupt_enable=1' \
              '&orifid=231619%24%24100303type%3D1%26t%3D3%24%24100103type%3D1%26q%3Dvivo%E8%B6%85%E8%AF%9D%26t%3D0&uicode=10000011&page_id=100808b09207f5c299c42708e1badb72c04025&moduleID=708' \
              '&featurecode=10000085&feed_mypage_card_remould_enable=true&just_followed=false' \
              '&wb_version=4033&c=android&s=5c4f0bff&ft=0' \
              '&ua={ua}&wm=2468_1001&aid=01Az61c9YF05xgx3kYZaYCVaztYMZANbI60EkQqwHsBvw3Ig4.' \
              '&ext=orifid%3A231619%24%24100303type%3D1%26t%3D3%24%24100103type%3D1%26q%3Dvivo%E8%B6%85%E8%AF%9D%26t%3D0%7Coriuicode%3A10000010_10000003_10000003&fid=100808b09207f5c299c42708e1badb72c04025_-_feed&v_f=2&v_p=76' \
              '&from=1098495010&gsid=_2A25MrsxdDeRxGeNH4lIY-C_FyDSIHXVt-liVrDV6PUJbkdANLRXtkWpNSnNFMRXZ9WOXcbKrF7jZMMkCARiajyEc&imsi=460066773510948&lang=zh_CN&lfid=100103type%3D1%26q%3Dvivo%E8%B6%85%E8%AF%9D%26t%3D0&page={page}&skin=default&count=15&oldwm=2468_1001&sflag=1&oriuicode=10000010_10000003_10000003&containerid=100808b09207f5c299c42708e1badb72c04025_-_sort_time&ignore_inturrpted_error=true&luicode=10000003&sensors_mark=0&android_id=3192ca97e726dcf5&header_skin_enable=0&sensors_is_first_day=none&page_common_ext=topicPrompt%3A1%7Cpage%3Afeed%3D1&cum=25F5A005'.format(
                num=random.choice(NumPool),
                network=random.choice(NetworkPool),
                ua=random.choice(User_Agent),
                page=page,)

        ##iphone
        # url = 'https://api.weibo.cn/{num}/page?networktype={network}&extparam=iPhone&page_reform_enable=1&sensors_device_id=none&page_interrupt_enable=1&orifid=231619%24%24100303type%3D1%26t%3D3%24%24100103type%3D1%26q%3DiPhone%E8%B6%85%E8%AF%9D%26t%3D1&uicode=10000011&page_id=100808867e57bd062c7169995dc03cc0541c19&moduleID=708&featurecode=10000085&feed_mypage_card_remould_enable=true&just_followed=false&wb_version=4033&c=android&s=5c4f0bff&ft=0&ua={ua}&wm=2468_1001&aid=01Az61c9YF05xgx3kYZaYCVaztYMZANbI60EkQqwHsBvw3Ig4.&ext=orifid%3A231619%24%24100303type%3D1%26t%3D3%24%24100103type%3D1%26q%3DiPhone%E8%B6%85%E8%AF%9D%26t%3D1%7Coriuicode%3A10000010_10000003_10000003&v_f=2&v_p=76&from=1098495010&gsid=_2A25MrsxdDeRxGeNH4lIY-C_FyDSIHXVt-liVrDV6PUJbkdANLRXtkWpNSnNFMRXZ9WOXcbKrF7jZMMkCARiajyEc&imsi=460066773510948&lang=zh_CN&lfid=100103type%3D1%26q%3DiPhone%E8%B6%85%E8%AF%9D%26t%3D1&page={page}&skin=default&count=20&oldwm=2468_1001&sflag=1&oriuicode=10000010_10000003_10000003&ignore_inturrpted_error=true&luicode=10000003&sensors_mark=0&android_id=3192ca97e726dcf5&header_skin_enable=0&sensors_is_first_day=none&cum=E5FDD130'.format(
        #     num=random.choice(NumPool),
        #     network=random.choice(NetworkPool),
        #     ua=random.choice(User_Agent),
        #     page=page, )

        #xiaomi
        # urlll = 'https://api.weibo.cn/{num}/page?networktype={network}&extparam=%E5%B0%8F%E7%B1%B3&page_reform_enable=1&sensors_device_id=none&page_interrupt_enable=1&orifid=231619%24%24100303type%3D1%26t%3D3%24%24100103type%3D1%26q%3D%E5%B0%8F%E7%B1%B3%E8%B6%85%E8%AF%9D%26t%3D0&uicode=10000011&page_id=1008082be446577762bb6cec8681f5e2d67d18&moduleID=708&featurecode=10000085&feed_mypage_card_remould_enable=true&just_followed=false&wb_version=4033&c=android&s=5c4f0bff&ft=0&ua={ua}&wm=2468_1001&aid=01Az61c9YF05xgx3kYZaYCVaztYMZANbI60EkQqwHsBvw3Ig4.&ext=orifid%3A231619%24%24100303type%3D1%26t%3D3%24%24100103type%3D1%26q%3D%E5%B0%8F%E7%B1%B3%E8%B6%85%E8%AF%9D%26t%3D0%7Coriuicode%3A10000010_10000003_10000003&fid=1008082be446577762bb6cec8681f5e2d67d18_-_feed&v_f=2&v_p=76&from=1098495010&gsid=_2A25MrsxdDeRxGeNH4lIY-C_FyDSIHXVt-liVrDV6PUJbkdANLRXtkWpNSnNFMRXZ9WOXcbKrF7jZMMkCARiajyEc&imsi=460066773510948&lang=zh_CN&lfid=100103type%3D1%26q%3D%E5%B0%8F%E7%B1%B3%E8%B6%85%E8%AF%9D%26t%3D0&page={page}&skin=default&count=15&oldwm=2468_1001&sflag=1&oriuicode=10000010_10000003_10000003&containerid=1008082be446577762bb6cec8681f5e2d67d18_-_sort_time&ignore_inturrpted_error=true&luicode=10000003&sensors_mark=0&android_id=3192ca97e726dcf5&header_skin_enable=0&sensors_is_first_day=none&page_common_ext=topicPrompt%3A1%7Cpage%3Afeed%3D1&cum=9D05682B'.format(
        #     num=random.choice(NumPool),
        #     network=random.choice(NetworkPool),
        #     ua=random.choice(User_Agent),
        #     page=page, )
        # url = 'https://api.weibo.cn/{num}/page?networktype={network}&extparam=%E5%B0%8F%E7%B1%B3&page_reform_enable=1&sensors_device_id=none&page_interrupt_enable=1&orifid=231619%24%24100303type%3D1%26t%3D3%24%24100103type%3D1%26q%3D%E5%B0%8F%E7%B1%B3%E8%B6%85%E8%AF%9D%26t%3D0&uicode=10000011&page_id=1008082be446577762bb6cec8681f5e2d67d18&moduleID=708&featurecode=10000085&feed_mypage_card_remould_enable=true&just_followed=false&wb_version=4033&c=android&s=5c4f0bff&ft=0&ua={ua}&wm=2468_1001&aid=01Az61c9YF05xgx3kYZaYCVaztYMZANbI60EkQqwHsBvw3Ig4.&ext=orifid%3A231619%24%24100303type%3D1%26t%3D3%24%24100103type%3D1%26q%3D%E5%B0%8F%E7%B1%B3%E8%B6%85%E8%AF%9D%26t%3D0%7Coriuicode%3A10000010_10000003_10000003&fid=1008082be446577762bb6cec8681f5e2d67d18_-_feed&v_f=2&v_p=76&from=1098495010&gsid=_2A25MrsxdDeRxGeNH4lIY-C_FyDSIHXVt-liVrDV6PUJbkdANLRXtkWpNSnNFMRXZ9WOXcbKrF7jZMMkCARiajyEc&imsi=460066773510948&lang=zh_CN&lfid=100103type%3D1%26q%3D%E5%B0%8F%E7%B1%B3%E8%B6%85%E8%AF%9D%26t%3D0&page={page}&skin=default&count=15&oldwm=2468_1001&sflag=1&oriuicode=10000010_10000003_10000003&containerid=1008082be446577762bb6cec8681f5e2d67d18_-_sort_time&ignore_inturrpted_error=true&luicode=10000003&sensors_mark=0&android_id=3192ca97e726dcf5&header_skin_enable=0&sensors_is_first_day=none&page_common_ext=topicPrompt%3A1%7Cpage%3Afeed%3D1%26sort_time%3D1&cum=235733A6'.format(
        #     num=random.choice(NumPool),
        #     network=random.choice(NetworkPool),
        #     ua=random.choice(User_Agent),
        #     page=page,
        # )
        #xiaomi2
        # containerid = 1076032598123925
        cards = get_response(url)
        time.sleep(2)
        if cards != 1:
            flag = handle_data(cards,page,p,user_name)
            # print("-{}-{}-Page-{}-Finished-{}-".format('-' * 10, user_name, page,'-' * 10))
            logging.info("-{}-{}-Page-{}-Finished-{}-".format('-' * 10, user_name, page,'-' * 10))
        else:
            break

        if flag != 1:
            time.sleep(11)
            continue
        else:
            break

def main():

    user_name = 'iphone超话'
    p = mongo()

    get_page(user_name, p)

    """
    all = get_app_uid()

    for item in all:
        item = item.split('#####')
        user_id = item[1].strip()
        user_name = item[0].strip()
        get_page(user_id,user_name,p)
    """

if __name__ == '__main__':
    main()
