# !/usr/bin/python
# -*- coding:utf-8 -*-
# Author：Mongoole
# Date：2020/11/6

import time
import os
import io
import sys
import re
import json
import random
import pymongo
import requests
from snownlp import SnowNLP
import urllib3
import logging

urllib3.disable_warnings()
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030') #change the default encoding of standard output

try:
    from UserAgentPool import User_Agent
except:
    from ContentApi.UserAgentPool import User_Agent

def mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1',port=27017)
    #选择数据库
    db = client.weibo_publish_content
    #选择操作的集合
    p = db.user_publish_content
    return p

def get_response(url):

    # response = requests.get(url,verify=False)
    response = requests.get(url)
    try:
        if response.status_code == 200:
            content = response.json()
            print(content)
            breakpoint()
            if content['cards']:
                results = content['cards']
                return results
            else:
                print('This function get_response has error:{}'.format(content))
                flag = 1
                return flag

    except Exception as e:
        print(e)


def handle_data(results,page,p):

    since_time = '2021-3-31 00:00:00'
    until_time = '2021-01-02 00:00:00'

    for result in results:
        data_dict = {}
        if 'mblog' in result.keys() and 'title' not in result['mblog'].keys():
            item = result["mblog"]
            # data_dict["text"] = item["text"]        #文本内容
            text = item["text"]
            data_dict["text"] = text
            # nlp = SnowNLP(text)
            # data_dict['score'] = nlp.sentiments
            created_at = item["created_at"]
            struct_time = time.strptime(created_at, "%a %b %d %H:%M:%S %z %Y")
            format_time = time.strftime("%Y-%m-%d %H:%M:%S", struct_time)
            created_stamp_time = time.mktime(time.strptime(format_time, "%Y-%m-%d %H:%M:%S"))
            since_stamp_time = time.mktime(time.strptime(since_time, "%Y-%m-%d %H:%M:%S"))
            until_stamp_time = time.mktime(time.strptime(until_time, "%Y-%m-%d %H:%M:%S"))
            ##This Judge time direction
            if since_stamp_time < created_stamp_time:
            # if since_time < format_time and format_time < until_time:
                data_dict["created_at"] = format_time       #发布时间
                data_dict["comments_count"] = item["comments_count"]    #评论数量
                data_dict["attitude_count"] = item["attitudes_count"]   #正文点赞
                data_dict["reposts_count"] = item["reposts_count"]      #转发数量

                # 是否是转repost
                if "retweeted_status" in item.keys():
                    retweeted = item["retweeted_status"]
                    data_dict["retweeted_flag"] = "转发"

                    if "text" in retweeted.keys():
                        # data_dict["retweeted_text"] = retweeted["text"]
                        retweeted_text = retweeted["text"]
                        data_dict["retweeted_text"] = retweeted_text
                        retweeted_nlp = SnowNLP(retweeted_text)
                        data_dict['retweeted_score'] = retweeted_nlp.sentiments
                    else:
                        data_dict["retweeted_text"] = None
                        data_dict['retweeted_score'] = None
                    if "attitudes_count" in retweeted.keys():
                        data_dict["retweeted_attitude"] = retweeted["attitudes_count"]
                    else:
                        data_dict["retweeted_attitude"] = None

                    if "comments_count" in retweeted.keys():
                        data_dict["retweeted_comment"] = retweeted["comments_count"]
                    else:
                        data_dict["retweeted_comment"] = None

                    re_created_at = retweeted["created_at"]
                    re_struct_time = time.strptime(re_created_at, "%a %b %d %H:%M:%S %z %Y")
                    re_format_time = time.strftime("%Y-%m-%d %H:%M:%S", re_struct_time)
                    stamp_time = time.mktime(time.strptime(re_format_time, "%Y-%m-%d %H:%M:%S"))
                    data_dict["retweeted_created_at"] = re_format_time

                    if "pic_num" in retweeted.keys():
                        retweeted_pic_num = retweeted["pic_num"]
                    else:
                        retweeted_pic_num = None

                    # print(type(retweeted_pic_num))
                    data_dict["retweeted_pic_num"] = retweeted_pic_num

                    if "pic_ids" in retweeted.keys():
                        re_pic_ids = retweeted["pic_ids"]
                    else:
                        re_pic_ids = None


                    if "pic_infos" in retweeted.keys():
                        retweeted_pic = retweeted["pic_infos"]
                        pic_lst = [retweeted_pic[pic_ids]["large"]["url"] for pic_ids in re_pic_ids]
                        data_dict["retweeted_imgs"] = pic_lst
                    else:
                        data_dict["retweeted_imgs"] = None

                    if "reposts_count" in retweeted.keys():
                        data_dict["retweeted_repost"] = retweeted["reposts_count"]
                    else:
                        data_dict["retweeted_repost"] = None

                    if "source" in retweeted.keys():
                        source = item["source"]
                        tools = re.findall(r'rel="nofollow">(.*?)</a>', source)
                        data_dict["retweeted_tools"] = tools
                    else:
                        data_dict["retweeted_tools"] = None

                    if "user" in retweeted.keys():
                        data_dict["retweeted_account"] = retweeted["user"]["name"]

                        re_account_id = retweeted['user']['idstr']
                        re_scheme = retweeted["scheme"].split("=")[1]
                        data_dict["retweeted_url"] = "https://weibo.com/{}/".format(re_account_id) + re_scheme
                        # print(data_dict["retweeted_url"])
                    else:
                        data_dict["retweeted_account"] = None
                        data_dict["retweeted_url"] = None


                else:
                    data_dict["retweeted_text"] = None
                    data_dict["retweeted_attitude"] = None
                    data_dict["retweeted_comment"] = None
                    data_dict["retweeted_created_at"] = None
                    data_dict["retweeted_pic_num"] = None
                    data_dict["retweeted_repost"] = None
                    data_dict["retweeted_account"] = None
                    data_dict["retweeted_url"] = None
                    data_dict["retweeted_flag"] = "原创"

                pic_num = item["pic_num"]
                data_dict["pic_num"] = pic_num

                if "source" in item.keys():
                    source = item["source"]
                    # print(source)
                    tools = re.findall(r'"rel="nofollow">(.*?)</a>', source)
                    data_dict["tools"] = tools
                else:
                    data_dict["tools"] = None

                data_dict["id"] = item["idstr"]
                data_dict["mblogid"] = item["mblogid"]

                '''
                if 'obj_ext' in item.keys():
                    data_dict["visit_count"] = item["obj_ext"]
                    data_dict["content_type"] = 'text_video'
                else:
                    data_dict["visit_count"] = None
                    if pic_num == '0':
                        data_dict["content_type"] = 'text'
                    else:
                        data_dict["content_type"] = 'text_img'
                '''

                if 'page_info' in item.keys():
                    try:
                        data_dict["visit_count"] = item["page_info"]["media_info"]["online_users"]
                        data_dict["content_type"] = 'text_video'
                    except:
                        data_dict["visit_count"] = None
                        if pic_num == '0':
                            data_dict["content_type"] = 'text'
                        else:
                            data_dict["content_type"] = 'text_img'
                else:
                    data_dict["visit_count"] = None
                    if pic_num == '0':
                        data_dict["content_type"] = 'text'
                    else:
                        data_dict["content_type"] = 'text_img'

                data_dict['account'] = item['user']['name']

                account_id = item['user']['idstr']
                scheme = item["scheme"].split("=")[1]
                data_dict["weibo_url"] = "https://weibo.com/{}/".format(account_id) + scheme

                data_dict["project_name"] = "MainMediaResearch"
                # data_dict["project_name"] = "KexieResearch"

                p.insert(data_dict)

            else:
                print('{}WE DIDN\'T NEED THIS PAGE {} !{}'.format('-'*20,page,'-'*20))
                flag = 1
                return flag

def get_app_uid():
    with open('UserAccountFidMainMedia',encoding='gbk') as f:
        all = f.readlines()

    return all


def get_page(app_uid,user_name,p):

    NetworkPool = ['wifi','4G','5G']
    NumPool = [1,2]
    # 此处的url,每一个账号的containerid都是不一样的
    # for page in range(1, 500):
    for page in range(91, 800):
        url = 'https://api.weibo.cn//2/profile/statuses?networktype={network}&sensors_device_id=none&orifid=231619%24%24100303type%3D1%26t%3D0%24%24100103type%3D1%26q%3D%E4%B8%AD%E5%9B%BD%E7%BB%86%E8%83%9E%E7%94%9F%E7%89%A9%E5%AD%A6%E5%AD%A6%E4%BC%9A%26t%3D1&uicode=10000198&moduleID=708&featurecode=10000085&wb_version={wb}&lcardid=1001030111_0_0_seqid%3A2088066897%7Ctype%3A1%7Ct%3A1%7Cpos%3A1-0-0%7Cq%3A%E4%B8%AD%E5%9B%BD%E7%BB%86%E8%83%9E%E7%94%9F%E7%89%A9%E5%AD%A6%E5%AD%A6%E4%BC%9A%7Cext%3A%26cate%3D1%26uid%3D2233916294%26qri%3D0%26qrt%3D1%26qtime%3D1625733584%26_2233916294_874f5fd6&c=android&s=85a290af&ft=0&ua={ua}&wm=2468_1001&aid=01A5bWyxVdBbINO3LYKfrE4PdCyGNRi_rdI10mF_L7xphh1Zs.&ext=orifid%3A231619%24%24100303type%3D1%26t%3D0%24%24100103type%3D1%26q%3D%E4%B8%AD%E5%9B%BD%E7%BB%86%E8%83%9E%E7%94%9F%E7%89%A9%E5%AD%A6%E5%AD%A6%E4%BC%9A%26t%3D1%7Coriuicode%3A10000010_10000003_10000003&fid={app_uid}_-_WEIBO_SECOND_PROFILE_WEIBO&uid=7490293470&v_f=2&v_p=76&from=1098495010&gsid=_2A25N2EPIDeRxGeFK4lIT-S3IzDyIHXVszNAArDV6PUJbkdANLWijkWpNQtkA4KE9HC6nhNbbeZcx5I3fyGjcF6kY&imsi=460033091264872&lang=zh_CN&lfid=100103type%3D1%26q%3D%E4%B8%AD%E5%9B%BD%E7%BB%86%E8%83%9E%E7%94%9F%E7%89%A9%E5%AD%A6%E5%AD%A6%E4%BC%9A%26t%3D1&page={page}&skin=default&count=20&oldwm=2468_1001&sflag=1&oriuicode=10000010_10000003_10000003&containerid={app_uid}_-_WEIBO_SECOND_PROFILE_WEIBO&ignore_inturrpted_error=true&luicode=10000003&sensors_mark=0&android_id=74827df6acded925&client_key=d41d8cd98f00b204e9800998ecf8427e&need_new_pop=1&sensors_is_first_day=none&need_head_cards=0&cum=7208E327'.format(num=random.choice(NumPool),
       #url = 'https://api.weibo.cn//{num}/profile/statuses?networktype={network}&sensors_device_id=none&orifid=231619%24%24100303type%3D1%26t%3D3%24%24100103type%3D1%26q%3D%E7%A6%8F%E5%BB%BA%E7%A7%91%E6%99%AE%26t%3D1&uicode=10000198&moduleID=708&featurecode=10000085&wb_version={wb}&lcardid=1001030111_0_0_seqid%3A801785577%7Ctype%3A1%7Ct%3A1%7Cpos%3A1-0-0%7Cq%3A%E7%A6%8F%E5%BB%BA%E7%A7%91%E6%99%AE%7Cext%3A%26cate%3D1%26uid%3D6083461336%26qri%3D0%26qrt%3D1%26qtime%3D1625141867%26_6083461336_80899419&c=android&s=85a290af&ft=0&ua={ua}&wm=2468_1001&aid=01A5bWyxVdBbINO3LYKfrE4PdCyGNRi_rdI10mF_L7xphh1Zs.&ext=orifid%3A231619%24%24100303type%3D1%26t%3D3%24%24100103type%3D1%26q%3D%E7%A6%8F%E5%BB%BA%E7%A7%91%E6%99%AE%26t%3D1%7Coriuicode%3A10000010_10000003_10000003&fid={app_uid}_-_WEIBO_SECOND_PROFILE_WEIBO&uid=7490293470&v_f=2&v_p=76&from=1098495010&gsid=_2A25N2EPIDeRxGeFK4lIT-S3IzDyIHXVszNAArDV6PUJbkdANLWijkWpNQtkA4KE9HC6nhNbbeZcx5I3fyGjcF6kY&imsi=460033091264872&lang=zh_CN&lfid=100103type%3D1%26q%3D%E7%A6%8F%E5%BB%BA%E7%A7%91%E6%99%AE%26t%3D1&page={page}&skin=default&count=20&oldwm=2468_1001&sflag=1&oriuicode=10000010_10000003_10000003&containerid={app_uid}_-_WEIBO_SECOND_PROFILE_WEIBO&ignore_inturrpted_error=true&luicode=10000003&sensors_mark=0&android_id=74827df6acded925&client_key=d41d8cd98f00b204e9800998ecf8427e&need_new_pop=1&sensors_is_first_day=none&need_head_cards=0&cum=2C7ABE22'.format(num=random.choice(NumPool),
                                              network=random.choice(NetworkPool),
                                              wb=random.choice(range(4033, 5033)),
                                              ua=random.choice(User_Agent),
                                              page=page,
                                              fid=app_uid,
                                              app_uid=app_uid
                                              )
        # containerid = 1076032598123925
        results = get_response(url)
        time.sleep(2)
        if results != 1:
            flag = handle_data(results,page,p)
            print("-{}-{}-Page-{}-Finished-{}-".format('-' * 10, user_name, page,'-' * 10))
        else:
            break

        if flag != 1:
            time.sleep(11)
            continue
        else:
            break


def main():

    p = mongo()

    all = get_app_uid()

    for item in all:
        item = item.split('#####')
        user_id = item[1].strip()
        user_name = item[0].strip()
        get_page(user_id,user_name,p)


if __name__ == '__main__':
    main()
