# !/usr/bin/python
# -*-coding:utf-8-*-
# Author：Mongoole
# Date：2020/10/26

import time
import json
import pymongo
import re

# until_time = "2020-09-31 00:00:00"


def mongo():
    #连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1',port=27017)
    #选择数据库
    db = client.weibo_content_text
    #选择操作的集合
    p = db.weibo_content
    return p

p = mongo()

#必须这样写！因为是要获取响应的内容，mitmdump规定必须这样写才能获取到数据,所以必须写response
def response(flow):
    #获取博文内容
    if "/2/profile/statuses?" in flow.request.url:
        print("-{}-We got The Data-{}-".format('-'*50,'-'*50))
        text = flow.response.text
        content = json.loads(text)
        results = content["cards"]
        for result in results:
            data_dict = {}
            if 'mblog' in result:
                # print(result)
                item = result["mblog"]
                data_dict["text"] = item["text"]

                created_at = item["created_at"]
                struct_time = time.strptime(created_at,"%a %b %d %H:%M:%S %z %Y")
                format_time = time.strftime("%Y-%m-%d %H:%M:%S",struct_time)
                stamp_time = time.mktime(time.strptime(format_time,"%Y-%m-%d %H:%M:%S"))
                data_dict["created_at"] = format_time

                data_dict["comments_count"] = item["comments_count"]
                data_dict["attitude_count"] = item["attitudes_count"]
                data_dict["reposts_count"] = item["reposts_count"]

                #是否是转repost
                if "retweeted_status" in item.keys():
                    retweeted = item["retweeted_status"]

                    if "text" in retweeted.keys():
                        data_dict["retweeted_text"] = retweeted["text"]
                    else:
                        data_dict["retweeted_text"] = None

                    if "attitudes_count" in retweeted.keys():
                        data_dict["retweeted_attitude"] = retweeted["attitudes_count"]
                    else:
                        data_dict["retweeted_attitude"] = None

                    if "comments_count" in retweeted.keys():
                        data_dict["retweeted_comment"] = retweeted["comments_count"]
                    else:
                        data_dict["retweeted_comment"] = None


                    re_created_at = retweeted["created_at"]
                    re_struct_time = time.strptime(re_created_at, "%a %b %d %H:%M:%S %z %Y")
                    re_format_time = time.strftime("%Y-%m-%d %H:%M:%S", re_struct_time)
                    stamp_time = time.mktime(time.strptime(re_format_time, "%Y-%m-%d %H:%M:%S"))
                    data_dict["retweeted_created_at"] = re_format_time

                    if "pic_num" in retweeted.keys():
                        retweeted_pic_num = retweeted["pic_num"]
                    else:
                        retweeted_pic_num = None

                    # print(type(retweeted_pic_num))
                    data_dict["retweeted_pic_num"] = retweeted_pic_num

                    if "pic_ids" in retweeted.keys():
                        re_pic_ids = retweeted["pic_ids"]
                    else:
                        re_pic_ids = None

                    # print(re_pic_ids)

                    if "pic_infos" in retweeted.keys():
                        retweeted_pic = retweeted["pic_infos"]
                        pic_lst =[retweeted_pic[pic_ids]["large"]["url"] for pic_ids in re_pic_ids]
                        data_dict["retweeted_imgs"] = pic_lst
                    else:
                        data_dict["retweeted_imgs"] = None
                    if "reposts_count" in retweeted.keys():
                        data_dict["retweeted_repost"] = retweeted["reposts_count"]
                    else:
                        data_dict["retweeted_repost"] = None

                    if "source" in retweeted.keys():
                        source = item["source"]
                        # print(source)
                        tools = re.findall(r'"rel="nofollow">(.*?)</a>', source)
                        data_dict["retweeted_tools"] = tools
                    else:
                        data_dict["retweeted_tools"] = None

                    if "user" in retweeted.keys():
                        data_dict["retweeted_account"] = retweeted["user"]["name"]

                        re_account_id = retweeted['user']['idstr']
                        re_scheme = retweeted["scheme"].split("=")[1]
                        data_dict["retweeted_url"] = "https://weibo.com/{}/".format(re_account_id) + re_scheme
                        # print(data_dict["retweeted_url"])
                    else:
                        data_dict["retweeted_account"] = None
                        data_dict["retweeted_url"] = None


                else:
                    data_dict["retweeted_text"] = None
                    data_dict["retweeted_attitude"] = None
                    data_dict["retweeted_comment"] = None
                    data_dict["retweeted_created_at"] = None
                    data_dict["retweeted_pic_num"] = None
                    data_dict["retweeted_repost"] = None
                    data_dict["retweeted_account"] = None
                    data_dict["retweeted_url"] = None

                pic_num = item["pic_num"]

                data_dict["pic_num"] = pic_num

                if "source" in item.keys():
                    source = item["source"]
                    # print(source)
                    tools = re.findall(r'"rel="nofollow">(.*?)</a>',source)
                    data_dict["tools"] = tools
                else:
                    data_dict["tools"] = None

                data_dict["id"] = item["idstr"]
                data_dict["mblogid"] = item["mblogid"]

                if 'obj_ext' in item.keys():
                    data_dict["visit_count"] = item["obj_ext"]
                    data_dict["content_type"] = 'text_video'
                else:
                    data_dict["visit_count"] = None
                    if pic_num == '0':
                        data_dict["content_type"] = 'text'
                    else:
                        data_dict["content_type"] = 'text_img'

                data_dict['account'] = item['user']['name']

                account_id = item['user']['idstr']
                scheme = item["scheme"].split("=")[1]
                data_dict["weibo_url"] = "https://weibo.com/{}/".format(account_id) + scheme

                p.insert(data_dict)
                print("-{}-Insert into MongoDB Sucessfully-{}-".format('-'*40,'-'*40))
