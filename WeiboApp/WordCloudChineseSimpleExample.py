# !/usr/bin/python
# -*-coding:utf-8-*-
# Author：Mongoole
# Date：2020/11/11


from wordcloud import WordCloud
import matplotlib.pyplot as plt

f = open(r'CommentContentTxt/comment_content', 'r',encoding='utf-8').read()
wordcloud = WordCloud(background_color="white", width=1000, height=860, margin=2).generate(f)
plt.imshow(wordcloud)
plt.axis("off")
plt.show()
wordcloud.to_file(r'CommentContentTxt/example1.png')
