from wordcloud import WordCloud
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import jieba

# 打开文本
text = open(r'CommentContentTxt/comment_content',encoding='utf-8').read()

# 中文分词
text = ' '.join(jieba.cut(text))
print(text[:100])

# 生成对象
mask = np.array(Image.open(r"BackGroundPicture\timg.jpg"))

# wc = WordCloud(mask=mask, font_path=r'C:\Windows\Fonts\STFANGSO.TTF', mode='RGBA', background_color='white').generate(text)
wc = WordCloud(mask=mask, font_path=r'C:\Windows\Fonts\STFANGSO.TTF', mode='RGBA', background_color='white').generate(text)

# 显示词云
plt.imshow(wc, interpolation='bilinear')
plt.axis("off")

#new_add_here
plt.figure()
plt.imshow(mask, cmap="gray", interpolation='bilinear')
plt.axis("off")
#new_add_final

plt.show()

# 保存到文件
wc.to_file(r'WordCloudPicture\wordcloud2.png')