# -*- coding:utf-8 -*-
# Author : mongooses
# Date : 2022/8/23

import os
import io
import re
import sys
import time
import json
import pymongo
import asyncio
import cchardet
import requests
import logging

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')  # 改变标准输出的默认编码
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongo():
    # 连接数据库mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # 选择数据库
    db = client.baidu_content
    # 选择操作的集合
    mongodb = db.baidu_keyword

    return mongodb


def get_response(url):

    headers = {
        "Accept": "*/*",
        # "Accept-Encoding": "gzip, deflate, br",   # 压缩方式不一样，一般都是gzip。br压缩方式较轻便。会导致乱码。
        "Accept-Language": "zh-CN,zh;q=0.9",
        "Connection": "keep-alive",
        "Cookie": "BIDUPSID=437ED3B513110FD24FC3A21A213B4174; PSTM=1661218247; BAIDUID=437ED3B513110FD27D7ABD60330749E9:FG=1; BD_HOME=1; BAIDUID_BFESS=437ED3B513110FD27D7ABD60330749E9:FG=1; BD_UPN=12314353; BA_HECTOR=ak8g8k8181ag8l0g8k8nc94t1hg8be817; ZFY=Va703jQrf756jQyZhT4vlbBFy1Gmc98iNRPeS603rkA:C; delPer=0; BD_CK_SAM=1; PSINO=2; kleck=ee25827314ad06e13b932f075326ed34; BDORZ=B490B5EBF6F3CD402E515D22BCDA1598; COOKIE_SESSION=0_0_0_0_1_0_0_0_0_0_0_0_0_0_2_0_1661218274_0_1661218272%7C1%230_0_1661218272%7C1; H_PS_PSSID=36549_36625_37110_36884_36918_37003_36570_36779_36786_37074_37136_37055_26350_36863_37195; H_PS_645EC=2976BB%2BtJtHBFiQrv6dXax8TfE%2FcfYhditDoiN87VgVDA8lq5ByQv6BOUcM; baikeVisitId=e6d65f3e-c5b0-4b63-bc77-7bd98bf66ebb; B64_BOT=1",
        "Host": "www.baidu.com",
        "is_pbs": "%E6%96%B0%E5%86%A0",
        "is_referer": "https://www.baidu.com/s?wd=%E6%96%B0%E5%86%A0&pn=10&oq=%E6%96%B0%E5%86%A0&ie=utf-8&usm=5&fenlei=256&rsv_idx=1&rsv_pq=d281107c0009d033&rsv_t=c000Q9ODzZ7YbmV8GM%2Bx%2BkzIEc6a7WHFDkt5mmcjKn4ifbirxEaDg8HWcck&bs=%E6%96%B0%E5%86%A0",
        "is_xhr": "1",
        "Referer": "https://www.baidu.com/s?wd=%E6%96%B0%E5%86%A0&pn=20&oq=%E6%96%B0%E5%86%A0&ie=utf-8&usm=5&fenlei=256&rsv_idx=1&rsv_pq=defb60ec00002e53&rsv_t=22f1dBSia5f7RW9V46TGGpv%2F6PMd7%2Byhrccn7ycKdPNaZY6e9eV9kvHT%2FyE",
        "sec-ch-ua": '"Chromium";v="104", " Not A;Brand";v="99", "Google Chrome";v="104"',
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "Windows",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "same-origin",
        "X-Requested-With": "XMLHttpRequest",
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
    }

    try:
        response = requests.get(url, headers=headers)
        logging.warning(response.status_code)
        if response.status_code == 200:
            # html_chariest = cchardet.detect(response.content)
            # print(response.headers["content-type"])
            # print(response.encoding)
            # print(html_chariest)
            # breakpoint()
            html_content = response.text
            # print(html_content)
            # breakpoint()
            return html_content

    except Exception as e:
        print(e)


def get_data(response, mongodb):
    items_list = re.findall(r'class="c-container"><!--s-data:(.*?)--><', response, re.M | re.S)
    # items_list = re.findall(r'"toolsData":"(.*?)",', response, re.M | re.S)
    # print(len(items_list))
    # print(items_list)
    # breakpoint()
    item_key = ['title', 'url', 'source', 'content', 'newtime']
    for item in items_list:
        item = json.loads(item)
        title = item.get('title').replace('<em>', '').replace('</em>', '')
        url = item.get('titleUrl')
        # officialFlag = item.get('officialFlag')
        source = item.get('source')['sitename']
        # ts_pt = item.get('ts_pt')
        content = item.get('contentText').replace('<em>', '').replace('</em>', '')
        newtime = item.get('newTimeFactorStr')
        # print(title, url, source, content, newtime)
        # breakpoint()
        item_value = [title, url, source, content, newtime]
        item_dict = dict(zip(item_key, item_value))
        mongodb.insert_one(item_dict)
        logging.warning('Title: {} Insert MongoDB!'.format(title))


def main():
    mongodb = mongo()
    url = 'https://www.baidu.com/s?wd=%E6%96%B0%E5%86%A0&pn=40&oq=%E6%96%B0%E5%86%A0&ie=utf-8&usm=5&fenlei=256&rsv_idx=1&rsv_pq=bc9f5cc9000158e9&rsv_t=270bcboKXilpv4GtWsLXAHqHpyBDymds0ECJuHfeHoJ178IbdyiWBUEB7dU'
    response = get_response(url)
    get_data(response, mongodb)


if __name__ == '__main__':
    main()