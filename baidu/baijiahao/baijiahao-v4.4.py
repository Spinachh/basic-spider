# -*- coding:utf-8 -*-
# Author：Mongoose
# Date：2023/08/02

import time
import random
import re
import json
import requests
import pymongo
import ssl
import logging
from fake_useragent import UserAgent
import urllib3

urllib3.disable_warnings()
ssl._create_default_https_context = ssl._create_unverified_context


class BaiJiaHao(object):
    since_time = "2020-01-01 00:00:00"
    until_time = "2020-12-01 00:00:00"
    project_name = ""
    project_timezone = ""

    def __init__(self):
        self.crawl_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

    @staticmethod
    def logger_init():
        # 日志输出格式
        logging.basicConfig(format='%(asctime)s,%(msecs)d %(levelname)-4s %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S')
        # 打印日志级别
        # logging.getLogger().setLevel(logging.DEBUG)
        logging.getLogger().setLevel(logging.WARN)
        # logging.getLogger().setLevel(logging.CRITICAL)
        logger = logging.getLogger()
        return logger

    @staticmethod
    def get_user():
        with open(r'user_scienceTec/author_scienceTec', 'r', encoding='utf-8') as f:
            users = f.readlines()
        return users

    @staticmethod
    def mongodb():
        # 连接数据库mongodb
        client = pymongo.MongoClient(host='127.0.0.1', port=27017)
        # 选择数据库
        db = client.baijiahao_content
        # 选择操作的集合
        collection = db.user_content_2023_Q3
        return collection

    def get_response(self, url, user_name):

        cookies = {
            'BIDUPSID': '560C6A7C97B0776F224D5F6110897385',
            'PSTM': '1689919685',
            'BAIDUID': '560C6A7C97B0776FD2C1CD12A868B663:FG=1',
            'MCITY': '-131%3A',
            'BDORZ': 'B490B5EBF6F3CD402E515D22BCDA1598',
            'H_PS_PSSID': '36553_38642_38831_38943_39008_39114_39039_38918_39084_26350_39041_39132_39100_39044_38951',
            'delPer': '0',
            'PSINO': '2',
            'BAIDUID_BFESS': '560C6A7C97B0776FD2C1CD12A868B663:FG=1',
            'Hmery-Time': '2680258296',
            'ab_sr': '1.0.1_MGM1MDc0MWMzZjYzNjBhZTI2Njk2Mzk0OWQ0NWE0NjI3YmU1Zjc0ZjIwMzQzNDc1MTY4NjNjNjA5OTAwMGRlY2EyOTZiMjYwMDc5MmE1YTFlZmM3MzU0ZTJiNTE3YTdkNmIzODExMTViNDQ2NjUzYmE1MmE1ZTBmZGUxNjgwYTI0ODkxNWM5OTUxMDI2YTE0MDRlMDUxM2QwMTU5ZDRlMw==',
        }

        headers = {
            'Accept': '*/*',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Connection': 'keep-alive',
            'Referer': 'https://author.baidu.com/',
            'Sec-Fetch-Dest': 'script',
            'Sec-Fetch-Mode': 'no-cors',
            'Sec-Fetch-Site': 'same-site',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                          'Chrome/114.0.0.0 Safari/537.36',
            'sec-ch-ua': '"Not.A/Brand";v="8", "Chromium";v="114", "Google Chrome";v="114"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
        }

        _jsonp = requests.get(url, headers=headers, cookies=cookies)
        # print(_jsonp.text)
        # breakpoint()
        try:
            content = json.loads(re.match(".*?({.*}).*", _jsonp.text, re.S).group(1)).get('data')
            # print(content)
            # breakpoint()
            for item in content.get('list'):
                target_dict = {'user_name': user_name, 'feed_id': item['feed_id'], 'thread_id': item['thread_id']}
                stamp_publish_time = item['dynamic_ctime']
                str_publish_time = time.localtime(int(stamp_publish_time))
                content_publish_time = time.strftime("%Y-%m-%d %H:%M:%S", str_publish_time)
                target_dict['created'] = content_publish_time

                # get deadline data
                if content_publish_time >= self.since_time:
                    # if self.until_time >= content_publish_time >= self.since_time:
                    target_dict['updated'] = content_publish_time

                    if 'url' in item['itemData'].keys():
                        target_dict['url'] = item['itemData']['url']
                    else:
                        target_dict['url'] = ''

                    if 'title' in item['itemData'].keys():

                        target_dict['title'] = item['itemData']['title']
                    else:
                        target_dict['title'] = ''

                    if 'article_id' in item['itemData'].keys():
                        target_dict['article_id'] = item['itemData']['article_id']
                    else:
                        target_dict['article_id'] = ''

                    if 'content' in item['itemData'].keys():
                        target_dict['content'] = item['itemData']['content']
                    else:
                        target_dict['content'] = ''

                    if 'duration' in item['itemData'].keys():
                        target_dict['duration'] = item['itemData']['duration']
                    else:
                        target_dict['duration'] = ''

                    if 'source' in item['itemData'].keys():
                        target_dict['source'] = item['itemData']['source']
                    else:
                        target_dict['source'] = ''

                    target_dict['type'] = item['itemType']

                    # get detail data: read、like、comment
                    origin_url = 'https://mbd.baidu.com/webpage?type=homepage' \
                                 '&action=interact' \
                                 '&format=jsonp' \
                                 '&Tenger-Mhor=2245291379' \
                                 '&uk=nfVcq7tJjMOYtV0Jyqq0xw'
                    params = {
                        "user_type": item["user_type"],
                        "feed_id": item["feed_id"],
                        "thread_id": item["thread_id"],
                        "dynamic_id": item["dynamic_id"],
                        "dynamic_type": item["dynamic_type"],
                        "dynamic_sub_type": item["dynamic_sub_type"],
                        "dynamic_ctime": item["dynamic_ctime"],
                        "is_top": item["is_top"],
                    }
                    url = origin_url + '&params=[' + str(json.dumps(params)) + ']'

                    detail_jsonp = requests.get(url, headers=headers).text
                    # print(detail_jsonp.text)
                    try:
                        detail_content = json.loads(re.match(".*?({.*}).*", detail_jsonp, re.S).group(1)).get(
                            'data').get('user_list')
                        for k, v in detail_content.items():
                            detail = v
                            target_dict['praise_num'] = detail['praise_num']
                            target_dict['comment_num'] = detail['comment_num']
                            target_dict['read_num'] = detail['read_num']
                            target_dict['is_praise'] = detail['is_praise']
                            target_dict['forward_num'] = detail['forward_num']
                            target_dict['live_back_num'] = detail['live_back_num']
                    except:
                        # raise ValueError('Invalid Input')
                        target_dict['praise_num'] = ''
                        target_dict['comment_num'] = ''
                        target_dict['read_num'] = ''
                        target_dict['is_praise'] = ''
                        target_dict['forward_num'] = ''
                        target_dict['live_back_num'] = ''
                    finally:
                        target_dict['crawl_dtime'] = self.crawl_time
                        target_dict['project_name'] = self.project_name
                        target_dict['project_timezone'] = self.project_timezone
                        self.mongodb().insert_one(target_dict)

                else:
                    flag = "0"
                    ctime = content.get('query').get('ctime')
                    return flag, ctime
                time.sleep(random.randint(1, 3))
            flag = str(content.get('hasMore'))
            ctime = content.get('query').get('ctime')
            return flag, ctime

        except Exception as e:
            self.logger_init().warning("Function get_response Error:{}".format(e))

    def get_data(self, content):
        pass

    def cron_user(self, user_key, user_name):
        """
        :param user_key:
        :param user_name:
        :return:
        """
        start_ctime = '0'  # 默认为0, 可以调整数值（长时间采集出错情况下）
        page_num = 0
        # user_key = 'nfVcq7tJjMOYtV0Jyqq0xw'
        while True:
            '''
            url = 'https://mbd.baidu.com/webpage?tab=main' \
                  '&num=10' \
                  '&uk={}' \
                  '&source=pc' \
                  '&ctime={}' \
                  '&type=newhome' \
                  '&action=dynamic' \
                  '&format=jsonp' \
                  '&otherext=h5_20230302163851' \
                  '&Tenger-Mhor=1864694231'.format(user_key, start_ctime)  # API URL
            # print(url)
            # breakpoint()
            '''
            url = 'https://mbd.baidu.com/webpage?tab=main&num=10&uk={}&source=pc' \
                  '&type=newhome&action=dynamic&format=jsonp&otherext=h5_20230717181759' \
                  '&Tenger-Mhor=2680258296&callback=__jsonp0{}'.format(user_key, str(time.time()).replace('.', '')[:13])

            flag, ctime = self.get_response(url, user_name)
            start_ctime = ctime
            mm = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(start_ctime[:10])))

            self.logger_init().warning(
                'UserName: {} Article HasMore:{} Time is:{}.TimeStamp :{}'.format(user_name, flag, mm, ctime))
            since_time = time.mktime(time.strptime(self.since_time, '%Y-%m-%d %H:%M:%S'))

            if int(since_time) > int(start_ctime[:10]):
                break

            if flag == "0" or flag == 'None':
                break
            page_num += 1
            time.sleep(30)

    def start_run(self):
        users = (user for user in self.get_user())
        for user in users:
            user = user.split('#####')
            user_key = user[1].strip()
            user_name = user[0].strip()
            self.logger_init().warning('{}'.format(user_name))
            self.cron_user(user_key, user_name)
            time.sleep(30)


if __name__ == '__main__':
    BaiJiaHaoSpider = BaiJiaHao()
    BaiJiaHaoSpider.since_time = "2023-07-01 00:00:00"  # 指定起始时间，默认时间为2020-01-01
    # BaiJiaHaoSpider.until_time = "2022-01-10 00:00:00"  # 指定终止时间，默认时间为2020-12-01
    BaiJiaHaoSpider.project_name = "kexie"
    BaiJiaHaoSpider.project_timezone = "2023-Q1"
    BaiJiaHaoSpider.start_run()
