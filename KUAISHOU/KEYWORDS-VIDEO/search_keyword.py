# -*- coding:utf-8 -*-
# Author: mongoole
# Date: 2023-08-30
# SEARCH KEYWORD VIDEO

import io
import json
import logging
import sys
import time
import yaml
import pymongo
import requests

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')

project_name = 'GT'
project_timezone = '2023-Q'
since_time = time.mktime(time.strptime('2023-07-01 00:00:00', '%Y-%m-%d %H:%M:%S'))


def mongodb():
    # connect mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # choose the mongodb
    db = client.kuaishou_content
    # choose the collection
    mongo = db.keywordvideo_content
    return mongo


def get_user_url():
    # with open(r'E:\kexieproject\kuaishou\data_user\kuaishou_user_account_url.txt', 'r') as f:
    with open(r'E:\kexieproject\kuaishou\data_user\kuaishou_GT_user_account_url.txt', 'r') as f:
        user_account_urls = f.readlines()
    return user_account_urls


def get_cookie_yml():
    with open(r'E:\kexieproject\kuaishou\application.yml', 'rb') as f:
        cookie_config = yaml.safe_load(f)
    return cookie_config


def get_response(keyword, pcursor):
    keyword = keyword.encode('utf-8').decode('latin1')

    headers = {
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Origin': 'https://www.kuaishou.com',
        'Referer': 'https://www.kuaishou.com/search/video?searchKey=%E6%9A%B4%E9%9B%A8',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/116.0.0.0 Safari/537.36',
        'Cookie': str(get_cookie_yml()['cookie']),
        'accept': '*/*',
        'content-type': 'application/json',
        'sec-ch-ua': '"Chromium";v="116", "Not)A;Brand";v="24", "Google Chrome";v="116"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    data = '{"operationName":"visionSearchPhoto","variables":{"keyword":"%s","pcursor":"%s","page":"search",' \
           '"searchSessionId":"MTRfMjc0NjUxNzU4MV8xNjkzMzc3MjIzNzQ0X-aatOmbqF82Njg4"},"query":"fragment photoContent ' \
           'on PhotoEntity {\\n  __typename\\n  id\\n  duration\\n  caption\\n  originCaption\\n  likeCount\\n  ' \
           'viewCount\\n  commentCount\\n  realLikeCount\\n  coverUrl\\n  photoUrl\\n  photoH265Url\\n  manifest\\n  ' \
           'manifestH265\\n  videoResource\\n  coverUrls {\\n    url\\n    __typename\\n  }\\n  timestamp\\n  ' \
           'expTag\\n  animatedCoverUrl\\n  distance\\n  videoRatio\\n  liked\\n  stereoType\\n  ' \
           'profileUserTopPhoto\\n  musicBlocked\\n}\\n\\nfragment recoPhotoFragment on recoPhotoEntity {\\n  ' \
           '__typename\\n  id\\n  duration\\n  caption\\n  originCaption\\n  likeCount\\n  viewCount\\n  ' \
           'commentCount\\n  realLikeCount\\n  coverUrl\\n  photoUrl\\n  photoH265Url\\n  manifest\\n  ' \
           'manifestH265\\n  videoResource\\n  coverUrls {\\n    url\\n    __typename\\n  }\\n  timestamp\\n  ' \
           'expTag\\n  animatedCoverUrl\\n  distance\\n  videoRatio\\n  liked\\n  stereoType\\n  ' \
           'profileUserTopPhoto\\n  musicBlocked\\n}\\n\\nfragment feedContent on Feed {\\n  type\\n  author {\\n    ' \
           'id\\n    name\\n    headerUrl\\n    following\\n    headerUrls {\\n      url\\n      __typename\\n    ' \
           '}\\n    __typename\\n  }\\n  photo {\\n    ...photoContent\\n    ...recoPhotoFragment\\n    __typename\\n ' \
           ' }\\n  canAddComment\\n  llsid\\n  status\\n  currentPcursor\\n  tags {\\n    type\\n    name\\n    ' \
           '__typename\\n  }\\n  __typename\\n}\\n\\nquery visionSearchPhoto($keyword: String, $pcursor: String, ' \
           '$searchSessionId: String, $page: String, $webPageArea: String) {\\n  visionSearchPhoto(keyword: $keyword, ' \
           'pcursor: $pcursor, searchSessionId: $searchSessionId, page: $page, webPageArea: $webPageArea) {\\n    ' \
           'result\\n    llsid\\n    webPageArea\\n    feeds {\\n      ...feedContent\\n      __typename\\n    }\\n   ' \
           ' searchSessionId\\n    pcursor\\n    aladdinBanner {\\n      imgUrl\\n      link\\n      __typename\\n    ' \
           '}\\n    __typename\\n  }\\n}\\n"} ' % (keyword, pcursor)

    response = requests.post('https://www.kuaishou.com/graphql', headers=headers, data=data)
    try:
        if response.status_code == 200:
            resp_content = response.json()
            llsid = resp_content.get("llsid")
            pcursor = resp_content.get("data").get("visionSearchPhoto").get("pcursor")
            content_lst = resp_content.get("data").get("visionSearchPhoto")
            result = resp_content.get("data").get("visionSearchPhoto").get("result")
            # print(content_lst)
            # print(pcursor)
            # breakpoint()
            return content_lst, pcursor, result

    except Exception as e:
        logging.warning("GET RESPONSE FUNCTION HAS ERROR.{}".format(e))


def get_video_detail(keyword, content_lst,  mongo):

    video_lst = content_lst.get("feeds")
    for video in video_lst:
        video_info_dict = {}

        # AUTHOR INFORMAITON
        author_information = video.get("author")
        video_info_dict["name"] = author_information.get("name")
        video_info_dict["user_id"] = author_information.get("id")    # USER ID

        # VIDEO INFORMAITON
        video_information = video.get("photo")

        timestamp = str(video_information.get("timestamp"))[:10]
        if int(timestamp) > int(since_time):
            video_info_dict["videoId"] = video_information.get("id")    # COMMENT ID
            video_info_dict["title"] = video_information.get("caption")
            video_info_dict["commentCount"] = video_information.get("commentCount")
            video_info_dict["likeCount"] = video_information.get("likeCount")
            video_info_dict["realLikeCount"] = video_information.get("realLikeCount")
            video_info_dict["viewCount"] = video_information.get("viewCount")
            video_info_dict["duration"] = video_information.get("duration")
            timestamp = str(video_information.get("timestamp"))[:10]
            publish_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(timestamp)))
            video_info_dict["publish_time"] = publish_time

            # videoResource = video_information.get("videoResource").get("hevc").get("adaptationSet")[0]
            videoResource = video_information.get("videoResource").get("h264").get("adaptationSet")[0]
            # print(videoResource)
            # print(type(videoResource))
            video_representation = videoResource.get("representation")[0]
            video_info_dict["video_short_url"] = 'https://www.kuaishou.com/short-video/{}?' \
                                                 'authorId={}'.format(video_information.get("id"),
                                                                      author_information.get("id"))

            video_url = video_representation.get("url")
            video_info_dict["video_url"] = video_url
            video_info_dict["keyword"] = keyword
            video_info_dict["project_name"] = project_name
            video_info_dict["project_timezone"] = project_timezone

            # print(video_info_dict)
            # breakpoint()
            mongo.insert_one(video_info_dict)
            logging.warning('KEYWORD: {} -- ACCOUNT:{} TITLE:{} PUBLISH:{} INSERT TO MONGODB.'
                            .format(keyword, video_info_dict.get('name'), video_info_dict.get('title').strip()[:15],
                                    video_info_dict.get('publish_time')))
            time.sleep(0.5)

        else:
            logging.warning('{} ACCOUNT:{} THIS VIDEO INFORMATION\'S TIME WAS DEAD.{}'
                            .format('*' * 15, video_info_dict.get('nickname'), '*' * 15))
            deadline = True
            return deadline


def get_cursor(keyword,  mongo):

    pcursor = ""

    while 1:
        content_lst, next_pcursor, result = get_response(keyword, pcursor)
        if next_pcursor:
            deadline = get_video_detail(keyword, content_lst,  mongo)
            pcursor = next_pcursor
            logging.warning('{} NEXT CURSOR:{} {}'.format('*' * 15, next_pcursor, '*' * 15))
            if deadline:
                break
        else:
            break
        time.sleep(5)


def main():
    mongo = mongodb()
    # EG.
    keyword = '暴雨'
    get_cursor(keyword, mongo)


if __name__ == '__main__':
    main()
