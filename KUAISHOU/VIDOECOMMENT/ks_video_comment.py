# -*- coding:utf-8 -*-
# Author: mongoole
# Date: 2023-8-22

import io
import json
import logging
import sys
import yaml
import time
import pymongo
import requests

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')


def mongodb():
    # connect mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # choose the mongodb
    db = client.kuaishou_content
    # choose the collection
    mongo = db.comment_content
    return mongo


def get_comment_id():
    # with open(r'E:\kexieproject\kuaishou\data_user\kuaishou_user_account_url.txt', 'r') as f:
    with open(r'E:\kexieproject\kuaishou\data_user\kuaishou_video_comment_id.txt', 'r') as f:
        video_comment_ids = f.readlines()
    return video_comment_ids


def get_cookie_yml():
    with open(r'E:\kexieproject\kuaishou\application.yml', 'rb') as f:
        cookie_config = yaml.safe_load(f)
    return cookie_config


def get_response(photoId, pcursor):

    headers = {
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Origin': 'https://www.kuaishou.com',
        # 'Referer': 'https://www.kuaishou.com/short-video/3x8nvt3gznj83c4?authorId=3xcabjk2f68kenw&streamSource'
        #            '=profile&area=profilexxnull',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/115.0.0.0 Safari/537.36',
        'Cookie': str(get_cookie_yml()['cookie']),
        'accept': '*/*',
        'content-type': 'application/json',
        'sec-ch-ua': '"Not/A)Brand";v="99", "Google Chrome";v="115", "Chromium";v="115"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    data = '{"operationName":"commentListQuery","variables":{"photoId":"%s","pcursor":"%s"},"query":"query ' \
           'commentListQuery($photoId: String, $pcursor: String) {\\n  visionCommentList(photoId: $photoId, ' \
           'pcursor: $pcursor) {\\n    commentCount\\n    pcursor\\n    rootComments {\\n      commentId\\n      ' \
           'authorId\\n      authorName\\n      content\\n      headurl\\n      timestamp\\n      likedCount\\n      ' \
           'realLikedCount\\n      liked\\n      status\\n      authorLiked\\n      subCommentCount\\n      ' \
           'subCommentsPcursor\\n      subComments {\\n        commentId\\n        authorId\\n        authorName\\n   ' \
           '     content\\n        headurl\\n        timestamp\\n        likedCount\\n        realLikedCount\\n       ' \
           ' liked\\n        status\\n        authorLiked\\n        replyToUserName\\n        replyTo\\n        ' \
           '__typename\\n      }\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n"} ' % (photoId, pcursor)

    response = requests.post('https://www.kuaishou.com/graphql', headers=headers, data=data)
    try:
        if response.status_code == 200:
            resp_content = response.json()
            data = resp_content.get("data")
            visicomment_lst = data.get("visionCommentList")
            pcursor = data.get("visionCommentList").get("pcursor")
            # print(visicomment_lst)
            # print(pcursor)
            # breakpoint()
            return visicomment_lst, pcursor

    except Exception as e:
        logging.warning("GET RESPONSE FUNCTION HAS ERROR.{}".format(e))


def get_comment_detail(photoId, visicomment_lst, mongo):

    comment_lst = visicomment_lst.get("rootComments")
    # print(video_lst)
    # breakpoint()
    for comment in comment_lst:
        comment_dict = {}

        # AUTHOR INFORMAITON
        comment_dict["video_comment_id"] = photoId  # VIDEO ROOT COMMENT ID
        comment_dict["authorLiked"] = comment.get("authorLiked")
        comment_dict["authorName"] = comment.get("authorName")
        comment_dict["authorId"] = comment.get("authorId")
        comment_dict["author_headurl"] = comment.get("headurl")

        # TEXT INFORMAITON
        comment_dict["commentId"] = comment.get("commentId")    # AUTHOR COMMENT ID
        comment_dict["content"] = comment.get("content")
        comment_dict["liked"] = comment.get("liked")
        comment_dict["likeCount"] = comment.get("likeCount")
        comment_dict["realLikeCount"] = comment.get("realLikeCount")
        timestamp = str(comment.get("timestamp"))[:10]
        publish_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(timestamp)))
        comment_dict["publish_time"] = publish_time
        comment_dict["status"] = comment.get("status")
        # print(comment_dict)
        # breakpoint()
        mongo.insert_one(comment_dict)
        logging.warning('ACCOUNT:{} CONTENT:{} PUBLISH:{} INSERT TO MONGODB.'
                        .format(comment_dict.get('authorName'), comment_dict.get('content').strip(),
                                comment_dict.get('publish_time')))
        time.sleep(0.2)


def get_cursor(photoId, mongo):

    pcursor = ""

    while 1:
        visicomment_lst, next_pcursor = get_response(photoId, pcursor)
        if next_pcursor == "no_more":
            break
        else:
            get_comment_detail(photoId, visicomment_lst, mongo)
            pcursor = next_pcursor
            logging.warning('{} NEXT CURSOR:{} {}'.format('*' * 15, next_pcursor, '*' * 15))
        time.sleep(5)


def main():
    mongo = mongodb()
    # photoId = "3xvmt73ps8f7zj9"     # COMMENT ID
    for photoId in get_comment_id():
        photoId = photoId.strip()
        get_cursor(photoId, mongo)
        logging.warning('{} COMMENT ID:{}  WAS FINISHED. {}'.format('*' * 15, photoId, '*' * 15))
        time.sleep(10)


if __name__ == '__main__':
    main()
