# -*- coding:utf-8 -*-
# Author: mongoole
# Date: 2023-08-22

import io
import json
import logging
import sys
import time
import pymongo
import requests

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
logging.basicConfig(level=logging.WARN, format='%(asctime)s-:%(message)s')

project_name = 'KeXie'
project_timezone = '2023-Q3'
since_time = time.mktime(time.strptime('2023-07-01 00:00:00', '%Y-%m-%d %H:%M:%S'))



def mongodb():
    # connect mongodb
    client = pymongo.MongoClient(host='127.0.0.1', port=27017)
    # choose the mongodb
    db = client.kuaishou_content
    # choose the collection
    mongo = db.user_content
    return mongo


def get_user_url():
    with open(r'E:\kexieproject\kuaishou\data_user\kuaishou_user_account_url.txt', 'r') as f:
        user_account_urls = f.readlines()
    return user_account_urls


def get_response(userId, pcursor):
    cookies = {
            '',
    }

    headers = {
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Connection': 'keep-alive',
        'Origin': 'https://www.kuaishou.com',
        'Referer': 'https://www.kuaishou.com/profile/{}'.format(userId),
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/115.0.0.0 '
                      'Safari/537.36',
        'accept': '*/*',
        'content-type': 'application/json',
        'sec-ch-ua': '"Not/A)Brand";v="99", "Google Chrome";v="115", "Chromium";v="115"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    data = '{"operationName":"visionProfilePhotoList","variables":{"userId": "%s","pcursor":"%s",' \
           '"page":"profile"},"query":"fragment photoContent on PhotoEntity {\\n  __typename\\n  id\\n  duration\\n  ' \
           'caption\\n  originCaption\\n  likeCount\\n  viewCount\\n  commentCount\\n  realLikeCount\\n  coverUrl\\n  ' \
           'photoUrl\\n  photoH265Url\\n  manifest\\n  manifestH265\\n  videoResource\\n  coverUrls {\\n    url\\n    ' \
           '__typename\\n  }\\n  timestamp\\n  expTag\\n  animatedCoverUrl\\n  distance\\n  videoRatio\\n  liked\\n  ' \
           'stereoType\\n  profileUserTopPhoto\\n  musicBlocked\\n}\\n\\nfragment recoPhotoFragment on ' \
           'recoPhotoEntity {' \
           '\\n  __typename\\n  id\\n  duration\\n  caption\\n  originCaption\\n  likeCount\\n  viewCount\\n  ' \
           'commentCount\\n  realLikeCount\\n  coverUrl\\n  photoUrl\\n  photoH265Url\\n  manifest\\n  ' \
           'manifestH265\\n  ' \
           'videoResource\\n  coverUrls {\\n    url\\n    __typename\\n  }\\n  timestamp\\n  expTag\\n  ' \
           'animatedCoverUrl\\n  distance\\n  videoRatio\\n  liked\\n  stereoType\\n  profileUserTopPhoto\\n  ' \
           'musicBlocked\\n}\\n\\nfragment feedContent on Feed {\\n  type\\n  author {\\n    id\\n    name\\n    ' \
           'headerUrl\\n    following\\n    headerUrls {\\n      url\\n      __typename\\n    }\\n    __typename\\n  ' \
           '}\\n ' \
           ' photo {\\n    ...photoContent\\n    ...recoPhotoFragment\\n    __typename\\n  }\\n  canAddComment\\n  ' \
           'llsid\\n  status\\n  currentPcursor\\n  tags {\\n    type\\n    name\\n    __typename\\n  }\\n  ' \
           '__typename\\n}\\n\\nquery visionProfilePhotoList($pcursor: String, $userId: String, $page: String, ' \
           '$webPageArea: String) {\\n  visionProfilePhotoList(pcursor: $pcursor, userId: $userId, page: $page, ' \
           'webPageArea: $webPageArea) {\\n    result\\n    llsid\\n    webPageArea\\n    feeds {\\n      ' \
           '...feedContent\\n      __typename\\n    }\\n    hostName\\n    pcursor\\n    __typename\\n  }\\n}\\n"} ' \
           % (userId, pcursor)

    # print(data)
    # breakpoint()
    response = requests.post('https://www.kuaishou.com/graphql', headers=headers, cookies=cookies, data=data)
    try:
        if response.status_code == 200:
            resp_content = response.json()
            llsid = resp_content.get("llsid")
            pcursor = resp_content.get("data").get("visionProfilePhotoList").get("pcursor")
            content_lst = resp_content.get("data").get("visionProfilePhotoList")
            result = resp_content.get("data").get("visionProfilePhotoList").get("result")
            # print(pcursor)
            # breakpoint()
            return content_lst, pcursor, result

    except Exception as e:
        logging.warning("GET RESPONSE FUNCTION HAS ERROR.{}".format(e))


def get_video_detail(content_lst,  mongo):

    video_lst = content_lst.get("feeds")
    for video in video_lst:
        video_info_dict = {}

        # AUTHOR INFORMAITON
        author_information = video.get("author")
        video_info_dict["name"] = author_information.get("name")
        video_info_dict["id"] = author_information.get("id")

        # VIDEO INFORMAITON
        video_information = video.get("photo")

        timestamp = str(video_information.get("timestamp"))[:10]
        if int(timestamp) > int(since_time):
            video_info_dict["videoId"] = video_information.get("id")
            video_info_dict["title"] = video_information.get("caption")
            video_info_dict["commentCount"] = video_information.get("commentCount")
            video_info_dict["likeCount"] = video_information.get("likeCount")
            video_info_dict["realLikeCount"] = video_information.get("realLikeCount")
            video_info_dict["viewCount"] = video_information.get("viewCount")
            video_info_dict["duration"] = video_information.get("duration")
            timestamp = str(video_information.get("timestamp"))[:10]
            publish_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(timestamp)))
            video_info_dict["publish_time"] = publish_time

            videoResource = video_information.get("videoResource").get("hevc").get("adaptationSet")[0]
            # print(videoResource)
            # print(type(videoResource))
            video_representation = videoResource.get("representation")[0]
            video_url = video_representation.get("url")
            video_info_dict["video_url"] = video_url

            # print(video_info_dict)
            # breakpoint()
            mongo.insert_one(video_info_dict)
            logging.warning('ACCOUNT:{} TITLE:{} PUBLISH:{} INSERT TO MONGODB.'
                            .format(video_info_dict.get('name'), video_info_dict.get('title').strip()[:15],
                                    video_info_dict.get('publish_time')))
            time.sleep(0.5)

        else:
            logging.warning('{} ACCOUNT:{} THIS VIDEO INFORMATION\'S TIME WAS DEAD.{}'
                            .format('*' * 15, video_info_dict.get('nickname'), '*' * 15))
            deadline = True
            return deadline


def get_cursor(userId,  mongo):

    pcursor = ""

    while 1:
        content_lst, next_pcursor, result = get_response(userId, pcursor)
        if next_pcursor:
            deadline = get_video_detail(content_lst,  mongo)
            pcursor = next_pcursor

            if deadline:
                break
        else:
            break
        time.sleep(5)


def main():
    mongo = mongodb()
    # EG. userId = "3xcabjk2f68kenw"
    for account in get_user_url():
        userId = account.split('/')[-1]
        get_cursor(userId, mongo)


if __name__ == '__main__':
    main()
